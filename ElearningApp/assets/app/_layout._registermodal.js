﻿(function () {
    $.material.init({
        "inputElements": "#_layout_register_form input.form-control, #_layout_register_form textarea.form-control, #_layout_register_form select.form-control"
    });
    $("#_layout_register_doc, #_layout_register_ndoc").change(function () {
        var l_tdoc = $("#_layout_register_doc").val(), l_ndoc = $("#_layout_register_ndoc").val();
        if (!gf_isValue(l_tdoc) || !gf_isValue(l_ndoc)) {
            return;
        }
        $("#_layout_register_alert").hide();
        $("#_layout_register_alert_message").text("");
        $("#_layout_register_form .app-error").removeClass("app-error");
        $("#_layout_register_form .app-error-message").remove();
        $.ajax({
            data: JSON.stringify({ "DocumentType": l_tdoc, "DocumentNumber": l_ndoc }),
            url: '/Account/GetRegister',
            type: 'post',
            async: false,
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            success: _success
        });
        function _success(r) {
            if (r.status == 1) {
                var l_r = r.response;
                if (l_r[0] == "New") {
                    $("#_layout_register_lastname1, #_layout_register_lastname2, #_layout_register_firstname, #_layout_register_email, #_layout_register_phone").prop("disabled", false);
                    $("#_layout_register_lastname1").val("");
                    $("#_layout_register_lastname2").val("");
                    $("#_layout_register_firstname").val("");
                    $("#_layout_register_email").val("");
                    $('#_layout_register_phone').val("");
                } else {
                    $("#_layout_register_lastname1, #_layout_register_lastname2, #_layout_register_firstname, #_layout_register_email, #_layout_register_phone").prop("disabled", true);
                    $("#_layout_register_lastname1").val(l_r[1]);
                    $("#_layout_register_lastname2").val(l_r[2]);
                    $("#_layout_register_firstname").val(l_r[3]);
                    $("#_layout_register_email").data("initial", l_r[4]).val(l_r[4]);
                    $('#_layout_register_phone').val(l_r[5]);
                    if (l_r[0] == "Deny") {
                        $("#_layout_register_alert_message").text(l_r[6]);
                        $("#_layout_register_alert").show();
                    }
                }
                $("#_layout_register_form").data({ "action": l_r[0] });
            }
        }
    });
    $("#_layout_register_form").submit(function (evt) {
        evt.preventDefault();
    });
    $("#_layout_register_lastname1, #_layout_register_lastname2, #_layout_register_firstname").data({
        "PresaveRequired": true,
        "PresaveJson": '{"maxLength":100}'
    });
    $("#_layout_register_doc").data("PresaveRequired", true);
    $("#_layout_register_ndoc, #_layout_register_email").data({
        "PresaveRequired": true,
        "PresaveJson": '{"maxLength":50}'
    });
    $('#_layout_register_phone').data('PresaveJson', '{"maxLength":50}');
    $("#_layout_register_email").data("PresaveList", "email");
    gf_Helpers_DocumentTypeDropDown({
        $: $("#_layout_register_doc"),
        callback: function (o) {
            o.val(null).addClass('selectpicker').selectpicker();
        }
    });
}());