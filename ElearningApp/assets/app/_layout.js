﻿"use strict";
$('document').ready(function () {
    $('#ms-navbar li.app-nav-item-active')
        .addClass('active')
        .removeClass('app-nav-item-active');
    new Typed("#typed1",
        {
            strings: ["Virtual de Enseñanza"],
            typeSpeed: 40, startDelay: 2e3, loop: !0, backDelay: 1200
        });
    //new Typed("#typed2",
    //    {
    //        strings: ["Virtual de Enseñanza"],
    //        typeSpeed: 40, startDelay: 2e3, loop: !0, backDelay: 1200
    //    });
    new Typed("#typed3",
        {
            strings: ["Virtual de Enseñanza"],
            typeSpeed: 40, startDelay: 2e3, loop: !0, backDelay: 1200
        });
    /* --------------------------------------------------------
        Session
        --------------------------------------------------------   */
    (function () {
        $(window).focus(function (e) {
            if (e.target != this) {
                return;
            }
            fn_get_status();
        });
        function fn_get_status() {
            var l_status = false, l_session, l_window_session = sessionStorage.getItem("d2luZG93LXNlc3Npb24=");
            $.ajax({
                url: '/Account/GetStatus',
                type: 'post',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: _success,
                complete: _complete
            });
            function _success(r) {
                l_status = r.status;
                l_session = r.session;
            }
            function _complete() {
                if (l_status) {
                    if (l_window_session != l_session) {
                        window.location.reload(true);
                    }
                } else {
                    if (window.location.pathname != '/') {
                        if (!sessionStorage.getItem("d2luZG93LXJlbG9hZA==")) {
                            sessionStorage.setItem("d2luZG93LXJlbG9hZA==", '1');
                        }
                        var l_reload = parseInt(sessionStorage.getItem("d2luZG93LXJlbG9hZA=="));
                        if (l_reload <= 5) {
                            l_reload++;
                            sessionStorage.setItem("d2luZG93LXJlbG9hZA==", l_reload);
                            window.location.reload(true);
                        }
                    }
                }
            }
        }
        //init
        fn_get_status();
    }());
    (function () {
        setInterval(function () {
            $.ajax({
                url: '/Account/KeepActive',
                type: 'post',
                dataType: 'json',
                contentType: "application/json; charset=utf-8"
            });
        }, 300000);//active session every 5 minutes
    }());
});
function gf__layout_login_modal(o, to) {
    $(o).blur();
    if ($(o).closest("div.sb-slidebar-container").length != 0) {
        $("a.btn-navbar-menu").trigger("click");
    }
    window.bGF5b3V0X2xvZ2luX21vZGFsX3Rv = to;
    window.bGF5b3V0X2xvZ2luX21vZGFs = $.daAlert({
        content: {
            type: "ajax",
            value: {
                url: "/Account/LoginModal"
            }
        },
        maximizable: false,
        title: "<h3 class='mt-0 mb-0'>Inicia sesión</h3>",
        movable: false,
        onclose: function () {
            window.bGF5b3V0X2xvZ2luX21vZGFs = null;
        }
    });
}
function gf__layout_register_modal(o) {
    $(o).blur();
    var l_a;
    if ($(o).closest("div.sb-slidebar-container").length != 0) {
        $("a.btn-navbar-menu").trigger("click");
    }
    if (window.bGF5b3V0X2xvZ2luX21vZGFs) {
        window.bGF5b3V0X2xvZ2luX21vZGFs.close();
        window.bGF5b3V0X2xvZ2luX21vZGFs = null;
    }
    l_a = $.daAlert({
        content: {
            type: "ajax",
            value: {
                url: "/Account/RegisterModal"
            }
        },
        buttons: [
            {
                type: "cancel",
                fn: function () {
                    return true;
                }
            },
            {
                type: "custom",
                icon: "",
                text: "Registrar",
                className: "btn btn-raised btn-primary",
                fn: fn_save
            }
        ],
        maximizable: false,
        title: "<h3 class='mt-0 mb-0'>Regístrate</h3>",
        maxWidth: "700px",
        movable: false
    });
    function fn_save(p, e) {
        if (!$("#_layout_register_form").data("action") || $("#_layout_register_form").data("action") == "Deny") {
            if ($("#_layout_register_form").data("action") == "Deny") {
                $.jGrowl("Usted ya está registrado", {
                    header: "Error",
                    type: "danger"
                });
            }
            return false;
        }
        var l_psave = gf_presaveForm($("#_layout_register_form")), l_success = false;
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return false;
        }
        $.ajax({
            data: JSON.stringify({
                LastName1: $("#_layout_register_lastname1").val().trim(),
                LastName2: $("#_layout_register_lastname2").val().trim(),
                FirstName: $("#_layout_register_firstname").val().trim(),
                DocumentType: $("#_layout_register_doc").val(),
                DocumentNumber: $("#_layout_register_ndoc").val().trim(),
                Email: $("#_layout_register_form").data("action") == "OnlyAccess" ?
                    $("#_layout_register_email").data("initial") : $("#_layout_register_email").val().trim(),
                Phone: $("#_layout_register_phone").val().trim(),
                Actions: $("#_layout_register_form").data("action")
            }),
            url: "/Account/Register",
            type: "post",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                gf_addLoadingTo(e);
            },
            success: function (r) {
                if (parseInt(r.status) == 1000) {
                    if ($("#_layout_register_modal_confirm").length == 0) {
                        $("body").append('<div id="_layout_register_modal_confirm" class="modal fade">' +
                            '<div class="modal-dialog modal-confirm">' +
                            '<div class="modal-content">' +
                            '<div class="modal-header">' +
                            '<div class="icon-box">' +
                            '<i class="material-icons">&#xE876;</i>' +
                            '</div>' +
                            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                            '</div>' +
                            '<div class="modal-body text-center">' +
                            '<h4>¡Genial!</h4>	' +
                            '<p data-modal="message">' + r.response + '</p>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>');
                    } else {
                        $("#_layout_register_modal_confirm p[data-modal=message]").html(r.response);
                    }
                    $('#_layout_register_modal_confirm').modal('show');
                    l_success = true;
                } else {
                    $.jGrowl(r.response, {
                        header: "Error",
                        type: "danger",
                        clipboard: r.exception
                    });
                }
            },
            complete: function () {
                gf_removeLoadingTo(e);
                if (l_success) {
                    l_a.close();
                }
            }
        });
        return false;
    }
}
function gf__layout_change_password_modal(o) {
    $(o).blur();
    var l_a;
    if ($(o).closest("div.sb-slidebar-container").length != 0) {
        $("a.btn-navbar-menu").trigger("click");
    }
    l_a = $.daAlert({
        content: {
            type: "ajax",
            value: {
                url: "/Account/ChangePasswordModal"
            }
        },
        buttons: [
            {
                type: "cancel",
                fn: function () {
                    return true;
                }
            },
            {
                type: "custom",
                icon: "",
                text: "Guardar",
                className: "btn btn-raised btn-primary",
                fn: fn_save
            }
        ],
        maximizable: false,
        title: "<h3 class='mt-0 mb-0'>Cambiar contraseña</h3>",
        //maxWidth: "700px",
        movable: false
    });
    function fn_save(p, e) {
        var l_psave = gf_presaveForm($("#_layout_changepassword_form")), l_param, l_success = false;
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return false;
        }
        l_param = {
            OldPassword: $('#_layout_changepassword_old_password').val(),
            NewPassword: $('#_layout_changepassword_new_password').val(),
            ConfirmNewPassword: $('#_layout_changepassword_confirm_new_password').val()
        };
        if (l_param.NewPassword != l_param.ConfirmNewPassword) {
            $('#_layout_changepassword_confirm_new_password').addClass('app-error');
            $('#_layout_changepassword_confirm_new_password').after('<div class="app-error-message">Debe coincidir con la Contraseña Nueva</div>');
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return false;
        }
        $.ajax({
            data: JSON.stringify(l_param),
            url: "/Account/ChangePassword",
            type: "post",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                gf_addLoadingTo(e);
            },
            success: function (r) {
                if (parseInt(r.status) == 1000) {
                    $.jGrowl(r.response, {
                        header: 'Información',
                        type: 'primary'
                    });
                    l_success = true;
                } else {
                    $.jGrowl(r.response, {
                        header: 'Error',
                        type: 'danger',
                        clipboard: r.exception
                    });
                }
            },
            complete: function () {
                gf_removeLoadingTo(e);
                if (l_success) {
                    l_a.close();
                }
            }
        });
        return false;
    }
}

function gf__layout_retrieve_user_pass_modal(o) {
    $(o).blur();
    var l_a;
    if ($(o).closest("div.sb-slidebar-container").length != 0) {
        $("a.btn-navbar-menu").trigger("click");
    }
    if (window.bGF5b3V0X2xvZ2luX21vZGFs) {
        window.bGF5b3V0X2xvZ2luX21vZGFs.close();
        window.bGF5b3V0X2xvZ2luX21vZGFs = null;
    }
    l_a = $.daAlert({
        content: {
            type: "ajax",
            value: {
                url: "/Account/RetrieveUserPassModal"
            }
        },
        buttons: [
            {
                type: "cancel",
                fn: function () {
                    return true;
                }
            },
            {
                type: "custom",
                icon: "",
                text: "Enviar",
                className: "btn btn-raised btn-primary",
                fn: fn_save
            }
        ],
        maximizable: false,
        title: "<h3 class='mt-0 mb-0'>Recuperar Usuario y Contraseña</h3>",
        maxWidth: "500px",
        movable: false
    });
    function fn_save(p, e) {
        var l_psave = gf_presaveForm($("#_layout_register_form")), l_success = false;
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return false;
        }
        $.ajax({
            data: JSON.stringify({
                Email: $("#_layout_register_form").data("action") == "OnlyAccess" ?
                    $("#_layout_register_email").data("initial") : $("#_layout_register_email").val().trim(),
                Actions: $("#_layout_register_form").data("action")
            }),
            url: "/Account/RetrieveUserPass",
            type: "post",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                gf_addLoadingTo(e);
            },
            success: function (r) {
                if (parseInt(r.status) == 1000) {
                    $.jGrowl(r.response, {
                        header: "Información",
                        type: "primary"
                    });
                    l_success = true;
                } else {
                    $.jGrowl(r.response, {
                        header: "Error",
                        type: "danger",
                        clipboard: r.exception
                    });
                }
            },
            complete: function () {
                gf_removeLoadingTo(e);
                if (l_success) {
                    l_a.close();
                }
            }
        });
        return false;
    }
}