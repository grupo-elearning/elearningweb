﻿"use strict";
function gf_fileValidation(p_settings) {
    var l_default = {
        fileInput: null,
        imgPreview: null,
        extensions: /(\.jpg|\.jpeg|\.png|\.bmp)$/i,
        showPreview: true,
        cleanAfter: true,
        fnError: function () {
        }
    };
    l_default = $.extend({}, l_default, p_settings);
    var filePath = l_default.fileInput.val(), fileInput = l_default.fileInput[0], reader;
    if (filePath.trim() == '') {
        l_default.fileInput.val('');
        return false;
    }
    if (filePath.trim() != '' && !l_default.extensions.exec(filePath)) {
        if (l_default.cleanAfter) {
            l_default.fileInput.val('');
        }
        l_default.fnError();
        return false;
    }
    if (l_default.showPreview) {
        if (fileInput.files && fileInput.files[0]) {
            reader = new FileReader();
            reader.onload = function (e) {
                l_default.imgPreview.attr('src', e.target.result);
            };
            reader.readAsDataURL(fileInput.files[0]);
        }
    }
    return true;
}
var gf_getClone = (function () {
    // @Private
    var _toString = Object.prototype.toString;
    // @Private
    function _clone(from, dest, objectsCache) {
        var prop;
        // determina si @from es un valor primitivo o una funcion
        if (from == null || typeof from != "object") return from;
        // revisa si @from es un objeto ya guardado en cache
        if (_toString.call(from) == "[object Object]") {
            if (objectsCache.filter(function (item) {
                return item == from;
            }).length) return from;
            // guarda la referencia de los objetos creados
            objectsCache.push(from);
        }
        // determina si @from es una instancia de alguno de los siguientes constructores
        if (from.constructor == Date || from.constructor == RegExp || from.constructor == Function ||
            from.constructor == String || from.constructor == Number || from.constructor == Boolean) {
            return new from.constructor(from);
        }
        if (from.constructor != Object && from.constructor != Array) return from;
        // crea un nuevo objeto y recursivamente itera sus propiedades
        dest = dest || new from.constructor();
        for (prop in from) {
            // TODO: allow overwrite existing properties
            dest[prop] = (typeof dest[prop] == "undefined" ?
                _clone(from[prop], null, objectsCache) :
                dest[prop]);
        }
        return dest;
    }
    // función retornada en el closure
    return function (from, dest) {
        var objectsCache = [];
        return _clone(from, dest, objectsCache);
    };
}());
var g_isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        return (g_isMobile.Android() || g_isMobile.BlackBerry() || g_isMobile.iOS() || g_isMobile.Opera() || g_isMobile.Windows());
    }
};
function gf_isValue(p_data) {
    return (typeof p_data != 'undefined' && p_data != null && p_data.toString().trim() != '');
}
function gf_isNumeric(p_data) {
    return p_data.toString().trim() == '' ? false : !isNaN(p_data);
}
function gf_isInteger(p_data) {
    try {
        return gf_isNumeric(p_data) ? parseFloat(p_data) % 1 == 0 : false;
    } catch (e) {
        return false;
    }
}
function gf_noZero(p_data) {
    try {
        return gf_isNumeric(p_data) ? parseFloat(p_data) != 0 : false;
    } catch (e) {
        return false;
    }
}
function gf_isPositive(p_data, p_noZero) {
    try {
        return gf_isNumeric(p_data) ? typeof p_noZero == 'undefined' ? parseFloat(p_data) >= 0 : p_noZero ? parseFloat(p_data) > 0 : parseFloat(p_data) >= 0 : false;
    } catch (e) {
        return false;
    }
}
function gf_isEmail(p_data) {
    try {
        return /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(p_data);
    } catch (e) {
        return false;
    }
}
function gf_getFileName(p_fakepath) {
    return p_fakepath.split(/(\\|\/)/g).pop();
}
function gf_getNameMonth(p_number, pb_shortName, ps_language) {
    var li_number, lb_shortName, ls_language;
    var l_lang_es = ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO', 'JULIO', 'AGOSTO', 'SEPTIEMBRE', 'OCTUBRE', 'NOVIEMBRE', 'DICIEMBRE'];
    var l_lang_es_short = ['ENE', 'FEB', 'MAR', 'ABR', 'MAY', 'JUN', 'JUL', 'AGO', 'SEP', 'OCT', 'NOV', 'DIC'];
    li_number = typeof p_number == 'string' ? parseInt(p_number) : p_number;
    lb_shortName = typeof pb_shortName == 'undefined' ? false : pb_shortName == null ? false : pb_shortName;
    ls_language = typeof ps_language == 'undefined' ? 'es' : ps_language == null ? 'es' : ps_language;
    return lb_shortName ? l_lang_es_short[(li_number - 1)] : l_lang_es[(li_number - 1)];
}
function gf_getCapitalize(ps_string) {
    var l_str = ps_string.toLowerCase();
    return l_str.charAt(0).toUpperCase() + l_str.slice(1);
}
function gf_getLoremIpsum(p_paragraphs, p_letters) {
    var l_lorem = "lorem ipsum dolor sit amet consectetur adipiscing elit phasellus convallis tempor turpis eu accumsan ex vestibulum at curabitur semper nam sed tellus nunc aliquam velit augue vel lectus tempus leo sagittis malesuada in suspendisse nisl ac quisque quis enim pulvinar arcu vehicula urna vivamus purus condimentum diam lacinia suscipit praesent efficitur ligula blandit donec euismod orci ornare tristique sollicitudin scelerisque bibendum tortor libero pretium justo vitae morbi auctor non felis interdum fusce faucibus pellentesque ultrices class aptent taciti sociosqu ad litora torquent per conubia nostra inceptos himenaeos cras mattis congue mauris nec dui aenean odio varius porta metus cursus ut lobortis laoreet neque volutpat massa sem integer ante commodo viverra a elementum eget quam mi eros feugiat dictum pharetra risus proin sapien mollis nulla facilisi facilisis venenatis maecenas nisi imperdiet gravida erat est et magna lacus potenti dignissim placerat maximus vulputate posuere fermentum iaculis ullamcorper etiam id eleifend porttitor egestas fringilla rutrum rhoncus molestie luctus tincidunt duis nullam finibus consequat sodales ultricies hendrerit aliquet nibh habitant senectus netus fames hac habitasse platea dictumst dapibus natoque penatibus magnis dis parturient montes nascetur ridiculus mus primis cubilia curae";
    var l_list_token = l_lorem.split(" ");
    var l_letters = typeof p_letters == 'undefined' ? 10 : p_letters;
    var l_paragraphs = typeof p_paragraphs == 'undefined' ? 5 : p_paragraphs;
    var l_return = null;
    for (var i = 1; i <= l_paragraphs; i++) {
        if (l_return == null) {
            l_return = gf_getCapitalize(l_list_token[Math.floor((Math.random() * l_list_token.length) + 0)]) + " ";
        } else {
            l_return += " " + gf_getCapitalize(l_list_token[Math.floor((Math.random() * l_list_token.length) + 0)]) + " ";
        }
        for (var j = 1; j <= l_letters - 1; j++) {
            if (j == l_letters - 1) {
                l_return += l_list_token[Math.floor((Math.random() * l_list_token.length) + 0)] + ".";
            } else {
                l_return += l_list_token[Math.floor((Math.random() * l_list_token.length) + 0)] + " ";
            }
        }
    }
    return l_return.trim();
}
function gf_getFirstDateCurrentMonth(p_date) {
    var date = typeof p_date == 'undefined' ? new Date() : p_date == null ? new Date() : p_date;
    return new Date(date.getFullYear(), date.getMonth(), 1);
}
function gf_getLastDateCurrentMonth(p_date) {
    var date = typeof p_date == 'undefined' ? new Date() : p_date == null ? new Date() : p_date;
    return new Date(date.getFullYear(), date.getMonth() + 1, 0);
}
function gf_isYear(p_data, p_min, p_max) {
    var l_min = typeof p_min == 'undefined' ? 0 : p_min, l_max = typeof p_max == 'undefined' ? Number.POSITIVE_INFINITY : p_max, l_data = p_data;
    if (!gf_isInteger(l_data)) {
        return false;
    }
    l_data = parseInt(l_data);
    return l_data >= l_min && l_data <= l_max;
}
function gf_inArrayJson(p_item, p_array, p_key) {
    for (var i in p_array) {
        if (p_array[i][p_key] == p_item) {
            return true;
        }
    }
    return false;
}
function gf_inArray(p_item, p_array) {
    for (var i in p_array) {
        if (p_array[i] == p_item) {
            return true;
        }
    }
    return false;
}
function gf_roundNumber(p_number, p_scale) {
    if (!("" + p_number).includes("e")) {
        return +(Math.round(p_number + "e+" + p_scale) + "e-" + p_scale);
    } else {
        var arr = ("" + p_number).split("e");
        var sig = ""
        if (+arr[1] + p_scale > 0) {
            sig = "+";
        }
        return +(Math.round(+arr[0] + "e" + sig + (+arr[1] + p_scale)) + "e-" + p_scale);
    }
}
function gf_getRandomColorHex() {
    var l_pos, l_hex, l_rcolor;
    l_hex = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"];
    l_rcolor = "#";
    for (var i = 0; i < 6; i++) {
        l_pos = fn_random(0, l_hex.length)
        l_rcolor += l_hex[l_pos];
    }
    function fn_random(p_from, p_to) {
        var l_a = Math.random() * (p_to - p_from);
        l_a = Math.floor(l_a);
        return parseInt(p_from) + l_a;
    }
    return l_rcolor;
}
function gf_getLightenDarkenColor(r, n) {
    var a = r, t = !1; "#" == a[0] && (a = a.slice(1), t = !0); var e = parseInt(a, 16), g = (e >> 16) + n; 255 < g ? g = 255 : g < 0 && (g = 0); var i = (e >> 8 & 255) + n; 255 < i ? i = 255 : i < 0 && (i = 0); var o = (255 & e) + n; return 255 < o ? o = 255 : o < 0 && (o = 0), (t ? "#" : "") + (o | i << 8 | g << 16).toString(16)
}
function gf_fillNumber(p_num, p_len) {
    var l_len = typeof p_len == 'undefined' ? 2 : p_len;
    return gf_roundNumber(parseFloat(p_num), l_len).toFixed(l_len);
}
function gf_presaveForm(p_f, p_a) {
    var g = new Object();
    g.analysis = typeof p_a != 'undefined' ? p_a : typeof p_f.attr('data-presave-analysis') == 'undefined' ? 'insert' : p_f.attr('data-presave-analysis');//[insert|update]
    g.hasChange = false;
    g.hasError = false;
    g.presaveObjects = p_f.find('[data-presave-object]');//data-presave-object='Description'
    g.objects = [];
    g.rule = typeof p_f.attr('data-presave-rule') == 'undefined' ? 'object' : p_f.attr('data-presave-rule');//[object|attribute]
    g.erros = {
        'integer': 'entero',
        'numeric': 'numérico',
        'noZero': 'diferente de cero',
        'positive': 'positivo',
        'negative': 'negativo',
        'max': 'menor o igual a',
        'min': 'mayor o igual a',
        'maxLength': 'de longitud menor o igual a',
        'minLength': 'de longitud mayor o igual a',
        'length': 'de longitud igual a',
        'isRequired': 'es campo requerido',
        'maxDecimals': 'de cantidad de decimales menor o igual a',
        'minDecimals': 'de cantidad de decimales mayor o igual a',
        'email': 'un correo electrónico correcto'
    };

    //initialize list
    (function () {
        var l_o;
        for (var i = 0; i < g.presaveObjects.length; i++) {
            l_o = {};
            switch (g.rule) {
                case 'attribute':
                    l_o.json = typeof $(g.presaveObjects[i]).attr('data-presave-json') == 'undefined' ? null : $(g.presaveObjects[i]).attr('data-presave-json');
                    l_o.list = typeof $(g.presaveObjects[i]).attr('data-presave-list') == 'undefined' ? null : $(g.presaveObjects[i]).attr('data-presave-list');
                    l_o.required = typeof $(g.presaveObjects[i]).attr('data-presave-required') == 'undefined' ? false : $(g.presaveObjects[i]).attr('data-presave-required');
                    break;
                case 'object':
                    l_o.json = typeof $(g.presaveObjects[i]).data('PresaveJson') == 'undefined' ? null : $(g.presaveObjects[i]).data('PresaveJson');
                    l_o.list = typeof $(g.presaveObjects[i]).data('PresaveList') == 'undefined' ? null : $(g.presaveObjects[i]).data('PresaveList');
                    l_o.required = typeof $(g.presaveObjects[i]).data('PresaveRequired') == 'undefined' ? false : $(g.presaveObjects[i]).data('PresaveRequired');
                    break;
            }
            l_o.value = typeof $(g.presaveObjects[i]).val() == 'undefined' ? "" : $(g.presaveObjects[i]).val() == null ? "" : $(g.presaveObjects[i]).val().toString().trim();
            l_o.$ = $(g.presaveObjects[i]);
            l_o.hasError = false;
            l_o.errorMessage = '';
            l_o.hasException = false;
            l_o.hasChange = false;
            l_o.requiredError = false;
            l_o.valueInitial = typeof $(g.presaveObjects[i]).data('PresaveInitial') == 'undefined' ? null : $(g.presaveObjects[i]).data('PresaveInitial').toString().trim();
            l_o.description = $(g.presaveObjects[i]).attr('data-presave-object') == '' ? null : $(g.presaveObjects[i]).attr('data-presave-object');
            l_o.isEmpty = l_o.value == "";

            g.objects.push(l_o);
        }
    }());
    //initialize validation
    (function () {
        for (var i in g.objects) {
            //
            (function (p_o) {
                if (p_o.required && p_o.isEmpty) {
                    p_o.requiredError = true;
                    p_o.errorMessage = (p_o.description == null ? '[[ debe ingresar un valor en data-presave-object ]]' : p_o.description) + ' ' + g.erros.isRequired;
                }
            }(g.objects[i]));
            //validate
            if (!g.objects[i].requiredError && !g.objects[i].isEmpty) {
                //validate json, list
                try {
                    (function (p_o) {
                        if (p_o.json == null) {
                            return;
                        }
                        var l_json = JSON.parse(p_o.json);
                        for (var i in l_json) {
                            switch (i) {
                                case 'max':
                                    if (!lf_getValidation_max_n(p_o.value, parseInt(l_json[i]))) {
                                        p_o.errorMessage += g.erros[i] + ' ' + l_json[i] + ', ';
                                        p_o.hasError = true;
                                    }
                                    break;
                                case 'min':
                                    if (!lf_getValidation_min_n(p_o.value, parseInt(l_json[i]))) {
                                        p_o.errorMessage += g.erros[i] + ' ' + l_json[i] + ', ';
                                        p_o.hasError = true;
                                    }
                                    break;
                                case 'minLength':
                                    if (!lf_getValidation_minLength_n(p_o.value, parseInt(l_json[i]))) {
                                        p_o.errorMessage += g.erros[i] + ' ' + l_json[i] + ', ';
                                        p_o.hasError = true;
                                    }
                                    break;
                                case 'maxLength':
                                    if (!lf_getValidation_maxLength_n(p_o.value, parseInt(l_json[i]))) {
                                        p_o.errorMessage += g.erros[i] + ' ' + l_json[i] + ', ';
                                        p_o.hasError = true;
                                    }
                                    break;
                                case 'length':
                                    if (!lf_getValidation_length_n(p_o.value, parseInt(l_json[i]))) {
                                        p_o.errorMessage += g.erros[i] + ' ' + l_json[i] + ', ';
                                        p_o.hasError = true;
                                    }
                                    break;
                                case 'maxDecimals':
                                    if (!lf_getValidation_maxDecimals_n(p_o.value, parseInt(l_json[i]))) {
                                        p_o.errorMessage += g.erros[i] + ' ' + l_json[i] + ', ';
                                        p_o.hasError = true;
                                    }
                                    break;
                                case 'minDecimals':
                                    if (!lf_getValidation_minDecimals_n(p_o.value, parseInt(l_json[i]))) {
                                        p_o.errorMessage += g.erros[i] + ' ' + l_json[i] + ', ';
                                        p_o.hasError = true;
                                    }
                                    break;
                            }
                        }
                    }(g.objects[i]));
                    (function (p_o) {
                        if (p_o.list == null) {
                            return;
                        }
                        var l_list = p_o.list.split(',');
                        var l_key;
                        for (var i = 0; i < l_list.length; i++) {
                            l_key = l_list[i].trim();
                            switch (l_key) {
                                case 'numeric':
                                    if (!lf_getValidation_numeric(p_o.value)) {
                                        p_o.errorMessage += g.erros[l_key] + ', ';
                                        p_o.hasError = true;
                                    }
                                    break;
                                case 'integer':
                                    if (!lf_getValidation_integer(p_o.value)) {
                                        p_o.errorMessage += g.erros[l_key] + ', ';
                                        p_o.hasError = true;
                                    }
                                    break;
                                case 'noZero':
                                    if (!lf_getValidation_noZero(p_o.value)) {
                                        p_o.errorMessage += g.erros[l_key] + ', ';
                                        p_o.hasError = true;
                                    }
                                    break;
                                case 'positive':
                                    if (!lf_getValidation_positive(p_o.value)) {
                                        p_o.errorMessage += g.erros[l_key] + ', ';
                                        p_o.hasError = true;
                                    }
                                    break;
                                case 'negative':
                                    if (!lf_getValidation_negative(p_o.value)) {
                                        p_o.errorMessage += g.erros[l_key] + ', ';
                                        p_o.hasError = true;
                                    }
                                    break;
                                case 'email':
                                    if (!lf_getValidation_email(p_o.value)) {
                                        p_o.errorMessage += g.erros[l_key] + ', ';
                                        p_o.hasError = true;
                                    }
                                    break;
                            }
                        }
                    }(g.objects[i]));
                } catch (e) {
                    g.objects[i].hasError = true;
                    g.objects[i].errorMessage = e.message;
                    g.objects[i].hasException = true;
                }
            }
            if (g.objects[i].hasError && !g.objects[i].hasException && !g.objects[i].requiredError) {
                g.objects[i].errorMessage = g.objects[i].errorMessage.trim();
                g.objects[i].errorMessage = 'Debe ser: ' + g.objects[i].errorMessage.substring(0, (g.objects[i].errorMessage.length - 1));
            }
            //remove message error
            (function (p_o) {
                switch (p_o.$.prop('tagName')) {
                    case 'SELECT':
                        if (p_o.$.closest('div.input-group').length != 0) {
                            //p_o.$.closest('div.input-group').removeClass('app-error');
                            lf_removeError_message(p_o.$.closest('div.input-group'));
                        } else {
                            if (p_o.$.hasClass("selectpicker")) {
                                p_o.$.closest('div.bootstrap-select').removeClass('app-error');
                                lf_removeError_message(p_o.$.closest('div.bootstrap-select'));
                            } else if (p_o.$.next().hasClass("chosen-container")) {
                                p_o.$.next('div.chosen-container').find('a.chosen-single').removeClass('app-error');
                                lf_removeError_message(p_o.$.next('div.chosen-container'));
                            } else {
                                p_o.$.removeClass('app-error');
                                lf_removeError_message(p_o.$);
                            }
                        }
                        break;
                    default:
                        if (p_o.$.closest('div.input-group').length != 0) {
                            //p_o.$.closest('div.input-group').removeClass('app-error');
                            lf_removeError_message(p_o.$.closest('div.input-group'));
                        } else {
                            p_o.$.removeClass('app-error');
                            lf_removeError_message(p_o.$);
                        }
                        break;
                }
            }(g.objects[i]));
            //set message error
            (function (p_o) {
                if (!p_o.hasError && !p_o.requiredError) {
                    return;
                }
                switch (p_o.$.prop('tagName')) {
                    case 'SELECT':
                        if (p_o.$.closest('div.input-group').length != 0) {
                            //p_o.$.closest('div.input-group').addClass('app-error');
                            lf_setError_message(p_o.$.closest('div.input-group'), p_o.errorMessage);
                        } else {
                            if (p_o.$.hasClass("selectpicker")) {
                                p_o.$.closest('div.bootstrap-select').addClass('app-error');
                                lf_setError_message(p_o.$.closest('div.bootstrap-select'), p_o.errorMessage);
                            } else if (p_o.$.next().hasClass("chosen-container")) {
                                p_o.$.next('div.chosen-container').find('a.chosen-single').addClass('app-error');
                                lf_setError_message(p_o.$.next('div.chosen-container'), p_o.errorMessage);
                            } else {
                                p_o.$.addClass('app-error');
                                lf_setError_message(p_o.$, p_o.errorMessage);
                            }
                        }
                        break;
                    default:
                        if (p_o.$.closest('div.input-group').length != 0) {
                            //p_o.$.closest('div.input-group').addClass('app-error');
                            lf_setError_message(p_o.$.closest('div.input-group'), p_o.errorMessage);
                        } else {
                            p_o.$.addClass('app-error');
                            lf_setError_message(p_o.$, p_o.errorMessage);
                        }
                        break;
                }
            }(g.objects[i]));
        }
    }());
    //set global error
    (function () {
        for (var i in g.objects) {
            if (g.objects[i].hasError || g.objects[i].requiredError) {
                g.hasError = true;
                break;
            }
        }
    }());
    //look changes
    (function () {
        if (g.analysis != 'update') {
            return;
        }
        for (var i in g.objects) {
            switch (g.objects[i].$.prop('tagName')) {
                case 'INPUT':
                    switch (g.objects[i].$.attr('type')) {
                        case 'radio':
                        case 'checkbox':
                            if ((g.objects[i].$.is(":checked") ? '1' : '0') != g.objects[i].valueInitial) {
                                g.objects[i].hasChange = true;
                            }
                            break;
                        default:
                            if (g.objects[i].value != g.objects[i].valueInitial) {
                                g.objects[i].hasChange = true;
                            }
                            break;
                    }
                    break;
                default:
                    if (g.objects[i].value != g.objects[i].valueInitial) {
                        g.objects[i].hasChange = true;
                    }
                    break;
            }
        }
    }());
    //set global change
    (function () {
        for (var i in g.objects) {
            if (g.objects[i].hasChange) {
                g.hasChange = true;
                break;
            }
        }
    }());

    function lf_setError_message(p_$, ps_message) {
        p_$.after('<div class="app-error-message">' + ps_message + '</div>');
    }
    function lf_removeError_message(p_$) {
        if (p_$.next('div.app-error-message').length != 0) {
            p_$.next('div.app-error-message').remove();
        }
    }
    //
    //'minDecimals': 'n'
    function lf_getValidation_minDecimals_n(p_value, n) {
        try {
            if (!lf_getValidation_numeric(p_value)) {
                return false;
            }
            var ls_value = p_value.toString().trim();
            var li_posDot = ls_value.indexOf('.');
            if (li_posDot == -1) {
                return true;
            }
            return ((ls_value.length - 1) - li_posDot) >= n;
        } catch (e) {
            return false;
        }
    }
    //'maxDecimals': 'n'
    function lf_getValidation_maxDecimals_n(p_value, n) {
        try {
            if (!lf_getValidation_numeric(p_value)) {
                return false;
            }
            var ls_value = p_value.toString().trim();
            var li_posDot = ls_value.indexOf('.');
            if (li_posDot == -1) {
                return true;
            }
            return ((ls_value.length - 1) - li_posDot) <= n;
        } catch (e) {
            return false;
        }
    }
    //'length': 'n'
    function lf_getValidation_length_n(p_value, n) {
        return p_value.trim().length == n;
    }
    //'maxLength': 'n'
    function lf_getValidation_maxLength_n(p_value, n) {
        return p_value.trim().length <= n;
    }
    //'minLength': 'n'
    function lf_getValidation_minLength_n(p_value, n) {
        return p_value.trim().length >= n;
    }
    //'min': 'n'
    function lf_getValidation_min_n(p_value, n) {
        try {
            return lf_getValidation_numeric(p_value) ? parseFloat(p_value) >= parseFloat(n) : false;
        } catch (e) {
            return false;
        }
    }
    //'max': 'n'
    function lf_getValidation_max_n(p_value, n) {
        try {
            return lf_getValidation_numeric(p_value) ? parseFloat(p_value) <= parseFloat(n) : false;
        } catch (e) {
            return false;
        }
    }
    //'negative'
    function lf_getValidation_negative(p_value) {
        try {
            return lf_getValidation_numeric(p_value) ? parseFloat(p_value) < 0 : false;
        } catch (e) {
            return false;
        }
    }
    //'positive'
    function lf_getValidation_positive(p_value) {
        return gf_isPositive(p_value);
    }
    //'noZero'
    function lf_getValidation_noZero(p_value) {
        return gf_noZero(p_value);
    }
    //'integer'
    function lf_getValidation_integer(p_value) {
        return gf_isInteger(p_value);
    }
    //'number'
    function lf_getValidation_numeric(p_value) {
        return gf_isNumeric(p_value);
    }
    // 'email'
    function lf_getValidation_email(p_value) {
        return gf_isEmail(p_value);
    }
    return g;
}
function gf_addLoadingTo(p_o) {
    p_o.trigger("blur");
    p_o.append($('<div />').addClass('app-loading-to').append($('<div />').addClass('lds-circle').append($('<div />'))));
    p_o.addClass('pointer-events-none');
    if (p_o.prop('tagName') == 'BUTTON') {
        p_o.prop('disabled', true);
    }
}
function gf_removeLoadingTo(p_o) {
    p_o.find('.app-loading-to').remove();
    p_o.removeClass('pointer-events-none');
    if (p_o.prop('tagName') == 'BUTTON') {
        p_o.prop('disabled', false);
    }
}
function gf_replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}
function gf_utf8_to_b64(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
}
function gf_b64_to_utf8(str) {
    return decodeURIComponent(escape(window.atob(str)));
}
function gf_isYoutubeVideo(url) {
    var v = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
    return (url.match(v)) ? RegExp.$1 : false;
}
function gf_requestFullscreen(p_o) {
    if (p_o) {
        p_o.addClass('app-request-fullscreen');
    } else {
        var l_doc = document.documentElement;
        if (l_doc.requestFullscreen) {
            l_doc.requestFullscreen();
        }
        else if (l_doc.msRequestFullscreen) {
            l_doc = document.body; //overwrite the element (for IE)
            l_doc.msRequestFullscreen();
        }
        else if (l_doc.mozRequestFullScreen) {
            l_doc.mozRequestFullScreen();
        }
        else if (l_doc.webkitRequestFullScreen) {
            l_doc.webkitRequestFullScreen();
        }
    }
}
function gf_exitFullscreen(p_o) {
    if (p_o) {
        p_o.removeClass('app-request-fullscreen');
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
        else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
        else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        }
        else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    }
}
function gf_convertToHHMM(p_number) {
    var l_decimalTime = parseFloat(p_number);
    l_decimalTime = l_decimalTime * 60 * 60;
    var l_hours = Math.floor((l_decimalTime / (60 * 60)));
    l_decimalTime = l_decimalTime - (l_hours * 60 * 60);
    var l_minutes = Math.floor((l_decimalTime / 60));
    l_decimalTime = l_decimalTime - (l_minutes * 60);
    var l_seconds = Math.round(l_decimalTime);
    if (l_hours < 10) {
        l_hours = "0" + l_hours;
    }
    if (l_minutes < 10) {
        l_minutes = "0" + l_minutes;
    }
    if (l_seconds < 10) {
        l_seconds = "0" + l_seconds;
    }
    return l_hours + ":" + l_minutes;
}
function gf_triggerKeepActive() {
    $.ajax({
        url: '/Account/KeepActive',
        type: 'post',
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8"
    });
}
function gf_isImage(p_name) {
    return /(\.jpg|\.jpeg|\.png|\.bmp|\.gif|\.svg)$/i.test(p_name)
}
function gf_getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
function gf_showLoading() {
    $('#app-loading').show();
}
function gf_hideLoading() {
    $('#app-loading').hide();
}