﻿(function () {
    $.material.init({
        "inputElements": "#_layout_changepassword_form input.form-control, #_layout_changepassword_form textarea.form-control, #_layout_changepassword_form select.form-control"
    });
    $("#_layout_changepassword_form").submit(function (evt) {
        evt.preventDefault();
    });
    $("#_layout_changepassword_old_password, #_layout_changepassword_new_password, #_layout_changepassword_confirm_new_password").data({
        "PresaveRequired": true,
        "PresaveJson": '{"maxLength":64}'
    });
}());