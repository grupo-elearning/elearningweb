﻿"use strict";
//DataTable
if (typeof $.fn.dataTable != 'undefined') {
    $.extend(true, $.fn.dataTable.defaults, {
        autoWidth: false,
        pagingType: 'full_numbers',
        bProcessing: true,
        aLengthMenu: [[5, 10, 25, 50, 75, 100], [5, 10, 25, 50, 75, 100]],
        iDisplayLength: 5,
        bSortMulti: false,
        iDisplayStart: 0,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando del _START_ al _END_ de _TOTAL_",
            "sInfoEmpty": "Mostrando del 0 al 0 de 0",
            "sInfoFiltered": "(filtrado de _MAX_)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
}
//Fileuploader
if (typeof $.fn.fileuploader != 'undefined') {
    $.fn.fileuploader.defaults.extensions = [];//['jpg', 'jpeg', 'png', 'audio/mp3', 'text/plain'];
    $.fn.fileuploader.defaults.addMore = true;
    $.fn.fileuploader.defaults.enableApi = true;
    $.fn.fileuploader.defaults.limit = null;
    $.fn.fileuploader.defaults.maxSize = 20;// file's maximal size in MB {null, Number} also with the appended files if null - has no limits example: 2
    $.fn.fileuploader.defaults.fileMaxSize = null;// each file's maximal size in MB {null, Number} if null - has no limits example: 2
    $.fn.fileuploader.defaults.dialogs = {
        alert: function (text) {
            $.jGrowl(text, {
                header: 'Error',
                type: 'error'
            });
        },
        confirm: function (text, callback) {
            //$.daAlert({
            //    ajax: null,
            //    content: text,
            //    maximizable: false,
            //    title: 'Confirmación',
            //    buttons: [{
            //        type: 'remove',
            //        fn: function (p) {
            //            p.fn();
            //            return true;
            //        },
            //        parameters: { 'fn': callback }
            //    }, {
            //        type: 'cancel',
            //        fn: function (p) { return true; },
            //        parameters: {}
            //    }]
            //});
        }
    };
    $.fn.fileuploader.defaults.thumbnails.removeConfirmation = true;// show a confirmation dialog by removing a file? {Boolean} it will not be shown in upload mode by canceling an upload
    $.fn.fileuploader.defaults.thumbnails.itemPrepend = true;// insert the thumbnail's item at the beginning of the list? {Boolean}
    $.fn.fileuploader.defaults.thumbnails.canvasImage = { width: null, height: null };
    $.fn.fileuploader.defaults.captions = {
        button: function (options) { return 'Seleccionar ' + (options.limit == 1 ? '' : ''); },
        feedback: function (options) { return 'Seleccionar ' + (options.limit == 1 ? 'archivo' : 'archivos') + ' para cargar'; },
        feedback2: function (options) { return options.length + ' ' + (options.length > 1 ? ' archivos fueron seleccionados' : ' archivo fue seleccionado'); },
        confirm: 'Confirmar',
        cancel: 'Cancelar',
        name: 'Nombre',
        type: 'Tipo',
        size: 'Tamaño',
        dimensions: 'Dimensiones',
        duration: 'Duración',
        crop: 'Cultivo',
        rotate: 'Girar',
        download: 'Descargar',
        remove: 'Eliminar',
        drop: 'Suelta los archivos aquí para subir',
        paste: '<div class="fileuploader-pending-loader"><div class="left-half" style="animation-duration: ${ms}s"></div><div class="spinner" style="animation-duration: ${ms}s"></div><div class="right-half" style="animation-duration: ${ms}s"></div></div> Pegando un archivo, haga clic aquí para cancelar.',
        removeConfirmation: '¿Seguro que quieres eliminar este archivo?',
        errors: {
            filesLimit: 'Solamente ${limit} archivos se pueden cargar.',
            filesType: 'Solamente archivos de tipo: ${extensions}, se pueden cargar.',
            fileSize: '${name} ¡Es demasiado grande! Seleccione un archivo hasta ${fileMaxSize}MB.',
            filesSizeAll: '¡Los archivos seleccionados son demasiados grandes! Seleccione archivos hasta ${maxSize} MB.',
            fileName: 'El archivo con el nombre ${name} ya está seleccionado.',
            folderUpload: 'No tienes permitido subir carpetas.'
        }
    };
}
//JGrowl
if (typeof $.jGrowl != 'undefined') {
    $.jGrowl.defaults.closerTemplate = '<div>[ cerrar todo ]</div>';
    $.jGrowl.defaults.sticky = false;
    $.jGrowl.defaults.life = 2250;
    $.jGrowl.defaults.sound = true;
    $.jGrowl.defaults.type = 'default';
    $.jGrowl.defaults.soundPath = document.location.origin + "/assets/media/jGrowl_default.mp3";
}
//Datepicker
if (typeof $.fn.datepicker != 'undefined') {
    $.fn.datepicker.dates['es'] = {
        days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
        daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
        daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá", "Do"],
        months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
        today: "Hoy",
        clear: "Limpiar",
        format: "dd/mm/yyyy",
        titleFormat: "MM yyyy",
        weekStart: 0
    };
    $.fn.datepicker.defaults.autoclose = true;
    $.fn.datepicker.defaults.calendarWeeks = false;
    $.fn.datepicker.defaults.clearBtn = false;
    $.fn.datepicker.defaults.container = "body";
    //$.fn.datepicker.defaults.disableTouchKeyboard = false;
    //$.fn.datepicker.defaults.endDate = "01/01/2019";
    $.fn.datepicker.defaults.format = "dd/mm/yyyy";
    $.fn.datepicker.defaults.immediateUpdates = false;
    $.fn.datepicker.defaults.language = "es";
    $.fn.datepicker.defaults.orientation = "bottom left";
    //$.fn.datepicker.defaults.startDate = "01/01/2019";
    //$.fn.datepicker.defaults.templates = {
    //    leftArrow: '<i class="icofont icofont-curved-left"></i>',
    //    rightArrow: '<i class="icofont icofont-curved-right"></i>'
    //};
    $.fn.datepicker.defaults.todayBtn = false;
    $.fn.datepicker.defaults.todayHighlight = true;
    //$.fn.datepicker.defaults.weekStart = 0;
}
//Selectpicker
if (typeof $.fn.selectpicker != 'undefined') {
    $.fn.selectpicker.Constructor.DEFAULTS.deselectAllText = "Deseleccionar todo";
    $.fn.selectpicker.Constructor.DEFAULTS.doneButtonText = "Cerrar";
    $.fn.selectpicker.Constructor.DEFAULTS.noneResultsText = "No hay resultados coincidentes {0}";
    $.fn.selectpicker.Constructor.DEFAULTS.noneSelectedText = "Nada seleccionado";
    $.fn.selectpicker.Constructor.DEFAULTS.selectAllText = "Seleccionar todo";
    $.fn.selectpicker.Constructor.DEFAULTS.liveSearch = true;
    $.fn.selectpicker.Constructor.DEFAULTS.dropupAuto = false;
    $.fn.selectpicker.Constructor.DEFAULTS.mobile = false;//g_isMobile.any();
}