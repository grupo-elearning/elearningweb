﻿"use strict";
var dalba = Object;
(function ($) {
    $.extend({
        /* daAlert */
        // ajax: {url: "", data: null, beforeSend: function(){}, success: function(){}, error: function(){}, complete: function(){}}
        // buttons: [{type: "custom", fn: function(p){return true;}, parameters: {}, text: "", icon: "", className: ""}]
        daAlert: function (p_Settings) {
            var go, g = new Object();
            /* default settings */
            g.defaults = {
                content: {
                    type: "text", // text, ajax
                    value: "" // "", ajax: {}
                },
                buttons: [],
                closableByDimmer: false,
                focus: 0,
                maximizable: true,
                maxWidth: "500px",
                modal: true,
                movable: true,
                resizable: false,
                title: "title",
                transition: "zoom", // pulse, slide,
                type: "primary",
                startMaximized: false,
                onclose: function () {
                },
                onshow: function () {
                }
            };
            g.buttons = [];
            g.name = null;
            g.close = function () {
                alertify[g.name]().close();
            };
            g.getAlertify = function () {
                return alertify[g.name]();
            };

            g.defaults = $.extend(true, g.defaults, p_Settings);
            go = g.defaults;
            //build buttons
            (function () {
                for (var i in go.buttons) {
                    //button cancel
                    if (go.buttons[i].type == 'cancel') {
                        g.buttons.push({
                            text: 'Cancelar',
                            key: 27,
                            //invokeOnClose: true,
                            className: 'btn btn-danger',
                            scope: 'auxiliary',
                            element: undefined,
                            type: go.buttons[i].type,
                            fn: go.buttons[i].fn,
                            parameters: go.buttons[i].parameters
                        });
                    }
                    //button add
                    if (go.buttons[i].type == 'add') {
                        g.buttons.push({
                            text: '<i class="fa fa-plus"></i> Agregar',
                            //key: 27,
                            //invokeOnClose: true,
                            className: 'btn btn-raised btn-success',
                            scope: 'auxiliary',
                            element: undefined,
                            type: go.buttons[i].type,
                            fn: go.buttons[i].fn,
                            parameters: go.buttons[i].parameters
                        });
                    }
                    //button remove
                    if (go.buttons[i].type == 'remove') {
                        g.buttons.push({
                            text: '<i class="fa fa-trash"></i> Eliminar',
                            //key: 27,
                            //invokeOnClose: true,
                            className: 'btn btn-raised btn-danger',
                            scope: 'auxiliary',
                            element: undefined,
                            type: go.buttons[i].type,
                            fn: go.buttons[i].fn,
                            parameters: go.buttons[i].parameters
                        });
                    }
                    //button accept
                    if (go.buttons[i].type == 'accept') {
                        g.buttons.push({
                            text: '<i class="fa fa-check"></i> Aceptar',
                            //key: 27,
                            //invokeOnClose: true,
                            className: 'btn btn-raised btn-info',
                            scope: 'auxiliary',
                            element: undefined,
                            type: go.buttons[i].type,
                            fn: go.buttons[i].fn,
                            parameters: go.buttons[i].parameters
                        });
                    }
                    //button warning
                    if (go.buttons[i].type == 'warning') {
                        g.buttons.push({
                            text: '<i class="fa fa-warning"></i> Advertencia',
                            //key: 27,
                            //invokeOnClose: true,
                            className: 'btn btn-raised btn-warning',
                            scope: 'auxiliary',
                            element: undefined,
                            type: go.buttons[i].type,
                            fn: go.buttons[i].fn,
                            parameters: go.buttons[i].parameters
                        });
                    }
                    //button save
                    if (go.buttons[i].type == 'save') {
                        g.buttons.push({
                            text: '<i class="fa fa-save"></i> Guardar',
                            //key: 27,
                            //invokeOnClose: true,
                            className: 'btn btn-raised btn-info',
                            scope: 'auxiliary',
                            element: undefined,
                            type: go.buttons[i].type,
                            fn: go.buttons[i].fn,
                            parameters: go.buttons[i].parameters
                        });
                    }
                    //button custom
                    if (go.buttons[i].type == 'custom') {
                        g.buttons.push({
                            text: '<i class="' + go.buttons[i].icon + '"></i> ' + go.buttons[i].text,
                            //key: 27,
                            //invokeOnClose: true,
                            className: go.buttons[i].className,
                            scope: 'auxiliary',
                            element: undefined,
                            type: go.buttons[i].type,
                            fn: go.buttons[i].fn,
                            parameters: go.buttons[i].parameters
                        });
                    }
                }
            }());
            //build
            (function () {
                //update current alertify
                g.name = new Date().getTime();
                if (!alertify[g.name]) {
                    //define a new dialog
                    alertify.dialog(g.name, function factory() {
                        return {
                            // The dialog startup function
                            // This will be called each time the dialog is invoked
                            // For example: alertify.myDialog( data );
                            main: function (message) {
                                // manipulate parameters and set options
                                this.message = message;
                            },
                            // The dialog setup function
                            // This should return the dialog setup object ( buttons, focus and options overrides ).
                            setup: function () {
                                return {
                                    /* buttons collection */
                                    buttons: g.buttons,
                                    /* default focus */
                                    focus: {
                                        /* the element to receive default focus, has differnt meaning based on value type:
                                         number:     action button index.
                                         string:     querySelector to select from dialog body contents.
                                         function:   when invoked, should return the focus element.
                                         DOMElement: the focus element.
                                         object:     an object that implements .focus() and .select() functions. 
                                         */
                                        element: go.focus,
                                        /* indicates if the element should be selected on focus or not*/
                                        select: false
                                    },
                                    /* dialog options, these override the defaults */
                                    options: {
                                        title: go.title,
                                        modal: go.modal,
                                        basic: false,
                                        frameless: false,
                                        pinned: false,
                                        movable: go.movable,
                                        moveBounded: false,
                                        resizable: go.resizable,
                                        autoReset: true,
                                        closable: true,
                                        closableByDimmer: go.closableByDimmer,
                                        maximizable: go.maximizable,
                                        startMaximized: go.startMaximized,
                                        pinnable: false,
                                        transition: go.transition,
                                        padding: false,
                                        overflow: true,
                                        onshow: go.onshow,
                                        onclose: go.onclose,
                                        //onfocus:...,
                                        //onmove:...,
                                        //onmoved:...,
                                        //onresize:...,
                                        //onresized:...,
                                        //onmaximize:...,
                                        //onmaximized:...,
                                        //onrestore:...,
                                        //onrestored:...,
                                    }
                                };
                            },
                            // This will be called once the dialog DOM has been created, just before its added to the document.
                            // Its invoked only once.
                            build: function () {
                                // Do custom DOM manipulation here, accessible via this.elements

                                // this.elements.root           ==> Root div 
                                // this.elements.dimmer         ==> Modal dimmer div
                                // this.elements.modal          ==> Modal div (dialog wrapper)
                                // this.elements.dialog         ==> Dialog div
                                // this.elements.reset          ==> Array containing the tab reset anchor links
                                // this.elements.reset[0]       ==> First reset element (button).
                                // this.elements.reset[1]       ==> Second reset element (button).
                                // this.elements.header         ==> Dialog header div
                                // this.elements.body           ==> Dialog body div
                                // this.elements.content        ==> Dialog body content div
                                // this.elements.footer         ==> Dialog footer div
                                // this.elements.resizeHandle   ==> Dialog resize handle div            

                                // Dialog commands (Pin/Maximize/Close)
                                // this.elements.commands           ==> Object containing  dialog command buttons references
                                // this.elements.commands.container ==> Root commands div
                                // this.elements.commands.pin       ==> Pin command button
                                // this.elements.commands.maximize  ==> Maximize command button
                                // this.elements.commands.close     ==> Close command button

                                // Dialog action buttons (Ok, cancel ... etc)
                                // this.elements.buttons                ==>  Object containing  dialog action buttons references
                                // this.elements.buttons.primary        ==>  Primary buttons div
                                // this.elements.buttons.auxiliary      ==>  Auxiliary buttons div

                                // Each created button will be saved with the button definition inside buttons collection
                                // this.__internal.buttons[x].element

                                //add class to this.elements.commands
                                $(this.elements.commands.close).addClass("dalba-alert");
                                $(this.elements.commands.container).addClass("dalba-alert");
                                $(this.elements.commands.maximize).addClass("dalba-alert");
                                $(this.elements.commands.pin).addClass("dalba-alert");
                                //add class to this.elements.header
                                $(this.elements.header).addClass("dalba-alert " + go.type);
                                if (go.type == "primary") {
                                    $(this.elements.header).css({
                                        "background-color": $(".btn-circle.btn-circle-raised.btn-circle-primary").css("background-color"),
                                        "color": "#ffffff"
                                    });
                                }
                                //add class to this.elements.dialog
                                $(this.elements.dialog).addClass("dalba-alert " + go.type);
                                if (go.type == "primary") {
                                    $(this.elements.dialog).css({
                                        "border-bottom-color": $(".btn-circle.btn-circle-raised.btn-circle-primary").css("background-color")
                                    });
                                }
                                //add class to this.elements.content
                                $(this.elements.content).addClass("dalba-alert");
                                //add class to this.elements.footer
                                $(this.elements.footer).addClass("dalba-alert");
                                //add class to this.elements.modal
                                $(this.elements.modal).addClass("dalba-alert");
                                //set maxWidth to this.elements.dialog
                                $(this.elements.dialog).css({ "max-width": go.maxWidth });
                                //hide footer
                                if (go.buttons.length == 0) {
                                    $(this.elements.footer).hide();
                                }
                            },
                            // This will be called each time the dialog is shown
                            prepare: function () {
                                this.setContent(this.message);
                            },
                            // This will be called each time an action button is clicked.
                            callback: function (closeEvent) {
                                //The closeEvent has the following properties
                                //
                                // index: The index of the button triggering the event.
                                // button: The button definition object.
                                // cancel: When set true, prevent the dialog from closing.
                                closeEvent.cancel = !closeEvent.button.fn(closeEvent.button.parameters, $(closeEvent.button.element));
                            },
                            // To make use of AlertifyJS settings API, group your custom settings into a settings object.
                            settings: {
                                myProp: 'value'
                            },
                            // AlertifyJS will invoke this each time a settings value gets updated.
                            settingUpdated: function (key, oldValue, newValue) {
                                // Use this to respond to specific setting updates.
                                switch (key) {
                                    case 'myProp':
                                        // Do something when 'myProp' changes
                                        break;
                                }
                            },
                            // listen to internal dialog events.
                            hooks: {
                                // triggered when the dialog is shown, this is seperate from user defined onshow
                                onshow: function () {
                                },
                                // triggered when the dialog is closed, this is seperate from user defined onclose
                                onclose: function () {
                                    setTimeout(function () {
                                        alertify[g.name]().destroy();
                                        alertify[g.name] = null;
                                    }, 500);
                                },
                                // triggered when a dialog option gets updated.
                                // IMPORTANT: This will not be triggered for dialog custom settings updates ( use settingUpdated instead).
                                onupdate: function () {
                                }
                            }
                        }
                    });
                }
            }());
            //launch
            (function () {
                //launch it.
                alertify[g.name]('');
                //set content
                if (go.content.type == "ajax") {
                    $.ajax({
                        url: go.content.value.url,
                        data: JSON.stringify(go.content.value.data ? go.content.value.data : {}),
                        contentType: 'application/json; charset=utf-8',
                        type: go.content.value.type ? go.content.value.type : 'post',
                        async: false,
                        dataType: 'html',
                        beforeSend: function () {
                            alertify[g.name]().setContent('<div class="ajs-loading dalba-alert">Cargando...</div>');
                            if (typeof go.content.value.beforeSend != 'undefined') {
                                go.content.value.beforeSend();
                            }
                        },
                        success: function (r) {
                            $(alertify[g.name]().elements.body.lastChild).html(r);
                            if (typeof go.content.value.success != 'undefined') {
                                go.content.value.success();
                            }
                        },
                        error: function (xhr, status) {
                            alertify[g.name]().setContent('Error : ' + xhr.status + ', ' + xhr.statusText);
                            if (typeof go.content.value.error != 'undefined') {
                                go.content.value.error();
                            }
                        },
                        complete: function (xhr, status) {
                            if (typeof go.content.value.complete != 'undefined') {
                                go.content.value.complete();
                            }
                        }
                    });
                    return;
                } else if (go.content.type == "text") {
                    alertify[g.name]().setContent(go.content.value);
                    return;
                }
            }());
            return g;
        }
    });
    $.fn.extend({
        daDataTable: function (p_Settings) {
            var go, g = new Object();
            /* default settings */
            g.defaults = {
                DataTable: null,
                search: {
                    "btn": { "column": 0, "active": true, "icon": "fa fa-filter" },
                    "noSearch": [],
                    "position": "foot"//foot|head
                },
                visibilityDropdown: {
                    "content": null,
                    "type": "info",
                    "responsive": false,
                    "isMobile": false,
                    "noColumns": []
                },
                imgProcessing: "",
                theme: "shaded_circle",
                scrollbar: true,
                showGlobalFilter: false,
                fnDrawCallback: function () {
                }
            };
            g.$ = $(this);
            g.$content = $("<div />");
            g.$btnSearch = $("<a />");
            g.$processing = null;
            g.$globalFilter = null;
            g.$dropdown = $("<div />");
            g.$selectLength = null;
            g.$paginate = null;
            g.$header = null;
            g.$searchRow = null;
            g.$footer = null;
            g.Datatable = null;
            g.id = g.$.attr("id");
            g.isDataTable = g.$.hasClass('dalba-datatable-table');
            g.columns = null;

            g.defaults = $.extend(true, g.defaults, p_Settings);
            go = g.defaults;

            //build
            (function () {
                g.$header = g.$.find("thead");
                g.$footer = g.$.find("tfoot");
                if (!g.isDataTable) {
                    g.$content.addClass("dalba-datatable");
                    g.$.wrap(g.$content);
                    g.$.addClass('dalba-datatable-table');
                    if (go.search != null) {
                        if (go.search.position == "head") {
                            g.$searchRow = $("<tr />");
                            g.$header
                                .find("th")
                                .each(function (idx) {
                                    g.$searchRow.append("<th />");
                                });
                            g.$header.append(g.$searchRow);
                        } else if (go.search.position == "foot") {
                            if (g.$footer.length == 0) {
                                g.$footer = $("<tfoot />");
                                g.$searchRow = $("<tr />");
                                g.$header
                                    .find("th")
                                    .each(function (idx) {
                                        g.$searchRow.append("<th />");
                                    });
                                g.$footer
                                    .append(g.$searchRow)
                                    .insertAfter(g.$header);
                            } else {
                                g.$searchRow = g.$footer.find("tr:first-child");
                            }
                            g.$footer.css({ "display": "table-header-group" });
                        }
                        g.$searchRow.addClass("dalba-datatable search-row");
                    }
                } else {
                    g.$searchRow = g.$.find("tr.dalba-datatable.search-row");
                }
            }());
            //build search
            (function () {
                var l_o = go.search;
                if (l_o != null) {
                    if (!g.isDataTable) {
                        //build search by column
                        g.$searchRow
                            .find("th")
                            .each(function (index) {
                                if (l_o.noSearch != null) {
                                    if (l_o.noSearch.indexOf(index) == -1) {
                                        $(this).html('<input type="text" class="form-control" data-dalba="search" />');
                                    }
                                } else {
                                    $(this).html('<input type="text" class="form-control" data-dalba="search" />');
                                }
                            });
                        //build btn
                        if (l_o.btn != null) {
                            if (!l_o.btn.active) {
                                g.$searchRow.hide();
                            }
                            var l_thead = g.$header.find("tr:first th:nth-child(" + (parseInt(l_o.btn.column) + 1) + ")");
                            var l_tsearch = g.$searchRow.find("th:nth-child(" + (parseInt(l_o.btn.column) + 1) + ")");
                            g.$btnSearch
                                .addClass("btn-circle btn-circle-xs btn-circle-raised btn-circle-primary dalba-datatable btn-search")
                                .css({ "cursor": "pointer" })
                                .html("<i class='" + l_o.btn.icon + " text-white'></i>");
                            l_tsearch
                                .css({ "text-align": "center", "font-size": ".85em" })
                                .html(l_thead.html());
                            l_thead
                                .empty()
                                .append(g.$btnSearch)
                                .css({ "text-align": "center" });
                            g.$btnSearch.off("click");
                            g.$btnSearch.on("click", function (e) {
                                //toggle
                                if (g.$searchRow.is(":visible")) {
                                    g.$searchRow.hide();
                                } else {
                                    g.$searchRow.show();
                                }
                            });
                        }
                    } else {
                        g.$searchRow
                            .find("input")
                            .val("");
                    }
                }
            }());
            //initialize Datatable
            (function () {
                if ($.fn.DataTable.isDataTable(g.$)) {
                    g.$.DataTable().destroy();
                }
                go.DataTable.fnDrawCallback = function () {
                    if (g.$paginate == null) {
                        g.$paginate = $("#" + g.id + "_paginate");
                    }
                    (function () {
                        g.$paginate.addClass("dalba-datatable");
                        var l_first = $("#" + g.id + "_first a"),
                            l_previous = $("#" + g.id + "_previous a"),
                            l_next = $("#" + g.id + "_next a"),
                            l_last = $("#" + g.id + "_last a");
                        l_first.attr("title", l_first.text()).html("«");
                        l_previous.attr("title", l_previous.text()).html("«");
                        l_next.attr("title", l_next.text()).html("»");
                        l_last.attr("title", l_last.text()).html("»");
                        if (go.theme.toLowerCase() == "flat_circle") {
                            g.$paginate.find("ul.pagination").addClass("pagination-plain");
                        } else if (go.theme.toLowerCase() == "shaded_circle") {
                            g.$paginate.find("ul.pagination").addClass("pagination-circle");
                        } else if (go.theme.toLowerCase() == "flat_rounded") {
                            g.$paginate.find("ul.pagination").addClass("pagination-round pagination-plain");
                        } else if (go.theme.toLowerCase() == "shaded_rounded") {
                            g.$paginate.find("ul.pagination").addClass("pagination-round");
                        } else if (go.theme.toLowerCase() == "flat_square") {
                            g.$paginate.find("ul.pagination").addClass("pagination-square pagination-plain");
                        } else if (go.theme.toLowerCase() == "shaded_square") {
                            g.$paginate.find("ul.pagination").addClass("pagination-square");
                        } else if (go.theme.toLowerCase() == "devexpress") {
                            g.$paginate.find("ul.pagination").addClass("devexpress");
                            l_first.html("");
                            l_previous.html("");
                            l_next.html("");
                            l_last.html("");
                        }
                    }());
                    go.fnDrawCallback();
                }
                g.Datatable = g.$.DataTable(go.DataTable);
            }());
            //build spin processing
            (function () {
                g.$processing = $('#' + g.id + '_processing');
                g.$processing
                    .addClass("dalba-datatable")
                    .html('<div><img src="' + go.imgProcessing + '" /> <span>' + $.fn.dataTable.defaults.language.sProcessing + '</span></div>');
            }());
            //initialize search by column
            (function () {
                var lo = go.search;
                dalba.Timeout = false;
                if (lo != null) {
                    g.Datatable.columns().every(function () {
                        var my_that = this;
                        $(this.header()).closest("thead").off('keyup change', 'input[data-dalba=search]');
                        $(this.footer()).off('keyup change', 'input[data-dalba=search]');
                        if (lo.position == "head") {
                            $(this.header()).closest("thead").on('keyup change', 'input[data-dalba=search]', function () {
                                if (my_that.search() != this.value) {
                                    my_that
                                        .search(this.value)
                                        .draw();
                                }
                            });
                        } else if (lo.position == "foot") {
                            $(this.footer()).on('keyup change', 'input[data-dalba=search]', function () {
                                if (my_that.search() != this.value) {
                                    if (dalba.Timeout) {
                                        clearTimeout(dalba.Timeout);
                                    }
                                    dalba.That = my_that;
                                    dalba.This = this;
                                    dalba.Timeout = setTimeout(function () {
                                        dalba.That
                                            .search(dalba.This.value)
                                            .draw();
                                    }, 500);
                                }
                            });
                        }
                    });
                }
            }());
            //update global filter
            (function () {
                g.$globalFilter = $("#" + g.id + "_filter");
                if (!go.showGlobalFilter) {
                    g.$globalFilter
                        .css("display", "none")
                        .find("label").remove();
                }
            }());
            //initialize scrollbar
            (function () {
                var l_ct = g.$.closest("div");
                if (go.scrollbar) {
                    l_ct.css({ "min-height": "120px" });
                    if (!l_ct.hasClass("scrollbar-rail")) {
                        l_ct
                            .addClass("scrollbar-rail")
                            .scrollbar();
                    }
                } else {
                    l_ct.css({ "padding": "0" });
                }
            }());
            //build visibility dropdown
            //(function () {
            //    var l_vo = go.visibilityDropdown;
            //    if (l_vo != null) {
            //        var l_menu = $('<div />'), l_so = go.search, l_type = l_vo.type != null ? l_vo.type : 'default';
            //        g.$dropdown.addClass('btn-group dropdown-split-' + l_type + ' dalba-datatable-visibility-dropdown');
            //        g.$dropdown.append('<button type="button" class="btn btn-' + l_type + '">Columnas</button>');
            //        g.$dropdown.append('<button type="button" class="btn btn-' + l_type + ' dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="sr-only">Toggle primary</span></button>');
            //        l_menu.addClass('dropdown-menu dropdown-menu-right');
            //        g.columns = g.Datatable.context[0].aoColumns;
            //        for (var i in g.columns) {
            //            if ($.inArray(i, l_vo.noColumns) == -1) {
            //                if (l_so != null && l_so.btn != null && l_so.btn.column != null) {
            //                    if (i != l_so.btn.column) {
            //                        if (g.columns[i].bVisible) {
            //                            l_menu.append('<a class="dropdown-item waves-effect waves-light" href="javascript:void(0)" data-column="' + i + '">' + g.columns[i].sTitle + '</a>');
            //                        }
            //                        else {
            //                            l_menu.append('<a class="dropdown-item waves-effect waves-light hidden" href="javascript:void(0)" data-column="' + i + '">' + g.columns[i].sTitle + '</a>');
            //                        }
            //                    }
            //                } else {
            //                    if (g.columns[i].bVisible) {
            //                        l_menu.append('<a class="dropdown-item waves-effect waves-light" href="javascript:void(0)" data-column="' + i + '">' + g.columns[i].sTitle + '</a>');
            //                    }
            //                    else {
            //                        l_menu.append('<a class="dropdown-item waves-effect waves-light hidden" href="javascript:void(0)" data-column="' + i + '">' + g.columns[i].sTitle + '</a>');
            //                    }
            //                }
            //            }
            //        }
            //        g.$dropdown.append(l_menu);
            //        //events
            //        g.$dropdown.off("click", ".dropdown-menu > a");
            //        g.$dropdown.on("click", ".dropdown-menu > a", function (evt) {
            //            evt.stopPropagation();
            //            evt.preventDefault();
            //            // Get the column API object
            //            var l_column = g.Datatable.column($(this).attr('data-column'));
            //            // Toggle the visibility
            //            if (l_column.visible()) {
            //                $(this).addClass('hidden');
            //                l_column.visible(false);
            //            } else {
            //                $(this).removeClass('hidden');
            //                l_column.visible(true);
            //            }
            //            // If no records, update the "No records" display element
            //            if (g.Datatable.page.info().recordsDisplay == 0) {
            //                g.Datatable.draw();
            //            }
            //        });
            //        //
            //        if (l_vo.content) {
            //            if (l_vo.responsive) {
            //                if (l_vo.isMobile) {
            //                    //l_vo.content.empty().append(g.$dropdown);
            //                }
            //            } else {
            //                l_vo.content.empty().append(g.$dropdown);
            //            }
            //        }
            //    }
            //}());
            //set dalba css
            (function () {
                g.$selectLength = $("#" + g.id + "_length");
                g.$.css({ "width": "100%" });
                g.$selectLength.closest("div").addClass("col-xs-6 col-6");
                g.$globalFilter.closest("div").addClass("col-xs-6 col-6");
            }());
            return g;
        },
        daUploadDirectory: function (p_Settings) {
            var go, g = new Object();
            /* default settings */
            g.defaults = {
                className: 'form-control',
                dalbaPath: '',
                openRoot: true,
                onchange: function (event) {
                },
                select_node: function (event, selected) {
                }
            };
            g.$ = $(this);
            g.$inputFile = $('<input type="file" webkitdirectory mozdirectory />');
            g.$inputInfo = $('<input type="text" readonly />');
            g.$jstree = $('<div />');
            g.data = null;
            g.files = null;
            g.getFiles = function () {
                return g.files;
            }
            g.getData = function () {
                return g.data;
            }

            g.defaults = $.extend(true, g.defaults, p_Settings);
            go = g.defaults;

            //build
            (function () {
                g.$inputFile
                    .attr('id', g.$.attr('id') + '_file')
                    .css('display', 'none');
                g.$inputInfo
                    .attr('id', g.$.attr('id') + '_info')
                    .addClass('dalba-uploaddirectory-info ' + go.className);
                g.$
                    .addClass('dalba-uploaddirectory')
                    .append(g.$inputInfo, g.$inputFile, g.$jstree);
            }());
            //initialize
            (function () {
                g.$inputFile.change(function (evt) {
                    var l_root, l_tree = {}, l_data = [];
                    //save files
                    g.files = evt.target.files;
                    //destroy jtree
                    g.$jstree.jstree('destroy');
                    //get tree
                    for (var i = 0; i < g.files.length; i++) {
                        var l_folders = g.files[i].webkitRelativePath.split('/'), l_node = l_tree;
                        for (var j = 0; j < l_folders.length - 1; j++) {
                            if (!l_node[l_folders[j]]) {
                                l_node[l_folders[j]] = {};
                            }
                            l_node = l_node[l_folders[j]];
                            if (j == l_folders.length - 2) {
                                if (!l_node['ZGFsYmEuZGF1cGxvYWRkaXJlY3RvcnkuZmlsZQ==']) {
                                    l_node['ZGFsYmEuZGF1cGxvYWRkaXJlY3RvcnkuZmlsZQ=='] = [];
                                }
                                l_node['ZGFsYmEuZGF1cGxvYWRkaXJlY3RvcnkuZmlsZQ=='].push({
                                    text: l_folders[j + 1],
                                    icon: go.dalbaPath + '/img/file.png',
                                    data: {
                                        path: g.files[i].webkitRelativePath
                                    }
                                });
                            }
                        }
                    }
                    //get data
                    l_root = Object.keys(l_tree)[0];
                    if (l_root) {
                        fn_get_child(l_root, l_tree[l_root], l_data);
                        //open the first node
                        l_data[0].state = { opened: go.openRoot };
                        //build jstree
                        g.$jstree.jstree({
                            'core': {
                                'data': l_data
                            }
                        }).on('select_node.jstree', go.select_node);
                        g.$inputInfo.val(l_root);
                    } else {
                        g.$inputInfo.val('');
                    }
                    //save data
                    g.data = l_data;
                    //call onchange
                    go.onchange(evt);
                });
                g.$inputInfo.click(function (evt) {
                    g.$inputFile.trigger('click');
                });
                function fn_get_child(p_i, p_node, p_children) {
                    if (p_i == 'ZGFsYmEuZGF1cGxvYWRkaXJlY3RvcnkuZmlsZQ==') {
                        for (var i in p_node) {
                            p_children.push(p_node[i]);
                        }
                    } else {
                        var l_object = {
                            text: p_i,
                            icon: go.dalbaPath + '/img/folder.png',
                            children: []
                        };
                        for (var i in p_node) {
                            fn_get_child(i, p_node[i], l_object.children);
                        }
                        p_children.push(l_object);
                    }
                }
            }());
            //***
            //var input = document.createElement("input");
            //if (typeof input.webkitdirectory !== "boolean") {}
            return g;
        },
        daRanking: function (p_Settings) {
            var go, g = new Object();
            /* default settings */
            g.defaults = {
                dalbaPath: '',
                items: [],//[{text: '', emoji: 'url', data: {}}]
                score: null,
                readonly: false,
                select: {
                    type: 'stack',//[single|stack]
                    animation: ''//[a|b|c|d]
                },
                onselect: function (i) {
                }
            };
            g.$ = $(this);
            g.$container = $('<div />');
            g.options = [];
            g.emoji = {};
            g.selected = null;
            g.getSelected = function () {
                return g.selected ? $(g.selected).data('item').data : null;
            }

            g.defaults = $.extend(true, g.defaults, p_Settings);
            go = g.defaults;

            //build options
            (function () {
                //calculate width
                var l_width = 100 / (go.items.length + 1);
                for (var i in go.items) {
                    var l_item = {
                        $: $('<div />').addClass('dalba-ranking-item').css({ 'width': l_width + '%' }).data('item', go.items[i]),
                        $image: $('<img />').addClass('dalba-ranking-item-image').attr('src', go.dalbaPath + '/img/star_unselected.svg'),
                        $text: $('<div />').addClass('dalba-ranking-item-text').text(go.items[i].text)
                    };
                    l_item.$.append(l_item.$image, l_item.$text);
                    g.options.push(l_item);
                }
                //emoji
                g.emoji.$ = $('<div />').addClass('dalba-ranking-emoji').css({ 'width': l_width + '%' });
                g.emoji.$image = $('<img />').addClass('dalba-ranking-emoji-image').attr('src', '');
                g.emoji.$.append(g.emoji.$image);
                //draw options
                g.$container.addClass('dalba-ranking-container');
                for (var i in g.options) {
                    g.$container.append(g.options[i].$);
                }
                //draw emoji
                g.$container.append(g.emoji.$);
                //
                g.$.append(g.$container);
            }());
            //events
            (function () {
                for (var i in g.options) {
                    if (go.select.type == 'single') {
                        g.options[i].$.on('mouseenter', function () {
                            $(this).find('img.dalba-ranking-item-image').attr('src', go.dalbaPath + '/img/star_selected.svg');
                        })
                        g.options[i].$.on('mouseleave', function () {
                            if (!g.selected) {
                                for (var j in g.options) {
                                    g.options[j].$image.attr('src', go.dalbaPath + '/img/star_unselected.svg');
                                }
                            } else {
                                for (var j in g.options) {
                                    if (g.selected != g.options[j].$[0]) {
                                        g.options[j].$image.attr('src', go.dalbaPath + '/img/star_unselected.svg');
                                    }
                                }
                            }
                        });
                    }
                    if (go.select.type == 'stack') {
                        g.options[i].$.on('mouseenter', function () {
                            var l_before = true;
                            for (var j in g.options) {
                                if (l_before) {
                                    g.options[j].$image.attr('src', go.dalbaPath + '/img/star_selected.svg');
                                } else {
                                    g.options[j].$image.attr('src', go.dalbaPath + '/img/star_unselected.svg');
                                }
                                if (this == g.options[j].$[0]) {
                                    l_before = false;
                                }
                            }
                        });
                        g.options[i].$.on('mouseleave', function () {
                            if (!g.selected) {
                                for (var j in g.options) {
                                    g.options[j].$image.attr('src', go.dalbaPath + '/img/star_unselected.svg');
                                }
                            } else {
                                var l_before = true;
                                for (var j in g.options) {
                                    if (l_before) {
                                        g.options[j].$image.attr('src', go.dalbaPath + '/img/star_selected.svg');
                                    } else {
                                        g.options[j].$image.attr('src', go.dalbaPath + '/img/star_unselected.svg');
                                    }
                                    if (g.selected == g.options[j].$[0]) {
                                        l_before = false;
                                    }
                                }
                            }
                        });
                    }
                    g.options[i].$.on('click', function (evt) {
                        evt.preventDefault();
                        g.selected = this;
                        if (go.select.type == 'single') {
                            for (var j in g.options) {
                                if (g.selected != g.options[j].$[0]) {
                                    g.options[j].$image.attr('src', go.dalbaPath + '/img/star_unselected.svg');
                                }
                            }
                        }
                        g.emoji.$image.attr('src', $(this).data('item').emoji);
                        if (go.select.animation == 'a' || go.select.animation == 'd') {
                            var l_image = $(this).find('img.dalba-ranking-item-image').css('animation-duration', '1s');
                            l_image.css('animation-name', 'dalba-ranking-animation-' + go.select.animation);
                            setTimeout(function () {
                                l_image.css('animation-name', '');
                            }, 1100);
                        } else if (go.select.animation == 'b' || go.select.animation == 'c') {
                            var l_image = $(this).find('img.dalba-ranking-item-image'), l_animation = $('<img />')
                                .attr('src', go.dalbaPath + '/img/star_selected.svg')
                                .css({
                                    'position': 'absolute',
                                    'top': l_image.position().top + 'px',
                                    'left': l_image.position().left + 'px',
                                    'width': l_image.width() + 'px',
                                    'animation-duration': '1s',
                                    'animation-name': 'dalba-ranking-animation-' + go.select.animation
                                });
                            $(this).append(l_animation);
                            setTimeout(function () {
                                l_animation.remove();
                            }, 1100);
                        }
                        go.onselect($(this).data('item'));
                    });
                }
            }());
            //set score, set readonly
            (function () {
                if (go.score) {
                    g.options[go.score - 1].$.trigger('click');
                    g.options[go.score - 1].$.trigger('mouseenter');
                }
                if (go.readonly) {
                    for (var i in g.options) {
                        g.options[i].$.off('mouseenter');
                        g.options[i].$.off('mouseleave');
                        g.options[i].$.off('click');
                    }
                }
            }());
            //
            return g;
        }
    });
}(jQuery));