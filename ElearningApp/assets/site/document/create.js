﻿"use strict";
$("document").ready(function () {
    //events
    $("#document_create_btn_clear_model").click(function (evt) {
        evt.preventDefault();
        $("#document_create_model").val('').trigger('change');
    });
    $("#document_create_form").submit(function (evt) {
        evt.preventDefault();
    });
    $("#document_create_save").click(function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        var l_psave = gf_presaveForm($("#document_create_form")), l_formdata = new FormData();
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return;
        }
        l_formdata.append('Description', $("#document_create_description").val().trim());
        l_formdata.append('ShortDescription', $("#document_create_short_description").val().trim());
        l_formdata.append('Type', $("#document_create_type").val());
        l_formdata.append('Order', $("#document_create_order").val());
        l_formdata.append('HasNumber', $("#document_create_has_number").is(":checked") ? "S" : "N");
        l_formdata.append('HasDates', $("#document_create_has_dates").is(":checked") ? "S" : "N");
        l_formdata.append('HasMassive', $("#document_create_has_massive").is(":checked") ? "S" : "N");
        l_formdata.append('HasLogistica', $("#document_create_has_logistica").is(":checked") ? "S" : "N");
        l_formdata.append('Status', $("#document_create_status").is(":checked") ? "A" : "I");
        l_formdata.append('Model.File', gf_isValue($("#document_create_model").val()) ? $("#document_create_model")[0].files[0] : null);
        $.daAlert({
            content: {
                type: "text",
                value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "save",
                    fn: function () {
                        $.ajax({
                            data: l_formdata,
                            url: '/Document/Create',
                            type: 'post',
                            dataType: 'json',
                            contentType: false,
                            processData: false,
                            beforeSend: _beforeSend,
                            success: _success
                        });
                        function _beforeSend() {
                            gf_addLoadingTo($("#document_create_save"));
                            gf_triggerKeepActive();
                        }
                        function _success(r) {
                            if (parseInt(r.status) == 1000) {
                                $.jGrowl(r.response, {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                location.href = '/Document/Index';
                            } else {
                                $.jGrowl(r.response, {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: r.exception
                                });
                                gf_removeLoadingTo($("#document_create_save"));
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: "Confirmación"
        });
    });
    //fn
    //init
    $("#document_create_description, #document_create_short_description, #document_create_type, #document_create_order").data({ 'PresaveRequired': true });
    $("#document_create_description").data({ 'PresaveJson': '{"maxLength":200}' });
    $("#document_create_short_description").data({ 'PresaveJson': '{"maxLength":20}' });
    $("#document_create_order").data({
        "PresaveList": "integer",
        "PresaveJson": '{"min":1}'
    });
    $("#document_create_type").val(null).addClass('selectpicker').selectpicker();
});