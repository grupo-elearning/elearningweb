﻿"use strict";
$("document").ready(function () {
    $("#document_index_table tbody").on('click', 'a[data-action=delete]', function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        var l_datatable = $("#document_index_table").DataTable();
        var l_data = l_datatable.row($(this).closest('tr')).data();
        $.daAlert({
            content: {
                type: 'text',
                value: '<div class="text-center p-05">¿Está seguro que desea eliminar?</div>'
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "remove",
                    fn: function (p, e) {
                        var l_success = false;
                        $.ajax({
                            data: JSON.stringify({ Id: l_data[0] }),
                            url: '/Document/Delete',
                            type: 'post',
                            dataType: 'json',
                            contentType: "application/json; charset=utf-8",
                            success: _success,
                            complete: _complete
                        });
                        function _success(r) {
                            if (r.status == 1000) {
                                $.jGrowl(r.response, {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                l_success = true;
                            } else {
                                $.jGrowl(r.response, {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: r.exception
                                });
                            }
                        }
                        function _complete() {
                            if (l_success) {
                                fn_datatable();
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: 'Confirmación'
        });
    });
    //fn
    function fn_datatable() {
        $("#document_index_table").daDataTable({
            "DataTable": {
                "ajax": {
                    "url": "/Document/GetList",
                    "type": "post"
                },
                "aoColumns": [
                    {
                        "data": 1,
                        "className": "text-left"
                    },
                    {
                        "data": 7,
                        "className": "text-center"
                    },
                    {
                        "data": 2,
                        "className": "text-center"
                    },
                    {
                        "data": 3,
                        "className": "text-center"
                    },
                    {
                        "data": 4,
                        "className": "text-center"
                    },
                    {
                        "data": 5,
                        "className": "text-center"
                    },
                    {
                        "data": null,
                        "className": "text-center",
                        "width": "50px",
                        "mRender": function (o) {
                            if (o[6] == "A") {
                                return "Activo";
                            } else if (o[6] == "I") {
                                return "Inactivo";
                            } else {
                                return '';
                            }
                        }
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center",
                        "width": "50px",
                        "mRender": function (o) {
                            return `<a href="/Document/Edit/` + gf_encode(o[0]) + `" class="btn btn-xs btn-raised btn-warning m-0 cls-1810072234" title="Editar" >
                                    <i class="fa fa-edit m-0"></i>
                                </a>
                                <a href="javascript:void(0)" class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" data-action="delete" title="Eliminar" >
                                    <i class="fa fa-trash m-0"></i>
                                </a>`;
                        }
                    }
                ],
                "order": [[0, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 10
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": { "column": 7 }
            }
        });
    }
    //init
    fn_datatable();
});