﻿"use strict";
$("document").ready(function () {
    (function (p_model) {
        //events
        $("#document_edit_btn_clear_model").click(function (evt) {
            evt.preventDefault();
            $("#document_edit_model").val('').trigger('change');
        });
        $("#document_edit_form").submit(function (evt) {
            evt.preventDefault();
        });
        $("#document_edit_save").click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_psave = gf_presaveForm($("#document_edit_form")), l_formdata = new FormData();
            if (l_psave.hasError) {
                $.jGrowl("Ingrese correctamente los datos del formulario", {
                    header: 'Validación'
                });
                return;
            }
            l_formdata.append('Id', p_model.Id);
            l_formdata.append('Description', $("#document_edit_description").val().trim());
            l_formdata.append('ShortDescription', $("#document_edit_short_description").val().trim());
            l_formdata.append('Type', $("#document_edit_type").val());
            l_formdata.append('Order', $("#document_edit_order").val());
            l_formdata.append('HasNumber', $("#document_edit_has_number").is(":checked") ? "S" : "N");
            l_formdata.append('HasDates', $("#document_edit_has_dates").is(":checked") ? "S" : "N");
            l_formdata.append('HasMassive', $("#document_edit_has_massive").is(":checked") ? "S" : "N");
            l_formdata.append('HasLogistica', $("#document_edit_has_logistica").is(":checked") ? "S" : "N");
            l_formdata.append('Status', $("#document_edit_status").is(":checked") ? "A" : "I");
            l_formdata.append('Model.File', gf_isValue($("#document_edit_model").val()) ? $("#document_edit_model")[0].files[0] : null);
            l_formdata.append('Model.Name', $("#document_edit_model_display").val());
            l_formdata.append('Model.OldName', p_model.Model.Name);
            $.daAlert({
                content: {
                    type: "text",
                    value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "save",
                        fn: function () {
                            $.ajax({
                                data: l_formdata,
                                url: '/Document/Edit',
                                type: 'post',
                                dataType: 'json',
                                contentType: false,
                                processData: false,
                                beforeSend: _beforeSend,
                                success: _success
                            });
                            function _beforeSend() {
                                gf_addLoadingTo($("#document_edit_save"));
                                gf_triggerKeepActive();
                            }
                            function _success(r) {
                                if (parseInt(r.status) == 1000) {
                                    $.jGrowl(r.response, {
                                        header: 'Información',
                                        type: 'primary'
                                    });
                                    location.href = '/Document/Index';
                                } else {
                                    $.jGrowl(r.response, {
                                        header: 'Error',
                                        type: 'danger',
                                        clipboard: r.exception
                                    });
                                    gf_removeLoadingTo($("#document_edit_save"));
                                }
                            }
                            return true;
                        }
                    }
                ],
                maximizable: false,
                title: "Confirmación"
            });
        });
        //fn
        //init
        $("#document_edit_description, #document_edit_short_description, #document_edit_type, #document_edit_order").data({ 'PresaveRequired': true });
        $("#document_edit_description").data({ 'PresaveJson': '{"maxLength":200}' });
        $("#document_edit_short_description").data({ 'PresaveJson': '{"maxLength":20}' });
        $("#document_edit_order").data({
            "PresaveList": "integer",
            "PresaveJson": '{"min":1}'
        });
        $("#document_edit_type").val(p_model.Type).addClass('selectpicker').selectpicker();
    }(gf_getClone(ngon.Model)));
});