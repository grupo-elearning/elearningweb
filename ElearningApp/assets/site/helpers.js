﻿"use strict";
function gf_encode(p_code) {
    return gf_replaceAll(gf_replaceAll(window.btoa(p_code), '[+]', '-'), '[/]', '_');
}
function gf_decode(p_code) {
    return window.atob(gf_replaceAll(gf_replaceAll(p_code, '[-]', '+'), '[_]', '/'));
}
function gf_is_logistica_document(p_tdos, p_documents) {
    var l_return = false;
    for (var i in p_documents) {
        if (p_tdos == p_documents[i].CodiTdos) {
            l_return = true;
            break;
        }
    }
    return l_return;
}
function gf_base64_to_file(p_base64, p_filename, p_mimeType) {
    return (fetch(p_base64)
        .then(function (res) { return res.arrayBuffer(); })
        .then(function (buf) { return new File([buf], p_filename, { type: p_mimeType }); })
    );
}