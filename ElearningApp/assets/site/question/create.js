﻿//"use strict";
$("document").ready(function () {
    var l_container = $("#question_create_container_question");
    $("#question_create_form").submit(function (evt) {
        evt.preventDefault();
    });
    $("#question_create_type").change(function () {
        var l_type = $("#question_create_type option:selected").attr("data-type");
        l_container.empty();
        switch (l_type) {
            case 'radio':
                fn_add_radio();
                break;
            case 'checkbox':
                fn_add_checkbox();
                break;
            case 'boolean':
                fn_add_boolean();
                break;
            case 'completar':
                fn_add_complete();
                break;
        }
    });
    $("#question_create_save").click(function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        var l_psave = gf_presaveForm($("#question_create_form")), l_data;
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return;
        }
        l_data = fn_getData();
        if (l_data == null) {
            return;
        }
        $.daAlert({
            content: {
                type: "text",
                value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "save",
                    fn: function () {
                        $.ajax({
                            data: JSON.stringify(l_data),
                            url: '/Question/Create',
                            type: 'post',
                            dataType: 'json',
                            contentType: "application/json; charset=utf-8",
                            beforeSend: _beforeSend,
                            success: _success
                        });
                        function _beforeSend() {
                            gf_addLoadingTo($("#question_create_save"));
                        }
                        function _success(r) {
                            if (parseInt(r.status) == 1000) {
                                $.jGrowl(r.response, {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                location.href = '/Question/Index';
                            } else {
                                $.jGrowl(r.response, {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: r.exception
                                });
                                gf_removeLoadingTo($("#question_create_save"));
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: "Confirmación"
        });
    });
    $("#question_create_container_question").on("keyup", "textarea[data-input=question-for-complete]", function () {
        var l_a = fn_get_answers($(this).val());
        l_container.find("input[data-input=correct]").tagsinput('removeAll');
        for (var i in l_a) {
            l_container.find("input[data-input=correct]").tagsinput('add', l_a[i]);
        }
    });
    $("#question_create_container_question").on("change", "textarea[data-input=question-for-complete]", function () {
        var l_a = fn_get_answers($(this).val());
        l_container.find("input[data-input=correct]").tagsinput('removeAll');
        for (var i in l_a) {
            l_container.find("input[data-input=correct]").tagsinput('add', l_a[i]);
        }
    });
    $("#question_create_container_question").on("click", "a[data-action=add]", function (evt) {
        evt.preventDefault();
        var l_tbl = $(this).closest("table");
        if ($("#question_create_type option:selected").attr("data-type") == 'radio') {
            l_tbl.find("tbody").append(`<tr>
                                            <td class="text-center" style="width: 50px;">
                                                <div class="radio radio-primary">
                                                    <label class="m-0">
                                                        <input type="radio" name="radiosradio" data-input="correct" />
                                                        <span class="circle"></span>
                                                        <span class="check"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <input class="form-control" data-input="answer" />
                                            </td>
                                            <td class="text-center" style="width: 50px;">
                                                <a href="javascript:void(0)" class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" data-action="delete" title="Eliminar">
                                                    <i class="fa fa-trash m-0"></i>
                                                </a>
                                            </td>
                                        </tr>`);
        }
        if ($("#question_create_type option:selected").attr("data-type") == 'checkbox') {
            l_tbl.find("tbody").append(`<tr>
                                            <td class="text-center" style="width: 50px;">
                                                <div class="checkbox">
                                                    <label class="m-0">
                                                        <input type="checkbox" data-input="correct"><span class="checkbox-material m-0"><span class="check"></span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <input class="form-control" data-input="answer" />
                                            </td>
                                            <td class="text-center" style="width: 50px;">
                                                <a href="javascript:void(0)" class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" data-action="delete" title="Eliminar">
                                                    <i class="fa fa-trash m-0"></i>
                                                </a>
                                            </td>
                                        </tr>`);
        }
    });
    $("#question_create_container_question").on("click", "a[data-action=delete]", function (evt) {
        evt.preventDefault();
        $(this).closest("tr").remove();
    });
    //fn
    function fn_getData() {
        var l_response = {
            "CourseId": $("#question_create_course").val(),
            "TypeId": $("#question_create_type").val(),
            "Status": $("#question_create_status").is(":checked") ? "A" : "I",
            "Question": $("#question_create_question").val().trim(),
            "AnswerId": [],
            "AnswerText": [],
            "AnswerCorrect": [],
            "AnswerOrder": []
        }, l_type = $("#question_create_type option:selected").attr("data-type");
        if (l_type == 'radio') {
            var l_rows = l_container.find("table[data-table=radio] tbody tr"), l_error, l_answer;
            if (l_rows.find("input[name=radiosradio]:checked").length == 0) {
                $.jGrowl("Indique de la lista de Respuestas cual es la Correcta", {
                    header: 'Validación'
                });
                return null;
            }
            l_error = false;
            for (var i = 0; i < l_rows.length; i++) {
                l_answer = $(l_rows[i]).find("input[data-input=answer]").val().trim();
                if (!gf_isValue(l_answer)) {
                    $.jGrowl("Existe una Respuesta no ingresada", {
                        header: 'Validación'
                    });
                    l_error = true;
                    break;
                }
                l_response.AnswerId.push(0);
                l_response.AnswerText.push(l_answer);
                l_response.AnswerCorrect.push($(l_rows[i]).find("input[data-input=correct]").is(":checked") ? "S" : "N");
                l_response.AnswerOrder.push(0);
            }
            if (l_error) {
                return null;
            }
        } else if (l_type == 'checkbox') {
            var l_rows = l_container.find("table[data-table=checkbox] tbody tr"), l_error, l_answer;
            if (l_rows.find("input[data-input=correct]:checked").length == 0) {
                $.jGrowl("Seleccione por lo menos una Respuesta Correcta", {
                    header: 'Validación'
                });
                return null;
            }
            l_error = false;
            for (var i = 0; i < l_rows.length; i++) {
                l_answer = $(l_rows[i]).find("input[data-input=answer]").val().trim();
                if (!gf_isValue(l_answer)) {
                    $.jGrowl("Existe una Respuesta no ingresada", {
                        header: 'Validación'
                    });
                    l_error = true;
                    break;
                }
                l_response.AnswerId.push(0);
                l_response.AnswerText.push(l_answer);
                l_response.AnswerCorrect.push($(l_rows[i]).find("input[data-input=correct]").is(":checked") ? "S" : "N");
                l_response.AnswerOrder.push(0);
            }
            if (l_error) {
                return null;
            }
        } else if (l_type == 'boolean') {
            var l_answer_a = l_container.find("input[data-input=answera]").val().trim(), l_answer_b = l_container.find("input[data-input=answerb]").val().trim(),
                l_correct_a = l_container.find("input[data-input=radioa]").is(":checked") ? "S" : "N",
                l_correct_b = l_container.find("input[data-input=radiob]").is(":checked") ? "S" : "N";
            if (!gf_isValue(l_answer_a)) {
                $.jGrowl("La respuesta A no está ingresada", {
                    header: 'Validación'
                });
                return null;
            }
            if (!gf_isValue(l_answer_b)) {
                $.jGrowl("La respuesta B no está ingresada", {
                    header: 'Validación'
                });
                return null;
            }
            //para a
            l_response.AnswerId.push(0);
            l_response.AnswerText.push(l_answer_a);
            l_response.AnswerCorrect.push(l_correct_a);
            l_response.AnswerOrder.push(0);
            //para b
            l_response.AnswerId.push(0);
            l_response.AnswerText.push(l_answer_b);
            l_response.AnswerCorrect.push(l_correct_b);
            l_response.AnswerOrder.push(0);
        } else if (l_type == 'completar') {
            var l_correct = l_container.find("input[data-input=correct]").tagsinput('items'),
                l_incorrect = l_container.find("input[data-input=incorrect]").tagsinput('items'), l_order;
            if (l_correct.length == 0) {
                $.jGrowl("Debe ingresar por lo menos una Respuesta Correcta", {
                    header: 'Validación'
                });
                return null;
            }
            if (l_incorrect.length == 0) {
                $.jGrowl("Debe ingresar por lo menos una Respuesta Incorrecta", {
                    header: 'Validación'
                });
                return null;
            }
            l_order = 1;
            for (var i = 0; i < l_correct.length; i++) {
                l_response.AnswerId.push(0);
                l_response.AnswerText.push(l_correct[i]);
                l_response.AnswerCorrect.push("S");
                l_response.AnswerOrder.push(l_order);
                l_order++;
            }
            for (var i = 0; i < l_incorrect.length; i++) {
                l_response.AnswerId.push(0);
                l_response.AnswerText.push(l_incorrect[i]);
                l_response.AnswerCorrect.push("N");
                l_response.AnswerOrder.push(0);
            }
        } else {
            return null;
        }
        return l_response;
    }
    function fn_get_answers(p_text) {
        var l_a = [], l_open = false, l_w = null;
        for (var i = 0; i < p_text.length; i++) {
            if (p_text.charAt(i) == '{') {
                l_open = true;
                l_w = '';
            } else if (p_text.charAt(i) == '}') {
                l_open = false;
                if (l_w != null) {
                    l_a.push(l_w);
                    l_w = null;
                }
            } else {
                if (l_open) {
                    l_w += p_text.charAt(i);
                }
            }
        }
        return l_a;
    }
    function fn_add_boolean() {
        var l_template = `<div class="col-12">
                                <div class="form-group mt-0">
                                    <label class="control-label" for="question_create_question"><span class="text-dark">Pregunta</span> <span class="text-danger">*</span> | Máximo 1000 caracteres</label>
                                    <input id="question_create_question" type="text" maxlength="1000" class="form-control" data-presave-object="Pregunta" />
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group mt-0">
                                    <label class="control-label mt-0"><span class="text-dark">Respuestas</span> <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-4 col-sm-3 col-md-2">
                                        <div class="form-group mt-0">
                                            <div class="radio radio-primary text-center cls-1812231220">
                                                <label>
                                                    <input type="radio" name="radiosboolean" data-input="radioa" checked />
                                                    <span class="circle">
                                                    </span>
                                                    <span class="check"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-8 col-sm-9 col-md-10">
                                        <div class="form-group mt-0">
                                            <input type="text" maxlength="200" class="form-control" data-input="answera" placeholder="Ingrese Respuesta A" data-presave-object="Respuesta A" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4 col-sm-3 col-md-2">
                                        <div class="form-group mt-0">
                                            <div class="radio radio-primary text-center cls-1812231220">
                                                <label>
                                                    <input type="radio" name="radiosboolean" data-input="radiob" />
                                                    <span class="circle">
                                                    </span>
                                                    <span class="check"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-8 col-sm-9 col-md-10">
                                        <div class="form-group mt-0">
                                            <input type="text" maxlength="200" class="form-control" data-input="answerb" placeholder="Ingrese Respuesta B" data-presave-object="Respuesta B" />
                                        </div>
                                    </div>
                                </div>
                            </div>`;
        l_container.html(l_template);
        //init
        $("#question_create_question").data({
            "PresaveRequired": true,
            "PresaveJson": '{"maxLength":1000}'
        });
        l_container.find("input[data-input=answera]").data({
            "PresaveRequired": true,
            "PresaveJson": '{"maxLength":200}'
        });
        l_container.find("input[data-input=answerb]").data({
            "PresaveRequired": true,
            "PresaveJson": '{"maxLength":200}'
        });
    };
    function fn_add_complete() {
        var l_template = `<div class="col-12">
                                <div class="form-group mt-0">
                                    <label class="control-label" for="question_create_question"><span class="text-dark">Pregunta</span> <span class="text-danger">*</span> | Máximo 1000 caracteres | Ingrese las respuestas entre <span class="text-dark">{}</span>, ejemplo: Mi nombres es <span class="text-dark">{Juan Perez}</span></label>
                                    <textarea id="question_create_question" maxlength="1000" class="form-control" style="resize: vertical;" data-input="question-for-complete" data-presave-object="Pregunta"></textarea>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group mt-0 cls-1812231357">
                                            <label class="control-label"><span class="text-dark">Respuestas Correctas</span> <span class="text-danger">*</span> | Ingreso automático desde la Pregunta</label>
                                            <input type="text" data-input="correct" class="form-control" disabled />
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <div class="form-group mt-0 cls-1812231357">
                                            <div class="form-group mt-0">
                                                <label class="control-label"><span class="text-dark">Respuestas Incorrectas</span> <span class="text-danger">*</span></label>
                                                <input type="text" data-input="incorrect" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
        l_container.html(l_template);
        //init
        $("#question_create_question").data({
            "PresaveRequired": true,
            "PresaveJson": '{"maxLength":1000}'
        });
        l_container.find("input[data-input=correct]").tagsinput({
            tagClass: 'badge badge-success cls-1812231352',
            trimValue: true,
            confirmKeys: [13]
        });
        l_container.find("input[data-input=incorrect]").tagsinput({
            tagClass: 'badge badge-danger cls-1812231352',
            trimValue: true,
            confirmKeys: [13]
        });
    }
    function fn_add_checkbox() {
        var l_template = `<div class="col-12">
                                <div class="form-group mt-0">
                                    <label class="control-label" for="question_create_question"><span class="text-dark">Pregunta</span> <span class="text-danger">*</span> | Máximo 1000 caracteres</label>
                                    <input id="question_create_question" type="text" maxlength="1000" class="form-control" data-presave-object="Pregunta" />
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group mt-0">
                                    <label class="control-label mt-0"><span class="text-dark">Respuestas</span> <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12">
                                <table class="table cls-1812231520" data-table="checkbox">
                                    <tbody>
                                        <tr>
                                            <td class="text-center" style="width: 50px;">
                                                <div class="checkbox">
                                                    <label class="m-0">
                                                        <input type="checkbox" data-input="correct"><span class="checkbox-material m-0"><span class="check"></span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <input class="form-control" data-input="answer" />
                                            </td>
                                            <td class="text-center" style="width: 50px;">
                                                <a href="javascript:void(0)" class="btn btn-xs btn-raised btn-success m-0 cls-1810072234" data-action="add" title="Nuevo">
                                                    <i class="fa fa-plus m-0"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>`;
        l_container.html(l_template);
        //init
        $("#question_create_question").data({
            "PresaveRequired": true,
            "PresaveJson": '{"maxLength":1000}'
        });
    }
    function fn_add_radio() {
        var l_template = `<div class="col-12">
                                <div class="form-group mt-0">
                                    <label class="control-label" for="question_create_question"><span class="text-dark">Pregunta</span> <span class="text-danger">*</span> | Máximo 1000 caracteres</label>
                                    <input id="question_create_question" type="text" maxlength="1000" class="form-control" data-presave-object="Pregunta" />
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group mt-0">
                                    <label class="control-label mt-0"><span class="text-dark">Respuestas</span> <span class="text-danger">*</span></label>
                                </div>
                            </div>
                            <div class="col-12">
                                <table class="table cls-1812231520" data-table="radio">
                                    <tbody>
                                        <tr>
                                            <td class="text-center" style="width: 50px;">
                                                <div class="radio radio-primary">
                                                    <label class="m-0">
                                                        <input type="radio" name="radiosradio" data-input="correct" />
                                                        <span class="circle"></span>
                                                        <span class="check"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <input class="form-control" data-input="answer" />
                                            </td>
                                            <td class="text-center" style="width: 50px;">
                                                <a href="javascript:void(0)" class="btn btn-xs btn-raised btn-success m-0 cls-1810072234" data-action="add" title="Nuevo">
                                                    <i class="fa fa-plus m-0"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>`;
        l_container.html(l_template);
        //init
        $("#question_create_question").data({
            "PresaveRequired": true,
            "PresaveJson": '{"maxLength":1000}'
        });
    }
    //init
    $("#question_create_course, #question_create_type").data({
        "PresaveRequired": true
    });
    gf_Helpers_CourseAreaDropDown({
        $: $("#question_create_course"),
        callback: function (o) {
            o.addClass("selectpicker").val(null).selectpicker();
        }
    });
    gf_Helpers_QuestionType2TestDropDown({
        $: $("#question_create_type"),
        callback: function (o) {
            o.addClass("selectpicker").val(null).selectpicker();
        }
    });
});