﻿"use strict";
$("document").ready(function () {
    var l_dataTable, l_chart;
    //events
    $('#dashboard1_index_filter_retrieve').click(function (evt) {
        evt.preventDefault();
        fn_get_dashboard();
    });
    $('#dashboard1_index_table tbody').on('click', 'i[data-action=detail]', function (evt) {
        evt.preventDefault();
        var l_this = $(this), l_tr = l_this.closest('tr'), l_row = l_dataTable.row(l_tr);
        if (l_row.child.isShown()) {
            l_this
                .removeClass('fa-minus-square-o')
                .addClass('fa-plus-square-o');
            l_row.child.hide();
        }
        else {
            l_this
                .removeClass('fa-plus-square-o')
                .addClass('fa-minus-square-o');
            if (l_row.child()) {
                l_row.child.show();
            } else {
                l_row.child('', 'cls-1901280028').show();
                fn_get_detail(l_row);
            }
        }
    });
    //scrollbar
    $('.scrollbar-rail').scrollbar();
    //fn
    function fn_get_dashboard() {
        fn_get_programming(0, '[SELECCIONE UN PROGRAMA]');
        var l_data = [], l_year = $('#dashboard1_index_filter_year').val();
        if (!gf_isYear(l_year)) {
            $.jGrowl("Ingrese un Año correcto", {
                header: "Validación"
            });
            return;
        }
        $.ajax({
            url: '/Dashboard1/GetDashboard1',
            data: JSON.stringify({ 'year': l_year }),
            type: 'post',
            //async: false,
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                gf_addLoadingTo($('#dashboard1_index_filter_retrieve'));
            },
            success: function (r) {
                if (r.status == 1) {
                    for (var i in r.response) {
                        l_data.push({
                            'programming': r.response[i][0],
                            'name': r.response[i][1],
                            'vacancies': r.response[i][2],
                            'registered': r.response[i][3],
                            'from_date': r.response[i][4],
                            'to_date': r.response[i][5]
                        });
                    }
                } else {
                    $.jGrowl(r.response, {
                        header: 'Error',
                        type: 'danger',
                        clipboard: r.exception
                    });
                }
            },
            complete: function () {
                gf_removeLoadingTo($('#dashboard1_index_filter_retrieve'));
                //  Min width
                $('#dashboard1_index_chart').css({ 'min-width': (l_data.length * 100) + 'px' });
                //  Set data
                l_chart.data = l_data;
            }
        });
    }
    function fn_get_detail(p_row) {
        var l_content = p_row.child().find('td');
        $.ajax({
            url: '/Dashboard1/Detail',
            data: JSON.stringify({ 'id': p_row.data()[0] }),
            contentType: 'application/json; charset=utf-8',
            type: 'post',
            dataType: 'html',
            beforeSend: function () {
                l_content.html('<div class="text-center small p-1 text-dark">Cargando...</div>');
            },
            success: function (r) {
                l_content.html(r);
            },
            error: function (xhr, status) {
                l_content.html('Error : ' + xhr.status + ', ' + xhr.statusText);
            }
        });
    }
    function fn_get_programming(p_prog, p_name) {
        $('#dashboard1_index_table_title').text('DETALLE DEL PROGRAMA: ' + p_name.toUpperCase());
        l_dataTable = $("#dashboard1_index_table").daDataTable({
            "DataTable": {
                "ajax": {
                    "url": "/Dashboard1/GetProgramming",
                    "type": "post",
                    "data": {
                        "programming": p_prog
                    }
                },
                "aoColumns": [
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center cls-1901280053",
                        "width": "10px",
                        "mRender": function (o) {
                            return '<i class="fa fa-plus-square-o cursor-pointer" data-action="detail"></i>';
                        }
                    },
                    {
                        "data": 3,
                        "className": "text-left"
                    },
                    {
                        "data": 1,
                        "className": "text-center"
                    },
                    {
                        "data": 2,
                        "className": "text-center"
                    },
                    {
                        "data": 4,
                        "className": "text-center"
                    },
                    {
                        "data": 5,
                        "className": "text-left"
                    }
                ],
                "order": [[1, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 10
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": null,
                "noSearch": [0]
            }
        }).Datatable;
        //buttons
        new $.fn.dataTable.Buttons(l_dataTable, {
            'buttons': [{
                'extend': 'excelHtml5',
                'text': '<i class="fa fa-file-excel-o"></i>Excel',
                'className': 'btn btn-sm btn-raised btn-success m-0 cls-1902151548',
                'exportOptions': {
                    'columns': [1, 2, 3, 4, 5]
                }
            }]
        }).container().appendTo($('#dashboard1_index_table_buttons'));
    }
    function fn_build_chart() {
        /*---------------------------- Build Chart ----------------------------*/
        //  Chart
        l_chart = am4core.create("dashboard1_index_chart", am4charts.XYChart3D);
        //  Add data
        l_chart.data = [];
        //  Legend
        l_chart.legend = new am4charts.Legend();
        //  Create axes
        var l_categoryAxis = l_chart.xAxes.push(new am4charts.CategoryAxis());
        l_categoryAxis.dataFields.category = "name";
        l_categoryAxis.renderer.grid.template.location = 0;
        l_categoryAxis.renderer.minGridDistance = 20;
        l_categoryAxis.fontSize = 14;
        //  Configure axis label
        var l_label = l_categoryAxis.renderer.labels.template;
        l_label.wrap = true;
        l_label.maxWidth = 120;
        l_label.fontSize = 14;
        l_label.events.on("hit", function (ev) {
            fn_get_programming(ev.target.dataItem._dataContext.programming, ev.target.dataItem._dataContext.name);
        }, this);
        //
        var l_valueAxis = l_chart.yAxes.push(new am4charts.ValueAxis());
        l_valueAxis.min = 0;
        l_valueAxis.fontSize = 14;
        //  Add Series =====================================================================>
        var l_series_registered = l_chart.series.push(new am4charts.ColumnSeries3D());
        l_series_registered.dataFields.valueY = "registered";
        l_series_registered.dataFields.categoryX = "name";
        l_series_registered.clustered = false;
        l_series_registered.columns.template.stroke = am4core.color("#fb6e52");
        l_series_registered.columns.template.fill = am4core.color("#fb6e52");
        l_series_registered.columns.template.fillOpacity = .75;
        l_series_registered.columns.template.width = am4core.percent(50);
        l_series_registered.columns.template
            .cursorOverStyle = [
                {
                    "property": "cursor",
                    "value": "pointer"
                }
            ];
        l_series_registered.tooltipText = `[bold]Registrados: {valueY}[/]`;
        l_series_registered.name = "Registrados";
        //  Add label
        var l_valueLabel_registered = l_series_registered.bullets.push(new am4charts.LabelBullet());
        l_valueLabel_registered.label.text = "{valueY}";
        l_valueLabel_registered.label.horizontalCenter = "center";
        l_valueLabel_registered.label.dy = -10;
        l_valueLabel_registered.label.hideOversized = false;
        l_valueLabel_registered.label.truncate = false;
        //  Add events
        l_series_registered.columns.template.events.on("hit", function (ev) {
            fn_get_programming(ev.target.dataItem._dataContext.programming, ev.target.dataItem._dataContext.name);
        }, this);
        //
        //  Add Series
        var l_series_vacancies = l_chart.series.push(new am4charts.ColumnSeries3D());
        l_series_vacancies.dataFields.valueY = "vacancies";
        l_series_vacancies.dataFields.categoryX = "name";
        l_series_vacancies.clustered = false;
        l_series_vacancies.columns.template.stroke = am4core.color("#ffce56");
        l_series_vacancies.columns.template.fill = am4core.color("#ffce56");
        l_series_vacancies.columns.template.fillOpacity = .75;
        l_series_vacancies.columns.template
            .cursorOverStyle = [
                {
                    "property": "cursor",
                    "value": "pointer"
                }
            ];
        l_series_vacancies.tooltipText = `[bold]Vacantes: {valueY}[/]
            Fecha Inicio: {from_date}
            Fecha Fin: {to_date}`;
        l_series_vacancies.name = "Vacantes";
        //  Add label
        var l_valueLabel_vacancies = l_series_vacancies.bullets.push(new am4charts.LabelBullet());
        l_valueLabel_vacancies.label.text = "{valueY}";
        l_valueLabel_vacancies.label.horizontalCenter = "center";
        l_valueLabel_vacancies.label.dy = -10;
        l_valueLabel_vacancies.label.hideOversized = false;
        l_valueLabel_vacancies.label.truncate = false;
        //  Add events
        l_series_vacancies.columns.template.events.on("hit", function (ev) {
            fn_get_programming(ev.target.dataItem._dataContext.programming, ev.target.dataItem._dataContext.name);
        }, this);
        //<==================================================================================
        //  Add Cursor
        l_chart.cursor = new am4charts.XYCursor();
        l_chart.cursor.lineX.disabled = true;
        l_chart.cursor.lineY.disabled = true;
        //  Add chart title
        var l_title = l_chart.titles.create();
        l_title.text = "Vacantes vs Inscritos";
        l_title.fontSize = 25;
        l_title.marginBottom = 20;
        //  Enable export
        l_chart.exporting.menu = new am4core.ExportMenu();
        l_chart.exporting.useWebFonts = false;
        l_chart.exporting.menu.items = [
            {
                "label": "...",
                "menu": [
                    { "type": "png", "label": "PNG" },
                    { "type": "jpg", "label": "JPG" }
                ]
            }
        ];
    }
    //init
    //  Themes
    am4core.useTheme(am4themes_material);
    am4core.useTheme(am4themes_animated);
    fn_build_chart();
    fn_get_dashboard();
});