﻿"use strict";
$("document").ready(function () {
    (function (p_model) {
        $("#place_edit_form").submit(function (evt) {
            evt.preventDefault();
        });
        $("#place_edit_save").click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_psave = gf_presaveForm($("#place_edit_form")), l_data;
            if (l_psave.hasError) {
                $.jGrowl("Ingrese correctamente los datos del formulario", {
                    header: 'Validación'
                });
                return;
            }
            l_data = {
                'Id': p_model.Id,
                'Description': $("#place_edit_description").val().trim(),
                'Status': $("#place_edit_status").is(":checked") ? "A" : "I"
            };
            $.daAlert({
                content: {
                    type: "text",
                    value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "save",
                        fn: function () {
                            $.ajax({
                                data: JSON.stringify(l_data),
                                url: '/Place/Edit',
                                type: 'post',
                                dataType: 'json',
                                contentType: "application/json; charset=utf-8",
                                beforeSend: _beforeSend,
                                success: _success
                            });
                            function _beforeSend() {
                                gf_addLoadingTo($("#place_edit_save"));
                            }
                            function _success(r) {
                                if (parseInt(r.status) == 1000) {
                                    $.jGrowl(r.response, {
                                        header: 'Información',
                                        type: 'primary'
                                    });
                                    location.href = '/Place/Index';
                                } else {
                                    $.jGrowl(r.response, {
                                        header: 'Error',
                                        type: 'danger',
                                        clipboard: r.exception
                                    });
                                    gf_removeLoadingTo($("#place_edit_save"));
                                }
                            }
                            return true;
                        }
                    }
                ],
                maximizable: false,
                title: "Confirmación"
            });
        });
        //fn
        //init
        $("#place_edit_description").data({
            PresaveRequired: true,
            PresaveJson: '{"maxLength":200}'
        });
    }(gf_getClone(ngon.Model)));
});