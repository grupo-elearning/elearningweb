﻿"use strict";
$("document").ready(function () {
    var l_chart_campus, l_chart_course, l_chart_participants, l_chart_participants_label;
    //plugins
    $('#dashboard2_index_campus_filter_from_date').datepicker().on('changeDate', function (e) {
        $('#dashboard2_index_campus_filter_to_date').datepicker('setStartDate', e.date);
    });
    $('#dashboard2_index_campus_filter_to_date').datepicker({
        orientation: "bottom right"
    }).on('changeDate', function (e) {
        $('#dashboard2_index_campus_filter_from_date').datepicker('setEndDate', e.date);
    });
    //
    $('#dashboard2_index_course_filter_from_date').datepicker().on('changeDate', function (e) {
        $('#dashboard2_index_course_filter_to_date').datepicker('setStartDate', e.date);
    });
    $('#dashboard2_index_course_filter_to_date').datepicker({
        orientation: "bottom right"
    }).on('changeDate', function (e) {
        $('#dashboard2_index_course_filter_from_date').datepicker('setEndDate', e.date);
    });
    //scrollbar
    $('.scrollbar-rail').scrollbar();
    //events
    $('#dashboard2_index_campus_filter_retrieve').click(function (evt) {
        evt.preventDefault();
        fn_get_campus();
    });
    $('#dashboard2_index_academic_situation_refresh').click(function (evt) {
        evt.preventDefault();
        fn_get_academic_situation();
    });
    $('#dashboard2_index_statistics_course_refresh').click(function (evt) {
        evt.preventDefault();
        fn_get_statistics_course();
    });
    $('#dashboard2_index_statistics_student_refresh').click(function (evt) {
        evt.preventDefault();
        fn_get_statistics_student();
    });
    $('#dashboard2_index_course_filter_retrieve').click(function (evt) {
        evt.preventDefault();
        fn_get_course();
    });
    $('#dashboard2_index_satisfaction_refresh').click(function (evt) {
        evt.preventDefault();
        fn_get_satisfaction();
    });
    //fn
    function fn_get_campus() {
        var l_from_date = $('#dashboard2_index_campus_filter_from_date').val(), l_to_date = $('#dashboard2_index_campus_filter_to_date').val(), l_data = [],
            l_ttl_active_users = 0, l_ttl_new_users = 0, l_ttl_users_login = 0, l_ttl_hours_on_campus_n = 0;
        if (!gf_isValue(l_from_date) || !gf_isValue(l_to_date)) {
            $.jGrowl("Ingrese un Rango de Fechas válido", {
                header: "Validación"
            });
            return;
        }
        l_from_date = $('#dashboard2_index_campus_filter_from_date').data('datepicker').getFormattedDate('yyyy-mm-dd');
        l_to_date = $('#dashboard2_index_campus_filter_to_date').data('datepicker').getFormattedDate('yyyy-mm-dd');
        $.ajax({
            url: '/Dashboard2/GetCampusInformation',
            data: JSON.stringify({ 'from_date': l_from_date, 'to_date': l_to_date }),
            type: 'post',
            //async: false,
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                gf_addLoadingTo($('#dashboard2_index_campus_filter_retrieve'));
            },
            success: function (r) {
                if (r.status == 1) {
                    for (var i in r.response) {
                        l_data.push({
                            'date': r.response[i][0],
                            'active_users': r.response[i][1],
                            'new_users': r.response[i][2],
                            'users_login': r.response[i][3],
                            'hours_on_campus_n': r.response[i][4] / 60,
                            'hours_on_campus_t': gf_convertToHHMM((r.response[i][4] / 60))
                        });
                        //totals
                        l_ttl_active_users += r.response[i][1];
                        l_ttl_new_users += r.response[i][2];
                        l_ttl_users_login += r.response[i][3];
                        l_ttl_hours_on_campus_n += r.response[i][4] / 60;
                    }
                } else {
                    $.jGrowl(r.response, {
                        header: 'Error',
                        type: 'danger',
                        clipboard: r.exception
                    });
                }
            },
            complete: function () {
                gf_removeLoadingTo($('#dashboard2_index_campus_filter_retrieve'));
                /*---------------------------- Set Counter ----------------------------*/
                $('#dashboard2_index_campus_active_users').text(l_ttl_active_users);
                $('#dashboard2_index_campus_new_users').text(l_ttl_new_users);
                $('#dashboard2_index_campus_users_login').text(l_ttl_users_login);
                $('#dashboard2_index_campus_hours_on_campus_t').text(gf_convertToHHMM(l_ttl_hours_on_campus_n));
                //counters
                $('#dashboard2_index_campus_active_users, #dashboard2_index_campus_new_users, #dashboard2_index_campus_users_login').counterUp({
                    delay: 10,
                    time: 100,
                });
                //set dates
                $('#dashboard2_index_campus_from_date').text($('#dashboard2_index_campus_filter_from_date').val());
                $('#dashboard2_index_campus_to_date').text($('#dashboard2_index_campus_filter_to_date').val());
                /*---------------------------- Set Data ----------------------------*/
                l_chart_campus.data = l_data;
            }
        });
    }
    function fn_get_academic_situation() {
        var l_pg = $('#dashboard2_index_academic_situation_pg'), l_pmax_img = $('#dashboard2_index_academic_situation_pmax_img'),
            l_pmax_name = $('#dashboard2_index_academic_situation_pmax_name'), l_pmax_value = $('#dashboard2_index_academic_situation_pmax_value'),
            l_pmin_img = $('#dashboard2_index_academic_situation_pmin_img'), l_pmin_name = $('#dashboard2_index_academic_situation_pmin_name'),
            l_pmin_value = $('#dashboard2_index_academic_situation_pmin_value');
        var l_no_data = false, l_result = {};
        $.ajax({
            url: '/Dashboard2/GetAcademicSituation',
            type: 'post',
            //async: false,
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                l_pg.text('');
                l_pmax_img.attr('src', '/assets/app/img/no-image-icon.svg');
                l_pmax_name.text('');
                l_pmax_value.text('');
                l_pmin_img.attr('src', '/assets/app/img/no-image-icon.svg');
                l_pmin_name.text('');
                l_pmin_value.text('');
                //
                l_result.avg = 0;
                l_result.min = {
                    id: 0,
                    value: Number.POSITIVE_INFINITY,
                    name: '',
                    image: ''
                };
                l_result.max = {
                    id: 0,
                    value: Number.NEGATIVE_INFINITY,
                    name: '',
                    image: ''
                };
            },
            success: function (r) {
                if (r.status == 1) {
                    if (r.response.length != 0) {
                        for (var i in r.response) {
                            if (r.response[i][4] < l_result.min.value) {
                                l_result.min.id = r.response[i][0];
                                l_result.min.value = r.response[i][4];
                                l_result.min.name = r.response[i][2];
                                l_result.min.image = r.response[i][3];
                            }
                            if (r.response[i][4] > l_result.max.value) {
                                l_result.max.id = r.response[i][0];
                                l_result.max.value = r.response[i][4];
                                l_result.max.name = r.response[i][2];
                                l_result.max.image = r.response[i][3];
                            }
                            //avg
                            l_result.avg += r.response[i][4];
                        }
                        l_result.avg = parseInt((l_result.avg / r.response.length));
                    } else {
                        l_no_data = true;
                    }
                } else {
                    $.jGrowl(r.response, {
                        header: 'Error',
                        type: 'danger',
                        clipboard: r.exception
                    });
                    l_no_data = true;
                }
            },
            complete: function () {
                if (!l_no_data) {
                    //avg
                    l_pg.text(l_result.avg + ' %');
                    //max
                    l_pmax_img.attr('src', l_result.max.image);
                    l_pmax_name.text(l_result.max.name);
                    l_pmax_value.text(l_result.max.value + ' %');
                    //min
                    if (l_result.max.id != l_result.min.id) {
                        l_pmin_img.attr('src', l_result.min.image);
                        l_pmin_name.text(l_result.min.name);
                        l_pmin_value.text(l_result.min.value + ' %');
                    }
                }
            }
        });
    }
    function fn_get_satisfaction() {
        var l_pg = $('#dashboard2_index_satisfaction_pg'), l_pmax_img = $('#dashboard2_index_satisfaction_pmax_img'),
            l_pmax_name = $('#dashboard2_index_satisfaction_pmax_name'), l_pmax_value = $('#dashboard2_index_satisfaction_pmax_value'),
            l_pmin_img = $('#dashboard2_index_satisfaction_pmin_img'), l_pmin_name = $('#dashboard2_index_satisfaction_pmin_name'),
            l_pmin_value = $('#dashboard2_index_satisfaction_pmin_value');
        var l_no_data = false, l_result = {};
        $.ajax({
            url: '/Dashboard2/GetSatisfaction',
            type: 'post',
            //async: false,
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                l_pg.text('');
                l_pmax_img.attr('src', '/assets/app/img/no-image-icon.svg');
                l_pmax_name.text('');
                l_pmax_value.text('');
                l_pmin_img.attr('src', '/assets/app/img/no-image-icon.svg');
                l_pmin_name.text('');
                l_pmin_value.text('');
                //
                l_result.avg = 0;
                l_result.min = {
                    id: 0,
                    value: Number.POSITIVE_INFINITY,
                    name: '',
                    image: ''
                };
                l_result.max = {
                    id: 0,
                    value: Number.NEGATIVE_INFINITY,
                    name: '',
                    image: ''
                };
            },
            success: function (r) {
                if (r.status == 1) {
                    if (r.response.length != 0) {
                        for (var i in r.response) {
                            if (r.response[i][4] < l_result.min.value) {
                                l_result.min.id = r.response[i][0];
                                l_result.min.value = r.response[i][4];
                                l_result.min.name = r.response[i][2];
                                l_result.min.image = r.response[i][3];
                            }
                            if (r.response[i][4] > l_result.max.value) {
                                l_result.max.id = r.response[i][0];
                                l_result.max.value = r.response[i][4];
                                l_result.max.name = r.response[i][2];
                                l_result.max.image = r.response[i][3];
                            }
                            //avg
                            l_result.avg += r.response[i][4];
                        }
                        l_result.avg = parseInt((l_result.avg / r.response.length));
                    } else {
                        l_no_data = true;
                    }
                } else {
                    $.jGrowl(r.response, {
                        header: 'Error',
                        type: 'danger',
                        clipboard: r.exception
                    });
                    l_no_data = true;
                }
            },
            complete: function () {
                if (!l_no_data) {
                    //avg
                    l_pg.text(l_result.avg + ' %');
                    //max
                    l_pmax_img.attr('src', l_result.max.image);
                    l_pmax_name.text(l_result.max.name);
                    l_pmax_value.text(l_result.max.value + ' %');
                    //min
                    if (l_result.max.id != l_result.min.id) {
                        l_pmin_img.attr('src', l_result.min.image);
                        l_pmin_name.text(l_result.min.name);
                        l_pmin_value.text(l_result.min.value + ' %');
                    }
                }
            }
        });
    }
    function fn_get_statistics_course() {
        var l_dataTable = $("#dashboard2_index_statistics_course").daDataTable({
            "DataTable": {
                "ajax": {
                    "url": "/Dashboard2/GetStatisticsCourse",
                    "type": "post"
                },
                "aoColumns": [
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center cls-1902141700",
                        "mRender": function (o) {
                            return `<a href="javascript:void(0)" class="img-thumbnail mb-0 cursor-default cls-1902071714">
                                        <div class="thumbnail-container">
                                            <img src="` + o[1] + `" alt="..." class="img-fluid">
                                        </div>
                                    </a>`;
                        }
                    },
                    {
                        "data": 2,
                        "className": "text-left"
                    },
                    {
                        "data": 3,
                        "className": "text-center"
                    },
                    {
                        "data": 5,
                        "className": "text-center"
                    },
                    {
                        "data": 4,
                        "className": "text-center"
                    },
                    {
                        "data": null,
                        "className": "text-center",
                        "mRender": function (o) {
                            return o[6] ? (o[6] + ` %`) : ``;
                        }
                    }
                ],
                "order": [[3, 'desc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 10
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": null,
                "noSearch": [0]
            }
        }).Datatable;
        //buttons
        new $.fn.dataTable.Buttons(l_dataTable, {
            'buttons': [{
                'extend': 'excelHtml5',
                'text': '<i class="fa fa-file-excel-o"></i>Excel',
                'className': 'btn btn-sm btn-raised btn-success m-0 cls-1902151548',
                'exportOptions': {
                    'columns': [1, 2, 3, 4, 5]
                }
            }]
        }).container().appendTo($('#dashboard2_index_statistics_course_buttons'));
    }
    function fn_get_statistics_student() {
        var l_dataTable = $("#dashboard2_index_statistics_student").daDataTable({
            "DataTable": {
                "ajax": {
                    "url": "/Dashboard2/GetStatisticsStudent",
                    "type": "post"
                },
                "aoColumns": [
                    {
                        "data": 1,
                        "className": "text-left"
                    },
                    {
                        "data": 2,
                        "className": "text-left"
                    },
                    {
                        "data": 3,
                        "className": "text-left"
                    },
                    {
                        "data": null,
                        "className": "text-center",
                        "width": "100px",
                        "mRender": function (o) {
                            return `<span style="display: none;">` + o[4] + `</span>` + gf_convertToHHMM((o[4] / 60)) + ` Hrs`;
                        }
                    }
                ],
                "order": [[3, 'desc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 10
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": null
            }
        }).Datatable;
        //buttons
        new $.fn.dataTable.Buttons(l_dataTable, {
            'buttons': [{
                'extend': 'excelHtml5',
                'text': '<i class="fa fa-file-excel-o"></i>Excel',
                'className': 'btn btn-sm btn-raised btn-success m-0 cls-1902151548',
                'exportOptions': {
                    'format': {
                        'body': function (data, row, column, node) {
                            return column == 3 ?
                                data.replace(/<\s*span[^>]*>(.*?)<\s*\/\s*span>/g, '') :
                                data;
                        }
                    }
                }
            }]
        }).container().appendTo($('#dashboard2_index_statistics_student_buttons'));
    }
    function fn_get_course() {
        var l_course = $('#dashboard2_index_course_filter_course').val(), l_from_date = $('#dashboard2_index_course_filter_from_date').val(),
            l_to_date = $('#dashboard2_index_course_filter_to_date').val(), l_data = [],
            l_ttl_not_started = 0, l_ttl_in_progress = 0, l_ttl_graduates = 0, l_ttl_registers_by_date, l_current_date;
        if (!gf_isValue(l_course)) {
            $.jGrowl("Seleccione un Curso", {
                header: "Validación"
            });
            return;
        }
        if (!gf_isValue(l_from_date) || !gf_isValue(l_to_date)) {
            $.jGrowl("Ingrese un Rango de Fechas válido", {
                header: "Validación"
            });
            return;
        }
        l_from_date = $('#dashboard2_index_course_filter_from_date').data('datepicker').getFormattedDate('yyyy-mm-dd');
        l_to_date = $('#dashboard2_index_course_filter_to_date').data('datepicker').getFormattedDate('yyyy-mm-dd');
        $.ajax({
            url: '/Dashboard2/GetCourseCharacteristic',
            data: JSON.stringify({ 'course': l_course, 'from_date': l_from_date, 'to_date': l_to_date }),
            type: 'post',
            //async: false,
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                gf_addLoadingTo($('#dashboard2_index_course_filter_retrieve'));
            },
            success: function (r) {
                if (r.status == 1) {
                    l_current_date = null;
                    l_ttl_registers_by_date = 0;
                    for (var i in r.response) {
                        if (l_current_date && l_current_date != r.response[i][0]) {
                            l_data.push({
                                'date': l_current_date,
                                'registered': l_ttl_registers_by_date,
                                'visits': 0,
                                'completed_contents': 0,
                                'graduates': 0,
                                'hours_on_course_n': 0,
                                'hours_on_course_t': "00:00"
                            });
                            l_ttl_registers_by_date = 0;
                            l_current_date = null;
                        }
                        if (r.response[i][1] == "X") {
                            l_data.push({
                                'date': r.response[i][0],
                                'registered': l_ttl_registers_by_date,
                                'visits': r.response[i][3],
                                'completed_contents': r.response[i][5],
                                'graduates': r.response[i][6],
                                'hours_on_course_n': r.response[i][4] / 60,
                                'hours_on_course_t': gf_convertToHHMM((r.response[i][4] / 60))
                            });
                            l_ttl_registers_by_date = 0;
                            l_current_date = null;
                        } else {
                            l_current_date = r.response[i][0];
                            l_ttl_registers_by_date += r.response[i][2];
                        }
                        //totals
                        l_ttl_not_started += r.response[i][1] == "NC" ? r.response[i][2] : 0;
                        l_ttl_in_progress += r.response[i][1] == "EP" ? r.response[i][2] : 0;
                        l_ttl_graduates += r.response[i][1] == "GR" ? r.response[i][2] : 0;
                    }
                } else {
                    $.jGrowl(r.response, {
                        header: 'Error',
                        type: 'danger',
                        clipboard: r.exception
                    });
                }
            },
            complete: function () {
                gf_removeLoadingTo($('#dashboard2_index_course_filter_retrieve'));
                //set dates
                $('#dashboard2_index_course_from_date').text($('#dashboard2_index_course_filter_from_date').val());
                $('#dashboard2_index_course_to_date').text($('#dashboard2_index_course_filter_to_date').val());
                /*---------------------------- Set Data ----------------------------*/
                l_chart_course.data = l_data;
                /*---------------------------- Participantes ----------------------------*/
                l_chart_participants_label.text = l_ttl_not_started + l_ttl_in_progress + l_ttl_graduates;
                l_chart_participants.data = [{
                    "status": "No han comenzado",
                    "value": l_ttl_not_started
                }, {
                    "status": "En progreso",
                    "value": l_ttl_in_progress
                }, {
                    "status": "Graduados",
                    "value": l_ttl_graduates
                }];
            }
        });
    }
    function fn_build_campus_chart() {
        /*---------------------------- Build Chart ----------------------------*/
        //  Chart
        l_chart_campus = am4core.create("dashboard2_index_campus_chart", am4charts.XYChart);
        l_chart_campus.language.locale = am4lang_es_ES;
        //  Add data
        l_chart_campus.data = [];
        //  Legend
        l_chart_campus.legend = new am4charts.Legend();
        l_chart_campus.legend.fontSize = 13;
        // Create axes
        var l_dateAxis = l_chart_campus.xAxes.push(new am4charts.DateAxis());
        l_dateAxis.dateFormats.setKey("day", "dd MMM yyyy");
        l_dateAxis.dateFormats.setKey("month", "MMM yyyy");
        l_dateAxis.fontSize = 14;
        //l_dateAxis.periodChangeDateFormats.setKey("day", "MMMM yyyy");
        var l_label = l_dateAxis.renderer.labels.template;
        l_label.fontSize = 14;
        //  Configure axis label
        var l_valueAxis = l_chart_campus.yAxes.push(new am4charts.ValueAxis());
        l_valueAxis.min = 0;
        l_valueAxis.fontSize = 14;
        //  Add Series =====================================================================>
        var l_series_active_users = l_chart_campus.series.push(new am4charts.LineSeries());
        l_series_active_users.dataFields.valueY = "active_users";
        l_series_active_users.dataFields.dateX = "date";
        l_series_active_users.tooltipText = "Usuario Activo: {valueY}";
        l_series_active_users.strokeWidth = 2;
        l_series_active_users.fillOpacity = 0.25;
        l_series_active_users.minBulletDistance = 10;
        l_series_active_users.tooltip.pointerOrientation = "vertical";
        l_series_active_users.tooltip.background.cornerRadius = 20;
        l_series_active_users.tooltip.background.fillOpacity = 0.85;
        l_series_active_users.tooltip.label.padding(12, 12, 12, 12);
        l_series_active_users.name = "Usuario Activo";
        l_series_active_users.fill = am4core.color("#fb6e52");
        l_series_active_users.stroke = am4core.color("#fb6e52");
        // Make bullets grow on hover
        var l_bullet_active_users = l_series_active_users.bullets.push(new am4charts.CircleBullet());
        l_bullet_active_users.circle.strokeWidth = 2;
        l_bullet_active_users.circle.radius = 4;
        l_bullet_active_users.circle.fill = am4core.color("#fff");
        var l_bullethover_active_users = l_bullet_active_users.states.create("hover");
        l_bullethover_active_users.properties.scale = 1.3;
        //  Add Series
        var l_series_new_users = l_chart_campus.series.push(new am4charts.LineSeries());
        l_series_new_users.dataFields.valueY = "new_users";
        l_series_new_users.dataFields.dateX = "date";
        l_series_new_users.tooltipText = "Usuario Nuevo: {valueY}";
        l_series_new_users.strokeWidth = 2;
        l_series_new_users.fillOpacity = 0.25;
        l_series_new_users.minBulletDistance = 10;
        l_series_new_users.tooltip.pointerOrientation = "vertical";
        l_series_new_users.tooltip.background.cornerRadius = 20;
        l_series_new_users.tooltip.background.fillOpacity = 0.85;
        l_series_new_users.tooltip.label.padding(12, 12, 12, 12);
        l_series_new_users.name = "Usuario Nuevo";
        l_series_new_users.fill = am4core.color("#ffce55");
        l_series_new_users.stroke = am4core.color("#ffce55");
        // Make bullets grow on hover
        var l_bullet_new_users = l_series_new_users.bullets.push(new am4charts.CircleBullet());
        l_bullet_new_users.circle.strokeWidth = 2;
        l_bullet_new_users.circle.radius = 4;
        l_bullet_new_users.circle.fill = am4core.color("#fff");
        var l_bullethover_new_users = l_bullet_new_users.states.create("hover");
        l_bullethover_new_users.properties.scale = 1.3;
        //  Add Series
        var l_series_users_login = l_chart_campus.series.push(new am4charts.LineSeries());
        l_series_users_login.dataFields.valueY = "users_login";
        l_series_users_login.dataFields.dateX = "date";
        l_series_users_login.tooltipText = "Usuario Total: {valueY}";
        l_series_users_login.strokeWidth = 2;
        l_series_users_login.fillOpacity = 0.25;
        l_series_users_login.minBulletDistance = 10;
        l_series_users_login.tooltip.pointerOrientation = "vertical";
        l_series_users_login.tooltip.background.cornerRadius = 20;
        l_series_users_login.tooltip.background.fillOpacity = 0.85;
        l_series_users_login.tooltip.label.padding(12, 12, 12, 12);
        l_series_users_login.name = "Usuario Total";
        l_series_users_login.fill = am4core.color("#a0d468");
        l_series_users_login.stroke = am4core.color("#a0d468");
        // Make bullets grow on hover
        var l_bullet_users_login = l_series_users_login.bullets.push(new am4charts.CircleBullet());
        l_bullet_users_login.circle.strokeWidth = 2;
        l_bullet_users_login.circle.radius = 4;
        l_bullet_users_login.circle.fill = am4core.color("#fff");
        var l_bullethover_users_login = l_bullet_users_login.states.create("hover");
        l_bullethover_users_login.properties.scale = 1.3;
        //  Add Series
        var l_series_hours_on_campus = l_chart_campus.series.push(new am4charts.LineSeries());
        l_series_hours_on_campus.dataFields.valueY = "hours_on_campus_n";
        l_series_hours_on_campus.dataFields.dateX = "date";
        l_series_hours_on_campus.tooltipText = "Cantidad de Horas Utilizadas: {hours_on_campus_t}";
        l_series_hours_on_campus.strokeWidth = 2;
        l_series_hours_on_campus.fillOpacity = 0.25;
        l_series_hours_on_campus.minBulletDistance = 10;
        l_series_hours_on_campus.tooltip.pointerOrientation = "vertical";
        l_series_hours_on_campus.tooltip.background.cornerRadius = 20;
        l_series_hours_on_campus.tooltip.background.fillOpacity = 0.85;
        l_series_hours_on_campus.tooltip.label.padding(12, 12, 12, 12);
        l_series_hours_on_campus.name = "Cantidad de Horas Utilizadas";
        l_series_hours_on_campus.fill = am4core.color("#2dc3e8");
        l_series_hours_on_campus.stroke = am4core.color("#2dc3e8");
        // Make bullets grow on hover
        var l_bullet_hours_on_campus = l_series_hours_on_campus.bullets.push(new am4charts.CircleBullet());
        l_bullet_hours_on_campus.circle.strokeWidth = 2;
        l_bullet_hours_on_campus.circle.radius = 4;
        l_bullet_hours_on_campus.circle.fill = am4core.color("#fff");
        var l_bullethover_hours_on_campus = l_bullet_hours_on_campus.states.create("hover");
        l_bullethover_hours_on_campus.properties.scale = 1.3;
        //<==================================================================================
        // Make a panning cursor
        l_chart_campus.cursor = new am4charts.XYCursor();
        l_chart_campus.cursor.behavior = "panXY";
        l_chart_campus.cursor.xAxis = l_dateAxis;
        //l_chart.cursor.snapToSeries = l_series_active_users;
        //  Add chart title
        var l_title = l_chart_campus.titles.create();
        l_title.align = "left";
        l_title.text = "Información estadística de los participantes de tu Campus";
        l_title.fontSize = 13;
        l_title.fontWeight = 400;
        l_title.marginBottom = 15;
        l_title.fill = am4core.color("#aaa");
        // Create vertical scrollbar and place it before the value axis
        l_chart_campus.scrollbarY = new am4core.Scrollbar();
        l_chart_campus.scrollbarY.parent = l_chart_campus.leftAxesContainer;
        l_chart_campus.scrollbarY.toBack();
        // Create a horizontal scrollbar with previe and place it underneath the date axis
        l_chart_campus.scrollbarX = new am4charts.XYChartScrollbar();
        l_chart_campus.scrollbarX.fontSize = 14;
        l_chart_campus.scrollbarX.height = 60;
        l_chart_campus.scrollbarX.series.push(l_series_active_users);
        l_chart_campus.scrollbarX.series.push(l_series_new_users);
        l_chart_campus.scrollbarX.series.push(l_series_users_login);
        l_chart_campus.scrollbarX.series.push(l_series_hours_on_campus);
        //l_chart.scrollbarX.parent = l_chart.bottomAxesContainer;
        //  Enable export
        l_chart_campus.exporting.menu = new am4core.ExportMenu();
        l_chart_campus.exporting.useWebFonts = false;
        l_chart_campus.exporting.menu.items = [
            {
                "label": "...",
                "menu": [
                    { "type": "png", "label": "PNG" },
                    { "type": "jpg", "label": "JPG" },
                    //{ "type": "pdf", "label": "PDF" }
                ]
            }
        ];
    }
    function fn_build_course_chart() {
        /*---------------------------- Build Chart ----------------------------*/
        //  Chart
        l_chart_course = am4core.create("dashboard2_index_course_chart", am4charts.XYChart);
        l_chart_course.language.locale = am4lang_es_ES;
        //  Add data
        l_chart_course.data = [];
        //  Legend
        l_chart_course.legend = new am4charts.Legend();
        l_chart_course.legend.fontSize = 13;
        // Create axes
        var l_dateAxis = l_chart_course.xAxes.push(new am4charts.DateAxis());
        l_dateAxis.dateFormats.setKey("day", "dd MMM yyyy");
        l_dateAxis.dateFormats.setKey("month", "MMM yyyy");
        l_dateAxis.fontSize = 14;
        //l_dateAxis.periodChangeDateFormats.setKey("day", "MMMM yyyy");
        var l_label = l_dateAxis.renderer.labels.template;
        l_label.fontSize = 14;
        //  Configure axis label
        var l_valueAxis = l_chart_course.yAxes.push(new am4charts.ValueAxis());
        l_valueAxis.min = 0;
        l_valueAxis.fontSize = 14;
        //  Add Series =====================================================================>
        var l_series_registered = l_chart_course.series.push(new am4charts.LineSeries());
        l_series_registered.dataFields.valueY = "registered";
        l_series_registered.dataFields.dateX = "date";
        l_series_registered.tooltipText = "Inscripciones: {valueY}";
        l_series_registered.strokeWidth = 2;
        l_series_registered.fillOpacity = 0.25;
        l_series_registered.minBulletDistance = 10;
        l_series_registered.tooltip.pointerOrientation = "vertical";
        l_series_registered.tooltip.background.cornerRadius = 20;
        l_series_registered.tooltip.background.fillOpacity = 0.85;
        l_series_registered.tooltip.label.padding(12, 12, 12, 12);
        l_series_registered.name = "Inscripciones";
        l_series_registered.fill = am4core.color("#ed4e2a");
        l_series_registered.stroke = am4core.color("#ed4e2a");
        // Make bullets grow on hover
        var l_bullet_registered = l_series_registered.bullets.push(new am4charts.CircleBullet());
        l_bullet_registered.circle.strokeWidth = 2;
        l_bullet_registered.circle.radius = 4;
        l_bullet_registered.circle.fill = am4core.color("#fff");
        var l_bullethover_registered = l_bullet_registered.states.create("hover");
        l_bullethover_registered.properties.scale = 1.3;
        //  Add Series
        var l_series_visits = l_chart_course.series.push(new am4charts.LineSeries());
        l_series_visits.dataFields.valueY = "visits";
        l_series_visits.dataFields.dateX = "date";
        l_series_visits.tooltipText = "Visitas al Curso: {valueY}";
        l_series_visits.strokeWidth = 2;
        l_series_visits.fillOpacity = 0.25;
        l_series_visits.minBulletDistance = 10;
        l_series_visits.tooltip.pointerOrientation = "vertical";
        l_series_visits.tooltip.background.cornerRadius = 20;
        l_series_visits.tooltip.background.fillOpacity = 0.85;
        l_series_visits.tooltip.label.padding(12, 12, 12, 12);
        l_series_visits.name = "Visitas al Curso";
        l_series_visits.fill = am4core.color("#ffce55");
        l_series_visits.stroke = am4core.color("#ffce55");
        // Make bullets grow on hover
        var l_bullet_visits = l_series_visits.bullets.push(new am4charts.CircleBullet());
        l_bullet_visits.circle.strokeWidth = 2;
        l_bullet_visits.circle.radius = 4;
        l_bullet_visits.circle.fill = am4core.color("#fff");
        var l_bullethover_visits = l_bullet_visits.states.create("hover");
        l_bullethover_visits.properties.scale = 1.3;
        //  Add Series
        var l_series_hours_on_course = l_chart_course.series.push(new am4charts.LineSeries());
        l_series_hours_on_course.dataFields.valueY = "hours_on_course_n";
        l_series_hours_on_course.dataFields.dateX = "date";
        l_series_hours_on_course.tooltipText = "Horas en el curso: {hours_on_course_t}";
        l_series_hours_on_course.strokeWidth = 2;
        l_series_hours_on_course.fillOpacity = 0.25;
        l_series_hours_on_course.minBulletDistance = 10;
        l_series_hours_on_course.tooltip.pointerOrientation = "vertical";
        l_series_hours_on_course.tooltip.background.cornerRadius = 20;
        l_series_hours_on_course.tooltip.background.fillOpacity = 0.85;
        l_series_hours_on_course.tooltip.label.padding(12, 12, 12, 12);
        l_series_hours_on_course.name = "Horas en el curso";
        l_series_hours_on_course.fill = am4core.color("#8cc474");
        l_series_hours_on_course.stroke = am4core.color("#8cc474");
        // Make bullets grow on hover
        var l_bullet_hours_on_course = l_series_hours_on_course.bullets.push(new am4charts.CircleBullet());
        l_bullet_hours_on_course.circle.strokeWidth = 2;
        l_bullet_hours_on_course.circle.radius = 4;
        l_bullet_hours_on_course.circle.fill = am4core.color("#fff");
        var l_bullethover_hours_on_course = l_bullet_hours_on_course.states.create("hover");
        l_bullethover_hours_on_course.properties.scale = 1.3;
        //  Add Series
        var l_series_completed_contents = l_chart_course.series.push(new am4charts.LineSeries());
        l_series_completed_contents.dataFields.valueY = "completed_contents";
        l_series_completed_contents.dataFields.dateX = "date";
        l_series_completed_contents.tooltipText = "Contenidos Completados: {valueY}";
        l_series_completed_contents.strokeWidth = 2;
        l_series_completed_contents.fillOpacity = 0.25;
        l_series_completed_contents.minBulletDistance = 10;
        l_series_completed_contents.tooltip.pointerOrientation = "vertical";
        l_series_completed_contents.tooltip.background.cornerRadius = 20;
        l_series_completed_contents.tooltip.background.fillOpacity = 0.85;
        l_series_completed_contents.tooltip.label.padding(12, 12, 12, 12);
        l_series_completed_contents.name = "Contenidos Completados";
        l_series_completed_contents.fill = am4core.color("#11a9cc");
        l_series_completed_contents.stroke = am4core.color("#11a9cc");
        // Make bullets grow on hover
        var l_bullet_completed_contents = l_series_completed_contents.bullets.push(new am4charts.CircleBullet());
        l_bullet_completed_contents.circle.strokeWidth = 2;
        l_bullet_completed_contents.circle.radius = 4;
        l_bullet_completed_contents.circle.fill = am4core.color("#fff");
        var l_bullethover_completed_contents = l_bullet_completed_contents.states.create("hover");
        l_bullethover_completed_contents.properties.scale = 1.3;
        //  Add Series
        var l_series_graduates = l_chart_course.series.push(new am4charts.LineSeries());
        l_series_graduates.dataFields.valueY = "graduates";
        l_series_graduates.dataFields.dateX = "date";
        l_series_graduates.tooltipText = "Graduados: {valueY}";
        l_series_graduates.strokeWidth = 2;
        l_series_graduates.fillOpacity = 0.25;
        l_series_graduates.minBulletDistance = 10;
        l_series_graduates.tooltip.pointerOrientation = "vertical";
        l_series_graduates.tooltip.background.cornerRadius = 20;
        l_series_graduates.tooltip.background.fillOpacity = 0.85;
        l_series_graduates.tooltip.label.padding(12, 12, 12, 12);
        l_series_graduates.name = "Graduados";
        l_series_graduates.fill = am4core.color("#6f85bf");
        l_series_graduates.stroke = am4core.color("#6f85bf");
        // Make bullets grow on hover
        var l_bullet_graduates = l_series_graduates.bullets.push(new am4charts.CircleBullet());
        l_bullet_graduates.circle.strokeWidth = 2;
        l_bullet_graduates.circle.radius = 4;
        l_bullet_graduates.circle.fill = am4core.color("#fff");
        var l_bullethover_graduates = l_bullet_graduates.states.create("hover");
        l_bullethover_graduates.properties.scale = 1.3;
        //<==================================================================================
        // Make a panning cursor
        l_chart_course.cursor = new am4charts.XYCursor();
        l_chart_course.cursor.behavior = "panXY";
        l_chart_course.cursor.xAxis = l_dateAxis;
        //l_chart.cursor.snapToSeries = l_series_active_users;
        // Create vertical scrollbar and place it before the value axis
        l_chart_course.scrollbarY = new am4core.Scrollbar();
        l_chart_course.scrollbarY.parent = l_chart_course.leftAxesContainer;
        l_chart_course.scrollbarY.toBack();
        // Create a horizontal scrollbar with previe and place it underneath the date axis
        l_chart_course.scrollbarX = new am4charts.XYChartScrollbar();
        l_chart_course.scrollbarX.fontSize = 14;
        l_chart_course.scrollbarX.height = 60;
        l_chart_course.scrollbarX.series.push(l_series_registered);
        l_chart_course.scrollbarX.series.push(l_series_visits);
        l_chart_course.scrollbarX.series.push(l_series_hours_on_course);
        l_chart_course.scrollbarX.series.push(l_series_completed_contents);
        l_chart_course.scrollbarX.series.push(l_series_graduates);
        //l_chart.scrollbarX.parent = l_chart.bottomAxesContainer;
        //  Enable export
        l_chart_course.exporting.menu = new am4core.ExportMenu();
        l_chart_course.exporting.useWebFonts = false;
        l_chart_course.exporting.menu.items = [
            {
                "label": "...",
                "menu": [
                    { "type": "png", "label": "PNG" },
                    { "type": "jpg", "label": "JPG" },
                    //{ "type": "pdf", "label": "PDF" }
                ]
            }
        ];
    }
    function fn_build_participantes_chart() {
        /*---------------------------- Build Chart ----------------------------*/
        //  Chart
        l_chart_participants = am4core.create("dashboard2_index_participants_chart", am4charts.PieChart);
        l_chart_participants.hiddenState.properties.opacity = 0;
        l_chart_participants.innerRadius = am4core.percent(40);
        l_chart_participants.language.locale = am4lang_es_ES;
        //  Add label
        l_chart_participants_label = l_chart_participants.seriesContainer.createChild(am4core.Label);
        //l_chart_participantes_label.text = ``;
        l_chart_participants_label.fontSize = 30;
        l_chart_participants_label.horizontalCenter = "middle";
        l_chart_participants_label.verticalCenter = "middle";
        //  Add data
        l_chart_participants.data = [];
        //  Legend
        l_chart_participants.legend = new am4charts.Legend();
        //  Add Series
        var l_series = l_chart_participants.series.push(new am4charts.PieSeries());
        l_series.dataFields.value = "value";
        l_series.dataFields.category = "status";
        l_series.slices.template.cornerRadius = 5;
        l_series.slices.template.stroke = am4core.color("#fff");
        l_series.slices.template.strokeWidth = 2;
        l_series.slices.template.strokeOpacity = 1;
        l_series.ticks.template.disabled = true;
        l_series.labels.template.disabled = true;
        // Create a base filter effect (as if it's not there) for the hover to return to
        var l_shadow = l_series.slices.template.filters.push(new am4core.DropShadowFilter);
        l_shadow.opacity = 0;
        // Create hover state
        var l_hoverState = l_series.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists
        // Slightly shift the shadow and make it more prominent on hover
        var l_hoverShadow = l_hoverState.filters.push(new am4core.DropShadowFilter);
        l_hoverShadow.opacity = 0.7;
        l_hoverShadow.blur = 5;
        //  Set colors
        l_series.colors.list = [am4core.color("rgb(174,174,174)"), am4core.color("rgb(165,30,23)"), am4core.color("rgb(75,14,11)")];
        //  Enable export
        l_chart_participants.exporting.menu = new am4core.ExportMenu();
        l_chart_participants.exporting.useWebFonts = false;
        l_chart_participants.exporting.menu.items = [
            {
                "label": "...",
                "menu": [
                    { "type": "png", "label": "PNG" },
                    { "type": "jpg", "label": "JPG" },
                    //{ "type": "pdf", "label": "PDF" }
                ]
            }
        ];
    }
    //init
    //  Themes
    am4core.useTheme(am4themes_material);
    am4core.useTheme(am4themes_animated);
    fn_build_campus_chart();
    fn_build_course_chart();
    fn_build_participantes_chart();
    fn_get_campus();
    fn_get_academic_situation();
    fn_get_satisfaction();
    fn_get_statistics_course();
    fn_get_statistics_student();
    //
    gf_Helpers_CourseAreaDropDownAll({
        $: $("#dashboard2_index_course_filter_course"),
        callback: function (o) {
            o.addClass("selectpicker").val(null).selectpicker();
        }
    });
});