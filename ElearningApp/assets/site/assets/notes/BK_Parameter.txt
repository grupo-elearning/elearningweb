﻿/////////////////////////////////////////////////////////Edit.cshtml
@model ElearningApp.Models.Entity.Parameter_Entity
@{
    ViewBag.title = "Parámetros";
    ViewBag.app_version = ElearningApp.Models.Helpers.APP_VERSION;
}
@using NGon
@Html.IncludeNGon()
<div class="material-background ms-hero-img-elearning ms-hero-bg-app"></div>
<div class="container container-full app-container">
    <div class="app-container-breadcrumb app-has-ms-paper-menu">
        <ul class="app-breadcrumb animated fadeInRight animation-delay-5">
            <li><a href="@Url.Action("Index", "Home")"><span class="glyphicon glyphicon-home"> </span></a></li>
            <li><a href="javascript:void(0)" class="active">Parámetros</a></li>
        </ul>
    </div>
    <div class="ms-paper">
        <div class="row">
            <div class="col-12 ms-paper-content-container">
                <div class="ms-paper-content">
                    <div class="app-row mb-5">
                        <h1 class="pull-left m-0">Parámetros</h1>
                    </div>
                    <form id="parameter_edit_form" autocomplete="off" action="#" class="app-form" data-presave-rule="object" data-presave-analysis="insert">
                        <h4 class="right-line right-line-white cls-1812051504">Curso Conductor</h4>
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group mt-0">
                                    <label class="control-label" for="parameter_edit_course"><span class="text-dark">Curso</span> <span class="text-danger">*</span></label>
                                    <select id="parameter_edit_course" class="form-control" data-live-search="true" data-dropup-auto="false" data-presave-object="Curso"></select>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group mt-0">
                                    <label class="control-label" for="parameter_edit_time"><span class="text-dark">Tiempo para Cerrar Sesión</span> <span class="text-danger">*</span> | Segundos, Número entero no menor a cero</label>
                                    <input id="parameter_edit_time" type="number" min="0" class="form-control" value="@Model.WaitTimeCloseSession" data-presave-object="Tiempo para Cerrar Sesión" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group mt-0">
                                    <label class="control-label" for="parameter_edit_image"><span class="text-dark">Imagen</span> <span class="text-danger">*</span> | Imagen que aparece al finalizar un examen</label>
                                    <input id="parameter_edit_image_display" type="text" readonly class="form-control" placeholder="Seleccionar..." value="@Model.ImageFinalizedName" data-presave-object="Imagen" />
                                    <input id="parameter_edit_image" type="file" title=" " data-retrieved="true" data-retrieved-name="@Model.ImageFinalizedName" />
                                </div>
                                <figure class="ms-thumbnail ms-thumbnail-diagonal card cls-1810071216">
                                    <img id="parameter_edit_image_preview" src="@Model.ImageFinalizedUrl" alt="" class="img-fluid cls-1810071157" />
                                    <figcaption class="ms-thumbnail-caption text-center">
                                        <div class="ms-thumbnail-caption-content">
                                            <h3 id="parameter_edit_image_notification" class="ms-thumbnail-caption-title" style="display: none;">Ninguna imagen seleccionada</h3>
                                            <div class="mt-2">
                                                <a id="parameter_edit_image_btn_add" href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm mr-1 btn-circle-white color-primary" style="display: none;">
                                                    <i class="zmdi zmdi-plus"></i>
                                                </a>
                                                <a id="parameter_edit_image_btn_remove" href="javascript:void(0)" class="btn-circle btn-circle-raised btn-circle-sm mr-1 btn-circle-white color-danger">
                                                    <i class="zmdi zmdi-close"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group mt-0">
                                    <label class="control-label"><span class="text-dark">Mensaje</span> <span class="text-danger">*</span> | Mensaje que aparece al finalizar un examen</label>
                                    <div id="parameter_edit_message">@Html.Raw(Model.MessageFinalized)</div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="form-group mt-0">
                                    <label class="control-label"><span class="text-dark">Mensaje Aprobado</span> <span class="text-danger">*</span> | Mensaje que aparece si ha aprobado un examen</label>
                                    <div id="parameter_edit_message_approved">@Html.Raw(Model.MessageApproved)</div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group mt-0">
                                    <label class="control-label"><span class="text-dark">Mensaje Desaprobado</span> <span class="text-danger">*</span> | Mensaje que aparece si ha desaprobado un examen</label>
                                    <div id="parameter_edit_message_disapproved">@Html.Raw(Model.MessageDisapproved)</div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <hr class="border-dark" />
                    <div class="app-row">
                        @Helpers.TagHelper.buttonSave("parameter_edit_save")
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section styles{
    <link href="~/bower_components/summernote/summernote.css" rel="stylesheet" />
    <link href="~/assets/app/app.css?v=@ViewBag.app_version" rel="stylesheet" />
    <link href="~/assets/site/site.css?v=@ViewBag.app_version" rel="stylesheet" />
}
@section scripts{
    <script src="~/bower_components/summernote/summernote.js"></script>
    <script src="~/bower_components/summernote/lang/summernote-es-ES.js"></script>
    <script src="~/assets/app/initialize.js?v=@ViewBag.app_version"></script>
    <script src="~/assets/site/parameter/edit.js?v=@ViewBag.app_version"></script>
}
/////////////////////////////////////////////////////////////////////////edit.js
"use strict";
$("document").ready(function () {
    (function (p_model) {
        $('#parameter_edit_message').summernote({
            lang: 'es-ES',
            placeholder: 'Ingrese el mensaje',
            height: 150,
            toolbar: [
                ['color', ['color']],
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ]
        });
        $('#parameter_edit_message_approved').summernote({
            lang: 'es-ES',
            placeholder: 'Ingrese el mensaje para Aprobado',
            height: 300,
            toolbar: [
                ['color', ['color']],
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['customButton', ['cb_preview']]
            ],
            'buttons': {
                'cb_preview': function (c) {
                    return $.summernote.ui.button({
                        //tooltip: 'Vista Previa',
                        contents: 'Vista Previa',
                        click: function () {
                            $.daAlert({
                                content: {
                                    type: 'text',
                                    value: `<div class="p-1">
                                                <div class="cls-1901290014">
                                                    <div class="message">` + c.$note.summernote('code') + `</div>
                                                    <img src="/assets/site/assets/images/aprobado.png" class="img-approved" />
                                                </div>
                                            </div>`
                                },
                                buttons: [
                                    {
                                        type: "cancel",
                                        fn: function () {
                                            return true;
                                        }
                                    }
                                ],
                                maximizable: false,
                                title: 'Vista Previa',
                                maxWidth: '700px'
                            });
                        }
                    }).render();
                }
            }
        });
        $('#parameter_edit_message_disapproved').summernote({
            lang: 'es-ES',
            placeholder: 'Ingrese el mensaje para Desaprobado',
            height: 300,
            toolbar: [
                ['color', ['color']],
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['customButton', ['cb_preview']]
            ],
            'buttons': {
                'cb_preview': function (c) {
                    return $.summernote.ui.button({
                        //tooltip: 'Vista Previa',
                        contents: 'Vista Previa',
                        click: function () {
                            $.daAlert({
                                content: {
                                    type: 'text',
                                    value: `<div class="p-1">
                                                <div class="cls-1901290014">
                                                    <div class="message">` + c.$note.summernote('code') + `</div>
                                                    <img src="/assets/site/assets/images/desaprobado.png" class="img-disapproved" />
                                                </div>
                                            </div>`
                                },
                                buttons: [
                                    {
                                        type: "cancel",
                                        fn: function () {
                                            return true;
                                        }
                                    }
                                ],
                                maximizable: false,
                                title: 'Vista Previa',
                                maxWidth: '700px'
                            });
                        }
                    }).render();
                }
            }
        });
        //events
        $('#parameter_edit_image_btn_add').click(function (evt) {
            evt.preventDefault();
            $("#parameter_edit_image").trigger("click");
        });
        $('#parameter_edit_image_btn_remove').click(function (evt) {
            evt.preventDefault();
            $("#parameter_edit_image").val("").trigger("change");
        });
        $('#parameter_edit_image').change(function () {
            var l_validate = gf_fileValidation({
                fileInput: $(this),
                imgPreview: $("#parameter_edit_image_preview"),
                fnError: function () {
                    $.jGrowl("Solo puede seleccionar archivos tipo: jpg, jpeg, png, bmp", {
                        header: "Validación"
                    });
                }
            });
            if (l_validate) {
                $("#parameter_edit_image_notification").hide();
                $("#parameter_edit_image_btn_add").hide();
                $("#parameter_edit_image_btn_remove").show();
            } else {
                $("#parameter_edit_image_preview").attr("src", "/assets/app/img/no-image-icon.svg");
                $("#parameter_edit_image_notification").show();
                $("#parameter_edit_image_btn_remove").hide();
                $("#parameter_edit_image_btn_add").show();
            }
            $('#parameter_edit_image').data("retrieved", false);
        });
        $('#parameter_edit_save').click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_psave = gf_presaveForm($("#parameter_edit_form")), l_formdata = new FormData();
            if (l_psave.hasError) {
                $.jGrowl("Ingrese correctamente los datos del formulario", {
                    header: 'Validación'
                });
                return;
            }
            if ($('#parameter_edit_message').summernote('isEmpty')) {
                $.jGrowl("Mensaje es campo requerido", {
                    header: 'Validación'
                });
                return;
            }
            if ($('#parameter_edit_message_approved').summernote('isEmpty')) {
                $.jGrowl("Mensaje Aprobado es campo requerido", {
                    header: 'Validación'
                });
                return;
            }
            if ($('#parameter_edit_message_disapproved').summernote('isEmpty')) {
                $.jGrowl("Mensaje Desaprobado es campo requerido", {
                    header: 'Validación'
                });
                return;
            }
            l_formdata.append('CarrierCourseId', $("#parameter_edit_course").val());
            l_formdata.append('WaitTimeCloseSession', $("#parameter_edit_time").val());
            l_formdata.append('MessageFinalized', $('#parameter_edit_message').summernote('code'));
            l_formdata.append('MessageApproved', $('#parameter_edit_message_approved').summernote('code'));
            l_formdata.append('MessageDisapproved', $('#parameter_edit_message_disapproved').summernote('code'));
            l_formdata.append('ImageFinalizedFile.File', typeof $("#parameter_edit_image")[0].files[0] == 'undefined' ? null : $("#parameter_edit_image")[0].files[0]);
            l_formdata.append('ImageFinalizedFile.WasRetrieved', $("#parameter_edit_image").data("retrieved"));
            l_formdata.append('ImageFinalizedFile.WasChoosed', !$("#parameter_edit_image").data("retrieved"));
            l_formdata.append('ImageFinalizedName', $("#parameter_edit_image").data("retrievedName"));
            $.daAlert({
                content: {
                    type: "text",
                    value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "save",
                        fn: function () {
                            $.ajax({
                                data: l_formdata,
                                url: '/Parameter/Edit',
                                type: 'post',
                                dataType: 'json',
                                contentType: false,
                                processData: false,
                                beforeSend: _beforeSend,
                                success: _success
                            });
                            function _beforeSend() {
                                gf_addLoadingTo($("#parameter_edit_save"));
                            }
                            function _success(r) {
                                if (parseInt(r.status) == 1000) {
                                    $.jGrowl(r.response, {
                                        header: 'Información',
                                        type: 'primary'
                                    });
                                    location.href = '/Parameter/Edit';
                                } else {
                                    $.jGrowl(r.response, {
                                        header: 'Error',
                                        type: 'danger',
                                        clipboard: r.exception
                                    });
                                    gf_removeLoadingTo($("#parameter_edit_save"));
                                }
                            }
                            return true;
                        }
                    }
                ],
                maximizable: false,
                title: "Confirmación"
            });
        });
        //init
        $('#parameter_edit_course, #parameter_edit_time, #parameter_edit_image_display').data('PresaveRequired', true);
        $("#parameter_edit_time").data({
            "PresaveList": "integer",
            "PresaveJson": '{"min":0}'
        });
        gf_Helpers_CourseAreaDropDown({
            $: $("#parameter_edit_course"),
            callback: function (o) {
                o.addClass("selectpicker").val(p_model.CarrierCourseId).selectpicker();
            }
        });
    }(gf_getClone(ngon.Model)));
});
/////////////////////////////////////////////////////////////////////////ParameterController.cs
using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class ParameterController : Controller
    {
        private readonly string StorageUrl = Helpers.GetAppSettings("Azure_StorageUrl_Prueba");
        // GET: Parameter
        [AppAuthorize]
        [AppLog]
        public ActionResult Edit()
        {
            Bus_List l_bus = new Bus_List();
            DataTable l_records = l_bus.RetrieveParameter();
            Parameter_Entity l_model = new Parameter_Entity
            {
                CarrierCourseId = l_records.Rows[0]["Codi_Curs_Transportista"].ToString(),
                WaitTimeCloseSession = l_records.Rows[0]["Tiempo_Cerrar_Session"].ToString(),
                ImageFinalizedName = l_records.Rows[0]["Imagen_Fin_Examen"].ToString(),
                MessageFinalized = l_records.Rows[0]["Mensaje_Fin_Examen"].ToString(),
                MessageApproved = l_records.Rows[0]["Mensaje_Aprobado_Fin_Examen"].ToString(),
                MessageDisapproved = l_records.Rows[0]["Mensaje_Desaprobado_Fin_Examen"].ToString(),
                ImageFinalizedUrl = StorageUrl + Convert.ToString(l_records.Rows[0]["Imagen_Fin_Examen"]),
            };

            Helpers.ClearDataTable(l_records);
            l_bus = null;

            ViewBag.NGon.Model = l_model;
            return View(l_model);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Parameter_Entity p_Entity)
        {
            Bus_Crud l_bus;
            AzureStorageHelpers AzureSM = null;
            DataTable l_dt = null;
            try
            {
                l_bus = new Bus_Crud();
                AzureSM = new AzureStorageHelpers("Azure_ContainerName_Prueba", BlobContainerPublicAccessType.Blob);
                //save img
                if (p_Entity.ImageFinalizedFile != null && p_Entity.ImageFinalizedFile.File != null)
                {
                    p_Entity.ImageFinalizedFile.Name = DateTime.Now.ToString("ddMMyyyyHHmmssffffff") + "-" + p_Entity.ImageFinalizedFile.File.FileName;
                    p_Entity.ImageFinalizedFile.Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.ImageFinalizedFile.File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.ImageFinalizedFile.File.ContentLength) + "}";
                    //upload to storage
                    AzureSM.UploadBlob(p_Entity.ImageFinalizedFile.Name, p_Entity.ImageFinalizedFile.File.InputStream);
                    p_Entity.ImageFinalizedFile.Uploaded = true;
                }
                if (p_Entity.ImageFinalizedFile.WasRetrieved)
                {
                    p_Entity.ImageFinalizedFile.Name = p_Entity.ImageFinalizedName;
                }
                //save in db
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                l_dt = l_bus.UpdateParameter(p_Entity);
                if (l_dt.Rows[0]["Value"].ToString() == "1000")
                {
                    try
                    {
                        //remove old img
                        if (!p_Entity.ImageFinalizedFile.WasRetrieved)
                        {
                            AzureSM.DeleteBlob(p_Entity.ImageFinalizedName);
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                else
                {
                    try
                    {
                        //remove img
                        if (p_Entity.ImageFinalizedFile != null && p_Entity.ImageFinalizedFile.Uploaded)
                        {
                            AzureSM.DeleteBlob(p_Entity.ImageFinalizedFile.Name);
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetMessageCrud(l_dt);
            }
            catch (Exception e)
            {
                if (AzureSM != null)
                {
                    try
                    {
                        //remove img
                        if (p_Entity.ImageFinalizedFile != null && p_Entity.ImageFinalizedFile.Uploaded)
                        {
                            AzureSM.DeleteBlob(p_Entity.ImageFinalizedFile.Name);
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetException(e);
            }
            finally
            {
                l_bus = null;
                if (AzureSM != null)
                {
                    AzureSM.Clear();
                    AzureSM = null;
                }
                if (l_dt != null)
                {
                    Helpers.ClearDataTable(l_dt);
                }
            }
        }
    }
}
///////////////////////////////////////////////////////////Parameter_Entity.cs
	public class Parameter_Entity : Audit_Entity
    {
        public string CarrierCourseId { get; set; }
        public string WaitTimeCloseSession { get; set; }
        public int iWaitTimeCloseSession { get; set; }
        public string MessageFinalized { get; set; }
        public string MessageApproved { get; set; }
        public string MessageDisapproved { get; set; }
        public File_Entity ImageFinalizedFile { get; set; }
        public string ImageFinalizedName { get; set; }
        public string ImageFinalizedUrl { get; set; }
    }