﻿"use strict";
$("document").ready(function () {
    (function (p_model) {
        var l_template = {
            sId: fn_create_sid(),
            oImage: {
                sBase64: '',
                oFile: null
            },
            oSize: {
                fWidthPt: 0,
                fHeightPt: 0
            },
            sOrientation: 'landscape',
            sPageFormat: 'a4',
            oChildren: []
        };
        var l_page_formats = {
            'a0': [2383.94, 3370.39],
            'a1': [1683.78, 2383.94],
            'a2': [1190.55, 1683.78],
            'a3': [841.89, 1190.55],
            'a4': [595.28, 841.89],
            'a5': [419.53, 595.28],
            'a6': [297.64, 419.53],
            'a7': [209.76, 297.64],
            'a8': [147.40, 209.76],
            'a9': [104.88, 147.40],
            'a10': [73.70, 104.88],
            'b0': [2834.65, 4008.19],
            'b1': [2004.09, 2834.65],
            'b2': [1417.32, 2004.09],
            'b3': [1000.63, 1417.32],
            'b4': [708.66, 1000.63],
            'b5': [498.90, 708.66],
            'b6': [354.33, 498.90],
            'b7': [249.45, 354.33],
            'b8': [175.75, 249.45],
            'b9': [124.72, 175.75],
            'b10': [87.87, 124.72],
            'c0': [2599.37, 3676.54],
            'c1': [1836.85, 2599.37],
            'c2': [1298.27, 1836.85],
            'c3': [918.43, 1298.27],
            'c4': [649.13, 918.43],
            'c5': [459.21, 649.13],
            'c6': [323.15, 459.21],
            'c7': [229.61, 323.15],
            'c8': [161.57, 229.61],
            'c9': [113.39, 161.57],
            'c10': [79.37, 113.39],
            'dl': [311.81, 623.62],
            'letter': [612, 792],
            'government-letter': [576, 756],
            'legal': [612, 1008],
            'junior-legal': [576, 360],
            'ledger': [1224, 792],
            'tabloid': [792, 1224],
            'credit-card': [153, 243]
        };
        var l_no_image_base64 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIABAMAAAAGVsnJAAAAA3NCSVQICAjb4U/gAAAACXBIWXMAAA3XAAAN1wFCKJt4AAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAA9QTFRF////4hsb4hsbmZmZ4hsbWMYoTwAAAAN0Uk5TAAgQLxhesgAAAp9JREFUeNrt27ENwyAURVFnhGwQeQVW8P4zpUUWilLY+oZ3bg3N+YgKtnZqG9QGLbMOAAAAAAAAAAAAAAAAAAAACARIDQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEgCIAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADI4AEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAID5AEgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMngAAAAAAADgwt5dkQBHFwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADgToA6iVqABxwFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABmBngdF/cBAADATD0e4O7LHwAAAAAAAAAAAAAAAAAAAAAAAACAMIA/8nscAAAAAAAAAAAAAAAAAAAAAAAAAIAKAI+lAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgHUB6vJSFAAAAAAAAMgF2LsiAWoDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABRACQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGTwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwHwAJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAYPAAAAAAAArAvQBv3auMo6AAAAAAAAAAAAAAAAAAAAIBDgC7tK/ATcy9KHAAAAAElFTkSuQmCC';
        //events
        $('#certificate_create_format').change(function () {
            fn_resize_board();
        });
        $('#certificate_create_orientation').change(function () {
            fn_resize_board();
        });
        $('#certificate_create_btn_img').click(function (evt) {
            evt.preventDefault();
            $('#certificate_create_file').trigger('click');
        });
        $('#certificate_create_file').change(function () {
            fn_change_input_image($(this), $('#certificate_create_board'), l_template);
        });
        $('#certificate_create_board').on('click', 'button[data-action=remove]', function (evt) {
            evt.preventDefault();
            fn_remove_child_by_sid($(this).attr('data-sid'));
            $(this).closest('div[data-object]').remove();
        });
        $('#certificate_create_board').on('click', 'button[data-action=image]', function (evt) {
            evt.preventDefault();
            $(this).closest('div[data-object=image]').find('input[data-input=file]').trigger('click');
        });
        $('#certificate_create_board').on('change', 'input[data-input=file]', function () {
            var l_object = fn_get_object_by_sid($(this).attr('data-sid')), l_board = $(this).closest('div[data-object=image]');
            fn_change_input_image($(this), l_board, l_object);
        });
        $('#certificate_create_add_image').click(function (evt) {
            evt.preventDefault();
            l_template.oChildren.push(fn_create_child('image'));
            fn_draw_children();
        });
        $('#certificate_create_add_name').click(function (evt) {
            evt.preventDefault();
            l_template.oChildren.push(fn_create_child('text', { oData: { sKey: 'person_name' }, oSize: { bFullWidth: true }, oText: { sText: 'Nombre del participante' } }));
            fn_draw_children();
        });
        $('#certificate_create_add_course').click(function (evt) {
            evt.preventDefault();
            l_template.oChildren.push(fn_create_child('text', { oData: { sKey: 'course_name' }, oSize: { bFullWidth: true }, oText: { sText: 'Nombre del curso' } }));
            fn_draw_children();
        });
        $('#certificate_create_add_score').click(function (evt) {
            evt.preventDefault();
            l_template.oChildren.push(fn_create_child('text', { oData: { sKey: 'test_score' }, oText: { sText: '00.00' } }));
            fn_draw_children();
        });
        $('#certificate_create_add_date').click(function (evt) {
            evt.preventDefault();
            l_template.oChildren.push(fn_create_child('text', { oData: { sKey: 'approved_date', sDateFormat: p_model.DateFormat }, oText: { sText: p_model.DateFormat } }));
            fn_draw_children();
        });
        $('#certificate_create_download_preview').click(function (evt) {
            evt.preventDefault();
            if (!fn_validate_template()) {
                return;
            }
            //pdf
            //[orientation One of "portrait" or "landscape"(or shortcuts "p"(Default), "l")]
            //[unit Measurement unit to be used when coordinates are specified. One of "pt" (points), "mm" (Default), "cm", "in"]
            //[format One of 'a3', 'a4' (Default),'a5' ,'letter' ,'legal']
            var l_pdf = new jsPDF(l_template.sOrientation, "pt", l_template.sPageFormat);
            //add fonts
            //=====> ShiningTimes
            l_pdf.addFileToVFS(jspdfCustomfonts.ShiningTimes.file, jspdfCustomfonts.ShiningTimes.base64);
            l_pdf.addFont(jspdfCustomfonts.ShiningTimes.file, jspdfCustomfonts.ShiningTimes.name, "normal");
            //=====> SoDamnBeautiful
            l_pdf.addFileToVFS(jspdfCustomfonts.SoDamnBeautiful.file, jspdfCustomfonts.SoDamnBeautiful.base64);
            l_pdf.addFont(jspdfCustomfonts.SoDamnBeautiful.file, jspdfCustomfonts.SoDamnBeautiful.name, "normal");
            //=====> Aliquest
            l_pdf.addFileToVFS(jspdfCustomfonts.Aliquest.file, jspdfCustomfonts.Aliquest.base64);
            l_pdf.addFont(jspdfCustomfonts.Aliquest.file, jspdfCustomfonts.Aliquest.name, "normal");
            //=====> AnthoniSignature
            l_pdf.addFileToVFS(jspdfCustomfonts.AnthoniSignature.file, jspdfCustomfonts.AnthoniSignature.base64);
            l_pdf.addFont(jspdfCustomfonts.AnthoniSignature.file, jspdfCustomfonts.AnthoniSignature.name, "normal");
            //=====> Yaty
            l_pdf.addFileToVFS(jspdfCustomfonts.Yaty.file, jspdfCustomfonts.Yaty.base64);
            l_pdf.addFont(jspdfCustomfonts.Yaty.file, jspdfCustomfonts.Yaty.name, "normal");
            //=====> SilverstainSignature
            l_pdf.addFileToVFS(jspdfCustomfonts.SilverstainSignature.file, jspdfCustomfonts.SilverstainSignature.base64);
            l_pdf.addFont(jspdfCustomfonts.SilverstainSignature.file, jspdfCustomfonts.SilverstainSignature.name, "normal");
            //=====> Abecedary
            l_pdf.addFileToVFS(jspdfCustomfonts.Abecedary.file, jspdfCustomfonts.Abecedary.base64);
            l_pdf.addFont(jspdfCustomfonts.Abecedary.file, jspdfCustomfonts.Abecedary.name, "normal");
            //=====> Bintanghu
            l_pdf.addFileToVFS(jspdfCustomfonts.Bintanghu.file, jspdfCustomfonts.Bintanghu.base64);
            l_pdf.addFont(jspdfCustomfonts.Bintanghu.file, jspdfCustomfonts.Bintanghu.name, "normal");
            //=====> Hatachi
            l_pdf.addFileToVFS(jspdfCustomfonts.Hatachi.file, jspdfCustomfonts.Hatachi.base64);
            l_pdf.addFont(jspdfCustomfonts.Hatachi.file, jspdfCustomfonts.Hatachi.name, "normal");
            //add board
            l_pdf.addImage(l_template.oImage.sBase64, 'JPEG', 0, 0, l_template.oSize.fWidthPt, l_template.oSize.fHeightPt);
            //add children
            for (var i in l_template.oChildren) {
                if (l_template.oChildren[i].sType == 'image') {
                    l_pdf.addImage(l_template.oChildren[i].oImage.sBase64, 'PNG', l_template.oChildren[i].oPosition.fLeftPt, l_template.oChildren[i].oPosition.fTopPt + 0.5, l_template.oChildren[i].oSize.fWidthPt, l_template.oChildren[i].oSize.fHeightPt);
                } else if (l_template.oChildren[i].sType == 'text') {
                    l_pdf.setFont(l_template.oChildren[i].oFont.sFamily);
                    l_pdf.setFontSize(l_template.oChildren[i].oFont.iSizePt);
                    l_pdf.setFontStyle(l_template.oChildren[i].oFont.sStyle);
                    l_pdf.setTextColor(l_template.oChildren[i].oFont.sColor);
                    if (l_template.oChildren[i].oSize.bFullWidth) {
                        l_pdf.customText(l_template.oChildren[i].oText.sText, { align: "center" }, l_template.oChildren[i].oPosition.fLeftPt, (l_template.oChildren[i].oPosition.fTopPt + l_template.oChildren[i].oSize.fHeightPt - 4));
                    } else {
                        l_pdf.text(l_template.oChildren[i].oPosition.fLeftPt, (l_template.oChildren[i].oPosition.fTopPt + l_template.oChildren[i].oSize.fHeightPt - 4), l_template.oChildren[i].oText.sText);
                    }
                }
            }
            //save
            l_pdf.save('Vista Previa.pdf');
        });
        $('body').on('click', 'button[data-action=update]', function (evt) {
            evt.preventDefault();
            var l_object = fn_get_object_by_sid($(this).attr('data-sid')), l_settings = $(this).closest('table[data-object=settings]');
            l_object.oFont.sFamily = l_settings.find('select[name=fontfamily]').val();
            l_object.oFont.sWebFamily = l_settings.find('select[name=fontfamily] option:selected').attr('data-web');
            l_object.oFont.iSizePt = parseFloat(l_settings.find('input[name=fontsize]').val());
            l_object.oFont.sColor = l_settings.find('input[name=color]').val();
            l_object.oSize.fHeightPt = l_object.oFont.iSizePt;
            fn_update_children_text(l_object);
        });
        $('#certificate_create_form').submit(function (evt) {
            evt.preventDefault();
        });
        $('#certificate_create_save').click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_psave = gf_presaveForm($("#certificate_create_form"));
            if (l_psave.hasError) {
                $.jGrowl("Ingrese correctamente los datos del formulario", {
                    header: 'Validación'
                });
                return;
            }
            if (!fn_validate_template()) {
                return;
            }
            fn_resize_children(gf_getClone(l_template));
        });
        //fn
        function fn_validate_template() {
            var l_success = true;
            if (!l_template.oImage.sBase64 || l_template.oImage.sBase64 == '') {
                $.jGrowl("Ingrese un Fondo para la Plantilla", {
                    header: 'Validación'
                });
                l_success = false;
            }
            if (l_success) {
                for (var i in l_template.oChildren) {
                    if (l_template.oChildren[i].sType == 'image') {
                        if (!l_template.oChildren[i].oImage.sBase64 || l_template.oChildren[i].oImage.sBase64 == '') {
                            $.jGrowl("Existe Imágenes en blanco", {
                                header: 'Validación'
                            });
                            l_success = false;
                            break;
                        }
                    }
                }
            }
            return l_success;
        }
        function fn_resize_board() {
            l_template.sPageFormat = $('#certificate_create_format').val();
            l_template.sOrientation = $('#certificate_create_orientation').val();
            fn_initialize_template();
        }
        function fn_change_input_image(p_this, p_board, p_object) {
            var l_validate = gf_fileValidation({
                fileInput: p_this,
                showPreview: false,
                fnError: function () {
                    $.jGrowl("Solo puede seleccionar archivos tipo: jpg, jpeg, png, bmp", {
                        header: "Validación"
                    });
                }
            });
            if (l_validate) {
                p_object.oImage.oFile = p_this[0].files[0];
                var l_reader = new FileReader();
                l_reader.onload = function (e) {
                    p_object.oImage.sBase64 = e.target.result;
                    p_board.css('background-image', 'url("' + p_object.oImage.sBase64 + '")');
                };
                l_reader.readAsDataURL(p_object.oImage.oFile);
            } else {
                p_object.oImage.oFile = null;
                p_object.oImage.sBase64 = '';
                p_board.css('background-image', 'url("' + l_no_image_base64 + '")');
            }
        }
        function fn_create_child(p_type, p_default) {
            var l_child = {
                sId: fn_create_sid(),
                sType: p_type,
                oData: {
                    sKey: '',
                    sDateFormat: ''
                },
                bDrawn: false,
                bResize: false,
                oFont: {
                    sFamily: '',
                    sWebFamily: '',
                    sStyle: '',
                    iSizePt: 0,
                    sColor: ''
                },
                oText: {
                    sText: ''
                },
                oImage: {
                    sBase64: '',
                    oFile: null
                },
                oPosition: {
                    fLeftPt: 0,
                    fTopPt: 0
                },
                oSize: {
                    bFullWidth: false,
                    fWidthPt: 0,
                    fHeightPt: 0
                }
            };
            l_child = $.extend(true, l_child, fn_initialize_child(p_type));
            if (p_default) {
                l_child = $.extend(true, l_child, p_default);
            }
            return l_child;
        }
        function fn_initialize_child(p_type) {
            if (p_type == 'image') {
                return {
                    oSize: {
                        fWidthPt: fn_px_to_pt(100),
                        fHeightPt: fn_px_to_pt(100)
                    },
                    oPosition: {
                        fLeftPt: fn_px_to_pt(3),
                        fTopPt: fn_px_to_pt(24)
                    }
                };
            } else if (p_type == 'text') {
                return {
                    oFont: {
                        sFamily: 'Times',
                        sWebFamily: 'Times',
                        sStyle: 'normal',
                        iSizePt: 22,
                        sColor: '#000'
                    },
                    oText: {
                        sText: 'Mi texto'
                    },
                    oSize: {
                        bFullWidth: false,
                        fHeightPt: 22
                    }
                };
            }
        }
        function fn_px_to_pt(p_px) {
            return (72 / 96) * p_px;
        }
        function fn_pt_to_px(p_pt) {
            return (p_pt / 72) * 96;
        }
        function fn_create_sid() {
            return moment().format('YYYY_MM_DD_HH_mm_ss_SSS');
        }
        function fn_get_object_by_sid(p_sid) {
            var l_return = null;
            if (l_template.sId == p_sid) {
                l_return = l_template;
            } else {
                for (var i in l_template.oChildren) {
                    if (l_template.oChildren[i].sId == p_sid) {
                        l_return = l_template.oChildren[i];
                        break;
                    }
                }
            }
            return l_return;
        }
        function fn_remove_child_by_sid(p_sid) {
            var l_idx = null;
            for (var i = 0; i < l_template.oChildren.length; i++) {
                if (l_template.oChildren[i].sId == p_sid) {
                    l_idx = i;
                    break;
                }
            }
            if (l_idx != null) {
                l_template.oChildren.splice(l_idx, 1);
            }
        }
        function fn_initialize_template() {
            l_template.oSize.fWidthPt = l_template.sOrientation == 'landscape' ? l_page_formats[l_template.sPageFormat][1] : l_page_formats[l_template.sPageFormat][0];
            l_template.oSize.fHeightPt = l_template.sOrientation == 'landscape' ? l_page_formats[l_template.sPageFormat][0] : l_page_formats[l_template.sPageFormat][1];
            //
            $('#certificate_create_board')
                .css({
                    'width': l_template.oSize.fWidthPt + 'pt',
                    'height': l_template.oSize.fHeightPt + 'pt',
                    'background-image': 'url("' + l_template.oImage.sBase64 + '")'
                });
        }
        function fn_draw_children() {
            for (var i in l_template.oChildren) {
                if (!l_template.oChildren[i].bDrawn) {
                    if (l_template.oChildren[i].sType == 'image') {
                        fn_draw_children_image(l_template.oChildren[i]);
                    } else if (l_template.oChildren[i].sType == 'text') {
                        fn_draw_children_text(l_template.oChildren[i]);
                    }
                    l_template.oChildren[i].bDrawn = true;
                }
            }
        }
        function fn_draw_children_text(p_child) {
            var l_object = $('<div />'), l_btn = $('<button />'), l_remove = $('<button />');
            l_btn
                .attr({
                    'data-action': 'settings',
                    'data-sid': p_child.sId
                })
                .addClass('app-btn app-btn-default cls-1903070846')
                .html(`<i class="fa fa-gear pointer-events-none m-0"></i>`);
            l_remove
                .attr({
                    'data-action': 'remove',
                    'data-sid': p_child.sId
                })
                .addClass('btn btn-danger btn-raised cls-1903070857')
                .html(`<i class="fa fa-close pointer-events-none m-0"></i>`);
            l_object
                .attr({
                    'data-object': 'text',
                    'data-sid': p_child.sId
                })
                .css({
                    'font-family': p_child.oFont.sWebFamily,
                    'font-size': p_child.oFont.iSizePt + 'pt',
                    'color': p_child.oFont.sColor
                })
                .addClass('cls-1903070509 ' + (p_child.oSize.bFullWidth ? 'cls-1903071036' : ''))
                .text(p_child.oText.sText);
            l_object.append(l_btn, l_remove);
            $('#certificate_create_board').append(l_object);
            //draggable
            l_object.draggable({
                containment: 'parent',
                stop: function (event, ui) {
                    var l_object = fn_get_object_by_sid(ui.helper.attr('data-sid'));
                    l_object.oPosition.fTopPt = fn_px_to_pt(ui.position.top)
                    l_object.oPosition.fLeftPt = fn_px_to_pt(ui.position.left);
                }
            });
            //set position
            l_object
                .css({
                    'top': p_child.oPosition.fTopPt + 'pt',
                    'left': p_child.oPosition.fLeftPt + 'pt'
                });
            //popover
            l_btn.popover({
                container: 'body',
                placement: 'right',
                content: `<table data-object="settings" data-sid="` + p_child.sId + `">
                        <tbody>
                            <tr>
                                <td>Fuente </td>
                                <td>:</td>
                                <td>
                                    <select name="fontfamily" class="w-100 cls-1901182309">
                                        <option value="Abecedary" data-web="jsPDF_Abecedary">Abecedary</option>
                                        <option value="Aliquest" data-web="jsPDF_Aliquest">Aliquest</option>
                                        <option value="AnthoniSignature" data-web="jsPDF_AnthoniSignature">AnthoniSignature</option>
                                        <option value="Bintanghu" data-web="jsPDF_Bintanghu">Bintanghu</option>
                                        <option value="Courier" data-web="Courier">Courier</option>
                                        <option value="Hatachi" data-web="jsPDF_Hatachi">Hatachi</option>
                                        <option value="Helvetica" data-web="Helvetica">Helvetica</option>
                                        <option value="ShiningTimes" data-web="jsPDF_ShiningTimes">ShiningTimes</option>
                                        <option value="SilverstainSignature" data-web="jsPDF_SilverstainSignature">SilverstainSignature</option>
                                        <option value="SoDamnBeautiful" data-web="jsPDF_SoDamnBeautiful">SoDamnBeautiful</option>
                                        <option value="Times" data-web="Times">Times</option>                                   
                                        <option value="Yaty" data-web="jsPDF_Yaty">Yaty</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Tamaño </td>
                                <td>:</td>
                                <td>
                                    <input name="fontsize" class="w-100 cls-1901182309" type="number" min="1" />
                                </td>
                            </tr>
                            <tr>
                                <td>Color </td>
                                <td>:</td>
                                <td>
                                    <input name="color" class="w-100 cls-1901182309" type="color" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" class="text-right">
                                    <button data-action="update" data-sid="` + p_child.sId + `" class="app-btn app-btn-default mt-05">Actualizar</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>`,
                html: true
            });
            l_btn.on('shown.bs.popover', function (e) {
                var l_object = fn_get_object_by_sid($(this).attr('data-sid')), l_settings = $('table[data-object=settings][data-sid=' + $(this).attr('data-sid') + ']');
                l_settings.find('select[name=fontfamily]').val(l_object.oFont.sFamily);
                l_settings.find('input[name=fontsize]').val(l_object.oFont.iSizePt);
                l_settings.find('input[name=color]').val(l_object.oFont.sColor);
            });
        }
        function fn_draw_children_image(p_child) {
            var l_object = $('<div />'), l_input = $('<input />'), l_btn = $('<button />'), l_remove = $('<button />');
            l_input
                .attr({
                    'type': 'file',
                    'data-input': 'file',
                    'data-sid': p_child.sId
                })
                .addClass('d-none');
            l_btn
                .attr({
                    'data-action': 'image',
                    'data-sid': p_child.sId
                })
                .addClass('app-btn app-btn-default cls-1903070846')
                .html(`<i class="fa fa-file-image-o pointer-events-none m-0"></i>`);
            l_remove
                .attr({
                    'data-action': 'remove',
                    'data-sid': p_child.sId
                })
                .addClass('btn btn-danger btn-raised cls-1903070857')
                .html(`<i class="fa fa-close pointer-events-none m-0"></i>`);
            l_object
                .attr({
                    'data-object': 'image',
                    'data-sid': p_child.sId
                }).css({
                    'background-image': 'url("' + (p_child.oImage.sBase64 == '' ? l_no_image_base64 : p_child.oImage.sBase64) + '")'
                })
                .addClass('cls-1903070509 cls-1903070515');
            l_object.append(l_input, l_btn, l_remove);
            $('#certificate_create_board').append(l_object);
            //draggable
            l_object.draggable({
                containment: 'parent',
                stop: function (event, ui) {
                    var l_object = fn_get_object_by_sid(ui.helper.attr('data-sid'));
                    l_object.oPosition.fTopPt = fn_px_to_pt(ui.position.top);
                    l_object.oPosition.fLeftPt = fn_px_to_pt(ui.position.left);
                }
            });
            //set position
            l_object
                .css({
                    'top': p_child.oPosition.fTopPt + 'pt',
                    'left': p_child.oPosition.fLeftPt + 'pt'
                });
            //resizable
            l_object.resizable({
                minWidth: 50,
                minHeight: 50,
                stop: function (event, ui) {
                    var l_object = fn_get_object_by_sid(ui.helper.attr('data-sid'));
                    l_object.oSize.fWidthPt = fn_px_to_pt(ui.size.width);
                    l_object.oSize.fHeightPt = fn_px_to_pt(ui.size.height);
                }
            });
            //set size
            l_object
                .css({
                    'width': p_child.oSize.fWidthPt + 'pt',
                    'height': p_child.oSize.fHeightPt + 'pt'
                });
        }
        function fn_update_children_text(p_child) {
            $('#certificate_create_board div[data-object=text][data-sid=' + p_child.sId + ']').css({
                'font-family': p_child.oFont.sWebFamily,
                'font-size': p_child.oFont.iSizePt + 'pt',
                'color': p_child.oFont.sColor
            });
        }
        function fn_resize_children(p_template) {
            var l_next = null;
            for (var i in p_template.oChildren) {
                if (!p_template.oChildren[i].bResize) {
                    if (p_template.oChildren[i].sType == 'image') {
                        l_next = p_template.oChildren[i];
                        break;
                    } else {
                        p_template.oChildren[i].bResize = true;
                    }
                }
            }
            if (l_next) {
                (function (p_child, p_callback, p_template) {
                    var l_image = new Image();
                    l_image.onload = function (evt) {
                        var l_canvas = document.createElement('canvas');
                        l_canvas.width = fn_pt_to_px(p_child.oSize.fWidthPt);
                        l_canvas.height = fn_pt_to_px(p_child.oSize.fHeightPt);
                        l_canvas.getContext('2d').drawImage(l_image, 0, 0, fn_pt_to_px(p_child.oSize.fWidthPt), fn_pt_to_px(p_child.oSize.fHeightPt));
                        p_child.oImage.sBase64 = l_canvas.toDataURL('image/png');
                        gf_base64_to_file(p_child.oImage.sBase64, (p_child.oImage.oFile ? p_child.oImage.oFile.name.substring(p_child.oImage.oFile.name.length - 15) : 'test.png'), 'image/png')
                            .then(function (p_file) {
                                p_child.oImage.oFile = p_file;
                                p_child.bResize = true;
                                p_callback(p_template);
                            });
                    }
                    l_image.src = p_child.oImage.sBase64;
                }(l_next, fn_resize_children, p_template));
            } else {
                fn_save(p_template);
            }
        }
        function fn_save(p_template) {
            var l_formdata = new FormData();
            //clean base64
            p_template.oImage.sBase64 = '';
            p_template.sId = p_template.sId + (p_template.oImage.oFile ? p_template.oImage.oFile.name.substring(p_template.oImage.oFile.name.length - 15) : '');
            for (var i in p_template.oChildren) {
                if (p_template.oChildren[i].sType == 'image') {
                    p_template.oChildren[i].oImage.sBase64 = '';
                    if (!gf_isImage(p_template.oChildren[i].sId)) {
                        p_template.oChildren[i].sId = p_template.oChildren[i].sId + p_template.oChildren[i].oImage.oFile.name.substring(p_template.oChildren[i].oImage.oFile.name.length - 15);
                    }
                }
            }
            //
            l_formdata.append('Description', $("#certificate_create_description").val().trim());
            l_formdata.append('Status', $("#certificate_create_status").is(":checked") ? "A" : "I");
            l_formdata.append('Files[0].File', p_template.oImage.oFile);
            l_formdata.append('Files[0].Name', p_template.sId);
            for (var i = 0; i < p_template.oChildren.length; i++) {
                l_formdata.append('Files[' + (i + 1) + '].File', p_template.oChildren[i].oImage.oFile);
                l_formdata.append('Files[' + (i + 1) + '].Name', p_template.oChildren[i].sId);
            }
            //set null
            p_template.oImage.oFile = null;
            for (var i = 0; i < p_template.oChildren.length; i++) {
                p_template.oChildren[i].oImage.oFile = null;
                p_template.oChildren[i].bDrawn = false;
                p_template.oChildren[i].bResize = false;
            }
            //
            l_formdata.append('CertificateJSON', JSON.stringify(p_template));
            $.daAlert({
                content: {
                    type: "text",
                    value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "save",
                        fn: function () {
                            $.ajax({
                                data: l_formdata,
                                url: '/Certificate/Create',
                                type: 'post',
                                dataType: 'json',
                                contentType: false,
                                processData: false,
                                beforeSend: _beforeSend,
                                success: _success
                            });
                            function _beforeSend() {
                                gf_addLoadingTo($("#certificate_create_save"));
                            }
                            function _success(r) {
                                if (parseInt(r.status) == 1000) {
                                    $.jGrowl(r.response, {
                                        header: 'Información',
                                        type: 'primary'
                                    });
                                    location.href = '/Certificate/Index';
                                } else {
                                    $.jGrowl(r.response, {
                                        header: 'Error',
                                        type: 'danger',
                                        clipboard: r.exception
                                    });
                                    gf_removeLoadingTo($("#certificate_create_save"));
                                }
                            }
                            return true;
                        }
                    }
                ],
                maximizable: false,
                title: "Confirmación"
            });
        }
        //init
        $("#certificate_create_description").data({ PresaveRequired: true, PresaveJson: '{"maxLength":200}' });
        $('body').click(function (e) {
            if ($(e.target).closest('button[data-action=settings]').length == 0 && $(e.target).closest('div.popover').length == 0) {
                $('div.popover').popover('hide');
            }
        });
        $('#certificate_create_format').val(l_template.sPageFormat).addClass('selectpicker').selectpicker();
        $('#certificate_create_orientation').val(l_template.sOrientation).addClass('selectpicker').selectpicker();
        fn_initialize_template();
        //jsPDF
        (function (API) {
            API.customText = function (txt, options, x, y) {
                options = options || {};
                /* Use the options align property to specify desired text alignment
                 * Param x will be ignored if desired text alignment is 'center'.
                 * Usage of options can easily extend the function to apply different text 
                 * styles and sizes 
                */
                if (options.align == "center") {
                    // Get current font size
                    var fontSize = this.internal.getFontSize();
                    // Get page width
                    var pageWidth = this.internal.pageSize.width;
                    // Get the actual text's width
                    /* You multiply the unit width of your string by your font size and divide
                     * by the internal scale factor. The division is necessary
                     * for the case where you use units other than 'pt' in the constructor
                     * of jsPDF.
                    */
                    var txtWidth = this.getStringUnitWidth(txt) * fontSize / this.internal.scaleFactor;
                    // Calculate text's x coordinate
                    x = (pageWidth - txtWidth) / 2;
                }
                // Draw text at x,y
                this.text(txt, x, y);
            }
        })(jsPDF.API);
    }(gf_getClone(ngon.Model)));
});