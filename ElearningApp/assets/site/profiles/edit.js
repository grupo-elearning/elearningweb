﻿"use strict";
$("document").ready(function () {
    var l_dataTable, l_current_sipe = null, l_sipe_provider = gf_Helpers_GetParameterByKey('CODI_SIPE_ELEARNING_PROVEEDOR');
    $("#profiles_edit_retrieve").click(function (evt) {
        evt.preventDefault();
        var l_sipe = $("#profiles_edit_profile").val();
        if (!gf_isValue(l_sipe)) {
            $.jGrowl("Seleccione un Perfil", {
                header: "Validación"
            });
            return;
        }
        l_current_sipe = l_sipe;
        fn_datatable(l_sipe);
    });
    $("#profiles_edit_add_item").click(function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        if (!gf_isValue(l_current_sipe)) {
            $.jGrowl("Seleccione un Perfil", {
                header: "Validación"
            });
            return;
        }
        var l_ids = [], l_rows = l_dataTable.rows().nodes(), l_data;
        $(l_rows).each(function () {
            l_data = l_dataTable.row($(this)).data();
            l_ids.push(gf_isValue(l_data[2]) ? l_data[2] : 0);
        });
        $.daAlert({
            content: {
                type: "ajax",
                value: {
                    url: "/Profiles/Item",
                    data: {
                        'ItemIds': l_ids.toString(),
                        'AppProfileId': l_current_sipe
                    }
                }
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "accept",
                    fn: fn_add_item
                }
            ],
            maximizable: false,
            title: "Seleccionar " + (l_current_sipe == l_sipe_provider ? 'Proveedor' : 'Persona'),
            maxWidth: "700px"
        });
    });
    $("#profiles_edit_table tbody").on('click', 'a[data-action=delete]', function (evt) {
        evt.preventDefault();
        l_dataTable.row($(this).closest("tr")).remove().draw(false);
    });
    $('#profiles_edit_table tbody').on('change', 'input[type=checkbox]', function () {
        $(this).parents('tr').toggleClass("cls-1901271254");
    });
    $("#profiles_edit_save").click(function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        if (l_current_sipe == null) {
            return;
        }
        if (l_current_sipe == l_sipe_provider) {
            var l_param = fn_get_provider_data();
            if (l_param == null) {
                return;
            }
            $.daAlert({
                content: {
                    type: "text",
                    value: `<div class="text-center p-05 pt-1">
                                ¿Está seguro que desea guardar?
                                <br />
                                <div class="alert alert-info alert-light alert-dismissible mt-2 mb-0" role="alert">
                                    <strong><i class="zmdi zmdi-info"></i> ¡Nota!</strong> Solo se actualizará el Correo Electrónico y enviará su respectiva Contraseña a los proveedores seleccionados.
                                </div>
                            </div>`
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: 'custom',
                        text: 'Guardar y Enviar',
                        icon: 'glyphicon glyphicon-send',
                        className: 'btn btn-raised btn-info',
                        fn: function (p, e) {
                            $.ajax({
                                data: JSON.stringify(l_param),
                                url: '/Profiles/EditSend',
                                type: 'post',
                                dataType: 'json',
                                contentType: "application/json; charset=utf-8",
                                beforeSend: _beforeSend,
                                success: _success
                            });
                            function _beforeSend() {
                                gf_addLoadingTo($("#profiles_edit_save"));
                            }
                            function _success(r) {
                                if (parseInt(r.status) == 1000) {
                                    $.jGrowl(r.response, {
                                        header: 'Información',
                                        type: 'primary'
                                    });
                                    fn_datatable(l_current_sipe);
                                } else {
                                    $.jGrowl(r.response, {
                                        header: 'Error',
                                        type: 'danger',
                                        clipboard: r.exception
                                    });
                                }
                                gf_removeLoadingTo($("#profiles_edit_save"));
                            }
                            return true;
                        }
                    }
                ],
                maximizable: false,
                title: "Confirmación"
            });
        } else {
            var l_param = fn_get_data();
            $.daAlert({
                content: {
                    type: "text",
                    value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "save",
                        fn: function () {
                            $.ajax({
                                data: JSON.stringify(l_param),
                                url: '/Profiles/Edit',
                                type: 'post',
                                dataType: 'json',
                                contentType: "application/json; charset=utf-8",
                                beforeSend: _beforeSend,
                                success: _success
                            });
                            function _beforeSend() {
                                gf_addLoadingTo($("#profiles_edit_save"));
                            }
                            function _success(r) {
                                if (parseInt(r.status) == 1000) {
                                    $.jGrowl(r.response, {
                                        header: 'Información',
                                        type: 'primary'
                                    });
                                    fn_datatable(l_current_sipe);
                                } else {
                                    $.jGrowl(r.response, {
                                        header: 'Error',
                                        type: 'danger',
                                        clipboard: r.exception
                                    });
                                }
                                gf_removeLoadingTo($("#profiles_edit_save"));
                            }
                            return true;
                        }
                    }
                ],
                maximizable: false,
                title: "Confirmación"
            });
        }
    });
    //fn
    function fn_get_provider_data() {
        var l_row_data, l_tr, l_error = false, l_return = {
            'AppProfileId': l_current_sipe,
            'PrfRecordId': [],
            'PrfUsername': [],
            'ProviderId': [],
            'ProviderBusinessName': [],
            'ProviderEmail': [],
            'ProviderUsername': [],
            'ProviderPassword': []
        };
        l_dataTable.rows().nodes().to$().each(function () {
            l_row_data = l_dataTable.row($(this)).data();
            l_return.PrfRecordId.push(l_row_data[0]);
            l_return.PrfUsername.push(l_row_data[1]);
            if ($(this).find(':checkbox').is(':checked')) {
                l_return.ProviderId.push(l_row_data[2]);
                l_return.ProviderBusinessName.push(l_row_data[4]);
                l_return.ProviderUsername.push(l_row_data[1]);
                l_return.ProviderPassword.push(l_row_data[5]);
                l_return.ProviderEmail.push($(this).find('input[data-input=email]').val().trim());
            } else {
                l_return.ProviderId.push(0);
                l_return.ProviderBusinessName.push('-');
                l_return.ProviderUsername.push('-');
                l_return.ProviderPassword.push('-');
                l_return.ProviderEmail.push('-');
            }
        });
        //validate email
        for (var i = 0; i < l_return.ProviderId.length; i++) {
            if (l_return.ProviderId[i] != 0) {
                if (!gf_isEmail(l_return.ProviderEmail[i])) {
                    $.jGrowl("El Correo Electrónico del Proveedor '" + l_return.ProviderBusinessName[i] + "' no es válido", {
                        header: 'Validación'
                    });
                    l_error = true;
                    break;
                }
            }
        }
        if (l_error) {
            return null;
        }
        if (l_return.PrfRecordId.length == 0) {
            l_return.PrfRecordId.push(-1);
            l_return.PrfUsername.push('-');
            l_return.ProviderId.push(0);
            l_return.ProviderBusinessName.push('-');
            l_return.ProviderUsername.push('-');
            l_return.ProviderPassword.push('-');
            l_return.ProviderEmail.push('-');
        }
        return l_return;
    }
    function fn_get_data() {
        var l_return = {
            'AppProfileId': l_current_sipe,
            'PrfRecordId': [],
            'PrfUsername': []
        };
        l_dataTable.rows().nodes().to$().each(function () {
            l_return.PrfRecordId.push(l_dataTable.row($(this)).data()[0]);
            l_return.PrfUsername.push(l_dataTable.row($(this)).data()[1]);
        });
        if (l_return.PrfRecordId.length == 0) {
            l_return.PrfRecordId.push(-1);
            l_return.PrfUsername.push('-');
        }
        return l_return;
    }
    function fn_add_item() {
        var l_dt_item, l_rows, l_row_data;
        l_dt_item = $("#profiles_item_table").DataTable();
        l_rows = l_dt_item.rows().nodes();
        if ($(l_rows).find(":checkbox:checked").length == 0) {
            $.jGrowl("Seleccione por lo menos " + (l_current_sipe == l_sipe_provider ? 'un Proveedor' : 'una Persona'), {
                header: 'Validación'
            });
            return false;
        }
        if (l_current_sipe == l_sipe_provider) {
            $(l_rows).find(":checkbox:checked").each(function () {
                l_row_data = l_dt_item.row($(this).closest("tr")).data();
                var l_nrow = l_dataTable.row.add([
                    0,
                    l_row_data[0],
                    l_row_data[1],
                    l_row_data[2],
                    l_row_data[3],
                    l_row_data[4],
                    l_row_data[5]
                ]).draw(false).node();
                $(l_nrow).addClass('cls-1901271254');
                $(l_nrow).find(':checkbox').prop('checked', true);
            });
        } else {
            $(l_rows).find(":checkbox:checked").each(function () {
                l_row_data = l_dt_item.row($(this).closest("tr")).data();
                l_dataTable.row.add([
                    0,
                    l_row_data[0],
                    l_row_data[1],
                    l_row_data[2],
                    l_row_data[3],
                    l_row_data[4],
                    l_row_data[5]
                ]).draw(false);
            });
        }
        return true;
    }
    function fn_datatable(p_sipe) {
        $('#profiles_edit_table thead th[data-object=document_ruc]').text(p_sipe == l_sipe_provider ? 'RUC' : 'Documento');
        $('#profiles_edit_table thead th[data-object=name_businessname]').text(p_sipe == l_sipe_provider ? 'Razón Social' : 'Nombre');
        $('#profiles_edit_legend').css('display', (p_sipe == l_sipe_provider ? 'block' : 'none'));
        $('#profiles_edit_save').html((p_sipe == l_sipe_provider ? `<i class="glyphicon glyphicon-send"></i>  Guardar y Enviar` : `<i class="fa fa-save"></i>  Guardar`));
        l_dataTable = $("#profiles_edit_table").daDataTable({
            "DataTable": {
                "ajax": {
                    "url": "/Profiles/GetList",
                    "type": "post",
                    "data": {
                        "id": p_sipe
                    }
                },
                "aoColumns": [
                    {
                        'data': null,
                        'orderable': false,
                        'searchable': false,
                        "visible": p_sipe == l_sipe_provider,
                        'width': '20px',
                        'className': 'text-center',
                        'mRender': function (o) {
                            return `<div class="checkbox">
                                        <label class="m-0">
                                            <input type="checkbox" /><span class="checkbox-material m-0"><span class="check"></span></span>
                                        </label>
                                    </div>`;
                        }
                    },
                    {
                        "data": 3,
                        "className": "text-center"
                    },
                    {
                        "data": 4,
                        "className": "text-left"
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "visible": p_sipe == l_sipe_provider,
                        "className": "cls-1901202130",
                        "mRender": function (o) {
                            return `<input type="text" maxlength="200" class="form-control text-left bg-white" data-input="email" value="` + o[6] + `" />`;
                        }
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center",
                        "width": "50px",
                        "mRender": function (o) {
                            return `<a href="javascript:void(0)" class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" data-action="delete" title="Eliminar" >
                                        <i class="fa fa-trash m-0"></i>
                                    </a>`;
                        }
                    }
                ],
                "order": [[2, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 10
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": { "column": 4 },
                "noSearch": [0, 3]
            }
        }).Datatable;
    }
    //init
    gf_Helpers_ELearningProfileDropDown({
        $: $("#profiles_edit_profile"),
        callback: function (o) {
            o.addClass("selectpicker").val(null).selectpicker();
        }
    });
    fn_datatable(0);
});