﻿"use strict";
$("document").ready(function () {
    var l_dataTable, l_current_area = null;
    $("#areaprovider_edit_retrieve").click(function (evt) {
        evt.preventDefault();
        var l_area = $("#areaprovider_edit_area").val();
        if (!gf_isValue(l_area)) {
            $.jGrowl("Seleccione una Área", {
                header: "Validación"
            });
            return;
        }
        l_current_area = l_area;
        fn_datatable(l_area);
    });
    $("#areaprovider_edit_add_provider").click(function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        if (!gf_isValue(l_current_area)) {
            $.jGrowl("Seleccione una Área", {
                header: "Validación"
            });
            return;
        }
        var l_ids = [], l_rows = l_dataTable.rows().nodes(), l_data;
        $(l_rows).each(function () {
            l_data = l_dataTable.row($(this)).data();
            l_ids.push(l_data[1]);
        });
        $.daAlert({
            content: {
                type: "ajax",
                value: {
                    url: "/AreaProvider/Provider",
                    data: {
                        'ProviderIds': l_ids.toString()
                    }
                }
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "accept",
                    fn: fn_add_provider
                }
            ],
            maximizable: false,
            title: "Seleccionar Proveedores",
            maxWidth: "700px"
        });
    });
    $("#areaprovider_edit_table tbody").on('click', 'a[data-action=delete]', function (evt) {
        evt.preventDefault();
        l_dataTable.row($(this).closest("tr")).remove().draw(false);
    });
    $("#areaprovider_edit_save").click(function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        var l_param;
        if (l_current_area == null) {
            return;
        }
        l_param = fn_get_data();
        $.daAlert({
            content: {
                type: "text",
                value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "save",
                    fn: function () {
                        $.ajax({
                            data: JSON.stringify(l_param),
                            url: '/AreaProvider/Edit',
                            type: 'post',
                            dataType: 'json',
                            contentType: "application/json; charset=utf-8",
                            beforeSend: _beforeSend,
                            success: _success
                        });
                        function _beforeSend() {
                            gf_addLoadingTo($("#areaprovider_edit_save"));
                        }
                        function _success(r) {
                            if (parseInt(r.status) == 1000) {
                                $.jGrowl(r.response, {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                fn_datatable(l_current_area);
                            } else {
                                $.jGrowl(r.response, {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: r.exception
                                });
                            }
                            gf_removeLoadingTo($("#areaprovider_edit_save"));
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: "Confirmación"
        });
    });
    //fn
    function fn_get_data() {
        var l_return = {
            'AreaId': l_current_area,
            'PrvRecordId': [],
            'PrvProviderId': []
        };
        $(l_dataTable.rows().nodes()).each(function () {
            l_return.PrvRecordId.push(l_dataTable.row($(this)).data()[0]);
            l_return.PrvProviderId.push(l_dataTable.row($(this)).data()[1]);
        });
        if (l_return.PrvRecordId.length == 0) {
            l_return.PrvRecordId.push(-1);
            l_return.PrvProviderId.push(-1);
        }
        return l_return;
    }
    function fn_add_provider() {
        var l_dt_provider, l_rows, l_row_data;
        l_dt_provider = $("#areaprovider_provider_table").DataTable();
        l_rows = l_dt_provider.rows().nodes();
        if ($(l_rows).find(":checkbox:checked").length == 0) {
            $.jGrowl("Seleccione por lo menos un Proveedor", {
                header: 'Validación'
            });
            return false;
        }
        $(l_rows).find(":checkbox:checked").each(function () {
            l_row_data = l_dt_provider.row($(this).closest("tr")).data();
            l_dataTable.row.add([
                0,
                l_row_data[0],
                l_row_data[1],
                l_row_data[2]
            ]).draw(false);
        });
        return true;
    }
    function fn_datatable(p_area) {
        l_dataTable = $("#areaprovider_edit_table").daDataTable({
            "DataTable": {
                "ajax": {
                    "url": "/AreaProvider/GetList",
                    "type": "post",
                    "data": {
                        "id": p_area
                    }
                },
                "aoColumns": [
                    {
                        "data": 2,
                        "className": "text-center"
                    },
                    {
                        "data": 3,
                        "className": "text-left"
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center",
                        "width": "50px",
                        "mRender": function (o) {
                            return `<a href="javascript:void(0)" class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" data-action="delete" title="Eliminar" >
                                        <i class="fa fa-trash m-0"></i>
                                    </a>`;
                        }
                    }
                ],
                "order": [[1, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 10
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": { "column": 2 }
            }
        }).Datatable;
    }
    //init
    gf_Helpers_AreaDropDown({
        $: $("#areaprovider_edit_area"),
        callback: function (o) {
            o.addClass("selectpicker").val(null).selectpicker();
        }
    });
    fn_datatable(0);
});