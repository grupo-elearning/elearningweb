﻿"use strict";
$("document").ready(function () {
    var l_directory;
    $("#interactivecontent_create_form").submit(function (evt) {
        evt.preventDefault();
    });
    $("#interactivecontent_create_save").click(function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        var l_psave = gf_presaveForm($("#interactivecontent_create_form")), l_data, l_formdata = new FormData();
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return;
        }
        l_data = fn_getData();
        if (!l_data) {
            return;
        }
        for (var i in l_data) {
            l_formdata.append(i, l_data[i]);
        }
        $.daAlert({
            content: {
                type: "text",
                value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "save",
                    fn: function () {
                        $.ajax({
                            data: l_formdata,
                            url: '/InteractiveContent/Create',
                            type: 'post',
                            dataType: 'json',
                            contentType: false,
                            processData: false,
                            beforeSend: _beforeSend,
                            success: _success
                        });
                        function _beforeSend() {
                            gf_addLoadingTo($("#interactivecontent_create_save"));
                            gf_triggerKeepActive();
                        }
                        function _success(r) {
                            if (parseInt(r.status) == 1000) {
                                $.jGrowl(r.response, {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                location.href = '/InteractiveContent/Index';
                            } else {
                                $.jGrowl(r.response, {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: r.exception
                                });
                                gf_removeLoadingTo($("#interactivecontent_create_save"));
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: "Confirmación"
        });
    });
    //fn
    function fn_getData() {
        var l_files = l_directory.getFiles(), l_n = 0, l_response = {
            'Name': $('#interactivecontent_create_name').val().trim(),
            'Url': $('#interactivecontent_create_index').val().trim(),
            'Directory': $('#interactivecontent_create_directory_info').val().trim(),
            'Status': $('#interactivecontent_create_status').is(':checked') ? 'A' : 'I'
        };
        //get files
        if (l_files.length > 0) {
            l_n = l_files[0].webkitRelativePath.indexOf('/');
        }
        for (var i = 0; i < l_files.length; i++) {
            l_response["Files[" + i + "].File"] = l_files[i];
            l_response["Files[" + i + "].Path"] = l_files[i].webkitRelativePath.substring(l_n, l_files[i].webkitRelativePath.length);
        }
        return l_response;
    }
    //plugins
    l_directory = $('#interactivecontent_create_directory').daUploadDirectory({
        className: 'form-control',
        dalbaPath: '/assets/dalba',
        select_node: function (event, selected) {
            if (selected.node.data) {
                var l_index = selected.node.data.path, l_n = l_index.indexOf('/');
                $('#interactivecontent_create_index').val(l_index.substring(l_n, l_index.length));
            }
        },
        onchange: function () {
            $('#interactivecontent_create_index').val('');
        }
    });
    //init
    $('#interactivecontent_create_directory_info').attr('data-presave-object', 'Directorio');
    $('#interactivecontent_create_name, #interactivecontent_create_directory_info, #interactivecontent_create_index').data('PresaveRequired', true);
    $('#interactivecontent_create_name').data('PresaveJson', '{"maxLength":200}');
});