﻿"use strict";
$("document").ready(function () {
    (function (p_model) {
        var l_directory, l_oDirectory;
        $("#interactivecontent_edit_form").submit(function (evt) {
            evt.preventDefault();
        });
        $("#interactivecontent_edit_save").click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_psave = gf_presaveForm($("#interactivecontent_edit_form")), l_data, l_formdata = new FormData();
            if (l_psave.hasError) {
                $.jGrowl("Ingrese correctamente los datos del formulario", {
                    header: 'Validación'
                });
                return;
            }
            l_data = fn_getData();
            if (!l_data) {
                return;
            }
            for (var i in l_data) {
                l_formdata.append(i, l_data[i]);
            }
            $.daAlert({
                content: {
                    type: "text",
                    value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "save",
                        fn: function () {
                            $.ajax({
                                data: l_formdata,
                                url: '/InteractiveContent/Edit',
                                type: 'post',
                                dataType: 'json',
                                contentType: false,
                                processData: false,
                                beforeSend: _beforeSend,
                                success: _success
                            });
                            function _beforeSend() {
                                gf_addLoadingTo($("#interactivecontent_edit_save"));
                                gf_triggerKeepActive();
                            }
                            function _success(r) {
                                if (parseInt(r.status) == 1000) {
                                    $.jGrowl(r.response, {
                                        header: 'Información',
                                        type: 'primary'
                                    });
                                    location.href = '/InteractiveContent/Index';
                                } else {
                                    $.jGrowl(r.response, {
                                        header: 'Error',
                                        type: 'danger',
                                        clipboard: r.exception
                                    });
                                    gf_removeLoadingTo($("#interactivecontent_edit_save"));
                                }
                            }
                            return true;
                        }
                    }
                ],
                maximizable: false,
                title: "Confirmación"
            });
        });
        //fn
        function fn_getData() {
            var l_files = l_directory.getFiles(), l_n, l_response = {
                'Id': p_model.Id,
                'Name': $('#interactivecontent_edit_name').val().trim(),
                'Url': l_oDirectory.WasRetrieved ? l_oDirectory.OldUrl : $('#interactivecontent_edit_index').val().trim(),
                'Directory': $('#interactivecontent_edit_directory_info').val().trim(),
                'Status': $('#interactivecontent_edit_status').is(':checked') ? 'A' : 'I',
                'OldDirectory': l_oDirectory.OldDirectory,
                'WasRetrieved': l_oDirectory.WasRetrieved,
                'WasChoosed': l_oDirectory.WasChoosed
            };
            if (l_response.WasRetrieved) {
                return l_response;
            }
            //get files
            if (l_files.length > 0) {
                l_n = l_files[0].webkitRelativePath.indexOf('/');
            }
            for (var i = 0; i < l_files.length; i++) {
                l_response["Files[" + i + "].File"] = l_files[i];
                l_response["Files[" + i + "].Path"] = l_files[i].webkitRelativePath.substring(l_n, l_files[i].webkitRelativePath.length);
            }
            return l_response;
        }
        //plugins
        l_directory = $('#interactivecontent_edit_directory').daUploadDirectory({
            className: 'form-control',
            dalbaPath: '/assets/dalba',
            select_node: function (event, selected) {
                if (selected.node.data) {
                    var l_index = selected.node.data.path, l_n = l_index.indexOf('/');
                    $('#interactivecontent_edit_index').val(l_index.substring(l_n, l_index.length));
                }
            },
            onchange: function (e) {
                $('#interactivecontent_edit_index').val('');
                l_oDirectory.WasRetrieved = false;
                l_oDirectory.WasChoosed = true;
            }
        });
        //init
        l_oDirectory = { 'OldUrl': p_model.Url, 'OldDirectory': p_model.Directory, 'WasRetrieved': true, 'WasChoosed': false };
        $('#interactivecontent_edit_index').val(p_model.Url);
        $('#interactivecontent_edit_directory_info').attr('data-presave-object', 'Directorio').val(p_model.Directory);
        $('#interactivecontent_edit_name, #interactivecontent_edit_directory_info, #interactivecontent_edit_index').data('PresaveRequired', true);
        $('#interactivecontent_edit_name').data('PresaveJson', '{"maxLength":200}');
    }(gf_getClone(ngon.Model)));
});