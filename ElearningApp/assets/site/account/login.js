﻿(function () {
    var l_waiting, l_only_one_profile;
    $.material.init({
        "inputElements": "#account_login_form input.form-control, #account_login_form textarea.form-control, #account_login_form select.form-control"
    });
    $("#account_login_username").data({
        "PresaveRequired": true,
        "PresaveJson": '{"maxLength":20}'
    });
    $("#account_login_password").data({
        "PresaveRequired": true,
        "PresaveJson": '{"maxLength":64}'
    });
    $("#account_login_form").submit(function (evt) {
        evt.preventDefault();
        if (l_waiting) {
            return;
        }
        var l_psave = gf_presaveForm($("#account_login_form")), l_param;
        if (l_psave.hasError) {
            return false;
        }
        fn_initialize();
        l_param = {
            Username: $("#account_login_username").val().trim(),
            Password: $("#account_login_password").val().trim()
        };
        $.ajax({
            data: JSON.stringify(l_param),
            url: '@Url.Action("GetLoginValidate", "Account")',
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: _beforeSend,
            success: _success,
            complete: _complete
        });
        function _beforeSend() {
            $("#account_login_form input[type=submit]").prop("disabled", true);
        }
        function _success(r) {
            if (r.status == 1) {
                fn_buildProfiles(r.response, l_param);
            } else {
                $("#account_login_error_message").html(r.response);
                $("#account_login_error").show();
            }
        }
        function _complete(xhr, status) {
            $("#account_login_form input[type=submit]").prop("disabled", false);
        }
    });
    $("#account_login_profiles_list").on("click", "button", function (evt) {
        evt.preventDefault();
        var l_param = $("#account_login_profiles_list").data("Param"), l_go = false;
        l_param["ProfileId"] = $(this).data("Code");
        $.ajax({
            data: JSON.stringify(l_param),
            url: '@Url.Action("Login", "Account")',
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: _beforeSend,
            success: _success,
            complete: _complete
        });
        function _beforeSend() {
            fn_disableProfiles();
            l_waiting = true;
        }
        function _success(r) {
            if (r.status == 1) {
                l_go = true;
            }
        }
        function _complete() {
            if (l_go) {
                if (window.bGF5b3V0X2xvZ2luX21vZGFsX3Rv) {
                    window.location = window.bGF5b3V0X2xvZ2luX21vZGFsX3Rv;
                } else {
                    window.location = '@Url.Action("Index", "Home")';
                }
            } else {
                fn_enableProfiles();
                l_waiting = false;
            }
        }
    });
    $("#account_login_username, #account_login_password").keyup(function (evt) {
        evt.preventDefault();
        var l_text = $(this).val(), l_username = $("#account_login_username").val(), l_password = $("#account_login_password").val();
        if (l_text != "" && $(this).hasClass("app-error")) {
            $(this).removeClass("app-error");
            $(this).next("div.app-error-message").remove();
            if (l_username !== "" && l_password !== "") {
                $("#account_login_error").hide();
            }
        }
    });
    function fn_disableProfiles() {
        $("#account_login_profiles_list button").prop("disabled", true).addClass("disabled");
        $("#account_login_loading").show();
        if (l_only_one_profile) {
            gf_addLoadingTo($("#account_login_form button[type=submit]"));
        }
    }
    function fn_enableProfiles() {
        $("#account_login_profiles_list button").prop("disabled", false).removeClass("disabled");
        $("#account_login_loading").hide();
        if (l_only_one_profile) {
            gf_removeLoadingTo($("#account_login_form button[type=submit]"));
            l_only_one_profile = false
        }
    }
    function fn_buildProfiles(p_data, p_param) {
        var l_item;
        if (p_data[0][0] == "Error") {
            $("#account_login_error_message").html(p_data[0][2]);
            $("#account_login_error").show();
            return;
        }
        //
        $("#account_login_profiles_list").data("Param", p_param);
        $("#account_login_profiles_list button").remove();
        for (var i in p_data) {
            l_item = $("<button />");
            l_item.text(p_data[i][2]);
            l_item.data("Code", p_data[i][1]);
            l_item.addClass('btn btn-raised btn-primary btn-block m-0 mb-1 cls-1901150924');
            $("#account_login_profiles_list").append(l_item);
        }
        //
        if ($("#account_login_profiles_list button").length == 1) {
            l_only_one_profile = true;
            $("#account_login_profiles_list button").trigger('click');
        } else {
            $("#account_login_profiles").show();
        }
    }
    function fn_initialize() {
        $("#account_login_error").hide();
        $("#account_login_error_message").html("");
        $("#account_login_profiles").hide();
        $("#account_login_profiles button").remove();
        $("#account_login_loading").hide();
    }
}());