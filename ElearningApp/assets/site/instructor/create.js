﻿"use strict";
$("document").ready(function () {
    var l_type_doc, l_number_doc, l_save_person, l_instructor_type, l_person_id;
    //events
    $("#instructor_create_form").submit(function (evt) {
        evt.preventDefault();
    });
    $("#instructor_create_save").click(function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        var l_psave = gf_presaveForm($("#instructor_create_form")), l_data;
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return;
        }
        l_data = {
            'DocumentTypeId': $("#instructor_create_document_type").val(),
            'DocumentNumber': $("#instructor_create_document_number").val().trim(),
            'LastName1': $("#instructor_create_last_name_1").val().trim(),
            'LastName2': $("#instructor_create_last_name_2").val().trim(),
            'FirstName': $("#instructor_create_first_name").val().trim(),
            'Email': $("#instructor_create_email").val().trim(),
            'Type': l_instructor_type,
            'SavePerson': l_save_person ? 'S' : 'N',
            'Status': $("#instructor_create_status").is(":checked") ? "A" : "I",
            'PersonId': l_person_id
        };
        if (l_data.DocumentTypeId != l_type_doc || l_data.DocumentNumber != l_number_doc) {
            $.jGrowl("Los valores de Tipo Documento y/o N° Documento de la Recuperación es diferente a los valores actuales", {
                header: 'Validación'
            });
            return;
        }
        $.daAlert({
            content: {
                type: "text",
                value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "save",
                    fn: function () {
                        $.ajax({
                            data: JSON.stringify(l_data),
                            url: '/Instructor/Create',
                            type: 'post',
                            dataType: 'json',
                            contentType: "application/json; charset=utf-8",
                            beforeSend: _beforeSend,
                            success: _success
                        });
                        function _beforeSend() {
                            gf_addLoadingTo($("#instructor_create_save"));
                        }
                        function _success(r) {
                            if (parseInt(r.status) == 1000) {
                                $.jGrowl(r.response, {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                location.href = '/Instructor/Index';
                            } else {
                                $.jGrowl(r.response, {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: r.exception
                                });
                                gf_removeLoadingTo($("#instructor_create_save"));
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: "Confirmación"
        });
    });
    $("#instructor_create_retrieve").click(function (evt) {
        evt.preventDefault();
        var l_tdoc = $("#instructor_create_document_type").val(), l_ndoc = $("#instructor_create_document_number").val().trim();
        if (!gf_isValue(l_tdoc)) {
            $.jGrowl("Seleccione un Tipo de Documento", {
                header: 'Validación'
            });
            return;
        }
        if (!gf_isValue(l_ndoc)) {
            $.jGrowl("Ingrese un N° Documento", {
                header: 'Validación'
            });
            return;
        }
        $.ajax({
            data: JSON.stringify({ document: l_tdoc, number: l_ndoc }),
            url: '/Instructor/GetPerson',
            type: 'post',
            async: false,
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            success: _success
        });
        function _success(r) {
            if (r.status == 1) {
                if (r.response && r.response.length != 0) {
                    $("#instructor_create_last_name_1").val(r.response[1]);
                    $("#instructor_create_last_name_2").val(r.response[2]);
                    $("#instructor_create_first_name").val(r.response[3]);
                    $("#instructor_create_email").val(r.response[4]);
                    $("#instructor_create_type").val(r.response[5] == 'E' ? 'Externo' : r.response[5] == 'I' ? 'Interno' : '');
                    if (r.response[5] == 'I') {
                        $("#instructor_create_last_name_1").prop('disabled', true).addClass('pointer-events-none').data('PresaveRequired', false);
                        $("#instructor_create_last_name_2").prop('disabled', true).addClass('pointer-events-none').data('PresaveRequired', false);
                        $("#instructor_create_first_name").prop('disabled', true).addClass('pointer-events-none').data('PresaveRequired', false);
                        $("#instructor_create_email").prop('disabled', true).addClass('pointer-events-none').data('PresaveRequired', false);
                        l_instructor_type = 'I';
                        l_save_person = false;
                    } else if (r.response[5] == 'E') {
                        $("#instructor_create_last_name_1").prop('disabled', false).removeClass('pointer-events-none').data('PresaveRequired', true);
                        $("#instructor_create_last_name_2").prop('disabled', false).removeClass('pointer-events-none').data('PresaveRequired', true);
                        $("#instructor_create_first_name").prop('disabled', false).removeClass('pointer-events-none').data('PresaveRequired', true);
                        $("#instructor_create_email").prop('disabled', false).removeClass('pointer-events-none').data('PresaveRequired', false);
                        l_instructor_type = 'E';
                        l_save_person = true;
                    }
                    l_person_id = r.response[0];
                } else {
                    $("#instructor_create_last_name_1").prop('disabled', false).removeClass('pointer-events-none').val('').data('PresaveRequired', true);
                    $("#instructor_create_last_name_2").prop('disabled', false).removeClass('pointer-events-none').val('').data('PresaveRequired', true);
                    $("#instructor_create_first_name").prop('disabled', false).removeClass('pointer-events-none').val('').data('PresaveRequired', true);
                    $("#instructor_create_email").prop('disabled', false).removeClass('pointer-events-none').val('').data('PresaveRequired', false);
                    $("#instructor_create_type").val('Externo');
                    l_instructor_type = 'E';
                    l_save_person = true;
                    l_person_id = 0;
                }
                //save filter
                l_type_doc = l_tdoc;
                l_number_doc = l_ndoc;
            } else {
                $.jGrowl(r.response, {
                    header: 'Error',
                    type: 'danger',
                    clipboard: r.exception
                });
            }
        }
    });
    //fn
    //init
    $("#instructor_create_document_type, #instructor_create_document_number, #instructor_create_last_name_1, #instructor_create_last_name_2, #instructor_create_first_name").data('PresaveRequired', true);
    $("#instructor_create_document_number").data({
        PresaveJson: '{"maxLength":30}'
    });
    $("#instructor_create_last_name_1, #instructor_create_last_name_2, #instructor_create_first_name").data({
        PresaveJson: '{"maxLength":100}'
    });
    $("#instructor_create_email").data({
        PresaveJson: '{"maxLength":50}',
        PresaveList: 'email'
    });
    gf_Helpers_DocumentTypeDropDown({
        $: $("#instructor_create_document_type"),
        callback: function (o) {
            o.addClass("selectpicker").val(null).selectpicker();
        }
    });
});