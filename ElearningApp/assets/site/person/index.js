﻿"use strict";
$("document").ready(function () {
    var l_dataTable;
    $("#person_index_table tbody").on('click', 'a[data-action=send]', function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        var l_datatable = $("#person_index_table").DataTable();
        var l_data = l_datatable.row($(this).closest('tr')).data();

        $.daAlert({
            content: {
                type: 'text',
                value: '<div class="text-center p-05">¿Está seguro que desea enviar sus datos de Usuario a esta persona?</div>'
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "custom",
                    icon: "fa fa-envelope",
                    text: "Enviar",
                    className: "btn btn-raised btn-info",
                    fn: function (p, e) {
                        $.ajax({
                            data: JSON.stringify({
                                'Id': l_data[5],
                                'Fullname': l_data[2],
                                'Username': l_data[3],
                                'Password': l_data[4]
                            }),
                            url: '/Person/Send',
                            type: 'post',
                            dataType: 'json',
                            contentType: "application/json; charset=utf-8",
                            success: _success
                        });
                        function _success(r) {
                            if (r.status == 1) {
                                $.jGrowl(r.response, {
                                    header: 'Información',
                                    type: 'primary'
                                });
                            } else {
                                $.jGrowl(r.response, {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: r.exception
                                });
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: 'Confirmación'
        });
    });
    $("#person_index_table tbody").on('click', 'a[data-action=edit]', function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        var l_datatable = $("#person_index_table").DataTable();
        var l_data = l_datatable.row($(this).closest('tr')).data();
        var l_tipo_doc = l_data[7];
        var l_nro_doc = l_data[1];
        var l_codipers = l_data[5];

        var l_data_person = {
            'tipoDoc': l_tipo_doc,
            'nroDoc': l_nro_doc,
            'codiPers': l_codipers
        };

        var l_a;
        l_a = $.daAlert({
            content: {
                type: "ajax",
                value: {
                    url: "/Person/RegisterModal",
                    data: l_data_person
                }
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "custom",
                    icon: "",
                    text: "Guardar",
                    className: "btn btn-raised btn-primary",
                    fn: fn_save
                }
            ],
            maximizable: false,
            title: "<h3 class='mt-0 mb-0'>Actualizar</h3>",
            maxWidth: "700px",
            movable: false
        });
        function fn_save(p, e) {
            //if (!$("#_layout_register_form").data("action") || $("#_layout_register_form").data("action") == "Deny") {
            //    if ($("#_layout_register_form").data("action") == "Deny") {
            //        $.jGrowl("Usted ya está registrado", {
            //            header: "Error",
            //            type: "danger"
            //        });
            //    }
            //    return false;
            //}
            var l_psave = gf_presaveForm($("#_layout_register_form")), l_success = false;
            if (l_psave.hasError) {
                $.jGrowl("Ingrese correctamente los datos del formulario", {
                    header: 'Validación'
                });
                return false;
            }
            $.ajax({
                data: JSON.stringify({
                    LastName1: $("#_layout_register_lastname1").val().trim(),
                    LastName2: $("#_layout_register_lastname2").val().trim(),
                    FirstName: $("#_layout_register_firstname").val().trim(),
                    Phone: $('#_layout_register_phone').val().trim(),
                    RegisteredId: l_codipers,

                    //DocumentType: $("#_layout_register_doc").val(),
                    //DocumentNumber: $("#_layout_register_ndoc").val().trim(),
                    Email: $("#_layout_register_form").data("action") == "OnlyAccess" ?
                        $("#_layout_register_email").data("initial") : $("#_layout_register_email").val().trim(),
                    Actions: $("#_layout_register_form").data("action")
                }),
                url: "/Person/SavePerson",
                type: "post",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                beforeSend: function () {
                    gf_addLoadingTo(e);
                },
                success: function (r) {
                    if (parseInt(r.status) == 1000) {
                        if ($("#_layout_register_modal_confirm").length == 0) {
                            $("body").append('<div id="_layout_register_modal_confirm" class="modal fade">' +
                                '<div class="modal-dialog modal-confirm">' +
                                '<div class="modal-content">' +
                                '<div class="modal-header">' +
                                '<div class="icon-box">' +
                                '<i class="material-icons">&#xE876;</i>' +
                                '</div>' +
                                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
                                '</div>' +
                                '<div class="modal-body text-center">' +
                                '<h4>¡Genial!</h4>	' +
                                '<p data-modal="message">' + r.response + '</p>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>');
                        } else {
                            $("#_layout_register_modal_confirm p[data-modal=message]").html(r.response);
                        }
                        $('#_layout_register_modal_confirm').modal('show');
                        l_success = true;
                    } else {
                        $.jGrowl(r.response, {
                            header: "Error",
                            type: "danger",
                            clipboard: r.exception
                        });
                    }
                },
                complete: function () {
                    gf_removeLoadingTo(e);
                    if (l_success) {
                        l_a.close();
                    }
                    location.href = '/Person/Index';
                }
            });
            return false;
        }
        
    });
    //fn
    function fn_datatable() {
        l_dataTable = $("#person_index_table").daDataTable({
            "DataTable": {
                "ajax": {
                    "url": "/Person/GetList",
                    "type": "post"
                },
                "aoColumns": [
                    {
                        "data": 2,
                        "className": "text-left"
                    },
                    {
                        "data": null,
                        "className": "text-left",
                        "mRender": function (o) {
                            return o[0] + ' - ' + o[1];
                        }
                    },
                    {
                        "data": 6,
                        "className": "text-left"
                    },
                    {
                        "data": 8,
                        "className": "text-left"
                    },
                    {
                        "data": 3,
                        "className": "text-left"
                    },
                    {
                        "data": 4,
                        "className": "text-left"
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center",
                        "width": "85px",
                        "mRender": function (o) {
                            


                            if (o[0] != 'FICHA SAP') {
                                return `<a href = "javascript:void(0)" class="btn btn-xs btn-raised btn-warning m-0 cls-1810072234" data-action="edit" title = "Editar" style="float:left;">
                                    <i class="fa fa-edit m-0"></i>
                                    </a >
                                    <a href="javascript:void(0)" class="btn btn-xs btn-raised btn-info m-0 cls-1810072234" data-action="send" title="Enviar" style="float:right;">
                                        <i class="fa fa-envelope m-0"></i>
                                    </a>`;

                            } else {
                                return `<a href="javascript:void(0)" class="btn btn-xs btn-raised btn-info m-0 cls-1810072234" data-action="send" title="Enviar" >
                                        <i class="fa fa-envelope m-0"></i>
                                    </a>`;
                            }
                            
                        }
                    }
                ],
                "order": [[0, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 10
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": null,
                "noSearch": [6]
            }
        }).Datatable;
        //buttons
        new $.fn.dataTable.Buttons(l_dataTable, {
            'buttons': [{
                'extend': 'excelHtml5',
                'text': '<i class="fa fa-file-excel-o"></i>Excel',
                'className': 'btn btn-sm btn-raised btn-success m-0 cls-1902151548',
                'exportOptions': {
                    'columns': [0, 1, 2, 3, 4, 5]
                }
            }]
        }).container().appendTo($('#person_index_table_buttons'));
    }
    //init
    fn_datatable();
    
});