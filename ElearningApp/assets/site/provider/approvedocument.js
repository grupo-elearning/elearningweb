﻿"use strict";
$('document').ready(function () {
    var l_dataTable, l_param = { 'Filter_Type': 'X', 'Filter_RUC': '0' };
    //events
    $('#provider_approvedocument_filter_type').change(function () {
        var l_filter = $(this).val();
        if (l_filter == "all") {
            $('#provider_approvedocument_filter_container_ruc').hide();
        } else if (l_filter == "ruc") {
            $('#provider_approvedocument_filter_container_ruc').show();
        } else {
            $('#provider_approvedocument_filter_container_ruc').hide();
        }
    });
    $('#provider_approvedocument_filter_retrieve').click(function (evt) {
        evt.preventDefault();
        fn_retrieve();
    });
    $('#provider_approvedocument_table tbody').on('click', 'i[data-action=detail]', function (evt) {
        evt.preventDefault();
        var l_this = $(this), l_tr = l_this.closest('tr'), l_row = l_dataTable.row(l_tr);
        if (l_row.child.isShown()) {
            l_this
                .removeClass('fa-minus-square-o')
                .addClass('fa-plus-square-o');
            l_row.child.remove();
        }
        else {
            l_this
                .removeClass('fa-plus-square-o')
                .addClass('fa-minus-square-o');
            l_row.child('', 'bg-white').show();
            fn_get_detail(l_row);
        }
    });
    //fn
    function fn_get_detail(p_row) {
        var l_content = p_row.child().find('td');
        $.ajax({
            url: '/Provider/ListApproveDocumentDetail',
            data: JSON.stringify({ 'id': p_row.data()[0] }),
            contentType: 'application/json; charset=utf-8',
            type: 'post',
            dataType: 'html',
            beforeSend: function () {
                l_content.html('<div class="text-center small p-1 text-dark">Cargando...</div>');
            },
            success: function (r) {
                l_content.html(r);
            },
            error: function (xhr, status) {
                l_content.html('Error : ' + xhr.status + ', ' + xhr.statusText);
            }
        });
    }
    function fn_retrieve() {
        var l_filter = $('#provider_approvedocument_filter_type').val(), l_ruc = $('#provider_approvedocument_filter_ruc').val().trim();
        if (l_filter == 'ruc') {
            if (!gf_isValue(l_ruc)) {
                $.jGrowl("Ingrese un número de RUC", {
                    header: "Validación"
                });
                return;
            }
        }
        l_param.Filter_Type = l_filter == 'all' ? 'T' : 'R';
        l_param.Filter_RUC = l_filter == 'all' ? '0' : l_ruc;
        fn_datatable();
    }
    function fn_datatable() {
        l_dataTable = $("#provider_approvedocument_table").daDataTable({
            "DataTable": {
                "bServerSide": true,
                "sAjaxSource": "/Provider/GetApproveDocumentList",
                "fnServerData": function (sSource, aoData, fnCallback, oSettings) {
                    aoData.push(
                        {
                            "name": "Filter_Type", "value": l_param.Filter_Type
                        },
                        {
                            "name": "Filter_RUC", "value": l_param.Filter_RUC
                        }
                    );
                    oSettings.jqXHR = $.ajax({
                        "dataType": 'json',
                        "type": "post",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    });
                },
                "aoColumns": [
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center cls-1901280053",
                        "width": "10px",
                        "mRender": function (o) {
                            return '<i class="fa fa-plus-square-o cursor-pointer" data-action="detail"></i>';
                        }
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center",
                        "width": "50px",
                        "mRender": function (o) {
                            return `<a href="/Provider/ToApprove/` + gf_encode(o[0]) + `" class="btn btn-xs btn-raised btn-success m-0 cls-1810072234" title="Aprobar" >
                                        <i class="fa fa-check m-0"></i>
                                    </a>`;
                        }
                    },
                    {
                        "data": 5,
                        "className": "text-left"
                    },
                    {
                        "data": 3,
                        "className": "text-left"
                    },
                    {
                        "data": 1,
                        "className": "text-center"
                    },
                    {
                        "data": 4,
                        "className": "text-left"
                    },
                    {
                        "data": null,
                        "className": "text-center",
                        "mRender": function (o) {
                            if (o[7] == 'A') {
                                return '<span class="color-info">ACTIVO</span>';
                            } else if (o[7] == 'I') {
                                return '<span class="color-danger">INACTIVO</span>';
                            } else {
                                return '[Status]';
                            }
                        }
                    }
                ],
                "order": [[3, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 15
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": { "column": 0 },
                "noSearch": [1]
            }
        }).Datatable;
    }
    //init
    fn_retrieve();
});