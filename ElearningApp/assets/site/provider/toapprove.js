﻿"use strict";
$("document").ready(function () {
    (function (p_model) {
        var l_employee_dataTable_documents, l_employee_dataTable_employees, l_employee_temp_employee, l_employee_all_data = {};
        var l_vehicle_dataTable_documents, l_vehicle_dataTable_vehicles, l_vehicle_temp_vehicle, l_vehicle_all_data = {};
        //events
        $('#provider_toapprove_employee_table tbody').on('click', 'td[data-action=select-employee]', function (evt) {
            evt.preventDefault();
            if (fn_employee_has_empty_reason()) {
                return;
            }
            var l_tr = $(this).closest('tr'), l_data = l_employee_dataTable_employees.row(l_tr).data();
            l_employee_dataTable_employees.rows().nodes().to$().removeClass('cls-1902221730');
            l_tr.addClass('cls-1902221730');
            $('#provider_toapprove_employee_fullname').text(l_data[3]);
            $('#provider_toapprove_employee_document').text(l_data[1] + ': ' + l_data[2]);
            //save employee data
            if (!l_employee_all_data[l_data[0]]) {
                l_employee_all_data[l_data[0]] = {
                    'iPcon': l_data[0],
                    'oHtmlDocument': {},
                    'oInitialDocument': gf_getClone(l_data[7])
                };
            }
            //save temp employee
            l_employee_temp_employee = l_data;
            fn_employee_datatable_documents(p_model.EmployeesDocuments);
        });
        $('#provider_toapprove_employee_table_document tbody').on('click', 'i[data-action=detail]', function (evt) {
            evt.preventDefault();
            var l_this = $(this), l_tr = l_this.closest('tr'), l_row = l_employee_dataTable_documents.row(l_tr);
            if (l_row.child.isShown()) {
                l_this
                    .removeClass('fa-minus-square-o')
                    .addClass('fa-plus-square-o');
                l_row.child.remove();
            }
            else {
                l_this
                    .removeClass('fa-plus-square-o')
                    .addClass('fa-minus-square-o');
                l_row.child('', 'cls-1901280028').show();
                fn_employee_get_detail(l_row);
            }
        });
        $('#provider_toapprove_vehicle_table tbody').on('click', 'td[data-action=select-vehicle]', function (evt) {
            evt.preventDefault();
            if (fn_vehicle_has_empty_reason()) {
                return;
            }
            var l_tr = $(this).closest('tr'), l_data = l_vehicle_dataTable_vehicles.row(l_tr).data();
            l_vehicle_dataTable_vehicles.rows().nodes().to$().removeClass('cls-1902221730');
            l_tr.addClass('cls-1902221730');
            $('#provider_toapprove_vehicle_description').text(l_data[1]);
            //save vehicle data
            if (!l_vehicle_all_data[l_data[0]]) {
                l_vehicle_all_data[l_data[0]] = {
                    'iPrun': l_data[0],
                    'oHtmlDocument': {},
                    'oInitialDocument': gf_getClone(l_data[5])
                };
            }
            //save temp vehicle
            l_vehicle_temp_vehicle = l_data;
            fn_vehicle_datatable_documents(p_model.VehiclesDocuments);
        });
        $('#provider_toapprove_vehicle_table_document tbody').on('click', 'i[data-action=detail]', function (evt) {
            evt.preventDefault();
            var l_this = $(this), l_tr = l_this.closest('tr'), l_row = l_vehicle_dataTable_documents.row(l_tr);
            if (l_row.child.isShown()) {
                l_this
                    .removeClass('fa-minus-square-o')
                    .addClass('fa-plus-square-o');
                l_row.child.remove();
            }
            else {
                l_this
                    .removeClass('fa-plus-square-o')
                    .addClass('fa-minus-square-o');
                l_row.child('', 'cls-1901280028').show();
                fn_vehicle_get_detail(l_row);
            }
        });
        $('#provider_toapprove_provider_documents div[data-object=switch] input[data-input=switch]').change(function () {
            var l_container = $(this).closest('div[data-object=sustenance-document]').find('div[data-object=reason]'), l_reason = l_container.find('textarea[data-input=reason]');
            if (this.value == 'R') {
                l_container.slideDown('fast');
            } else {
                l_container.slideUp('fast');
                l_reason.val('');
            }
            l_reason.focus();
        });
        $('#provider_toapprove_employee_table_document tbody').on('change', 'div[data-object=switch] input[data-input=switch]', function () {
            var l_container = $(this).closest('div[data-object=sustenance-document]').find('div[data-object=reason]'), l_reason = l_container.find('textarea[data-input=reason]');
            if (this.value == 'R') {
                l_container.slideDown('fast');
            } else {
                l_container.slideUp('fast');
                l_reason.val('');
            }
            l_reason.focus();
        });
        $('#provider_toapprove_vehicle_table_document tbody').on('change', 'div[data-object=switch] input[data-input=switch]', function () {
            var l_container = $(this).closest('div[data-object=sustenance-document]').find('div[data-object=reason]'), l_reason = l_container.find('textarea[data-input=reason]');
            if (this.value == 'R') {
                l_container.slideDown('fast');
            } else {
                l_container.slideUp('fast');
                l_reason.val('');
            }
            l_reason.focus();
        });
        $('#provider_toapprove_save').click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_employee, l_vehicle, l_provider, l_data;
            if (fn_provider_has_empty_reason()) {
                return;
            }
            if (fn_employee_has_empty_reason()) {
                return;
            }
            if (fn_vehicle_has_empty_reason()) {
                return;
            }
            l_employee = fn_employee_get_data();
            l_vehicle = fn_vehicle_get_data();
            l_provider = fn_provider_get_data();
            l_data = {
                'ProviderId': p_model.Provider.Id,
                'ProviderPrdo': l_provider.ProviderPrdo,
                'ProviderSwitch': l_provider.ProviderSwitch,
                'ProviderReason': l_provider.ProviderReason,
                'EmployeePcdo': l_employee.EmployeePcdo,
                'EmployeeSwitch': l_employee.EmployeeSwitch,
                'EmployeeReason': l_employee.EmployeeReason,
                'VehiclePcdo': l_vehicle.VehiclePcdo,
                'VehicleSwitch': l_vehicle.VehicleSwitch,
                'VehicleReason': l_vehicle.VehicleReason
            };
            if (l_data.ProviderPrdo.length == 0 && l_data.EmployeePcdo.length == 0 && l_data.VehiclePcdo.length == 0) {
                return;
            }
            $.daAlert({
                content: {
                    type: "text",
                    value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "save",
                        fn: function () {
                            $.ajax({
                                data: JSON.stringify(l_data),
                                url: '/Provider/ToApprove',
                                type: 'post',
                                dataType: 'json',
                                contentType: "application/json; charset=utf-8",
                                beforeSend: _beforeSend,
                                success: _success
                            });
                            function _beforeSend() {
                                gf_addLoadingTo($("#provider_toapprove_save"));
                            }
                            function _success(r) {
                                if (parseInt(r.status) == 1000) {
                                    $.jGrowl(r.response, {
                                        header: 'Información',
                                        type: 'primary'
                                    });
                                    location.href = '/Provider/ApproveDocument';
                                } else {
                                    $.jGrowl(r.response, {
                                        header: 'Error',
                                        type: 'danger',
                                        clipboard: r.exception
                                    });
                                    gf_removeLoadingTo($("#provider_toapprove_save"));
                                }
                            }
                            return true;
                        }
                    }
                ],
                maximizable: false,
                title: "Confirmación"
            });
        });
        //fn
        function fn_provider_get_data() {
            var l_response = {
                'ProviderPrdo': [],
                'ProviderSwitch': [],
                'ProviderReason': []
            };
            $('#provider_toapprove_provider_documents div[data-object=sustenance-document][data-status=APR]').each(function (idx) {
                var l_switch = $(this).find('div[data-object=switch]'), l_status = l_switch.find('input[data-input=switch]:checked').val(),
                    l_prdo = $(this).attr('data-prdo'), l_reason = $(this).find('textarea[data-input=reason]').val().trim();
                if (l_switch.attr('data-initial') != l_status) {
                    l_response.ProviderPrdo.push(l_prdo);
                    l_response.ProviderSwitch.push(l_status);
                    l_response.ProviderReason.push(l_status == 'R' ? l_reason : '');
                }
            });
            if (l_response.ProviderPrdo.length == 0) {
                l_response.ProviderPrdo.push(0);
                l_response.ProviderSwitch.push('X');
                l_response.ProviderReason.push('');
            }
            return l_response;
        }
        function fn_employee_get_document_status(p_document) {
            var l_documents = l_employee_temp_employee[7], l_pending = null, l_status = null, l_html;
            for (var i in l_documents) {
                if (l_documents[i][1] == p_document[0]) {
                    l_pending = l_documents[i][8];
                    l_status = l_documents[i][10];
                }
            }
            l_html = `<div class="cls-1903022357">`;
            if (l_pending && l_pending == 'P') {
                l_html += `<span style="display: none;">PA</span><span class="glyphicon glyphicon-font app-center" title="Pendiente de Aprobación"></span>`;
            }
            if (l_status && l_pending && l_pending == 'A') {
                if (l_status == 'V') {
                    l_html += `<span style="display: none;">V</span>
                                <div class="app-center app-state" style="background-color: #4caf50;">
                                    <i class="fa fa-check"></i>
                                </div>`;
                } else if (l_status == 'A') {
                    l_html += `<span style="display: none;">A</span>
                                <div class="app-center app-state" style="background-color: #ff9800;">
                                    <i class="fa fa-info"></i>
                                </div>`;
                } else if (l_status == 'R') {
                    l_html += `<span style="display: none;">R</span>
                                <div class="app-center app-state" style="background-color: #f44336;">
                                    <i class="fa fa-times"></i>
                                </div>`;
                } else if (l_status == 'P') {
                    l_html += `<span class="color-info-inverse d-inline-block">P. Aprobación</span>`;
                } else {
                    l_html += `<span class="color-danger-inverse d-inline-block">Sin Documentos</span>`;
                }
            }
            l_html += `</div>`;
            return l_html;
        }
        function fn_employee_datatable_documents(p_data) {
            l_employee_dataTable_documents = $('#provider_toapprove_employee_table_document').daDataTable({
                "DataTable": {
                    "data": p_data,
                    "aoColumns": [
                        {
                            "data": null,
                            "orderable": false,
                            "searchable": false,
                            "className": "text-center cls-1901280053",
                            "width": "10px",
                            "mRender": function (o) {
                                return '<i class="fa fa-plus-square-o cursor-pointer" data-action="detail"></i>';
                            }
                        },
                        {
                            "data": 1,
                            "className": "text-left"
                        },
                        {
                            "data": 7,
                            "className": "text-left"
                        },
                        {
                            "data": null,
                            "className": "text-center position-relative",
                            "mRender": function (o) {
                                return fn_employee_get_document_status(o);
                            }
                        }
                    ],
                    "order": [],
                    "processing": true,
                    "bLengthChange": false,
                    "iDisplayLength": 15,
                    //"paging": false,
                    "fnRowCallback": function (nRow, aData) {
                        $(nRow).addClass('cls-1902221730');
                    }
                },
                "theme": "devexpress",
                "imgProcessing": "/assets/dalba/img/DXR.gif",
                "search": {
                    "btn": null,
                    "noSearch": [0, 3]
                }
            }).Datatable;
        }
        function fn_employee_get_detail(p_row) {
            var l_content = p_row.child().find('td'), l_employee = l_employee_all_data[l_employee_temp_employee[0]], l_initial = true, l_has_documents;
            if (!l_employee.oHtmlDocument[p_row.data()[0]]) {
                l_has_documents = false;
                //get documents
                l_employee.oHtmlDocument[p_row.data()[0]] = $('<div />');
                for (var i = 0; i < l_employee.oInitialDocument.length; i++) {
                    if (p_row.data()[0] == l_employee.oInitialDocument[i][1]) {
                        var l_html = $(fn_employee_get_document_html(p_row.data(), l_employee.oInitialDocument[i], l_initial));
                        l_has_documents = true;
                        //save data
                        l_html.attr({ 'data-tdos': p_row.data()[0], 'data-pcdo': l_employee.oInitialDocument[i][0], 'data-status': l_employee.oInitialDocument[i][8] });
                        //disable to approve
                        if (l_employee.oInitialDocument[i][8] == 'A') {
                            l_html.find('input').each(function () {
                                $(this).attr({
                                    'data-initialValue': $(this).val(),
                                    'data-disabled': true
                                });
                            });
                        }
                        //
                        l_employee.oHtmlDocument[p_row.data()[0]].append(l_html);
                        l_initial = false;
                    }
                }
                if (!l_has_documents) {
                    var l_html = $(fn_employee_get_document_html(p_row.data(), null, l_initial));
                    //save data
                    l_html.attr({ 'data-tdos': p_row.data()[0], 'data-pcdo': 0, 'data-status': 'X' });
                    //
                    l_employee.oHtmlDocument[p_row.data()[0]].append(l_html);
                }
            }
            l_content.append(l_employee.oHtmlDocument[p_row.data()[0]]);
        }
        function fn_employee_get_document_html(p_document, p_data, p_initial, p_massive) {
            var l_html_dates = null, l_html_number = null, l_html_download = null, l_html_switch = null, l_html_reason = null, l_html;
            if (p_document) {
                //build dates
                if (p_document[4] == 'S') {
                    l_html_dates = `<div class="col-lg-3 col-md-6 col-sm-6 col-6 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <div class="cls-1902141431 mt-0 text-nowrap">` + (p_data ? p_data[4] : ``) + `</div>
                                            <div class="icon-input small-calendar cls-1902240417" title="..."></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-6 col-6 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <div class="cls-1902141431 mt-0 text-nowrap">` + (p_data ? p_data[5] : ``) + `</div>
                                            <div class="icon-input small-calendar cls-1902240417" title="..."></div>
                                        </div>
                                    </div>`;
                }
                //build number
                if (p_document[8] == 'S') {
                    l_html_number = `<div class="col-lg-3 col-md-6 col-sm-6 col-12 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <div class="cls-1902141431 mt-0 text-nowrap">` + (p_data ? p_data[13] : ``) + `</div>
                                        </div>
                                    </div>`;
                }
            }
            if (p_data) {
                //build download
                if (p_data[2] != "") {
                    l_html_download = `<a href="` + p_data[14] + `" download class="btn btn-sm btn-raised btn-info m-0 pt-0 pb-0 position-absolute cls-1902240557" data-toggle="tooltip" data-placement="top" title="Descargar">
                                            <i class="icon ion-ios-cloud-download m-0"></i>
                                        </a>`;
                }
                //build switch
                if (p_data[8] == "P") {
                    l_html_switch = `<div class="p-0 border-0 mt-0 mb-05 cls-1902141431 cls-1902251629 cls-1902251909">
                                        <fieldset>
                                            <div class="switch-toggle switch-candy" data-object="switch" data-initial="` + p_data[8] + `">
                                                <input id="employee-approve-` + p_document[0] + `" name="employee-switch-` + p_document[0] + `" type="radio" data-input="switch" value="A" />
                                                <label for="employee-approve-` + p_document[0] + `" onclick="" class="cls-1902251629-approve">Aprobar</label>
                                                <input id="employee-nothing-` + p_document[0] + `" name="employee-switch-` + p_document[0] + `" type="radio" data-input="switch" value="P" checked />
                                                <label for="employee-nothing-` + p_document[0] + `" onclick="" class="cls-1902251629-nothing">o</label>
                                                <input id="employee-deny-` + p_document[0] + `" name="employee-switch-` + p_document[0] + `" type="radio" data-input="switch" value="R" />
                                                <label for="employee-deny-` + p_document[0] + `" onclick="" class="cls-1902251629-deny">Rechazar</label>
                                                <a></a>
                                            </div>
                                        </fieldset>
                                    </div>`;
                }
                //build reason
                if (p_data[8] == "P") {
                    l_html_reason = `<div class="row cls-1902240436" data-object="reason" style="display: none;">
                                        <div class="col-12 cls-1902240431">
                                            <div class="form-group mt-0">
                                                <label class="control-label m-0"><span class="text-dark">Motivo de Rechazo</span> | Máximo 500 caracteres</label>
                                                <textarea class="form-control" maxlength="500" rows="3" data-input="reason"></textarea>
                                            </div>
                                        </div>
                                    </div>`;
                } else if (p_data[8] == "R") {
                    l_html_reason = `<div class="row cls-1902240436" data-object="reason">
                                        <div class="col-12 cls-1902240431">
                                            <div class="form-group mt-0">
                                                <label class="control-label m-0"><span class="text-dark">Motivo de Rechazo</span></label>
                                                <div class="cls-1904291750">` + p_data[15] + `</div>
                                            </div>
                                        </div>
                                    </div>`;
                }
            }
            //build html
            var l_html = `<div class="cls-1901280058" data-object="sustenance-document">
                            <div class="cls-1902240408 pb-0">
                                <div class="app-row">
                                    <div class="pull-right">
                                        ` + (l_html_switch ? l_html_switch : ``) + `
                                    </div>
                                </div>
                                <div class="row cls-1902240436">
                                    ` + (l_html_number ? l_html_number : ``) + `
                                    <div class="`+ (l_html_number && l_html_dates ? `col-lg-3 col-md-6 col-sm-6` : (l_html_number && !l_html_dates ? `col-lg-9 col-md-6 col-sm-6` : (!l_html_number && l_html_dates ? `col-lg-6 col-md-12 col-sm-12` : ``))) + ` col-12 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <div class="cls-1902141431 mt-0 text-nowrap">` + (p_data ? p_data[11] : ``) + `</div>
                                            ` + (l_html_download ? l_html_download : ``) + `
                                        </div>
                                    </div>
                                    ` + (l_html_dates ? l_html_dates : ``) + `
                                </div>
                                ` + (l_html_reason ? l_html_reason : ``) + `
                            </div>
                        </div>`;
            return l_html;
        }
        function fn_employee_get_data() {
            var l_response = {
                'EmployeePcdo': [],
                'EmployeeSwitch': [],
                'EmployeeReason': []
            };
            for (var i in l_employee_all_data) {
                for (var j in l_employee_all_data[i].oHtmlDocument) {
                    l_employee_all_data[i].oHtmlDocument[j].find('div[data-object=sustenance-document][data-status=P]').each(function (idx) {
                        var l_switch = $(this).find('div[data-object=switch]'), l_status = l_switch.find('input[data-input=switch]:checked').val(),
                            l_pcdo = $(this).attr('data-pcdo'), l_reason = $(this).find('textarea[data-input=reason]').val().trim();
                        if (l_switch.attr('data-initial') != l_status) {
                            l_response.EmployeePcdo.push(l_pcdo);
                            l_response.EmployeeSwitch.push(l_status);
                            l_response.EmployeeReason.push(l_status == 'R' ? l_reason : '');
                        }
                    });
                }
            }
            if (l_response.EmployeePcdo.length == 0) {
                l_response.EmployeePcdo.push(0);
                l_response.EmployeeSwitch.push('X');
                l_response.EmployeeReason.push('');
            }
            return l_response;
        }
        function fn_employee_has_empty_reason() {
            var l_employee, l_index, l_empty = false;
            if (!l_employee_temp_employee) {
                return false;
            }
            l_employee = l_employee_all_data[l_employee_temp_employee[0]];
            if (!l_employee) {
                return false;
            }
            for (var i in l_employee.oHtmlDocument) {
                l_employee.oHtmlDocument[i].find('div[data-object=sustenance-document]').each(function (idx) {
                    var l_status = $(this).find('div[data-object=switch] input[data-input=switch]:checked').val(), l_reason = $(this).find('div[data-object=reason] textarea[data-input=reason]').val();
                    l_empty = l_status == 'R' && !gf_isValue(l_reason);
                    if (l_empty) {
                        l_index = idx + 1;
                        return false;
                    }
                });
                if (l_empty) {
                    var l_document = fn_employee_get_document(i);
                    $("a[href='#employees']").trigger('click');
                    $.jGrowl(`El Motivo de Rechazo está vacío en el formulario #` + l_index + ` del documento '` + l_document[1] + `'`, {
                        header: 'Validación'
                    });
                    break;
                }
            }
            return l_empty;
        }
        function fn_employee_get_document(p_tdos) {
            var l_return = null;
            for (var i in p_model.EmployeesDocuments) {
                if (p_model.EmployeesDocuments[i][0] == p_tdos) {
                    l_return = p_model.EmployeesDocuments[i];
                    break;
                }
            }
            return l_return;
        }
        function fn_vehicle_get_document_status(p_document) {
            var l_documents = l_vehicle_temp_vehicle[5], l_pending = null, l_status = null, l_html;
            for (var i in l_documents) {
                if (l_documents[i][1] == p_document[0]) {
                    l_pending = l_documents[i][8];
                    l_status = l_documents[i][10];
                }
            }
            l_html = `<div class="cls-1903022357">`;
            if (l_pending && l_pending == 'P') {
                l_html += `<span style="display: none;">PA</span><span class="glyphicon glyphicon-font app-center" title="Pendiente de Aprobación"></span>`;
            }
            if (l_status && l_pending && l_pending == 'A') {
                if (l_status == 'V') {
                    l_html += `<span style="display: none;">V</span>
                                <div class="app-center app-state" style="background-color: #4caf50;">
                                    <i class="fa fa-check"></i>
                                </div>`;
                } else if (l_status == 'A') {
                    l_html += `<span style="display: none;">A</span>
                                <div class="app-center app-state" style="background-color: #ff9800;">
                                    <i class="fa fa-info"></i>
                                </div>`;
                } else if (l_status == 'R') {
                    l_html += `<span style="display: none;">R</span>
                                <div class="app-center app-state" style="background-color: #f44336;">
                                    <i class="fa fa-times"></i>
                                </div>`;
                } else if (l_status == 'P') {
                    l_html += `<span class="color-info-inverse d-inline-block">P. Aprobación</span>`;
                } else {
                    l_html += `<span class="color-danger-inverse d-inline-block">Sin Documentos</span>`;
                }
            }
            l_html += `</div>`;
            return l_html;
        }
        function fn_vehicle_datatable_documents(p_data) {
            l_vehicle_dataTable_documents = $('#provider_toapprove_vehicle_table_document').daDataTable({
                "DataTable": {
                    "data": p_data,
                    "aoColumns": [
                        {
                            "data": null,
                            "orderable": false,
                            "searchable": false,
                            "className": "text-center cls-1901280053",
                            "width": "10px",
                            "mRender": function (o) {
                                return '<i class="fa fa-plus-square-o cursor-pointer" data-action="detail"></i>';
                            }
                        },
                        {
                            "data": 1,
                            "className": "text-left"
                        },
                        {
                            "data": 7,
                            "className": "text-left"
                        },
                        {
                            "data": null,
                            "className": "text-center position-relative",
                            "mRender": function (o) {
                                return fn_vehicle_get_document_status(o);
                            }
                        }
                    ],
                    "order": [],
                    "processing": true,
                    "bLengthChange": false,
                    "iDisplayLength": 15,
                    //"paging": false,
                    "fnRowCallback": function (nRow, aData) {
                        $(nRow).addClass('cls-1902221730');
                    }
                },
                "theme": "devexpress",
                "imgProcessing": "/assets/dalba/img/DXR.gif",
                "search": {
                    "btn": null,
                    "noSearch": [0, 3]
                }
            }).Datatable;
        }
        function fn_vehicle_get_detail(p_row) {
            var l_content = p_row.child().find('td'), l_vehicle = l_vehicle_all_data[l_vehicle_temp_vehicle[0]], l_initial = true, l_has_documents;
            if (!l_vehicle.oHtmlDocument[p_row.data()[0]]) {
                l_has_documents = false;
                //get documents
                l_vehicle.oHtmlDocument[p_row.data()[0]] = $('<div />');
                for (var i = 0; i < l_vehicle.oInitialDocument.length; i++) {
                    if (p_row.data()[0] == l_vehicle.oInitialDocument[i][1]) {
                        var l_html = $(fn_vehicle_get_document_html(p_row.data(), l_vehicle.oInitialDocument[i], l_initial));
                        l_has_documents = true;
                        //save data
                        l_html.attr({ 'data-tdos': p_row.data()[0], 'data-pcdo': l_vehicle.oInitialDocument[i][0], 'data-status': l_vehicle.oInitialDocument[i][8] });
                        //disable to approve
                        if (l_vehicle.oInitialDocument[i][8] == 'A') {
                            l_html.find('input').each(function () {
                                $(this).attr({
                                    'data-initialValue': $(this).val(),
                                    'data-disabled': true
                                });
                            });
                        }
                        //
                        l_vehicle.oHtmlDocument[p_row.data()[0]].append(l_html);
                        l_initial = false;
                    }
                }
                if (!l_has_documents) {
                    var l_html = $(fn_vehicle_get_document_html(p_row.data(), null, l_initial));
                    //save data
                    l_html.attr({ 'data-tdos': p_row.data()[0], 'data-pcdo': 0, 'data-status': 'X' });
                    //
                    l_vehicle.oHtmlDocument[p_row.data()[0]].append(l_html);
                }
            }
            l_content.append(l_vehicle.oHtmlDocument[p_row.data()[0]]);
        }
        function fn_vehicle_get_document_html(p_document, p_data, p_initial, p_massive) {
            var l_html_dates = null, l_html_number = null, l_html_download = null, l_html_switch = null, l_html_reason = null, l_html;
            if (p_document) {
                //build dates
                if (p_document[4] == 'S') {
                    l_html_dates = `<div class="col-lg-3 col-md-6 col-sm-6 col-6 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <div class="cls-1902141431 mt-0 text-nowrap">` + (p_data ? p_data[4] : ``) + `</div>
                                            <div class="icon-input small-calendar cls-1902240417" title="..."></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-6 col-6 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <div class="cls-1902141431 mt-0 text-nowrap">` + (p_data ? p_data[5] : ``) + `</div>
                                            <div class="icon-input small-calendar cls-1902240417" title="..."></div>
                                        </div>
                                    </div>`;
                }
                //build number
                if (p_document[8] == 'S') {
                    l_html_number = `<div class="col-lg-3 col-md-6 col-sm-6 col-12 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <div class="cls-1902141431 mt-0 text-nowrap">` + (p_data ? p_data[13] : ``) + `</div>
                                        </div>
                                    </div>`;
                }
            }
            if (p_data) {
                //build download
                if (p_data[2] != "") {
                    l_html_download = `<a href="` + p_data[14] + `" download class="btn btn-sm btn-raised btn-info m-0 pt-0 pb-0 position-absolute cls-1902240557" data-toggle="tooltip" data-placement="top" title="Descargar">
                                            <i class="icon ion-ios-cloud-download m-0"></i>
                                        </a>`;
                }
                //build switch
                if (p_data[8] == "P") {
                    l_html_switch = `<div class="p-0 border-0 mt-0 mb-05 cls-1902141431 cls-1902251629 cls-1902251909">
                                        <fieldset>
                                            <div class="switch-toggle switch-candy" data-object="switch" data-initial="` + p_data[8] + `">
                                                <input id="vehicle-approve-` + p_document[0] + `" name="vehicle-switch-` + p_document[0] + `" type="radio" data-input="switch" value="A" />
                                                <label for="vehicle-approve-` + p_document[0] + `" onclick="" class="cls-1902251629-approve">Aprobar</label>
                                                <input id="vehicle-nothing-` + p_document[0] + `" name="vehicle-switch-` + p_document[0] + `" type="radio" data-input="switch" value="P" checked />
                                                <label for="vehicle-nothing-` + p_document[0] + `" onclick="" class="cls-1902251629-nothing">o</label>
                                                <input id="vehicle-deny-` + p_document[0] + `" name="vehicle-switch-` + p_document[0] + `" type="radio" data-input="switch" value="R" />
                                                <label for="vehicle-deny-` + p_document[0] + `" onclick="" class="cls-1902251629-deny">Rechazar</label>
                                                <a></a>
                                            </div>
                                        </fieldset>
                                    </div>`;
                }
                //build reason
                if (p_data[8] == "P") {
                    l_html_reason = `<div class="row cls-1902240436" data-object="reason" style="display: none;">
                                        <div class="col-12 cls-1902240431">
                                            <div class="form-group mt-0">
                                                <label class="control-label m-0"><span class="text-dark">Motivo de Rechazo</span> | Máximo 500 caracteres</label>
                                                <textarea class="form-control" maxlength="500" rows="3" data-input="reason"></textarea>
                                            </div>
                                        </div>
                                    </div>`;
                } else if (p_data[8] == "R") {
                    l_html_reason = `<div class="row cls-1902240436" data-object="reason">
                                        <div class="col-12 cls-1902240431">
                                            <div class="form-group mt-0">
                                                <label class="control-label m-0"><span class="text-dark">Motivo de Rechazo</span></label>
                                                <div class="cls-1904291750">` + p_data[15] + `</div>
                                            </div>
                                        </div>
                                    </div>`;
                }
            }
            //build html
            var l_html = `<div class="cls-1901280058" data-object="sustenance-document">
                            <div class="cls-1902240408 pb-0">
                                <div class="app-row">
                                    <div class="pull-right">
                                        ` + (l_html_switch ? l_html_switch : ``) + `
                                    </div>
                                </div>
                                <div class="row cls-1902240436">
                                    ` + (l_html_number ? l_html_number : ``) + `
                                    <div class="`+ (l_html_number && l_html_dates ? `col-lg-3 col-md-6 col-sm-6` : (l_html_number && !l_html_dates ? `col-lg-9 col-md-6 col-sm-6` : (!l_html_number && l_html_dates ? `col-lg-6 col-md-12 col-sm-12` : ``))) + ` col-12 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <div class="cls-1902141431 mt-0 text-nowrap">` + (p_data ? p_data[11] : ``) + `</div>
                                            ` + (l_html_download ? l_html_download : ``) + `
                                        </div>
                                    </div>
                                    ` + (l_html_dates ? l_html_dates : ``) + `
                                </div>
                                ` + (l_html_reason ? l_html_reason : ``) + `
                            </div>
                        </div>`;
            return l_html;
        }
        function fn_vehicle_get_data() {
            var l_response = {
                'VehiclePcdo': [],
                'VehicleSwitch': [],
                'VehicleReason': []
            };
            for (var i in l_vehicle_all_data) {
                for (var j in l_vehicle_all_data[i].oHtmlDocument) {
                    l_vehicle_all_data[i].oHtmlDocument[j].find('div[data-object=sustenance-document][data-status=P]').each(function (idx) {
                        var l_switch = $(this).find('div[data-object=switch]'), l_status = l_switch.find('input[data-input=switch]:checked').val(),
                            l_pcdo = $(this).attr('data-pcdo'), l_reason = $(this).find('textarea[data-input=reason]').val().trim();
                        if (l_switch.attr('data-initial') != l_status) {
                            l_response.VehiclePcdo.push(l_pcdo);
                            l_response.VehicleSwitch.push(l_status);
                            l_response.VehicleReason.push(l_status == 'R' ? l_reason : '');
                        }
                    });
                }
            }
            if (l_response.VehiclePcdo.length == 0) {
                l_response.VehiclePcdo.push(0);
                l_response.VehicleSwitch.push('X');
                l_response.VehicleReason.push('');
            }
            return l_response;
        }
        function fn_vehicle_has_empty_reason() {
            var l_vehicle, l_index, l_empty = false;
            if (!l_vehicle_temp_vehicle) {
                return false;
            }
            l_vehicle = l_vehicle_all_data[l_vehicle_temp_vehicle[0]];
            if (!l_vehicle) {
                return false;
            }
            for (var i in l_vehicle.oHtmlDocument) {
                l_vehicle.oHtmlDocument[i].find('div[data-object=sustenance-document]').each(function (idx) {
                    var l_status = $(this).find('div[data-object=switch] input[data-input=switch]:checked').val(), l_reason = $(this).find('div[data-object=reason] textarea[data-input=reason]').val();
                    l_empty = l_status == 'R' && !gf_isValue(l_reason);
                    if (l_empty) {
                        l_index = idx + 1;
                        return false;
                    }
                });
                if (l_empty) {
                    var l_document = fn_vehicle_get_document(i);
                    $("a[href='#vehicles']").trigger('click');
                    $.jGrowl(`El Motivo de Rechazo está vacío en el formulario #` + l_index + ` del documento '` + l_document[1] + `'`, {
                        header: 'Validación'
                    });
                    break;
                }
            }
            return l_empty;
        }
        function fn_vehicle_get_document(p_tdos) {
            var l_return = null;
            for (var i in p_model.VehiclesDocuments) {
                if (p_model.VehiclesDocuments[i][0] == p_tdos) {
                    l_return = p_model.VehiclesDocuments[i];
                    break;
                }
            }
            return l_return;
        }
        function fn_provider_has_empty_reason() {
            var l_empty = false, l_tdos;
            $('#provider_toapprove_provider_documents div[data-object=sustenance-document]').each(function (idx) {
                var l_status = $(this).find('div[data-object=switch] input[data-input=switch]:checked').val(), l_reason = $(this).find('div[data-object=reason] textarea[data-input=reason]').val();
                l_empty = l_status == 'R' && !gf_isValue(l_reason);
                if (l_empty) {
                    l_tdos = $(this).attr('data-tdos');
                    return false;
                }
            });
            if (l_empty) {
                var l_document = fn_provider_get_document(l_tdos);
                $("a[href='#provider']").trigger('click');
                $.jGrowl(`El Motivo de Rechazo está vacío del documento '` + l_document['DescripcionCorta'] + `'`, {
                    header: 'Validación'
                });
            }
            return l_empty;
        }
        function fn_provider_get_document(p_tdos) {
            var l_return = null;
            for (var i in p_model.Documents) {
                if (p_model.Documents[i]['CodiTdos'] == p_tdos) {
                    l_return = p_model.Documents[i];
                    break;
                }
            }
            return l_return;
        }
        //init
        l_employee_dataTable_employees = $('#provider_toapprove_employee_table').daDataTable({
            "DataTable": {
                "data": p_model.Employees,
                "aoColumns": [
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "position-relative cls-1902191550 cls-1902240273",
                        "mRender": function (o) {
                            return `<a href="javascript:void(0)" class="m-0 cursor-default">
                                        <img src="` + (o[4] == "" ? `/assets/app/img/default.png` : o[5]) + `" alt="..." class="app-center cls-1901280906" />
                                    </a>`;
                        }
                    },
                    {
                        "data": 3,
                        "className": "text-left align-top cursor-pointer"
                    },
                    {
                        "data": null,
                        "className": "text-center align-top position-relative cursor-pointer",
                        "mRender": function (o) {
                            if (o[6] == 'V') {
                                return `<span style="display: none;">V</span>
                                        <div class="app-center app-state" style="background-color: #4caf50;">
                                            <i class="fa fa-check"></i>
                                        </div>`;
                            } else if (o[6] == 'A') {
                                return `<span style="display: none;">A</span>
                                        <div class="app-center app-state" style="background-color: #ff9800;">
                                            <i class="fa fa-info"></i>
                                        </div>`;
                            } else if (o[6] == 'R') {
                                return `<span style="display: none;">R</span>
                                        <div class="app-center app-state" style="background-color: #f44336;">
                                            <i class="fa fa-times"></i>
                                        </div>`;
                            } else if (o[6] == 'P') {
                                return `<span class="color-info-inverse d-inline-block">P. Aprobación</span>`;
                            } else {
                                return `<span class="color-danger-inverse d-inline-block">Sin Documentos</span>`;
                            }
                        }
                    }
                ],
                "order": [[1, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 15,
                //"paging": false,
                "fnRowCallback": function (nRow, aData) {
                    $(nRow).find('td').eq(1).attr('data-action', 'select-employee');
                    $(nRow).find('td').eq(2).attr('data-action', 'select-employee');
                }
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": null,
                "noSearch": [0]
            }
        }).Datatable;
        fn_employee_datatable_documents([]);
        l_vehicle_dataTable_vehicles = $('#provider_toapprove_vehicle_table').daDataTable({
            "DataTable": {
                "data": p_model.Vehicles,
                "aoColumns": [
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "position-relative cls-1902191550 cls-1902240273",
                        "mRender": function (o) {
                            return `<a href="javascript:void(0)" class="m-0 cursor-default">
                                        <img src="` + (o[2] == "" ? `/assets/site/assets/images/noimage.png` : o[3]) + `" alt="..." class="app-center cls-1901280906" />
                                    </a>`;
                        }
                    },
                    {
                        "data": 1,
                        "className": "text-left align-top cursor-pointer"
                    },
                    {
                        "data": null,
                        "className": "text-center align-top position-relative cursor-pointer",
                        "mRender": function (o) {
                            if (o[4] == 'V') {
                                return `<span style="display: none;">V</span>
                                        <div class="app-center app-state" style="background-color: #4caf50;">
                                            <i class="fa fa-check"></i>
                                        </div>`;
                            } else if (o[4] == 'A') {
                                return `<span style="display: none;">A</span>
                                        <div class="app-center app-state" style="background-color: #ff9800;">
                                            <i class="fa fa-info"></i>
                                        </div>`;
                            } else if (o[4] == 'R') {
                                return `<span style="display: none;">R</span>
                                        <div class="app-center app-state" style="background-color: #f44336;">
                                            <i class="fa fa-times"></i>
                                        </div>`;
                            } else if (o[4] == 'P') {
                                return `<span class="color-info-inverse d-inline-block">P. Aprobación</span>`;
                            } else {
                                return `<span class="color-danger-inverse d-inline-block">Sin Documentos</span>`;
                            }
                        }
                    }
                ],
                "order": [[1, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 15,
                //"paging": false,
                "fnRowCallback": function (nRow, aData) {
                    $(nRow).find('td').eq(1).attr('data-action', 'select-vehicle');
                    $(nRow).find('td').eq(2).attr('data-action', 'select-vehicle');
                }
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": null,
                "noSearch": [0]
            }
        }).Datatable;
        fn_vehicle_datatable_documents([]);
        $('#provider_toapprove_provider_documents div[data-object=switch] input[data-input=switch]:checked').trigger('change');
    }(gf_getClone(ngon.Model)));
});