﻿"use strict";
$("document").ready(function () {
    (function (p_model) {
        var l_list_document_type = gf_Helpers_DocumentTypeDropDown_List(), l_list_vehicle_type = gf_Helpers_VehicleTypeDropDown_List(),
            l_list_account_type = gf_Helpers_AccountTypeDropDown_List(), l_list_bank = gf_Helpers_BankDropDown_List(),
            l_list_money_type = gf_Helpers_MoneyTypeDropDown_List(), l_tdoc_dni = gf_Helpers_GetParameterByKey('CODI_TDOC_DNI'),
            l_term_FOB = gf_Helpers_GetParameterByKey('CODI_TERM_FOB');
        var l_array_delete_pcon = [];
        var l_phones_container = $('#provider_edit_phones'), l_employees_container = $('#provider_edit_table_employee tbody'),
            l_vehicles_container = $('#provider_edit_table_vehicle tbody'), l_accounts_container = $('#provider_edit_table_account tbody'),
            l_documents_container = $('#provider_edit_list_document'), l_emails_container = $('#provider_edit_emails');
        //plugins
        $('#provider_edit_list_document input[data-input=issue-date], #provider_edit_list_document input[data-input=expiration-date]').datepicker({
            orientation: 'top left'
        });
        //events
        l_phones_container.on('click', 'button[data-action=remove-phone]', function (evt) {
            evt.preventDefault();
            $(this).closest('li[data-object=item]').remove();
        });
        l_emails_container.on('click', 'button[data-action=remove-email]', function (evt) {
            evt.preventDefault();
            $(this).closest('li[data-object=item]').remove();
        });
        l_employees_container.on('click', 'button[data-action=remove-employee]', function (evt) {
            evt.preventDefault();
            var pcon = $(this).closest('tr[data-object=employee]').attr("data-pcon");
            if (pcon != undefined) {
                l_array_delete_pcon.push(pcon);
            }
            $(this).closest('tr[data-object=employee]').remove();
        });
        l_employees_container.on('click', 'button[data-action=add-phone]', function (evt) {
            evt.preventDefault();
            fn_employee_add_phone($(this).closest('ul[data-object=phones]'));
        });
        l_employees_container.on('click', 'button[data-action=remove-phone]', function (evt) {
            evt.preventDefault();
            $(this).closest('li[data-object=phone]').remove();
        });
        l_vehicles_container.on('click', 'button[data-action=remove-vehicle]', function (evt) {
            evt.preventDefault();
            $(this).closest('tr[data-object=vehicle]').remove();
        });
        l_accounts_container.on('click', 'button[data-action=remove-account]', function (evt) {
            evt.preventDefault();
            $(this).closest('tr[data-object=account]').remove();
        });
        l_documents_container.on('click', 'button[data-action=remove-document]', function (evt) {
            evt.preventDefault();
            var l_document = $(this).closest('div[data-object=sustenance-document]');
            l_document.find('input[data-input=document]').val('');
            l_document.find('input[data-object=document-name]').val('');
            l_document.find('input[data-input=issue-date]').val('').datepicker('update');
            l_document.find('input[data-input=expiration-date]').val('').datepicker('update');
        });
        l_employees_container.on('change', 'input[data-input=document-number]', function (evt) {
            fn_validate_employee_document_to_retrieve_data($(this));
            fn_set_license_number($(this).val(), $(this));
        });
        l_employees_container.on('keyup', 'input[data-input=document-number]', function (evt) {
            if (evt.keyCode == 13) {
                fn_validate_employee_document_to_retrieve_data($(this));
                fn_set_license_number($(this).val(), $(this));
            }
        });
        l_employees_container.on('change', 'select[data-input=document-type]', function (evt) {
            fn_validate_employee_document_to_retrieve_data($(this), true);
        });
        l_employees_container.on('keyup', 'input[data-input=license-number]', function (evt) {
            //if (evt.keyCode == 13) {
            //    fn_validate_employee_license_to_retrieve_data(this);
            //}
        });
        l_employees_container.on('change', 'input[data-input=license-number]', function (evt) {
            //fn_validate_employee_license_to_retrieve_data(this);
        });
        l_employees_container.on('change', 'input[type=text]', function (evt) {
            $(this).val($(this).val().toUpperCase());
        });
        l_vehicles_container.on('change', 'input[type=text]', function (evt) {
            $(this).val($(this).val().toUpperCase());
        });
        l_accounts_container.on('change', 'input[type=text]', function (evt) {
            $(this).val($(this).val().toUpperCase());
        });



        l_employees_container.on('change keyup', 'input[data-input=last-name-1],input[data-input=last-name-2],input[data-input=first-name]', function (evt) {
            var p_o = $(this);
            var l_nombre_completo_ori =  p_o.closest('tr[data-object=employee]').attr("nombres_vali");
            var l_apPaternoValue = p_o.closest('tr[data-object=employee]').find("input[data-input=last-name-1]").val().trim().toUpperCase().replace(' ', '_');
            var l_apMaternoValue = p_o.closest('tr[data-object=employee]').find("input[data-input=last-name-2]").val().trim().toUpperCase().replace(' ', '_');
            var l_apNombresValue = p_o.closest('tr[data-object=employee]').find("input[data-input=first-name]").val().trim().toUpperCase().replace(' ', '_');
            var l_nombre_completo_modi = l_apPaternoValue + "_" + l_apMaternoValue + "_" + l_apNombresValue;

            if (event.type == "keyup") {
                if (event.keyCode === 13) {
                    //Validamos que el apellido paterno con el original es igual, caso contrario lo marcamos como modificado
                    if (l_nombre_completo_ori !== l_nombre_completo_modi) {
                        p_o.closest('tr[data-object=employee]').attr("modify", "S");
                    } else {
                        p_o.closest('tr[data-object=employee]').removeAttr("modify");
                    }
                }
            } else {
                //Validamos que el apellido paterno con el original es igual, caso contrario lo marcamos como modificado
                if (l_nombre_completo_ori !== l_nombre_completo_modi) {
                    p_o.closest('tr[data-object=employee]').attr("modify", "S");
                } else {
                    p_o.closest('tr[data-object=employee]').removeAttr("modify");
                }
            }
        });

        $('#provider_edit_btn_add_account').click(function (evt) {
            evt.preventDefault();
            fn_add_account();
        });
        $('#provider_edit_btn_add_vehicle').click(function (evt) {
            evt.preventDefault();
            fn_add_vehicle();
        });
        $('#provider_edit_btn_add_employee').click(function (evt) {
            evt.preventDefault();
            debugger;
            fn_add_employee();
        });
        $('#provider_edit_btn_add_phone').click(function (evt) {
            evt.preventDefault();
            fn_add_phone();
        });
        $('#provider_edit_btn_add_email').click(function (evt) {
            evt.preventDefault();
            fn_add_email();
        });
        $('#provider_edit_has_retention').change(function () {
            var l_retention = $('#provider_edit_retention');
            l_retention
                .prop('disabled', !$(this).is(':checked'))
                .data('PresaveRequired', $(this).is(':checked'));
            if ($(this).is(':checked')) {
                l_retention.val(l_retention.data('lastValue') ? l_retention.data('lastValue') : '');
            } else {
                l_retention.data('lastValue', l_retention.val())
                l_retention.val('');
            }
        });
        $("#provider_edit_form").submit(function (evt) {
            evt.preventDefault();
        });
        $('#provider_edit_save_send').click(function (evt) {
            evt.preventDefault();
            fn_save('E');
        });
        $('#provider_edit_save').click(function (evt) {
            evt.preventDefault();
            fn_save('M');
        });
        $('#provider_edit_ruc').click(function (evt) {
            evt.preventDefault();
            $(this).data('lastValue', $(this).val());
        });
        $('#provider_edit_ruc').change(function () {
            $('#provider_edit_display_username').text($(this).val());
            var l_val = $(this).val(), l_exists;
            if (gf_isValue(l_val)) {
                l_exists = fn_exists_ruc({ 'ProviderRUC': l_val });
                if (l_exists) {
                    $(this).val($(this).data('lastValue'));
                    $('#provider_edit_display_username').text($(this).data('lastValue'));
                    $.jGrowl("El RUC que ingreso ya existe", {
                        header: 'Validación'
                    });
                }
            }
        });
        $('#provider_edit_ruc').keyup(function () {
            $('#provider_edit_display_username').text($(this).val());
        });
        $('#provider_edit_filter_employee').keyup(function () {
            var l_filter = $(this).val().trim().toLowerCase();
            l_employees_container.find('tr').each(function (idx) {
                var l_tr = $(this), l_found = false;
                //in selects
                l_tr.find('select option:selected').each(function (idx) {
                    var l_option = $(this).text().toLowerCase();
                    l_found = l_option.search(l_filter) > -1;
                    if (l_found) {
                        return false;
                    }
                });
                if (!l_found) {
                    //in inputs
                    l_tr.find('input').each(function (idx) {
                        var l_value = $(this).val().trim().toLowerCase();
                        l_found = l_value.search(l_filter) > -1;
                        if (l_found) {
                            return false;
                        }
                    });
                }
                //
                if (l_found) {
                    l_tr.show();
                } else {
                    l_tr.hide();
                }
            });
        });
        $('#provider_edit_filter_vehicle').keyup(function () {
            var l_filter = $(this).val().trim().toLowerCase();
            l_vehicles_container.find('tr').each(function (idx) {
                var l_tr = $(this), l_found = false;
                //in selects
                l_tr.find('select option:selected').each(function (idx) {
                    var l_option = $(this).text().toLowerCase();
                    l_found = l_option.search(l_filter) > -1;
                    if (l_found) {
                        return false;
                    }
                });
                if (!l_found) {
                    //in inputs
                    l_tr.find('input').each(function (idx) {
                        var l_value = $(this).val().trim().toLowerCase();
                        l_found = l_value.search(l_filter) > -1;
                        if (l_found) {
                            return false;
                        }
                    });
                }
                //
                if (l_found) {
                    l_tr.show();
                } else {
                    l_tr.hide();
                }
            });
        });
        $('#provider_edit_filter_account').keyup(function () {
            var l_filter = $(this).val().trim().toLowerCase();
            l_accounts_container.find('tr').each(function (idx) {
                var l_tr = $(this), l_found = false;
                //in selects
                l_tr.find('select option:selected').each(function (idx) {
                    var l_option = $(this).text().toLowerCase();
                    l_found = l_option.search(l_filter) > -1;
                    if (l_found) {
                        return false;
                    }
                });
                if (!l_found) {
                    //in inputs
                    l_tr.find('input').each(function (idx) {
                        var l_value = $(this).val().trim().toLowerCase();
                        l_found = l_value.search(l_filter) > -1;
                        if (l_found) {
                            return false;
                        }
                    });
                }
                //
                if (l_found) {
                    l_tr.show();
                } else {
                    l_tr.hide();
                }
            });
        });
        $('#provider_edit_incoterms').change(function () {
            var l_term = $(this).val();
            if (gf_isValue(l_term)) {
                if (l_term == l_term_FOB) {
                    $("a[href='#employees']").trigger('click');
                    $("a[href='#cars'], a[href='#documents'], a[href='#accounts']").hide();
                } else {
                    $("a[href='#cars'], a[href='#documents'], a[href='#accounts']").show();
                }
            }
        });
        //fn
        function fn_save(p_actions) {
            $(this).trigger("blur");
            var l_psave = gf_presaveForm($("#provider_edit_form")), l_ruc = $('#provider_edit_ruc').val(),
                l_data, l_param_phones, l_param_employees, l_param_vehicles, l_param_documents, l_param_accounts,
                l_param_emails, l_formdata = new FormData();
            if (l_psave.hasError) {
                $.jGrowl("Ingrese correctamente los datos del formulario", {
                    header: 'Validación'
                });
                return;
            }
            l_param_emails = fn_presave_emails();
            if (!l_param_emails) {
                return;
            }
            l_param_phones = fn_presave_phones();
            l_param_employees = fn_presave_employees();
            if (!l_param_employees) {
                return;
            }
            l_param_vehicles = fn_presave_vehicles();
            if (!l_param_vehicles) {
                return;
            }
            l_param_documents = fn_presave_documents();
            if (!l_param_documents) {
                return;
            }
            l_param_accounts = fn_presave_accounts();
            if (!l_param_accounts) {
                return;
            }
            l_data = {
                'Id': p_model.Id,
                'RUC': p_model.RUC,
                'Name': $('#provider_edit_business_name').val().trim(),
                'BusinessName': $('#provider_edit_business_name').val().trim(),
                'SAPCode': $('#provider_edit_sap_code').val().trim(),
                'Address': $('#provider_edit_address').val().trim(),
                'Email': $('#provider_edit_email').val().trim(),
                'HasRetention': $('#provider_edit_has_retention').is(':checked') ? 'S' : 'N',
                'Retention': $('#provider_edit_has_retention').is(':checked') ? $('#provider_edit_retention').val() : 0,
                'Status': p_model.IsProviderProfile ? p_model.Status : $('#provider_edit_status').is(':checked') ? 'A' : 'I',
                'IncotermsId': 0,//p_model.IsProviderProfile ? p_model.IncotermsId : $('#provider_edit_incoterms').val(),
                'Actions': p_actions
            };
            //emails
            for (var i = 0; i < l_param_emails.EmailRecordId.length; i++) {
                l_data['EmailRecordId[' + i + ']'] = l_param_emails.EmailRecordId[i];
                l_data['EmailAddress[' + i + ']'] = l_param_emails.EmailAddress[i];
            }
            //phones
            for (var i = 0; i < l_param_phones.PhoneRecordId.length; i++) {
                l_data['PhoneRecordId[' + i + ']'] = l_param_phones.PhoneRecordId[i];
                l_data['PhoneNumber[' + i + ']'] = l_param_phones.PhoneNumber[i];
            }
            //person
            for (var i = 0; i < l_param_employees.PersonRecordId.length; i++) {
                l_data['PersonRecordId[' + i + ']'] = l_param_employees.PersonRecordId[i];
                l_data['PersonPersonId[' + i + ']'] = l_param_employees.PersonPersonId[i];
                l_data['PersonDocumentType[' + i + ']'] = l_param_employees.PersonDocumentType[i];
                l_data['PersonDocumentNumber[' + i + ']'] = l_param_employees.PersonDocumentNumber[i];
                l_data['PersonLicenseNumber[' + i + ']'] = l_param_employees.PersonLicenseNumber[i];
                l_data['PersonLastname1[' + i + ']'] = l_param_employees.PersonLastname1[i];
                l_data['PersonLastname2[' + i + ']'] = l_param_employees.PersonLastname2[i];
                l_data['PersonFirstname[' + i + ']'] = l_param_employees.PersonFirstname[i];
                l_data['PersonBirthday[' + i + ']'] = l_param_employees.PersonBirthday[i];
                l_data['PersonEmail[' + i + ']'] = l_param_employees.PersonEmail[i];
                l_data['Modify[' + i + ']'] = l_param_employees.Modify[i];
                l_data['Userid[' + i + ']'] = l_param_employees.Userid[i];
            }
            //person phone
            for (var i = 0; i < l_param_employees.PersonPhoneRecordId.length; i++) {
                l_data['PersonPhoneRecordId[' + i + ']'] = l_param_employees.PersonPhoneRecordId[i];
                l_data['PersonPhonePersonRecordId[' + i + ']'] = l_param_employees.PersonPhonePersonRecordId[i];
                l_data['PersonPhoneNumber[' + i + ']'] = l_param_employees.PersonPhoneNumber[i];
            }
            //vehicle
            for (var i = 0; i < l_param_vehicles.VehicleRecordId.length; i++) {
                l_data['VehicleRecordId[' + i + ']'] = l_param_vehicles.VehicleRecordId[i];
                l_data['VehicleTypeId[' + i + ']'] = l_param_vehicles.VehicleTypeId[i];
                l_data['VehiclePlate[' + i + ']'] = l_param_vehicles.VehiclePlate[i];
            }
            //document
            for (var i = 0; i < l_param_documents.DocumentRecordId.length; i++) {
                l_data['DocumentRecordId[' + i + ']'] = l_param_documents.DocumentRecordId[i];
                l_data['DocumentTypeId[' + i + ']'] = l_param_documents.DocumentTypeId[i];
                l_data['DocumentHasDate[' + i + ']'] = l_param_documents.DocumentHasDate[i];
                l_data['DocumentIssueDate[' + i + ']'] = l_param_documents.DocumentIssueDate[i];
                l_data['DocumentExpirationDate[' + i + ']'] = l_param_documents.DocumentExpirationDate[i];
                l_data['DocumentFile[' + i + '].File'] = l_param_documents.DocumentFile[i];
                l_data['DocumentFile[' + i + '].Name'] = l_param_documents.DocumentFileName[i];
                l_data['DocumentOldFileName[' + i + ']'] = l_param_documents.DocumentOldFileName[i];
                l_data['DocumentOldFileRealname[' + i + ']'] = l_param_documents.DocumentOldFileRealname[i];
            }
            //account
            for (var i = 0; i < l_param_accounts.AccountRecordId.length; i++) {
                l_data['AccountRecordId[' + i + ']'] = l_param_accounts.AccountRecordId[i];
                l_data['AccountTypeId[' + i + ']'] = l_param_accounts.AccountTypeId[i];
                l_data['AccountNumber[' + i + ']'] = l_param_accounts.AccountNumber[i];
                l_data['AccountBank[' + i + ']'] = l_param_accounts.AccountBank[i];
                l_data['AccountMoney[' + i + ']'] = l_param_accounts.AccountMoney[i];
                l_data['AccountFile[' + i + '].File'] = l_param_accounts.AccountFile[i];
                l_data['AccountOldFileName[' + i + ']'] = l_param_accounts.AccountOldFileName[i];
                l_data['AccountOldFileRealname[' + i + ']'] = l_param_accounts.AccountOldFileRealname[i];
            }
            for (var i in l_data) {
                l_formdata.append(i, l_data[i]);
            }
            for (var i = 0; i < l_array_delete_pcon.length; i++) {
                l_formdata.append("pa_del_codi_pcon", l_array_delete_pcon[i]);
            }
            $.daAlert({
                content: {
                    type: "text",
                    value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "save",
                        fn: function () {
                            $.ajax({
                                data: l_formdata,
                                url: '/Provider/Edit',
                                type: 'post',
                                dataType: 'json',
                                contentType: false,
                                processData: false,
                                beforeSend: _beforeSend,
                                success: _success
                            });
                            function _beforeSend() {
                                gf_addLoadingTo($("#provider_edit_save"));
                                gf_addLoadingTo($("#provider_edit_save_send"));
                                gf_triggerKeepActive();
                            }
                            function _success(r) {
                                if (parseInt(r.status) == 1000) {
                                    $.jGrowl(r.response, {
                                        header: 'Información',
                                        type: 'primary'
                                    });
                                    //location.href = '/Provider/Index';
                                    location.reload(true);
                                } else {
                                    $.jGrowl(r.response, {
                                        header: 'Error',
                                        type: 'danger',
                                        clipboard: r.exception
                                    });
                                    gf_removeLoadingTo($("#provider_edit_save"));
                                    gf_removeLoadingTo($("#provider_edit_save_send"));
                                }
                            }
                            return true;
                        }
                    }
                ],
                maximizable: false,
                title: "Confirmación"
            });
        }
        function fn_add_account(p_data) {
            var l_new_account = $(`<tr data-object="account" data-item="` + (p_data ? p_data[0] : 0) + `">
                                    <td class="align-top position-relative cls-1904051245">
                                        <select class="form-control bg-white" data-input="account-type"></select>
                                    </td>
                                    <td class="align-top position-relative cls-1904051245">
                                        <input type="text" maxlength="50" class="form-control bg-white" data-input="account-number" value="` + (p_data ? p_data[1] : ``) + `" />
                                    </td>
                                    <td class="align-top position-relative cls-1904051245">
                                        <select class="form-control bg-white" data-input="bank"></select>
                                    </td>
                                    <td class="align-top position-relative cls-1904051245">
                                        <select class="form-control bg-white" data-input="money-type"></select>
                                    </td>
                                    <td class="align-top">
                                        <div class="form-group m-0 p-0 is-fileinput">
                                            <input type="text" readonly class="form-control bg-white" placeholder="Seleccionar..." data-object="document-name" value="` + (p_data ? p_data[6] : ``) + `" />
                                            <input type="file" title=" " data-input="document" data-name="` + (p_data ? p_data[5] : ``) + `" />
                                            ` + (p_data ? p_data[5] != '' ? `<a href="` + p_data[7] + `" download class="btn btn-sm btn-raised btn-info m-0 pt-0 pb-0 position-absolute cls-1902240557" data-toggle="tooltip" data-placement="top" title="Descargar">
                                                                                <i class="icon ion-ios-cloud-download m-0"></i>
                                                                            </a>`: `` : ``) + `
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" title="Eliminar" data-action="remove-account">
                                            <i class="fa fa-trash m-0"></i>
                                        </button>
                                    </td>
                                </tr>`);
            var l_select_account_type = l_new_account.find('select[data-input=account-type]');
            var l_select_bank = l_new_account.find('select[data-input=bank]');
            var l_select_money_type = l_new_account.find('select[data-input=money-type]');
            fn_build_dropdown_account_type(l_select_account_type);
            fn_build_dropdown_bank(l_select_bank);
            fn_build_dropdown_money_type(l_select_money_type);
            if (p_data) {
                l_select_account_type.val(p_data[2]);
                l_select_bank.val(p_data[3]);
                l_select_money_type.val(p_data[4]);
            }
            l_accounts_container.prepend(l_new_account);
        }
        function fn_add_vehicle(p_data) {
            var l_new_vehicle = $(`<tr data-object="vehicle" data-prun="` + (p_data ? p_data[0] : 0) + `">
                                        <td class="align-top position-relative cls-1904051245">
                                            <select class="form-control bg-white" data-input="vehicle-type"></select>
                                        </td>
                                        <td class="align-top position-relative cls-1904051245">
                                            <input type="text" maxlength="50" class="form-control bg-white" data-input="plate-number" value="` + (p_data ? p_data[2] : ``) + `" />
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" title="Eliminar" data-action="remove-vehicle">
                                                <i class="fa fa-trash m-0"></i>
                                            </button>
                                        </td>
                                    </tr>`);
            var l_select_vehicle_type = l_new_vehicle.find('select[data-input=vehicle-type]');
            fn_build_dropdown_vehicle_type(l_select_vehicle_type);
            if (p_data) {
                l_select_vehicle_type.val(p_data[1]);
            }
            l_vehicles_container.prepend(l_new_vehicle);
        }
        function fn_employee_add_phone(p_list, p_data) {
            p_list.append(`<li class="list-group-item bg-transparent border-0 p-0 cls-1902141021" data-object="phone">
                                <div class="d-inline-block cls-1902141012">
                                    <input type="text" maxlength="20" class="form-control bg-white" data-input="phone" value="` + (p_data ? p_data.phone : ``) + `" data-ctel="` + (p_data ? p_data.ctel : `0`) + `" />
                                </div>
                                <div class="d-inline-block text-center cls-1902141011">
                                    <button class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" title="Quitar" data-action="remove-phone">
                                        <i class="fa fa-minus m-0"></i>
                                    </button>
                                </div>
                            </li>`);
        }
        function fn_add_employee(p_data, p_phones) {
            var l_new_employee;
            debugger;
            if (p_data) {
                l_new_employee = $(`<tr data-object="employee" nombres_vali="` + p_data[2] + "_" + p_data[3] + "_" + p_data[4] +`" data-pcon="` + p_data[0] + `" data-pers="` + p_data[1] + `">
                                        <td class="align-top">
                                            <input type="text" maxlength="50" class="form-control bg-white" data-userid=`+ p_data[11] +` data-input="codi_pers" value="` + p_data[1] + `" disabled/>
                                        </td>
                                        <td class="align-top">
                                            <input type="text" maxlength="50" class="form-control bg-white" data-input="license-number" value="` + p_data[10] + `" />
                                        </td>
                                        <td class="position-relative align-top cls-1904051245">
                                            <select class="form-control bg-white" data-input="document-type" disabled></select>
                                        </td>
                                        <td class="position-relative align-top cls-1904051245">
                                            <input type="text" maxlength="50" class="form-control bg-white" data-input="document-number" value="` + p_data[7] + `" disabled />
                                        </td>
                                        <td class="align-top">
                                            <input type="text" maxlength="100" class="form-control bg-white" data-input="last-name-1" value="` + p_data[2] + `" />
                                        </td>
                                        <td class="align-top">
                                            <input type="text" maxlength="100" class="form-control bg-white" data-input="last-name-2" value="` + p_data[3] + `" />
                                        </td>
                                        <td class="position-relative align-top cls-1904051245">
                                            <input type="text" maxlength="100" class="form-control bg-white" data-input="first-name" value="` + p_data[4] + `" />
                                        </td>
                                        <td class="align-top">
                                            <input type="text" maxlength="100" class="form-control bg-white" data-input="birthday" value="` + (p_data[12] == "01/01/0001" ? "" : p_data[12])+`" />
                                        </td>
                                        <td class="align-top">
                                            <input type="text" maxlength="200" class="form-control bg-white" data-input="email" value="` + p_data[5] + `" />
                                        </td>
                                        <td class="position-relative cls-1904051245">
                                            <ul class="list-group" data-object="phones">
                                                <li class="list-group-item bg-transparent border-0 p-0 cls-1902141021" data-object="phone">
                                                    <div class="d-inline-block cls-1902141012">
                                                        <input type="text" maxlength="20" class="form-control bg-white" data-input="phone" value="` + (p_phones && p_phones.length > 0 ? p_phones[0].phone : ``) + `" data-ctel="` + (p_phones && p_phones.length > 0 ? p_phones[0].ctel : `0`) + `" />
                                                    </div>
                                                    <div class="d-inline-block text-center cls-1902141011">
                                                        <button class="btn btn-xs btn-raised btn-success m-0 cls-1810072234" title="Agregar" data-action="add-phone">
                                                            <i class="fa fa-plus m-0"></i>
                                                        </button>
                                                    </div>
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" title="Eliminar" data-action="remove-employee">
                                                <i class="fa fa-trash m-0"></i>
                                            </button>
                                        </td>
                                    </tr>`);
            } else {
                l_new_employee = $(`<tr data-object="employee" data-pcon="0">
                                        <td class="align-top">
                                            <input type="text" maxlength="50" class="form-control bg-white"  data-userid=0 data-input="codi_pers" disabled/>
                                        </td>
                                        <td class="align-top">
                                            <input type="text" maxlength="50" class="form-control bg-white" data-input="license-number" />
                                        </td>
                                        <td class="position-relative align-top cls-1904051245">
                                            <select class="form-control bg-white" data-input="document-type"></select>
                                        </td>
                                        <td class="position-relative align-top cls-1904051245">
                                            <input type="text" maxlength="50" class="form-control bg-white" data-input="document-number" />
                                        </td>
                                        <td class="align-top">
                                            <input type="text" maxlength="100" class="form-control bg-white" data-input="last-name-1" disabled />
                                        </td>
                                        <td class="align-top">
                                            <input type="text" maxlength="100" class="form-control bg-white" data-input="last-name-2" disabled />
                                        </td>
                                        <td class="position-relative align-top cls-1904051245">
                                            <input type="text" maxlength="100" class="form-control bg-white" data-input="first-name" disabled />
                                        </td>
                                        <td class="align-top">
                                            <input type="text" maxlength="100" class="form-control bg-white" data-input="birthday"/>
                                        </td>
                                        <td class="align-top">
                                            <input type="text" maxlength="200" class="form-control bg-white" data-input="email" disabled />
                                        </td>
                                        <td class="position-relative cls-1904051245">
                                            <ul class="list-group" data-object="phones">
                                                <li class="list-group-item bg-transparent border-0 p-0 cls-1902141021" data-object="phone">
                                                    <div class="d-inline-block cls-1902141012">
                                                        <input type="text" maxlength="20" class="form-control bg-white" data-input="phone" disabled />
                                                    </div>
                                                    <div class="d-inline-block text-center cls-1902141011">
                                                        <button class="btn btn-xs btn-raised btn-success m-0 cls-1810072234" title="Agregar" data-action="add-phone">
                                                            <i class="fa fa-plus m-0"></i>
                                                        </button>
                                                    </div>
                                                </li>
                                            </ul>
                                        </td>
                                        <td class="text-center">
                                            <button class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" title="Eliminar" data-action="remove-employee">
                                                <i class="fa fa-trash m-0"></i>
                                            </button>
                                        </td>
                                    </tr>`);
            }
            l_new_employee.find("input[data-input=birthday]").datepicker();
            var l_select_document_type = l_new_employee.find('select[data-input=document-type]');
            fn_build_dropdown_document_type(l_select_document_type);
            l_select_document_type.val(p_data ? p_data[6] : l_tdoc_dni);
            if (p_phones) {
                var l_list = l_new_employee.find('ul[data-object=phones]');
                for (var i = 1; i < p_phones.length; i++) {
                    fn_employee_add_phone(l_list, p_phones[i]);
                }
            }
            l_employees_container.prepend(l_new_employee);
            if (!p_data) {
                l_new_employee.find('input[data-input=license-number]').focus();
            }
        }
        function fn_add_phone(p_data) {
            var l_item = $(`<li class="list-group-item bg-transparent border-0 p-0" data-object="item">
                                <div class="app-row w-100 mt-05">
                                    <div class="pull-left cls-1902131706">
                                        <input type="text" maxlength="20" class="form-control" value="` + (p_data ? p_data[1] : ``) + `" data-input="phone" data-ptel="` + (p_data ? p_data[0] : 0) + `" />
                                    </div>
                                    <div class="pull-right cls-1902191656">
                                        <button class="btn btn-raised btn-danger m-0 cls-1904150857" title="Quitar teléfono" data-action="remove-phone">
                                            <i class="fa fa-minus m-0 cls-1904150856"></i>
                                            <i class="icon ion-md-call m-0 app-center cls-1904150855"></i>
                                        </button>
                                    </div>
                                </div>
                            </li>`);
            l_phones_container.append(l_item);
        }
        function fn_add_email(p_data) {
            var l_item = $(`<li class="list-group-item bg-transparent border-0 p-0" data-object="item">
                                <div class="app-row w-100 mt-05">
                                    <div class="pull-left cls-1902131706">
                                        <input type="text" maxlength="200" class="form-control" value="` + (p_data ? p_data[1] : ``) + `" data-input="email" data-pema="` + (p_data ? p_data[0] : 0) + `" />
                                    </div>
                                    <div class="pull-right cls-1902191656">
                                        <button class="btn btn-raised btn-danger m-0 cls-1904150857" title="Quitar email" data-action="remove-email">
                                            <i class="fa fa-minus m-0 cls-1904150856"></i>
                                            <i class="icon ion-ios-at m-0 app-center cls-1904150855"></i>
                                        </button>
                                    </div>
                                </div>
                            </li>`);
            l_emails_container.append(l_item);
        }
        function fn_presave_accounts() {
            var l_return = {
                'AccountRecordId': [],
                'AccountTypeId': [],
                'AccountNumber': [],
                'AccountBank': [],
                'AccountMoney': [],
                'AccountFile': [],
                'AccountOldFileName': [],
                'AccountOldFileRealname': []
            }, l_type, l_number, l_bank, l_money, l_document_name, l_error = false;
            l_accounts_container.find('tr[data-object=account]').each(function (idx) {
                var l_account = $(this);
                l_type = l_account.find('select[data-input=account-type]').val();
                l_number = l_account.find('input[data-input=account-number]').val().trim();
                l_bank = l_account.find('select[data-input=bank]').val();
                l_money = l_account.find('select[data-input=money-type]').val();
                l_document_name = l_account.find('input[data-object=document-name]').val();
                if (!gf_isValue(l_type)) {
                    $.jGrowl("La Cuenta Bancaria de la fila [" + (idx + 1) + "] no tiene Tipo Cuenta.<br />El 'Tipo Cuenta' es campo requerido.", {
                        header: 'Validación'
                    });
                    l_error = true;
                    return false;
                }
                if (!gf_isValue(l_number)) {
                    $.jGrowl("La Cuenta Bancaria de la fila [" + (idx + 1) + "] no tiene Nro. Cuenta.<br />El 'Nro. Cuenta' es campo requerido.", {
                        header: 'Validación'
                    });
                    l_error = true;
                    return false;
                }
                if (!gf_isValue(l_bank)) {
                    $.jGrowl("La Cuenta Bancaria de la fila [" + (idx + 1) + "] no tiene Banco.<br />El 'Banco' es campo requerido.", {
                        header: 'Validación'
                    });
                    l_error = true;
                    return false;
                }
                if (!gf_isValue(l_money)) {
                    $.jGrowl("La Cuenta Bancaria de la fila [" + (idx + 1) + "] no tiene Tipo Moneda.<br />El 'Tipo Moneda' es campo requerido.", {
                        header: 'Validación'
                    });
                    l_error = true;
                    return false;
                }
                l_return.AccountRecordId.push(l_account.attr('data-item'));
                l_return.AccountTypeId.push(l_type);
                l_return.AccountNumber.push(l_number);
                l_return.AccountBank.push(l_bank);
                l_return.AccountMoney.push(l_money);
                l_return.AccountFile.push(gf_isValue(l_account.find('input[data-input=document]').val()) ? l_account.find('input[data-input=document]')[0].files[0] : null);
                l_return.AccountOldFileName.push(l_account.find('input[data-input=document]').attr('data-name'));
                l_return.AccountOldFileRealname.push(l_document_name);
            });
            if (l_error) {
                return null;
            }
            if (l_return.AccountRecordId.length == 0) {
                l_return.AccountRecordId.push(-1);
                l_return.AccountTypeId.push(0);
                l_return.AccountNumber.push("");
                l_return.AccountBank.push(0);
                l_return.AccountMoney.push(0);
                l_return.AccountFile.push(null);
                l_return.AccountOldFileName.push('');
                l_return.AccountOldFileRealname.push('');
                //$.jGrowl("Ingrese por lo menos una Cuenta Bancaria", {
                //    header: 'Validación'
                //});
                //return null;
            }
            return l_return;
        }
        function fn_presave_documents() {
            var l_return = {
                'DocumentRecordId': [],
                'DocumentTypeId': [],
                'DocumentHasDate': [],
                'DocumentIssueDate': [],
                'DocumentExpirationDate': [],
                'DocumentFile': [],
                'DocumentFileName': [],
                'DocumentOldFileName': [],
                'DocumentOldFileRealname': []
            }, l_issue_date, l_expiration_date, l_document_name, l_has_date, l_tdos, l_description, l_prdo, l_error = false;
            l_documents_container.find('div[data-object=sustenance-document]').each(function () {
                var l_document = $(this);
                l_has_date = l_document.attr('data-has-date') == 'S';
                l_document_name = l_document.find('input[data-object=document-name]').val();
                l_issue_date = l_has_date ? l_document.find('input[data-input=issue-date]').data('datepicker').getFormattedDate('dd/mm/yyyy') : '';
                l_expiration_date = l_has_date ? l_document.find('input[data-input=expiration-date]').data('datepicker').getFormattedDate('dd/mm/yyyy') : '';
                if (l_document.attr('data-status') != '' || gf_isValue(l_document_name) || gf_isValue(l_issue_date) || gf_isValue(l_expiration_date)) {
                    l_description = l_document.attr('data-description');
                    if (gf_isValue(l_document_name) || gf_isValue(l_issue_date) || gf_isValue(l_expiration_date)) {
                        if (l_has_date) {
                            if (!gf_isValue(l_issue_date)) {
                                $.jGrowl("La Fecha Emisión de '" + l_description + "' es requerido.", {
                                    header: 'Validación'
                                });
                                l_error = true;
                                return false;
                            }
                            if (!gf_isValue(l_expiration_date)) {
                                $.jGrowl("La Fecha Vencimiento de '" + l_description + "' es requerido.", {
                                    header: 'Validación'
                                });
                                l_error = true;
                                return false;
                            }
                        }
                        if (!gf_isValue(l_document_name)) {
                            $.jGrowl("El Documento de '" + l_description + "' es requerido.", {
                                header: 'Validación'
                            });
                            l_error = true;
                            return false;
                        }
                    }
                    l_tdos = l_document.attr('data-tdos');
                    l_prdo = l_document.attr('data-prdo');
                    l_return.DocumentRecordId.push(l_prdo);
                    l_return.DocumentTypeId.push(l_tdos);
                    l_return.DocumentHasDate.push(l_has_date ? 'S' : 'N');
                    l_return.DocumentIssueDate.push(l_issue_date);
                    l_return.DocumentExpirationDate.push(l_expiration_date);
                    l_return.DocumentFile.push(gf_isValue(l_document.find('input[data-input=document]').val()) ? l_document.find('input[data-input=document]')[0].files[0] : null);
                    l_return.DocumentFileName.push(l_document_name);
                    l_return.DocumentOldFileName.push(l_document.find('input[data-input=document]').attr('data-name'));
                    l_return.DocumentOldFileRealname.push(l_document_name);
                }
            });
            if (l_error) {
                return null;
            }
            if (l_return.DocumentRecordId.length == 0) {
                l_return.DocumentRecordId.push(0);
                l_return.DocumentTypeId.push(0);
                l_return.DocumentHasDate.push('');
                l_return.DocumentIssueDate.push('');
                l_return.DocumentExpirationDate.push('');
                l_return.DocumentFile.push(null);
                l_return.DocumentFileName.push('');
                l_return.DocumentOldFileName.push('');
                l_return.DocumentOldFileRealname.push('');
            }
            return l_return;
        }
        function fn_presave_vehicles() {
            var l_return = {
                'VehicleRecordId': [],
                'VehicleTypeId': [],
                'VehiclePlate': []
            }, l_type, l_plate_number, l_negative_id = 0, l_error = false;
            l_vehicles_container.find('tr[data-object=vehicle]').each(function (idx) {
                var l_vehicle = $(this), l_prun;
                l_negative_id--;
                l_prun = l_vehicle.attr('data-prun') == 0 ? l_negative_id : l_vehicle.attr('data-prun');
                l_type = l_vehicle.find('select[data-input=vehicle-type]').val();
                l_plate_number = l_vehicle.find('input[data-input=plate-number]').val().trim();
                if (!gf_isValue(l_type)) {
                    $.jGrowl("El Vehículo de la fila [" + (idx + 1) + "] no tiene Tipo Vehículo.<br />El 'Tipo Vehículo' es campo requerido.", {
                        header: 'Validación'
                    });
                    l_error = true;
                    return false;
                }
                if (!gf_isValue(l_plate_number)) {
                    $.jGrowl("El Vehículo de la fila [" + (idx + 1) + "] no tiene Número Placa.<br />El 'Número Placa' es campo requerido.", {
                        header: 'Validación'
                    });
                    l_error = true;
                    return false;
                }
                l_return.VehicleRecordId.push(l_prun);
                l_return.VehicleTypeId.push(l_type);
                l_return.VehiclePlate.push(l_plate_number);
            });
            if (l_error) {
                return null;
            }
            if (l_return.VehicleRecordId.length == 0) {
                l_return.VehicleRecordId.push(0);
                l_return.VehicleTypeId.push(0);
                l_return.VehiclePlate.push("");
            }
            return l_return;
        }
        function fn_presave_employees() {
            var l_return = {
                'PersonRecordId': [],
                'PersonPersonId': [],
                'PersonLicenseNumber': [],
                'PersonDocumentType': [],
                'PersonDocumentNumber': [],
                'PersonLastname1': [],
                'PersonLastname2': [],
                'PersonFirstname': [],
                'PersonBirthday': [],
                'PersonEmail': [],
                'PersonPhoneRecordId': [],
                'PersonPhonePersonRecordId': [],
                'PersonPhoneNumber': [],
                'Modify': [],
                'Userid': []
            }, l_document_type, l_document_number, l_license_number, l_last_name1, l_last_name2, l_first_name,
                l_birdthday, l_email, l_phone, l_negative_id = 0, l_error = false, l_userid, l_birthday;
            var l_ttl_phones;
            l_employees_container.find('tr[data-object=employee]').each(function (idx) {
                var l_employee = $(this), l_phones = l_employee.find('ul[data-object=phones]'), l_pcon;
                l_ttl_phones = 0;
                l_negative_id--;
                l_pcon = l_employee.attr('data-pcon') == 0 ? l_negative_id : l_employee.attr('data-pcon');
                l_document_type = l_employee.find('select[data-input=document-type]').val();
                l_document_number = l_employee.find('input[data-input=document-number]').val().trim();
                l_license_number = l_employee.find('input[data-input=license-number]').val().trim();
                l_last_name1 = l_employee.find('input[data-input=last-name-1]').val().trim();
                l_last_name2 = l_employee.find('input[data-input=last-name-2]').val().trim();
                l_first_name = l_employee.find('input[data-input=first-name]').val().trim();
                l_birthday = l_employee.find('input[data-input=birthday]').val().trim();
                l_email = l_employee.find('input[data-input=email]').val().trim();
                l_userid = l_employee.find("input[data-input=codi_pers]").attr("data-userid");
                
                if (!gf_isValue(l_document_type)) {
                    $.jGrowl("El Trabajador de la fila [" + (idx + 1) + "] no tiene Tipo Documento.<br />El 'Tipo Documento' es campo requerido.", {
                        header: 'Validación'
                    });
                    l_error = true;
                    return false;
                }
                if (!gf_isValue(l_document_number)) {
                    $.jGrowl("El Trabajador de la fila [" + (idx + 1) + "] no tiene Número Documento.<br />El 'Número Documento' es campo requerido.", {
                        header: 'Validación'
                    });
                    l_error = true;
                    return false;
                }
                if (!gf_isValue(l_first_name)) {
                    $.jGrowl("El Trabajador de la fila [" + (idx + 1) + "] no tiene Nombre.<br />El 'Nombres' es campo requerido.", {
                        header: 'Validación'
                    });
                    l_error = true;
                    return false;
                }
                if (gf_isValue(l_email) && !gf_isEmail(l_email)) {
                    $.jGrowl("El Trabajador de la fila [" + (idx + 1) + "] tiene un Email incorrecto.", {
                        header: 'Validación'
                    });
                    l_error = true;
                    return false;
                }
                l_return.PersonRecordId.push(l_pcon);
                l_return.PersonPersonId.push(l_employee.attr('data-pers') ? l_employee.attr('data-pers') : l_negative_id);
                l_return.PersonDocumentType.push(l_document_type);
                l_return.PersonDocumentNumber.push(l_document_number);
                l_return.PersonLicenseNumber.push(l_license_number);
                l_return.PersonLastname1.push(l_last_name1);
                l_return.PersonLastname2.push(l_last_name2);
                l_return.PersonFirstname.push(l_first_name);
                l_return.PersonBirthday.push(l_birthday);
                l_return.PersonEmail.push(l_email);
                l_return.Userid.push(l_userid);

                if (l_pcon > 0) {
                    var l_row_table = l_employee.closest('tr[data-object=employee]');
                    console.log(l_row_table.attr("modify"));
                    if (l_row_table.attr('modify')!= undefined) {
                        l_return.Modify.push("M");
                    } else {
                        l_return.Modify.push("");
                    }
                } else {
                    l_return.Modify.push("M");
                }

                l_phones.find('input[data-input=phone]').each(function () {
                    l_phone = $(this).val().trim();
                    if (gf_isValue(l_phone)) {
                        l_return.PersonPhoneRecordId.push($(this).attr('data-ctel'));
                        l_return.PersonPhonePersonRecordId.push(l_pcon);
                        l_return.PersonPhoneNumber.push(l_phone);
                        l_ttl_phones++;
                    }
                });
                if (l_ttl_phones == 0) {
                    $.jGrowl("El Trabajador de la fila [" + (idx + 1) + "] no tiene Celular.<br />Ingrese por lo menos un Celular.", {
                        header: 'Validación'
                    });
                    l_error = true;
                    return false;
                }
            });
            if (l_error) {
                return null;
            }
            if (l_return.PersonRecordId.length == 0) {
                l_return.PersonRecordId.push(0);
                l_return.PersonPersonId.push(0);
                l_return.PersonDocumentType.push(0);
                l_return.PersonDocumentNumber.push("0");
                l_return.PersonLicenseNumber.push("0");
                l_return.PersonLastname1.push("0");
                l_return.PersonLastname2.push("0");
                l_return.PersonFirstname.push("0");
                l_return.PersonBirthday.push("0");
                l_return.PersonEmail.push("0");
                l_return.Modify.push("");
                l_return.Userid.push(-1);
            }
            if (l_return.PersonPhoneRecordId.length == 0) {
                l_return.PersonPhoneRecordId.push(0);
                l_return.PersonPhonePersonRecordId.push(0);
                l_return.PersonPhoneNumber.push('');
            }
            return l_return;
        }
        function fn_presave_phones() {
            var l_return = {
                'PhoneRecordId': [],
                'PhoneNumber': []
            }, l_phone;
            l_phones_container.find('input[data-input=phone]').each(function () {
                l_phone = $(this).val().trim();
                if (gf_isValue(l_phone)) {
                    l_return.PhoneRecordId.push($(this).attr('data-ptel'));
                    l_return.PhoneNumber.push(l_phone);
                }
            });
            if (l_return.PhoneRecordId.length == 0) {
                l_return.PhoneRecordId.push(-1);
                l_return.PhoneNumber.push('');
            }
            return l_return;
        }
        function fn_presave_emails() {
            var l_return = {
                'EmailRecordId': [],
                'EmailAddress': []
            }, l_email, l_error = false, l_input;
            l_emails_container.find('input[data-input=email]').each(function () {
                l_email = $(this).val().trim();
                if (gf_isValue(l_email)) {
                    if (!gf_isEmail(l_email)) {
                        $.jGrowl("El Email '" + l_email + "' es incorrecto.<br />Ingrese un Email válido.", {
                            header: 'Validación'
                        });
                        l_error = true;
                        l_input = this;
                        return false;
                    } else {
                        l_return.EmailRecordId.push($(this).attr('data-pema'));
                        l_return.EmailAddress.push(l_email);
                    }
                }
            });
            if (l_error) {
                $(l_input).focus();
                return null;
            }
            if (l_return.EmailRecordId.length == 0) {
                l_return.EmailRecordId.push(-1);
                l_return.EmailAddress.push('');
            }
            return l_return;
        }
        function fn_build_dropdown_document_type(p_o) {
            p_o.append('<option value="0" disabled selected>-Seleccione-</option>');
            for (var i in l_list_document_type) {
                p_o.append(("<option value='" + l_list_document_type[i].Id + "'>" + l_list_document_type[i].Description + "</option>"));
            }
        }
        function fn_build_dropdown_vehicle_type(p_o) {
            p_o.append('<option value="" disabled selected>-Seleccione-</option>');
            for (var i in l_list_vehicle_type) {
                p_o.append(("<option value='" + l_list_vehicle_type[i].Id + "'>" + l_list_vehicle_type[i].Description + "</option>"));
            }
        }
        function fn_build_dropdown_account_type(p_o) {
            p_o.append('<option value="" disabled selected>-Seleccione-</option>');
            for (var i in l_list_account_type) {
                p_o.append(("<option value='" + l_list_account_type[i].Id + "'>" + l_list_account_type[i].Description + "</option>"));
            }
        }
        function fn_build_dropdown_bank(p_o) {
            p_o.append('<option value="" disabled selected>-Seleccione-</option>');
            for (var i in l_list_bank) {
                p_o.append(("<option value='" + l_list_bank[i].Id + "'>" + l_list_bank[i].Description + "</option>"));
            }
        }
        function fn_build_dropdown_money_type(p_o) {
            p_o.append('<option value="" disabled selected>-Seleccione-</option>');
            for (var i in l_list_money_type) {
                p_o.append(("<option value='" + l_list_money_type[i].Id + "'>" + l_list_money_type[i].Description + "</option>"));
            }
        }
        function fn_validate_employee_document_to_retrieve_data(p_o, p_no_show_alert) {
            var l_employee = p_o.closest('tr[data-object=employee]');
            var l_type = l_employee.find('select[data-input=document-type]').val(), l_number = l_employee.find('input[data-input=document-number]').val().trim();
            if (gf_isValue(l_type) && gf_isValue(l_number)) {
                if (!fn_exists_employee_document(p_o)) {
                    var l_data = fn_get_person_data_by_document({ 'DocumentType': l_type, 'DocumentNumber': l_number });
                    fn_set_employee_data_by_document(l_data, p_o);
                } else {
                    fn_clear_employee(p_o);
                    $.jGrowl("No se puede ingresar el mismo Tipo de Documento y Número de Documento", {
                        header: 'Validación'
                    });
                }
            } else {
                if (!p_no_show_alert) {
                    $.jGrowl("Seleccione un Tipo Documento e ingrese un Nro. Documento", {
                        header: 'Validación'
                    });
                }
            }
        }
        function fn_get_person_data_by_document(p_param) {
            var l_result = [];
            $.ajax({
                data: JSON.stringify(p_param),
                url: '/Provider/GetPersonDataByDocument',
                async: false,
                type: 'post',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: function (r) {
                    if (r.response.length > 0) {
                        l_result = r.response[0];
                    }
                }
            });
            return l_result;
        }
        function fn_exists_ruc(p_param) {
            var l_exists = false;
            $.ajax({
                data: JSON.stringify(p_param),
                url: '/Provider/GetProviderIdByRUC',
                async: false,
                type: 'post',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: function (r) {
                    l_exists = r.response.length > 0;
                }
            });
            return l_exists;
        }
        function fn_exists_employee_document(p_o) {
            var l_employee = p_o.closest('tr[data-object=employee]'), l_exists = false;
            var l_type = l_employee.find('select[data-input=document-type]').val().trim(), l_number = l_employee.find('input[data-input=document-number]').val().trim();
            l_employees_container.find('tr[data-object=employee]').each(function (index) {
                if (this != l_employee[0]) {
                    if ($(this).find('select[data-input=document-type]').val() == l_type &&
                        $(this).find('input[data-input=document-number]').val().trim() == l_number) {
                        l_exists = true;
                        return false;
                    }
                }
            });
            return l_exists;
        }
        function fn_clear_employee(p_o) {
            var l_employee = p_o.closest('tr[data-object=employee]');
            l_employee.find('select[data-input=document-type]').val('0');
            l_employee.find('input[data-input=document-number]').val('');
            l_employee.find('input[data-input=last-name-1]').val('').prop('disabled', true);
            l_employee.find('input[data-input=last-name-2]').val('').prop('disabled', true);
            l_employee.find('input[data-input=first-name]').val('').prop('disabled', true);
            l_employee.find('input[data-input=email]').val('').prop('disabled', true);
            l_employee.find('ul[data-object=phones] input[data-input=phone]').val('').prop('disabled', true);
            l_employee.attr('data-pers', null);
        }
        function fn_new_employee(p_o) {
            var l_employee = p_o.closest('tr[data-object=employee]');
            //l_employee.find('input[data-input=license-number]').val('').prop('disabled', false);
            l_employee.find('input[data-input=last-name-1]').val('').prop('disabled', false);
            l_employee.find('input[data-input=last-name-2]').val('').prop('disabled', false);
            l_employee.find('input[data-input=first-name]').val('').prop('disabled', false);
            l_employee.find('input[data-input=email]').val('').prop('disabled', false);
            l_employee.find('ul[data-object=phones] input[data-input=phone]').val('').prop('disabled', false);
            l_employee.attr('data-pers', null);
            l_employee.find("input[data-input=codi_pers]").attr("data-userid",0);
        }
        function fn_retrieve_employee(p_data, p_o) {
            var l_employee = p_o.closest('tr[data-object=employee]');
            //l_employee.find('input[data-input=license-number]').val(p_data[6]).prop('disabled', false);
            l_employee.find('input[data-input=last-name-1]').val(p_data[1]).prop('disabled', true);
            l_employee.find('input[data-input=last-name-2]').val(p_data[2]).prop('disabled', true);
            l_employee.find('input[data-input=first-name]').val(p_data[3]).prop('disabled', true);
            l_employee.find('input[data-input=email]').val('').prop('disabled', false);
            l_employee.find('ul[data-object=phones] input[data-input=phone]').val('').prop('disabled', false);
            l_employee.attr('data-pers', p_data[0]);
            l_employee.find("input[data-input=codi_pers]").attr("data-userid", p_data[11]);
        }
        function fn_set_employee_data_by_document(p_data, p_o) {
            if (p_data.length > 0) {
                if (p_data[0] == -1) {
                    fn_clear_employee(p_o);
                    $.jGrowl(p_data[1], {
                        header: 'Validación'
                    });
                } else {
                    fn_retrieve_employee(p_data, p_o);
                }
            } else {
                fn_new_employee(p_o);
            }
        }
        function fn_validate_employee_license_to_retrieve_data(p_this) {
            var l_number = $(p_this).val();
            if (gf_isValue(l_number)) {
                var l_list = l_number.match(/\d+/g), l_document_number = l_list ? l_list.join('') : '';
                $(p_this).closest('tr[data-object=employee]').find('input[data-input=document-number]').val(l_document_number);
                fn_validate_employee_document_to_retrieve_data($(p_this));
            }
        }
        function fn_set_license_number(p_number, p_o) {
            var l_license = p_o.closest('tr[data-object=employee]').find('input[data-input=license-number]'), l_number = l_license.val();
            if (gf_isValue(l_number)) {
                var l_list = l_number.match(/[a-z]+/gi), l_letters = l_list ? l_list.join('') : '';
                l_license.val(l_letters + p_number);
            }
        }
        //init retention
        $('#provider_edit_retention').data('lastValue', p_model.Retention);
        $('#provider_edit_has_retention').trigger('change');
        //init phones
        for (var i = 0; i < p_model.Phones.length; i++) {
            if (i == 0) {
                l_phones_container.find('input[data-input=phone]').val(p_model.Phones[i][1]).attr('data-ptel', p_model.Phones[i][0]);
            } else {
                fn_add_phone(p_model.Phones[i]);
            }
        }
        //init emails
        for (var i = 0; i < p_model.Emails.length; i++) {
            fn_add_email(p_model.Emails[i]);
        }
        //init employees
        (function () {
            var l_last_employee = null, l_phones = null;
            for (var i = 0; i < p_model.Employees.length; i++) {
                if (!l_last_employee) {
                    l_last_employee = p_model.Employees[i];
                    l_phones = [];
                }
                if (l_last_employee && p_model.Employees[i][0] != l_last_employee[0]) {
                    if (l_phones) {
                        fn_add_employee(l_last_employee, l_phones);
                    }
                    l_last_employee = p_model.Employees[i];
                    l_phones = [];
                }
                l_phones.push({ 'phone': p_model.Employees[i][9], 'ctel': p_model.Employees[i][8] });
                if (i == p_model.Employees.length - 1) {
                    if (l_phones) {
                        fn_add_employee(l_last_employee, l_phones);
                    }
                }
            }
        }());
        //init vehicles
        for (var i in p_model.Vehicles) {
            fn_add_vehicle(p_model.Vehicles[i]);
        }
        //init accounts
        for (var i in p_model.Accounts) {
            fn_add_account(p_model.Accounts[i]);
        }
        //init
        $("#provider_edit_ruc, #provider_edit_sap_code, #provider_edit_business_name, #provider_edit_email, #provider_edit_incoterms").data('PresaveRequired', true);
        $("#provider_edit_ruc, #provider_edit_sap_code").data({
            PresaveJson: '{"maxLength":20}'
        });
        $("#provider_edit_business_name, #provider_edit_address, #provider_edit_email").data({
            PresaveJson: '{"maxLength":200}'
        });
        $("#provider_edit_retention").data({
            "PresaveList": "numeric",
            "PresaveJson": '{"min":0}'
        });
        if (p_model.IsProviderProfile) {
            if (p_model.IncotermsId == l_term_FOB) {
                $("a[href='#employees']").trigger('click');
                $("a[href='#cars'], a[href='#documents'], a[href='#accounts']").hide();
            }
        } else {
            gf_Helpers_IncotermsDropDown({
                $: $("#provider_edit_incoterms"),
                callback: function (o) {
                    o.addClass("selectpicker").val(p_model.IncotermsId).selectpicker();
                    o.trigger('change');
                },
                options: [{ 'value': 0, 'text': 'Ninguno' }]
            });
        }
    }(gf_getClone(ngon.Model)));
});