﻿"use strict";
$("document").ready(function () {
    (function (p_model) {
        var l_employee_dataTable_documents, l_employee_dataTable_employees, l_employee_temp_employee, l_employee_all_data = {};
        var l_vehicle_dataTable_documents, l_vehicle_dataTable_vehicles, l_vehicle_temp_vehicle, l_vehicle_all_data = {};
        //events
        $('#provider_detail_employee_table tbody').on('click', 'td[data-action=select-employee]', function (evt) {
            evt.preventDefault();
            var l_tr = $(this).closest('tr'), l_data = l_employee_dataTable_employees.row(l_tr).data();
            l_employee_dataTable_employees.rows().nodes().to$().removeClass('cls-1902221730');
            l_tr.addClass('cls-1902221730');
            $('#provider_detail_employee_fullname').text(l_data[3]);
            $('#provider_detail_employee_document').text(l_data[1] + ': ' + l_data[2]);
            //save employee data
            if (!l_employee_all_data[l_data[0]]) {
                l_employee_all_data[l_data[0]] = {
                    'iPcon': l_data[0],
                    'oHtmlDocument': {},
                    'oInitialDocument': gf_getClone(l_data[7])
                };
            }
            //save temp employee
            l_employee_temp_employee = l_data;
            fn_employee_datatable_documents(p_model.EmployeesDocuments);
        });
        $('#provider_detail_employee_table_document tbody').on('click', 'i[data-action=detail]', function (evt) {
            evt.preventDefault();
            var l_this = $(this), l_tr = l_this.closest('tr'), l_row = l_employee_dataTable_documents.row(l_tr);
            if (l_row.child.isShown()) {
                l_this
                    .removeClass('fa-minus-square-o')
                    .addClass('fa-plus-square-o');
                l_row.child.remove();
            }
            else {
                l_this
                    .removeClass('fa-plus-square-o')
                    .addClass('fa-minus-square-o');
                l_row.child('', 'cls-1901280028').show();
                fn_employee_get_detail(l_row);
            }
        });
        $('#provider_detail_employee_table_export_btn').click(function (evt) {
            evt.preventDefault();
            $('#provider_detail_employee_table_export button.buttons-excel').trigger('click');
        });
        $('#provider_detail_vehicle_table tbody').on('click', 'td[data-action=select-vehicle]', function (evt) {
            evt.preventDefault();
            var l_tr = $(this).closest('tr'), l_data = l_vehicle_dataTable_vehicles.row(l_tr).data();
            l_vehicle_dataTable_vehicles.rows().nodes().to$().removeClass('cls-1902221730');
            l_tr.addClass('cls-1902221730');
            $('#provider_detail_vehicle_description').text(l_data[1]);
            //save vehicle data
            if (!l_vehicle_all_data[l_data[0]]) {
                l_vehicle_all_data[l_data[0]] = {
                    'iPrun': l_data[0],
                    'oHtmlDocument': {},
                    'oInitialDocument': gf_getClone(l_data[5])
                };
            }
            //save temp vehicle
            l_vehicle_temp_vehicle = l_data;
            fn_vehicle_datatable_documents(p_model.VehiclesDocuments);
        });
        $('#provider_detail_vehicle_table_document tbody').on('click', 'i[data-action=detail]', function (evt) {
            evt.preventDefault();
            var l_this = $(this), l_tr = l_this.closest('tr'), l_row = l_vehicle_dataTable_documents.row(l_tr);
            if (l_row.child.isShown()) {
                l_this
                    .removeClass('fa-minus-square-o')
                    .addClass('fa-plus-square-o');
                l_row.child.remove();
            }
            else {
                l_this
                    .removeClass('fa-plus-square-o')
                    .addClass('fa-minus-square-o');
                l_row.child('', 'cls-1901280028').show();
                fn_vehicle_get_detail(l_row);
            }
        });
        $('#provider_detail_vehicle_table_export_btn').click(function (evt) {
            evt.preventDefault();
            $('#provider_detail_vehicle_table_export button.buttons-excel').trigger('click');
        });
        //fn
        function fn_employee_get_document_status(p_document) {
            var l_documents = l_employee_temp_employee[7], l_pending = null, l_status = null, l_html;
            for (var i in l_documents) {
                if (l_documents[i][1] == p_document[0]) {
                    l_pending = l_documents[i][8];
                    l_status = l_documents[i][10];
                }
            }
            l_html = `<div class="cls-1903022357">`;
            if (l_pending && l_pending == 'P') {
                l_html += `<span style="display: none;">PA</span><span class="glyphicon glyphicon-font app-center" title="Pendiente de Aprobación"></span>`;
            }
            if (l_status && l_pending && l_pending == 'A') {
                if (l_status == 'V') {
                    l_html += `<span style="display: none;">V</span>
                                <div class="app-center app-state" style="background-color: #4caf50;">
                                    <i class="fa fa-check"></i>
                                </div>`;
                } else if (l_status == 'A') {
                    l_html += `<span style="display: none;">A</span>
                                <div class="app-center app-state" style="background-color: #ff9800;">
                                    <i class="fa fa-info"></i>
                                </div>`;
                } else if (l_status == 'R') {
                    l_html += `<span style="display: none;">R</span>
                                <div class="app-center app-state" style="background-color: #f44336;">
                                    <i class="fa fa-times"></i>
                                </div>`;
                } else if (l_status == 'P') {
                    l_html += `<span class="color-info-inverse d-inline-block">P. Aprobación</span>`;
                } else {
                    l_html += `<span class="color-danger-inverse d-inline-block">Sin Documentos</span>`;
                }
            }
            l_html += `</div>`;
            return l_html;
        }
        function fn_employee_datatable_documents(p_data) {
            l_employee_dataTable_documents = $('#provider_detail_employee_table_document').daDataTable({
                "DataTable": {
                    "data": p_data,
                    "aoColumns": [
                        {
                            "data": null,
                            "orderable": false,
                            "searchable": false,
                            "className": "text-center cls-1901280053",
                            "width": "10px",
                            "mRender": function (o) {
                                return '<i class="fa fa-plus-square-o cursor-pointer" data-action="detail"></i>';
                            }
                        },
                        {
                            "data": null,
                            "className": "text-left app-row",
                            "mRender": function (o) {
                                return o[1] + (p_model.IsLogisticaAdminProfile && gf_is_logistica_document(o[0], p_model.LogisticaEmployeesDocuments) ? fn_get_icon_logistica() : ``);
                            }
                        },
                        {
                            "data": 7,
                            "className": "text-left"
                        },
                        {
                            "data": null,
                            "className": "text-center position-relative",
                            "mRender": function (o) {
                                return fn_employee_get_document_status(o);
                            }
                        }
                    ],
                    "order": [],
                    "processing": true,
                    "bLengthChange": false,
                    "iDisplayLength": 15,
                    //"paging": false,
                    "fnRowCallback": function (nRow, aData) {
                        $(nRow).addClass('cls-1902221730');
                    }
                },
                "theme": "devexpress",
                "imgProcessing": "/assets/dalba/img/DXR.gif",
                "search": {
                    "btn": null,
                    "noSearch": [0, 3]
                }
            }).Datatable;
        }
        function fn_employee_get_detail(p_row) {
            var l_content = p_row.child().find('td'), l_employee = l_employee_all_data[l_employee_temp_employee[0]], l_initial = true, l_has_documents;
            if (!l_employee.oHtmlDocument[p_row.data()[0]]) {
                l_has_documents = false;
                //get documents
                l_employee.oHtmlDocument[p_row.data()[0]] = $('<div />');
                for (var i = 0; i < l_employee.oInitialDocument.length; i++) {
                    if (p_row.data()[0] == l_employee.oInitialDocument[i][1]) {
                        var l_html = $(fn_employee_get_document_html(p_row.data(), l_employee.oInitialDocument[i], l_initial));
                        l_has_documents = true;
                        //save data
                        l_html.attr({ 'data-tdos': p_row.data()[0], 'data-pcdo': l_employee.oInitialDocument[i][0], 'data-status': l_employee.oInitialDocument[i][8] });
                        //disable to approve
                        if (l_employee.oInitialDocument[i][8] == 'A') {
                            l_html.find('input').each(function () {
                                $(this).attr({
                                    'data-initialValue': $(this).val(),
                                    'data-disabled': true
                                });
                            });
                        }
                        //
                        l_employee.oHtmlDocument[p_row.data()[0]].append(l_html);
                        l_initial = false;
                    }
                }
                if (!l_has_documents) {
                    var l_html = $(fn_employee_get_document_html(p_row.data(), null, l_initial));
                    //save data
                    l_html.attr({ 'data-tdos': p_row.data()[0], 'data-pcdo': 0, 'data-status': 'X' });
                    //
                    l_employee.oHtmlDocument[p_row.data()[0]].append(l_html);
                }
            }
            l_content.append(l_employee.oHtmlDocument[p_row.data()[0]]);
        }
        function fn_employee_get_document_html(p_document, p_data, p_initial, p_massive) {
            var l_html_dates = null, l_html_number = null, l_html_download = null, l_html_status = null, l_html_reason = null, l_html;
            if (p_document) {
                //build dates
                if (p_document[4] == 'S') {
                    l_html_dates = `<div class="col-lg-3 col-md-6 col-sm-6 col-6 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <div class="cls-1902141431 mt-0 text-nowrap">` + (p_data ? p_data[4] : ``) + `</div>
                                            <div class="icon-input small-calendar cls-1902240417" title="..."></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-6 col-6 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <div class="cls-1902141431 mt-0 text-nowrap">` + (p_data ? p_data[5] : ``) + `</div>
                                            <div class="icon-input small-calendar cls-1902240417" title="..."></div>
                                        </div>
                                    </div>`;
                }
                //build number
                if (p_document[8] == 'S') {
                    l_html_number = `<div class="col-lg-3 col-md-6 col-sm-6 col-12 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <div class="cls-1902141431 mt-0 text-nowrap">` + (p_data ? p_data[13] : ``) + `</div>
                                        </div>
                                    </div>`;
                }
            }
            if (p_data) {
                //build download
                if (p_data[2] != "") {
                    l_html_download = `<a href="` + p_data[14] + `" download class="btn btn-sm btn-raised btn-info m-0 pt-0 pb-0 position-absolute cls-1902240557" data-toggle="tooltip" data-placement="top" title="Descargar">
                                            <i class="icon ion-ios-cloud-download m-0"></i>
                                        </a>`;
                }
                //build status
                if (p_data[8] == 'A') {
                    l_html_status = `<div class="color-success-inverse pull-left">Aprobado</div>`;
                } else if (p_data[8] == 'P') {
                    l_html_status = `<div class="color-info-inverse pull-left">P. Aprobación</div>`;
                } else if (p_data[8] == 'R') {
                    l_html_status = `<div class="color-danger-inverse pull-left">Rechazado</div>`;
                }
                //build reason
                if (p_data[8] == "R") {
                    l_html_reason = `<div class="row cls-1902240436" data-object="reason">
                                        <div class="col-12 cls-1902240431">
                                            <div class="form-group mt-0">
                                                <label class="control-label m-0"><span class="text-dark">Motivo de Rechazo</span></label>
                                                <div class="cls-1904291750">` + p_data[15] + `</div>
                                            </div>
                                        </div>
                                    </div>`;
                }
            }
            //build html
            var l_html = `<div class="cls-1901280058" data-object="sustenance-document">
                            <div class="cls-1902240408 pb-0">
                                <div class="app-row mb-05">
                                    ` + (l_html_status ? l_html_status : ``) + `
                                </div>
                                <div class="row cls-1902240436">
                                    ` + (l_html_number ? l_html_number : ``) + `
                                    <div class="`+ (l_html_number && l_html_dates ? `col-lg-3 col-md-6 col-sm-6` : (l_html_number && !l_html_dates ? `col-lg-9 col-md-6 col-sm-6` : (!l_html_number && l_html_dates ? `col-lg-6 col-md-12 col-sm-12` : ``))) + ` col-12 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <div class="cls-1902141431 mt-0 text-nowrap">` + (p_data ? p_data[11] : ``) + `</div>
                                            ` + (l_html_download ? l_html_download : ``) + `
                                        </div>
                                    </div>
                                    ` + (l_html_dates ? l_html_dates : ``) + `
                                </div>
                                ` + (l_html_reason ? l_html_reason : ``) + `
                            </div>
                        </div>`;
            return l_html;
        }
        function fn_vehicle_get_document_status(p_document) {
            var l_documents = l_vehicle_temp_vehicle[5], l_pending = null, l_status = null, l_html;
            for (var i in l_documents) {
                if (l_documents[i][1] == p_document[0]) {
                    l_pending = l_documents[i][8];
                    l_status = l_documents[i][10];
                }
            }
            l_html = `<div class="cls-1903022357">`;
            if (l_pending && l_pending == 'P') {
                l_html += `<span style="display: none;">PA</span><span class="glyphicon glyphicon-font app-center" title="Pendiente de Aprobación"></span>`;
            }
            if (l_status && l_pending && l_pending == 'A') {
                if (l_status == 'V') {
                    l_html += `<span style="display: none;">V</span>
                                <div class="app-center app-state" style="background-color: #4caf50;">
                                    <i class="fa fa-check"></i>
                                </div>`;
                } else if (l_status == 'A') {
                    l_html += `<span style="display: none;">A</span>
                                <div class="app-center app-state" style="background-color: #ff9800;">
                                    <i class="fa fa-info"></i>
                                </div>`;
                } else if (l_status == 'R') {
                    l_html += `<span style="display: none;">R</span>
                                <div class="app-center app-state" style="background-color: #f44336;">
                                    <i class="fa fa-times"></i>
                                </div>`;
                } else if (l_status == 'P') {
                    l_html += `<span class="color-info-inverse d-inline-block">P. Aprobación</span>`;
                } else {
                    l_html += `<span class="color-danger-inverse d-inline-block">Sin Documentos</span>`;
                }
            }
            l_html += `</div>`;
            return l_html;
        }
        function fn_vehicle_datatable_documents(p_data) {
            l_vehicle_dataTable_documents = $('#provider_detail_vehicle_table_document').daDataTable({
                "DataTable": {
                    "data": p_data,
                    "aoColumns": [
                        {
                            "data": null,
                            "orderable": false,
                            "searchable": false,
                            "className": "text-center cls-1901280053",
                            "width": "10px",
                            "mRender": function (o) {
                                return '<i class="fa fa-plus-square-o cursor-pointer" data-action="detail"></i>';
                            }
                        },
                        {
                            "data": null,
                            "className": "text-left app-row",
                            "mRender": function (o) {
                                return o[1] + (p_model.IsLogisticaAdminProfile && gf_is_logistica_document(o[0], p_model.LogisticaVehiclesDocuments) ? fn_get_icon_logistica() : ``);
                            }
                        },
                        {
                            "data": 7,
                            "className": "text-left"
                        },
                        {
                            "data": null,
                            "className": "text-center position-relative",
                            "mRender": function (o) {
                                return fn_vehicle_get_document_status(o);
                            }
                        }
                    ],
                    "order": [],
                    "processing": true,
                    "bLengthChange": false,
                    "iDisplayLength": 15,
                    //"paging": false,
                    "fnRowCallback": function (nRow, aData) {
                        $(nRow).addClass('cls-1902221730');
                    }
                },
                "theme": "devexpress",
                "imgProcessing": "/assets/dalba/img/DXR.gif",
                "search": {
                    "btn": null,
                    "noSearch": [0, 3]
                }
            }).Datatable;
        }
        function fn_vehicle_get_detail(p_row) {
            var l_content = p_row.child().find('td'), l_vehicle = l_vehicle_all_data[l_vehicle_temp_vehicle[0]], l_initial = true, l_has_documents;
            if (!l_vehicle.oHtmlDocument[p_row.data()[0]]) {
                l_has_documents = false;
                //get documents
                l_vehicle.oHtmlDocument[p_row.data()[0]] = $('<div />');
                for (var i = 0; i < l_vehicle.oInitialDocument.length; i++) {
                    if (p_row.data()[0] == l_vehicle.oInitialDocument[i][1]) {
                        var l_html = $(fn_vehicle_get_document_html(p_row.data(), l_vehicle.oInitialDocument[i], l_initial));
                        l_has_documents = true;
                        //save data
                        l_html.attr({ 'data-tdos': p_row.data()[0], 'data-pcdo': l_vehicle.oInitialDocument[i][0], 'data-status': l_vehicle.oInitialDocument[i][8] });
                        //disable to approve
                        if (l_vehicle.oInitialDocument[i][8] == 'A') {
                            l_html.find('input').each(function () {
                                $(this).attr({
                                    'data-initialValue': $(this).val(),
                                    'data-disabled': true
                                });
                            });
                        }
                        //
                        l_vehicle.oHtmlDocument[p_row.data()[0]].append(l_html);
                        l_initial = false;
                    }
                }
                if (!l_has_documents) {
                    var l_html = $(fn_vehicle_get_document_html(p_row.data(), null, l_initial));
                    //save data
                    l_html.attr({ 'data-tdos': p_row.data()[0], 'data-pcdo': 0, 'data-status': 'X' });
                    //
                    l_vehicle.oHtmlDocument[p_row.data()[0]].append(l_html);
                }
            }
            l_content.append(l_vehicle.oHtmlDocument[p_row.data()[0]]);
        }
        function fn_vehicle_get_document_html(p_document, p_data, p_initial, p_massive) {
            var l_html_dates = null, l_html_number = null, l_html_download = null, l_html_status = null, l_html_reason = null, l_html;
            if (p_document) {
                //build dates
                if (p_document[4] == 'S') {
                    l_html_dates = `<div class="col-lg-3 col-md-6 col-sm-6 col-6 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <div class="cls-1902141431 mt-0 text-nowrap">` + (p_data ? p_data[4] : ``) + `</div>
                                            <div class="icon-input small-calendar cls-1902240417" title="..."></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-6 col-6 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <div class="cls-1902141431 mt-0 text-nowrap">` + (p_data ? p_data[5] : ``) + `</div>
                                            <div class="icon-input small-calendar cls-1902240417" title="..."></div>
                                        </div>
                                    </div>`;
                }
                //build number
                if (p_document[8] == 'S') {
                    l_html_number = `<div class="col-lg-3 col-md-6 col-sm-6 col-12 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <div class="cls-1902141431 mt-0 text-nowrap">` + (p_data ? p_data[13] : ``) + `</div>
                                        </div>
                                    </div>`;
                }
            }
            if (p_data) {
                //build download
                if (p_data[2] != "") {
                    l_html_download = `<a href="` + p_data[14] + `" download class="btn btn-sm btn-raised btn-info m-0 pt-0 pb-0 position-absolute cls-1902240557" data-toggle="tooltip" data-placement="top" title="Descargar">
                                            <i class="icon ion-ios-cloud-download m-0"></i>
                                        </a>`;
                }
                //build status
                if (p_data[8] == 'A') {
                    l_html_status = `<div class="color-success-inverse pull-left">Aprobado</div>`;
                } else if (p_data[8] == 'P') {
                    l_html_status = `<div class="color-info-inverse pull-left">P. Aprobación</div>`;
                } else if (p_data[8] == 'R') {
                    l_html_status = `<div class="color-danger-inverse pull-left">Rechazado</div>`;
                }
                //build reason
                if (p_data[8] == "R") {
                    l_html_reason = `<div class="row cls-1902240436" data-object="reason">
                                        <div class="col-12 cls-1902240431">
                                            <div class="form-group mt-0">
                                                <label class="control-label m-0"><span class="text-dark">Motivo de Rechazo</span></label>
                                                <div class="cls-1904291750">` + p_data[15] + `</div>
                                            </div>
                                        </div>
                                    </div>`;
                }
            }
            //build html
            var l_html = `<div class="cls-1901280058" data-object="sustenance-document">
                            <div class="cls-1902240408 pb-0">
                                <div class="app-row mb-05">
                                    ` + (l_html_status ? l_html_status : ``) + `
                                </div>
                                <div class="row cls-1902240436">
                                    ` + (l_html_number ? l_html_number : ``) + `
                                    <div class="`+ (l_html_number && l_html_dates ? `col-lg-3 col-md-6 col-sm-6` : (l_html_number && !l_html_dates ? `col-lg-9 col-md-6 col-sm-6` : (!l_html_number && l_html_dates ? `col-lg-6 col-md-12 col-sm-12` : ``))) + ` col-12 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <div class="cls-1902141431 mt-0 text-nowrap">` + (p_data ? p_data[11] : ``) + `</div>
                                            ` + (l_html_download ? l_html_download : ``) + `
                                        </div>
                                    </div>
                                    ` + (l_html_dates ? l_html_dates : ``) + `
                                </div>
                                ` + (l_html_reason ? l_html_reason : ``) + `
                            </div>
                        </div>`;
            return l_html;
        }
        function fn_get_icon_logistica() {
            return `<div class="pull-right position-relative app-state" style="background-color: #11A9CC;">
                        <span class="cls-1903021233">L</span>
                    </div>`;
        }
        function fn_employee_get_icon_capacitado() {
            return `<div class="pull-right position-relative app-state" style="background-color: #FFB700;">
                        <span class="cls-1903021233">C</span>
                    </div>`;
        }
        //init
        l_employee_dataTable_employees = $('#provider_detail_employee_table').daDataTable({
            "DataTable": {
                "data": p_model.Employees,
                "aoColumns": [
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "position-relative cls-1902191550 cls-1902240273",
                        "mRender": function (o) {
                            return `<a href="javascript:void(0)" class="m-0 cursor-default">
                                        <img src="` + (o[4] == "" ? `/assets/app/img/default.png` : o[5]) + `" alt="..." class="app-center cls-1901280906" />
                                    </a>`;
                        }
                    },
                    {
                        "data": null,
                        "className": "text-left align-top cursor-pointer app-row",
                        "mRender": function (o) {
                            return o[3] + (p_model.IsLogisticaAdminProfile && $.inArray(o[0], p_model.EmployeesTrained) != -1 ? fn_employee_get_icon_capacitado() : ``);
                        }
                    },
                    {
                        "data": null,
                        "className": "text-center align-top position-relative cursor-pointer",
                        "mRender": function (o) {
                            if (o[6] == 'V') {
                                return `<span style="display: none;">V</span>
                                        <div class="app-center app-state" style="background-color: #4caf50;">
                                            <i class="fa fa-check"></i>
                                        </div>`;
                            } else if (o[6] == 'A') {
                                return `<span style="display: none;">A</span>
                                        <div class="app-center app-state" style="background-color: #ff9800;">
                                            <i class="fa fa-info"></i>
                                        </div>`;
                            } else if (o[6] == 'R') {
                                return `<span style="display: none;">R</span>
                                        <div class="app-center app-state" style="background-color: #f44336;">
                                            <i class="fa fa-times"></i>
                                        </div>`;
                            } else if (o[6] == 'P') {
                                return `<span class="color-info-inverse d-inline-block">P. Aprobación</span>`;
                            } else {
                                return `<span class="color-danger-inverse d-inline-block">Sin Documentos</span>`;
                            }
                        }
                    }
                ],
                "order": [[1, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 15,
                //"paging": false,
                "fnRowCallback": function (nRow, aData) {
                    $(nRow).find('td').eq(1).attr('data-action', 'select-employee');
                    $(nRow).find('td').eq(2).attr('data-action', 'select-employee');
                }
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": null,
                "noSearch": [0]
            }
        }).Datatable;
        //buttons
        new $.fn.dataTable.Buttons(l_employee_dataTable_employees, {
            'buttons': [{
                'extend': 'excelHtml5',
                'text': 'export',
                'title': 'Trabajador',
                'className': 'd-none',
                'exportOptions': {
                    'columns': [1, 2],
                    'format': {
                        'body': function (data, row, column, node) {
                            var l_data = l_employee_dataTable_employees.row($(node).closest('tr')).data();
                            if (column == 0) {
                                return l_data[3];
                            } else if (column == 1) {
                                return l_data[6] == 'V' ? 'Conforme' : l_data[6] == 'A' ? 'Por Vencer' : l_data[6] == 'R' ? 'Vencido' : l_data[6] == 'P' ? 'P. Aprobación' : 'Sin Documentos';
                            }
                        }
                    }
                }
            }]
        }).container().appendTo($('#provider_detail_employee_table_export'));
        fn_employee_datatable_documents([]);
        l_vehicle_dataTable_vehicles = $('#provider_detail_vehicle_table').daDataTable({
            "DataTable": {
                "data": p_model.Vehicles,
                "aoColumns": [
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "position-relative cls-1902191550 cls-1902240273",
                        "mRender": function (o) {
                            return `<a href="javascript:void(0)" class="m-0 cursor-default">
                                        <img src="` + (o[2] == "" ? `/assets/site/assets/images/noimage.png` : o[3]) + `" alt="..." class="app-center cls-1901280906" />
                                    </a>`;
                        }
                    },
                    {
                        "data": 1,
                        "className": "text-left align-top cursor-pointer"
                    },
                    {
                        "data": null,
                        "className": "text-center align-top position-relative cursor-pointer",
                        "mRender": function (o) {
                            if (o[4] == 'V') {
                                return `<span style="display: none;">V</span>
                                        <div class="app-center app-state" style="background-color: #4caf50;">
                                            <i class="fa fa-check"></i>
                                        </div>`;
                            } else if (o[4] == 'A') {
                                return `<span style="display: none;">A</span>
                                        <div class="app-center app-state" style="background-color: #ff9800;">
                                            <i class="fa fa-info"></i>
                                        </div>`;
                            } else if (o[4] == 'R') {
                                return `<span style="display: none;">R</span>
                                        <div class="app-center app-state" style="background-color: #f44336;">
                                            <i class="fa fa-times"></i>
                                        </div>`;
                            } else if (o[4] == 'P') {
                                return `<span class="color-info-inverse d-inline-block">P. Aprobación</span>`;
                            } else {
                                return `<span class="color-danger-inverse d-inline-block">Sin Documentos</span>`;
                            }
                        }
                    }
                ],
                "order": [[1, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 15,
                //"paging": false,
                "fnRowCallback": function (nRow, aData) {
                    $(nRow).find('td').eq(1).attr('data-action', 'select-vehicle');
                    $(nRow).find('td').eq(2).attr('data-action', 'select-vehicle');
                }
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": null,
                "noSearch": [0]
            }
        }).Datatable;
        //buttons
        new $.fn.dataTable.Buttons(l_vehicle_dataTable_vehicles, {
            'buttons': [{
                'extend': 'excelHtml5',
                'text': 'export',
                'title': 'Vehículo',
                'className': 'd-none',
                'exportOptions': {
                    'columns': [1, 2],
                    'format': {
                        'body': function (data, row, column, node) {
                            var l_data = l_vehicle_dataTable_vehicles.row($(node).closest('tr')).data();
                            if (column == 0) {
                                return l_data[1];
                            } else if (column == 1) {
                                return l_data[4] == 'V' ? 'Conforme' : l_data[4] == 'A' ? 'Por Vencer' : l_data[4] == 'R' ? 'Vencido' : l_data[4] == 'P' ? 'P. Aprobación' : 'Sin Documentos';
                            }
                        }
                    }
                }
            }]
        }).container().appendTo($('#provider_detail_vehicle_table_export'));
        fn_vehicle_datatable_documents([]);
    }(gf_getClone(ngon.Model)));
});