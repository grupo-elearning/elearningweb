﻿"use strict";
$('document').ready(function () {
    var l_dataTable, l_param = { 'Filter_Type': 'X', 'Filter_RUC': '0' }, l_term_FOB = gf_Helpers_GetParameterByKey('CODI_TERM_FOB');
    //events
    $('#provider_index_filter_type').change(function () {
        var l_filter = $(this).val();
        if (l_filter == "all") {
            $('#provider_index_filter_container_ruc').hide();
        } else if (l_filter == "ruc") {
            $('#provider_index_filter_container_ruc').show();
        } else {
            $('#provider_index_filter_container_ruc').hide();
        }
    });
    $('#provider_index_filter_retrieve').click(function (evt) {
        evt.preventDefault();
        fn_retrieve();
    });
    $('#provider_index_table tbody').on('click', 'i[data-action=detail]', function (evt) {
        evt.preventDefault();
        var l_this = $(this), l_tr = l_this.closest('tr'), l_row = l_dataTable.row(l_tr);
        if (l_row.child.isShown()) {
            l_this
                .removeClass('fa-minus-square-o')
                .addClass('fa-plus-square-o');
            l_row.child.remove();
        }
        else {
            l_this
                .removeClass('fa-plus-square-o')
                .addClass('fa-minus-square-o');
            l_row.child('', 'bg-white').show();
            fn_get_detail(l_row);
        }
    });

    //fn
    function fn_get_detail(p_row) {
        var l_content = p_row.child().find('td');
        $.ajax({
            url: '/Provider/ListDetail',
            data: JSON.stringify({ 'id': p_row.data()[0] }),
            contentType: 'application/json; charset=utf-8',
            type: 'post',
            dataType: 'html',
            beforeSend: function () {
                l_content.html('<div class="text-center small p-1 text-dark">Cargando...</div>');
            },
            success: function (r) {
                l_content.html(r);
            },
            error: function (xhr, status) {
                l_content.html('Error : ' + xhr.status + ', ' + xhr.statusText);
            }
        });
    }
    function fn_retrieve() {
        var l_filter = $('#provider_index_filter_type').val(), l_ruc = $('#provider_index_filter_ruc').val().trim();
        if (l_filter == 'ruc') {
            if (!gf_isValue(l_ruc)) {
                $.jGrowl("Ingrese un número de RUC", {
                    header: "Validación"
                });
                return;
            }
        }
        l_param.Filter_Type = l_filter == 'all' ? 'T' : 'R';
        l_param.Filter_RUC = l_filter == 'all' ? '0' : l_ruc;
        fn_datatable();
    }
    function fn_datatable() {
        l_dataTable = $("#provider_index_table").daDataTable({
            "DataTable": {
                "bServerSide": true,
                "sAjaxSource": "/Provider/GetList",
                "fnServerData": function (sSource, aoData, fnCallback, oSettings) {
                    aoData.push(
                        {
                            "name": "Filter_Type", "value": l_param.Filter_Type
                        },
                        {
                            "name": "Filter_RUC", "value": l_param.Filter_RUC
                        }
                    );
                    oSettings.jqXHR = $.ajax({
                        "dataType": 'json',
                        "type": "post",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    });
                },
                "aoColumns": [
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center cls-1901280053",
                        "width": "10px",
                        "mRender": function (o) {
                            return '<i class="fa fa-plus-square-o cursor-pointer" data-action="detail"></i>';
                        }
                    },
                    {
                        "data": 5,
                        "className": "text-left"
                    },
                    {
                        "data": 3,
                        "className": "text-left"
                    },
                    {
                        "data": 1,
                        "className": "text-center"
                    },
                    {
                        "data": 4,
                        "className": "text-left"
                    },
                    {
                        "data": null,
                        "className": "text-center",
                        "mRender": function (o) {
                            //o[14]
                            return '<label class="switch"><input type="checkbox" codi_prov=' + o[0] + ' data-eini="' + o[14] + '" data-action="logistica"' + (o[14] === "S" ? 'checked = "checked"' : '') + '><span class="slider round"></span></label>';
                        }
                    },
                    {
                        "data": null,
                        "className": "text-center",
                        "mRender": function (o) {
                            if (o[7] == 'A') {
                                return '<span class="color-info">ACTIVO</span>';
                            } else if (o[7] == 'I') {
                                return '<span class="color-danger">INACTIVO</span>';
                            } else {
                                return '[Status]';
                            }
                        }
                    }
                ],
                "order": [[3, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 15
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": { "column": 0 },
                "noSearch": [1]
            }
        }).Datatable;
    }

    $("#btn_provider_save").click(function () {
        var l_list_provider = $(l_dataTable.rows().nodes()).find("input:checkbox[data-action=logistica]");

        var l_param = { Proveedores: new Array() };

        $.each(l_list_provider, function (index, element) {
            var l_codi_prov = $(element).attr("codi_prov");
            var l_estado_ini = $(element).attr("data-eini");
            var l_estado_fin = $(element).prop("checked") ? "S" : "N";

            if (l_estado_ini != l_estado_fin) {
                var l_provider = {
                    "CodiProv": l_codi_prov,
                    "FlagLogistica": l_estado_fin
                };
                l_param.Proveedores.push(l_provider);
            }
        });
        
        if (l_param.Proveedores.length == 0) {
            return;
        }

        $.daAlert({
            content: {
                type: "text",
                value: "<div class='text-center p-05'>¿Está seguro que desea guardar los cambios?</div>"
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "save",
                    fn: function () {
                        $.ajax({
                            data: JSON.stringify(l_param),
                            url: '/Provider/UpdateProvider',
                            type: 'post',
                            dataType: 'json',
                            contentType: "application/json; charset=utf-8",
                            beforeSend: _beforeSend,
                            success: _success
                        });
                        function _beforeSend() {
                            gf_addLoadingTo($("#btn_provider_save"));
                            gf_triggerKeepActive();
                        }
                        function _success(r) {
                            if (parseInt(r.status) == 1000) {
                                $.jGrowl(r.response, {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                //location.href = '/Provider/Index';
                                location.reload(true);
                            } else {
                                $.jGrowl(r.response, {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: r.exception
                                });
                                gf_removeLoadingTo($("#btn_provider_save"));
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: "Confirmación"
        });
    });

    //init
    fn_retrieve();

});