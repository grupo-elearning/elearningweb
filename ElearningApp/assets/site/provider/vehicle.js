﻿"use strict";
$("document").ready(function () {
    (function (p_model) {
        var l_individual_dataTable_documents, l_individual_dataTable_vehicles, l_individual_temp_vehicle, l_individual_all_data = {}, l_individual_pcdo_delete = [];
        var l_massive_dataTable_vehicles;
        //events
        $('#provider_vehicle_individual_table tbody').on('click', 'a[data-action=select-photo]', function (evt) {
            evt.preventDefault();
            $(this).next('input[data-input=photo]').trigger('click');
        });
        $('#provider_vehicle_individual_table tbody').on('change', 'input[data-input=photo]', function () {
            var l_preview = $(this).closest('td').find('img[data-object=photo]');
            var l_validate = gf_fileValidation({
                fileInput: $(this),
                imgPreview: l_preview,
                fnError: function () {
                    $.jGrowl("Solo puede seleccionar archivos tipo: jpg, jpeg, png, bmp", {
                        header: "Validación"
                    });
                }
            });
            if (!l_validate) {
                l_preview.attr("src", "/assets/site/assets/images/noimage.png");
            }
            $(this).attr('data-modified', true);
        });
        $('#provider_vehicle_individual_table tbody').on('click', 'td[data-action=select-vehicle]', function (evt) {
            evt.preventDefault();
            if (fn_individual_has_empty_fields()) {
                return;
            }
            var l_tr = $(this).closest('tr'), l_data = l_individual_dataTable_vehicles.row(l_tr).data();
            l_individual_dataTable_vehicles.rows().nodes().to$().removeClass('cls-1902221730');
            l_tr.addClass('cls-1902221730');
            $('#provider_vehicle_individual_description').text(l_data[1]);
            //save vehicle data
            if (!l_individual_all_data[l_data[0]]) {
                l_individual_all_data[l_data[0]] = {
                    'iPrun': l_data[0],
                    'oHtmlDocument': {},
                    'oInitialDocument': gf_getClone(l_data[5])
                };
            }
            //save temp vehicle
            l_individual_temp_vehicle = l_data;
            fn_individual_datatable_documents(p_model.Documents);
        });
        $('#provider_vehicle_individual_table_document tbody').on('click', 'i[data-action=detail]', function (evt) {
            evt.preventDefault();
            var l_this = $(this), l_tr = l_this.closest('tr'), l_row = l_individual_dataTable_documents.row(l_tr);
            if (l_row.child.isShown()) {
                l_this
                    .removeClass('fa-minus-square-o')
                    .addClass('fa-plus-square-o');
                l_row.child.remove();
            }
            else {
                l_this
                    .removeClass('fa-plus-square-o')
                    .addClass('fa-minus-square-o');
                l_row.child('', 'cls-1901280028').show();
                fn_individual_get_detail(l_row);
            }
        });
        $('#provider_vehicle_individual_table_document tbody').on('click', 'button[data-action=remove-document]', function (evt) {
            evt.preventDefault();
            var l_sustenance = $(this).closest('div[data-object=sustenance-document]');
            if (l_sustenance.attr('data-status') != 'A') {
                fn_individual_save_document_data(l_sustenance);
                l_sustenance.remove();
            }
        });
        $('#provider_vehicle_individual_table_document tbody').on('click', 'button[data-action=clear-document]', function (evt) {
            evt.preventDefault();
            fn_clear_document($(this), true);
        });
        $('#provider_vehicle_individual_table_document tbody').on('change', 'div[data-object=sustenance-document] input[data-input]', function () {
            $(this).closest('div[data-object=sustenance-document]').attr('data-modified', true);
            $(this).closest('div[data-object=sustenance-document]').attr('data-validate', fn_individual_get_validate($(this).closest('div[data-object=sustenance-document]')));
        });
        $('#provider_vehicle_individual_table_export_btn').click(function (evt) {
            evt.preventDefault();
            $('#provider_vehicle_individual_table_export button.buttons-excel').trigger('click');
        });
        $('#provider_vehicle_massive_form').on('click', 'button[data-action=clear-document]', function (evt) {
            evt.preventDefault();
            fn_clear_document($(this));
        });
        $('#provider_vehicle_individual_table_document tbody').on('click', 'button[data-action=add-document]', function (evt) {
            evt.preventDefault();
            var l_sustenance = $(this).closest('div[data-object=sustenance-document]'), l_has_disapproval = false;
            if (l_sustenance.attr('data-status') != 'A') {
                l_has_disapproval = true;
            }
            if (!l_has_disapproval) {
                l_sustenance.nextAll('div[data-object=sustenance-document]').each(function () {
                    if ($(this).attr('data-status') != 'A') {
                        l_has_disapproval = true;
                        return false;
                    }
                });
            }
            if (!l_has_disapproval) {
                var l_vehicle = l_individual_all_data[l_individual_temp_vehicle[0]], l_document, l_html;
                l_document = fn_get_document(l_sustenance.attr('data-tdos'));
                l_html = $(fn_get_document_html(l_document, null, false));
                //save data
                l_html.attr({ 'data-tdos': l_document[0], 'data-pcdo': 0, 'data-status': 'NEW', 'data-modified': false, 'data-validate': false });
                //
                fn_initialize_detail(l_html);
                l_vehicle.oHtmlDocument[l_document[0]].append(l_html);
            }
        });
        $('#provider_vehicle_massive_table_check_all').change(function () {
            l_massive_dataTable_vehicles.cells().nodes().to$().find(':checkbox:checked').prop('checked', false);
            l_massive_dataTable_vehicles.cells().nodes().to$().closest('tr').removeClass('cls-1902221730');
            if ($(this).is(":checked")) {
                l_massive_dataTable_vehicles.rows().nodes().to$().find(':checkbox').prop('checked', true);
                l_massive_dataTable_vehicles.rows().nodes().to$().closest('tr').addClass('cls-1902221730');
            }
        });
        $("#provider_vehicle_massive_table tbody").on('change', 'tr input[type=checkbox]', function () {
            $(this).parents('tr').toggleClass('cls-1902221730');
        });
        $('#provider_vehicle_massive_document').change(function () {
            var l_tdos = $(this).val();
            if (gf_isValue(l_tdos)) {
                var l_document = fn_get_document(l_tdos), l_html;
                l_html = $(fn_get_document_html(l_document, null, true, true));
                //save data
                l_html.attr({ 'data-tdos': l_document[0], 'data-pcdo': 0, 'data-status': 'X', 'data-modified': false, 'data-validate': false });
                //
                fn_initialize_detail(l_html, true);
                $('#provider_vehicle_massive_form').empty().append(l_html);
            }
        });
        $('#provider_vehicle_individual_save').click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_data, l_formdata = new FormData();
            if (fn_individual_has_empty_fields()) {
                return;
            }
            l_data = fn_individual_get_data();
            if (!l_data) {
                return;
            }
            //provider
            l_formdata.append('ProviderId', p_model.Provider.Id);
            //vehicle
            for (var i = 0; i < l_data.VehiclePrun.length; i++) {
                l_formdata.append('VehiclePrun[' + i + ']', l_data.VehiclePrun[i]);
                l_formdata.append('VehicleFile[' + i + '].File', l_data.VehicleFile[i]);
            }
            //vehicle document
            for (var i = 0; i < l_data.VehicleDocumentPrun.length; i++) {
                l_formdata.append('VehicleDocumentPrun[' + i + ']', l_data.VehicleDocumentPrun[i]);
                l_formdata.append('VehicleDocumentPcdo[' + i + ']', l_data.VehicleDocumentPcdo[i]);
                l_formdata.append('VehicleDocumentTdos[' + i + ']', l_data.VehicleDocumentTdos[i]);
                l_formdata.append('VehicleDocumentNumber[' + i + ']', l_data.VehicleDocumentNumber[i]);
                l_formdata.append('VehicleDocumentFile[' + i + '].File', l_data.VehicleDocumentFile[i]);
                l_formdata.append('VehicleDocumentIssueDate[' + i + ']', (l_data.VehicleDocumentIssueDate[i] ? l_data.VehicleDocumentIssueDate[i].format('DD/MM/YYYY') : '0'));
                l_formdata.append('VehicleDocumentExpirationDate[' + i + ']', (l_data.VehicleDocumentExpirationDate[i] ? l_data.VehicleDocumentExpirationDate[i].format('DD/MM/YYYY') : '0'));
                l_formdata.append('VehicleDocumentOldFileName[' + i + ']', l_data.VehicleDocumentOldFileName[i]);
                l_formdata.append('VehicleDocumentOldFileRealName[' + i + ']', l_data.VehicleDocumentOldFileRealName[i]);
            }
            //delete
            if (l_individual_pcdo_delete.length == 0) {
                l_individual_pcdo_delete.push({ 'pcdo': 0, 'name': '' });
            }
            for (var i = 0; i < l_individual_pcdo_delete.length; i++) {
                l_formdata.append('DeleteDocumentPcdo[' + i + ']', l_individual_pcdo_delete[i].pcdo);
                l_formdata.append('DeleteDocumentName[' + i + ']', l_individual_pcdo_delete[i].name);
            }
            $.daAlert({
                content: {
                    type: "text",
                    value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "save",
                        fn: function () {
                            $.ajax({
                                data: l_formdata,
                                url: '/Provider/Vehicle',
                                type: 'post',
                                dataType: 'json',
                                contentType: false,
                                processData: false,
                                beforeSend: _beforeSend,
                                success: _success
                            });
                            function _beforeSend() {
                                gf_addLoadingTo($("#provider_vehicle_individual_save"));
                                gf_triggerKeepActive();
                            }
                            function _success(r) {
                                if (parseInt(r.status) == 1000) {
                                    $.jGrowl(r.response, {
                                        header: 'Información',
                                        type: 'primary'
                                    });
                                    //location.href = '/Provider/Index';
                                    location.reload(true);
                                } else {
                                    $.jGrowl(r.response, {
                                        header: 'Error',
                                        type: 'danger',
                                        clipboard: r.exception
                                    });
                                    gf_removeLoadingTo($("#provider_vehicle_individual_save"));
                                }
                            }
                            return true;
                        }
                    }
                ],
                maximizable: false,
                title: "Confirmación"
            });
        });
        $('#provider_vehicle_massive_save').click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_document, l_vehicle, l_formdata = new FormData();
            l_document = fn_massive_get_data_document();
            if (!l_document) {
                return;
            }
            l_vehicle = fn_massive_get_data_vehicle();
            if (!l_vehicle) {
                return;
            }
            //document
            l_formdata.append('ProviderId', l_document.ProviderId);
            l_formdata.append('DocumentId', l_document.DocumentId);
            l_formdata.append('IssueDate', l_document.IssueDate);
            l_formdata.append('ExpirationDate', l_document.ExpirationDate);
            l_formdata.append('DocumentNumber', l_document.DocumentNumber);
            l_formdata.append('DocumentFile.File', l_document.DocumentFile);
            //Vehicle
            for (var i = 0; i < l_vehicle.VehicleId.length; i++) {
                l_formdata.append('VehicleId[' + i + ']', l_vehicle.VehicleId[i]);
            }
            $.daAlert({
                content: {
                    type: "text",
                    value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "save",
                        fn: function () {
                            var l_error = false;
                            $.ajax({
                                data: JSON.stringify({ 'DocumentId': l_document.DocumentId, 'VehicleId': l_vehicle.VehicleId }),
                                url: '/Provider/VehicleMassiveValidate',
                                type: 'post',
                                dataType: 'json',
                                contentType: "application/json; charset=utf-8",
                                beforeSend: function () {
                                    gf_addLoadingTo($("#provider_vehicle_massive_save"));
                                    gf_triggerKeepActive();
                                },
                                success: function (r) {
                                    if (r.status == 1) {
                                        for (var i in r.response) {
                                            if (r.response[i][1] == 'P') {
                                                $.jGrowl(`El Vehículo '` + r.response[i][2] + `' tiene el documento Pendiente de Aprobación, no se puede insertar uno nuevo`, {
                                                    header: 'Validación'
                                                });
                                                l_error = true;
                                                break;
                                            } else if (r.response[i][1] == 'R') {
                                                $.jGrowl(`El Vehículo '` + r.response[i][2] + `' tiene el documento Rechazado, no se puede insertar uno nuevo`, {
                                                    header: 'Validación'
                                                });
                                                l_error = true;
                                                break;
                                            } else if (r.response[i][1] == 'A') {
                                                var l_minimum_date = moment(r.response[i][5], 'DD/MM/YYYY'), l_issue_date = moment(l_document.IssueDate, 'DD/MM/YYYY');
                                                if (l_issue_date.isBefore(l_minimum_date, 'day')) {
                                                    $.jGrowl(`El Vehículo '` + r.response[i][2] + `' tiene una Fecha de Emisión menor al del Documento anterior`, {
                                                        header: 'Validación'
                                                    });
                                                    l_error = true;
                                                    break;
                                                }
                                            }
                                        }
                                    } else {
                                        $.jGrowl(r.response, {
                                            header: 'Error',
                                            type: 'danger'
                                        });
                                    }
                                },
                                complete: function () {
                                    if (l_error) {
                                        gf_removeLoadingTo($("#provider_vehicle_massive_save"));
                                        return;
                                    }
                                    //save
                                    $.ajax({
                                        data: l_formdata,
                                        url: '/Provider/VehicleMassive',
                                        type: 'post',
                                        dataType: 'json',
                                        contentType: false,
                                        processData: false,
                                        success: _success
                                    });
                                    function _success(r) {
                                        if (parseInt(r.status) == 1000) {
                                            $.jGrowl(r.response, {
                                                header: 'Información',
                                                type: 'primary'
                                            });
                                            //location.href = '/Provider/Index';
                                            location.reload(true);
                                        } else {
                                            $.jGrowl(r.response, {
                                                header: 'Error',
                                                type: 'danger',
                                                clipboard: r.exception
                                            });
                                            gf_removeLoadingTo($("#provider_vehicle_massive_save"));
                                        }
                                    }
                                }
                            });
                            return true;
                        }
                    }
                ],
                maximizable: false,
                title: "Confirmación"
            });
        });
        //fn
        function fn_individual_get_document_status(p_document) {
            var l_documents = l_individual_temp_vehicle[5], l_pending = null, l_status = null, l_html;
            for (var i in l_documents) {
                if (l_documents[i][1] == p_document[0]) {
                    l_pending = l_documents[i][8];
                    l_status = l_documents[i][10];
                }
            }
            l_html = `<div class="cls-1903022357">`;
            if (l_pending && l_pending == 'P') {
                l_html += `<span style="display: none;">PA</span><span class="glyphicon glyphicon-font app-center" title="Pendiente de Aprobación"></span>`;
            }
            if (l_status && l_pending && l_pending == 'A') {
                if (l_status == 'V') {
                    l_html += `<span style="display: none;">V</span>
                                <div class="app-center app-state" style="background-color: #4caf50;">
                                    <i class="fa fa-check"></i>
                                </div>`;
                } else if (l_status == 'A') {
                    l_html += `<span style="display: none;">A</span>
                                <div class="app-center app-state" style="background-color: #ff9800;">
                                    <i class="fa fa-info"></i>
                                </div>`;
                } else if (l_status == 'R') {
                    l_html += `<span style="display: none;">R</span>
                                <div class="app-center app-state" style="background-color: #f44336;">
                                    <i class="fa fa-times"></i>
                                </div>`;
                } else if (l_status == 'P') {
                    l_html += `<span class="color-info-inverse d-inline-block">P. Aprobación</span>`;
                } else {
                    l_html += `<span class="color-danger-inverse d-inline-block">Sin Documentos</span>`;
                }
            }
            l_html += `</div>`;
            return l_html;
        }
        function fn_individual_datatable_documents(p_data) {
            l_individual_dataTable_documents = $('#provider_vehicle_individual_table_document').daDataTable({
                "DataTable": {
                    "data": p_data,
                    "aoColumns": [
                        {
                            "data": null,
                            "orderable": false,
                            "searchable": false,
                            "className": "text-center cls-1901280053",
                            "width": "10px",
                            "mRender": function (o) {
                                return '<i class="fa fa-plus-square-o cursor-pointer" data-action="detail"></i>';
                            }
                        },
                        {
                            "data": null,
                            "className": "text-left app-row",
                            "mRender": function (o) {
                                var l_document = o[10] == "" ? o[1] : `<a href="` + o[11] + o[10] + `" download class="cls-1905022222" title="Descargar Modelo">` + o[1] + ` <i class="icon ion-ios-cloud-download m-0"></i></a>`;
                                return l_document + ((p_model.IsProviderProfile || p_model.IsLogisticaAdminProfile) && gf_is_logistica_document(o[0], p_model.LogisticaDocuments) ? fn_individual_get_icon_logistica() : ``);
                            }
                        },
                        {
                            "data": 7,
                            "className": "text-left"
                        },
                        {
                            "data": null,
                            "className": "text-center position-relative",
                            "mRender": function (o) {
                                return fn_individual_get_document_status(o);
                            }
                        }
                    ],
                    "order": [],
                    "processing": true,
                    "bLengthChange": false,
                    "iDisplayLength": 15,
                    //"paging": false,
                    "fnRowCallback": function (nRow, aData) {
                        $(nRow).addClass('cls-1902221730');
                    }
                },
                "theme": "devexpress",
                "imgProcessing": "/assets/dalba/img/DXR.gif",
                "search": {
                    "btn": null,
                    "noSearch": [0, 3]
                }
            }).Datatable;
        }
        function fn_individual_get_detail(p_row) {
            var l_content = p_row.child().find('td'), l_vehicle = l_individual_all_data[l_individual_temp_vehicle[0]], l_initial = true, l_has_documents;
            if (!l_vehicle.oHtmlDocument[p_row.data()[0]]) {
                l_has_documents = false;
                //get documents
                l_vehicle.oHtmlDocument[p_row.data()[0]] = $('<div />');
                for (var i = 0; i < l_vehicle.oInitialDocument.length; i++) {
                    if (p_row.data()[0] == l_vehicle.oInitialDocument[i][1]) {
                        var l_html = $(fn_get_document_html(p_row.data(), l_vehicle.oInitialDocument[i], l_initial));
                        l_has_documents = true;
                        //save data
                        l_html.attr({ 'data-tdos': p_row.data()[0], 'data-pcdo': l_vehicle.oInitialDocument[i][0], 'data-status': l_vehicle.oInitialDocument[i][8], 'data-modified': false, 'data-validate': false });
                        //disable to approve
                        if (l_vehicle.oInitialDocument[i][8] == 'A') {
                            l_html.find('input').each(function () {
                                $(this).attr({
                                    'data-initial': $(this).val(),
                                    'data-disabled': true
                                });
                            });
                            l_html.find('input').prop('disabled', true);
                        }
                        //
                        l_vehicle.oHtmlDocument[p_row.data()[0]].append(l_html);
                        l_initial = false;
                    }
                }
                if (!l_has_documents) {
                    var l_html = $(fn_get_document_html(p_row.data(), null, l_initial));
                    //save data
                    l_html.attr({ 'data-tdos': p_row.data()[0], 'data-pcdo': 0, 'data-status': 'ROOT', 'data-modified': false, 'data-validate': false });
                    //
                    l_vehicle.oHtmlDocument[p_row.data()[0]].append(l_html);
                }
            }
            fn_initialize_detail(l_vehicle.oHtmlDocument[p_row.data()[0]]);
            l_content.append(l_vehicle.oHtmlDocument[p_row.data()[0]]);
        }
        function fn_individual_has_empty_fields() {
            var l_vehicle, l_index, l_empty = false;
            if (!l_individual_temp_vehicle) {
                return false;
            }
            l_vehicle = l_individual_all_data[l_individual_temp_vehicle[0]];
            if (!l_vehicle) {
                return false;
            }
            for (var i in l_vehicle.oHtmlDocument) {
                l_vehicle.oHtmlDocument[i].find('div[data-object=sustenance-document]').each(function (idx) {
                    if ($(this).attr('data-status') != 'A' && $(this).attr('data-validate') == 'true') {
                        var l_issue_date = $(this).find('input[data-input=issue-date]'), l_expiration_date = $(this).find('input[data-input=expiration-date]'),
                            l_document_number = $(this).find('input[data-input=document-number]'), l_document_name = $(this).find('input[data-object=document-name]');
                        if (l_issue_date && l_issue_date.length > 0) {
                            l_empty = l_empty ? l_empty : !gf_isValue(l_issue_date.val());
                        }
                        if (l_expiration_date && l_expiration_date.length > 0) {
                            l_empty = l_empty ? l_empty : !gf_isValue(l_expiration_date.val());
                        }
                        if (l_document_number && l_document_number.length > 0) {
                            l_empty = l_empty ? l_empty : !gf_isValue(l_document_number.val());
                        }
                        if (l_document_name && l_document_name.length > 0) {
                            l_empty = l_empty ? l_empty : !gf_isValue(l_document_name.val());
                        }
                        if (l_empty) {
                            l_index = idx + 1;
                            return false;
                        }
                    }
                });
                if (l_empty) {
                    var l_document = fn_get_document(i);
                    $.jGrowl(`Existe campos vacíos en el formulario #` + l_index + ` del documento '` + l_document[1] + `'`, {
                        header: 'Validación'
                    });
                    break;
                }
            }
            return l_empty;
        }
        function fn_individual_get_validate(p_object) {
            var l_issue_date = p_object.find('input[data-input=issue-date]'), l_expiration_date = p_object.find('input[data-input=expiration-date]'),
                l_document_number = p_object.find('input[data-input=document-number]'), l_document_file = p_object.find('input[data-input=document-file]'),
                l_return = false;
            if (l_issue_date && l_issue_date.length > 0) {
                l_return = l_return ? l_return : gf_isValue(l_issue_date.val());
            }
            if (l_expiration_date && l_expiration_date.length > 0) {
                l_return = l_return ? l_return : gf_isValue(l_expiration_date.val());
            }
            if (l_document_number && l_document_number.length > 0) {
                l_return = l_return ? l_return : gf_isValue(l_document_number.val());
            }
            if (l_document_file && l_document_file.length > 0) {
                l_return = l_return ? l_return : gf_isValue(l_document_file.val());
            }
            return l_return;
        }
        function fn_individual_get_data() {
            var l_response = {
                'VehiclePrun': [],
                'VehicleFile': [],
                'VehicleDocumentPrun': [],
                'VehicleDocumentPcdo': [],
                'VehicleDocumentTdos': [],
                'VehicleDocumentNumber': [],
                'VehicleDocumentFile': [],
                'VehicleDocumentOldFileName': [],
                'VehicleDocumentOldFileRealName': [],
                'VehicleDocumentIssueDate': [],
                'VehicleDocumentExpirationDate': []
            }, l_vehicle_count = l_individual_dataTable_vehicles.rows().count(), l_vehicle, l_vehicle_data, l_vehicle_photo, l_vehicle_file, l_error = false;
            for (var i = 0; i < l_vehicle_count; i++) {
                l_vehicle_data = l_individual_dataTable_vehicles.rows(i).data()[0];
                l_vehicle_photo = l_individual_dataTable_vehicles.rows(i).nodes().to$().find('input[data-input=photo]');
                l_vehicle_file = l_vehicle_photo.attr('data-modified') == 'true' ? gf_isValue(l_vehicle_photo.val()) ? l_vehicle_photo[0].files[0] : null : null;
                if (l_vehicle_file) {
                    l_response.VehiclePrun.push(l_vehicle_data[0]);
                    l_response.VehicleFile.push(l_vehicle_file);
                }
                l_vehicle = l_individual_all_data[l_vehicle_data[0]];
                if (l_vehicle) {
                    for (var j in l_vehicle.oHtmlDocument) {
                        var l_ranges = [], l_from = null, l_to = null, l_validate = null;
                        l_vehicle.oHtmlDocument[j].find('div[data-object=sustenance-document]').each(function (idx) {
                            var l_issue_date = $(this).find('input[data-input=issue-date]'), l_expiration_date = $(this).find('input[data-input=expiration-date]');
                            if ($(this).attr('data-status') == 'A') {
                                if (l_issue_date && l_issue_date.length > 0 && l_expiration_date && l_expiration_date.length > 0) {
                                    l_ranges.push({ 'from': moment(l_issue_date.val(), 'DD/MM/YYYY'), 'to': moment(l_expiration_date.val(), 'DD/MM/YYYY') });
                                }
                            }
                            if ($(this).attr('data-validate') == 'true') {
                                l_validate = $(this);
                                if (l_issue_date && l_issue_date.length > 0 && l_expiration_date && l_expiration_date.length > 0) {
                                    l_from = moment(l_issue_date.val(), 'DD/MM/YYYY');
                                    l_to = moment(l_expiration_date.val(), 'DD/MM/YYYY');
                                }
                            }
                        });
                        if (l_validate) {
                            if (l_from && l_to) {
                                if (!fn_individual_validate_dates_ranges(l_ranges, l_from, l_to, j, l_vehicle_data)) {
                                    l_error = true;
                                    break;
                                }
                            }
                            var l_issue_date = l_validate.find('input[data-input=issue-date]'), l_expiration_date = l_validate.find('input[data-input=expiration-date]'),
                                l_document_number = l_validate.find('input[data-input=document-number]'), l_document_file = l_validate.find('input[data-input=document-file]');
                            //get values
                            l_response.VehicleDocumentPrun.push(l_vehicle_data[0]);
                            l_response.VehicleDocumentPcdo.push(l_validate.attr('data-pcdo'));
                            l_response.VehicleDocumentTdos.push(l_validate.attr('data-tdos'));
                            //issue date
                            if (l_issue_date && l_issue_date.length > 0) {
                                l_response.VehicleDocumentIssueDate.push(moment(l_issue_date.val(), 'DD/MM/YYYY'));
                            } else {
                                l_response.VehicleDocumentIssueDate.push(null);
                            }
                            //expiration date
                            if (l_expiration_date && l_expiration_date.length > 0) {
                                l_response.VehicleDocumentExpirationDate.push(moment(l_expiration_date.val(), 'DD/MM/YYYY'));
                            } else {
                                l_response.VehicleDocumentExpirationDate.push(null);
                            }
                            //document number
                            if (l_document_number && l_document_number.length > 0) {
                                l_response.VehicleDocumentNumber.push(l_document_number.val());
                            } else {
                                l_response.VehicleDocumentNumber.push('0');
                            }
                            //document file
                            if (l_document_file && l_document_file.length > 0) {
                                if (l_document_file[0].files.length != 0) {
                                    l_response.VehicleDocumentFile.push(l_document_file[0].files[0]);
                                    l_response.VehicleDocumentOldFileName.push('');
                                    l_response.VehicleDocumentOldFileRealName.push('');
                                } else {
                                    l_response.VehicleDocumentFile.push(null);
                                    l_response.VehicleDocumentOldFileRealName.push(l_validate.find('input[data-object=document-name]').val());
                                    l_response.VehicleDocumentOldFileName.push(l_document_file.attr('data-name'));
                                }
                            } else {
                                l_response.VehicleDocumentFile.push(null);
                                l_response.VehicleDocumentOldFileName.push('');
                                l_response.VehicleDocumentOldFileRealName.push('');
                            }
                        }
                    }
                    if (l_error) {
                        break;
                    }
                }
            }
            if (l_error) {
                return null;
            }
            if (l_response.VehiclePrun.length == 0) {
                l_response.VehiclePrun.push(0);
                l_response.VehicleFile.push(null);
            }
            if (l_response.VehicleDocumentPrun.length == 0) {
                l_response.VehicleDocumentPrun.push(0);
                l_response.VehicleDocumentPcdo.push(0);
                l_response.VehicleDocumentTdos.push(0);
                l_response.VehicleDocumentNumber.push('0');
                l_response.VehicleDocumentFile.push(null);
                l_response.VehicleDocumentIssueDate.push(null);
                l_response.VehicleDocumentExpirationDate.push(null);
            }
            return l_response;
        }
        function fn_individual_validate_dates_ranges(p_ranges, p_from, p_to, p_tdos, p_vehicle) {
            var l_validate = true, l_document = fn_get_document(p_tdos), l_minimum_date = moment('2900-12-31', 'YYYY-MM-DD');
            //overlap
            for (var i in p_ranges) {
                l_minimum_date = moment.min(l_minimum_date, p_ranges[i].from);
                if (p_from.isBetween(p_ranges[i].from, p_ranges[i].to, null, '[]') ||
                    p_to.isBetween(p_ranges[i].from, p_ranges[i].to, null, '[]') ||
                    p_ranges[i].from.isBetween(p_from, p_to, null, '[]') ||
                    p_ranges[i].to.isBetween(p_from, p_to, null, '[]')) {
                    $.jGrowl(`En el vehículo '` + p_vehicle[1] + `', existe solapamiento de fechas en el documento '` + l_document[1] + `'. Las fechas ingresadas no deben tener ningún día en común dentro del mismo documento.`, {
                        header: 'Validación'
                    });
                    l_validate = false;
                    break;
                }
            }
            //from <= to
            if (l_validate) {
                if (p_to.diff(p_from, 'days') < 0) {
                    $.jGrowl(`En el vehículo '` + p_vehicle[1] + `', la Fecha de Emisión no debe ser mayor a la Fecha de Vencimiento en el documento '` + l_document[1] + `'.`, {
                        header: 'Validación'
                    });
                    l_validate = false;
                }
            }
            //minimum date
            if (l_validate && p_ranges.length > 0) {
                if (p_to.isBefore(l_minimum_date, 'day')) {
                    $.jGrowl(`En el vehículo '` + p_vehicle[1] + `', las fechas ingresadas no deben ser menor a '` + l_minimum_date.format('DD/MM/YYYY') + `' en el documento '` + l_document[1] + `'. La fecha '` + l_minimum_date.format('DD/MM/YYYY') + `' es la menor registrada dentro del documento.`, {
                        header: 'Validación'
                    });
                    l_validate = false;
                }
            }
            return l_validate;
        }
        function fn_individual_save_document_data(p_object) {
            var l_pcdo = p_object.attr('data-pcdo');
            if (l_pcdo != 0) {
                l_individual_pcdo_delete.push({
                    'pcdo': l_pcdo,
                    'name': p_object.find('input[data-input=document-file]').attr('data-name')
                });
                p_object.attr('data-pcdo', '0');
            }
        }
        function fn_massive_get_data_document() {
            var l_response = {
                'ProviderId': p_model.Provider.Id,
                'DocumentId': 0,
                'IssueDate': '0',
                'ExpirationDate': '0',
                'DocumentNumber': '0',
                'DocumentFile': null
            }, l_tdos = $('#provider_vehicle_massive_document').val(), l_empty = false, l_object = $('#provider_vehicle_massive_form'),
                l_issue_date = l_object.find('input[data-input=issue-date]'), l_expiration_date = l_object.find('input[data-input=expiration-date]'),
                l_document_number = l_object.find('input[data-input=document-number]'), l_document_file = l_object.find('input[data-input=document-file]');
            //document
            if (!gf_isValue(l_tdos)) {
                $.jGrowl(`Seleccione un Documento`, {
                    header: 'Validación'
                });
                return null;
            }
            l_response.DocumentId = l_tdos;
            //empty
            if (l_issue_date && l_issue_date.length > 0) {
                l_empty = l_empty ? l_empty : !gf_isValue(l_issue_date.val());
                l_response.IssueDate = l_issue_date.val();
            }
            if (l_expiration_date && l_expiration_date.length > 0) {
                l_empty = l_empty ? l_empty : !gf_isValue(l_expiration_date.val());
                l_response.ExpirationDate = l_expiration_date.val();
            }
            if (l_document_number && l_document_number.length > 0) {
                l_empty = l_empty ? l_empty : !gf_isValue(l_document_number.val());
                l_response.DocumentNumber = l_document_number.val();
            }
            if (l_document_file && l_document_file.length > 0) {
                l_empty = l_empty ? l_empty : !gf_isValue(l_document_file.val());
                l_response.DocumentFile = gf_isValue(l_document_file.val()) ? l_document_file[0].files[0] : null;
            }
            if (l_empty) {
                $.jGrowl(`Existe campos vacíos en el formulario del documento`, {
                    header: 'Validación'
                });
                return null;
            }
            //from <= to
            if (l_issue_date && l_issue_date.length > 0 && l_expiration_date && l_expiration_date.length > 0) {
                var l_from = moment(l_issue_date.val(), 'DD/MM/YYYY'), l_to = moment(l_expiration_date.val(), 'DD/MM/YYYY');
                if (l_to.diff(l_from, 'days') < 0) {
                    $.jGrowl(`La Fecha de Emisión no debe ser mayor a la Fecha de Vencimiento`, {
                        header: 'Validación'
                    });
                    return null;
                }
            }
            return l_response;
        }
        function fn_massive_get_data_vehicle() {
            var l_response = {
                'VehicleId': []
            }, l_vehicles = l_massive_dataTable_vehicles.rows().nodes(), l_data;
            $(l_vehicles).find(':checkbox:checked').each(function () {
                l_data = l_massive_dataTable_vehicles.row($(this).closest("tr")).data();
                l_response.VehicleId.push(l_data[0]);
            });
            if (l_response.VehicleId.length == 0) {
                $.jGrowl(`Seleccione por lo menos un Vehículo`, {
                    header: 'Validación'
                });
                return null;
            }
            return l_response;
        }
        function fn_initialize_detail(p_html, p_massive) {
            var l_issue_date = p_html.find('input[data-input=issue-date]'), l_expiration_date = p_html.find('input[data-input=expiration-date]');
            l_issue_date.datepicker({
                orientation: p_massive ? 'bottom right' : 'top left',
                //endDate: gf_isValue(l_expiration_date.val()) ? l_expiration_date.val() : Infinity
            }).on('changeDate', function (e) {
                //l_expiration_date.datepicker('setStartDate', e.date);
            });
            l_expiration_date.datepicker({
                orientation: p_massive ? 'bottom right' : 'top right',
                //startDate: gf_isValue(l_issue_date.val()) ? l_issue_date.val() : -Infinity
            }).on('changeDate', function (e) {
                //l_issue_date.datepicker('setEndDate', e.date);
            });
        }
        function fn_get_document_html(p_document, p_data, p_initial, p_massive) {
            var l_html_dates = null, l_html_number = null, l_html_download = null, l_html_buttons = null, l_html_status = null, l_html_reason = null, l_html;
            if (p_document) {
                //build dates
                if (p_document[4] == 'S') {
                    l_html_dates = `<div class="col-lg-3 col-md-6 col-sm-6 col-6 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <input type="text" class="form-control" placeholder="Fec. Emisión" value="` + (p_data ? p_data[4] : ``) + `" data-input="issue-date" />
                                            <div class="icon-input small-calendar cls-1902240417" title="..."></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-6 col-6 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <input type="text" class="form-control" placeholder="Fec. Vencimiento" value="` + (p_data ? p_data[5] : ``) + `" data-input="expiration-date" />
                                            <div class="icon-input small-calendar cls-1902240417" title="..."></div>
                                        </div>
                                    </div>`;
                }
                //build number
                if (p_document[8] == 'S') {
                    l_html_number = `<div class="col-lg-3 col-md-6 col-sm-6 col-12 cls-1902240431">
                                        <div class="form-group mt-0">
                                            <input type="text" class="form-control" placeholder="Nro. Documento" value="` + (p_data ? p_data[13] : ``) + `" data-input="document-number" />
                                        </div>
                                    </div>`;
                }
            }
            if (p_data) {
                //build download
                if (p_data[2] != "") {
                    l_html_download = `<a href="` + p_data[14] + `" download class="btn btn-sm btn-raised btn-info m-0 pt-0 pb-0 position-absolute cls-1902240557" data-toggle="tooltip" data-placement="top" title="Descargar">
                                            <i class="icon ion-ios-cloud-download m-0"></i>
                                        </a>`;
                }
                //build status
                if (p_data[8] == 'A') {
                    l_html_status = `<div class="color-success-inverse pull-left">Aprobado</div>`;
                } else if (p_data[8] == 'P') {
                    l_html_status = `<div class="color-info-inverse pull-left">P. Aprobación</div>`;
                } else if (p_data[8] == 'R') {
                    l_html_status = `<div class="color-danger-inverse pull-left">Rechazado</div>`;
                }
                //build reason
                if (p_data[8] == "R") {
                    l_html_reason = `<div class="row cls-1902240436" data-object="reason">
                                        <div class="col-12 cls-1902240431">
                                            <div class="form-group mt-0">
                                                <label class="control-label m-0"><span class="text-dark">Motivo de Rechazo</span></label>
                                                <div class="cls-1904291750">` + p_data[15] + `</div>
                                            </div>
                                        </div>
                                    </div>`;
                }
            }
            //build buttons
            if (p_initial) {
                if (p_massive) {
                    l_html_buttons = `<button class="btn btn-xs btn-raised btn-warning m-0 pt-0 cls-1902240500" title="Limpiar" data-action="clear-document">
                                        <i class="glyphicon glyphicon-erase m-0"></i>
                                    </button>`;
                } else {
                    l_html_buttons = `<button class="btn btn-xs btn-raised btn-warning m-0 pt-0 cls-1902240500" title="Limpiar" data-action="clear-document">
                                        <i class="glyphicon glyphicon-erase m-0"></i>
                                    </button>
                                    <button class="btn btn-xs btn-raised btn-info m-0 pt-0 cls-1902240500" title="Agregar" data-action="add-document">
                                        <i class="glyphicon glyphicon-plus m-0"></i>
                                    </button>`;
                }
            } else {
                l_html_buttons = `<button class="btn btn-xs btn-raised btn-danger m-0 pt-0 cls-1902240500" title="Quitar" data-action="remove-document">
                                        <i class="glyphicon glyphicon-minus m-0"></i>
                                    </button>`;
            }
            //build html
            var l_html = `<div class="cls-1901280058" data-object="sustenance-document">
                            <div class="cls-1902240408 pb-0">
                                <div class="app-row mb-05">
                                    ` + (l_html_status ? l_html_status : ``) + `
                                    <div class="pull-right">
                                        ` + (l_html_buttons ? l_html_buttons : ``) + `
                                    </div>
                                </div>
                                <div class="row cls-1902240436">
                                    ` + (l_html_number ? l_html_number : ``) + `
                                    <div class="`+ (l_html_number && l_html_dates ? `col-lg-3 col-md-6 col-sm-6` : (l_html_number && !l_html_dates ? `col-lg-9 col-md-6 col-sm-6` : (!l_html_number && l_html_dates ? `col-lg-6 col-md-12 col-sm-12` : ``))) + ` col-12 cls-1902240431">
                                        <div class="form-group mt-0 is-empty is-fileinput">
                                            <input type="text" readonly class="form-control" placeholder="Seleccionar..." value="` + (p_data ? p_data[11] : ``) + `" data-object="document-name" />
                                            <input type="file" title=" " data-input="document-file" value="` + (p_data ? p_data[11] : ``) + `" data-name="` + (p_data ? p_data[2] : ``) + `" />
                                            ` + (l_html_download ? l_html_download : ``) + `
                                        </div>
                                    </div>
                                    ` + (l_html_dates ? l_html_dates : ``) + `
                                </div>
                                ` + (l_html_reason ? l_html_reason : ``) + `
                            </div>
                        </div>`;
            return l_html;
        }
        function fn_get_document(p_tdos) {
            var l_return = null;
            for (var i in p_model.Documents) {
                if (p_model.Documents[i][0] == p_tdos) {
                    l_return = p_model.Documents[i];
                    break;
                }
            }
            return l_return;
        }
        function fn_clear_document(p_object, p_individual) {
            var l_sustenance = p_object.closest('div[data-object=sustenance-document]');
            if (l_sustenance.attr('data-status') != 'A') {
                if (p_individual) {
                    fn_individual_save_document_data(l_sustenance);
                }
                l_sustenance.find('input[data-input=issue-date]').val('').datepicker('update').trigger('change');
                l_sustenance.find('input[data-input=expiration-date]').val('').datepicker('update').trigger('change');
                l_sustenance.find('input[data-input=document-number]').val('').trigger('change');
                l_sustenance.find('input[data-input=document-file]').val('').trigger('change');
                l_sustenance.find('input[data-object=document-name]').val('').trigger('change');
            }
        }
        function fn_individual_get_icon_logistica() {
            return `<div class="pull-right position-relative app-state" style="background-color: #11A9CC;">
                        <span class="cls-1903021233">L</span>
                    </div>`;
        }
        //init
        l_individual_dataTable_vehicles = $('#provider_vehicle_individual_table').daDataTable({
            "DataTable": {
                "data": p_model.Vehicles,
                "aoColumns": [
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "position-relative cls-1902191550 cls-1902240273",
                        "mRender": function (o) {
                            return `<a href="javascript:void(0)" class="m-0" data-action="select-photo" title="Seleccionar foto">
                                        <img src="` + (o[2] == "" ? `/assets/site/assets/images/noimage.png` : o[3]) + `" alt="..." class="app-center cls-1901280906" data-object="photo" />
                                    </a>
                                    <input type="file" data-input="photo" data-modified="false" value="` + o[4] + `" style="display: none;" />`;
                        }
                    },
                    {
                        "data": 1,
                        "className": "text-left align-top cursor-pointer"
                    },
                    {
                        "data": null,
                        "className": "text-center align-top position-relative cursor-pointer",
                        "mRender": function (o) {
                            if (o[4] == 'V') {
                                return `<span style="display: none;">V</span>
                                        <div class="app-center app-state" style="background-color: #4caf50;">
                                            <i class="fa fa-check"></i>
                                        </div>`;
                            } else if (o[4] == 'A') {
                                return `<span style="display: none;">A</span>
                                        <div class="app-center app-state" style="background-color: #ff9800;">
                                            <i class="fa fa-info"></i>
                                        </div>`;
                            } else if (o[4] == 'R') {
                                return `<span style="display: none;">R</span>
                                        <div class="app-center app-state" style="background-color: #f44336;">
                                            <i class="fa fa-times"></i>
                                        </div>`;
                            } else if (o[4] == 'P') {
                                return `<span class="color-info-inverse d-inline-block">P. Aprobación</span>`;
                            } else {
                                return `<span class="color-danger-inverse d-inline-block">Sin Documentos</span>`;
                            }
                        }
                    }
                ],
                "order": [[1, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 15,
                //"paging": false,
                "fnRowCallback": function (nRow, aData) {
                    $(nRow).find('td').eq(1).attr('data-action', 'select-vehicle');
                    $(nRow).find('td').eq(2).attr('data-action', 'select-vehicle');
                }
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": null,
                "noSearch": [0]
            }
        }).Datatable;
        //buttons
        new $.fn.dataTable.Buttons(l_individual_dataTable_vehicles, {
            'buttons': [{
                'extend': 'excelHtml5',
                'text': 'export',
                'title': 'Vehículo',
                'className': 'd-none',
                'exportOptions': {
                    'columns': [1, 2],
                    'format': {
                        'body': function (data, row, column, node) {
                            var l_data = l_individual_dataTable_vehicles.row($(node).closest('tr')).data();
                            if (column == 0) {
                                return l_data[1];
                            } else if (column == 1) {
                                return l_data[4] == 'V' ? 'Conforme' : l_data[4] == 'A' ? 'Por Vencer' : l_data[4] == 'R' ? 'Vencido' : l_data[4] == 'P' ? 'P. Aprobación' : 'Sin Documentos';
                            }
                        }
                    }
                }
            }]
        }).container().appendTo($('#provider_vehicle_individual_table_export'));
        l_massive_dataTable_vehicles = $('#provider_vehicle_massive_table').daDataTable({
            "DataTable": {
                "data": p_model.Vehicles,
                "aoColumns": [
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "width": "20px",
                        "className": "text-center",
                        "mRender": function (o) {
                            return `<div class="checkbox">
                                        <label class="m-0">
                                            <input type="checkbox"><span class="checkbox-material"><span class="check"></span></span>
                                        </label>
                                    </div>`;
                        }
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "position-relative cls-1902191550 cls-1902240273",
                        "mRender": function (o) {
                            return `<img src="` + (o[2] == "" ? `/assets/site/assets/images/noimage.png` : o[3]) + `" alt="..." class="app-center cls-1901280906" />`;
                        }
                    },
                    {
                        "data": 1,
                        "className": "text-left align-top"
                    }
                ],
                "order": [[2, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 15,
                //"paging": false,
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": null,
                "noSearch": [0, 1]
            }
        }).Datatable;
        fn_individual_datatable_documents([]);
        $('#provider_vehicle_massive_document').val(null).trigger('change');
    }(gf_getClone(ngon.Model)));
});