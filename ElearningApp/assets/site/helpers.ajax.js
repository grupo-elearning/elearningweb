﻿"use strict";
function gf_Helpers_IncotermsDropDown(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=incoterms]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/IncotermsDropDown',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option value='" + l_r[i][0] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_ProgrammingAreaDropDownAll(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=all-programmingarea]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/ProgrammingAreaDropDownAll',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option value='" + l_r[i][0] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_AccountTypeDropDown_List(p_settings) {
    var l_result = [];
    var l_default = {
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/AccountTypeDropDown',
        async: false,
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (r) {
            var l_r = r.response;
            for (var i in l_r) {
                l_result.push({ 'Id': l_r[i][0], 'Description': l_r[i][1] });
            }
        }
    });
    return l_result;
}
function gf_Helpers_BankDropDown_List(p_settings) {
    var l_result = [];
    var l_default = {
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/BankDropDown',
        async: false,
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (r) {
            var l_r = r.response;
            for (var i in l_r) {
                l_result.push({ 'Id': l_r[i][0], 'Description': l_r[i][1] });
            }
        }
    });
    return l_result;
}
function gf_Helpers_MoneyTypeDropDown_List(p_settings) {
    var l_result = [];
    var l_default = {
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/MoneyTypeDropDown',
        async: false,
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (r) {
            var l_r = r.response;
            for (var i in l_r) {
                l_result.push({ 'Id': l_r[i][0], 'Description': l_r[i][1], 'Symbol': l_r[i][2] });
            }
        }
    });
    return l_result;
}
function gf_Helpers_VehicleTypeDropDown(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=vehicletype]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/VehicleTypeDropDown',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option value='" + l_r[i][0] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_VehicleTypeDropDown_List(p_settings) {
    var l_result = [];
    var l_default = {
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/VehicleTypeDropDown',
        async: false,
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (r) {
            var l_r = r.response;
            for (var i in l_r) {
                l_result.push({ 'Id': l_r[i][0], 'Description': l_r[i][1] });
            }
        }
    });
    return l_result;
}
function gf_Helpers_PollDropDown(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=poll]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/PollDropDown',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option value='" + l_r[i][0] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_CourseAreaDropDownAll(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=all-coursearea]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/CourseAreaDropDownAll',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option value='" + l_r[i][0] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_ProviderAreaDropDownAll(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=all-providerarea]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/ProviderAreaDropDownAll',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option value='" + l_r[i][0] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_QuestionTypeDropDown(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=questiontype]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/QuestionTypeDropDown',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option data-type='" + l_r[i][2] + "' value='" + l_r[i][0] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_CourseDropDown(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=course]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/CourseDropDown',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option value='" + l_r[i][0] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_DocumentTypeDropDown(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=documenttype]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/DocumentTypeDropDown',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option value='" + l_r[i][0] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_ModeDropDown(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=mode]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/ModeDropDown',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option value='" + l_r[i][0] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_PlaceDropDown(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=place]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/PlaceDropDown',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option value='" + l_r[i][0] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_CourseAreaDropDown(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=coursearea]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/CourseAreaDropDown',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option value='" + l_r[i][0] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_TestCourseDropDown(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=testcourse]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/TestCourseDropDown',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option value='" + l_r[i][0] + "' data-random='" + l_r[i][2] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_CategoryDropDown(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=category]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/CategoryDropDown',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option value='" + l_r[i][0] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_CertificateDropDown(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=certificate]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/CertificateDropDown',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option value='" + l_r[i][0] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_AreaDropDown(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=area]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/AreaDropDown',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option value='" + l_r[i][0] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_ELearningProfileDropDown(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=elearningprofile]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/ELearningProfileDropDown',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option value='" + l_r[i][0] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_InteractiveContentDropDown(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=interactivecontent]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/InteractiveContentDropDown',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option value='" + l_r[i][0] + "' data-url='" + l_r[i][2] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_QuestionType2TestDropDown(p_settings) {
    var l_object;
    var l_default = {
        callback: null,
        $: null,
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    l_object = l_default.$ == null ? $("select[data-dropdown=questiontype2test]") : l_default.$;
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/QuestionType2TestDropDown',
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: _beforeSend,
        success: _success,
        complete: _complete
    });
    function _beforeSend() {
        l_object.empty();
        l_object.append("<option disabled='disabled'> Cargando...</option>");
    }
    function _success(r) {
        var l_r = r.response;
        l_object.empty();
        if (r.status == -1) {
            l_object.append("<option disabled='disabled'>" + l_r + "</option>");
            console.error(r.response);
        } else {
            if (l_default.options != null) {
                for (var i in l_default.options) {
                    l_object.append(("<option value='" + l_default.options[i]["value"] + "'>" + l_default.options[i]["text"] + "</option>"));
                }
            }
            for (var i in l_r) {
                l_object.append(("<option data-type='" + l_r[i][2] + "' value='" + l_r[i][0] + "'>" + l_r[i][1] + "</option>"));
            }
        }
    }
    function _complete() {
        if (typeof l_default.callback != 'undefined' && l_default.callback != null) {
            l_default.callback(l_default.$);
        }
    }
}
function gf_Helpers_DocumentTypeDropDown_List(p_settings) {
    var l_result = [];
    var l_default = {
        param: null,
        options: null
    };
    l_default = $.extend({}, l_default, p_settings);
    $.ajax({
        data: JSON.stringify(l_default.param),
        url: '/Helpers/DocumentTypeDropDown',
        async: false,
        type: 'post',
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        success: function (r) {
            var l_r = r.response;
            for (var i in l_r) {
                l_result.push({ 'Id': l_r[i][0], 'Description': l_r[i][1] });
            }
        }
    });
    return l_result;
}
function gf_Helpers_GetParameterByKey(p_key) {
    var l_value = null;
    $.ajax({
        data: JSON.stringify({ key: p_key }),
        url: '/Helpers/GetParameterByKey',
        type: 'post',
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8",
        success: _success
    });
    function _success(r) {
        if (r.status == 1) {
            l_value = r.response;
        }
    }
    return l_value;
}
function gf_Helpers_GetUTCDate() {
    var l_value = '01/01/1900';
    $.ajax({
        url: '/Helpers/GetUTCDate',
        type: 'post',
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8",
        success: _success
    });
    function _success(r) {
        if (r.status == 1) {
            l_value = r.response;
        }
    }
    return l_value;
}
function gf_Helpers_GetUTCTime() {
    var l_value = '00:00';
    $.ajax({
        url: '/Helpers/GetUTCTime',
        type: 'post',
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8",
        success: _success
    });
    function _success(r) {
        if (r.status == 1) {
            l_value = r.response;
        }
    }
    return l_value;
}
function gf_Helpers_InSiderperu() {
    var l_value = false;
    $.ajax({
        url: '/Helpers/InSiderperu',
        type: 'post',
        dataType: 'json',
        async: false,
        contentType: "application/json; charset=utf-8",
        success: _success
    });
    function _success(r) {
        if (r.status == 1) {
            l_value = r.response;
        }
    }
    return l_value;
}
