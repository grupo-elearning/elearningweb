﻿"use strict";
$("document").ready(function () {
    $("#course_index_collapseMenu a").click(function (evt) {
        evt.preventDefault();
        $("#course_index_collapseMenu a").removeClass("active");
        $(this).addClass("active");
        $("#course_index_container div[data-classification]").hide();
        $("#course_index_container div[data-classification=" + $(this).data("classification") + "]").show();
    });
    $("#course_index_especifico tbody").on('click', 'a[data-action=delete]', function (evt) {
        evt.preventDefault();
        fn_delete($(this), $('#course_index_especifico'), 'especifico');
    });
    $("#course_index_transversal tbody").on('click', 'a[data-action=delete]', function (evt) {
        evt.preventDefault();
        fn_delete($(this), $('#course_index_transversal'), 'transversal');
    });
    //fn
    function fn_datatable_especifico() {
        $("#course_index_especifico").daDataTable({
            "DataTable": {
                "sAjaxSource": "/Course/GetList",
                "fnServerData": function (sSource, aoData, fnCallback, oSettings) {
                    aoData.push(
                        {
                            "name": "Classification", "value": "E"
                        }
                    );
                    oSettings.jqXHR = $.ajax({
                        "dataType": 'json',
                        "type": "post",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    });
                },
                "aoColumns": [
                    {
                        "data": 1,
                        "className": "text-left"
                    },
                    {
                        "data": 2,
                        "className": "text-left"
                    },
                    {
                        "data": 3,
                        "className": "text-left"
                    },
                    {
                        "data": 4
                    },
                    {
                        "data": null,
                        "className": "text-center",
                        "width": "50px",
                        "mRender": function (o) {
                            if (o[5] == "A") {
                                return "Activo";
                            } else if (o[5] == "I") {
                                return "Inactivo";
                            } else {
                                return '';
                            }
                        }
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center",
                        "width": "50px",
                        "mRender": function (o) {
                            return `<a href="/Course/Edit/` + gf_encode(o[0]) + `" class="btn btn-xs btn-raised btn-warning m-0 cls-1810072234" title="Editar" >
                                    <i class="fa fa-edit m-0"></i>
                                </a>
                                <a href="javascript:void(0)" class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" data-action="delete" title="Eliminar" >
                                    <i class="fa fa-trash m-0"></i>
                                </a>
                                <a href="/Course/Presentation/` + gf_encode(o[0]) + `" class="btn btn-xs btn-raised btn-info m-0 cls-1810072234" title="Presentación" >
                                    <i class="fa fa-eye m-0"></i>
                                </a>`;
                        }
                    }
                ],
                "order": [[1, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 10
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": { "column": 5 }
            }
        });
    }
    function fn_datatable_transversal() {
        $("#course_index_transversal").daDataTable({
            "DataTable": {
                "sAjaxSource": "/Course/GetList",
                "fnServerData": function (sSource, aoData, fnCallback, oSettings) {
                    aoData.push(
                        {
                            "name": "Classification", "value": "T"
                        }
                    );
                    oSettings.jqXHR = $.ajax({
                        "dataType": 'json',
                        "type": "post",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback
                    });
                },
                "aoColumns": [
                    {
                        "data": 1,
                        "className": "text-left"
                    },
                    {
                        "data": 2,
                        "className": "text-left"
                    },
                    {
                        "data": 3,
                        "className": "text-left"
                    },
                    {
                        "data": 4
                    },
                    {
                        "data": null,
                        "className": "text-center",
                        "width": "50px",
                        "mRender": function (o) {
                            if (o[5] == "A") {
                                return "Activo";
                            } else if (o[5] == "I") {
                                return "Inactivo";
                            } else {
                                return '';
                            }
                        }
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center",
                        "width": "50px",
                        "mRender": function (o) {
                            return `<a href="/Course/Edit/` + gf_encode(o[0]) + `" class="btn btn-xs btn-raised btn-warning m-0 cls-1810072234" title="Editar" >
                                    <i class="fa fa-edit m-0"></i>
                                </a>
                                <a href="javascript:void(0)" class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" data-action="delete" title="Eliminar" >
                                    <i class="fa fa-trash m-0"></i>
                                </a>
                                <a href="/Course/Presentation/` + gf_encode(o[0]) + `" class="btn btn-xs btn-raised btn-info m-0 cls-1810072234" title="Presentación" >
                                    <i class="fa fa-eye m-0"></i>
                                </a>`;
                        }
                    }
                ],
                "order": [[1, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 10
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": { "column": 5 }
            }
        });
    }
    function fn_delete(p_o, p_table, p_n) {
        p_o.trigger("blur");
        var l_datatable = p_table.DataTable();
        var l_data = l_datatable.row(p_o.closest('tr')).data();
        $.daAlert({
            content: {
                type: 'text',
                value: '<div class="text-center p-05">¿Está seguro que desea eliminar?</div>'
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "remove",
                    fn: function (p, e) {
                        var l_success = false;
                        $.ajax({
                            data: JSON.stringify({ Id: l_data[0] }),
                            url: '/Course/Delete',
                            type: 'post',
                            dataType: 'json',
                            contentType: "application/json; charset=utf-8",
                            success: _success,
                            complete: _complete
                        });
                        function _success(r) {
                            if (r.status == 1000) {
                                $.jGrowl(r.response, {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                l_success = true;
                            } else {
                                $.jGrowl(r.response, {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: r.exception
                                });
                            }
                        }
                        function _complete() {
                            if (l_success) {
                                if (p_n == 'especifico') {
                                    fn_datatable_especifico();
                                } else if (p_n == 'transversal') {
                                    fn_datatable_transversal();
                                }
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: 'Confirmación'
        });
    }
    //init
    fn_datatable_especifico();
    fn_datatable_transversal();
});