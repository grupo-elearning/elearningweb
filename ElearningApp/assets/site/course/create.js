﻿"use strict";
$("document").ready(function () {
    var l_themes = [], l_id = 0, l_theme = null,
        l_codes = {
            'text': gf_Helpers_GetParameterByKey('CODI_TCON_TEXTO'),
            'document': gf_Helpers_GetParameterByKey('CODI_TCON_DOCUMENTO'),
            'video': gf_Helpers_GetParameterByKey('CODI_TCON_VIDEO'),
            'interactive': gf_Helpers_GetParameterByKey('CODI_TCON_INTERACTIVO')
        };
    $('#course_create_front_page').summernote({
        placeholder: 'Ingrese la descripcion de la portada',
        height: 150,
        toolbar: [
            ['color', ['color']],
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ],
        lang: 'es-ES'
    });
    $('#course_create_objetives').summernote({
        placeholder: 'Ingrese los objetivos',
        height: 150,
        toolbar: [
            ['color', ['color']],
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ],
        lang: 'es-ES'
    });
    $('#course_create_summary').summernote({
        placeholder: 'Ingrese el resumen',
        height: 150,
        toolbar: [
            ['color', ['color']],
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ],
        lang: 'es-ES'
    });
    $("#course_create_form").submit(function (evt) {
        evt.preventDefault();
    });
    $('#course_create_image').change(function () {
        var l_validate = gf_fileValidation({
            fileInput: $(this),
            imgPreview: $("#course_create_image_preview"),
            fnError: function () {
                $.jGrowl("Solo puede seleccionar archivos tipo: jpg, jpeg, png, bmp", {
                    header: "Validación"
                });
            }
        });
        if (l_validate) {
            $("#course_create_image_notification").hide();
            $("#course_create_image_btn_add").hide();
            $("#course_create_image_btn_remove").show();
        } else {
            $("#course_create_image_preview").attr("src", "/assets/app/img/no-image-icon.svg");
            $("#course_create_image_notification").show();
            $("#course_create_image_btn_remove").hide();
            $("#course_create_image_btn_add").show();
        }
    });
    $('#course_create_image_btn_add').click(function (evt) {
        evt.preventDefault();
        $("#course_create_image").trigger("click");
    });
    $('#course_create_image_btn_remove').click(function (evt) {
        evt.preventDefault();
        $("#course_create_image").val("").trigger("change");
    });
    $('#course_create_new_theme').click(function (evt) {
        evt.preventDefault();
        $.daAlert({
            content: {
                type: "text",
                value: `<div class="cls-1810071553">
                            <form id="course_create_new_theme_form" autocomplete="off" action="#" class="app-form" data-presave-rule="object" data-presave-analysis="insert">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group mt-0">
                                            <label class="control-label" for="course_create_new_theme_name"><span class="text-dark">Nombre</span> <span class="text-danger">*</span> | Máximo 200 caracteres</label>
                                            <input id="course_create_new_theme_name" type="text" maxlength="200" class="form-control" data-presave-object="Nombre" />
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group mt-0">
                                            <label class="control-label" for="course_create_new_theme_order"><span class="text-dark">Secuencia</span> <span class="text-danger">*</span> | Número entero no menor a uno</label>
                                            <input id="course_create_new_theme_order" type="number" min="0" class="form-control" data-presave-object="Secuencia" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>`
            },
            title: "Nuevo Tema",
            maximizable: false,
            focus: 1,
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "add",
                    fn: function () {
                        return fn_addTheme();
                    }
                }
            ]
        });
    });
    $('#course_create_save').click(function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        var l_psave = gf_presaveForm($("#course_create_form")), l_data, l_formdata = new FormData();
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return;
        }
        if (!fn_getValidateThemeItemOrder()) {
            $.jGrowl("Las Secuencias de los contenidos deben ser Números enteros no menores a uno", {
                header: 'Validación'
            });
            return;
        }
        if ($('#course_create_front_page').summernote('isEmpty')) {
            $.jGrowl("Descripción de Portada es campo requerido", {
                header: 'Validación'
            });
            return;
        }
        if ($('#course_create_objetives').summernote('isEmpty')) {
            $.jGrowl("Objetivos es campo requerido", {
                header: 'Validación'
            });
            return;
        }
        if ($('#course_create_summary').summernote('isEmpty')) {
            $.jGrowl("Resumen es campo requerido", {
                header: 'Validación'
            });
            return;
        }
        l_data = fn_getData();
        for (var i in l_data) {
            l_formdata.append(i, l_data[i]);
        }
        $.daAlert({
            content: {
                type: "text",
                value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "save",
                    fn: function () {
                        $.ajax({
                            data: l_formdata,
                            url: '/Course/Create',
                            type: 'post',
                            dataType: 'json',
                            contentType: false,
                            processData: false,
                            beforeSend: _beforeSend,
                            success: _success
                        });
                        function _beforeSend() {
                            gf_addLoadingTo($("#course_create_save"));
                            gf_triggerKeepActive();
                        }
                        function _success(r) {
                            if (parseInt(r.status) == 1000) {
                                $.jGrowl(r.response, {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                location.href = '/Course/Index';
                            } else {
                                $.jGrowl(r.response, {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: r.exception
                                });
                                gf_removeLoadingTo($("#course_create_save"));
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: "Confirmación"
        });
    });
    $('#course_create_has_validity').change(function () {
        $('#course_create_validity_scale').selectpicker('destroy');
        if ($(this).is(":checked")) {
            $('#course_create_validity')
                .prop('disabled', false)
                .data('PresaveRequired', true)
                .val($('#course_create_validity').data('lastValue'));
            $('#course_create_validity_scale')
                .prop('disabled', false)
                .removeClass('pointer-events-none');
        } else {
            $('#course_create_validity')
                .prop('disabled', true)
                .data({
                    lastValue: $("#course_create_validity").val(),
                    PresaveRequired: false
                })
                .val('');
            $('#course_create_validity_scale')
                .prop('disabled', true)
                .addClass('pointer-events-none');
        }
        $('#course_create_validity_scale').selectpicker();
    });
    //on
    $('#course_create_theme_accordion').on('click', 'span[data-action=remove-theme]', function (evt) {
        evt.preventDefault();
        fn_destroyTheme(fn_getThemeById($(this).attr("data-theme")));
        evt.stopPropagation();
    });
    $('#course_create_theme_accordion').on('click', 'span[data-action=edit-theme]', function (evt) {
        evt.preventDefault();
        l_theme = fn_getThemeById($(this).attr("data-theme"));
        $.daAlert({
            content: {
                type: "text",
                value: `<div class="cls-1810071553">
                            <form id="course_create_new_theme_form" autocomplete="off" action="#" class="app-form" data-presave-rule="object" data-presave-analysis="insert">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group mt-0">
                                            <label class="control-label" for="course_create_new_theme_name"><span class="text-dark">Nombre</span> <span class="text-danger">*</span> | Máximo 200 caracteres</label>
                                            <input id="course_create_new_theme_name" type="text" maxlength="200" class="form-control" data-presave-object="Nombre" value="` + l_theme.sName + `" />
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group mt-0">
                                            <label class="control-label" for="course_create_new_theme_order"><span class="text-dark">Secuencia</span> <span class="text-danger">*</span> | Número entero no menor a uno</label>
                                            <input id="course_create_new_theme_order" type="number" min="0" class="form-control" data-presave-object="Secuencia" value="` + l_theme.iOrder + `" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>`
            },
            title: "Editar Tema",
            maximizable: false,
            focus: 1,
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "accept",
                    fn: function () {
                        return fn_updateTheme();
                    }
                }
            ]
        });
        evt.stopPropagation();
    });
    $('#course_create_theme_accordion').on('click', 'a[data-action=new-content-text]', function (evt) {
        evt.preventDefault();
        l_theme = fn_getThemeById($(this).attr("data-theme"));
        $.daAlert({
            content: {
                type: "text",
                value: `<div class="cls-1810071553">
                            <form id="course_create_new_content_form" autocomplete="off" action="#" class="app-form" data-presave-rule="object" data-presave-analysis="insert">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group mt-0">
                                            <label class="control-label" for="course_create_new_content_title"><span class="text-dark">Título</span> <span class="text-danger">*</span> | Máximo 200 caracteres</label>
                                            <input id="course_create_new_content_title" type="text" maxlength="200" class="form-control" data-presave-object="Título" />
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group mt-0">
                                            <label class="control-label"><span class="text-dark">Texto</span> <span class="text-danger">*</span></label>
                                            <div id="course_create_new_content_text"></div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group mt-0">
                                            <label class="control-label" for="course_create_new_content_order"><span class="text-dark">Secuencia</span> <span class="text-danger">*</span> | Número entero no menor a uno</label>
                                            <input id="course_create_new_content_order" type="number" min="0" class="form-control" data-presave-object="Secuencia" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>`
            },
            title: "Agregar Texto",
            maximizable: false,
            focus: 1,
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "add",
                    fn: function () {
                        return fn_addContentText();
                    }
                }
            ],
            onshow: function () {
                setTimeout(function () {
                    //init
                    $('#course_create_new_content_text').summernote({
                        placeholder: 'Ingrese texto',
                        height: 150,
                        toolbar: [
                            ['color', ['color']],
                            ['style', ['bold', 'italic', 'underline', 'clear']],
                            ['font', ['strikethrough', 'superscript', 'subscript']],
                            ['fontsize', ['fontsize']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['height', ['height']]
                        ],
                        lang: 'es-ES'
                    });
                }, 500);
            }
        });
    });
    $('#course_create_theme_accordion').on('click', 'a[data-action=new-content-document]', function (evt) {
        evt.preventDefault();
        l_theme = fn_getThemeById($(this).attr("data-theme"));
        $.daAlert({
            content: {
                type: "text",
                value: `<div class="cls-1810071553">
                            <form id="course_create_new_content_form" autocomplete="off" action="#" class="app-form" data-presave-rule="object" data-presave-analysis="insert">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group mt-0">
                                            <label class="control-label" for="course_create_new_content_title"><span class="text-dark">Título</span> <span class="text-danger">*</span> | Máximo 200 caracteres</label>
                                            <input id="course_create_new_content_title" type="text" maxlength="200" class="form-control" data-presave-object="Título" />
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group mt-0 is-fileinput">
                                            <label class="control-label" for="course_create_new_content_document"><span class="text-dark">Documento</span> <span class="text-danger">*</span> | El Documento debe ser de tipo: pdf, doc, docx, xls, xlsx, xml, png, jpg, jpeg, bmp, gif</label>
                                            <input id="course_create_new_content_document_display" type="text" readonly="" class="form-control" data-presave-object="Documento" placeholder="Seleccionar...">
                                            <input id="course_create_new_content_document" type="file" title=" " accept=".pdf,.doc,.docx,.xls,.xlsx,.xml,.png,.jpg,.jpeg,.bmp,.gif" />
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group mt-0">
                                            <label class="control-label" for="course_create_new_content_order"><span class="text-dark">Secuencia</span> <span class="text-danger">*</span> | Número entero no menor a uno</label>
                                            <input id="course_create_new_content_order" type="number" min="0" class="form-control" data-presave-object="Secuencia" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>`
            },
            title: "Agregar Documento",
            maximizable: false,
            focus: 1,
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "add",
                    fn: function () {
                        return fn_addContentDocument();
                    }
                }
            ]
        });
    });
    $('#course_create_theme_accordion').on('click', 'a[data-action=new-content-video]', function (evt) {
        evt.preventDefault();
        l_theme = fn_getThemeById($(this).attr("data-theme"));
        $.daAlert({
            content: {
                type: "text",
                value: `<div class="cls-1810071553">
                            <form id="course_create_new_content_form" autocomplete="off" action="#" class="app-form" data-presave-rule="object" data-presave-analysis="insert">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group mt-0">
                                            <label class="control-label" for="course_create_new_content_title"><span class="text-dark">Título</span> <span class="text-danger">*</span> | Máximo 200 caracteres</label>
                                            <input id="course_create_new_content_title" type="text" maxlength="200" class="form-control" data-presave-object="Título" />
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group mt-0 pb-0">
                                            <div class="togglebutton">
                                                <div class="app-row">
                                                    <label class="pull-right">
                                                        <input type="checkbox" id="course_create_new_content_as_file" />
                                                        <span class="toggle"></span> <span class="cls-1810072122">Documento</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div id="course_create_new_content_url_container_youtube" class="form-group mt-0">
                                            <label class="control-label" for="course_create_new_content_url_youtube"><span class="text-dark">Url - Youtube</span> <span class="text-danger">*</span> | Máximo 200 caracteres</label>
                                            <input id="course_create_new_content_url_youtube" type="text" maxlength="200" class="form-control" data-presave-object="Url - Youtube" />
                                        </div>
                                        <div id="course_create_new_content_url_container_channel_sider" class="form-group mt-0">
                                            <label class="control-label" for="course_create_new_content_url_sider_channel"><span class="text-dark">Url - Canal Sider</span> <span class="text-danger">*</span> | Máximo 200 caracteres</label>
                                            <input id="course_create_new_content_url_sider_channel" type="text" maxlength="200" class="form-control" data-presave-object="Url - Canal Sider" />
                                        </div>
                                        <div id="course_create_new_content_file_container" class="form-group mt-0 is-fileinput" style="display: none;">
                                            <label class="control-label" for="course_create_new_content_file"><span class="text-dark">Documento</span> <span class="text-danger">*</span> | El Documento debe ser de tipo: mp4</label>
                                            <input id="course_create_new_content_file_display" type="text" readonly="" class="form-control" data-presave-object="Documento" placeholder="Seleccionar...">
                                            <input id="course_create_new_content_file" type="file" title=" " accept=".mp4" />
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group mt-0">
                                            <label class="control-label" for="course_create_new_content_order"><span class="text-dark">Secuencia</span> <span class="text-danger">*</span> | Número entero no menor a uno</label>
                                            <input id="course_create_new_content_order" type="number" min="0" class="form-control" data-presave-object="Secuencia" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>`
            },
            title: "Agregar Video",
            maximizable: false,
            focus: 1,
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "add",
                    fn: function () {
                        return fn_addContentVideo();
                    }
                }
            ],
            onclose: function () {
                $('#course_create_new_content_as_file').off("change");
            },
            onshow: function () {
                setTimeout(function () {
                    //init
                    $('#course_create_new_content_as_file').on("change", function () {
                        if ($(this).is(":checked")) {
                            $("#course_create_new_content_url_container_youtube, #course_create_new_content_url_container_channel_sider").hide();
                            $("#course_create_new_content_file_container").show();
                        } else {
                            $("#course_create_new_content_file_container").hide();
                            $("#course_create_new_content_url_container_youtube, #course_create_new_content_url_container_channel_sider").show();
                        }
                    });
                }, 500);
            }
        });
    });
    $('#course_create_theme_accordion').on('click', 'a[data-action=new-content-interactive]', function (evt) {
        evt.preventDefault();
        var l_interactive = 0, l_rows, l_data;
        for (var i in l_themes) {
            l_rows = l_themes[i].oDataTable.rows().count();
            for (var j = 0; j < l_rows; j++) {
                l_data = l_themes[i].oDataTable.rows(j).data()[0];
                l_interactive = l_interactive + (l_data[6] == l_codes.interactive ? 1 : 0);
            }
        }
        if (l_interactive != 0) {
            $.jGrowl("Ya existe un Contenido Interactivo ingresado. Solo se permite el ingreso de UN Contenido Interactivo", {
                header: 'Validación'
            });
            return;
        }
        l_theme = fn_getThemeById($(this).attr("data-theme"));
        $.daAlert({
            content: {
                type: 'text',
                value: `<div class="cls-1810071553">
                            <form id="course_create_new_content_form" autocomplete="off" action="#" class="app-form" data-presave-rule="object" data-presave-analysis="insert">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group mt-0">
                                            <label class="control-label" for="course_create_new_content_title"><span class="text-dark">Título</span> <span class="text-danger">*</span> | Máximo 200 caracteres</label>
                                            <input id="course_create_new_content_title" type="text" maxlength="200" class="form-control" data-presave-object="Título" />
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group mt-0">
                                            <label class="control-label" for="course_create_new_content_interactive"><span class="text-dark">Contenido</span> <span class="text-danger">*</span></label>
                                            <select id="course_create_new_content_interactive" class="form-control" data-live-search="true" data-dropup-auto="false" data-presave-object="Contenido"></select>
                                        </div>
                                        <div class="cls-1901311542">
                                            <iframe id="course_create_new_content_interactive_preview" src="" class="cls-1901311608">
                                                <p>Your browser does not support iframes.</p>
                                            </iframe>
                                            <div class="cls-1901311616"></div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group mt-0">
                                            <label class="control-label" for="course_create_new_content_order"><span class="text-dark">Secuencia</span> <span class="text-danger">*</span> | Número entero no menor a uno</label>
                                            <input id="course_create_new_content_order" type="number" min="0" class="form-control" data-presave-object="Secuencia" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>`
            },
            title: "Agregar Contenido Interactivo",
            maximizable: false,
            focus: 1,
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "add",
                    fn: function () {
                        return fn_addContentInteractive();
                    }
                }
            ],
            onclose: function () {
                $('#course_create_new_content_interactive').off("change");
            },
            onshow: function () {
                setTimeout(function () {
                    //init
                    $('#course_create_new_content_interactive').on('change', function () {
                        $('#course_create_new_content_interactive_preview')
                            .removeClass('cls-1901311608')
                            .attr('src', '')
                            .attr('src', $(this).find('option:selected').attr('data-url'));
                    });
                    gf_Helpers_InteractiveContentDropDown({
                        $: $("#course_create_new_content_interactive"),
                        callback: function (o) {
                            o.addClass("selectpicker").val(null).selectpicker();
                        }
                    });
                }, 500);
            }
        });
    });
    $('#course_create_theme_accordion').on('click', 'a[data-action=remove-content]', function (evt) {
        evt.preventDefault();
        var l_theme = fn_getThemeById($(this).closest("table").attr("data-theme"));
        l_theme.oDataTable
            .row($(this).closest("tr"))
            .remove()
            .draw(false);
    });
    //fn
    function fn_getValidateThemeItemOrder() {
        var l_rows, l_item_order, l_error = false;
        for (var i = 0; i < l_themes.length; i++) {
            l_rows = l_themes[i].oDataTable.rows().count();
            for (var j = 0; j < l_rows; j++) {
                l_item_order = l_themes[i].oDataTable.rows(j).nodes().to$().find('input[data-input=order]').val();
                if (!gf_isInteger(l_item_order) || !gf_isPositive(l_item_order, true)) {
                    l_error = true;
                    break;
                }
            }
            if (l_error) {
                break;
            }
        }
        return !l_error;
    }
    function fn_getData() {
        var l_response = {
            "Name": $("#course_create_name").val().trim(),
            "Status": $("#course_create_status").is(":checked") ? "A" : "I",
            "CategoryId": $("#course_create_category").val(),
            "Classification": null,
            "ImageFile.File": $("#course_create_image")[0].files[0],
            "FrontPage": $('#course_create_front_page').summernote('code'),
            "Objetives": $('#course_create_objetives').summernote('code'),
            "Summary": $('#course_create_summary').summernote('code'),
            "HasValidity": $('#course_create_has_validity').is(":checked") ? "S" : "N",
            "Validity": 0,
            "ValidityScale": "M"
        }, l_idx, l_tmp_id = 0, l_data, l_rows, l_item_order;
        if ($("#course_create_clasification_t").is(":checked")) {
            l_response.Classification = "T";
        }
        if ($("#course_create_clasification_e").is(":checked")) {
            l_response.Classification = "E";
        }
        l_response.Validity = l_response.HasValidity == 'S' ? $("#course_create_validity").val() : 0;
        l_response.ValidityScale = l_response.HasValidity == 'S' ? $("#course_create_validity_scale").val() : 'M';
        //content
        l_idx = 0;
        for (var i = 0; i < l_themes.length; i++) {
            l_tmp_id--;
            l_rows = l_themes[i].oDataTable.rows().count();
            for (var j = 0; j < l_rows; j++) {
                l_data = l_themes[i].oDataTable.rows(j).data()[0];
                l_item_order = l_themes[i].oDataTable.rows(j).nodes().to$().find('input[data-input=order]').val();
                l_response["ThemeId[" + l_idx + "]"] = l_tmp_id;
                l_response["ThemeName[" + l_idx + "]"] = l_themes[i].sName;
                l_response["ThemeOrder[" + l_idx + "]"] = l_themes[i].iOrder;
                l_response["ThemeItemId[" + l_idx + "]"] = 0;
                l_response["ThemeItemTypeId[" + l_idx + "]"] = l_data[6];
                l_response["ThemeItemTitle[" + l_idx + "]"] = l_data[1];
                l_response["ThemeItemOrder[" + l_idx + "]"] = l_item_order;
                if (l_data[6] == l_codes.text) {
                    l_response["ThemeItemText[" + l_idx + "]"] = l_data[2];
                    l_response["ThemeItemUrlYoutube[" + l_idx + "]"] = "";
                    l_response["ThemeItemUrlSiderChannel[" + l_idx + "]"] = "";
                    l_response["ThemeItemName[" + l_idx + "]"] = "";
                    l_response["ThemeItemInteractive[" + l_idx + "]"] = 0;
                    l_response["ThemeItemFiles[" + l_idx + "]"] = null;
                } else if (l_data[6] == l_codes.document) {
                    l_response["ThemeItemText[" + l_idx + "]"] = "";
                    l_response["ThemeItemUrlYoutube[" + l_idx + "]"] = "";
                    l_response["ThemeItemUrlSiderChannel[" + l_idx + "]"] = "";
                    l_response["ThemeItemName[" + l_idx + "]"] = l_data[2];
                    l_response["ThemeItemInteractive[" + l_idx + "]"] = 0;
                    l_response["ThemeItemFiles[" + l_idx + "].File"] = l_data[4];
                } else if (l_data[6] == l_codes.video) {
                    l_response["ThemeItemText[" + l_idx + "]"] = "";
                    if (l_data[4] == null) {
                        l_response["ThemeItemUrlYoutube[" + l_idx + "]"] = l_data[2].url_youtube;
                        l_response["ThemeItemUrlSiderChannel[" + l_idx + "]"] = l_data[2].url_sider_channel;
                        l_response["ThemeItemName[" + l_idx + "]"] = "";
                        l_response["ThemeItemInteractive[" + l_idx + "]"] = 0;
                        l_response["ThemeItemFiles[" + l_idx + "]"] = null;
                    } else {
                        l_response["ThemeItemUrlYoutube[" + l_idx + "]"] = "";
                        l_response["ThemeItemUrlSiderChannel[" + l_idx + "]"] = "";
                        l_response["ThemeItemName[" + l_idx + "]"] = l_data[2];
                        l_response["ThemeItemInteractive[" + l_idx + "]"] = 0;
                        l_response["ThemeItemFiles[" + l_idx + "].File"] = l_data[4];
                    }
                } else if (l_data[6] == l_codes.interactive) {
                    l_response["ThemeItemText[" + l_idx + "]"] = "";
                    l_response["ThemeItemUrlYoutube[" + l_idx + "]"] = "";
                    l_response["ThemeItemUrlSiderChannel[" + l_idx + "]"] = "";
                    l_response["ThemeItemName[" + l_idx + "]"] = "";
                    l_response["ThemeItemInteractive[" + l_idx + "]"] = l_data[5];
                    l_response["ThemeItemFiles[" + l_idx + "]"] = null;
                }
                l_idx++;
            }
            if (l_rows == 0) {
                l_response["ThemeId[" + l_idx + "]"] = l_tmp_id;
                l_response["ThemeName[" + l_idx + "]"] = l_themes[i].sName;
                l_response["ThemeOrder[" + l_idx + "]"] = l_themes[i].iOrder;
                l_response["ThemeItemId[" + l_idx + "]"] = -1;
                l_response["ThemeItemTypeId[" + l_idx + "]"] = 0;
                l_response["ThemeItemTitle[" + l_idx + "]"] = "";
                l_response["ThemeItemOrder[" + l_idx + "]"] = 0;
                l_response["ThemeItemText[" + l_idx + "]"] = "";
                l_response["ThemeItemUrlYoutube[" + l_idx + "]"] = "";
                l_response["ThemeItemUrlSiderChannel[" + l_idx + "]"] = "";
                l_response["ThemeItemName[" + l_idx + "]"] = "";
                l_response["ThemeItemInteractive[" + l_idx + "]"] = 0;
                l_response["ThemeItemFiles[" + l_idx + "]"] = null;
                l_idx++;
            }
        }
        if (l_themes.length == 0) {
            l_response["ThemeId[0]"] = 0;
            l_response["ThemeName[0]"] = "";
            l_response["ThemeOrder[0]"] = 0;
            l_response["ThemeItemId[0]"] = 0;
            l_response["ThemeItemTypeId[0]"] = 0;
            l_response["ThemeItemTitle[0]"] = "";
            l_response["ThemeItemOrder[0]"] = 0;
            l_response["ThemeItemText[0]"] = "";
            l_response["ThemeItemUrlYoutube[0]"] = "";
            l_response["ThemeItemUrlSiderChannel[0]"] = "";
            l_response["ThemeItemName[0]"] = "";
            l_response["ThemeItemInteractive[0]"] = 0;
            l_response["ThemeItemFiles[0]"] = null;
        }
        return l_response;
    }
    function fn_addContentText() {
        $("#course_create_new_content_title").data({
            "PresaveRequired": true,
            "PresaveJson": '{"maxLength":200}'
        });
        $("#course_create_new_content_order").data({
            "PresaveRequired": true,
            "PresaveList": "integer",
            "PresaveJson": '{"min":1}'
        });
        var l_psave = gf_presaveForm($("#course_create_new_content_form")), l_title, l_text, l_order;
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return false;
        }
        if ($('#course_create_new_content_text').summernote('isEmpty')) {
            $.jGrowl("Debe ingresar un Texto", {
                header: "Validación"
            });
            return false;
        }
        l_title = $("#course_create_new_content_title").val().trim();
        l_text = $("#course_create_new_content_text").summernote('code');
        l_order = $("#course_create_new_content_order").val();
        l_theme
            .oDataTable
            .row
            .add(['Texto', l_title, l_text, l_order, null, null, l_codes.text])
            .draw(false);
        return true;
    }
    function fn_addContentDocument() {
        $("#course_create_new_content_title").data({
            "PresaveRequired": true,
            "PresaveJson": '{"maxLength":200}'
        });
        $("#course_create_new_content_document_display").data('PresaveRequired', true);
        $("#course_create_new_content_order").data({
            "PresaveRequired": true,
            "PresaveList": "integer",
            "PresaveJson": '{"min":1}'
        });
        var l_psave = gf_presaveForm($("#course_create_new_content_form")), l_title, l_document, l_order, l_fvalidation;
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return false;
        }
        l_fvalidation = gf_fileValidation({
            fileInput: $("#course_create_new_content_document"),
            showPreview: false,
            extensions: /(\.pdf|\.doc|\.docx|\.xls|\.xlsx|\.xml|\.png|\.jpg|\.jpeg|\.bmp|\.gif)$/i,
            cleanAfter: false,
            fnError: function () {
                $.jGrowl("El Documento debe ser de tipo: pdf, doc, docx, xls, xlsx, xml, png, jpg, jpeg, bmp, gif", {
                    header: "Validación"
                });
            }
        });
        if (!l_fvalidation) {
            return false;
        }
        l_title = $("#course_create_new_content_title").val().trim();
        l_document = $("#course_create_new_content_document").val();
        l_order = $("#course_create_new_content_order").val();
        l_theme
            .oDataTable
            .row
            .add(['Documento', l_title, gf_getFileName(l_document), l_order, $("#course_create_new_content_document")[0].files[0], null, l_codes.document])
            .draw(false);
        return true;
    }
    function fn_addContentVideo() {
        var l_asFile = $("#course_create_new_content_as_file").is(":checked");
        $("#course_create_new_content_title").data({
            "PresaveRequired": true,
            "PresaveJson": '{"maxLength":200}'
        });
        $("#course_create_new_content_url_youtube, #course_create_new_content_url_sider_channel").data({
            "PresaveJson": '{"maxLength":200}'
        });
        $("#course_create_new_content_order").data({
            "PresaveRequired": true,
            "PresaveList": "integer",
            "PresaveJson": '{"min":1}'
        });
        //
        $("#course_create_new_content_file_display").data('PresaveRequired', l_asFile);
        $("#course_create_new_content_url_youtube, #course_create_new_content_url_sider_channel").data('PresaveRequired', !l_asFile);
        var l_psave = gf_presaveForm($("#course_create_new_content_form")), l_title, l_url_youtube, l_url_sider_channel, l_document, l_order, l_fvalidation;
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return false;
        }
        if (l_asFile) {
            l_fvalidation = gf_fileValidation({
                fileInput: $("#course_create_new_content_file"),
                showPreview: false,
                extensions: /(\.mp4)$/i,
                cleanAfter: false,
                fnError: function () {
                    $.jGrowl("El Documento debe ser de tipo: mp4", {
                        header: "Validación"
                    });
                }
            });
            if (!l_fvalidation) {
                return false;
            }
        } else {
            if (!gf_isYoutubeVideo($("#course_create_new_content_url_youtube").val())) {
                $.jGrowl("La 'Url - Youtube' debe ser una dirección de YouTube válida", {
                    header: "Validación"
                });
                return false;
            }
        }
        l_title = $("#course_create_new_content_title").val().trim();
        l_url_youtube = $("#course_create_new_content_url_youtube").val().trim();
        l_url_sider_channel = $("#course_create_new_content_url_sider_channel").val().trim();
        l_document = $("#course_create_new_content_file").val();
        l_order = $("#course_create_new_content_order").val();
        if (l_asFile) {
            l_theme
                .oDataTable
                .row
                .add(['Video', l_title, gf_getFileName(l_document), l_order, $("#course_create_new_content_file")[0].files[0], null, l_codes.video])
                .draw(false);
        } else {
            l_theme
                .oDataTable
                .row
                .add(['Video', l_title, { 'url_youtube': l_url_youtube, 'url_sider_channel': l_url_sider_channel }, l_order, null, null, l_codes.video])
                .draw(false);
        }
        return true;
    }
    function fn_addContentInteractive() {
        $("#course_create_new_content_title").data({
            "PresaveRequired": true,
            "PresaveJson": '{"maxLength":200}'
        });
        $('#course_create_new_content_interactive').data('PresaveRequired', true);
        $("#course_create_new_content_order").data({
            "PresaveRequired": true,
            "PresaveList": "integer",
            "PresaveJson": '{"min":1}'
        });
        var l_psave = gf_presaveForm($("#course_create_new_content_form")), l_title, l_interactive, l_text, l_order;
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return false;
        }
        l_title = $('#course_create_new_content_title').val().trim();
        l_interactive = $('#course_create_new_content_interactive').val();
        l_text = $('#course_create_new_content_interactive option:selected').text();
        l_order = $('#course_create_new_content_order').val();
        l_theme
            .oDataTable
            .row
            .add(['Contenido Interactivo', l_title, l_text, l_order, null, l_interactive, l_codes.interactive])
            .draw(false);
        return true;
    }
    function fn_updateTheme() {
        $("#course_create_new_theme_name").data({
            "PresaveRequired": true,
            "PresaveJson": '{"maxLength":200}'
        });
        $("#course_create_new_theme_order").data({
            "PresaveRequired": true,
            "PresaveList": "integer",
            "PresaveJson": '{"min":1}'
        });
        var l_psave = gf_presaveForm($("#course_create_new_theme_form")), l_name, l_order;
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return false;
        }
        l_name = $("#course_create_new_theme_name").val().trim();
        l_order = $("#course_create_new_theme_order").val();
        if (fn_existsThemeName(l_name, l_theme.iId)) {
            $.jGrowl("El nombre ingresado ya está registrado", {
                header: "Validación"
            });
            return false;
        }
        l_theme.sName = l_name;
        l_theme.iOrder = l_order;
        $("#theme_heading_name_" + l_theme.iId).html(l_order + ' - ' + l_name);
        return true;
    }
    function fn_addTheme() {
        $("#course_create_new_theme_name").data({
            "PresaveRequired": true,
            "PresaveJson": '{"maxLength":200}'
        });
        $("#course_create_new_theme_order").data({
            "PresaveRequired": true,
            "PresaveList": "integer",
            "PresaveJson": '{"min":1}'
        });
        var l_psave = gf_presaveForm($("#course_create_new_theme_form")), l_name, l_order;
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return false;
        }
        l_name = $("#course_create_new_theme_name").val().trim();
        l_order = $("#course_create_new_theme_order").val();
        if (fn_existsThemeName(l_name)) {
            $.jGrowl("El nombre ingresado ya está registrado", {
                header: "Validación"
            });
            return false;
        }
        l_id++;
        l_themes.push({
            "iId": l_id,
            "sName": l_name,
            "iOrder": l_order,
            "bIsDrawn": false,
            "oContent": $('<div/>'),
            "oDataTable": null
        });
        fn_drawTheme();
        return true;
    }
    function fn_drawTheme() {
        for (var i in l_themes) {
            if (!l_themes[i].bIsDrawn) {
                l_themes[i].oContent
                    .addClass("mb-0 card card-primary")
                    .html(`<div class="card-header" role="tab" id="theme_heading_` + l_themes[i].iId + `">
                                <h4 class="card-title">
                                    <a class="collapsed withripple" role="button" data-toggle="collapse" href="#theme_collapse_` + l_themes[i].iId + `" aria-expanded="false" aria-controls="theme_collapse_` + l_themes[i].iId + `">
                                        <i class="zmdi zmdi-attachment-alt"></i> <span id="theme_heading_name_` + l_themes[i].iId + `">` + l_themes[i].iOrder + ' - ' + l_themes[i].sName + `</span>
                                        <span class="pull-right cls-1810071839" data-action="remove-theme" data-theme="` + l_themes[i].iId + `">Eliminar</span>
                                        <span class="pull-right cls-1810071839" data-action="edit-theme" data-theme="` + l_themes[i].iId + `">Editar</span>
                                    </a>
                                </h4>
                            </div>
                            <div id="theme_collapse_` + l_themes[i].iId + `" class="card-collapse collapse" role="tabpanel" aria-labelledby="theme_heading_` + l_themes[i].iId + `" data-parent="#course_create_theme_accordion">
                                <div class="app-row p-10 text-right">
                                    <a href="javascript:void(0)" data-action="new-content-text" class="btn-circle btn-circle-sm btn-circle-raised btn-circle-info" title="Texto" data-theme="` + l_themes[i].iId + `">
                                        <i class="zmdi zmdi-text-format"></i>
                                    </a>
                                    <a href="javascript:void(0)" data-action="new-content-document" class="btn-circle btn-circle-sm btn-circle-raised btn-circle-success" title="Documento" data-theme="` + l_themes[i].iId + `">
                                        <i class="zmdi zmdi-file"></i>
                                    </a>
                                    <a href="javascript:void(0)" data-action="new-content-video" class="btn-circle btn-circle-sm btn-circle-raised btn-circle-warning" title="Video" data-theme="` + l_themes[i].iId + `">
                                        <i class="zmdi zmdi-videocam"></i>
                                    </a>
                                    <a href="javascript:void(0)" data-action="new-content-interactive" class="btn-circle btn-circle-sm btn-circle-raised btn-circle-primary" title="Contenido Interactivo" data-theme="` + l_themes[i].iId + `">
                                        <i class="zmdi zmdi-collection-video"></i>
                                    </a>
                                </div>
                                <div class="card-body pt-0">
                                    <table id="theme_table_` + l_themes[i].iId + `" class="table table-bordered" data-theme="` + l_themes[i].iId + `" >
                                        <thead>
                                            <tr>
                                                <th>Tipo</th>
                                                <th>Título</th>
                                                <th>Texto / Documento / Url / Nombre</th>
                                                <th>Secuencia</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>`);
                $("#course_create_theme_accordion").append(l_themes[i].oContent);
                l_themes[i].bIsDrawn = true;
                fn_buildDataTable(l_themes[i]);
            }
        }
    }
    function fn_buildDataTable(p_theme) {
        p_theme.oDataTable = p_theme.oContent.find("table").daDataTable({
            "DataTable": {
                "aoColumns": [
                    {
                        "data": 0,
                        "className": "text-center"
                    },
                    {
                        "data": 1,
                        "className": "text-left"
                    },
                    {
                        "data": null,
                        "className": "text-left",
                        "mRender": function (o) {
                            if (o[6] == l_codes.video && o[4] == null) {
                                return `Url - Youtube: ` + o[2].url_youtube + `<br />Url - Canal Sider: ` + o[2].url_sider_channel;
                            } else {
                                return o[2];
                            }
                        }
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "cls-1901202130",
                        "mRender": function (o) {
                            return `<input type="number" min="0" class="form-control text-right" data-input="order" value="` + o[3] + `" />`;
                        }
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center",
                        "width": "50px",
                        "mRender": function (o) {
                            return `<a href="javascript:void(0)" class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" data-action="remove-content" >
                                        <i class="fa fa-trash m-0"></i>
                                    </a>`;
                        }
                    }
                ],
                "order": [[0, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 10
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": { "column": 4 },
                "noSearch": [3]
            }
        }).Datatable;
    }
    function fn_existsThemeName(p_name, p_id) {
        var l_exists = false;
        for (var i in l_themes) {
            if (l_themes[i].sName == p_name && l_themes[i].iId != p_id) {
                l_exists = true;
                break;
            }
        }
        return l_exists;
    }
    function fn_getThemeById(p_id) {
        var l_theme = null;
        for (var i in l_themes) {
            if (l_themes[i].iId == p_id) {
                l_theme = l_themes[i];
                break;
            }
        }
        return l_theme;
    }
    function fn_destroyTheme(p_theme) {
        var l_idx = -1;
        for (var i in l_themes) {
            if (l_themes[i] == p_theme) {
                l_idx = i;
                break;
            }
        }
        if (l_idx != -1) {
            p_theme.oContent.remove();
            p_theme.oDataTable.destroy();
            l_themes.splice(l_idx, 1);
        }
    }
    //init
    $("#course_create_has_validity").trigger('change');
    $("#course_create_validity").data({
        "PresaveList": "integer",
        "PresaveJson": '{"min":1}'
    });
    $("#course_create_name, #course_create_category, #course_create_image_display").data({
        "PresaveRequired": true
    });
    $("#course_create_name").data("PresaveJson", '{"maxLength":200}');
    gf_Helpers_CategoryDropDown({
        $: $("#course_create_category"),
        callback: function (o) {
            o.addClass("selectpicker").val(null).selectpicker();
        }
    });
});