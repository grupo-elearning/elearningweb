﻿"use strict";
$("document").ready(function () {
    (function (p_model) {
        var l_tcon_txt = gf_Helpers_GetParameterByKey('CODI_TCON_TEXTO'), l_tcon_video = gf_Helpers_GetParameterByKey('CODI_TCON_VIDEO'),
            l_tcon_doc = gf_Helpers_GetParameterByKey('CODI_TCON_DOCUMENTO'), l_tcon_inter = gf_Helpers_GetParameterByKey('CODI_TCON_INTERACTIVO'),
            l_player = null, l_contents = {}, l_in_siderperu = gf_Helpers_InSiderperu();
        $("#programming_classroom_themes_nav a[data-object=content]").click(function (evt) {
            evt.preventDefault();
            var l_item = l_contents[$(this).attr("data-content")];
            //hide test content
            $("#programming_classroom_test").removeClass("active");
            //hide poll content
            $("#programming_classroom_poll").removeClass("active");
            //hide contents
            $("#programming_classroom_themes_nav a[data-object=content].active").removeClass("active").trigger("blur");
            fn_destroy_player();
            $("#programming_classroom_content").empty();
            //show this content
            $(this).addClass("active");
            $("#programming_classroom_content_title").text((l_item.sec + ' - ' + l_item.ttl));
            if (l_item.tcon == l_tcon_txt) {
                $("#programming_classroom_content").html(l_item.txt);
            } else if (l_item.tcon == l_tcon_video) {
                if (l_item.urlyt != '') {
                    if (l_in_siderperu) {
                        $("#programming_classroom_content").html(`<iframe src="` + l_item.urlsc + `?autoplay=false&amp;showinfo=true" class="cls-1902010351 cls-1904040948" allowfullscreen></iframe>`);
                    } else {
                        $("#programming_classroom_content").html(`<div id="programming_classroom_player" class="js-player" data-plyr-provider="youtube" data-plyr-embed-id="` + l_item.urlyt + `"></div>`);
                    }
                } else {
                    $("#programming_classroom_content").html(`<video id="programming_classroom_player" poster="/assets/app/img/elearning.jpg" playsinline controls>
                                                                <source src="` + l_item.urldoc + `" type="video/mp4">
                                                            </video>`);
                }
                if ($("#programming_classroom_player").length != 0) {
                    l_player = new Plyr("#programming_classroom_player");
                }
            } else if (l_item.tcon == l_tcon_doc) {
                if (gf_isImage(l_item.urldoc)) {
                    $("#programming_classroom_content").html(`<div class="text-center">
                                                                <img src="` + l_item.urldoc + `" class="mh-100 mw-100" />
                                                            </div>`);
                } else {
                    $("#programming_classroom_content").html(`<iframe src="https://docs.google.com/gview?url=` + l_item.urldoc.replace("+","") + `&embedded=true" class="cls-1902010351" style="width: 100%; min-height: 500px;"></iframe>`);
                }
            } else if (l_item.tcon == l_tcon_inter) {
                $("#programming_classroom_content")
                    .addClass('app-request-fullscreen')
                    .html(`<button class="btn btn-raised btn-primary m-0 cls-1903110411" title="Minimizar" data-status="1" data-action="fullscreen">
                                <i class="fa fa-compress m-0"></i>
                            </button>
                            <iframe src="` + l_item.urlcur + `" class="cls-1902010351" style="width: 100%; height: 100%; min-height: 500px;"></iframe>`);
            } else {
                $("#programming_classroom_content").html(``);
            }
        });
        $('#programming_classroom_content').on('click', 'button[data-action=fullscreen]', function (evt) {
            evt.preventDefault();
            var l_status = $(this).attr('data-status');
            if (l_status == '0') {
                $(this)
                    .attr({
                        'data-status': '1',
                        'title': 'Minimizar'
                    })
                    .find('i.fa')
                    .removeClass('fa-expand')
                    .addClass('fa-compress');
                gf_requestFullscreen($('#programming_classroom_content'));
            } else {
                $(this)
                    .attr({
                        'data-status': '0',
                        'title': 'Maximizar'
                    })
                    .find('i.fa')
                    .removeClass('fa-compress')
                    .addClass('fa-expand');
                gf_exitFullscreen($('#programming_classroom_content'));
            }
        });
        $('#programming_classroom_certificate').click(function (evt) {
            evt.preventDefault();
            $.jGrowl("La opción de 'Constancia' se activará si en la programación está configurada para descargarla y si la persona ha aprobado", {
                header: 'Validación'
            });
        });
        $("#programming_classroom_test").click(function (evt) {
            evt.preventDefault();
            //hide poll content
            $("#programming_classroom_poll").removeClass("active");
            //hide contents
            $("#programming_classroom_themes_nav a[data-object=content].active").removeClass("active");
            $("#programming_classroom_themes_nav ul.panel-collapse.collapse.show").removeClass('show');
            $("#programming_classroom_themes_nav a[data-toggle=collapse].withripple").addClass('collapsed');
            fn_destroy_player();
            //show test content
            $(this).addClass('active');
            $("#programming_classroom_content_title").text('Examen');
            $("#programming_classroom_content").html(`<div class="text-center"><span class="small">Información del Examen</span></div>`);
        });
        $("#programming_classroom_poll").click(function (evt) {
            evt.preventDefault();
            //hide test content
            $("#programming_classroom_test").removeClass("active");
            //hide contents
            $("#programming_classroom_themes_nav a[data-object=content].active").removeClass("active");
            $("#programming_classroom_themes_nav ul.panel-collapse.collapse.show").removeClass('show');
            $("#programming_classroom_themes_nav a[data-toggle=collapse].withripple").addClass('collapsed');
            fn_destroy_player();
            //show poll content
            $(this).addClass('active');
            $("#programming_classroom_content_title").text('Encuesta');
            $("#programming_classroom_content").html(`<div class="text-center"><span class="small">Información de la Encuesta</span></div>`);
        });
        function fn_destroy_player() {
            if (l_player) {
                l_player.destroy();
                l_player = null;
            }
        }
        //init
        (function () {
            for (var i in p_model.Themes) {
                for (var j in p_model.Themes[i][2]) {
                    l_contents[p_model.Themes[i][2][j][7]] = {
                        tcon: p_model.Themes[i][2][j][6],
                        ttl: p_model.Themes[i][2][j][1],
                        urlyt: p_model.Themes[i][2][j][12],
                        urlsc: p_model.Themes[i][2][j][13],
                        txt: p_model.Themes[i][2][j][10],
                        urldoc: p_model.Themes[i][2][j][11],
                        sec: p_model.Themes[i][2][j][3],
                        cuin: p_model.Themes[i][2][j][5],
                        ncur: p_model.Themes[i][2][j][2],
                        urlcur: p_model.Themes[i][2][j][9]
                    };
                }
            }
        }());
        (function () {
            var l_first = $($("#programming_classroom_themes_nav a[data-object=content]")[0]);
            l_first.closest("ul.panel-collapse.collapse").addClass('show');
            l_first.closest("ul.panel-collapse.collapse").prev("a.withripple").removeClass('collapsed');
            l_first.trigger("click");
        }());
    }(gf_getClone(ngon.Model)));
});