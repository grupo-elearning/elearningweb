﻿"use strict";
$("document").ready(function () {
    $("#category_create_form").submit(function (evt) {
        evt.preventDefault();
    });
    $("#category_create_save").click(function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        var l_psave = gf_presaveForm($("#category_create_form")), l_data;
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return;
        }
        l_data = {
            'Description': $("#category_create_description").val().trim(),
            'Status': $("#category_create_status").is(":checked") ? "A" : "I"
        };
        $.daAlert({
            content: {
                type: "text",
                value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "save",
                    fn: function () {
                        $.ajax({
                            data: JSON.stringify(l_data),
                            url: '/Category/Create',
                            type: 'post',
                            dataType: 'json',
                            contentType: "application/json; charset=utf-8",
                            beforeSend: _beforeSend,
                            success: _success
                        });
                        function _beforeSend() {
                            gf_addLoadingTo($("#category_create_save"));
                        }
                        function _success(r) {
                            if (parseInt(r.status) == 1000) {
                                $.jGrowl(r.response, {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                location.href = '/Category/Index';
                            } else {
                                $.jGrowl(r.response, {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: r.exception
                                });
                                gf_removeLoadingTo($("#category_create_save"));
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: "Confirmación"
        });
    });
    //fn
    //init
    $("#category_create_description").data({
        PresaveRequired: true,
        PresaveJson: '{"maxLength":200}'
    });
});