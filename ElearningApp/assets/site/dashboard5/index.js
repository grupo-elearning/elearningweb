﻿"use strict";
$('document').ready(function () {
    var l_codi_pins = 0;
    var l_dataTable, l_chart_course, l_executed = { 'data': [], 'programmed': 0, 'completed': 0, 'executed': 0 };
    $('#dashboard5_index_filter_anio').addClass("selectpicker");
    $('#dashboard5_index_filter_programming').addClass("selectpicker");

    //event
    $('#dashboard5_index_filter_anio').on("change", function () {
        gf_Helpers_ProgrammingAreaDropDownAll({
            $: $("#dashboard5_index_filter_programming"),
            param: { Year: $("#dashboard5_index_filter_anio").val() },
            callback: function (o) {
                o.val(null).selectpicker();
                o.selectpicker('refresh');
                fn_programming_get_registered(0);
            }
        });
    });

    $('#dashboard5_index_filter_retrieve').click(function (evt) {
        evt.preventDefault();
        var l_programming = $('#dashboard5_index_filter_programming').val();
        if (!gf_isValue(l_programming)) {
            $.jGrowl("Seleccione un Programa", {
                header: "Validación"
            });
            return;
        }
        fn_programming_get_registered(l_programming);
    });
    $('#dashboard5_index_table tbody').on('click', 'i[data-action=detail]', function (evt) {
        evt.preventDefault();
        var l_this = $(this), l_tr = l_this.closest('tr'), l_row = l_dataTable.row(l_tr);
        if (l_row.child.isShown()) {
            l_this
                .removeClass('fa-minus-square-o')
                .addClass('fa-plus-square-o');
            l_row.child.hide();
        }
        else {
            l_this
                .removeClass('fa-plus-square-o')
                .addClass('fa-minus-square-o');
            if (l_row.child()) {
                l_row.child.show();
            } else {
                l_row.child('', 'cls-1901280028').show();
                fn_programming_get_detail(l_row);
            }
        }
    });
    $('#dashboard5_index_course_filter_retrieve').click(function (evt) {
        evt.preventDefault();
        var l_course = $('#dashboard5_index_course_filter_course').val();
        if (!gf_isValue(l_course)) {
            $.jGrowl("Seleccione un Curso", {
                header: "Validación"
            });
            return;
        }
        fn_course_get_executed(l_course);
    });
    $('#dashboard5_index_course_export').click(function (evt) {
        evt.preventDefault();
        var l_this = $(this), l_data;
        if (l_executed.data.length == 0) {
            $.jGrowl("Nada que exportar", {
                header: "Validación"
            });
            return;
        }
        //build_data
        l_data = {
            'Area': [],
            'Staff': [],
            'Advance': [],
            'Executed': [],
            'AVGProgrammed': l_executed.programmed,
            'AVGCompleted': l_executed.completed,
            'AVGExecuted': l_executed.executed
        };
        for (var i in l_executed.data) {
            l_data.Area.push(l_executed.data[i][1]);
            l_data.Staff.push(l_executed.data[i][2]);
            l_data.Advance.push(l_executed.data[i][3]);
            l_data.Executed.push(l_executed.data[i][4]);
        }
        $.ajax({
            data: JSON.stringify(l_data),
            url: '/Dashboard5/ExecutedExportToExcel',
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                gf_addLoadingTo(l_this);
            },
            success: function (r) {
                if (r.status == 1) {
                    var tag_a = document.createElement('a');
                    tag_a.href = '/Helpers/DownloadExcel?FileName=' + r.response.name + '&DownloadName=Dashboard5.xlsx';
                    tag_a.download = 'myExport.xlsx';
                    tag_a.click();
                    tag_a = null;
                } else {
                    $.jGrowl(r.response, {
                        header: 'Error',
                        type: 'danger',
                        clipboard: r.exception
                    });
                }
            },
            complete: function () {
                gf_removeLoadingTo(l_this);
            }
        });
    });

    $('#dashboard5_index_table tbody').on('click', 'span[data-action=certificate]', function (evt) {
        evt.preventDefault();
        var l_register = l_dataTable.row($(this).closest('tr')).data()[6], l_this = $(this);
        $.ajax({
            data: JSON.stringify({ register: l_register }),
            url: '/Dashboard5/GetCertificate',
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: _beforeSend,
            success: _success,
            complete: _complete
        });
        function _beforeSend() {
            l_this.css('cssText', 'color: transparent !important;');
            gf_addLoadingTo(l_this);
        }
        function _success(r) {
            if (r.status == 1) {
                fn_programming_build_certificate(r.response);
            } else {
                $.jGrowl(r.response, {
                    header: 'Error',
                    type: 'danger',
                    clipboard: r.exception
                });
            }
        }
        function _complete() {
            l_this.css('cssText', '');
            gf_removeLoadingTo(l_this);
        }
    });

    $('#dashboard5_index_table tbody').on('click', 'span[data-action=upd_note]', function (evt) {
        evt.preventDefault();
        var l_tr = $(this).closest('tr');
        l_codi_pins = l_dataTable.row(l_tr).data()[6];
        console.log("CodiPins: " + l_codi_pins);
        $("#modal_update_note").modal("show");
    });

    function reset_modal() {
        l_codi_pins = 0;
        $("#modal_note").val("");
    }

    $("#modal_btn_save").click(function () {
        var nota = $("#modal_note").val();
        if (nota == null || nota.trim() == "" || nota < 0 || nota > 20) {
            $.jGrowl("La nota no es válida", {
                header: 'Error',
                type: 'danger',
                clipboard: "La nota no es válida"
            });
            return;
        }

        var l_param = {
            CodiPins: l_codi_pins,
            Nota: nota
        };
        $.ajax({
            url: '/Dashboard5/UpdateNote',
            data: JSON.stringify(l_param),
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: _beforeSend,
            success: _success,
            complete: _complete
        });

        function _beforeSend() {
        }

        function _success(r) {
            if (r.status == 1000) {
                reset_modal();
                $.jGrowl(r.response, {
                    header: 'Informacion',
                    type: 'success',
                    clipboard: r.exception
                });


            } else {
                $.jGrowl(r.response, {
                    header: 'Error',
                    type: 'danger',
                    clipboard: r.exception
                });
            }
        }

        function _complete() {
        }

        console.log("modal save");
        $("#modal_update_note").modal("hide");
    });

    //fn
    function fn_set_dropdown_year() {
        var d = new Date();
        var n = d.getFullYear();
        for (var i = 1900; i <= n + 10; i++)
            $('#dashboard5_index_filter_anio').append("<option value=" + i + ">" + i + "</option>");

        $('#dashboard5_index_filter_anio').addClass("selectpicker").val(n).selectpicker();
        $('#dashboard5_index_filter_anio').trigger("change");
    }

    function fn_course_get_executed(p_course) {
        var l_tbody = $('#dashboard5_index_course_table tbody');
        $.ajax({
            url: '/Dashboard5/GetExecuted',
            data: JSON.stringify({ 'course': p_course }),
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: _beforeSend,
            success: _success,
            complete: _complete
        });
        function _beforeSend() {
            l_executed.data.length = 0;
            l_executed.programmed = 0;
            l_executed.completed = 0;
            l_executed.executed = 0;
            l_tbody
                .empty()
                .append(`<tr>
                            <td colspan="7" class="text-center">Cargando...</td>
                        </tr>`);
            l_chart_course.data = [];
            $('#dashboard5_index_course_table tfoot th[data-object=ttl-staff]').text(l_executed.programmed);
            $('#dashboard5_index_course_table tfoot th[data-object=ttl-advance]').text(l_executed.completed);
            $('#dashboard5_index_course_table tfoot th[data-object=ttl-executed]').text(l_executed.executed + ' %');
            $('#dashboard5_index_course_table tfoot th[data-object=avg-programmed]').text(l_executed.programmed);
            $('#dashboard5_index_course_table tfoot th[data-object=avg-completed]').text(l_executed.completed);
        }
        function _success(r) {
            if (r.status == 1) {
                l_executed.data = r.response;
            } else {
                $.jGrowl(r.response, {
                    header: 'Error',
                    type: 'danger',
                    clipboard: r.exception
                });
            }
        }
        function _complete() {
            l_tbody.empty();
            if (l_executed.data.length == 0) {
                l_tbody
                    .append(`<tr>
                                <td colspan="7" class="text-center">Ningún dato disponible en esta tabla</td>
                            </tr>`);
            } else {
                var l_chart_data = [];
                fn_course_get_avg();
                for (var i = 0; i < l_executed.data.length; i++) {
                    var l_tr = `<tr>
                                    <td class="text-left">` + l_executed.data[i][1] + `</td>
                                    <td class="text-center align-middle">` + l_executed.data[i][2] + `</td>
                                    <td class="text-center align-middle">` + l_executed.data[i][3] + `</td>
                                    <td class="text-center align-middle cls-1905072246" style="background-color: ` + fn_course_get_status_color(l_executed.data[i][4]) + `;">` + l_executed.data[i][4] + ` %</td>`;
                    if (i == 0) {
                        l_tr += `   <td rowspan="` + l_executed.data.length + `" class="text-center align-middle">` + l_executed.programmed + `</td>
                                    <td rowspan="` + l_executed.data.length + `" class="text-center align-middle">` + l_executed.completed + `</td>
                                    <td rowspan="` + l_executed.data.length + `" class="text-center align-middle">` + l_executed.executed + ` %</td>
                                </tr>`;
                    } else {
                        l_tr += `</tr>`;
                    }
                    l_tbody.append(l_tr);
                    l_chart_data.push({ 'area': l_executed.data[i][1].trim(), 'executed': l_executed.data[i][4] });
                }
                l_chart_course.data = l_chart_data;
                $('#dashboard5_index_course_table tfoot th[data-object=ttl-staff]').text(l_executed.programmed);
                $('#dashboard5_index_course_table tfoot th[data-object=ttl-advance]').text(l_executed.completed);
                $('#dashboard5_index_course_table tfoot th[data-object=ttl-executed]').text(l_executed.executed + ' %');
                $('#dashboard5_index_course_table tfoot th[data-object=avg-programmed]').text(l_executed.programmed);
                $('#dashboard5_index_course_table tfoot th[data-object=avg-completed]').text(l_executed.completed);
                $('#dashboard5_index_course_chart').css('min-width', ((l_chart_data.length * 80) + 100) + 'px');
            }
        }
    }
    function fn_course_get_status_color(l_executed) {
        return l_executed == 0 ? 'rgb(255, 97, 84)' : l_executed == 100 ? 'rgb(87, 205, 31)' : 'rgb(255, 231, 0)';
    }
    function fn_course_get_avg() {
        for (var i in l_executed.data) {
            l_executed.programmed += l_executed.data[i][2];
            l_executed.completed += l_executed.data[i][3];
        }
        l_executed.executed = gf_roundNumber(l_executed.programmed != 0 ? (l_executed.completed / l_executed.programmed) * 100 : 0, 2);
    }
    function fn_course_build_chart() {
        /*---------------------------- Build Chart ----------------------------*/
        //  Chart
        l_chart_course = am4core.create("dashboard5_index_course_chart", am4charts.XYChart);
        l_chart_course.language.locale = am4lang_es_ES;
        //  Add data
        l_chart_course.data = [];
        // Create axes
        var l_categoryAxis = l_chart_course.xAxes.push(new am4charts.CategoryAxis());
        l_categoryAxis.dataFields.category = "area";
        l_categoryAxis.renderer.grid.template.location = 0;
        l_categoryAxis.renderer.labels.template.rotation = 270;
        l_categoryAxis.renderer.minGridDistance = 20;
        l_categoryAxis.renderer.labels.template.horizontalCenter = "right";
        l_categoryAxis.renderer.labels.template.verticalCenter = "middle";
        l_categoryAxis.fontSize = 11;
        l_categoryAxis.fontWeight = "bold";

        l_categoryAxis.renderer.labels.template.adapter.add("dy", function (dy, target) {
            if (target.dataItem && target.dataItem.index & 2 == 2) {
                return dy + 25;
            }
            return dy;
        });

        var l_valueAxis = l_chart_course.yAxes.push(new am4charts.ValueAxis());
        l_valueAxis.min = 0;
        l_valueAxis.fontSize = 14;
        l_valueAxis.title.text = "Ejecutado (%)";

        //  Add Series =====================================================================>
        var l_series = l_chart_course.series.push(new am4charts.ColumnSeries());
        l_series.dataFields.valueY = "executed";
        l_series.dataFields.categoryX = "area";
        l_series.columns.template.tooltipText = "{categoryX}: [bold]{valueY} %[/]";
        l_series.columns.template.fillOpacity = .8;

        //  Add label
        var l_labelBullet = l_series.bullets.push(new am4charts.LabelBullet());
        l_labelBullet.label.text = "{valueY} %";
        l_labelBullet.label.dy = -10;
        l_labelBullet.label.hideOversized = false;
        l_labelBullet.label.truncate = false;
        l_labelBullet.label.fontSize = 12;

        var l_columnTemplate = l_series.columns.template;
        l_columnTemplate.strokeWidth = 2;
        l_columnTemplate.strokeOpacity = 1;
        //<==================================================================================
        //  Add chart title
        var l_title = l_chart_course.titles.create();
        l_title.text = "Áreas vs Ejecutados";
        l_title.fontSize = 25;
        l_title.marginBottom = 20;
        //  Enable export
        l_chart_course.exporting.menu = new am4core.ExportMenu();
        l_chart_course.exporting.useWebFonts = false;
        l_chart_course.exporting.menu.items = [
            {
                "label": "...",
                "menu": [
                    { "type": "png", "label": "PNG" },
                    { "type": "jpg", "label": "JPG" },
                    //{ "type": "pdf", "label": "PDF" }
                ]
            }
        ];
    }
    function fn_programming_get_registered(p_programming) {
        l_dataTable = $("#dashboard5_index_table").daDataTable({
            "DataTable": {
                "ajax": {
                    "url": "/Dashboard5/GetRegistered",
                    "type": "post",
                    "data": {
                        'programming': p_programming
                    }
                },
                "aoColumns": [
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center cls-1901280053",
                        "width": "10px",
                        "mRender": function (o) {
                            return '<i class="fa fa-plus-square-o cursor-pointer" data-action="detail"></i>';
                        }
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center cls-1901280053",
                        "width": "60px",
                        "mRender": function (o) {
                            var l_return = "";
                            if (o[7] == "No") {
                                l_return = '<input class="form-check-input" type="checkbox" fichasap="' + o[0] + '" data-input="recordatorio">';
                            } else {
                                l_return = "";
                            }
                            return l_return;//'<input class="form-check-input" type="checkbox" fichasap="' + o[0] + '" data-input="recordatorio">';
                        }
                    },
                    {
                        "data": 0,
                        "className": "text-center"
                    },
                    {
                        "data": 1,
                        "className": "text-left"
                    },
                    {
                        "data": 4,//null,
                        "className": "text-center",
                        //"mRender": function (o) {
                        //    return `<span style="display: none;">` + o[5] + `</span>` + o[4];
                        //}
                    },
                    {
                        "data": null,
                        "className": "text-center",
                        "mRender": function (o) {
                            return o[5] != "" ? o[2] : "";
                        }
                    },
                    {
                        "data": 3,
                        "className": "text-left"
                    },
                    {
                        "data": 7,
                        "className": "text-center"
                    },
                    {
                        "data": null,
                        "className": "text-center",
                        "mRender": function (o) {
                            return o[8] == 1 ? `<span class="glyphicon glyphicon-save-file color-primary cls-1905271248" title="Descargar" data-action="certificate"></span>` : ``;
                        }
                    },
                    {
                        "data": null,
                        "className": "text-center",
                        "mRender": function (o) {
                            return o[8] == 1 ? `` : `<span class="glyphicon 	glyphicon glyphicon-refresh color-primary cls-1905271248 btn" title="Actualizar Nota" data-action="upd_note"></span>`;
                        }
                    }
                ],
                "order": [[3, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 15
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": null,
                "noSearch": [0, 1]
            }
        }).Datatable;
        //buttons
        new $.fn.dataTable.Buttons(l_dataTable, {
            'buttons': [
                {
                    'extend': 'excelHtml5',
                    'text': '<i class="fa fa-file-excel-o"></i>Excel',
                    'className': 'btn btn-sm btn-raised btn-success m-0 cls-1902151548',
                    'exportOptions': {
                        'columns': [2, 3, 4, 5, 6, 7],
                        'format': {
                            'body': function (data, row, column, node) {
                                return column == 2 ?
                                    data.replace(/<\s*span[^>]*>(.*?)<\s*\/\s*span>/g, '') :
                                    data;
                            }
                        }
                    }
                },
                {
                    'text': '<i class="fa fa-envelope"></i>Enviar Recordatorio',
                    'className': 'btn btn-sm btn-raised btn-info m-0 cls-1902151548',
                    'action': function (e, dt, node, config) {
                        var l_pers_sel = $("#dashboard5_index_table tbody").find("input[data-input=recordatorio]:checked");
                        var l_fichas = new Array();
                        $.each(l_pers_sel, function (i, element) {
                            var l_ficha = $(element).attr("fichasap");
                            l_fichas.push(l_ficha);
                        });

                        if (l_fichas.length > 0) {
                            $.daAlert({
                                content: {
                                    type: "text",
                                    value: "<div class='text-center p-05'>¿Desea enviar un recordatorio a los colaboradores seleccionados?</div>"
                                },
                                buttons: [
                                    {
                                        type: "cancel",
                                        fn: function () {
                                            return true;
                                        }
                                    },
                                    {
                                        type: "accept",
                                        fn: function () {
                                            var l_param = { ProgrammingId: p_programming, Fichas: l_fichas };
                                            $.ajax({
                                                data: JSON.stringify(l_param),
                                                url: '/Dashboard5/SendRecordatorio',
                                                type: 'post',
                                                dataType: 'json',
                                                async: false,
                                                contentType: "application/json; charset=utf-8",
                                                success: _success
                                            });
                                            function _beforeSend() { }

                                            function _success(r) {
                                                if (parseInt(r.status) == 1000) {
                                                    $.jGrowl(r.response, {
                                                        header: 'Información',
                                                        type: 'primary'
                                                    });
                                                } else {
                                                    $.jGrowl(r.response, {
                                                        header: 'Error',
                                                        type: 'danger',
                                                        clipboard: r.exception
                                                    });
                                                }
                                                fn_reset_table_checks();
                                            }
                                            return true;
                                        }
                                    }
                                ],
                                maximizable: false,
                                title: "Confirmación"
                            });
                        } else {
                            $.jGrowl("Selecciona al menos un Colaborador.", {
                                header: 'Alerta',
                                type: 'danger',
                                clipboard: "."
                            });
                        }
                    }
                }
            ]
        }).container().appendTo($('#dashboard5_index_table_buttons'));
    }
    function fn_programming_get_detail(p_row) {
        var l_content = p_row.child().find('td');
        $.ajax({
            url: '/Dashboard1/Detail',
            data: JSON.stringify({ 'id': p_row.data()[6] }),
            contentType: 'application/json; charset=utf-8',
            type: 'post',
            dataType: 'html',
            beforeSend: function () {
                l_content.html('<div class="text-center small p-1 text-dark">Cargando...</div>');
            },
            success: function (r) {
                l_content.html(r);
            },
            error: function (xhr, status) {
                l_content.html('Error : ' + xhr.status + ', ' + xhr.statusText);
            }
        });
    }
    function fn_programming_build_certificate(p_data) {
        var l_template = p_data[4];
        //pdf
        //[orientation One of "portrait" or "landscape"(or shortcuts "p"(Default), "l")]
        //[unit Measurement unit to be used when coordinates are specified. One of "pt" (points), "mm" (Default), "cm", "in"]
        //[format One of 'a3', 'a4' (Default),'a5' ,'letter' ,'legal']
        var l_pdf = new jsPDF(l_template.sOrientation, "pt", l_template.sPageFormat);
        //add fonts
        //=====> ShiningTimes
        l_pdf.addFileToVFS(jspdfCustomfonts.ShiningTimes.file, jspdfCustomfonts.ShiningTimes.base64);
        l_pdf.addFont(jspdfCustomfonts.ShiningTimes.file, jspdfCustomfonts.ShiningTimes.name, "normal");
        //=====> SoDamnBeautiful
        l_pdf.addFileToVFS(jspdfCustomfonts.SoDamnBeautiful.file, jspdfCustomfonts.SoDamnBeautiful.base64);
        l_pdf.addFont(jspdfCustomfonts.SoDamnBeautiful.file, jspdfCustomfonts.SoDamnBeautiful.name, "normal");
        //=====> Aliquest
        l_pdf.addFileToVFS(jspdfCustomfonts.Aliquest.file, jspdfCustomfonts.Aliquest.base64);
        l_pdf.addFont(jspdfCustomfonts.Aliquest.file, jspdfCustomfonts.Aliquest.name, "normal");
        //=====> AnthoniSignature
        l_pdf.addFileToVFS(jspdfCustomfonts.AnthoniSignature.file, jspdfCustomfonts.AnthoniSignature.base64);
        l_pdf.addFont(jspdfCustomfonts.AnthoniSignature.file, jspdfCustomfonts.AnthoniSignature.name, "normal");
        //=====> Yaty
        l_pdf.addFileToVFS(jspdfCustomfonts.Yaty.file, jspdfCustomfonts.Yaty.base64);
        l_pdf.addFont(jspdfCustomfonts.Yaty.file, jspdfCustomfonts.Yaty.name, "normal");
        //=====> SilverstainSignature
        l_pdf.addFileToVFS(jspdfCustomfonts.SilverstainSignature.file, jspdfCustomfonts.SilverstainSignature.base64);
        l_pdf.addFont(jspdfCustomfonts.SilverstainSignature.file, jspdfCustomfonts.SilverstainSignature.name, "normal");
        //=====> Abecedary
        l_pdf.addFileToVFS(jspdfCustomfonts.Abecedary.file, jspdfCustomfonts.Abecedary.base64);
        l_pdf.addFont(jspdfCustomfonts.Abecedary.file, jspdfCustomfonts.Abecedary.name, "normal");
        //=====> Bintanghu
        l_pdf.addFileToVFS(jspdfCustomfonts.Bintanghu.file, jspdfCustomfonts.Bintanghu.base64);
        l_pdf.addFont(jspdfCustomfonts.Bintanghu.file, jspdfCustomfonts.Bintanghu.name, "normal");
        //=====> Hatachi
        l_pdf.addFileToVFS(jspdfCustomfonts.Hatachi.file, jspdfCustomfonts.Hatachi.base64);
        l_pdf.addFont(jspdfCustomfonts.Hatachi.file, jspdfCustomfonts.Hatachi.name, "normal");
        //add board
        l_pdf.addImage(l_template.oImage.sBase64, 'JPEG', 0, 0, l_template.oSize.fWidthPt, l_template.oSize.fHeightPt);
        //add children
        for (var i in l_template.oChildren) {
            if (l_template.oChildren[i].sType == 'image') {
                l_pdf.addImage(l_template.oChildren[i].oImage.sBase64, 'PNG', l_template.oChildren[i].oPosition.fLeftPt, l_template.oChildren[i].oPosition.fTopPt + 0.5, l_template.oChildren[i].oSize.fWidthPt, l_template.oChildren[i].oSize.fHeightPt);
            } else if (l_template.oChildren[i].sType == 'text') {
                l_pdf.setFont(l_template.oChildren[i].oFont.sFamily);
                l_pdf.setFontSize(l_template.oChildren[i].oFont.iSizePt);
                l_pdf.setFontStyle(l_template.oChildren[i].oFont.sStyle);
                l_pdf.setTextColor(l_template.oChildren[i].oFont.sColor);
                if (l_template.oChildren[i].oSize.bFullWidth) {
                    l_pdf.customText(fn_get_text_to_certificate(l_template.oChildren[i], p_data), { align: "center" }, l_template.oChildren[i].oPosition.fLeftPt, (l_template.oChildren[i].oPosition.fTopPt + l_template.oChildren[i].oSize.fHeightPt - 4));
                } else {
                    l_pdf.text(l_template.oChildren[i].oPosition.fLeftPt, (l_template.oChildren[i].oPosition.fTopPt + l_template.oChildren[i].oSize.fHeightPt - 4), fn_get_text_to_certificate(l_template.oChildren[i], p_data));
                }
            }
        }
        //save
        l_pdf.save('Constancia_' + p_data[1] + '.pdf');
        //fn 
        function fn_get_text_to_certificate(p_child, p_data) {
            if (p_child.oData.sKey == 'person_name') {
                return p_data[1];
            } else if (p_child.oData.sKey == 'course_name') {
                return p_data[0];
            } else if (p_child.oData.sKey == 'test_score') {
                return p_data[3].toString();
            } else if (p_child.oData.sKey == 'approved_date') {
                var l_date = moment(p_data[2], 'DD/MM/YYYY');
                if (p_child.oData.sDateFormat == 'dd/mm/yyyy') {
                    return l_date.format('DD/MM/YYYY');
                } else if (p_child.oData.sDateFormat == 'DIA de MES de AÑO') {
                    return l_date.format('DD') + ' de ' + gf_getNameMonth(l_date.format('MM')) + ' de ' + l_date.format('YYYY');
                } else {
                    return p_data[2];
                }
            } else {
                return '';
            }
        }
    }

    function fn_reset_table_checks() {
        var l_checks = $("#dashboard5_index_table tbody").find("input[data-input=recordatorio]:checked");
        $.each(l_checks, function (i, element) {
            $(element).prop("checked", false);
        });
    }
    //init
    //  Themes
    am4core.useTheme(am4themes_animated);
    fn_course_build_chart();
    //

    fn_set_dropdown_year();

    gf_Helpers_CourseAreaDropDownAll({
        $: $("#dashboard5_index_course_filter_course"),
        callback: function (o) {
            o.addClass("selectpicker").val(null).selectpicker();
        }
    });
    $('.scrollbar-rail').scrollbar();
    //jsPDF
    (function (API) {
        API.customText = function (txt, options, x, y) {
            options = options || {};
            /* Use the options align property to specify desired text alignment
             * Param x will be ignored if desired text alignment is 'center'.
             * Usage of options can easily extend the function to apply different text 
             * styles and sizes 
            */
            if (options.align == "center") {
                // Get current font size
                var fontSize = this.internal.getFontSize();
                // Get page width
                var pageWidth = this.internal.pageSize.width;
                // Get the actual text's width
                /* You multiply the unit width of your string by your font size and divide
                 * by the internal scale factor. The division is necessary
                 * for the case where you use units other than 'pt' in the constructor
                 * of jsPDF.
                */
                var txtWidth = this.getStringUnitWidth(txt) * fontSize / this.internal.scaleFactor;
                // Calculate text's x coordinate
                x = (pageWidth - txtWidth) / 2;
            }
            // Draw text at x,y
            this.text(txt, x, y);
        }
    })(jsPDF.API);
});