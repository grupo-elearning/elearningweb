﻿"use strict";
$("document").ready(function () {
    (function (p_model) {
        //plugins
        $("#Container").mixItUp({
            load: {
                filter: p_model.IsPerson ? 'all' : '.category-protected'
            }
        });
        if (p_model.Notices.length != 0) {
            $('#home_index_slider').slick({
                appendArrows: $('#home_index_slider_arrows'),
                accessibility: false,
                autoplay: true,
                autoplaySpeed: 3000,
                //arrows: false,
                dots: false,
                vertical: false,
                //centerMode: true,
                slidesToShow: 1,
                slidesToScroll: 1
            });
        }
        //fn
        function fn_get_programming(p_id) {
            var l_return = null;
            for (var i in p_model.Courses) {
                if (p_id == p_model.Courses[i].ProgrammingId) {
                    l_return = p_model.Courses[i];
                    break;
                }
            }
            return l_return;
        }
        //init
        if (!g_isMobile.any()) {
            $("[data-app=popover]").each(function () {
                var l_programming = fn_get_programming($(this).attr("data-pcur"));
                $(this).popover({
                    container: 'body',
                    trigger: 'hover',
                    placement: 'right',
                    content: `<div class="cls-1812201745">
                            <h3 class="mt-1 mb-1">
                                <strong>` + l_programming.CourseName + `</strong>
                            </h3>
                            <div class="cls-1812201745-categoria">
                                Categoría : ` + l_programming.CourseCategory + `
                            </div>
                            <div class="cls-1812201745-programa">
                                <span class="cls-1812201745-fechas"><i class="icon ion-ios-calendar"></i> ` + l_programming.FromDate + ` - ` + l_programming.ToDate + `</span>
                                <span class="cls-1812201745-horas"><i class="icon ion-md-time"></i> ` + l_programming.FromHour + ` - ` + l_programming.ToHour + `</span>
                            </div>
                            <div class="cls-1812201745-resumen">
                                ` + l_programming.CourseSummary + `
                            </div>
                            <div class="cls-1812201745-detalle">
                                <ul>
                                    <li>Modalidad : ` + l_programming.Mode + `</li>
                                    <li>Instructor : ` + l_programming.Instructor + `</li>
                                    <!--<li>Examen : ` + l_programming.HasTest + `</li>-->
                                    <li>Lugar : ` + l_programming.Place + `</li>
                                    <li>Sábados y Domingos : ` + l_programming.Weekend + `</li>
                                    <li>Acceso : ` + l_programming.AccessType + `</li>
                                    <li>Constancia : ` + l_programming.HasCertificate + `</li>
                                    <li>Encuestaqqqq : ` + l_programming.HasPoll + `</li>
                                </ul>
                            </div>
                            <div class="cls-1812201745-fin-inscripcion">
                                Fin Inscripción: ` + l_programming.RegistrationLimit + `
                            </div>
                        </div>`,
                    html: true
                });
            });
        }
    }(gf_getClone(ngon.Model)));
});