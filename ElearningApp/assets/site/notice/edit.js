﻿"use strict";
$("document").ready(function () {
    (function (p_model) {
        $('#notice_edit_content').summernote({
            placeholder: 'Ingrese el contenido',
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                //['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['height', ['height']],
                ['view', ['help']]
            ],
            styleTags: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
            height: 740,
            minHeight: 740,
            maxHeight: 740,
            lang: 'es-ES'
        });
        $('#notice_edit_form').submit(function (evt) {
            evt.preventDefault();
        });
        $('#notice_edit_save').click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_psave = gf_presaveForm($("#notice_edit_form")), l_formdata = new FormData(), l_blob, l_html;
            if (l_psave.hasError) {
                $.jGrowl("Ingrese correctamente los datos del formulario", {
                    header: 'Validación'
                });
                return;
            }
            if ($('#notice_edit_content').summernote('isEmpty')) {
                $.jGrowl("Contenido es campo requerido", {
                    header: 'Validación'
                });
                return;
            }
            //build blob
            l_html = `<!DOCTYPE html>
                        <html>
                            <head>
                                <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
                                <link href="https://webelearningsider.azurewebsites.net/assets/css/plugins.min.css" rel="stylesheet" />
                                <link href="https://webelearningsider.azurewebsites.net/assets/css/style.red-800.min.css" rel="stylesheet" />
                                <link href="https://webelearningsider.azurewebsites.net/assets/css/width-boxed.min.css" rel="stylesheet" />
                                <link href="https://webelearningsider.azurewebsites.net/assets/app/app.css" rel="stylesheet" />
                                <link href="https://webelearningsider.azurewebsites.net/assets/site/site.css" rel="stylesheet" />
                            </head>
                            <body style="background-color: #fff; padding: 10px;" data-elearning="notice">
                                ` + $('#notice_edit_content').summernote('code') + `
                            </body>
                        </html>`;
            l_blob = new Blob([l_html], { type: 'text/html;charset=UTF-8' });
            //
            l_formdata.append('Id', p_model.Id);
            l_formdata.append('Description', $("#notice_edit_description").val().trim());
            l_formdata.append('Status', $("#notice_edit_status").is(":checked") ? "A" : "I");
            l_formdata.append('Html.File', l_blob);
            l_formdata.append('Html.OldName', p_model.Html.Name);
            $.daAlert({
                content: {
                    type: "text",
                    value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "save",
                        fn: function () {
                            $.ajax({
                                data: l_formdata,
                                url: '/Notice/Edit',
                                type: 'post',
                                dataType: 'json',
                                contentType: false,
                                processData: false,
                                beforeSend: _beforeSend,
                                success: _success
                            });
                            function _beforeSend() {
                                gf_addLoadingTo($("#notice_edit_save"));
                            }
                            function _success(r) {
                                if (parseInt(r.status) == 1000) {
                                    $.jGrowl(r.response, {
                                        header: 'Información',
                                        type: 'primary'
                                    });
                                    location.href = '/Notice/Index';
                                } else {
                                    $.jGrowl(r.response, {
                                        header: 'Error',
                                        type: 'danger',
                                        clipboard: r.exception
                                    });
                                    gf_removeLoadingTo($("#notice_edit_save"));
                                }
                            }
                            return true;
                        }
                    }
                ],
                maximizable: false,
                title: "Confirmación"
            });
        });
        //init
        $("#notice_edit_description").data({
            'PresaveJson': '{"maxLength":200}',
            'PresaveRequired': true
        });
    }(gf_getClone(ngon.Model)));
});