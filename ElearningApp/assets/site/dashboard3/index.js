﻿"use strict";
$('document').ready(function () {
    var l_chart_fulfillment, l_chart_participants, l_chart_shift, l_chart_schedule, l_chart_schedule_label;
    //plugins
    $('#dashboard3_index_filter_from_date').datepicker().on('changeDate', function (e) {
        $('#dashboard3_index_filter_to_date').datepicker('setStartDate', e.date);
    });
    $('#dashboard3_index_filter_to_date').datepicker({
        orientation: "bottom right"
    }).on('changeDate', function (e) {
        $('#dashboard3_index_filter_from_date').datepicker('setEndDate', e.date);
    });
    //scrollbar
    $('.scrollbar-rail').scrollbar();
    //events
    $('#dashboard3_index_filter_retrieve').click(function (evt) {
        evt.preventDefault();
        fn_get_dashboard();
    });
    $('#dashboard3_index_participants_detail tbody').on('click', 'i[data-action=detail]', function (evt) {
        evt.preventDefault();
        var l_this = $(this), l_tr = l_this.closest('tr'), l_row = $('#dashboard3_index_participants_detail').DataTable().row(l_tr);
        if (l_row.child.isShown()) {
            l_this
                .removeClass('fa-minus-square-o')
                .addClass('fa-plus-square-o');
            l_row.child.hide();
        }
        else {
            l_this
                .removeClass('fa-plus-square-o')
                .addClass('fa-minus-square-o');
            if (l_row.child()) {
                l_row.child.show();
            } else {
                l_row.child('', 'cls-1901280028').show();
                fn_get_participant_detail(l_row);
            }
        }
    });
    //fn
    function fn_get_dashboard() {
        var l_provider = $('#dashboard3_index_filter_provider').val(), l_from_date = $('#dashboard3_index_filter_from_date').val(), l_to_date = $('#dashboard3_index_filter_to_date').val(),
            l_data_fulfillment = [], l_data_participants = [], l_data_schedule = [], l_data_am = [], l_data_pm = [];
        if (!gf_isValue(l_provider)) {
            $.jGrowl("Seleccione un Transportista", {
                header: "Validación"
            });
            return;
        }
        if (!gf_isValue(l_from_date) || !gf_isValue(l_to_date)) {
            $.jGrowl("Ingrese un Rango de Fechas válido", {
                header: "Validación"
            });
            return;
        }
        l_from_date = $('#dashboard3_index_filter_from_date').data('datepicker').getFormattedDate('yyyy-mm-dd');
        l_to_date = $('#dashboard3_index_filter_to_date').data('datepicker').getFormattedDate('yyyy-mm-dd');
        $.ajax({
            url: '/Dashboard3/GetDashboard3',
            data: JSON.stringify({ 'provider': l_provider, 'from_date': l_from_date, 'to_date': l_to_date }),
            type: 'post',
            //async: false,
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                gf_addLoadingTo($('#dashboard3_index_filter_retrieve'));
            },
            success: function (r) {
                if (r.status == 1) {
                    var l_fulfillment = r.response[0], l_participants = r.response[1], l_schedule = r.response[2];
                    //get data fulfillment
                    for (var i in l_fulfillment) {
                        l_data_fulfillment.push({
                            'name': l_fulfillment[i][1],
                            'programmed': l_fulfillment[i][2],
                            'assists': l_fulfillment[i][3]
                        });
                    }
                    //get data participants
                    for (var i in l_participants) {
                        l_data_participants.push({
                            'status': l_participants[i][0],
                            'value': l_participants[i][1]
                        });
                    }
                    //get data schedule
                    for (var i in l_schedule) {
                        if (l_schedule[i][1] == 'AM') {
                            l_data_am.push(l_schedule[i][0]);
                        } else {
                            l_data_pm.push(l_schedule[i][0]);
                        }
                    }
                    l_data_schedule = [{
                        'schedule': `Mañana
                        (00:00 - 11:59)`,
                        'shift': 'Mañana',
                        'value': l_data_am.length,
                        'color': am4core.color('#1D82D3'),
                        'data': l_data_am,
                        'code': 'AM'
                    }, {
                        'schedule': `Tarde
                        (12:00 - 23:59)`,
                        'shift': 'Tarde',
                        'value': l_data_pm.length,
                        'color': am4core.color('#FFC107'),
                        'data': l_data_pm,
                        'code': 'PM'
                    }];
                } else {
                    $.jGrowl(r.response, {
                        header: 'Error',
                        type: 'danger',
                        clipboard: r.exception
                    });
                }
            },
            complete: function () {
                gf_removeLoadingTo($('#dashboard3_index_filter_retrieve'));
                /*---------------------------- Set Data ----------------------------*/
                l_chart_fulfillment.data = l_data_fulfillment;
                l_chart_participants.data = l_data_participants;
                l_chart_shift.data = l_data_schedule;
                l_chart_schedule.data = [];
            }
        });
        fn_get_status_participants_detail('X', l_provider, l_from_date, l_to_date);
    }
    function fn_get_schedule(p_dataContext) {
        var l_data = [], l_schedule = p_dataContext.data, l_ttl;
        if (p_dataContext.code == 'AM') {
            for (var i = 0; i <= 1100; i += 100) {
                l_ttl = 0;
                for (var j in l_schedule) {
                    if (l_schedule[j] >= i && l_schedule[j] < (i + 100)) {
                        l_ttl++;
                    }
                }
                l_data.push({
                    'schedule': gf_convertToHHMM((i / 100)),
                    'value': l_ttl
                });
            }
        } else {
            for (var i = 1200; i <= 2300; i += 100) {
                l_ttl = 0;
                for (var j in l_schedule) {
                    if (l_schedule[j] >= i && l_schedule[j] < (i + 100)) {
                        l_ttl++;
                    }
                }
                l_data.push({
                    'schedule': gf_convertToHHMM((i / 100)),
                    'value': l_ttl
                });
            }
        }
        l_chart_schedule.data = l_data;
        l_chart_schedule_label.html = p_dataContext.code == 'AM' ?
            `<div style="background-image: url(/bower_components/amcharts4/free-animated-svg-weather-icons/day.svg); background-size: 100px 100px;" class="cls-1902100452"></div>` :
            `<div style="background-image: url(/bower_components/amcharts4/free-animated-svg-weather-icons/cloudy-day-2.svg)" class="cls-1902100452"></div>`;
    }
    function fn_get_status_participants_detail(p_approved, p_provider, p_from_date, p_to_date) {
        var l_dataTable = $("#dashboard3_index_participants_detail").daDataTable({
            "DataTable": {
                "ajax": {
                    "url": "/Dashboard3/GetStatusParticipantsDetail",
                    "type": "post",
                    "data": {
                        'approved': p_approved,
                        'provider': p_provider,
                        'from_date': p_from_date,
                        'to_date': p_to_date
                    }
                },
                "aoColumns": [
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center cls-1901280053",
                        "width": "10px",
                        "mRender": function (o) {
                            return '<i class="fa fa-plus-square-o cursor-pointer" data-action="detail"></i>';
                        }
                    },
                    {
                        "data": null,
                        "className": "text-center",
                        "mRender": function (o) {
                            return `<span style="display: none;">` + o[7] + `</span>` + o[0];
                        }
                    },
                    {
                        "data": null,
                        "className": "text-center",
                        "mRender": function (o) {
                            return o[1] + ` - ` + o[2];
                        }
                    },
                    {
                        "data": 9,
                        "className": "text-left"
                    },
                    {
                        "data": 3,
                        "className": "text-left"
                    },
                    {
                        "data": 10,
                        "className": "text-left"
                    },
                    {
                        "data": 4,
                        "className": "text-left"
                    },
                    {
                        "data": 6,
                        "className": "text-center"
                    },
                    {
                        "data": 5,
                        "className": "text-center"
                    }
                ],
                "order": [[1, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 10
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": null,
                "noSearch": [0]
            }
        }).Datatable;
        //buttons
        new $.fn.dataTable.Buttons(l_dataTable, {
            'buttons': [{
                'extend': 'excelHtml5',
                'text': '<i class="fa fa-file-excel-o"></i>Excel',
                'className': 'btn btn-sm btn-raised btn-success m-0 cls-1902151548',
                'exportOptions': {
                    'columns': [1, 2, 3, 4, 5, 6, 7, 8],
                    'format': {
                        'body': function (data, row, column, node) {
                            return column == 0 ?
                                data.replace(/<\s*span[^>]*>(.*?)<\s*\/\s*span>/g, '') :
                                data;
                        }
                    }
                }
            }]
        }).container().appendTo($('#dashboard3_index_participants_detail_buttons'));
    }
    function fn_build_fulfillment_chart() {
        /*---------------------------- Build Chart ----------------------------*/
        //  Chart
        l_chart_fulfillment = am4core.create("dashboard3_index_fulfillment_chart", am4charts.XYChart3D);
        l_chart_fulfillment.language.locale = am4lang_es_ES;
        //  Add data
        l_chart_fulfillment.data = [];
        //  Legend
        l_chart_fulfillment.legend = new am4charts.Legend();
        //  Create axes
        var l_categoryAxis = l_chart_fulfillment.xAxes.push(new am4charts.CategoryAxis());
        l_categoryAxis.dataFields.category = "name";
        l_categoryAxis.renderer.grid.template.location = 0;
        l_categoryAxis.renderer.minGridDistance = 20;
        //  Configure axis label
        var l_label = l_categoryAxis.renderer.labels.template;
        l_label.wrap = true;
        l_label.maxWidth = 120;
        l_label.fontSize = 14;
        //
        var l_valueAxis = l_chart_fulfillment.yAxes.push(new am4charts.ValueAxis());
        l_valueAxis.min = 0;
        l_valueAxis.fontSize = 14;
        //  Add Series =====================================================================>
        var l_series_assists = l_chart_fulfillment.series.push(new am4charts.ColumnSeries3D());
        l_series_assists.dataFields.valueY = "assists";
        l_series_assists.dataFields.categoryX = "name";
        l_series_assists.clustered = false;
        l_series_assists.columns.template.stroke = am4core.color("#fb6e52");
        l_series_assists.columns.template.fill = am4core.color("#fb6e52");
        l_series_assists.columns.template.fillOpacity = .75;
        l_series_assists.columns.template.width = 80;
        l_series_assists.columns.template.tooltipText = "Asistencias: {valueY}";
        l_series_assists.name = "Asistencias";
        //  Add label
        var l_valueLabel_vaca = l_series_assists.bullets.push(new am4charts.LabelBullet());
        l_valueLabel_vaca.label.text = "{valueY}";
        l_valueLabel_vaca.label.horizontalCenter = "center";
        l_valueLabel_vaca.label.dy = -10;
        l_valueLabel_vaca.label.hideOversized = false;
        l_valueLabel_vaca.label.truncate = false;
        //  Add Series
        var l_series_programmed = l_chart_fulfillment.series.push(new am4charts.ColumnSeries3D());
        l_series_programmed.dataFields.valueY = "programmed";
        l_series_programmed.dataFields.categoryX = "name";
        l_series_programmed.clustered = false;
        l_series_programmed.columns.template.stroke = am4core.color("#ffce56");
        l_series_programmed.columns.template.fill = am4core.color("#ffce56");
        l_series_programmed.columns.template.fillOpacity = .75;
        l_series_programmed.columns.template.width = 120;
        l_series_programmed.columns.template.tooltipText = "Programados: {valueY}";
        l_series_programmed.name = "Programados";
        //  Add label
        var l_valueLabel_programmed = l_series_programmed.bullets.push(new am4charts.LabelBullet());
        l_valueLabel_programmed.label.text = "{valueY}";
        l_valueLabel_programmed.label.horizontalCenter = "center";
        l_valueLabel_programmed.label.dy = -10;
        l_valueLabel_programmed.label.hideOversized = false;
        l_valueLabel_programmed.label.truncate = false;
        //<==================================================================================
        //  Add Cursor
        l_chart_fulfillment.cursor = new am4charts.XYCursor();
        l_chart_fulfillment.cursor.lineX.disabled = true;
        l_chart_fulfillment.cursor.lineY.disabled = true;
        //  Add chart title
        var l_title = l_chart_fulfillment.titles.create();
        l_title.text = "Programados vs Asistencias";
        l_title.fontSize = 25;
        l_title.marginBottom = 20;
        //  Enable export
        l_chart_fulfillment.exporting.menu = new am4core.ExportMenu();
        l_chart_fulfillment.exporting.useWebFonts = false;
        l_chart_fulfillment.exporting.menu.items = [
            {
                "label": "...",
                "menu": [
                    { "type": "png", "label": "PNG" },
                    { "type": "jpg", "label": "JPG" },
                    //{ "type": "pdf", "label": "PDF" }
                ]
            }
        ];
    }
    function fn_build_participantes_chart() {
        /*---------------------------- Build Chart ----------------------------*/
        //  Chart
        l_chart_participants = am4core.create("dashboard3_index_participants_chart", am4charts.PieChart);
        l_chart_participants.hiddenState.properties.opacity = 0;
        l_chart_participants.innerRadius = am4core.percent(40);
        l_chart_participants.language.locale = am4lang_es_ES;
        //  Add data
        l_chart_participants.data = [];
        //  Legend
        l_chart_participants.legend = new am4charts.Legend();
        //  Add Series
        var l_series = l_chart_participants.series.push(new am4charts.PieSeries());
        l_series.dataFields.value = "value";
        l_series.dataFields.category = "status";
        l_series.slices.template.cornerRadius = 5;
        l_series.slices.template.stroke = am4core.color("#fff");
        l_series.slices.template.strokeWidth = 2;
        l_series.slices.template.strokeOpacity = 1;
        //l_series.slices.template
        //    .cursorOverStyle = [
        //        {
        //            "property": "cursor",
        //            "value": "pointer"
        //        }
        //    ];
        l_series.alignLabels = false;
        l_series.labels.template.bent = true;
        l_series.labels.template.radius = 10;
        l_series.labels.template.padding(0, 0, 0, 0);
        l_series.labels.template.fontSize = 14;
        l_series.ticks.template.disabled = true;
        //  Add events
        l_series.slices.template.events.on("hit", function (ev) {
            //var l_filter = ev.target.dataItem._dataContext.filter;
            //fn_get_status_participants_detail(l_filter.approved, l_filter.provider, l_filter.from_date, l_filter.to_date);
        }, this);
        // Create a base filter effect (as if it's not there) for the hover to return to
        var l_shadow = l_series.slices.template.filters.push(new am4core.DropShadowFilter);
        l_shadow.opacity = 0;
        // Create hover state
        var l_hoverState = l_series.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists
        // Slightly shift the shadow and make it more prominent on hover
        var l_hoverShadow = l_hoverState.filters.push(new am4core.DropShadowFilter);
        l_hoverShadow.opacity = 0.7;
        l_hoverShadow.blur = 5;
        //  Set colors
        l_series.colors.list = [am4core.color("rgb(174,174,174)"), am4core.color("rgb(165,30,23)"), am4core.color("rgb(84, 84, 84)"), am4core.color("rgb(237,237,237)")];
        //  Enable export
        l_chart_participants.exporting.menu = new am4core.ExportMenu();
        l_chart_participants.exporting.useWebFonts = false;
        l_chart_participants.exporting.menu.items = [
            {
                "label": "...",
                "menu": [
                    { "type": "png", "label": "PNG" },
                    { "type": "jpg", "label": "JPG" }
                ]
            }
        ];
    }
    function fn_build_shift_chart() {
        /*---------------------------- Build Chart ----------------------------*/
        //  Chart
        l_chart_shift = am4core.create("dashboard3_index_shift_chart", am4charts.XYChart);
        l_chart_shift.language.locale = am4lang_es_ES;
        // Add data
        l_chart_shift.data = [];
        // Create axes
        var l_categoryAxis = l_chart_shift.yAxes.push(new am4charts.CategoryAxis());
        l_categoryAxis.dataFields.category = "schedule";
        l_categoryAxis.renderer.grid.template.location = 0;
        l_categoryAxis.renderer.minGridDistance = 30;
        l_categoryAxis.renderer.inversed = true;
        l_categoryAxis.fontSize = 14;
        //
        var l_valueAxis = l_chart_shift.xAxes.push(new am4charts.ValueAxis());
        l_valueAxis.min = 0;
        l_valueAxis.fontSize = 14;
        // Create series
        var l_series = l_chart_shift.series.push(new am4charts.ColumnSeries());
        l_series.dataFields.valueX = "value";
        l_series.dataFields.categoryY = "schedule";
        l_series.name = "Capacitaciones";
        l_series.columns.template.tooltipText = "{shift}: {valueX}";
        l_series.columns.template.propertyFields.stroke = "color";
        l_series.columns.template.propertyFields.fill = "color";
        l_series.columns.template.column.cornerRadiusTopRight = 10;
        l_series.columns.template.column.cornerRadiusBottomRight = 10;
        l_series.columns.template.fillOpacity = .8;
        l_series.columns.template
            .cursorOverStyle = [
                {
                    "property": "cursor",
                    "value": "pointer"
                }
            ];
        //  Add label
        var l_valueLabel = l_series.bullets.push(new am4charts.LabelBullet());
        l_valueLabel.label.text = "{valueX}";
        l_valueLabel.label.horizontalCenter = "center";
        l_valueLabel.label.dx = 10;
        l_valueLabel.label.hideOversized = false;
        l_valueLabel.label.truncate = false;
        //  Add events
        l_series.columns.template.events.on("hit", function (ev) {
            fn_get_schedule(ev.target.dataItem._dataContext);
        }, this);
        //  Enable export
        l_chart_shift.exporting.menu = new am4core.ExportMenu();
        l_chart_shift.exporting.useWebFonts = false;
        l_chart_shift.exporting.menu.items = [
            {
                "label": "...",
                "menu": [
                    { "type": "png", "label": "PNG" },
                    { "type": "jpg", "label": "JPG" }
                ]
            }
        ];
    }
    function fn_build_schedule_chart() {
        /*---------------------------- Build Chart ----------------------------*/
        //  Chart
        l_chart_schedule = am4core.create("dashboard3_index_schedule_chart", am4charts.RadarChart);
        l_chart_schedule.language.locale = am4lang_es_ES;
        l_chart_schedule.hiddenState.properties.opacity = 0; // this creates initial fade-in
        l_chart_schedule.data = [];
        l_chart_schedule.radius = am4core.percent(95);
        l_chart_schedule.startAngle = 270 - 180;
        l_chart_schedule.endAngle = 270 + 180;
        l_chart_schedule.innerRadius = am4core.percent(30);
        l_chart_schedule.colors.list = [am4core.color("#11a9cc")];
        l_chart_schedule.seriesContainer.zIndex = -1;
        l_chart_schedule.paddingLeft = 45;
        l_chart_schedule.paddingRight = 45;
        l_chart_schedule.paddingTop = 30;
        //  Add label
        var l_label_slider = l_chart_schedule.createChild(am4core.Label);
        l_label_slider.text = "Arrastre al deslizador para cambiar el radio";
        l_label_slider.exportable = false;
        l_label_slider.fontSize = 12;
        l_chart_schedule_label = l_chart_schedule.radarContainer.createChild(am4core.Label);
        l_chart_schedule_label.horizontalCenter = "middle";
        l_chart_schedule_label.verticalCenter = "middle";
        //  Create axes
        var l_categoryAxis = l_chart_schedule.xAxes.push(new am4charts.CategoryAxis());
        l_categoryAxis.dataFields.category = "schedule";
        l_categoryAxis.renderer.labels.template.location = 0.5;
        l_categoryAxis.renderer.grid.template.strokeOpacity = 0.1;
        l_categoryAxis.renderer.axisFills.template.disabled = true;
        l_categoryAxis.mouseEnabled = false;
        l_categoryAxis.fontSize = 12;
        //
        var l_valueAxis = l_chart_schedule.yAxes.push(new am4charts.ValueAxis());
        l_valueAxis.tooltip.disabled = true;
        l_valueAxis.renderer.grid.template.strokeOpacity = 0.05;
        l_valueAxis.renderer.axisFills.template.disabled = true;
        l_valueAxis.renderer.axisAngle = 260;
        l_valueAxis.renderer.labels.template.horizontalCenter = "right";
        l_valueAxis.min = 0;
        l_valueAxis.fontSize = 14;
        //  Create series
        var l_series = l_chart_schedule.series.push(new am4charts.RadarColumnSeries());
        l_series.columns.template.radarColumn.strokeOpacity = 1;
        l_series.columns.template.radarColumn.cornerRadius = 3;
        //l_series.columns.template.tooltipText = "{name} {categoryX}: {valueY.value}";
        l_series.columns.template.tooltipText = "Capacitaciones: {valueY.value}";
        l_series.name = "Horario";
        l_series.dataFields.categoryX = "schedule";
        l_series.dataFields.valueY = "value";
        l_series.stacked = true;
        //  Add slider
        var l_slider = l_chart_schedule.createChild(am4core.Slider);
        l_slider.start = 0.5;
        l_slider.exportable = false;
        l_slider.events.on("rangechanged", () => {
            var start = l_slider.start;
            l_chart_schedule.startAngle = 270 - start * 179 - 1;
            l_chart_schedule.endAngle = 270 + start * 179 + 1;
            l_valueAxis.renderer.axisAngle = l_chart_schedule.startAngle;
        });
        //  Enable export
        l_chart_schedule.exporting.menu = new am4core.ExportMenu();
        l_chart_schedule.exporting.useWebFonts = false;
        l_chart_schedule.exporting.menu.items = [
            {
                "label": "...",
                "menu": [
                    { "type": "png", "label": "PNG" },
                    { "type": "jpg", "label": "JPG" }
                ]
            }
        ];
    }
    function fn_get_participant_detail(p_row) {
        var l_content = p_row.child().find('td');
        $.ajax({
            url: '/Dashboard3/ParticipantDetail',
            data: JSON.stringify({ 'id': p_row.data()[8] }),
            contentType: 'application/json; charset=utf-8',
            type: 'post',
            dataType: 'html',
            beforeSend: function () {
                l_content.html('<div class="text-center small p-1 text-dark">Cargando...</div>');
            },
            success: function (r) {
                l_content.html(r);
            },
            error: function (xhr, status) {
                l_content.html('Error : ' + xhr.status + ', ' + xhr.statusText);
            }
        });
    }
    //init
    //  Themes
    am4core.useTheme(am4themes_material);
    am4core.useTheme(am4themes_animated);
    fn_build_fulfillment_chart();
    fn_build_participantes_chart();
    fn_build_shift_chart();
    fn_build_schedule_chart();
    //
    gf_Helpers_ProviderAreaDropDownAll({
        $: $("#dashboard3_index_filter_provider"),
        options: [{ 'value': 0, 'text': 'TODOS' }],
        callback: function (o) {
            o.addClass("selectpicker").val(0).selectpicker();
            fn_get_dashboard();
        }
    });
});