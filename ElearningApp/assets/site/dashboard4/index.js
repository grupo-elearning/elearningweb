﻿"use strict";
$("document").ready(function () {
    var l_colors = gradStop({
        stops: 101,
        inputFormat: 'hex',
        colorArray: ['#EA0000', '#FFEB00', '#3CA100']
    }), l_data = [], l_timeout;
    //events
    $('#dashboard4_index_filter').keyup(function () {
        var l_filter = $(this).val().toUpperCase(), l_filtered;
        if (l_timeout) {
            clearTimeout(l_timeout);
        }
        l_timeout = setTimeout(function () {
            l_filtered = 0;
            $('#dashboard4_index_list div[data-object=provider]').each(function (idx) {
                var l_name = $(this).attr('data-name').toUpperCase();
                if (l_name.search(l_filter) == -1) {
                    $(this).hide();
                } else {
                    $(this).show();
                    l_filtered++;
                }
            });
            $('#dashboard4_index_info_filtered').text(l_filtered);
            l_timeout = null;
        }, 500);
    });
    $('#dashboard4_index_sort_name').click(function (evt) {
        evt.preventDefault();
        gf_addLoadingTo($(this));
        setTimeout(function () {
            var l_this = $('#dashboard4_index_sort_name'), l_ordering = l_this.attr('data-ordering'), l_list = $('#dashboard4_index_list'), l_items = l_list.children('div[data-object=provider]').get();
            l_ordering = l_ordering == '' || l_ordering == 'desc' ? 'asc' : 'desc';
            if (l_ordering == 'asc') {
                l_items.sort(function (a, b) {
                    var l_aname = $(a).attr('data-name').toUpperCase(), l_bname = $(b).attr('data-name').toUpperCase();
                    return (l_aname < l_bname) ? -1 : (l_aname > l_bname) ? 1 : 0;
                });
            } else {
                l_items.sort(function (a, b) {
                    var l_aname = $(a).attr('data-name').toUpperCase(), l_bname = $(b).attr('data-name').toUpperCase();
                    return (l_aname > l_bname) ? -1 : (l_aname < l_bname) ? 1 : 0;
                });
            }
            $.each(l_items, function (idx, itm) { l_list.append(itm); });
            l_this
                .attr('data-ordering', l_ordering)
                .find('i')
                .removeClass('fa-arrows-v fa-sort-alpha-desc fa-sort-alpha-asc')
                .addClass(l_ordering == 'asc' ? 'fa-sort-alpha-asc' : 'fa-sort-alpha-desc');
            $('#dashboard4_index_sort_progress')
                .find('i')
                .removeClass('fa-sort-numeric-desc fa-sort-numeric-asc')
                .addClass('fa-arrows-v');
            gf_removeLoadingTo(l_this);
        }, 10);
    });
    $('#dashboard4_index_sort_progress').click(function (evt) {
        evt.preventDefault();
        gf_addLoadingTo($(this));
        setTimeout(function () {
            var l_this = $('#dashboard4_index_sort_progress'), l_ordering = l_this.attr('data-ordering'), l_list = $('#dashboard4_index_list'), l_items = l_list.children('div[data-object=provider]').get();
            l_ordering = l_ordering == '' || l_ordering == 'asc' ? 'desc' : 'asc';
            if (l_ordering == 'asc') {
                l_items.sort(function (a, b) {
                    var l_aprogress = parseFloat($(a).attr('data-progress')), l_bprogress = parseFloat($(b).attr('data-progress'));
                    return (l_aprogress < l_bprogress) ? -1 : (l_aprogress > l_bprogress) ? 1 : 0;
                });
            } else {
                l_items.sort(function (a, b) {
                    var l_aprogress = parseFloat($(a).attr('data-progress')), l_bprogress = parseFloat($(b).attr('data-progress'));
                    return (l_aprogress > l_bprogress) ? -1 : (l_aprogress < l_bprogress) ? 1 : 0;
                });
            }
            $.each(l_items, function (idx, itm) { l_list.append(itm); });
            l_this
                .attr('data-ordering', l_ordering)
                .find('i')
                .removeClass('fa-arrows-v fa-sort-numeric-desc fa-sort-numeric-asc')
                .addClass(l_ordering == 'asc' ? 'fa-sort-numeric-asc' : 'fa-sort-numeric-desc');
            $('#dashboard4_index_sort_name')
                .find('i')
                .removeClass('fa-sort-alpha-desc fa-sort-alpha-asc')
                .addClass('fa-arrows-v');
            gf_removeLoadingTo(l_this);
        }, 10);
    });
    $('#dashboard4_index_export_excel').click(function (evt) {
        evt.preventDefault();
        var l_export = fn_get_to_export('all');
        fn_export(l_export, $(this));
    });
    $('#dashboard4_index_list').on('click', 'button[data-action=export-provider]', function (evt) {
        evt.preventDefault();
        var l_provider = $(this).attr('data-provider');
        var l_export = fn_get_to_export('provider', l_provider);
        fn_export(l_export, $(this));
    });
    $('#dashboard4_index_list').on('click', 'button[data-action=export-company]', function (evt) {
        evt.preventDefault();
        var l_provider = $(this).attr('data-provider');
        var l_export = fn_get_to_export('type', l_provider, 'company');
        fn_export(l_export, $(this));
    });
    $('#dashboard4_index_list').on('click', 'button[data-action=export-employee]', function (evt) {
        evt.preventDefault();
        var l_provider = $(this).attr('data-provider');
        var l_export = fn_get_to_export('type', l_provider, 'employee');
        fn_export(l_export, $(this));
    });
    $('#dashboard4_index_list').on('click', 'button[data-action=export-vehicle]', function (evt) {
        evt.preventDefault();
        var l_provider = $(this).attr('data-provider');
        var l_export = fn_get_to_export('type', l_provider, 'vehicle');
        fn_export(l_export, $(this));
    });
    //fn
    function fn_export(p_data, p_this) {
        var l_formdata = new FormData();
        for (var i in p_data) {
            Array.isArray(p_data[i])
                ? p_data[i].forEach(value => l_formdata.append(i + '[]', value))
                : l_formdata.append(i, p_data[i]);
        }
        l_formdata.append('Theme', 'dalba');//[basic, dalba]
        $.ajax({
            //data: JSON.stringify(p_data),
            data: l_formdata,
            url: '/Dashboard4/ExportToExcel',
            type: 'post',
            dataType: 'json',
            //contentType: "application/json; charset=utf-8",
            contentType: false,
            processData: false,
            beforeSend: function () {
                gf_addLoadingTo(p_this);
            },
            success: function (r) {
                if (r.status == 1) {
                    var tag_a = document.createElement('a');
                    tag_a.href = '/Helpers/DownloadExcel?FileName=' + r.response.name + '&DownloadName=Dashboard4.xlsx';
                    tag_a.download = 'myExport.xlsx';
                    tag_a.click();
                    tag_a = null;
                } else {
                    $.jGrowl(r.response, {
                        header: 'Error',
                        type: 'danger',
                        clipboard: r.exception
                    });
                }
            },
            complete: function () {
                gf_removeLoadingTo(p_this);
            }
        });
    }
    function fn_get_to_export(p_export, p_provider, p_type) {
        var l_return = {
            'Export': p_export,
            'Type': p_type ? p_type : '',
            'Pid': [],
            'Ruc': [],  //Ruc
            'Pro': [],
            'Pdo': [],
            'Pds': [],
            'Eid': [],
            'End': [],  //Empleado - nro doc
            'Emp': [],
            'Efi': [],
            'Edo': [],
            'Eds': [],
            'Vid': [],
            'Veh': [],
            'Vdo': [],
            'Vds': []
        };

        if (p_export == 'all') {
            l_data.forEach(i => {
                i.Dc.forEach(j => {
                    l_return.Pid.push(i.Id);
                    l_return.Ruc.push(i.Ru);
                    l_return.Pro.push(i.Bn);
                    l_return.Pdo.push(j.Nm);
                    l_return.Pds.push(j.St);
                    l_return.Eid.push(0);
                    l_return.End.push("");
                    l_return.Emp.push("");
                    l_return.Efi.push("");
                    l_return.Edo.push("");
                    l_return.Eds.push("");
                    l_return.Vid.push(0);
                    l_return.Veh.push("");
                    l_return.Vdo.push("");
                    l_return.Vds.push("");
                });
                debugger;
                i.Ep.forEach(j => {
                    j.Dc.forEach(k => {
                        l_return.Pid.push(i.Id);
                        l_return.Ruc.push(i.Ru);
                        l_return.Pro.push(i.Bn);
                        l_return.Pdo.push("");
                        l_return.Pds.push("");
                        l_return.Eid.push(j.Id);
                        l_return.End.push(j.Nd);
                        l_return.Emp.push(j.Fn);
                        l_return.Efi.push(j.Fi);
                        l_return.Edo.push(k.Nm);
                        l_return.Eds.push(k.St);
                        l_return.Vid.push(0);
                        l_return.Veh.push("");
                        l_return.Vdo.push("");
                        l_return.Vds.push("");
                    });
                });
                i.Vc.forEach(j => {
                    j.Dc.forEach(k => {
                        l_return.Pid.push(i.Id);
                        l_return.Ruc.push(i.Ru);
                        l_return.Pro.push(i.Bn);
                        l_return.Pdo.push("");
                        l_return.Pds.push("");
                        l_return.Eid.push(0);
                        l_return.End.push("");
                        l_return.Emp.push("");
                        l_return.Efi.push("");
                        l_return.Edo.push("");
                        l_return.Eds.push("");
                        l_return.Vid.push(j.Id);
                        l_return.Veh.push(j.Nm);
                        l_return.Vdo.push(k.Nm);
                        l_return.Vds.push(k.St);
                    });
                });
            });
        } else if (p_export == 'provider') {
            l_data.forEach(i => {
                if (i.Id == p_provider) {
                    i.Dc.forEach(j => {
                        l_return.Pid.push(i.Id);
                        l_return.Ruc.push(i.Ru);
                        l_return.Pro.push(i.Bn);
                        l_return.Pdo.push(j.Nm);
                        l_return.Pds.push(j.St);
                        l_return.Eid.push(0);
                        l_return.End.push("");
                        l_return.Emp.push("");
                        l_return.Efi.push("");
                        l_return.Edo.push("");
                        l_return.Eds.push("");
                        l_return.Vid.push(0);
                        l_return.Veh.push("");
                        l_return.Vdo.push("");
                        l_return.Vds.push("");
                    });
                    i.Ep.forEach(j => {
                        j.Dc.forEach(k => {
                            l_return.Pid.push(i.Id);
                            l_return.Ruc.push(i.Ru);
                            l_return.Pro.push(i.Bn);
                            l_return.Pdo.push("");
                            l_return.Pds.push("");
                            l_return.Eid.push(j.Id);
                            l_return.End.push(j.Nd);
                            l_return.Emp.push(j.Fn);
                            l_return.Efi.push(j.Fi);
                            l_return.Edo.push(k.Nm);
                            l_return.Eds.push(k.St);
                            l_return.Vid.push(0);
                            l_return.Veh.push("");
                            l_return.Vdo.push("");
                            l_return.Vds.push("");
                        });
                    });
                    i.Vc.forEach(j => {
                        j.Dc.forEach(k => {
                            l_return.Pid.push(i.Id);
                            l_return.Ruc.push(i.Ru);
                            l_return.Pro.push(i.Bn);
                            l_return.Pdo.push("");
                            l_return.Pds.push("");
                            l_return.Eid.push(0);
                            l_return.End.push("");
                            l_return.Emp.push("");
                            l_return.Efi.push("");
                            l_return.Edo.push("");
                            l_return.Eds.push("");
                            l_return.Vid.push(j.Id);
                            l_return.Veh.push(j.Nm);
                            l_return.Vdo.push(k.Nm);
                            l_return.Vds.push(k.St);
                        });
                    });
                }
            });
        } else if (p_export == 'type') {
            if (p_type == 'company') {
                l_data.forEach(i => {
                    if (i.Id == p_provider) {
                        i.Dc.forEach(j => {
                            l_return.Pid.push(i.Id);
                            l_return.Ruc.push(i.Ru);
                            l_return.Pro.push(i.Bn);
                            l_return.Pdo.push(j.Nm);
                            l_return.Pds.push(j.St);
                            l_return.Eid.push(0);
                            l_return.End.push("");
                            l_return.Emp.push("");
                            l_return.Efi.push("");
                            l_return.Edo.push("");
                            l_return.Eds.push("");
                            l_return.Vid.push(0);
                            l_return.Veh.push("");
                            l_return.Vdo.push("");
                            l_return.Vds.push("");
                        });
                    }
                });
            } else if (p_type == 'employee') {
                l_data.forEach(i => {
                    if (i.Id == p_provider) {
                        i.Ep.forEach(j => {
                            j.Dc.forEach(k => {
                                l_return.Pid.push(i.Id);
                                l_return.Ruc.push(i.Ru);
                                l_return.Pro.push(i.Bn);
                                l_return.Pdo.push("");
                                l_return.Pds.push("");
                                l_return.Eid.push(j.Id);
                                l_return.End.push(j.Nd);
                                l_return.Emp.push(j.Fn);
                                l_return.Efi.push(j.Fi);
                                l_return.Edo.push(k.Nm);
                                l_return.Eds.push(k.St);
                                l_return.Vid.push(0);
                                l_return.Veh.push("");
                                l_return.Vdo.push("");
                                l_return.Vds.push("");
                            });
                        });
                    }
                });
            } else if (p_type == 'vehicle') {
                l_data.forEach(i => {
                    if (i.Id == p_provider) {
                        i.Vc.forEach(j => {
                            j.Dc.forEach(k => {
                                l_return.Pid.push(i.Id);
                                l_return.Ruc.push(i.Ru);
                                l_return.Pro.push(i.Bn);
                                l_return.Pdo.push("");
                                l_return.Pds.push("");
                                l_return.Eid.push(0);
                                l_return.End.push("");
                                l_return.Emp.push("");
                                l_return.Efi.push("");
                                l_return.Edo.push("");
                                l_return.Eds.push("");
                                l_return.Vid.push(j.Id);
                                l_return.Veh.push(j.Nm);
                                l_return.Vdo.push(k.Nm);
                                l_return.Vds.push(k.St);
                            });
                        });
                    }
                });
            }
        }

        return l_return;
    }
    function fn_get_progress_by_type(p_provider, p_type) {
        var l_ok = 0, l_total = 0;
        if (p_type == 'company') {
            l_data.forEach(i => {
                if (i.Id == p_provider) {
                    i.Dc.forEach(j => {
                        l_ok += j.St;
                        l_total++;
                    });
                }
            });
        } else if (p_type == 'employee') {
            l_data.forEach(i => {
                if (i.Id == p_provider) {
                    i.Ep.forEach(j => {
                        j.Dc.forEach(k => {
                            l_ok += k.St;
                            l_total++;
                        });
                    });
                }
            });
        } else if (p_type == 'vehicle') {
            l_data.forEach(i => {
                if (i.Id == p_provider) {
                    i.Vc.forEach(j => {
                        j.Dc.forEach(k => {
                            l_ok += k.St;
                            l_total++;
                        });
                    });
                }
            });
        }
        return l_total == 0 ? 0 : gf_roundNumber(((l_ok / l_total) * 100), 2);
    }
    function fn_initialize() {
        var l_list = $('#dashboard4_index_list'), l_tcompany = $('#dashboard4_index_total_company'), l_temployee = $('#dashboard4_index_total_employee'),
            l_tvehicle = $('#dashboard4_index_total_vehicle'), l_ttotal = $('#dashboard4_index_total_total'), l_ttl_company = 0,
            l_ttl_employee = 0, l_ttl_vehicle = 0, l_ttl_total = 0, l_count = l_data.length;
        //build list
        for (var i in l_data) {
            l_list.append(`<div class="row" data-object="provider" data-provider="` + l_data[i].Id + `" data-name="` + l_data[i].Bn + `" data-progress="">
                                <div class="col-12">
                                    <div class="app-row mt-05">
                                        <button class="btn btn-sm btn-raised btn-success m-0 pull-right" title="Exportar" data-action="export-provider" data-provider="` + l_data[i].Id + `">
                                            <i class="fa fa-file-excel-o m-0"></i>
                                        </button>
                                    </div>
                                    <h4 class="mb-15 mt-1 color-dark font-weight-normal app-row">` + l_data[i].Bn + `<span class="ml-1 pull-right text-info" data-object="progress"></span> </h4>
                                </div>
                                <div class="col-12 col-sm-12 col-md-4 mb-15">
                                    <div class="bg-white position-relative cls-1903151649 cls-1901230349">
                                        <button class="btn btn-sm btn-raised btn-success m-0 position-absolute cls-1903181624" title="Exportar" data-action="export-company" data-provider="` + l_data[i].Id + `">
                                            <i class="fa fa-file-excel-o m-0"></i>
                                        </button>
                                        <h3 class="text-center text-dark font-weight-normal mt-0 cls-1903151653">Empresa ( <span data-object="company-progress"></span>% )</h3>
                                        <div class="position-relative">
                                            <div class="position-absolute cls-1903151429-left"></div>
                                            <div class="cls-1903151429">
                                                <div class="position-relative cls-1903151429-content" data-chart="company-document">
                                                    <div class="position-absolute cls-1903151429-right cls-1903151429-top"></div>
                                                </div>
                                            </div>
                                            <div class="position-absolute cls-1903151429-right"></div>
                                            <div class="position-absolute cls-1903151429-progress-0">0%</div>
                                            <div class="position-absolute cls-1903151429-progress-50">50%</div>
                                            <div class="position-absolute cls-1903151429-progress-100">100%</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-4 mb-15">
                                    <div class="bg-white position-relative cls-1903151649 cls-1901230349">
                                        <button class="btn btn-sm btn-raised btn-success m-0 position-absolute cls-1903181624" title="Exportar" data-action="export-employee" data-provider="` + l_data[i].Id + `">
                                            <i class="fa fa-file-excel-o m-0"></i>
                                        </button>
                                        <h3 class="text-center text-dark font-weight-normal mt-0 cls-1903151653">Trabajador ( <span data-object="employee-progress"></span>% )</h3>
                                        <div class="position-relative">
                                            <div class="position-absolute cls-1903151429-left"></div>
                                            <div class="cls-1903151429">
                                                <div class="position-relative cls-1903151429-content" data-chart="employee-document">
                                                    <div class="position-absolute cls-1903151429-right cls-1903151429-top"></div>
                                                </div>
                                            </div>
                                            <div class="position-absolute cls-1903151429-right"></div>
                                            <div class="position-absolute cls-1903151429-progress-0">0%</div>
                                            <div class="position-absolute cls-1903151429-progress-50">50%</div>
                                            <div class="position-absolute cls-1903151429-progress-100">100%</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-4 mb-15">
                                    <div class="bg-white position-relative cls-1903151649 cls-1901230349">
                                        <button class="btn btn-sm btn-raised btn-success m-0 position-absolute cls-1903181624" title="Exportar" data-action="export-vehicle" data-provider="` + l_data[i].Id + `">
                                            <i class="fa fa-file-excel-o m-0"></i>
                                        </button>
                                        <h3 class="text-center text-dark font-weight-normal mt-0 cls-1903151653">Vehículo ( <span data-object="vehicle-progress"></span>% )</h3>
                                        <div class="position-relative">
                                            <div class="position-absolute cls-1903151429-left"></div>
                                            <div class="cls-1903151429">
                                                <div class="position-relative cls-1903151429-content" data-chart="vehicle-document">
                                                    <div class="position-absolute cls-1903151429-right cls-1903151429-top"></div>
                                                </div>
                                            </div>
                                            <div class="position-absolute cls-1903151429-right"></div>
                                            <div class="position-absolute cls-1903151429-progress-0">0%</div>
                                            <div class="position-absolute cls-1903151429-progress-50">50%</div>
                                            <div class="position-absolute cls-1903151429-progress-100">100%</div>
                                        </div>
                                    </div>
                                </div>
                            </div>`);
        }
        //
        l_list.find('div[data-object=provider]').each(function (idx) {
            var l_value, l_ttl_progress = 0, l_provider = $(this).attr('data-provider'), l_chart_company = $(this).find('div[data-chart=company-document]'),
                l_chart_employee = $(this).find('div[data-chart=employee-document]'), l_chart_vehicle = $(this).find('div[data-chart=vehicle-document]');
            //build chart company
            l_value = fn_get_progress_by_type(l_provider, 'company');
            $(this).find('span[data-object=company-progress]').text(l_value);
            l_chart_company
                .css({ 'width': (l_value < 4 ? 3.4 : l_value) + '%', 'background-color': l_colors[parseInt(l_value)] })
                .attr('title', l_value + '%')
                .tooltip({
                    'placement': 'right'
                });
            l_ttl_progress += l_value;
            l_ttl_company += l_value;
            //build chart employee
            l_value = fn_get_progress_by_type(l_provider, 'employee');
            $(this).find('span[data-object=employee-progress]').text(l_value);
            l_chart_employee
                .css({ 'width': (l_value < 4 ? 3.4 : l_value) + '%', 'background-color': l_colors[parseInt(l_value)] })
                .attr('title', l_value + '%')
                .tooltip({
                    'placement': 'right'
                });
            l_ttl_progress += l_value;
            l_ttl_employee += l_value;
            //build chart vehicle
            l_value = fn_get_progress_by_type(l_provider, 'vehicle');
            $(this).find('span[data-object=vehicle-progress]').text(l_value);
            l_chart_vehicle
                .css({ 'width': (l_value < 4 ? 3.4 : l_value) + '%', 'background-color': l_colors[parseInt(l_value)] })
                .attr('title', l_value + '%')
                .tooltip({
                    'placement': 'right'
                });
            l_ttl_progress += l_value;
            l_ttl_vehicle += l_value;
            //set progress
            l_ttl_progress = gf_roundNumber(l_ttl_progress / 3, 2);
            $(this).attr('data-progress', l_ttl_progress);
            $(this).find('span[data-object=progress]').text(l_ttl_progress + '%');
        });
        l_ttl_company = l_count == 0 ? 0 : gf_roundNumber(l_ttl_company / l_count, 2);
        l_ttl_employee = l_count == 0 ? 0 : gf_roundNumber(l_ttl_employee / l_count, 2);
        l_ttl_vehicle = l_count == 0 ? 0 : gf_roundNumber(l_ttl_vehicle / l_count, 2);
        l_ttl_total = gf_roundNumber((l_ttl_company + l_ttl_employee + l_ttl_vehicle) / 3, 2);
        l_tcompany.css('background-color', l_colors[parseInt(l_ttl_company)]);
        l_temployee.css('background-color', l_colors[parseInt(l_ttl_employee)]);
        l_tvehicle.css('background-color', l_colors[parseInt(l_ttl_vehicle)]);
        l_ttotal.css('background-color', l_colors[parseInt(l_ttl_total)]);
        l_tcompany.find('strong[data-object=value]').text(l_ttl_company + '%');
        l_temployee.find('strong[data-object=value]').text(l_ttl_employee + '%');
        l_tvehicle.find('strong[data-object=value]').text(l_ttl_vehicle + '%');
        l_ttotal.find('strong[data-object=value]').text(l_ttl_total + '%');
        $('#dashboard4_index_info_total, #dashboard4_index_info_filtered').text(l_count);
        gf_hideLoading();
    }
    //init
    (function () {
        $.ajax({
            url: '/Dashboard4/GetDashboard4',
            type: 'post',
            //async: false,
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                gf_showLoading();
            },
            success: function (r) {
                if (r.status == 1) {
                    l_data = r.response;
                } else {
                    $.jGrowl(r.response, {
                        header: 'Error',
                        type: 'danger',
                        clipboard: r.exception
                    });
                }
            },
            complete: fn_initialize
        });
    }());
});