﻿$(document).ready(function () {

    console.log(ngon);
    var cantPag = 8;
    var cPag = 1;
    var l_success = false;

    function loadDataInitial() {
        //Load Tipo Filtro
        $.ajax({
            data: JSON.stringify({ Codi_Mcap: ngon.Codi_Mcap, UserSystem: ngon.username, Ip: ngon.ipaddress}),
            url: ngon.UrlApi_microa_id,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    drawHtml(res.Body);
                } else {
                    $.jGrowl("No se pudo cargar los slider", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo cargar los slider"
                    });
                }
            },
            complete: function () { },
            error: function () {
                $.jGrowl("No se pudo cargar los slider", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "No se pudo cargar los slider"
                });
            }
        });

        //ANALITICA DE INTERACCION
        $.ajax({
            data: JSON.stringify({
                "Codi_Inte": 0,
                "Vista": "MICROAPRENDIZAJE_ITEM",
                "Identificador": ngon.Codi_Mcap,
                "Codigo": "",
                "Url": window.location.href,
                "UserSystem": ngon.username,
                "Ip": ngon.ipaddress
            }),
            url: ngon.UrlApi_HomeLms_saveInteraccion,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    
                } else {

                }
            },
            complete: function () { },
            error: function () {
            }
        });
    }

    function drawHtml(object) {
        console.log(object);
        $("#id_titulo").text(object.Titulo);
        $("#id_usua").text(object.Usua_Crea);
        $("#id_fecha").text(object.Fech_Crea);

        $("#id_contenedor").append(object.Html);

    }

    loadDataInitial();

});