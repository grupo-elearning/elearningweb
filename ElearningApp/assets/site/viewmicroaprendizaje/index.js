﻿$(document).ready(function () {

    console.log(ngon);
    var cantPag = 8;
    var cPag = 1;
    var l_success = false;

    $("#id_pagi").on('click', 'li[data-action=selecpag]', function (evt) {
        evt.preventDefault();

        if (!($(this).hasClass('active'))) {
            let d_start = $(this).attr("data-start");
            let d_end = $(this).attr("data-end");
            let d_pag = $(this).attr("data-pag");

            for (let dd = 1; dd < cPag; dd++) {
                $("#numpag_" + dd).removeClass("active");
            }
            $(this).addClass("active");
            loadDataInitial(d_start, d_end, d_pag, 'SELECT_PAG');
        }

    });

    $("#id_pagi").on('click', 'li[data-action=prev]', function (evt) {
        evt.preventDefault();
        console.log("PREV");
        for (let dd = 1; dd < cPag; dd++) {
            //$("#numpag_" + dd).removeClass("active");
            if (($("#numpag_" + dd).hasClass('active'))) {
                console.log("Pagina actual: " + dd);
                if (dd > 1) {
                    $("#numpag_" + (dd - 1)).trigger("click");
                }
                break;
            }
        }

    });

    $("#id_pagi").on('click', 'li[data-action=next]', function (evt) {
        evt.preventDefault();
        console.log("NEXT");
        for (let dd = 1; dd < cPag; dd++) {
            //$("#numpag_" + dd).removeClass("active");
            if (($("#numpag_" + dd).hasClass('active'))) {
                console.log("Pagina actual: " + dd);
                if (dd < cPag) {
                    $("#numpag_" + (dd + 1)).trigger("click");
                }
                break;
            }
        }

    });


    function loadDataInitial(v_start, v_end, v_pag, op) {
        //Load Tipo Filtro
        $.ajax({
            data: JSON.stringify({ Start: v_start, End: v_end, Pag: v_pag, Vis: 'VISIBLE' }),
            url: ngon.UrlApi_microa_list,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    drawNoteBlogs(res.ListBody, op);
                } else {
                    $.jGrowl("No se pudo cargar los slider", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo cargar los slider"
                    });
                }
            },
            complete: function () { },
            error: function () {
                $.jGrowl("No se pudo cargar los slider", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "No se pudo cargar los slider"
                });
            }
        });
    }

    function drawNoteBlogs(Posts, Op) {
        var cant_total = 0;
        $("#id_articulo").empty();
        for (let itm in Posts) {
            cant_total = Posts[itm].Total_Rows;
            $("#id_articulo").append(`<article class="card card-primary wow zoomInRight animation-delay-5 mb-4">
                        <div class="card-body overflow-hidden card-body-big" style="padding: 2rem !important">
                            <div class="row">
                                <div class="col-xl-8">
                                    <h3 class="no-mt">
                                        <a href="javascript:void(0)" style="font-weight: 500;">`+ Posts[itm].Titulo + `</a>
                                    </h3>
                                </div>
                                <div class="col-xl-4">
                                    <a href="javascript:void(0)" style="display:none;" class="btn-circle btn-circle-raised btn-circle-xs mb-1 ml-1 no-mr-md btn-facebook pull-right">
                                        <i class="zmdi zmdi-eye"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-10">
                                    <img src="`+ window.location.protocol + `assets/img/avatar.png" alt="..." class="rounded-circle mr-1" style="width: 40px;"> por
                                    <a href="javascript:void(0)">`+ Posts[itm].Usua_Crea + `</a>
                                    <span class="ml-1 d-none d-sm-inline">
                                        <i class="zmdi zmdi-time mr-05 color-info"></i>
                                        <span class="color-medium-dark">`+ Posts[itm].Fech_Crea + `</span>
                                    </span>
                                </div>
                                <div class="col-lg-2 text-right">
                                    <a href="/ViewMicroAprendizaje/Item/` + Posts[itm].Codi_Mcap + `" action-edit data-id="`+ Posts[itm].Codi_Mcap + `" class="btn btn-info btn-raised btn-block animate-icon">
                                        VER
                                        <i class="ml-1 no-mr zmdi zmdi-long-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </article>`);
        }
        if (Op == 'INICIAL') {
            buildPaginacion(cant_total);
        }
    }

    function buildPaginacion(cant_total) {
        console.log(cant_total);
        $("#id_pagi").empty();
        let opactive = 'active';
        let inicial = `<li class="page-item" data-action="prev">
                           <a class="page-link" href="javascript:void(0)" aria-label="Previous">
                              <span aria-hidden="true">&laquo;</span>
                           </a>
                       </li>`;
        let final = `<li class="page-item" data-action="next">
                           <a class="page-link" href="javascript:void(0)" aria-label="Next">
                             <span aria-hidden="true">&raquo;</span>
                           </a>
                     </li>`;

        let pag_general = inicial;
        cPag = 1;
        for (let d = 1; (d - 1) < cant_total; d = d + cantPag) {
            console.log(d);
            pag_general = pag_general + `<li id="numpag_` + cPag + `" class="page-item ` + opactive + `" data-action="selecpag" data-pag="` + cPag + `" data-start="` + (d) + `"  data-end="` + ((d + cantPag) - 1) + `">
                                            <a class="page-link" href="javascript:void(0)">`+ cPag + `</a>
                                        </li>`;
            opactive = '';
            cPag++;
        }

        pag_general = pag_general + final;

        $("#id_pagi").append(pag_general);
    }


    loadDataInitial(cantPag, 0, 1, 'INICIAL');


});