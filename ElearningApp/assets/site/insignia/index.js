﻿$(document).ready(function () {

    console.log(ngon);

    l_dataTable = $("#insginia_index_table").daDataTable({
        "DataTable": {
            "aoColumns": [
                {
                    "className": "text-left"
                },
                {
                    "width": "80px",
                    "className": "text-center"
                },
                {
                    "width": "80px",
                    "className": "text-center"
                },
                {
                    "width": "50px",
                    "className": "text-center"
                },
                {
                    "width": "50px",
                    "className": "text-center"
                }
            ],
            "processing": true,
            "bLengthChange": false,
            "iDisplayLength": 10
        },
        "theme": "devexpress",
        "imgProcessing": "/assets/dalba/img/DXR.gif",
        "search": {
            "btn": { "column": 4 }
        }
    }).Datatable;

    $("#insginia_index_table tbody").on('click', 'a[data-action=delete]', function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        console.log("hOLAS:  " + $(this).attr("data-id"));
        let codi_lmin = $(this).attr("data-id");
        var l_datatable = $("#insginia_index_table").DataTable();
        var l_data = l_datatable.row($(this).closest('tr')).data();
        $.daAlert({
            content: {
                type: 'text',
                value: '<div class="text-center p-05">¿Está seguro que desea eliminar?</div>'
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "remove",
                    fn: function (p, e) {
                        var l_success = false;
                        $.ajax({
                            data: JSON.stringify({ Codi_Lmin: codi_lmin, UserSystem: ngon.username, Ip: ngon.ipaddress }),
                            url: ngon.UrlApi_insign_delete,
                            headers: {
                                'Authorization': ngon.token_access
                            },
                            type: 'post',
                            dataType: 'json',
                            contentType: "application/json; charset=utf-8",
                            success: _success,
                            complete: _complete
                        });
                        function _success(res) {
                            if (res.Status != undefined && res.Status == 200) {
                                $.jGrowl("Se eliminó correctamente", {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                l_success = true;
                            } else {
                                $.jGrowl("Error", {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: "Error"
                                });
                            }
                        }
                        function _complete() {
                            if (l_success) {
                                loadDataInitial();
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: 'Confirmación'
        });
    });

    function loadDataInitial() {
        $.ajax({
            data: '',
            url: ngon.UrlApi_insign_list,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    drawTableInsignia(res.ListBody);
                } else {
                    $.jGrowl("No se pudo cargar las insignias", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo cargar las insignias"
                    });
                }
            },
            complete: function () { },
            error: function () {
                $.jGrowl("No se pudo cargar las insignias", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "No se pudo cargar las insignias"
                });
            }
        });
    }

    function drawTableInsignia(listInsignia) {
        let trDOM;
        console.log(listInsignia);
        l_dataTable.clear().draw();
        for (let itm in listInsignia) {
            console.log(itm);
            trDOM = l_dataTable.row.add([
                listInsignia[itm].Nombre,
                listInsignia[itm].Rango_Inicio,
                listInsignia[itm].Rango_Fin,
                `<img src="` + listInsignia[itm].Url + `" alt="Imagen" style="width:80px">`,
                `<a href="/Insignia/Edit/` + listInsignia[itm].Codi_Lmin +`" data-action="edit" data-id="` + listInsignia[itm].Codi_Lmin + `" class="btn btn-xs btn-raised btn-warning m-0 cls-1810072234" title="Editar" >
                                    <i class="fa fa-edit m-0"></i>
                                </a>
                                <a href="javascript:void(0)" class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" data-action="delete" data-id="`+ listInsignia[itm].Codi_Lmin + `" title="Eliminar" >
                             <i class="fa fa-trash m-0"></i>
                     </a>`
            ]).draw(false).node();

        }
    }

    loadDataInitial();

});