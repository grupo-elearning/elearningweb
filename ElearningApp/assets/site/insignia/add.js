﻿$(document).ready(function () {

    console.log(ngon);


    $("#insignia_create_form").submit(function (evt) {
        evt.preventDefault();
    });
    $('#insignia_create_image').change(function () {
        var l_validate = gf_fileValidation({
            fileInput: $(this),
            imgPreview: $("#insignia_create_image_preview"),
            fnError: function () {
                $.jGrowl("Solo puede seleccionar archivos tipo: jpg, jpeg, png, bmp", {
                    header: "Validación"
                });
            }
        });
        if (l_validate) {
            $("#insignia_create_image_notification").hide();
            $("#insignia_create_image_btn_add").hide();
            $("#insignia_create_image_btn_remove").show();
        } else {
            $("#insignia_create_image_preview").attr("src", "/assets/app/img/no-image-icon.svg");
            $("#insignia_create_image_notification").show();
            $("#insignia_create_image_btn_remove").hide();
            $("#insignia_create_image_btn_add").show();
        }
    });
    $('#insignia_create_image_btn_add').click(function (evt) {
        evt.preventDefault();
        $("#insignia_create_image").trigger("click");
    });
    $('#insignia_create_image_btn_remove').click(function (evt) {
        evt.preventDefault();
        $("#insignia_create_image").val("").trigger("change");
    });


    $('#insignia_create_save').click(function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        var l_psave = gf_presaveForm($("#insignia_create_form")), l_data, l_formdata = new FormData();
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return;
        }

        let rango_inicio = $('#insignia_rango_inicio').val();
        let rango_fin = $('#insignia_rango_fin').val();

        if (parseInt(rango_inicio) >= parseInt(rango_fin)) {
            $.jGrowl("El rango de inicio NO puede ser mayor al rango fin.", {
                header: 'Validación'
            });
            return;
        }


        let head = {
            codi_lmin: 0,
            nombre: $("#insignia_create_name").val().trim(),
            rango_inicio: parseInt($("#insignia_rango_inicio").val().trim()),
            rango_fin: parseInt($("#insignia_rango_fin").val().trim()),
            user_system: ngon.username,
            ip: ngon.ipaddress
        }
        console.log(JSON.stringify(head));


        l_formdata.append('head', JSON.stringify(head));

        let l_pos_file = 0;
        //Slider IMG
        let l_img = $('#insignia_create_image')[0].files;
        l_pos_file = 0;
        for (var i = 0; i < l_img.length; i++) {
            l_formdata.append("Files[" + l_pos_file + "].File", l_img[i]);
            l_formdata.append("Files[" + l_pos_file + "].Extension", l_img[i].name.split('.').pop());
            l_formdata.append("Files[" + l_pos_file + "].RealName", l_img[i].name);
            console.log(l_img[i].name);
            l_formdata.append("Files[" + l_pos_file + "].WasRemoved", false);
            l_formdata.append("Files[" + l_pos_file + "].WasRetrieved", false);
            l_formdata.append("Files[" + l_pos_file + "].WasChoosed", true);
            l_formdata.append("Files[" + l_pos_file + "].TipoDoc", "IMAGEN_INSIGNIA");
            l_pos_file++;
        }

        
        $.daAlert({
            content: {
                type: "text",
                value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "save",
                    fn: function () {
                        $.ajax({
                            data: l_formdata,
                            url: ngon.UrlApi_insign_saveUpd,
                            headers: {
                                'Authorization': ngon.token_access
                            },
                            type: 'post',
                            dataType: 'json',
                            contentType: false,
                            processData: false,
                            beforeSend: _beforeSend,
                            success: _success
                        });
                        function _beforeSend() {
                            gf_addLoadingTo($("#insignia_create_save"));
                            gf_triggerKeepActive();
                        }
                        function _success(res) {
                            if (res.Status != undefined && res.Status == 200) {
                                $.jGrowl("Se guardo correctamente.", {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                location.href = '/Insignia/Index';
                            } else {
                                $.jGrowl("Error", {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: "Error"
                                });
                                gf_removeLoadingTo($("#insignia_create_save"));
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: "Confirmación"
        });

    });

    $("#insignia_create_name, #insignia_create_image_display, #insignia_rango_fin, #insignia_rango_inicio").data({
        "PresaveRequired": true
    });


    $(".allownumericwithoutdecimal").on("keypress keyup blur paste", function (event) {
        var that = this;
        var rango_fin = $("#insignia_rango_fin").val();
        //paste event 
        if (event.type === "paste") {
            setTimeout(function () {
                $(that).val($(that).val().replace(/[^\d].+/, ""));
                if (parseInt($(that).val()) < 0 || parseInt($(that).val()) > 20) {
                    $(that).val("");
                }
            }, 100);
        } else {

            if (event.which < 48 || event.which > 57) {
                event.preventDefault();
            } else {
                $(this).val($(this).val().replace(/[^\d].+/, ""));
                if (parseInt($(this).val()) < 0 || parseInt($(this).val()) > 20) {
                    $(this).val("");
                }
            }
        }

    });



});