﻿$(document).ready(function () {

    console.log(ngon);
    var arrayCriterio = new Array();
    var arrayCondicion = new Array();
    var controlquery = 0;



    l_dataTable = $("#reglainsginia_index_table").daDataTable({
        "DataTable": {
            "aoColumns": [
                {
                    "className": "text-left"
                },
                {
                    "width": "100px",
                    "className": "text-center"
                },
                {
                    "width": "100px",
                    "className": "text-center"
                },
                {
                    "width": "100px",
                    "className": "text-center"
                },
                {
                    "width": "100px",
                    "className": "text-center"
                },
                {
                    "width": "100px",
                    "className": "text-center"
                }
            ],
            "processing": true,
            "bLengthChange": false,
            "iDisplayLength": 10
        },
        "theme": "devexpress",
        "imgProcessing": "/assets/dalba/img/DXR.gif",
        "search": {
            "btn": { "column": 5 }
        }
    }).Datatable;
    $("#_form_regla").submit(function (evt) {
        evt.preventDefault();
    });

    $("#reglainsginia_index_table tbody").on('click', 'a[data-action=delete]', function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        console.log("hOLAS:  " + $(this).attr("data-id"));
        let codi_crre = $(this).attr("data-id");
        $.daAlert({
            content: {
                type: 'text',
                value: '<div class="text-center p-05">¿Está seguro que desea eliminar?</div>'
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "remove",
                    fn: function (p, e) {
                        var l_success = false;
                        $.ajax({
                            data: JSON.stringify({ Codi_Crre: codi_crre}),
                            url: ngon.UrlApi_insign_deleteRegla,
                            headers: {
                                'Authorization': ngon.token_access
                            },
                            type: 'post',
                            dataType: 'json',
                            contentType: "application/json; charset=utf-8",
                            success: _success,
                            complete: _complete
                        });
                        function _success(res) {
                            if (res.Status != undefined && res.Status == 200) {
                                $.jGrowl("Se eliminó correctamente", {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                l_success = true;
                            } else {
                                $.jGrowl("Error", {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: "Error"
                                });
                            }
                        }
                        function _complete() {
                            if (l_success) {
                                callReglaInsignia();
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: 'Confirmación'
        });
    });

    $("#reglainsginia_index_table tbody").on('click', 'a[data-action=edit]', function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        console.log("hOLAS:  " + $(this).attr("data-id"));
        let codi_crre = $(this).attr("data-id");
        let codi_crit = $(this).attr("data-codi-crit");
        let codi_cond = $(this).attr("data-codi-cond");
        let valor_uno = $(this).attr("data-valor-uno");
        let valor_dos = $(this).attr("data-valor-dos");
        let puntos = $(this).attr("data-puntos");
        clearDataModal();
        $("#modal_regla").modal("show");
        $("#_form_regla").attr("data-id", codi_crre);
        $('#criterio').selectpicker('val', codi_crit);
        $('#condicion').selectpicker('val', codi_cond);
        $('#valor_uno').val(valor_uno);
        $('#valor_dos').val(valor_dos);
        $('#puntos').val(puntos);

    });

    $("#nueva_regla").on('click', function (evt) {
        evt.preventDefault();
        $("#modal_regla").modal("show");
        $("#_form_regla").attr("data-id", "0");
        clearDataModal();
    });

    $("#criterio").on('change', function (evt) {
        evt.preventDefault();

        if ($("#criterio").val() == 2 || $("#criterio").val() == 3) {
            $('#condicion').selectpicker('val', 1);
            $('#condicion').prop('disabled', true);
            $('#condicion').selectpicker('refresh');
            $("button[data-id='condicion']").css('color', 'rgba(0, 0, 0, .87)');

            if ($("#criterio").val() == 2) {
                $("#text-dinami").text("Semana"); 
            }
            if ($("#criterio").val() == 3) {
                $("#text-dinami").text("Nro. ayudas");
            }
        } else {
            $("#text-dinami").text("Valor uno"); 
            $('#condicion').selectpicker('val', null);
            $('#condicion').prop('disabled', false);
            $('#condicion').selectpicker('refresh');
            $("button[data-id='condicion']").removeAttr('style');  
        }
    });

    $("#condicion").on('change', function (evt) {
        evt.preventDefault();
        $('#valor_uno').val("");
        $('#valor_dos').val("");
        if ($("#condicion").val() == 7) {
            $('#valor_dos').prop('disabled', false);
        } else {
            $('#valor_dos').prop('disabled', true);
        }
    });

    $("#save_regla").on('click', function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");

        if ($("#condicion").val() == "7") {
            $("#valor_dos").data({
                "PresaveRequired": true
            });
        } else {
            $("#valor_dos").data({
                "PresaveRequired": false
            });
        }

        var l_psave = gf_presaveForm($("#_form_regla")), l_data, l_formdata = new FormData();
        console.log(l_psave);
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return;
        }



        let head = {
            codi_crre: parseInt($("#_form_regla").attr("data-id")),
            codi_crit: parseInt($("#criterio").val()),
            codi_cond: parseInt($("#condicion").val()),
            valor_uno: $("#valor_uno").val(),
            valor_dos: $("#valor_dos").val(),
            puntos: parseInt($("#puntos").val()),
            user_system: ngon.username,
            ip: ngon.ipaddress
        }

        l_formdata.append('head', JSON.stringify(head));

        $.ajax({
            data: l_formdata,
            url: ngon.UrlApi_insign_saveUpdRegla,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: false,
            processData: false,
            beforeSend: _beforeSend,
            success: _success
        });
        function _beforeSend() {
            gf_addLoadingTo($("#save_regla"));
            gf_triggerKeepActive();
        }
        function _success(res) {
            if (res.Status != undefined && res.Status == 200) {
                $.jGrowl("Se guardo correctamente.", {
                    header: 'Información',
                    type: 'primary'
                });
                callReglaInsignia();
                $("#modal_regla").modal("hide");
                gf_removeLoadingTo($("#save_regla"));
            } else {
                $.jGrowl("Error", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "Error"
                });
                gf_removeLoadingTo($("#save_regla"));
            }
        }


    });

    $("#valor_uno, #puntos, #condicion, #criterio").data({
        "PresaveRequired": true
    });

    function loadDataInitial() {

        $.ajax({
            data: '',
            url: ngon.UrlApi_insign_listCriterio,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    arrayCriterio = res.ListBody;
                    controlquery = controlquery + 1;
                    callReglaInsignia();
                    for (let d = 0; d < arrayCriterio.length; d++) {
                        $("#criterio").append(("<option value='" + arrayCriterio[d].Codi_Crit + "'>" + arrayCriterio[d].Nombre + "</option>"));
                    }
                    $("#criterio").addClass("selectpicker").val(null).selectpicker();
                } else {
                    $.jGrowl("No se pudo cargar los criterios", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo cargar los criterios"
                    });
                    $("#criterio").append("<option disabled='disabled'>Error</option>");
                }
            },
            complete: function () { },
            error: function () {
                $.jGrowl("No se pudo cargar los criterios", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "No se pudo cargar los criterios"
                });
                $("#criterio").append("<option disabled='disabled'>Error</option>");
            }
        });

        $.ajax({
            data: '',
            url: ngon.UrlApi_insign_listCondicion,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    arrayCondicion = res.ListBody;
                    controlquery = controlquery + 1;
                    callReglaInsignia();
                    for (let d = 0; d < arrayCondicion.length; d++) {
                        $("#condicion").append(("<option value='" + arrayCondicion[d].Codi_Cond + "'>" + arrayCondicion[d].Descripcion + "</option>"));
                    }
                    $("#condicion").addClass("selectpicker").val(null).selectpicker();
                } else {
                    $.jGrowl("No se pudo cargar los criterios", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo cargar los criterios"
                    });
                    $("#condicion").append("<option disabled='disabled'>Error</option>");
                }
            },
            complete: function () { },
            error: function () {
                $.jGrowl("No se pudo cargar los criterios", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "No se pudo cargar los criterios"
                });
                $("#condicion").append("<option disabled='disabled'>Error</option>");
            }
        });

    }

    function callReglaInsignia() {
        if (controlquery == 2) {
            $.ajax({
                data: '',
                url: ngon.UrlApi_insign_listRegla,
                headers: {
                    'Authorization': ngon.token_access
                },
                type: 'post',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                beforeSend: function () {

                },
                success: function (res) {
                    if (res.Status != undefined && res.Status == 200) {
                        drawTableReglaInsignia(res.ListBody);
                    } else {
                        $.jGrowl("No se pudo cargar los criterios", {
                            header: 'Error',
                            type: 'danger',
                            clipboard: "No se pudo cargar los criterios"
                        });

                    }
                },
                complete: function () { },
                error: function () {
                    $.jGrowl("No se pudo cargar los criterios", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo cargar los criterios"
                    });
                }
            });
        }
    }

    function drawTableReglaInsignia(listReglaInsignia) {
        let trDOM;
        console.log(listReglaInsignia);
        l_dataTable.clear().draw();
        
        for (let itm in listReglaInsignia) {
            
            let criterio = getCriterio(listReglaInsignia[itm].Codi_Crit);
            let condicion = getCondicion(listReglaInsignia[itm].Codi_Cond);

            trDOM = l_dataTable.row.add([
                criterio,
                condicion,
                listReglaInsignia[itm].Valor_Uno,
                listReglaInsignia[itm].Valor_Dos,
                listReglaInsignia[itm].Puntos,
                `<a href="javascript:void(0)" data-action="edit" 
                            data-id="` + listReglaInsignia[itm].Codi_Crre + `"
                            data-codi-crit="`+ listReglaInsignia[itm].Codi_Crit + `"
                            data-codi-cond="`+ listReglaInsignia[itm].Codi_Cond + `"
                            data-valor-uno="`+ listReglaInsignia[itm].Valor_Uno + `"
                            data-valor-dos="`+ listReglaInsignia[itm].Valor_Dos + `"
                            data-puntos="`+ listReglaInsignia[itm].Puntos + `"
                            class="btn btn-xs btn-raised btn-warning m-0 cls-1810072234" title="Editar" >
                                    <i class="fa fa-edit m-0"></i>
                 </a>
                 <a href="javascript:void(0)" class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" data-action="delete"
                            data-id="`+ listReglaInsignia[itm].Codi_Crre + `"
                            title="Eliminar" >
                             <i class="fa fa-trash m-0"></i>
                 </a>`
            ]).draw(false).node();

        }

    }

    function getCriterio(id) {
        for (let d = 0; d < arrayCriterio.length; d++) {
            if (arrayCriterio[d].Codi_Crit == id) {
                return arrayCriterio[d].Nombre
            }
        }
    }

    function getCondicion(id) {
        for (let d = 0; d < arrayCondicion.length; d++) {
            if (arrayCondicion[d].Codi_Cond == id) {
                return arrayCondicion[d].Descripcion
            }
        }
    }

    function clearDataModal() {
        $('#criterio').selectpicker('val', null);
        $('#condicion').selectpicker('val', null);
        $('#valor_uno').val("");
        $('#valor_dos').val("");
        $('#puntos').val("");
    }

    loadDataInitial();

});