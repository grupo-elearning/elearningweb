﻿"use strict";
$("document").ready(function () {
    (function (p_model) {
        if (p_model.ErrorCode == 4 || p_model.ErrorCode == 5) {
            $('#programming_notclassroom_table').daDataTable({
                'DataTable': {
                    'data': p_model.Details,
                    'aoColumns': [
                        {
                            'data': null,
                            'className': 'text-center',
                            'mRender': function (o) {
                                return '<span class="d-none">' + o[3] + '</span>' + o[0];
                            }
                        },
                        {
                            'data': 1,
                            'className': 'text-center'
                        },
                        {
                            'data': 2,
                            'className': 'text-center'
                        }
                    ],
                    'order': [[0, 'asc']],
                    'processing': true,
                    'bLengthChange': false,
                    'iDisplayLength': 10
                },
                'theme': 'devexpress',
                'imgProcessing': '/assets/dalba/img/DXR.gif',
                'search': {
                    'btn': null
                }
            });
        }
        if (p_model.FromCarrierLogin) {
            var l_time_close_session = parseInt(gf_Helpers_GetParameterByKey('DEMORA_CERRAR_SESION_TRANSPORTISTA_NO_AULA'));
            setTimeout(function () {
                location.href = '/Account/Logout';
            }, (l_time_close_session * 1000));
        }
    }(gf_getClone(ngon.Model)));
});