﻿"use strict";
$("document").ready(function () {
    var l_alert;
    $("#programming_index_table tbody").on('click', 'a[data-action=delete]', function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        var l_datatable = $("#programming_index_table").DataTable();
        var l_data = l_datatable.row($(this).closest('tr')).data();
        $.daAlert({
            content: {
                type: 'text',
                value: '<div class="text-center p-05">¿Está seguro que desea eliminar?</div>'
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "remove",
                    fn: function (p, e) {
                        var l_success = false;
                        $.ajax({
                            data: JSON.stringify({ Id: l_data[0] }),
                            url: '/Programming/Delete',
                            type: 'post',
                            dataType: 'json',
                            contentType: "application/json; charset=utf-8",
                            success: _success,
                            complete: _complete
                        });
                        function _success(r) {
                            if (r.status == 1000) {
                                $.jGrowl(r.response, {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                l_success = true;
                            } else {
                                $.jGrowl(r.response, {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: r.exception
                                });
                            }
                        }
                        function _complete() {
                            if (l_success) {
                                fn_datatable();
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: 'Confirmación'
        });
    });
    $("#programming_index_table tbody").on('click', 'input[data-action=activate]', function () {
        var l_status = $(this).is(':checked'), l_datatable = $("#programming_index_table").DataTable(), l_data = l_datatable.row($(this).closest('tr')).data();
        $.ajax({
            data: JSON.stringify({ 'Id': l_data[0], 'Status': l_status ? 'A' : 'I' }),
            url: '/Programming/UpdateStatus',
            type: 'post',
            async: false,
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            success: _success
        });
        function _success(r) {
            if (parseInt(r.status) == 1000) {
                $.jGrowl(r.response, {
                    header: 'Información',
                    type: 'primary'
                });
            } else {
                $.jGrowl(r.response, {
                    header: 'Error',
                    type: 'danger',
                    clipboard: r.exception
                });
            }
        }
    });
    $("#programming_index_migrate").click(function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        l_alert = $.daAlert({
            content: {
                type: 'text',
                value: '<div class="text-center p-05">¿Está seguro que desea migrar?</div>'
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "custom",
                    text: "Migrar",
                    icon: "glyphicon glyphicon-duplicate",
                    className: "btn btn-raised btn-info",
                    fn: function (p, e) {
                        $.ajax({
                            url: '/Programming/MigrateToCapacitacion',
                            type: 'post',
                            dataType: 'json',
                            contentType: "application/json; charset=utf-8",
                            beforeSend: _beforeSend,
                            success: _success,
                            complete: _complete
                        });
                        function _beforeSend() {
                            gf_addLoadingTo(e);
                        }
                        function _success(r) {
                            if (r.status == 1000) {
                                $.jGrowl(r.response, {
                                    header: 'Información',
                                    type: 'primary'
                                });
                            } else {
                                $.jGrowl(r.response, {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: r.exception
                                });
                            }
                        }
                        function _complete() {
                            l_alert.close();
                        }
                        return false;
                    }
                }
            ],
            maximizable: false,
            title: 'Confirmación'
        });
    });
    //fn
    function fn_datatable() {
        $("#programming_index_table").daDataTable({
            "DataTable": {
                "ajax": {
                    "url": '/Programming/GetList',
                    "type": "post"
                },
                "aoColumns": [
                    {
                        "data": 9,
                        "className": "text-left"
                    },
                    {
                        "data": 1,
                        "className": "text-left"
                    },
                    {
                        "data": 2,
                        "className": "text-left"
                    },
                    {
                        "data": 3,
                        "className": "text-right"
                    },
                    {
                        "data": 4,
                        "className": "text-left"
                    },
                    {
                        "data": null,
                        "className": "text-center",
                        "mRender": function (o) {
                            return "<span class='d-none'>" + o[6] + "</span>" + o[5];
                        }
                    },
                    {
                        "data": null,
                        "className": "text-center",
                        "mRender": function (o) {
                            return "<span class='d-none'>" + o[8] + "</span>" + o[7];
                        }
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center",
                        "width": "50px",
                        "mRender": function (o) {
                            return `<div class="togglebutton">
                                        <label class="m-0">
                                            <input type="checkbox" data-action="activate" ` + (o[10] == 'A' ? `checked` : ``) + ` /><span class="toggle m-0"></span>
                                        </label>
                                    </div>`;
                        }
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center",
                        "width": "50px",
                        "mRender": function (o) {
                            return `<a href="/Programming/Edit/` + gf_encode(o[0]) + `" class="btn btn-xs btn-raised btn-warning m-0 cls-1810072234" title="Editar" >
                                        <i class="fa fa-edit m-0"></i>
                                    </a>
                                    <a href="javascript:void(0)" class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" data-action="delete" title="Eliminar" >
                                        <i class="fa fa-trash m-0"></i>
                                    </a>`;
                        }
                    }
                ],
                "order": [[0, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 10
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": { "column": 8 },
                "noSearch": [7]
            }
        });
    }
    //init
    fn_datatable();
});