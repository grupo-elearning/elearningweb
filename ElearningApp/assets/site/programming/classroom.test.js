﻿(function (p_model) {
    var l_current_index, l_answer_to_complete, l_rules, l_current_slide, l_countdown, l_in_the_last, l_in_the_first;
    $.material.init({
        "radioElements": "#programming_test input[type=radio]",
        "checkboxElements": "#programming_test input[type=checkbox]"
    });
    $("#programming_test div[data-object=slide] p[data-object=question]").on("change", "select", function () {
        var l_prex = $(this).attr('data-prex'), l_values = [];
        $(this).closest('p[data-object=question]').find('select').each(function () {
            if (gf_isValue($(this).val())) {
                l_values.push($(this).val());
            }
            $(this).data('lastValue', $(this).val());
            $(this).find('option').remove();
        });
        $(this).closest('p[data-object=question]').find('select').each(function () {
            var l_options = l_answer_to_complete[l_prex], l_this = $(this), l_lastValue = $(this).data('lastValue');
            for (var i in l_options) {
                if (gf_inArray(l_options[i].value, l_values) && l_options[i].value != '0') {
                    if (l_options[i].value == l_lastValue) {
                        l_this.append('<option value="' + l_options[i].value + '">' + l_options[i].text + '</option>');
                    }
                } else {
                    l_this.append('<option value="' + l_options[i].value + '">' + l_options[i].text + '</option>');
                }
            }
            l_this.val(l_lastValue);
        });
    });
    $("#programming_test_finish").click(function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        $.daAlert({
            content: {
                type: 'text',
                value: '<div class="text-center p-05">¿Está seguro que desea Terminar el Examen?</div>'
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "accept",
                    fn: function (p, e) {
                        fn_finish();
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: 'Confirmación'
        });
    });
    $("#programming_test_finish_hidden").click(function (evt) {
        evt.preventDefault();
        fn_finish();
    });
    $("#programming_test_save_current_time").click(function (evt) {
        evt.preventDefault();
        fn_save_current_time();
    });
    //slider
    $("#programming_test a[data-object=prev]").click(function (evt) {
        evt.preventDefault();
        if (l_rules.prev() && !l_in_the_first) {
            fn_show_slide(l_current_index - 1);
        }
    });
    $("#programming_test a[data-object=next]").click(function (evt) {
        evt.preventDefault();
        if (l_rules.next() && !l_in_the_last) {
            fn_show_slide(l_current_index + 1);
        }
    });
    //$("#programming_test span[data-object=dot]").click(function (evt) {
    //    evt.preventDefault();
    //    if (l_rules.dot()) {
    //        fn_show_slide(parseInt($(this).attr("data-dot")));
    //    }
    //});
    //for complete
    (function () {
        l_answer_to_complete = {};
        for (var i in p_model.Questions) {
            if (p_model.Questions[i][1] == "completar") {
                var l_options = [{ 'value': 0, 'text': '' }], l_html = p_model.Questions[i][3], l_select = '<select class="cls-1901012048" data-object="answer" data-prex="' + p_model.Questions[i][0] + '">';
                l_select += '<option value="0"></option>';
                for (var j in p_model.Questions[i][4]) {
                    l_options.push({ 'value': p_model.Questions[i][4][j][0], 'text': p_model.Questions[i][4][j][1] });
                    l_select += '<option value="' + p_model.Questions[i][4][j][0] + '">' + p_model.Questions[i][4][j][1] + '</option>';
                }
                l_answer_to_complete[p_model.Questions[i][0]] = l_options;
                l_select += '</select>';
                l_html = gf_replaceAll(l_html, '{select_of_answer}', l_select);
                $("#programming_test div[data-object=slide][data-prex='" + gf_encode(p_model.Questions[i][0]) + "'] p[data-object=question]").html(l_html);
                $("#programming_test div[data-object=slide][data-prex='" + gf_encode(p_model.Questions[i][0]) + "'] p[data-object=question] select").val(0);
            }
        }
    }());
    //fn
    function fn_save_log() {
        $.ajax({
            data: JSON.stringify({ 'register': window.cHJvZ3JhbW1pbmdfY2xhc3Nyb29tX3JlZ2lzdGVy, 'item': window.cHJvZ3JhbW1pbmdfY2xhc3Nyb29tX2xvZw }),
            url: '/Programming/SaveClassroomLog',
            type: 'post',
            //async: false,
            dataType: 'json',
            contentType: "application/json; charset=utf-8"
        });
    }
    function fn_save_current_time() {
        $.ajax({
            data: JSON.stringify({
                Id: p_model.AttemptId,
                Seconds: l_countdown.getTime() + p_model.CurrentTime
            }),
            url: '/Programming/SaveAttemptCurrentTime',
            type: 'post',
            //async: false,
            dataType: 'json',
            contentType: "application/json; charset=utf-8"
        });
    }
    function fn_finish() {
        gf_addLoadingTo($("#programming_test_finish"));
        fn_save_prex();
        var l_param = {
            Id: p_model.AttemptId,
            Seconds: 0,
            Time: p_model.Time
        };
        if (p_model.Time == 'TE') {
            l_param.Seconds = l_countdown.getTime() + p_model.CurrentTime;
        }
        $.ajax({
            data: JSON.stringify(l_param),
            url: '/Programming/SaveAttempt',
            type: 'post',
            async: false,
            dataType: 'json',
            contentType: "application/json; charset=utf-8"
        });
        if (p_model.Time == 'TE') {
            l_countdown.destroy();
            $("#programming_test_countdown_content").remove();
        }
        //show message
        $("#programming_classroom_content").load("/Programming/AttemptFinalized/" + gf_encode(p_model.AttemptId), function (response, status, xhr) {
            if (status == "error") {
                var msg = "Sorry but there was an error: ";
                $("#programming_classroom_content").html(msg + xhr.status + " " + xhr.statusText);
            }
        });
    }
    function fn_show_slide(n) {
        fn_off_events();
        fn_save_prex();
        var l_slides = $("#programming_test div[data-object=slide]").length, l_index = n > l_slides ? 1 : n < 1 ? l_slides : n;
        $("#programming_test div[data-object=slide]").hide();
        $("#programming_test div[data-object=slide]").eq(l_index - 1).show();
        $("#programming_test span[data-object=dot]").removeClass('active');
        $("#programming_test span[data-object=dot]").eq(l_index - 1).addClass('active');
        l_current_index = l_index;
        l_current_slide = $("#programming_test div[data-object=slide]").eq(l_index - 1);
        fn_get_prex();
        //in the first?
        l_in_the_first = l_index == 1;
        if (l_in_the_first) {
            $("#programming_test a[data-object=prev]").hide();
        } else {
            $("#programming_test a[data-object=prev]").show();
        }
        //in the last?
        l_in_the_last = l_index == l_slides;
        if (l_in_the_last) {
            $("#programming_test a[data-object=next]").hide();
            $("#programming_test_finish").show();
        } else {
            $("#programming_test a[data-object=next]").show();
            $("#programming_test_finish").hide();
        }
        //response
        l_rules.afterResponse();
    }
    function fn_save_prex() {
        if (!l_current_slide) {
            return;
        }
        var l_type = l_current_slide.attr('data-type');
        var l_param = {
            Id: gf_decode(l_current_slide.attr('data-prex')),
            Seconds: 0,
            Answered: 'N',
            Time: p_model.Time,
            AnswerId: [],
            AnswerOrder: []
        };
        if (p_model.Time == 'TE' || p_model.Time == 'TP') {
            l_param.Seconds = l_countdown.getTime() + (p_model.Time == 'TE' ? p_model.CurrentTime : 0);
        }
        if (l_type == 'radio' || l_type == 'boolean' || l_type == 'checkbox') {
            l_current_slide.find('input[data-object=answer]:checked').each(function () {
                l_param.AnswerId.push(gf_decode($(this).val()));
                l_param.AnswerOrder.push(0);
            });
            l_param.Answered = l_current_slide.find('input[data-object=answer]:checked').length == 0 ? 'N' : 'S';
        } else if (l_type == 'completar') {
            var l_all_selected = true;
            l_current_slide.find('select[data-object=answer]').each(function (idx) {
                var l_v = $(this).val();
                if (gf_isValue(l_v) && l_v != '0') {
                    l_param.AnswerId.push(l_v);
                    l_param.AnswerOrder.push(idx + 1);
                } else {
                    l_all_selected = false;
                }
            });
            l_param.Answered = l_all_selected ? 'S' : 'N';
        }
        if (l_param.AnswerId.length == 0) {
            l_param.AnswerId.push(-1);
            l_param.AnswerOrder.push(-1);
        }
        $.ajax({
            data: JSON.stringify(l_param),
            url: '/Programming/SaveAttemptQuestion',
            type: 'post',
            async: false,
            dataType: 'json',
            contentType: "application/json; charset=utf-8"
        });
        if (p_model.Time == 'TP') {
            l_countdown.destroy();
            if (l_in_the_last) {
                $("#programming_test_countdown_content").remove();
            }
        }
    }
    function fn_get_prex() {
        var l_data = null;
        $.ajax({
            data: JSON.stringify({ id: gf_decode(l_current_slide.attr('data-prex')) }),
            url: '/Programming/GetAttemptQuestion',
            type: 'post',
            dataType: 'json',
            async: false,
            contentType: "application/json; charset=utf-8",
            success: _success
        });
        function _success(r) {
            if (r.status == 1) {
                l_data = r.response;
            } else {
                $.jGrowl(r.response, {
                    header: 'Error',
                    type: 'danger',
                    clipboard: r.exception
                });
            }
        }
        if (l_data) {
            var l_answers = l_data[1], l_type = l_current_slide.attr('data-type');
            if (l_type == 'radio' || l_type == 'boolean' || l_type == 'checkbox') {
                l_current_slide.find('input[data-object=answer]').each(function () {
                    var l_prep = gf_decode($(this).val());
                    for (var i in l_answers) {
                        if (l_answers[i][0] == l_prep) {
                            $(this).prop('checked', true);
                            break;
                        }
                    }
                });
            } else if (l_type == 'completar') {
                l_current_slide.find('select[data-object=answer]').each(function (idx) {
                    for (var i in l_answers) {
                        if (l_answers[i][1] == idx + 1) {
                            $(this).val(l_answers[i][0]).trigger('change');
                        }
                    }
                });
            }
        }
        if (p_model.Time == 'TP') {
            fn_init_countdown_to_prex();
        }
    }
    function fn_init_countdown_to_prex() {
        var l_seconds = (p_model.TimeLimit * 60);
        $('#programming_test_countdown_title').text('Para terminar la pregunta');
        l_countdown = $('#programming_test_countdown').dsCountDown({
            startDate: moment().toDate(),
            endDate: moment().add(l_seconds, 'seconds').toDate(),
            elemSelDays: false,
            theme: 'black',
            titleHours: 'Hora',
            titleMinutes: 'Minutos',
            titleSeconds: 'Segundos',
            onFinish: function (e) {
                if (l_in_the_last) {
                    fn_finish();
                } else {
                    fn_show_slide(l_current_index + 1);
                }
            }
        });
    }
    function fn_off_events() {
        if (p_model.Proceed == 'A' && l_current_slide) {
            l_current_slide.off('change', 'input[data-object=answer]');
            l_current_slide.off('change', 'select[data-object=answer]');
        }
    }
    //init
    (function () {
        l_rules = {
            next: null,
            prev: null,
            dot: null,
            afterResponse: null
        };
        //for TestType
        if (p_model.TestType == 'R') {//El usuario debera responder la pregunta
            l_rules.next = function () {
                var l_type = l_current_slide.attr('data-type');
                if (l_type == 'radio' || l_type == 'boolean') {
                    var l_c = l_current_slide.find('input[data-object=answer]:checked').length;
                    if (l_c == 1) {
                        return true;
                    } else {
                        $.jGrowl("Debe seleccionar una respuesta", {
                            header: 'Validación'
                        });
                        return false;
                    }
                } else if (l_type == 'checkbox') {
                    var l_c = l_current_slide.find('input[data-object=answer]:checked').length;
                    if (l_c >= 1) {
                        return true;
                    } else {
                        $.jGrowl("Debe seleccionar por lo menos una respuesta", {
                            header: 'Validación'
                        });
                        return false;
                    }
                } else if (l_type == 'completar') {
                    var l_c = true;
                    l_current_slide.find('select[data-object=answer]').each(function () {
                        var l_v = $(this).val();
                        if (!gf_isValue(l_v) || l_v == '0') {
                            l_c = false;
                            return false;
                        }
                    });
                    if (l_c) {
                        return true;
                    } else {
                        $.jGrowl("Debe completar todos los espacios en blanco", {
                            header: 'Validación'
                        });
                        return false;
                    }
                }
            }
            l_rules.prev = function () { return true; }
            l_rules.dot = function () { return false; }
            l_rules.afterResponse = function () { }
            //hide dot
            //$("#programming_test span[data-object=dot]").hide();
        } else if (p_model.TestType == 'S') {//El usuario puede saltar las preguntas
            l_rules.next = function () { return true; }
            l_rules.prev = function () { return true; }
            l_rules.dot = function () { return true; }
            l_rules.afterResponse = function () { }
        } else if (p_model.TestType == 'T') {//El usuario puede ver las respuestas correctas luego de responder una pregunta
            l_rules.next = function () { return true; }
            l_rules.prev = function () { return true; }
            l_rules.dot = function () { return true; }
            l_rules.afterResponse = function () { }
        }
        //for Proceed
        if (p_model.Proceed == 'A') {//Automático: proceder directamente después de seleccionar la respuesta
            l_rules.afterResponse = function () {
                var l_type = l_current_slide.attr('data-type');
                if (l_type == 'radio' || l_type == 'boolean') {
                    l_current_slide.on('change', 'input[data-object=answer]', function () {
                        if (l_in_the_last) {
                            fn_finish();
                        } else {
                            fn_show_slide(l_current_index + 1);
                        }
                    });
                } else if (l_type == 'completar') {
                    l_current_slide.on('change', 'select[data-object=answer]', function () {
                        var l_c = true;
                        l_current_slide.find('select[data-object=answer]').each(function () {
                            var l_v = $(this).val();
                            if (!gf_isValue(l_v) || l_v == '0') {
                                l_c = false;
                                return false;
                            }
                        });
                        if (l_c) {
                            if (l_in_the_last) {
                                fn_finish();
                            } else {
                                fn_show_slide(l_current_index + 1);
                            }
                        }
                    });
                }
            }
        } else if (p_model.Proceed == 'M') {//Manual: seleccionar la respuesta y luego hacer clic en siguiente para proceder
            l_rules.afterResponse = function () { }
        }
        //for Time
        if (p_model.Time == 'NL') {//No hay tiempo límite
            window.onbeforeunload = function () {
                fn_save_log();
                return null;
            }
        } else if (p_model.Time == 'TE') {//Tiempo límite del examen
            var l_seconds = (p_model.TimeLimit * 60) - p_model.CurrentTime;
            $('#programming_test_countdown_title').text('Para terminar el examen');
            l_countdown = $('#programming_test_countdown').dsCountDown({
                startDate: moment().toDate(),
                endDate: moment().add(l_seconds, 'seconds').toDate(),
                elemSelDays: false,
                theme: 'black',
                titleHours: 'Hora',
                titleMinutes: 'Minutos',
                titleSeconds: 'Segundos',
                onFinish: function (e) {
                    fn_finish();
                }
            });
            //onbeforeunload
            window.onbeforeunload = function () {
                fn_save_current_time();
                fn_save_log();
                return null;
            }
        } else if (p_model.Time == 'TP') {//Tiempo límite por pregunta
            window.onbeforeunload = function () {
                fn_save_log();
                return null;
            }
        }
    }());
    (function () {
        var l_index = 1;
        // get current index
        for (var i = 0; i < p_model.Questions.length; i++) {
            if (p_model.Questions[i][2] == "S") {
                l_index = i + 1;
                break;
            }
        }
        fn_show_slide(l_index);
    }());
}(gf_getClone(ngon.Model)));