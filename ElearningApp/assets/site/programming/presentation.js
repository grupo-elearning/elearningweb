﻿"use strict";
$("document").ready(function () {
    (function (p_model) {
        var l_alert_save_massive, l_tcon_texto, l_tcon_video, l_tcon_doc, l_contents, l_in_siderperu;
        $("#programming_presentation_register").click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_param = {
                'ProgrammingId': p_model.ProgrammingId,
                'Schedule': 'Fecha Inicio: ' + p_model.Programming.sFromDate + ' Fecha Fin: ' + p_model.Programming.sToDate + ' Hora Inicio: ' + p_model.Programming.FromHour + ' Hora Fin: ' + p_model.Programming.ToHour
            };
            $.daAlert({
                content: {
                    type: "text",
                    value: "<div class='text-center p-05'>¿Está seguro que desea inscribirse?</div>"
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "accept",
                        fn: function (p, e) {
                            $.ajax({
                                data: JSON.stringify(l_param),
                                url: '/Programming/Register',
                                type: 'post',
                                dataType: 'json',
                                contentType: "application/json; charset=utf-8",
                                beforeSend: _beforeSend,
                                success: _success
                            });
                            function _beforeSend() {
                                gf_addLoadingTo(e);
                            }
                            function _success(r) {
                                if (parseInt(r.status) == 1000) {
                                    $.jGrowl(r.response, {
                                        header: 'Información',
                                        type: 'primary'
                                    });
                                    location.href = '/Programming/Presentation/' + gf_encode(p_model.ProgrammingId);
                                } else {
                                    $.jGrowl(r.response, {
                                        header: 'Error',
                                        type: 'danger',
                                        clipboard: r.exception
                                    });
                                }
                            }
                            return true;
                        }
                    }
                ],
                maximizable: false,
                title: "Confirmación"
            });
        });
        $("#programming_presentation_registermassive").click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            l_alert_save_massive = $.daAlert({
                content: {
                    type: "ajax",
                    value: {
                        url: "/Programming/IURegisterMassive?programming=" + gf_encode(p_model.ProgrammingId),
                        type: 'get'
                    }
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "save",
                        fn: fn_save_massive
                    }
                ],
                title: 'Inscribir Empleados',
                maxWidth: "700px"
            });
        });
        $("#programming_presentation_accordion button[data-object=content]").click(function (evt) {
            evt.preventDefault();
            if (p_model.Programming.ShowContent != 'S') {
                return;
            }
            var l_content = $(this).attr("data-content"), l_item = l_contents[l_content], l_html;
            if (l_item.tcon == l_tcon_texto) {
                l_html = `<div class="p-1">` + l_item.txt + `</div>`;
            } else if (l_item.tcon == l_tcon_video) {
                if (l_item.ndc == '') {
                    if (l_in_siderperu) {
                        l_html = `<div class="p-1 cls-1901300059" style="height: 100%;">
                                    <iframe src="` + l_item.urlsc + `?autoplay=false&amp;showinfo=true" class="cls-1902010351 cls-1904040948" style="height: 100%;" allowfullscreen></iframe>
                                <div>`;
                    } else {
                        l_html = `<div class="p-1 cls-1901300059">
                                    <div id="player` + l_content + `" class="js-player" data-plyr-provider="youtube" data-plyr-embed-id="` + l_item.urlyt + `"></div>
                                <div>`;
                    }
                } else {
                    l_html = `<div class="p-1 cls-1901300059">
                                    <video id="player`+ l_content + `" poster="/assets/app/img/elearning.jpg" playsinline controls>
                                        <source src="` + p_model.StorageUrl + l_item.ndc + `" type="video/mp4">
                                    </video>
                                </div>`;
                }
            } else if (l_item.tcon == l_tcon_doc) {
                if (gf_isImage(l_item.ndc)) {
                    l_html = `<div class="position-relative h-100 mh-100">
                                <img src="` + p_model.StorageUrl + l_item.ndc + `" class="mh-100 mw-100 app-center" />
                            </div>`;
                } else {
                    l_html = `<iframe src="https://docs.google.com/gview?url=` + p_model.StorageUrl + l_item.ndc + `&embedded=true" class="cls-1902010351" style="width:100%; height:100% !important;"></iframe>`;
                }
            } else {
                l_html = `<div class="p-1 text-center">Nada que mostrar</div>`;
            }
            $.daAlert({
                content: {
                    type: "text",
                    value: l_html
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    }
                ],
                title: l_item.ttl,
                maximizable: true,
                movable: true,
                resizable: true,
                startMaximized: true,
                onshow: function () {
                    setTimeout(function () {
                        if ($("#player" + l_content).length != 0) {
                            new Plyr('#player' + l_content);
                        }
                    }, 1000);
                }
            });
        });
        $('#programming_presentation_accordion button[data-object=no-content]').click(function (evt) {
            evt.preventDefault();
            $.jGrowl('Los Contenidos Interactivos no tienen Vista Previa', {
                header: 'Información'
            });
        });
        function fn_save_massive(p, e) {
            var l_return = false, l_row_data, l_dt_employee = $('#programming_iuregistermassive_table').DataTable();
            var l_param = {
                "ProgrammingId": p_model.ProgrammingId,
                "RgRecordId": [],
                "RgPersonId": [],
                'ScheduleDate': 'Fecha Inicio: ' + p_model.Programming.sFromDate + ' Fecha Fin: ' + p_model.Programming.sToDate,
                'ScheduleTime': 'Hora Inicio: ' + p_model.Programming.FromHour + ' Hora Fin: ' + p_model.Programming.ToHour
            };
            l_dt_employee.rows().nodes().to$().find(':checkbox:checked').each(function () {
                l_row_data = l_dt_employee.row($(this).closest("tr")).data();
                l_param.RgRecordId.push(l_row_data[0]);
                l_param.RgPersonId.push(l_row_data[1]);
            });
            if (l_param.RgRecordId.length == 0) {
                l_param.RgRecordId.push(-1);
                l_param.RgPersonId.push(-1);
            }
            $.ajax({
                data: JSON.stringify(l_param),
                url: '/Programming/RegisterMassive',
                type: 'post',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                beforeSend: _beforeSend,
                success: _success,
                complete: _complete
            });
            function _beforeSend() {
                gf_addLoadingTo(e);
            }
            function _success(r) {
                if (parseInt(r.status) == 1000) {
                    $.jGrowl(r.response, {
                        header: 'Información',
                        type: 'primary'
                    });
                    l_return = true;
                } else {
                    $.jGrowl(r.response, {
                        header: 'Error',
                        type: 'danger',
                        clipboard: r.exception
                    });
                    l_return = false;
                }
            }
            function _complete() {
                if (l_return) {
                    l_alert_save_massive.close();
                } else {
                    gf_removeLoadingTo(e);
                }
            }
            return false;
        }
        //init
        (function () {
            if (p_model.Initiated) {
                $.jGrowl(p_model.InitiatedMessage, {
                    header: 'Información',
                    type: 'info',
                    sound: false
                });
            }
            if (!p_model.Register) {
                $.jGrowl(p_model.NoRegisterMessage, {
                    header: 'Advertencia',
                    type: 'warning',
                    sound: false
                });
            }
            if (p_model.Programming.ShowContent == 'S') {
                l_tcon_texto = gf_Helpers_GetParameterByKey('CODI_TCON_TEXTO');
                l_tcon_video = gf_Helpers_GetParameterByKey('CODI_TCON_VIDEO');
                l_tcon_doc = gf_Helpers_GetParameterByKey('CODI_TCON_DOCUMENTO');
                l_in_siderperu = gf_Helpers_InSiderperu();
                l_contents = {};
                //get contents
                for (var i in p_model.Course.Themes) {
                    for (var j in p_model.Course.Themes[i][2]) {
                        l_contents[p_model.Course.Themes[i][2][j][0]] = {
                            tcon: p_model.Course.Themes[i][2][j][1],
                            ttl: p_model.Course.Themes[i][2][j][2],
                            urlyt: p_model.Course.Themes[i][2][j][3],
                            urlsc: p_model.Course.Themes[i][2][j][8],
                            txt: p_model.Course.Themes[i][2][j][4],
                            ndc: p_model.Course.Themes[i][2][j][5],
                            sec: p_model.Course.Themes[i][2][j][6]
                        };
                    }
                }
            }
        }());
    }(gf_getClone(ngon.Model)));
});