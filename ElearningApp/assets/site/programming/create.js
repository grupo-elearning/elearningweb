﻿"use strict";
$("document").ready(function () {
    (function (p_model) {
        var l_dataTable_person, l_dataTable_area, l_events = [], l_mode_elea, l_mode_inper;
        //plugins
        $('#programming_create_registration_limit').datepicker();
        $('#programming_create_from_date').datepicker().on('changeDate', function (e) {
            $('#programming_create_to_date').datepicker('setStartDate', e.date);
        });
        $('#programming_create_to_date').datepicker().on('changeDate', function (e) {
            $('#programming_create_from_date').datepicker('setEndDate', e.date);
        });
        $("#programming_create_from_hour, #programming_create_to_hour, #programming_create_pnl_ctrl_from_hour, #programming_create_pnl_ctrl_to_hour").inputmask({
            alias: "datetime",
            inputFormat: "HH:MM",
            placeholder: "__:__"
        });
        l_dataTable_person = $("#programming_create_table_person").daDataTable({
            "DataTable": {
                "aoColumns": [
                    {
                        "data": 2,
                        "className": "text-left"
                    },
                    {
                        "data": 3,
                        "className": "text-center"
                    },
                    {
                        "data": 4,
                        "className": "text-left"
                    },
                    {
                        "data": 6,
                        "className": "text-center"
                    },
                    {
                        "data": 5,
                        "className": "text-center"
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center",
                        "width": "50px",
                        "mRender": function (o) {
                            return `<a href="javascript:void(0)" class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" data-action="delete" title="Eliminar" >
                                    <i class="fa fa-trash m-0"></i>
                                </a>`;
                        }
                    }
                ],
                "order": [[0, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 10
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": { "column": 5 }
            }
        }).Datatable;
        l_dataTable_area = $("#programming_create_table_area").daDataTable({
            "DataTable": {
                "aoColumns": [
                    {
                        "data": 2,
                        "className": "text-left"
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center",
                        "width": "100px",
                        "mRender": function (o) {
                            return `<a href="javascript:void(0)" class="btn btn-xs btn-raised btn-info m-0 cls-1810072234" data-action="provider" title="Ver Proveedores" >
                                    <i class="fa fa-eye m-0"></i>
                                </a>`;
                        }
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "className": "text-center",
                        "width": "50px",
                        "mRender": function (o) {
                            return `<a href="javascript:void(0)" class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" data-action="delete" title="Eliminar" >
                                    <i class="fa fa-trash m-0"></i>
                                </a>`;
                        }
                    }
                ],
                "order": [[0, 'asc']],
                "processing": true,
                "bLengthChange": false,
                "iDisplayLength": 10
            },
            "theme": "devexpress",
            "imgProcessing": "/assets/dalba/img/DXR.gif",
            "search": {
                "btn": { "column": 2 },
                "noSearch": [1]
            }
        }).Datatable;
        //events
        $("#programming_create_general_data_form input[type=radio][name=programmingtype]").change(function () {
            var l_type = $("#programming_create_general_data_form input[type=radio][name=programmingtype]:checked").val();
            if (l_type == "H") {
                $(".programming_create_section_mode_in_person_c").hide();
                $(".programming_create_section_mode_in_person_h").show();
                $("#programming_create_type_description").text("Seleccione un rango de fechas y horas");
                $("#programming_create_duration").val('').data("PresaveRequired", false);
                $("#programming_create_interval").val('').data("PresaveRequired", false);
                //update 'Tipo de Acceso'
                if ($("#programming_create_access_type option[data-value=V]").length == 0) {
                    $("#programming_create_access_type").append('<option value="V" data-value="V">Privado</option>');
                    $("#programming_create_access_type").selectpicker('refresh');
                }
            } else if (l_type == "C") {
                $(".programming_create_section_mode_in_person_h").hide();
                $(".programming_create_section_mode_in_person_c").show();
                $("#programming_create_type_description").text("Seleccione un rango de fechas, horas, duración e intervalo");
                $("#programming_create_duration").data("PresaveRequired", true);
                $("#programming_create_interval").data("PresaveRequired", true);
                //update 'Tipo de Acceso'
                if ($("#programming_create_access_type").val() == 'V') {
                    $("#programming_create_access_type").val('P');
                }
                if ($("#programming_create_access_type option[data-value=V]").length != 0) {
                    $("#programming_create_access_type option[data-value=V]").remove();
                    $("#programming_create_access_type").selectpicker('refresh');
                    $("#programming_create_access_type").trigger('change');
                }
            }
        });
        $("#programming_create_calendar").on("click", "div[data-action=delete-event]", function (evt) {
            evt.preventDefault();
            var l_id = $(this).attr("data-id");
            $('#programming_create_calendar').fullCalendar('removeEvents', [l_id]);
        });
        $("#programming_create_course").change(function () {
            var l_c = $(this).val();
            if (gf_isValue(l_c)) {
                gf_Helpers_TestCourseDropDown({
                    $: $("#programming_create_test"),
                    param: { id: l_c },
                    callback: function (o) {
                        o.val(null);
                        o.selectpicker('refresh');
                        o.trigger("change");
                    }
                });
            }
        });
        $("#programming_create_has_test").change(function () {
            if ($(this).is(":checked")) {
                $("#programming_create_test")
                    .val($("#programming_create_test").data('lastValue'))
                    .prop("disabled", false)
                    .data("PresaveRequired", true);
                $("#programming_create_attempts")
                    .val($("#programming_create_attempts").data('lastValue'))
                    .prop("disabled", false)
                    .data("PresaveRequired", true);
                $("#programming_create_test").trigger("change");
            } else {
                $("#programming_create_test")
                    .prop("disabled", true)
                    .data({
                        lastValue: $("#programming_create_test").val(),
                        PresaveRequired: false
                    })
                    .val(null);
                $("#programming_create_attempts")
                    .prop("disabled", true)
                    .data({
                        lastValue: $("#programming_create_attempts").val(),
                        PresaveRequired: false
                    })
                    .val(null);
                $("#programming_create_preview_test").prop("disabled", true);
            }
            $("#programming_create_test").selectpicker('refresh');
        });
        $("#programming_create_table_person").on("click", "a[data-action=delete]", function (evt) {
            evt.preventDefault();
            l_dataTable_person.row($(this).closest("tr")).remove().draw(false);
        });
        $("#programming_create_table_area").on("click", "a[data-action=delete]", function (evt) {
            evt.preventDefault();
            l_dataTable_area.row($(this).closest("tr")).remove().draw(false);
        });
        $("#programming_create_table_area").on("click", "a[data-action=provider]", function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_data = l_dataTable_area.row($(this).closest("tr")).data();
            $.daAlert({
                content: {
                    type: "ajax",
                    value: {
                        url: "/Programming/Provider",
                        data: {
                            'AreaId': l_data[1]
                        }
                    }
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    }
                ],
                maximizable: false,
                title: "Proveedores",
                maxWidth: "700px"
            });
        });
        $("#programming_create_test").change(function () {
            var l_random = $(this).find("option:selected").attr("data-random") == 'S';
            $("#programming_create_preview_test").prop("disabled", $(this).find("option:selected").length != 0 ? l_random : true);
        });
        $("#programming_create_has_instructor").change(function () {
            if ($(this).is(":checked")) {
                $("#programming_create_instructor")
                    .data("PresaveRequired", true)
                    .addClass("cls-1812131710")
                    .trigger("change");
                $("#programming_create_select_instructor").prop("disabled", false);
            } else {
                $("#programming_create_instructor")
                    .data("PresaveRequired", false)
                    .removeClass("cls-1812131710")
                    .val("");
                $("#programming_create_select_instructor").prop("disabled", true);
            }
        });
        $("#programming_create_access_type").change(function () {
            var l_at = $(this).val();
            if (l_at == 'P') {
                $("#programming_create_registration_content_protected").hide();
                $("#programming_create_registration_content_protected_list").hide();
                $("#programming_create_registration_content_private").hide();
                $("#programming_create_person_type").data("PresaveRequired", false);
            } else if (l_at == 'R') {
                $("#programming_create_registration_content_private").hide();
                $("#programming_create_registration_content_protected").show();
                $("#programming_create_registration_content_protected_list").show();
                $("#programming_create_person_type").data("PresaveRequired", true);
            } else if (l_at == 'V') {
                $("#programming_create_registration_content_protected").hide();
                $("#programming_create_registration_content_protected_list").hide();
                $("#programming_create_registration_content_private").show();
                $("#programming_create_person_type").data("PresaveRequired", false);
            }
        });
        $("#programming_create_build_calendar").click(function (evt) {
            evt.preventDefault();
            if (fn_get_programming()) {
                $('#programming_create_calendar').data('programming', {
                    isFixedTime: $("#programming_create_prog_type_h").is(":checked"),
                    isConsecutiveTime: $("#programming_create_prog_type_c").is(":checked"),
                    fromDate: $("#programming_create_from_date").val(),
                    toDate: $("#programming_create_to_date").val(),
                    fromHour: $("#programming_create_from_hour").val(),
                    toHour: $("#programming_create_to_hour").val(),
                    duration: $("#programming_create_duration").val(),
                    durationScale: $("#programming_create_duration_scale").val(),
                    interval: $("#programming_create_interval").val(),
                    intervalScale: $("#programming_create_interval_scale").val(),
                    isWeekend: $("#programming_create_weekend").is(":checked")
                });
                $('#programming_create_calendar').fullCalendar('destroy')
                $('#programming_create_calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month'
                    },
                    locale: 'es',
                    firstDay: 0,
                    events: l_events,
                    eventColor: '#c62828',
                    timeFormat: 'HH:mm',
                    displayEventTime: true,
                    displayEventEnd: true,
                    eventRender: function (event, element) {
                        element.attr('title', element.text());
                        element.addClass("cls-1812101145");
                        element.append('<div class="cls-1812101139" title="Eliminar" data-action="delete-event" data-id="' + event._id + '"><i class="fa fa-close"></i></div>');
                    }
                });
            }
        });
        $("#programming_create_general_data_form, #programming_create_registration_form, #programming_create_additional_form").submit(function (evt) {
            evt.preventDefault();
        });
        $("#programming_create_save").click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_psave = gf_presaveForm($("#programming_create_general_data_form")), l_registration, l_calendar, l_area, l_param;
            if (!l_psave.hasError) {
                l_psave = gf_presaveForm($("#programming_create_registration_form"));
                if (!l_psave.hasError) {
                    l_psave = gf_presaveForm($("#programming_create_additional_form"));
                    if (!l_psave.hasError && p_model.IsCapacitacionAdminProfile) {
                        l_psave = gf_presaveForm($("#programming_create_capacitacion_form"));
                        if (!l_psave.hasError) {
                            if (!fn_validate_capacitacion()) {
                                return;
                            }
                        }
                    }
                }
            }
            if (l_psave.hasError) {
                $.jGrowl("Ingrese correctamente los datos del formulario", {
                    header: 'Validación'
                });
                return;
            }
            if (!fn_validate_programming()) {
                return;
            }
            l_registration = fn_get_registration();
            if (l_registration == null) {
                return;
            }
            l_area = fn_get_area();
            if (l_area == null) {
                return;
            }
            l_calendar = fn_get_calendar();
            l_param = {
                'Name': $("#programming_create_name").val().trim(),
                'CourseId': $("#programming_create_course").val(),
                'ModeId': $("#programming_create_mode").val(),
                'Vacancies': $("#programming_create_vacancies").val().trim(),
                'HasTest': $("#programming_create_has_test").is(":checked") ? "S" : "N",
                'TestId': $("#programming_create_has_test").is(":checked") ? $("#programming_create_test").val() : 0,
                'Attempts': $("#programming_create_has_test").is(":checked") ? $("#programming_create_attempts").val() : 0,
                'HasInstructor': $("#programming_create_has_instructor").is(":checked") ? "S" : "N",
                'InstructorId': $("#programming_create_has_instructor").is(":checked") ? $("#programming_create_instructor").data("instructor").iId : 0,
                'ShowContent': $("#programming_create_show_content").is(":checked") ? "S" : "N",
                'PlaceId': $("#programming_create_mode").val() == l_mode_inper ? $("#programming_create_place").val() : 0,
                'PlaceDescription': $("#programming_create_mode").val() == l_mode_inper ?
                    $("#programming_create_place").val() == 0 ? $("#programming_create_place_other").val() : "_" : "_",
                'UnlimitedRegistration': $("#programming_create_unlimited").is(":checked") ? "S" : "N",
                'RegistrationLimit': !$("#programming_create_unlimited").is(":checked") ?
                    $("#programming_create_registration_limit").data('datepicker').getFormattedDate('yyyy-mm-dd') : "1900-01-01",
                'ApprovalType': 0,//$("#programming_create_approval_type").val(),
                'MinimumAssistance': 0,//$("#programming_create_approval_type").val() == '3' || $("#programming_create_approval_type").val() == '4' ?
                //$("#programming_create_minimum_assistance").val() : 0,
                'ProgrammingType': $("#programming_create_mode").val() != l_mode_inper ?
                    'X' : $("#programming_create_prog_type_h").is(":checked") ? 'H' : $("#programming_create_prog_type_c").is(":checked") ? 'C' : 'X',
                'UnlimitedProgramming': $("#programming_create_programming_unlimited").is(":checked") ? "S" : "N",
                'FromDate': $("#programming_create_mode").val() == l_mode_elea && $("#programming_create_programming_unlimited").is(":checked") ?
                    "1900-01-01" : $("#programming_create_from_date").data('datepicker').getFormattedDate('yyyy-mm-dd'),
                'ToDate': $("#programming_create_mode").val() == l_mode_elea && $("#programming_create_programming_unlimited").is(":checked") ?
                    "1900-01-01" : $("#programming_create_to_date").data('datepicker').getFormattedDate('yyyy-mm-dd'),
                'FromHour': $("#programming_create_mode").val() == l_mode_inper ? $("#programming_create_from_hour").val().replace(":", "") : "0000",
                'ToHour': $("#programming_create_mode").val() == l_mode_inper ? $("#programming_create_to_hour").val().replace(":", "") : "0000",
                'Duration': $("#programming_create_mode").val() == l_mode_inper && $("#programming_create_prog_type_c").is(":checked") ?
                    $("#programming_create_duration").val() : 0,
                'DurationScale': $("#programming_create_mode").val() == l_mode_inper && $("#programming_create_prog_type_c").is(":checked") ?
                    $("#programming_create_duration_scale").val() : "M",
                'Interval': $("#programming_create_mode").val() == l_mode_inper && $("#programming_create_prog_type_c").is(":checked") ?
                    $("#programming_create_interval").val() : 0,
                'IntervalScale': $("#programming_create_mode").val() == l_mode_inper && $("#programming_create_prog_type_c").is(":checked") ?
                    $("#programming_create_interval_scale").val() : "M",
                'Weekend': $("#programming_create_mode").val() == l_mode_inper && $("#programming_create_prog_type_h").is(":checked") ? $("#programming_create_weekend").is(":checked") ? "S" : "N" : "N",
                'AccessType': $("#programming_create_access_type").val(),
                'PersonType': $("#programming_create_person_type").val(),
                'AllArea': $("#programming_create_all_areas").is(":checked") ? "S" : "N",
                'HasCertificate': $("#programming_create_has_certificate").is(":checked") ? "S" : "N",
                'CertificateId': $("#programming_create_has_certificate").is(":checked") ? $("#programming_create_certificate").val() : 0,
                'HasPoll': $("#programming_create_has_poll").is(":checked") ? "S" : "N",
                'PollId': $("#programming_create_has_poll").is(":checked") ? $("#programming_create_poll").val() : 0,
                'CapacitacionHours': p_model.IsCapacitacionAdminProfile ? $('#programming_create_capacitacion_hours').val() : null,
                'CapacitacionFromHour': p_model.IsCapacitacionAdminProfile ? $('#programming_create_capacitacion_from_hour').val().replace(":", "") : null,
                'CapacitacionToHour': p_model.IsCapacitacionAdminProfile ? $('#programming_create_capacitacion_to_hour').val().replace(":", "") : null,
                'CalRecordId': l_calendar.RecordId,
                'CalDate': l_calendar.Date,
                'CalFromHour': l_calendar.FromHour,
                'CalToHour': l_calendar.ToHour,
                'PerRecordId': l_registration.RecordId,
                'PerPersonId': l_registration.PersonId,
                'AreaRecordId': l_area.RecordId,
                'AreaAreaId': l_area.AreaId
            };
            $.daAlert({
                content: {
                    type: "text",
                    value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "save",
                        fn: function () {
                            $.ajax({
                                data: JSON.stringify(l_param),
                                url: '/Programming/Create',
                                type: 'post',
                                dataType: 'json',
                                contentType: "application/json; charset=utf-8",
                                beforeSend: _beforeSend,
                                success: _success
                            });
                            function _beforeSend() {
                                gf_addLoadingTo($("#programming_create_save"));
                            }
                            function _success(r) {
                                if (parseInt(r.status) == 1000) {
                                    $.jGrowl(r.response, {
                                        header: 'Información',
                                        type: 'primary'
                                    });
                                    location.href = '/Programming/Index';
                                } else {
                                    $.jGrowl(r.response, {
                                        header: 'Error',
                                        type: 'danger',
                                        clipboard: r.exception
                                    });
                                    gf_removeLoadingTo($("#programming_create_save"));
                                }
                            }
                            return true;
                        }
                    }
                ],
                maximizable: false,
                title: "Confirmación"
            });
        });
        $("#programming_create_preview_test").click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_t = $("#programming_create_test").val();
            $.daAlert({
                content: {
                    type: "ajax",
                    value: {
                        url: "/Programming/Question",
                        data: {
                            'TestId': l_t
                        }
                    }
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    }
                ],
                maximizable: false,
                title: "Preguntas",
                maxWidth: "700px"
            });
        });
        $("#programming_create_select_instructor").click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_instructor = $("#programming_create_instructor").data('instructor');
            if (!l_instructor) {
                l_instructor = {
                    'iId': 0
                };
            }
            $.daAlert({
                content: {
                    type: "ajax",
                    value: {
                        url: "/Programming/Instructor",
                        data: {
                            'InstructorId': l_instructor.iId
                        }
                    }
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "accept",
                        fn: fn_add_instructor
                    }
                ],
                maximizable: false,
                title: "Seleccionar Instructor",
                maxWidth: "700px"
            });
        });
        $("#programming_create_place").change(function () {
            var l_p = $(this).val();
            if (l_p == '0') {
                $("#programming_create_place_section_other").show();
                $("#programming_create_place_other").data("PresaveRequired", true);
            } else {
                $("#programming_create_place_section_other").hide();
                $("#programming_create_place_other").data("PresaveRequired", false);
                $("#programming_create_place_other").val('');
            }
        });
        $("#programming_create_add_person").click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_vaca = $("#programming_create_vacancies").val(), l_ids = [], l_rows = l_dataTable_person.rows().nodes(), l_data;
            if (!gf_isInteger(l_vaca) || !gf_isPositive(l_vaca, true)) {
                $.jGrowl("La cantidad de Vacantes debe ser un número entero no menor a uno", {
                    header: 'Validación'
                });
                return;
            }
            if ($(l_rows).length >= l_vaca) {
                $.jGrowl("La cantidad de Vacantes ha sido ya ocupada, elimine una persona de la lista para agregar a otra", {
                    header: 'Validación'
                });
                return;
            }
            $(l_rows).each(function () {
                l_data = l_dataTable_person.row($(this)).data();
                l_ids.push(l_data[1]);
            });
            $.daAlert({
                content: {
                    type: "ajax",
                    value: {
                        url: "/Programming/Person",
                        data: {
                            'PersonIds': l_ids.toString()
                        }
                    }
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "accept",
                        fn: fn_add_person
                    }
                ],
                maximizable: false,
                title: "Seleccionar Personas",
                maxWidth: "700px"
            });
        });
        $("#programming_create_add_event").click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            $.daAlert({
                content: {
                    type: "ajax",
                    value: {
                        url: "/Programming/NewEvent"
                    }
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "add",
                        fn: fn_add_event
                    }
                ],
                maximizable: false,
                title: "Nuevo Evento"
            });
        });
        $("#programming_create_add_area").click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_ids = [], l_rows = l_dataTable_area.rows().nodes(), l_data;
            $(l_rows).each(function () {
                l_data = l_dataTable_area.row($(this)).data();
                l_ids.push(l_data[1]);
            });
            $.daAlert({
                content: {
                    type: "ajax",
                    value: {
                        url: "/Programming/Area",
                        data: {
                            'AreaIds': l_ids.toString()
                        }
                    }
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "accept",
                        fn: fn_add_area
                    }
                ],
                maximizable: false,
                title: "Seleccionar Areas",
                maxWidth: "700px"
            });
        });
        $("#programming_create_instructor").change(function () {
            $(this).prop('disabled', true);
            var l_data = $(this).data('instructor');
            if (l_data) {
                $(this).val((l_data.sDocument + " : " + l_data.sDocumentNumber + " ||| " + l_data.sFullname));
            }
        });
        $("#programming_create_person_type").change(function () {
            var l_t = $(this).val();
            if (l_t == 'I') {
                l_dataTable_area.column(1).visible(false);
            } else if (l_t == 'E') {
                l_dataTable_area.column(1).visible(true);
            } else {
                l_dataTable_area.column(0).visible(false);
                l_dataTable_area.column(1).visible(false);
            }
        });
        $("#programming_create_all_areas").change(function () {
            if ($(this).is(":checked")) {
                $("#programming_create_registration_content_protected_list_no_all").hide();
            } else {
                $("#programming_create_registration_content_protected_list_no_all").show();
            }
        });
        $("#programming_create_approval_type").change(function () {
            var l_t = $(this).val();
            if (l_t == '3' || l_t == '4') {
                $("#programming_create_section_assistance").show();
                $("#programming_create_minimum_assistance").data("PresaveRequired", true);
            } else {
                $("#programming_create_section_assistance").hide();
                $("#programming_create_minimum_assistance").data("PresaveRequired", false);
                $("#programming_create_minimum_assistance").val('');
            }
        });
        $("#programming_create_vacancies").change(function () {
            if ($("#programming_create_access_type").val() == 'V') {
                var l_last = $(this).data('lastValue'), l_value = $(this).val();
                if (!gf_isInteger(l_value) || !gf_isPositive(l_value, true)) {
                    $.jGrowl("La cantidad de Vacantes debe ser un número entero no menor a uno", {
                        header: 'Validación'
                    });
                    $(this).val(l_last);
                    return;
                }
                if ($(l_dataTable_person.rows().nodes()).length > parseInt(l_value)) {
                    $.daAlert({
                        content: {
                            type: "text",
                            value: `<div class="p-10 small text-center">La nueva cantidad de Vacantes es menor al total de Personas ya inscritas,<br />¿Está seguro que desea modificar la cantidad de Vacantes, se eliminarán las últimas personas de la lista?<div>`
                        },
                        buttons: [
                            {
                                type: "cancel",
                                fn: function () {
                                    $("#programming_create_vacancies").val(l_last);
                                    return true;
                                }
                            },
                            {
                                type: "accept",
                                fn: function () {
                                    var l_rows = l_dataTable_person.rows().nodes();
                                    $(l_rows).each(function (idx) {
                                        if (idx >= parseInt(l_value)) {
                                            $(this).addClass('row-to-eliminate');
                                        }
                                    });
                                    l_dataTable_person.rows('.row-to-eliminate').remove().draw(false);
                                    return true;
                                }
                            }
                        ],
                        maximizable: false,
                        title: "Confirmación"
                    });
                }
            }
        });
        $("#programming_create_vacancies").focus(function () {
            $(this).data('lastValue', parseInt($(this).val()));
        });
        $("#programming_create_unlimited").change(function () {
            if ($(this).is(":checked")) {
                $("#programming_create_registration_limit")
                    .prop('disabled', true)
                    .data({
                        lastValue: $("#programming_create_registration_limit").val(),
                        PresaveRequired: false
                    })
                    .val('');
            } else {
                $("#programming_create_registration_limit")
                    .prop('disabled', false)
                    .data('PresaveRequired', true)
                    .val($("#programming_create_registration_limit").data("lastValue"));
            }
        });
        $("#programming_create_mode").change(function () {
            //1 Presencial, 2 E-Learning
            var l_m = $(this).val();
            if (l_m == l_mode_inper) {
                $(".programming_create_section_mode_in_person").show();
                $(".programming_create_section_mode_in_person_elearning").show();
                $(".programming_create_section_mode_elearning, .programming_create_section_mode_in_person_elearning")
                    .removeClass('col-12 col-md-4')
                    .addClass('col-12 col-sm-6 col-md-6 col-lg-3');
                $(".programming_create_section_mode_elearning").hide();
                $("#programming_create_from_date, #programming_create_to_date")
                    .prop('disabled', false)
                    .data('PresaveRequired', true);
                $(".programming_create_section_mode_in_person_elearning label.control-label").removeClass("mt-0");
                $(".programming_create_section_mode_in_person_elearning div.small-calendar").removeClass("cls-1812131647");
                $("#programming_create_from_hour, #programming_create_to_hour, #programming_create_place").data('PresaveRequired', true);
                $("#programming_create_place").trigger('change');
                $('#programming_create_prog_type_c').trigger('change');
            } else if (l_m == l_mode_elea) {
                $(".programming_create_section_mode_in_person").hide();
                $(".programming_create_section_mode_in_person_elearning").show();
                $(".programming_create_section_mode_elearning, .programming_create_section_mode_in_person_elearning")
                    .removeClass('col-12 col-sm-6 col-md-6 col-lg-3')
                    .addClass('col-12 col-md-4');
                $(".programming_create_section_mode_elearning").show();
                $("#programming_create_programming_unlimited").trigger("change");
                $(".programming_create_section_mode_in_person_elearning label.control-label").addClass("mt-0");
                $(".programming_create_section_mode_in_person_elearning div.small-calendar").addClass("cls-1812131647");
                $("#programming_create_from_hour, #programming_create_to_hour, #programming_create_place, #programming_create_place_other").data('PresaveRequired', false);
                //update 'Tipo de Acceso'
                if ($("#programming_create_access_type option[data-value=V]").length == 0) {
                    $("#programming_create_access_type").append('<option value="V" data-value="V">Privado</option>');
                    $("#programming_create_access_type").selectpicker('refresh');
                }
            }
        });
        $("#programming_create_programming_unlimited").change(function () {
            if ($(this).is(":checked")) {
                $("#programming_create_from_date, #programming_create_to_date")
                    .prop('disabled', true)
                    .data('PresaveRequired', false);
                $("#programming_create_from_date").data('lastValue', $("#programming_create_from_date").val());
                $("#programming_create_to_date").data('lastValue', $("#programming_create_to_date").val());
                $("#programming_create_from_date, #programming_create_to_date").val('');
            } else {
                $("#programming_create_from_date, #programming_create_to_date")
                    .prop('disabled', false)
                    .data('PresaveRequired', true);
                $("#programming_create_from_date").val($("#programming_create_from_date").data("lastValue"));
                $("#programming_create_to_date").val($("#programming_create_to_date").data("lastValue"));
            }
        });
        $("#programming_create_only_saturday").change(function () {
            if ($(this).is(':checked')) {
                $("#programming_create_only_saturday_until_noon_content").show();
            } else {
                $("#programming_create_only_saturday_until_noon_content").hide();
            }
        });
        $("#programming_create_only_sunday").change(function () {
            if ($(this).is(':checked')) {
                $("#programming_create_only_sunday_until_noon_content").show();
            } else {
                $("#programming_create_only_sunday_until_noon_content").hide();
            }
        });
        $("#programming_create_rebuild_calendar_hours").click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            //si al generar la programacion se acepta que la hora inicio puede ser mayor a la hora fin, entonces cambiar logica !AQUI¡
            var l_fromHour = $("#programming_create_pnl_ctrl_from_hour").val(), l_toHour = $("#programming_create_pnl_ctrl_to_hour").val(),
                l_events, l_hhmm_start, l_hhmm_end;
            if (!gf_isValue(l_fromHour) || !gf_isValue(l_toHour)) {
                $.jGrowl("Ingrese un rango de horas válido", {
                    header: 'Validación'
                });
                return;
            }
            if (l_fromHour.indexOf('_') != -1 || l_toHour.indexOf('_') != -1) {
                $.jGrowl("Ingrese un rango de horas válido", {
                    header: 'Validación'
                });
                return;
            }
            l_fromHour = parseInt(l_fromHour.replace(':', ''));
            l_toHour = parseInt(l_toHour.replace(':', ''));
            if (l_fromHour > l_toHour) {
                $.jGrowl("La Hora Inicio no debe ser mayor a la Hora Fin", {
                    header: 'Validación'
                });
                return false;
            }
            l_events = $("#programming_create_calendar").fullCalendar('clientEvents');
            if (!Array.isArray(l_events)) {
                $.jGrowl("Primero debe generar el Calendario", {
                    header: 'Validación'
                });
                return;
            }
            $("#programming_create_calendar").fullCalendar('removeEvents', function (evt) {
                l_hhmm_start = parseInt(evt.start.format("HHmm"));
                l_hhmm_end = parseInt(evt.end ? evt.end.format("HHmm") : evt.start.format("HHmm"));
                if ((l_fromHour <= l_hhmm_start && l_toHour >= l_hhmm_start) || (l_fromHour <= l_hhmm_end && l_toHour >= l_hhmm_end) ||
                    (l_hhmm_start <= l_fromHour && l_hhmm_end >= l_fromHour) || (l_hhmm_start <= l_toHour && l_hhmm_end >= l_toHour)) {
                    return true;
                }
                return false;
            });
        });
        $("#programming_create_has_certificate").change(function () {
            if ($(this).is(":checked")) {
                $("#programming_create_certificate")
                    .val($("#programming_create_certificate").data('lastValue'))
                    .prop("disabled", false)
                    .data("PresaveRequired", true)
                    .trigger('change');
            } else {
                $("#programming_create_certificate")
                    .prop("disabled", true)
                    .data({
                        lastValue: $("#programming_create_certificate").val(),
                        PresaveRequired: false
                    })
                    .val(null)
                    .trigger('change');
            }
            $("#programming_create_certificate").selectpicker('refresh');
        });
        $('#programming_create_certificate').change(function () {
            $('#programming_create_preview_certificate').prop('disabled', !gf_isValue($(this).val()));
        });
        $('#programming_create_preview_certificate').click(function (evt) {
            evt.preventDefault();
            var l_certificate = $('#programming_create_certificate').val();
            if (!gf_isValue(l_certificate)) {
                return;
            }
            $.ajax({
                data: JSON.stringify({ CertificateId: l_certificate }),
                url: '/Programming/GetPreviewCertificate',
                type: 'post',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                beforeSend: _beforeSend,
                success: _success,
                complete: _complete
            });
            function _beforeSend() {
                gf_addLoadingTo($("#programming_create_preview_certificate"));
            }
            function _success(r) {
                if (r.status == 1) {
                    var l_template = r.response;
                    //pdf
                    //[orientation One of "portrait" or "landscape"(or shortcuts "p"(Default), "l")]
                    //[unit Measurement unit to be used when coordinates are specified. One of "pt" (points), "mm" (Default), "cm", "in"]
                    //[format One of 'a3', 'a4' (Default),'a5' ,'letter' ,'legal']
                    var l_pdf = new jsPDF(l_template.sOrientation, "pt", l_template.sPageFormat);
                    //add fonts
                    //=====> ShiningTimes
                    l_pdf.addFileToVFS(jspdfCustomfonts.ShiningTimes.file, jspdfCustomfonts.ShiningTimes.base64);
                    l_pdf.addFont(jspdfCustomfonts.ShiningTimes.file, jspdfCustomfonts.ShiningTimes.name, "normal");
                    //=====> SoDamnBeautiful
                    l_pdf.addFileToVFS(jspdfCustomfonts.SoDamnBeautiful.file, jspdfCustomfonts.SoDamnBeautiful.base64);
                    l_pdf.addFont(jspdfCustomfonts.SoDamnBeautiful.file, jspdfCustomfonts.SoDamnBeautiful.name, "normal");
                    //=====> Aliquest
                    l_pdf.addFileToVFS(jspdfCustomfonts.Aliquest.file, jspdfCustomfonts.Aliquest.base64);
                    l_pdf.addFont(jspdfCustomfonts.Aliquest.file, jspdfCustomfonts.Aliquest.name, "normal");
                    //=====> AnthoniSignature
                    l_pdf.addFileToVFS(jspdfCustomfonts.AnthoniSignature.file, jspdfCustomfonts.AnthoniSignature.base64);
                    l_pdf.addFont(jspdfCustomfonts.AnthoniSignature.file, jspdfCustomfonts.AnthoniSignature.name, "normal");
                    //=====> Yaty
                    l_pdf.addFileToVFS(jspdfCustomfonts.Yaty.file, jspdfCustomfonts.Yaty.base64);
                    l_pdf.addFont(jspdfCustomfonts.Yaty.file, jspdfCustomfonts.Yaty.name, "normal");
                    //=====> SilverstainSignature
                    l_pdf.addFileToVFS(jspdfCustomfonts.SilverstainSignature.file, jspdfCustomfonts.SilverstainSignature.base64);
                    l_pdf.addFont(jspdfCustomfonts.SilverstainSignature.file, jspdfCustomfonts.SilverstainSignature.name, "normal");
                    //=====> Abecedary
                    l_pdf.addFileToVFS(jspdfCustomfonts.Abecedary.file, jspdfCustomfonts.Abecedary.base64);
                    l_pdf.addFont(jspdfCustomfonts.Abecedary.file, jspdfCustomfonts.Abecedary.name, "normal");
                    //=====> Bintanghu
                    l_pdf.addFileToVFS(jspdfCustomfonts.Bintanghu.file, jspdfCustomfonts.Bintanghu.base64);
                    l_pdf.addFont(jspdfCustomfonts.Bintanghu.file, jspdfCustomfonts.Bintanghu.name, "normal");
                    //=====> Hatachi
                    l_pdf.addFileToVFS(jspdfCustomfonts.Hatachi.file, jspdfCustomfonts.Hatachi.base64);
                    l_pdf.addFont(jspdfCustomfonts.Hatachi.file, jspdfCustomfonts.Hatachi.name, "normal");
                    //add board
                    l_pdf.addImage(l_template.oImage.sBase64, 'JPEG', 0, 0, l_template.oSize.fWidthPt, l_template.oSize.fHeightPt);
                    //add children
                    for (var i in l_template.oChildren) {
                        if (l_template.oChildren[i].sType == 'image') {
                            l_pdf.addImage(l_template.oChildren[i].oImage.sBase64, 'PNG', l_template.oChildren[i].oPosition.fLeftPt, l_template.oChildren[i].oPosition.fTopPt + 0.5, l_template.oChildren[i].oSize.fWidthPt, l_template.oChildren[i].oSize.fHeightPt);
                        } else if (l_template.oChildren[i].sType == 'text') {
                            l_pdf.setFont(l_template.oChildren[i].oFont.sFamily);
                            l_pdf.setFontSize(l_template.oChildren[i].oFont.iSizePt);
                            l_pdf.setFontStyle(l_template.oChildren[i].oFont.sStyle);
                            l_pdf.setTextColor(l_template.oChildren[i].oFont.sColor);
                            if (l_template.oChildren[i].oSize.bFullWidth) {
                                l_pdf.customText(l_template.oChildren[i].oText.sText, { align: "center" }, l_template.oChildren[i].oPosition.fLeftPt, (l_template.oChildren[i].oPosition.fTopPt + l_template.oChildren[i].oSize.fHeightPt - 4));
                            } else {
                                l_pdf.text(l_template.oChildren[i].oPosition.fLeftPt, (l_template.oChildren[i].oPosition.fTopPt + l_template.oChildren[i].oSize.fHeightPt - 4), l_template.oChildren[i].oText.sText);
                            }
                        }
                    }
                    //save
                    l_pdf.save('Vista Previa.pdf');
                } else {
                    $.jGrowl(r.response, {
                        header: 'Error',
                        type: 'danger',
                        clipboard: r.exception
                    });
                }
            }
            function _complete() {
                gf_removeLoadingTo($("#programming_create_preview_certificate"));
            }
        });
        $("#programming_create_has_poll").change(function () {
            if ($(this).is(":checked")) {
                $("#programming_create_poll")
                    .val($("#programming_create_poll").data('lastValue'))
                    .prop("disabled", false)
                    .data("PresaveRequired", true)
                    .trigger('change');
            } else {
                $("#programming_create_poll")
                    .prop("disabled", true)
                    .data({
                        lastValue: $("#programming_create_poll").val(),
                        PresaveRequired: false
                    })
                    .val(null)
                    .trigger('change');
            }
            $("#programming_create_poll").selectpicker('refresh');
        });
        $('#programming_create_poll').change(function () {
            $('#programming_create_preview_poll').prop('disabled', !gf_isValue($(this).val()));
        });
        $('#programming_create_preview_poll').click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_poll = $('#programming_create_poll').val();
            if (gf_isValue(l_poll)) {
                $.daAlert({
                    content: {
                        type: "ajax",
                        value: {
                            url: "/Programming/PreviewPoll",
                            data: {
                                'PollId': l_poll
                            }
                        }
                    },
                    buttons: [
                        {
                            type: "cancel",
                            fn: function () {
                                return true;
                            }
                        }
                    ],
                    maximizable: false,
                    title: "Preguntas",
                    maxWidth: "700px"
                });
            }
        });
        //fn
        function fn_add_area() {
            var l_dt_area, l_rows, l_row_data;
            l_dt_area = $("#programming_area_table").DataTable();
            l_rows = l_dt_area.rows().nodes();
            if ($(l_rows).find(":checkbox:checked").length == 0) {
                $.jGrowl("Seleccione por lo menos un Área", {
                    header: 'Validación'
                });
                return false;
            }
            $(l_rows).find(":checkbox:checked").each(function () {
                l_row_data = l_dt_area.row($(this).closest("tr")).data();
                l_dataTable_area.row.add([
                    0,
                    l_row_data[0],
                    l_row_data[1]
                ]).draw(false);
            });
            return true;
        }
        function fn_add_instructor() {
            var l_dt_instructor, l_rows, l_row_data;
            l_dt_instructor = $("#programming_instructor_table").DataTable();
            l_rows = l_dt_instructor.rows().nodes();
            if ($(l_rows).find(":radio:checked").length == 0) {
                $.jGrowl("Seleccione un Instructor", {
                    header: 'Validación'
                });
                return false;
            }
            l_row_data = l_dt_instructor.row($(l_rows).find(":radio:checked").closest("tr")).data();
            $("#programming_create_instructor").data('instructor', {
                'iId': l_row_data[0],
                'sDocument': l_row_data[1],
                'sDocumentNumber': l_row_data[2],
                'sFullname': l_row_data[3],
                'sType': l_row_data[4]
            });
            $("#programming_create_instructor").val((l_row_data[1] + " : " + l_row_data[2] + " ||| " + l_row_data[3]));
            return true;
        }
        function fn_add_event() {
            var l_psave = gf_presaveForm($("#programming_newevent_form")), l_fromHour, l_toHour, l_start, l_end;
            if (l_psave.hasError) {
                $.jGrowl("Ingrese correctamente los datos del formulario", {
                    header: 'Validación'
                });
                return false;
            }
            l_fromHour = $("#programming_newevent_from_hour").val();
            l_toHour = $("#programming_newevent_to_hour").val();
            if (l_fromHour.indexOf('_') != -1 || l_toHour.indexOf('_') != -1) {
                $.jGrowl("Ingrese un rango de horas válido", {
                    header: 'Validación'
                });
                return false;
            }
            l_fromHour = parseInt(l_fromHour.replace(':', ''));
            l_toHour = parseInt(l_toHour.replace(':', ''));
            l_start = moment($("#programming_newevent_date").datepicker('getDate'));
            l_end = moment($("#programming_newevent_date").datepicker('getDate'));
            l_start.hour(parseInt((l_fromHour / 100)));
            l_start.minute(parseInt((l_fromHour % 100)));
            if (l_toHour < l_fromHour) {
                l_toHour = 2400 + l_toHour;
            }
            l_end.hour(parseInt((l_toHour / 100)));
            l_end.minute(parseInt((l_toHour % 100)));
            if (fn_get_overlap(l_start, l_end)) {
                $.jGrowl("Existe intersección entre el nuevo evento y los existentes", {
                    header: 'Validación'
                });
                return false;
            }
            $('#programming_create_calendar').fullCalendar('renderEvent', {
                title: l_start.format('DD/MM/YYYY'),
                start: l_start.toDate(),
                end: l_end.toDate(),
                color: '#1565c0'
            });
            return true;
        }
        function fn_get_overlap(p_start, p_end) {
            var l_calendar = $("#programming_create_calendar").fullCalendar('clientEvents');
            for (var i in l_calendar) {
                if ((moment(l_calendar[i].start).valueOf() < p_start.valueOf() && moment(l_calendar[i].end).valueOf() > p_start.valueOf()) ||
                    (moment(l_calendar[i].start).valueOf() < p_end.valueOf() && moment(l_calendar[i].end).valueOf() > p_end.valueOf()) ||
                    (p_start.valueOf() < moment(l_calendar[i].start).valueOf() && p_end.valueOf() > moment(l_calendar[i].start).valueOf()) ||
                    (p_start.valueOf() < moment(l_calendar[i].end).valueOf() && p_end.valueOf() > moment(l_calendar[i].end).valueOf())) {
                    return true;
                }
            }
            return false;
        }
        function fn_get_area() {
            var l_return = {
                RecordId: [],
                AreaId: []
            };
            if ($("#programming_create_access_type").val() == 'R' && !$("#programming_create_all_areas").is(":checked")) {
                $(l_dataTable_area.rows().nodes()).each(function () {
                    l_return.RecordId.push(l_dataTable_area.row($(this)).data()[0]);
                    l_return.AreaId.push(l_dataTable_area.row($(this)).data()[1]);
                });
                if (l_return.RecordId.length == 0) {
                    $.jGrowl("Debe ingresar por lo menos un área en la tabla de Áreas en Inscripción", {
                        header: 'Validación'
                    });
                    return null;
                }
            } else {
                l_return.RecordId.push(-1);
                l_return.AreaId.push(-1);
            }
            return l_return;
        }
        function fn_get_calendar() {
            var l_return = {
                RecordId: [],
                Date: [],
                FromHour: [],
                ToHour: []
            };
            if ($("#programming_create_mode").val() == l_mode_elea) {
                l_return.RecordId.push(-1);
                l_return.Date.push("1900-01-01");
                l_return.FromHour.push("0000");
                l_return.ToHour.push("0000");
            } else {
                var l_calendar = $("#programming_create_calendar").fullCalendar('clientEvents'), l_start, l_end;
                for (var i in l_calendar) {
                    if (l_calendar[i].end) {
                        l_start = moment(l_calendar[i].start);
                        //l_end = moment((l_calendar[i].end == null ? l_calendar[i].start : l_calendar[i].end));
                        l_end = moment(l_calendar[i].end);
                        l_return.RecordId.push(0);
                        l_return.Date.push(l_start.format('YYYY-MM-DD'));
                        l_return.FromHour.push(l_start.format('HHmm'));
                        l_return.ToHour.push(l_end.format('HHmm'));
                    }
                }
            }
            return l_return;
        }
        function fn_get_registration() {
            var l_return = {
                RecordId: [],
                PersonId: []
            };
            if ($("#programming_create_access_type").val() == 'V') {
                $(l_dataTable_person.rows().nodes()).each(function () {
                    l_return.RecordId.push(l_dataTable_person.row($(this)).data()[0]);
                    l_return.PersonId.push(l_dataTable_person.row($(this)).data()[1]);
                });
                if (l_return.RecordId.length == 0) {
                    $.jGrowl("Debe ingresar por lo menos una persona en la tabla de Inscritos en Inscripción", {
                        header: 'Validación'
                    });
                    return null;
                }
                if (parseInt($("#programming_create_vacancies").val()) < l_return.RecordId.length) {
                    $.jGrowl("El total de personas Inscritas supera a la cantidad de Vacantes", {
                        header: 'Validación'
                    });
                    return null;
                }
            } else {
                l_return.RecordId.push(-1);
                l_return.PersonId.push(-1);
            }
            return l_return;
        }
        function fn_validate_programming() {
            if ($("#programming_create_mode").val() == l_mode_elea) {
                return true;
            }
            var l_events = $("#programming_create_calendar").fullCalendar('clientEvents');
            if (!Array.isArray(l_events)) {
                $.jGrowl("Primero debe generar el Calendario", {
                    header: 'Validación'
                });
                return false;
            }
            if (l_events.length == 0) {
                $.jGrowl("No se encontró ningún horario en el Calendario", {
                    header: 'Validación'
                });
                return false;
            }
            var l_programming = $("#programming_create_calendar").data("programming");
            if (l_programming.isFixedTime != $("#programming_create_prog_type_h").is(":checked")) {
                $.jGrowl("El valor de 'Hora Fija' no es el mismo que generó el Calendario de Programación", {
                    header: 'Validación'
                });
                return false;
            }
            if (l_programming.isConsecutiveTime != $("#programming_create_prog_type_c").is(":checked")) {
                $.jGrowl("El valor de 'Consecutivo' no es el mismo que generó el Calendario de Programación", {
                    header: 'Validación'
                });
                return false;
            }
            if (l_programming.fromDate != $("#programming_create_from_date").val()) {
                $.jGrowl("El valor de 'Fecha Inicio' no es el mismo que generó el Calendario de Programación", {
                    header: 'Validación'
                });
                return false;
            }
            if (l_programming.toDate != $("#programming_create_to_date").val()) {
                $.jGrowl("El valor de 'Fecha Fin' no es el mismo que generó el Calendario de Programación", {
                    header: 'Validación'
                });
                return false;
            }
            if (l_programming.fromHour != $("#programming_create_from_hour").val()) {
                $.jGrowl("El valor de 'Hora Inicio' no es el mismo que generó el Calendario de Programación", {
                    header: 'Validación'
                });
                return false;
            }
            if (l_programming.toHour != $("#programming_create_to_hour").val()) {
                $.jGrowl("El valor de 'Hora Fin' no es el mismo que generó el Calendario de Programación", {
                    header: 'Validación'
                });
                return false;
            }
            if (l_programming.isConsecutiveTime) {
                if (l_programming.duration != $("#programming_create_duration").val()) {
                    $.jGrowl("El valor de 'Duración' no es el mismo que generó el Calendario de Programación", {
                        header: 'Validación'
                    });
                    return false;
                }
                if (l_programming.durationScale != $("#programming_create_duration_scale").val()) {
                    $.jGrowl("El valor de la escala de 'Duración' no es el mismo que generó el Calendario de Programación", {
                        header: 'Validación'
                    });
                    return false;
                }
                if (l_programming.interval != $("#programming_create_interval").val()) {
                    $.jGrowl("El valor de 'Intervalo' no es el mismo que generó el Calendario de Programación", {
                        header: 'Validación'
                    });
                    return false;
                }
                if (l_programming.intervalScale != $("#programming_create_interval_scale").val()) {
                    $.jGrowl("El valor de la escala de 'Intervalo' no es el mismo que generó el Calendario de Programación", {
                        header: 'Validación'
                    });
                    return false;
                }
            }
            if (l_programming.isFixedTime) {
                if (l_programming.isWeekend != $("#programming_create_weekend").is(":checked")) {
                    $.jGrowl("El valor de 'Sábados y Domingos' no es el mismo que generó el Calendario de Programación", {
                        header: 'Validación'
                    });
                    return false;
                }
            }
            return true;
        }
        function fn_add_person() {
            var l_dt_person, l_rows, l_row_data, l_persons = $(l_dataTable_person.rows().nodes()).length,
                l_vaca = parseInt($("#programming_create_vacancies").val());
            l_dt_person = $("#programming_person_table").DataTable();
            l_rows = l_dt_person.rows().nodes();
            if ($(l_rows).find(":checkbox:checked").length == 0) {
                $.jGrowl("Seleccione por lo menos una Persona", {
                    header: 'Validación'
                });
                return false;
            }
            if ($(l_rows).find(":checkbox:checked").length + l_persons > l_vaca) {
                $.jGrowl("La cantidad de Vacantes será superada si agrega a estas personas.<br />Vacantes disponibles : " + (l_vaca - l_persons), {
                    header: 'Validación'
                });
                return false;
            }
            $(l_rows).find(":checkbox:checked").each(function () {
                l_row_data = l_dt_person.row($(this).closest("tr")).data();
                l_dataTable_person.row.add([
                    0,
                    l_row_data[0],
                    l_row_data[1],
                    l_row_data[2],
                    l_row_data[3],
                    l_row_data[4],
                    l_row_data[5]
                ]).draw(false);
            });
            return true;
        }
        function fn_get_programming() {
            var l_from_date = $("#programming_create_from_date").datepicker('getDate'), l_to_date = $("#programming_create_to_date").datepicker('getDate'),
                l_from_hour = $("#programming_create_from_hour").val(), l_to_hour = $("#programming_create_to_hour").val(), l_ptype, l_duration = null,
                l_interval = null, l_weekend = $("#programming_create_weekend").is(":checked"), l_from_date_moment, l_to_date_moment, l_diff_days, l_ptr,
                l_only_saturday = $("#programming_create_only_saturday").is(":checked"), l_only_saturday_noon = $("#programming_create_only_saturday_until_noon").is(":checked"),
                l_only_sunday = $("#programming_create_only_sunday").is(":checked"), l_only_sunday_noon = $("#programming_create_only_sunday_until_noon").is(":checked");
            //validation
            if (!gf_isValue(l_from_date) || !gf_isValue(l_to_date)) {
                $.jGrowl("Ingrese un rango de fechas válido", {
                    header: 'Validación'
                });
                return false;
            }
            if (!gf_isValue(l_from_hour) || !gf_isValue(l_to_hour)) {
                $.jGrowl("Ingrese un rango de horas válido", {
                    header: 'Validación'
                });
                return false;
            }
            if (l_from_hour.indexOf('_') != -1 || l_to_hour.indexOf('_') != -1) {
                $.jGrowl("Ingrese un rango de horas válido", {
                    header: 'Validación'
                });
                return false;
            }
            l_from_hour = parseInt(l_from_hour.replace(':', ''));
            l_to_hour = parseInt(l_to_hour.replace(':', ''));
            if (l_from_hour > l_to_hour) {
                $.jGrowl("La Hora Inicio no debe ser mayor a la Hora Fin", {
                    header: 'Validación'
                });
                return false;
            }
            $("#programming_create_general_data_form input[type=radio][name=programmingtype]").each(function () {
                if ($(this).is(":checked")) {
                    l_ptype = $(this).val();
                }
            });
            if (l_ptype == 'C') {
                // los tiempos de 'Duración', 'Intervalo' estan en minutos
                l_duration = $("#programming_create_duration").val();
                l_interval = $("#programming_create_interval").val();
                if (!gf_isInteger(l_duration) || !gf_isPositive(l_duration, true)) {
                    $.jGrowl("Ingrese una duración válida, la duración debe ser un valor numérico entero positivo diferente de cero", {
                        header: 'Validación'
                    });
                    return false;
                }
                if (!gf_isInteger(l_interval) || !gf_isPositive(l_interval, true)) {
                    $.jGrowl("Ingrese una intervalo válido, el intervalo debe ser un valor numérico entero positivo diferente de cero", {
                        header: 'Validación'
                    });
                    return false;
                }
                l_duration = parseInt(l_duration);
                l_interval = parseInt(l_interval);
                if ($("#programming_create_duration_scale").val() == 'H') {
                    l_duration = l_duration * 60;
                }
                if ($("#programming_create_interval_scale").val() == 'H') {
                    l_interval = l_interval * 60;
                }
            }
            //build events
            l_events.length = 0;
            l_from_date_moment = moment(l_from_date);
            l_to_date_moment = moment(l_to_date);
            l_diff_days = l_to_date_moment.diff(l_from_date_moment, 'days');
            l_ptr = 0;
            while (l_ptr <= l_diff_days) {
                fn_build_event({
                    'dDate': moment(l_from_date_moment).add(l_ptr, 'days'),
                    'iFromHour': l_from_hour,
                    'iToHour': l_to_hour,
                    'bInterval': l_ptype == 'C',
                    'iDuration': l_duration,
                    'iInterval': l_interval,
                    'bWeekend': l_weekend,
                    'bSaturday': l_only_saturday,
                    'bSaturdayNoon': l_only_saturday_noon,
                    'bSunday': l_only_sunday,
                    'bSundayNoon': l_only_sunday_noon
                });
                l_ptr++;
            }
            return true;
        }
        function fn_build_event(p_data) {
            var l_saturday_noon, l_sunday_noon;
            if (p_data.bInterval) {
                if ((!p_data.bSaturday && p_data.dDate.day() == 6) || (!p_data.bSunday && p_data.dDate.day() == 0)) {
                    return;
                }
                if (p_data.bSaturday && p_data.bSaturdayNoon && p_data.dDate.day() == 6) {
                    l_saturday_noon = true;
                }
                if (p_data.bSunday && p_data.bSundayNoon && p_data.dDate.day() == 0) {
                    l_sunday_noon = true;
                }
            } else {
                if (!p_data.bWeekend && (p_data.dDate.day() == 0 || p_data.dDate.day() == 6)) {
                    return;
                }
            }
            var l_start = moment(p_data.dDate), l_end = moment(p_data.dDate), l_toHour, l_istart, l_iend;
            l_start.hour(parseInt((p_data.iFromHour / 100)));
            l_start.minute(parseInt((p_data.iFromHour % 100)));
            if (p_data.iToHour < p_data.iFromHour) {
                l_toHour = 2400 + p_data.iToHour;
            } else {
                l_toHour = p_data.iToHour;
            }
            l_end.hour(parseInt((l_toHour / 100)));
            l_end.minute(parseInt((l_toHour % 100)));
            //events
            if (p_data.bInterval) {
                l_istart = moment(l_start);
                while (l_istart.toDate().getTime() <= l_end.toDate().getTime()) {
                    l_iend = moment(l_istart);
                    l_iend.add(p_data.iDuration, 'minutes');
                    if (l_iend.toDate().getTime() >= l_end.toDate().getTime()) {
                        l_iend = moment(l_end);
                    }
                    //for saturday, sunday
                    if (l_saturday_noon || l_sunday_noon) {
                        if (parseInt(l_istart.format("HHmm")) >= 1200 || parseInt(l_iend.format("HHmm")) >= 1200) {
                            break;
                        }
                    }
                    //
                    l_events.push({
                        title: l_istart.format('DD/MM/YYYY'),
                        start: l_istart.toDate(),
                        end: l_iend.toDate()
                    });
                    l_istart.add((p_data.iDuration + p_data.iInterval), 'minutes');
                }
            } else {
                l_events.push({
                    title: l_start.format('DD/MM/YYYY'),
                    start: l_start.toDate(),
                    end: l_end.toDate()
                });
            }
        }
        function fn_validate_capacitacion() {
            var l_fromHour = $("#programming_create_capacitacion_from_hour").val(), l_toHour = $("#programming_create_capacitacion_to_hour").val();
            if (l_fromHour.indexOf('_') != -1 || l_toHour.indexOf('_') != -1) {
                $.jGrowl("Ingrese un rango de horas válido", {
                    header: 'Validación'
                });
                $('a[href="#tab-capacitacion"]').trigger('click');
                return false;
            }
            return true;
        }
        //init
        l_mode_elea = gf_Helpers_GetParameterByKey('CODI_MODA_ELEA');
        l_mode_inper = gf_Helpers_GetParameterByKey('CODI_MODA_PRES');
        $("#programming_create_unlimited").trigger("change");
        $("#programming_create_all_areas").trigger("change");
        $("#programming_create_person_type").trigger("change");
        $("#programming_create_has_test").trigger("change");
        $("#programming_create_has_poll").trigger("change");
        $("#programming_create_has_certificate").trigger("change");
        $("#programming_create_has_instructor").trigger("change");
        $("#programming_create_access_type").trigger("change");
        $("#programming_create_general_data_form input[type=radio][name=programmingtype]").each(function () {
            if ($(this).is(":checked")) {
                $(this).trigger("change");
            }
        });
        $("#programming_create_course, #programming_create_mode, #programming_create_vacancies, #programming_create_approval_type, #programming_create_access_type, #programming_create_from_date, #programming_create_to_date").data({
            "PresaveRequired": true
        });
        $("#programming_create_name").data({ "PresaveRequired": true, "PresaveJson": '{"maxLength":200}' });
        $("#programming_create_vacancies, #programming_create_minimum_assistance, #programming_create_attempts").data({
            "PresaveList": "integer",
            "PresaveJson": '{"min":1}'
        });
        $("#programming_create_place_other").data("PresaveJson", '{"maxLength":200}');
        $("#programming_create_approval_type").val(null).addClass('selectpicker').selectpicker();
        if (p_model.IsCapacitacionAdminProfile) {
            $('#programming_create_capacitacion_hours, #programming_create_capacitacion_from_hour, #programming_create_capacitacion_to_hour').data({
                "PresaveRequired": true
            });
            $('#programming_create_capacitacion_hours').data({
                "PresaveList": "numeric,positive,noZero"
            });
            $("#programming_create_capacitacion_from_hour, #programming_create_capacitacion_to_hour").inputmask({
                alias: "datetime",
                inputFormat: "HH:MM",
                placeholder: "__:__"
            });
        }
        gf_Helpers_CourseAreaDropDown({
            $: $("#programming_create_course"),
            callback: function (o) {
                o.addClass("selectpicker").val(null).selectpicker();
            }
        });
        gf_Helpers_ModeDropDown({
            $: $("#programming_create_mode"),
            callback: function (o) {
                o.addClass("selectpicker").val(null).selectpicker();
            }
        });
        gf_Helpers_PlaceDropDown({
            $: $("#programming_create_place"),
            options: [{ value: 0, text: 'Otros' }],
            callback: function (o) {
                o.addClass("selectpicker").val(null).selectpicker();
            }
        });
        gf_Helpers_PollDropDown({
            $: $("#programming_create_poll"),
            callback: function (o) {
                o.addClass("selectpicker").val(null).selectpicker();
            }
        });
        gf_Helpers_CertificateDropDown({
            $: $("#programming_create_certificate"),
            callback: function (o) {
                o.addClass("selectpicker").val(null).selectpicker();
            }
        });
        //jsPDF
        (function (API) {
            API.customText = function (txt, options, x, y) {
                options = options || {};
                /* Use the options align property to specify desired text alignment
                 * Param x will be ignored if desired text alignment is 'center'.
                 * Usage of options can easily extend the function to apply different text 
                 * styles and sizes 
                */
                if (options.align == "center") {
                    // Get current font size
                    var fontSize = this.internal.getFontSize();
                    // Get page width
                    var pageWidth = this.internal.pageSize.width;
                    // Get the actual text's width
                    /* You multiply the unit width of your string by your font size and divide
                     * by the internal scale factor. The division is necessary
                     * for the case where you use units other than 'pt' in the constructor
                     * of jsPDF.
                    */
                    var txtWidth = this.getStringUnitWidth(txt) * fontSize / this.internal.scaleFactor;
                    // Calculate text's x coordinate
                    x = (pageWidth - txtWidth) / 2;
                }
                // Draw text at x,y
                this.text(txt, x, y);
            }
        })(jsPDF.API);
    }(gf_getClone(ngon.Model)));
});