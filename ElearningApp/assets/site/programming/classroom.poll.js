﻿(function (p_model) {
    var l_questions = {};
    //events
    if (p_model.Source == 'Poll') {
        $('#programming_poll_save').click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_data = fn_getData();
            if (!l_data) {
                return;
            }
            $.daAlert({
                content: {
                    type: "text",
                    value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "save",
                        fn: function () {
                            $.ajax({
                                data: JSON.stringify(l_data),
                                url: '/Programming/SaveRegisterPoll',
                                type: 'post',
                                dataType: 'json',
                                contentType: "application/json; charset=utf-8",
                                beforeSend: _beforeSend,
                                success: _success
                            });
                            function _beforeSend() {
                                gf_addLoadingTo($("#programming_poll_save"));
                            }
                            function _success(r) {
                                if (parseInt(r.status) == 1000) {
                                    $.jGrowl(r.response, {
                                        header: 'Información',
                                        type: 'primary'
                                    });
                                    $('#programming_classroom_poll').trigger('click');
                                } else {
                                    $.jGrowl(r.response, {
                                        header: 'Error',
                                        type: 'danger',
                                        clipboard: r.exception
                                    });
                                    gf_removeLoadingTo($("#programming_poll_save"));
                                }
                            }
                            return true;
                        }
                    }
                ],
                maximizable: false,
                title: "Confirmación"
            });
        });
    }
    //fn
    function fn_getData() {
        var l_return = {
            'RegisterId': p_model.RegisterId,
            'HasComment': p_model.HasComment,
            'CommentQuestion': p_model.CommentQuestion,
            'Comment': $('#programming_poll_comment').val().trim(),
            'QtnId': [],
            'QtnQuestion': [],
            'QtnOrder': [],
            'QtnScore': []
        }, l_error = false;
        //get ranking
        for (var i in l_questions) {
            var l_ranking = l_questions[i].getSelected();
            if (!l_ranking) {
                $.jGrowl("No todas las preguntas han sido respondidas", {
                    header: "Validación"
                });
                l_error = true;
                break;
            }
            l_return.QtnId.push(l_ranking.question[4]);
            l_return.QtnQuestion.push(l_ranking.question[1]);
            l_return.QtnOrder.push(l_ranking.question[2]);
            l_return.QtnScore.push(l_ranking.value);
        }
        if (l_error) {
            return null;
        }
        //get comment
        if (l_return.Comment.length == 0) {
            $.jGrowl("Comentario es campo requerido", {
                header: "Validación"
            });
            return null;
        }
        if (l_return.Comment.length > 200) {
            $.jGrowl("Comentario debe ser de longitud menor o igual a 200", {
                header: "Validación"
            });
            return null;
        }
        return l_return;
    }
    //init
    for (var i in p_model.Questions) {
        l_questions[p_model.Questions[i][0]] = $('#programming_poll div[data-question=' + p_model.Questions[i][0] + ']').daRanking({
            dalbaPath: '/assets/dalba',
            items: [{ text: '', data: { 'value': 1, 'question': p_model.Questions[i] }, emoji: '/assets/site/assets/images/emojis/unhappy.svg' },
            { text: '', data: { 'value': 2, 'question': p_model.Questions[i] }, emoji: '/assets/site/assets/images/emojis/sad.svg' },
            { text: '', data: { 'value': 3, 'question': p_model.Questions[i] }, emoji: '/assets/site/assets/images/emojis/suspicious.svg' },
            { text: '', data: { 'value': 4, 'question': p_model.Questions[i] }, emoji: '/assets/site/assets/images/emojis/smiling.svg' },
            { text: '', data: { 'value': 5, 'question': p_model.Questions[i] }, emoji: '/assets/site/assets/images/emojis/happy.svg' }],
            score: p_model.Source == 'Poll' ? null : p_model.Questions[i][3],
            readonly: p_model.Source != 'Poll'
        });
    }
    gf_removeLoadingTo($('#programming_classroom_poll'));
}(gf_getClone(ngon.Model)));