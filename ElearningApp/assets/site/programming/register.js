﻿"use strict";
$("document").ready(function () {
    (function (p_model) {
        var l_events = [], l_stoday = gf_Helpers_GetUTCDate() + ' ' + gf_Helpers_GetUTCTime(), l_today = moment(l_stoday, 'DD/MM/YYYY HH:mm');
        $("#programming_register_calendar").on('click', 'div[data-action=delete-event]', function (evt) {
            evt.preventDefault();
            var l_this = $(this);
            l_this.addClass('pointer-events-none');
            var l_event = $("#programming_register_calendar").fullCalendar('clientEvents', $(this).attr("data-id"))[0].data;
            var l_alert = $.daAlert({
                content: {
                    type: 'text',
                    value: '<div class="text-center p-05">¿Está seguro que desea eliminar la inscripción del día ' + l_event[2] + '?</div>'
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "remove",
                        fn: function (p, e) {
                            $.ajax({
                                data: JSON.stringify({ ProgrammingId: l_event[0], Item: l_event[1] }),
                                url: '/Programming/DeleteRegister',
                                type: 'post',
                                dataType: 'json',
                                contentType: "application/json; charset=utf-8",
                                beforeSend: _beforeSend,
                                success: _success,
                                complete: _complete
                            });
                            function _beforeSend() {
                                gf_addLoadingTo(e);
                            }
                            function _success(r) {
                                if (parseInt(r.status) == 1000) {
                                    $.jGrowl(r.response, {
                                        header: 'Información',
                                        type: 'primary'
                                    });
                                    fn_refresh_calendar();
                                } else {
                                    $.jGrowl(r.response, {
                                        header: 'Error',
                                        type: 'danger',
                                        clipboard: r.exception
                                    });
                                }
                            }
                            function _complete() {
                                l_alert.close();
                            }
                            return false;
                        }
                    }
                ],
                maximizable: false,
                title: 'Confirmación',
                onclose: function () {
                    l_this.removeClass('pointer-events-none');
                }
            });
        });
        function fn_save(evt) {
            var l_param = {
                "ProgrammingId": p_model.Id,
                "Item": evt.data[1],
                'Schedule': evt.data[2] + ' ' + evt.start.format('HH:mm') + ' - ' + evt.end.format('HH:mm')
            };
            var l_alert = $.daAlert({
                content: {
                    type: "text",
                    value: "<div class='text-center p-4'>¿Está seguro que desea inscribirse para el día " + evt.data[2] + "?</div>"
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "save",
                        fn: function (p, e) {
                            $.ajax({
                                data: JSON.stringify(l_param),
                                url: '/Programming/Register',
                                type: 'post',
                                dataType: 'json',
                                contentType: "application/json; charset=utf-8",
                                beforeSend: _beforeSend,
                                success: _success,
                                complete: _complete
                            });
                            function _beforeSend() {
                                gf_addLoadingTo(e);
                            }
                            function _success(r) {
                                if (parseInt(r.status) == 1000) {
                                    $.jGrowl(r.response, {
                                        header: 'Información',
                                        type: 'primary'
                                    });
                                    fn_refresh_calendar();
                                } else {
                                    $.jGrowl(r.response, {
                                        header: 'Error',
                                        type: 'danger',
                                        clipboard: r.exception
                                    });
                                }
                            }
                            function _complete() {
                                l_alert.close();
                            }
                            return false;
                        }
                    }
                ],
                maximizable: false,
                title: "Confirmación"
            });
        }
        function fn_build_calendar() {
            $('#programming_register_calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month'//agendaDay
                },
                locale: 'es',
                firstDay: 0,
                events: l_events,
                eventColor: '#c62828',
                timeFormat: 'HH:mm',
                displayEventTime: true,
                displayEventEnd: true,
                eventRender: function (event, element) {
                    element.attr('title', element.text());
                    element.addClass("cls-1812101145");
                    element.find('div.fc-content').append('<span class="fc-vacancies">Vac. Disp.: ' + (event.data[5] - event.data[6]) + '</span>');
                    if (l_today.isBefore(event.start)) {
                        if (event.data[7] == 1) {
                            element.append('<div class="cls-1812101139" title="Eliminar" data-action="delete-event" data-id="' + event._id + '"><i class="fa fa-close"></i></div>');
                        }
                    } else {
                        element.addClass('cls-1901161207');
                        if (event.data[7] == 1) {
                            element.append('<div class="cls-1901170442" title="Inscrito"><i class="fa fa-check"></i></div>');
                        }
                    }
                },
                eventClick: function (event, jsEvent, view) {
                    if (l_today.isBefore(event.start)) {
                        if ($(jsEvent.target).closest("div[data-action=delete-event]").length == 0) {
                            fn_save(event);
                        }
                    }
                }
            });
        }
        function fn_get_event(p_item) {
            var l_fromHour = parseInt(p_item[3]), l_toHour = parseInt(p_item[4]), l_start = moment(p_item[2], "DD/MM/YYYY"), l_end = moment(p_item[2], "DD/MM/YYYY");
            l_start.hour(parseInt((l_fromHour / 100)));
            l_start.minute(parseInt((l_fromHour % 100)));
            if (l_toHour < l_fromHour) {
                l_toHour = 2400 + l_toHour;
            }
            l_end.hour(parseInt((l_toHour / 100)));
            l_end.minute(parseInt((l_toHour % 100)));
            return {
                title: l_start.format('DD/MM/YYYY'),
                start: l_start.toDate(),
                end: l_end.toDate(),
                data: p_item
            };
        }
        function fn_refresh_calendar() {
            $.ajax({
                data: JSON.stringify({ programming: p_model.Id }),
                url: '/Programming/RefreshCalendarToRegister',
                type: 'post',
                async: false,
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                beforeSend: _beforeSend,
                success: _success,
                complete: _complete
            });
            function _beforeSend() {
                l_events.length = 0;
            }
            function _success(r) {
                var l_r = r.response;
                if (r.status == 1) {
                    for (var i in l_r) {
                        l_events.push(fn_get_event(l_r[i]));
                    }
                }
            }
            function _complete() {
                $("#programming_register_calendar").fullCalendar('destroy');
                fn_build_calendar();
            }
        }
        //init
        (function () {
            for (var i in p_model.Events) {
                l_events.push(fn_get_event(p_model.Events[i]));
            }
            fn_build_calendar();
        }());
    }(gf_getClone(ngon.Model)));
});