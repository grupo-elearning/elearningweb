﻿(function (p_model) {
    if (p_model.IsCarrier && p_model.IsApproved) {
        var l_tag_a = document.createElement('a');
        l_tag_a.href = p_model.CarrierProtocol;
        l_tag_a.click();
        l_tag_a = null;
    }
    if (p_model.FromCarrierLogin) {
        setTimeout(function () {
            location.href = '/Account/Logout';
        }, ((p_model.Parameter.iWaitTimeCloseSession + 1) * 1000));
    }
}(gf_getClone(ngon.Model)));