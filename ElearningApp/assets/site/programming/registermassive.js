﻿"use strict";
$("document").ready(function () {
    (function (p_model) {
        var l_events = [], l_alert_edit, l_dataTable, l_row_data, l_stoday = gf_Helpers_GetUTCDate() + ' ' + gf_Helpers_GetUTCTime(), l_today = moment(l_stoday, 'DD/MM/YYYY HH:mm');
        $("#programming_registermassive_calendar").on('click', 'div[data-action=edit-event]', function (evt) {
            evt.preventDefault();
            var l_this = $(this);
            l_this.addClass('pointer-events-none');
            var l_event = $("#programming_registermassive_calendar").fullCalendar('clientEvents', l_this.attr("data-id"))[0];
            l_alert_edit = $.daAlert({
                content: {
                    type: "ajax",
                    value: {
                        url: "/Programming/EditRegisterMassive?programming=" + gf_encode(p_model.Id) + "&item=" + gf_encode(l_event.data[1]),
                        type: 'get'
                    }
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "custom",
                        icon: "fa fa-pencil",
                        text: "Editar",
                        className: "btn btn-raised btn-warning",
                        fn: function (p, e) {
                            fn_edit(p, e, l_event);
                        }
                    }
                ],
                title: "Editar Inscripciones del " + l_event.data[2],
                onclose: function () {
                    l_this.removeClass('pointer-events-none');
                }
            });
        });
        $("#programming_registermassive_calendar").on('click', 'div[data-action=view-event]', function (evt) {
            evt.preventDefault();
            var l_this = $(this);
            l_this.addClass('pointer-events-none');
            var l_event = $("#programming_registermassive_calendar").fullCalendar('clientEvents', l_this.attr("data-id"))[0];
            $.daAlert({
                content: {
                    type: "ajax",
                    value: {
                        url: "/Programming/ViewRegisterMassive?programming=" + gf_encode(p_model.Id) + "&item=" + gf_encode(l_event.data[1]),
                        type: 'get'
                    }
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    }
                ],
                title: "Inscripciones del " + l_event.data[2],
                onclose: function () {
                    l_this.removeClass('pointer-events-none');
                }
            });
        });
        function fn_edit(p, e, p_evt) {
            var l_return = false, l_row_data, l_dt_employee = $('#programming_editregistermassive_table').DataTable();
            var l_param = {
                'ProgrammingId': p_model.Id,
                'EvtRecordIdA': [],
                'EvtRecordIdB': [],
                'EvtRecordIdC': [],
                'ScheduleDate': p_evt.data[2],
                'ScheduleTime': p_evt.start.format('HH:mm') + ' - ' + p_evt.end.format('HH:mm')
            };
            l_dt_employee.rows().nodes().to$().find(':checkbox').each(function () {
                if (!$(this).is(':checked')) {
                    l_row_data = l_dt_employee.row($(this).closest("tr")).data();
                    l_param.EvtRecordIdA.push(l_row_data[0]);
                    l_param.EvtRecordIdB.push(l_row_data[1]);
                    l_param.EvtRecordIdC.push(l_row_data[2]);
                }
            });
            if (l_param.EvtRecordIdA.length == 0) {
                $.jGrowl("No ha quitado la Inscripción a ningún Empleado de la lista", {
                    header: 'Validación'
                });
                return false;
            }
            $.ajax({
                data: JSON.stringify(l_param),
                url: '/Programming/EditRegisterMassive',
                type: 'post',
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                beforeSend: _beforeSend,
                success: _success,
                complete: _complete
            });
            function _beforeSend() {
                gf_addLoadingTo(e);
            }
            function _success(r) {
                if (parseInt(r.status) === 1000) {
                    $.jGrowl(r.response, {
                        header: 'Información',
                        type: 'primary'
                    });
                    l_return = true;
                    fn_refresh_calendar();
                } else {
                    $.jGrowl(r.response, {
                        header: 'Error',
                        type: 'danger',
                        clipboard: r.exception
                    });
                    l_return = false;
                }
            }
            function _complete() {
                if (l_return) {
                    l_alert_edit.close();
                } else {
                    gf_removeLoadingTo(e);
                }
            }
            return false;
        }
        function fn_save(evt) {
            var l_alert, l_param = {
                'ProgrammingId': evt.data[0],
                'Item': evt.data[1],
                'RgRecordId': [],
                'RgPersonId': [],
                'ScheduleDate': evt.data[2],
                'ScheduleTime': evt.start.format('HH:mm') + ' - ' + evt.end.format('HH:mm')
            };
            l_dataTable.rows().nodes().to$().find(':checkbox:checked').each(function () {
                l_row_data = l_dataTable.row($(this).closest("tr")).data();
                if (l_row_data[3] != 'S') {
                    l_param.RgRecordId.push(0);
                    l_param.RgPersonId.push(l_row_data[0]);
                }
            });
            if (l_param.RgPersonId.length == 0) {
                $.jGrowl("No ha seleccionado a ningún Empleado de la lista", {
                    header: 'Validación'
                });
                return;
            }
            l_alert = $.daAlert({
                content: {
                    type: "text",
                    value: "<div class='text-center p-4'>¿Está seguro que desea inscribir a los Empleados seleccionados para el día " + evt.data[2] + "?</div>"
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "save",
                        fn: function (p, e) {
                            $.ajax({
                                data: JSON.stringify(l_param),
                                url: '/Programming/RegisterMassive',
                                type: 'post',
                                dataType: 'json',
                                contentType: "application/json; charset=utf-8",
                                beforeSend: _beforeSend,
                                success: _success,
                                complete: _complete
                            });
                            function _beforeSend() {
                                gf_addLoadingTo(e);
                            }
                            function _success(r) {
                                if (parseInt(r.status) == 1000) {
                                    $.jGrowl(r.response, {
                                        header: 'Información',
                                        type: 'primary'
                                    });
                                    l_dataTable.cells().nodes().to$().find(':checkbox:checked').prop('checked', false);
                                    fn_refresh_calendar();
                                } else {
                                    $.jGrowl(r.response, {
                                        header: 'Error',
                                        type: 'danger',
                                        clipboard: r.exception
                                    });
                                }
                            }
                            function _complete() {
                                l_alert.close();
                            }
                            return false;
                        }
                    }
                ],
                maximizable: false,
                title: "Confirmación"
            });
        }
        function fn_build_calendar() {
            $('#programming_registermassive_calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month'//agendaDay
                },
                locale: 'es',
                firstDay: 0,
                events: l_events,
                eventColor: '#c62828',
                timeFormat: 'HH:mm',
                displayEventTime: true,
                displayEventEnd: true,
                eventRender: function (event, element) {
                    element.attr('title', element.text());
                    element.addClass("cls-1812101145");
                    element.find('div.fc-content').append('<span class="fc-vacancies">Vac. Disp.: ' + (event.data[5] - event.data[6]) + '</span>');
                    if (l_today.isBefore(event.start)) {
                        if (event.data[7] == 1) {
                            element.append('<div class="cls-1812262109" title="Editar" data-action="edit-event" data-id="' + event._id + '"><i class="fa fa-pencil"></i></div>');
                        }
                    } else {
                        element.addClass('cls-1901161207');
                        if (event.data[7] == 1) {
                            element.append('<div class="cls-1901161208" title="Ver" data-action="view-event" data-id="' + event._id + '"><i class="fa fa-eye"></i></div>');
                        }
                    }
                },
                eventClick: function (event, jsEvent, view) {
                    if (l_today.isBefore(event.start)) {
                        if ($(jsEvent.target).closest("div[data-action=edit-event]").length == 0 &&
                            $(jsEvent.target).closest("div[data-action=view-event]").length == 0) {
                            fn_save(event);
                        }
                    }
                }
            });
        }
        function fn_get_event(p_item) {
            var l_fromHour = parseInt(p_item[3]), l_toHour = parseInt(p_item[4]), l_start = moment(p_item[2], "DD/MM/YYYY"), l_end = moment(p_item[2], "DD/MM/YYYY");
            l_start.hour(parseInt((l_fromHour / 100)));
            l_start.minute(parseInt((l_fromHour % 100)));
            if (l_toHour < l_fromHour) {
                l_toHour = 2400 + l_toHour;
            }
            l_end.hour(parseInt((l_toHour / 100)));
            l_end.minute(parseInt((l_toHour % 100)));
            return {
                title: l_start.format('DD/MM/YYYY'),
                start: l_start.toDate(),
                end: l_end.toDate(),
                data: p_item
            };
        }
        function fn_refresh_calendar() {
            $.ajax({
                data: JSON.stringify({ programming: p_model.Id }),
                url: '/Programming/RefreshCalendarToRegister',
                type: 'post',
                async: false,
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                beforeSend: _beforeSend,
                success: _success,
                complete: _complete
            });
            function _beforeSend() {
                l_events.length = 0;
            }
            function _success(r) {
                var l_r = r.response;
                if (r.status == 1) {
                    for (var i in l_r) {
                        l_events.push(fn_get_event(l_r[i]));
                    }
                }
            }
            function _complete() {
                $("#programming_registermassive_calendar").fullCalendar('destroy');
                fn_build_calendar();
            }
        }
        //init
        (function () {
            l_dataTable = $('#programming_registermassive_table').daDataTable({
                'DataTable': {
                    'data': p_model.Persons,
                    'aoColumns': [
                        {
                            'data': null,
                            'orderable': false,
                            'searchable': false,
                            'width': '20px',
                            'className': 'text-center',
                            'mRender': function (o) {
                                if (o[3] != 'S') {
                                    return `<div class="checkbox">
                                                <label class="m-0">
                                                    <input type="checkbox" /><span class="checkbox-material m-0"><span class="check"></span></span>
                                                </label>
                                            </div>`;
                                } else {
                                    return ``;
                                }
                            }
                        },
                        {
                            //'data': 1,
                            //'className': 'text-left'
                            "data": null,
                            "mRender": function (o) {
                                if (o[5] == "S") {
                                    return o[1] + '<i style="font-size: 18px; color: #140080;" class="icofont icofont-heart-beat"></i>';
                                } else {
                                    return o[1];
                                }
                            }
                        },
                        {
                            'data': null,
                            'width': '100px',
                            'className': 'text-center',
                            'mRender': function (o) {
                                return `<span class="d-none">` + o[4] + `</span>` + o[2];
                            }
                        }
                    ],
                    'order': [[1, 'asc']],
                    'processing': true,
                    'bLengthChange': false,
                    'iDisplayLength': 10,
                    'rowCallback': function (row, data) {
                        if (data[3] == 'S') {
                            $(row).addClass('cls-1901161009');
                        }
                    }
                },
                'theme': 'devexpress',
                'imgProcessing': '/assets/dalba/img/DXR.gif',
                'search': {
                    'btn': null,
                    'noSearch': [0]
                }
            }).Datatable;
        }());
        (function () {
            for (var i in p_model.Events) {
                l_events.push(fn_get_event(p_model.Events[i]));
            }
            fn_build_calendar();
        }());
    }(gf_getClone(ngon.Model)));
});