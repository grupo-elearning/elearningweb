﻿"use strict";
$("document").ready(function () {
    (function (p_model) {
        var l_tcon_txt = gf_Helpers_GetParameterByKey('CODI_TCON_TEXTO'), l_tcon_video = gf_Helpers_GetParameterByKey('CODI_TCON_VIDEO'),
            l_tcon_doc = gf_Helpers_GetParameterByKey('CODI_TCON_DOCUMENTO'), l_tcon_inter = gf_Helpers_GetParameterByKey('CODI_TCON_INTERACTIVO'),
            l_player = null, l_contents = {}, l_is_carrier = p_model.IsCarrier, l_current_index, l_exists_tcon_inter = false,
            l_total_contents = $('#programming_classroom_themes_nav a[data-object=content]').length, l_log_item = -1,
            l_in_siderperu = gf_Helpers_InSiderperu();
        $("#programming_classroom_themes_nav a[data-object=content]").click(function (evt) {
            evt.preventDefault();
            //validate in test
            if (p_model.TestId != 0) {
                if (fn_get_in_test()) {
                    $.jGrowl("Tiene un Examen pendiente", {
                        header: 'Validación'
                    });
                    return;
                }
            }
            var l_content = $(this).attr("data-content"), l_item = l_contents[l_content];
            
            //validate in interactive content
            if (l_exists_tcon_inter) {
                if (fn_get_in_interactive_content(l_content)) {
                    $.jGrowl("Tiene un Contenido Interactivo pendiente", {
                        header: 'Validación'
                    });
                    return;
                }
            }
            //validate maximum attempts
            if (p_model.TestId == 0 && l_item.tcon == l_tcon_inter) {
                if (!fn_validate_maximum_attempts_to_interactive(l_content)) {
                    $.jGrowl("Usted ha alcanzado la cantidad máxima de intentos para este Contenido Interactivo", {
                        header: 'Validación'
                    });
                    return;
                }
            }
            //hide test content
            $("#programming_classroom_test").removeClass("active");
            //hide poll content
            $("#programming_classroom_poll").removeClass("active");
            //hide contents
            $("#programming_classroom_themes_nav a[data-object=content].active").removeClass("active").trigger("blur");
            fn_destroy_player();
            $("#programming_classroom_content").empty();
            //show this content
            $(this).addClass("active");
            $("#programming_classroom_content_title").text((l_item.sec + ' - ' + l_item.ttl));
            //set content
            if (l_item.tcon == l_tcon_txt) {
                $("#programming_classroom_content").html(l_item.txt);
            } else if (l_item.tcon == l_tcon_video) {
                if (l_item.urlyt != '') {
                    if (l_in_siderperu) {
                        $("#programming_classroom_content").html(`<iframe src="` + l_item.urlsc + `?autoplay=false&amp;showinfo=true" class="cls-1902010351 cls-1904040948" allowfullscreen></iframe>`);
                    } else {
                        $("#programming_classroom_content").html(`<div id="programming_classroom_player" class="js-player" data-plyr-provider="youtube" data-plyr-embed-id="` + l_item.urlyt + `"></div>`);
                    }
                } else {
                    $("#programming_classroom_content").html(`<video id="programming_classroom_player" poster="/assets/app/img/elearning.jpg" playsinline controls>
                                                                <source src="` + l_item.urldoc + `" type="video/mp4">
                                                            </video>`);
                }
                //for carrier
                if (l_is_carrier) {
                    if ($("#programming_classroom_player").length != 0) {
                        l_player = new Plyr("#programming_classroom_player", {
                            controls: ['play-large', 'play', 'current-time', 'mute', 'volume', 'captions', 'fullscreen']
                        });
                        l_player.on('ended', event => {
                            //go next content
                            var l_next = l_current_index + 1;
                            if (l_next < l_total_contents) {
                                $('#programming_classroom_themes_nav a[data-index=' + l_next + ']').trigger('click');
                            } else {
                                if (p_model.TestId != 0) {
                                    $('#programming_classroom_test').trigger('click');
                                }
                            }
                        });
                        l_player.play();
                    }
                } else {
                    if ($("#programming_classroom_player").length != 0) {
                        l_player = new Plyr("#programming_classroom_player");
                    }
                }
            } else if (l_item.tcon == l_tcon_doc) {
                if (gf_isImage(l_item.urldoc)) {
                    $("#programming_classroom_content").html(`<div class="text-center">
                                                                <img src="` + l_item.urldoc + `" class="mh-100 mw-100" />
                                                            </div>`);
                } else {
                    $("#programming_classroom_content").html(`<iframe src="https://docs.google.com/gview?url=` + l_item.urldoc.replace("+", "") + `&embedded=true" class="cls-1902010351" style="width: 100%; min-height: 500px;"></iframe>`);
                }
            } else if (l_item.tcon == l_tcon_inter) {
                var l_current_attempt, l_maximum;
                if (p_model.TestId == 0) {
                    l_current_attempt = fn_get_current_attempt_to_interactive(l_content);
                    l_maximum = parseInt(gf_Helpers_GetParameterByKey('MAX_INTENTO_CURSO_INTERACTIVO'));
                    $.jGrowl(`Usted tiene ` + (l_maximum - l_current_attempt - 1) + ` intento(s) mas por realizar`, {
                        header: 'Validación'
                    });
                } else {
                    l_current_attempt = 0;
                    l_maximum = 9999;
                }
                $("#programming_classroom_content")
                    .addClass('app-request-fullscreen')
                    .html(`<button class="btn btn-raised btn-primary m-0 cls-1903110411" title="Minimizar" data-status="1" data-action="fullscreen">
                                <i class="fa fa-compress m-0"></i>
                            </button>
                            <iframe src="` + l_item.urlcur + `?ID=` + p_model.RegisterId + `&ID2=` + l_content + `&IACTUAL=` + (l_current_attempt + 1) + `&IMAXIMOS=` + l_maximum + `" style="width: 100%; height: 100%; min-height: 500px;"></iframe>`);
            } else {
                $("#programming_classroom_content").html(``);
            }
            //save current index
            l_current_index = parseInt($(this).attr('data-index'));
            //save visit
            fn_save_visit(l_content);
        });
        $('#programming_classroom_content').on('click', 'button[data-action=fullscreen]', function (evt) {
            evt.preventDefault();
            var l_status = $(this).attr('data-status');
            if (l_status == '0') {
                $(this)
                    .attr({
                        'data-status': '1',
                        'title': 'Minimizar'
                    })
                    .find('i.fa')
                    .removeClass('fa-expand')
                    .addClass('fa-compress');
                gf_requestFullscreen($('#programming_classroom_content'));
            } else {
                $(this)
                    .attr({
                        'data-status': '0',
                        'title': 'Maximizar'
                    })
                    .find('i.fa')
                    .removeClass('fa-compress')
                    .addClass('fa-expand');
                gf_exitFullscreen($('#programming_classroom_content'));
            }
        });
        if (p_model.TestId != 0) {
            $("#programming_classroom_test").click(function (evt) {
                evt.preventDefault();
                //validate in interactive content
                if (l_exists_tcon_inter) {
                    if (fn_get_in_interactive_content(0)) {
                        $.jGrowl("Tiene un Contenido Interactivo pendiente", {
                            header: 'Validación'
                        });
                        return;
                    }
                }
                //save current time
                if ($("#programming_test_save_current_time").length != 0) {
                    $("#programming_test_save_current_time").trigger('click');
                }
                //hide poll content
                $("#programming_classroom_poll").removeClass("active");
                //hide contents
                $("#programming_classroom_themes_nav a[data-object=content].active").removeClass("active");
                $("#programming_classroom_themes_nav ul.panel-collapse.collapse.show").removeClass('show');
                $("#programming_classroom_themes_nav a[data-toggle=collapse].withripple").addClass('collapsed');
                fn_destroy_player();
                //show test content
                $(this).addClass('active');
                $("#programming_classroom_content_title").text('Examen');
                $("#programming_classroom_content").html(`<div class="cls-1812242242">
                                                            <div class="app-center text-center w-100">
                                                                <div class="cls-1812251213 text-left">
                                                                    <div class="list-wrapper">
                                                                        <div class="red-line"></div>
                                                                        <div class="list-item-wrapper">
                                                                            <div class="list-bullet">1</div>
                                                                            <div class="list-item">
                                                                                <div class="list-title">Tipo de Examen</div>
                                                                                <div class="list-text">
                                                                                    ` + (p_model.Test.TestType == `R` ? `El usuario deberá responder la pregunta` : p_model.Test.TestType == `S` ? `El usuario puede saltar las preguntas` : p_model.Test.TestType == `T` ? `El usuario puede ver las respuestas correctas al final del examen` : ``) + `
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="list-item-wrapper">
                                                                            <div class="list-bullet">2</div>
                                                                            <div class="list-item">
                                                                                <div class="list-title">Proceder a la siguiente pregunta</div>
                                                                                <div class="list-text">
                                                                                    ` + (p_model.Test.Proceed == `A` ? `Automático: proceder directamente después de seleccionar la respuesta` : p_model.Test.Proceed == `M` ? `Manual: seleccionar la respuesta y luego hacer clic en siguiente para proceder` : ``) + `
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="list-item-wrapper">
                                                                            <div class="list-bullet">3</div>
                                                                            <div class="list-item">
                                                                                <div class="list-title">¿Este examen es a base de Tiempo?</div>
                                                                                <div class="list-text">
                                                                                    ` + (p_model.Test.Time == `NL` ? `No hay tiempo límite` : p_model.Test.Time == `TE` ? `Tiempo límite del examen : ` + p_model.Test.TimeLimit + ` minutos` : p_model.Test.Time == `TP` ? `Tiempo límite por pregunta : ` + p_model.Test.TimeLimit + ` minutos` : ``) + `
                                                                                </div>
                                                                            </div>
                                                                            <div class="white-line"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <h4 class="color-primary">Al iniciar el examen no se podrá acceder a los temas del curso hasta concluirlo</h4>
                                                                <a href="javascript:void(0)" class="btn btn-primary btn-raised btn-app" style="visibility: visible; animation-name: flipInX;" data-action="starttest">
                                                                    <div class="btn-container">
                                                                        <i class="fa fa-play-circle-o"></i>
                                                                        <span>Iniciar</span>
                                                                        <br>
                                                                        <strong>Examen</strong>
                                                                    </div>
                                                                    <div class="ripple-container"></div>
                                                                </a>
                                                            </div>
                                                        </div>`);
            });
            $("#programming_classroom_content").on('click', 'a[data-action=starttest]', function (evt) {
                evt.preventDefault();
                if (!fn_get_new_attempt_validate()) {
                    return;
                }
                gf_addLoadingTo($(this));
                $("#programming_classroom_content").load("/Programming/Test/" + gf_encode(p_model.RegisterId), function (response, status, xhr) {
                    if (status == "error") {
                        var msg = "Sorry but there was an error: ";
                        $("#programming_classroom_content").html(msg + xhr.status + " " + xhr.statusText);
                    }
                });
            });
        }
        if (p_model.PollId != 0) {
            $("#programming_classroom_poll").click(function (evt) {
                evt.preventDefault();
                //validate in test
                if (p_model.TestId != 0) {
                    if (fn_get_in_test()) {
                        $.jGrowl("Tiene un Examen pendiente", {
                            header: 'Validación'
                        });
                        return;
                    }
                }
                //validate in interactive content
                if (l_exists_tcon_inter) {
                    if (fn_get_in_interactive_content(0)) {
                        $.jGrowl("Tiene un Contenido Interactivo pendiente", {
                            header: 'Validación'
                        });
                        return;
                    }
                }
                //hide test content
                $("#programming_classroom_test").removeClass("active");
                //hide contents
                $("#programming_classroom_themes_nav a[data-object=content].active").removeClass("active");
                $("#programming_classroom_themes_nav ul.panel-collapse.collapse.show").removeClass('show');
                $("#programming_classroom_themes_nav a[data-toggle=collapse].withripple").addClass('collapsed');
                fn_destroy_player();
                //show poll content
                var l_this = $(this);
                l_this.addClass('active');
                $("#programming_classroom_content_title").text('Encuesta');
                gf_addLoadingTo(l_this);
                $("#programming_classroom_content").load("/Programming/Poll/" + gf_encode(p_model.RegisterId), function (response, status, xhr) {
                    if (status == "error") {
                        gf_removeLoadingTo(l_this);
                        var msg = "Sorry but there was an error: ";
                        $("#programming_classroom_content").html(msg + xhr.status + " " + xhr.statusText);
                    }
                });
            });
        }
        if (p_model.HasCertificate) {
            $('#programming_classroom_certificate').click(function (evt) {
                evt.preventDefault();
                var l_this = $(this);
                $.ajax({
                    data: JSON.stringify({ id: p_model.RegisterId }),
                    url: '/Programming/GetClassroomCertificate',
                    type: 'post',
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8",
                    beforeSend: _beforeSend,
                    success: _success,
                    complete: _complete
                });
                function _beforeSend() {
                    gf_addLoadingTo(l_this);
                }
                function _success(r) {
                    if (r.status == 1) {
                        fn_build_certificate(r.response);
                    } else {
                        $.jGrowl(r.response, {
                            header: 'Error',
                            type: 'danger',
                            clipboard: r.exception
                        });
                    }
                }
                function _complete() {
                    gf_removeLoadingTo(l_this);
                }
            });
        }
        function fn_destroy_player() {
            if (l_player) {
                l_player.destroy();
                l_player = null;
            }
        }
        function fn_save_visit(p_content) {
            $.ajax({
                data: JSON.stringify({ 'register': p_model.RegisterId, 'content': p_content }),
                url: '/Programming/SaveVisit',
                type: 'post',
                async: false,
                dataType: 'json',
                contentType: "application/json; charset=utf-8"
            });
        }
        function fn_get_in_test() {
            var l_status = false;
            $.ajax({
                data: JSON.stringify({ id: p_model.RegisterId }),
                url: '/Programming/GetInTest',
                type: 'post',
                dataType: 'json',
                async: false,
                contentType: "application/json; charset=utf-8",
                success: _success
            });
            function _success(r) {
                if (r.status == 1) {
                    l_status = r.response;
                } else {
                    $.jGrowl(r.response, {
                        header: 'Error',
                        type: 'danger',
                        clipboard: r.exception
                    });
                }
            }
            return l_status;
        }
        function fn_get_new_attempt_validate() {
            var l_status = true;
            $.ajax({
                data: JSON.stringify({ register: p_model.RegisterId }),
                url: '/Programming/GetNewAttemptValidate',
                type: 'post',
                dataType: 'json',
                async: false,
                contentType: "application/json; charset=utf-8",
                success: _success
            });
            function _success(r) {
                if (r.status == 1) {
                    if (r.response[0] != 0) {
                        $.jGrowl(r.response[1], {
                            header: 'Validación'
                        });
                        l_status = false;
                    }
                } else {
                    $.jGrowl(r.response, {
                        header: 'Error',
                        type: 'danger',
                        clipboard: r.exception
                    });
                }
            }
            return l_status;
        }

        function fn_build_certificate(p_data) {
            var l_template = p_data[4];
            //pdf
            //[orientation One of "portrait" or "landscape"(or shortcuts "p"(Default), "l")]
            //[unit Measurement unit to be used when coordinates are specified. One of "pt" (points), "mm" (Default), "cm", "in"]
            //[format One of 'a3', 'a4' (Default),'a5' ,'letter' ,'legal']
            var l_pdf = new jsPDF(l_template.sOrientation, "pt", l_template.sPageFormat);
            //add fonts
            //=====> ShiningTimes
            l_pdf.addFileToVFS(jspdfCustomfonts.ShiningTimes.file, jspdfCustomfonts.ShiningTimes.base64);
            l_pdf.addFont(jspdfCustomfonts.ShiningTimes.file, jspdfCustomfonts.ShiningTimes.name, "normal");
            //=====> SoDamnBeautiful
            l_pdf.addFileToVFS(jspdfCustomfonts.SoDamnBeautiful.file, jspdfCustomfonts.SoDamnBeautiful.base64);
            l_pdf.addFont(jspdfCustomfonts.SoDamnBeautiful.file, jspdfCustomfonts.SoDamnBeautiful.name, "normal");
            //=====> Aliquest
            l_pdf.addFileToVFS(jspdfCustomfonts.Aliquest.file, jspdfCustomfonts.Aliquest.base64);
            l_pdf.addFont(jspdfCustomfonts.Aliquest.file, jspdfCustomfonts.Aliquest.name, "normal");
            //=====> AnthoniSignature
            l_pdf.addFileToVFS(jspdfCustomfonts.AnthoniSignature.file, jspdfCustomfonts.AnthoniSignature.base64);
            l_pdf.addFont(jspdfCustomfonts.AnthoniSignature.file, jspdfCustomfonts.AnthoniSignature.name, "normal");
            //=====> Yaty
            l_pdf.addFileToVFS(jspdfCustomfonts.Yaty.file, jspdfCustomfonts.Yaty.base64);
            l_pdf.addFont(jspdfCustomfonts.Yaty.file, jspdfCustomfonts.Yaty.name, "normal");
            //=====> SilverstainSignature
            l_pdf.addFileToVFS(jspdfCustomfonts.SilverstainSignature.file, jspdfCustomfonts.SilverstainSignature.base64);
            l_pdf.addFont(jspdfCustomfonts.SilverstainSignature.file, jspdfCustomfonts.SilverstainSignature.name, "normal");
            //=====> Abecedary
            l_pdf.addFileToVFS(jspdfCustomfonts.Abecedary.file, jspdfCustomfonts.Abecedary.base64);
            l_pdf.addFont(jspdfCustomfonts.Abecedary.file, jspdfCustomfonts.Abecedary.name, "normal");
            //=====> Bintanghu
            l_pdf.addFileToVFS(jspdfCustomfonts.Bintanghu.file, jspdfCustomfonts.Bintanghu.base64);
            l_pdf.addFont(jspdfCustomfonts.Bintanghu.file, jspdfCustomfonts.Bintanghu.name, "normal");
            //=====> Hatachi
            l_pdf.addFileToVFS(jspdfCustomfonts.Hatachi.file, jspdfCustomfonts.Hatachi.base64);
            l_pdf.addFont(jspdfCustomfonts.Hatachi.file, jspdfCustomfonts.Hatachi.name, "normal");
            //add board
            l_pdf.addImage(l_template.oImage.sBase64, 'JPEG', 0, 0, l_template.oSize.fWidthPt, l_template.oSize.fHeightPt);
            //add children
            for (var i in l_template.oChildren) {
                if (l_template.oChildren[i].sType == 'image') {
                    l_pdf.addImage(l_template.oChildren[i].oImage.sBase64, 'PNG', l_template.oChildren[i].oPosition.fLeftPt, l_template.oChildren[i].oPosition.fTopPt + 0.5, l_template.oChildren[i].oSize.fWidthPt, l_template.oChildren[i].oSize.fHeightPt);
                } else if (l_template.oChildren[i].sType == 'text') {
                    l_pdf.setFont(l_template.oChildren[i].oFont.sFamily);
                    l_pdf.setFontSize(l_template.oChildren[i].oFont.iSizePt);
                    l_pdf.setFontStyle(l_template.oChildren[i].oFont.sStyle);
                    l_pdf.setTextColor(l_template.oChildren[i].oFont.sColor);
                    if (l_template.oChildren[i].oSize.bFullWidth) {
                        l_pdf.customText(fn_get_text_to_certificate(l_template.oChildren[i], p_data), { align: "center" }, l_template.oChildren[i].oPosition.fLeftPt, (l_template.oChildren[i].oPosition.fTopPt + l_template.oChildren[i].oSize.fHeightPt - 4));
                    } else {
                        l_pdf.text(l_template.oChildren[i].oPosition.fLeftPt, (l_template.oChildren[i].oPosition.fTopPt + l_template.oChildren[i].oSize.fHeightPt - 4), fn_get_text_to_certificate(l_template.oChildren[i], p_data));
                    }
                }
            }
            //save
            l_pdf.save('Constancia.pdf');
            //fn 
            function fn_get_text_to_certificate(p_child, p_data) {
                if (p_child.oData.sKey == 'person_name') {
                    return p_data[1];
                } else if (p_child.oData.sKey == 'course_name') {
                    return p_data[0];
                } else if (p_child.oData.sKey == 'test_score') {
                    return p_data[3].toString();
                } else if (p_child.oData.sKey == 'approved_date') {
                    var l_date = moment(p_data[2], 'DD/MM/YYYY');
                    if (p_child.oData.sDateFormat == 'dd/mm/yyyy') {
                        return l_date.format('DD/MM/YYYY');
                    } else if (p_child.oData.sDateFormat == 'DIA de MES de AÑO') {
                        return l_date.format('DD') + ' de ' + gf_getNameMonth(l_date.format('MM')) + ' de ' + l_date.format('YYYY');
                    } else {
                        return p_data[2];
                    }
                } else {
                    return '';
                }
            }
        }
        function fn_get_in_interactive_content(p_content) {
            return false;
            //
            var l_status = false;
            $.ajax({
                data: JSON.stringify({ register: p_model.RegisterId, content: p_content }),
                url: '/Programming/GetInInteractiveContent',
                type: 'post',
                dataType: 'json',
                async: false,
                contentType: "application/json; charset=utf-8",
                success: _success
            });
            function _success(r) {
                if (r.status == 1) {
                    l_status = r.response;
                } else {
                    $.jGrowl(r.response, {
                        header: 'Error',
                        type: 'danger',
                        clipboard: r.exception
                    });
                }
            }
            return l_status;
        }
        function fn_validate_maximum_attempts_to_interactive(p_content) {
            return true;
            //
            var l_status = false;
            $.ajax({
                data: JSON.stringify({ register: p_model.RegisterId, content: p_content }),
                url: '/Programming/GetMaximumAttemptsToInteractiveValidate',
                type: 'post',
                dataType: 'json',
                async: false,
                contentType: "application/json; charset=utf-8",
                success: _success
            });
            function _success(r) {
                if (r.status == 1) {
                    l_status = r.response;
                } else {
                    $.jGrowl(r.response, {
                        header: 'Error',
                        type: 'danger',
                        clipboard: r.exception
                    });
                }
            }
            return l_status;
        }
        function fn_get_current_attempt_to_interactive(p_content) {
            console.log(p_content);
            var l_value = 0;
            $.ajax({
                data: JSON.stringify({ register: p_model.RegisterId, content: p_content }),
                url: '/Programming/GetCurrentAttemptToInteractive',
                type: 'post',
                dataType: 'json',
                async: false,
                contentType: "application/json; charset=utf-8",
                success: _success
            });
            function _success(r) {
                if (r.status == 1) {
                    l_value = r.response;
                }
            }
            return l_value;
        }
        //Declaracion Jurada
        $(".close-modal-dec-jur").click(function () {
            $(".modal-dec-jur").addClass("d-block-none");
        });
        //init
        (function () {
            for (var i in p_model.Course.Themes) {
                for (var j in p_model.Course.Themes[i][2]) {
                    l_contents[p_model.Course.Themes[i][2][j][0]] = {
                        tcon: p_model.Course.Themes[i][2][j][1],
                        ttl: p_model.Course.Themes[i][2][j][2],
                        urlyt: p_model.Course.Themes[i][2][j][3],
                        urlsc: p_model.Course.Themes[i][2][j][10],
                        txt: p_model.Course.Themes[i][2][j][4],
                        urldoc: p_model.Course.Themes[i][2][j][5],
                        sec: p_model.Course.Themes[i][2][j][6],
                        cuin: p_model.Course.Themes[i][2][j][7],
                        ncur: p_model.Course.Themes[i][2][j][8],
                        urlcur: p_model.Course.Themes[i][2][j][9]
                    };
                }
            }
            for (var i in l_contents) {
                if (l_contents[i].tcon == l_tcon_inter) {
                    l_exists_tcon_inter = true;
                    break;
                }
            }
        }());
        (function () {
            if (p_model.AttemptId == 0) {
                var l_cont = p_model.CurrentThemeItem == 0 ? $($("#programming_classroom_themes_nav a[data-object=content]")[0]) : $("#programming_classroom_themes_nav a[data-object=content][data-content='" + p_model.CurrentThemeItem + "']");
                l_cont.closest("ul.panel-collapse.collapse").addClass('show');
                l_cont.closest("ul.panel-collapse.collapse").prev("a.withripple").removeClass('collapsed');
                l_cont.trigger("click");
            } else {
                $("#programming_classroom_test").trigger('click');
            }
        }());
        (function () {
            if (p_model.SecondsRemaining > 0) {
                var l_milliseconds = p_model.SecondsRemaining * 1000;
                l_milliseconds = l_milliseconds > 2147483647 ? 2147483647 : l_milliseconds;
                setTimeout(function () {
                    if ($('#programming_test_finish_hidden').length != 0) {
                        $.jGrowl('El Horario del Aula ha concluído', {
                            header: 'Horario Finalizado',
                            type: 'danger'
                        });
                        $('#programming_test_finish_hidden').trigger('click');
                        if (!l_is_carrier) {
                            window.location.reload(true);
                        }
                    } else {
                        window.location.reload(true);
                    }
                }, (l_milliseconds));
            }
        }());
        (function () {
            $.ajax({
                data: JSON.stringify({ 'register': p_model.RegisterId, 'item': 0 }),
                url: '/Programming/SaveClassroomLog',
                type: 'post',
                async: false,
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                success: function (r) {
                    if (r.status == 1000) {
                        l_log_item = r.others;
                    }
                },
                complete: function () {
                    window.cHJvZ3JhbW1pbmdfY2xhc3Nyb29tX2xvZw = l_log_item;
                }
            });
            window.cHJvZ3JhbW1pbmdfY2xhc3Nyb29tX3JlZ2lzdGVy = p_model.RegisterId;
            window.onbeforeunload = function () {
                $.ajax({
                    data: JSON.stringify({ 'register': window.cHJvZ3JhbW1pbmdfY2xhc3Nyb29tX3JlZ2lzdGVy, 'item': window.cHJvZ3JhbW1pbmdfY2xhc3Nyb29tX2xvZw }),
                    url: '/Programming/SaveClassroomLog',
                    type: 'post',
                    //async: false,
                    dataType: 'json',
                    contentType: "application/json; charset=utf-8"
                });
                return null;
            }
        }());
        //jsPDF
        (function (API) {
            API.customText = function (txt, options, x, y) {
                options = options || {};
                /* Use the options align property to specify desired text alignment
                 * Param x will be ignored if desired text alignment is 'center'.
                 * Usage of options can easily extend the function to apply different text 
                 * styles and sizes 
                */
                if (options.align == "center") {
                    // Get current font size
                    var fontSize = this.internal.getFontSize();
                    // Get page width
                    var pageWidth = this.internal.pageSize.width;
                    // Get the actual text's width
                    /* You multiply the unit width of your string by your font size and divide
                     * by the internal scale factor. The division is necessary
                     * for the case where you use units other than 'pt' in the constructor
                     * of jsPDF.
                    */
                    var txtWidth = this.getStringUnitWidth(txt) * fontSize / this.internal.scaleFactor;
                    // Calculate text's x coordinate
                    x = (pageWidth - txtWidth) / 2;
                }
                // Draw text at x,y
                this.text(txt, x, y);
            }
        })(jsPDF.API);
    }(gf_getClone(ngon.Model)));
});