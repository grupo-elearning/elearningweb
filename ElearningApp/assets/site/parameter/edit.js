﻿"use strict";
$("document").ready(function () {
    (function (p_model) {
        $('#parameter_edit_message').summernote({
            lang: 'es-ES',
            placeholder: 'Ingrese el mensaje',
            height: 150,
            toolbar: [
                ['color', ['color']],
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ]
        });
        $('#parameter_edit_message_approved').summernote({
            lang: 'es-ES',
            placeholder: 'Ingrese el mensaje para Aprobado',
            height: 300,
            toolbar: [
                ['color', ['color']],
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['customButton', ['cb_preview']]
            ],
            'buttons': {
                'cb_preview': function (c) {
                    return $.summernote.ui.button({
                        //tooltip: 'Vista Previa',
                        contents: 'Vista Previa',
                        click: function () {
                            $.daAlert({
                                content: {
                                    type: 'text',
                                    value: `<div class="p-1">
                                                <div class="cls-1901290014">
                                                    <div class="message">` + c.$note.summernote('code') + `</div>
                                                    <img src="/assets/site/assets/images/aprobado.png" class="img-approved" />
                                                </div>
                                            </div>`
                                },
                                buttons: [
                                    {
                                        type: "cancel",
                                        fn: function () {
                                            return true;
                                        }
                                    }
                                ],
                                maximizable: false,
                                title: 'Vista Previa',
                                maxWidth: '700px'
                            });
                        }
                    }).render();
                }
            }
        });
        $('#parameter_edit_message_disapproved').summernote({
            lang: 'es-ES',
            placeholder: 'Ingrese el mensaje para Desaprobado',
            height: 300,
            toolbar: [
                ['color', ['color']],
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['customButton', ['cb_preview']]
            ],
            'buttons': {
                'cb_preview': function (c) {
                    return $.summernote.ui.button({
                        //tooltip: 'Vista Previa',
                        contents: 'Vista Previa',
                        click: function () {
                            $.daAlert({
                                content: {
                                    type: 'text',
                                    value: `<div class="p-1">
                                                <div class="cls-1901290014">
                                                    <div class="message">` + c.$note.summernote('code') + `</div>
                                                    <img src="/assets/site/assets/images/desaprobado.png" class="img-disapproved" />
                                                </div>
                                            </div>`
                                },
                                buttons: [
                                    {
                                        type: "cancel",
                                        fn: function () {
                                            return true;
                                        }
                                    }
                                ],
                                maximizable: false,
                                title: 'Vista Previa',
                                maxWidth: '700px'
                            });
                        }
                    }).render();
                }
            }
        });
        //events
        $('#parameter_edit_save').click(function (evt) {
            evt.preventDefault();
            $(this).trigger("blur");
            var l_psave = gf_presaveForm($("#parameter_edit_form")), l_formdata = new FormData();
            if (l_psave.hasError) {
                $.jGrowl("Ingrese correctamente los datos del formulario", {
                    header: 'Validación'
                });
                return;
            }
            if ($('#parameter_edit_message').summernote('isEmpty')) {
                $.jGrowl("Mensaje es campo requerido", {
                    header: 'Validación'
                });
                return;
            }
            if ($('#parameter_edit_message_approved').summernote('isEmpty')) {
                $.jGrowl("Mensaje Aprobado es campo requerido", {
                    header: 'Validación'
                });
                return;
            }
            if ($('#parameter_edit_message_disapproved').summernote('isEmpty')) {
                $.jGrowl("Mensaje Desaprobado es campo requerido", {
                    header: 'Validación'
                });
                return;
            }
            l_formdata.append('CarrierCourseId', $("#parameter_edit_course").val());
            l_formdata.append('WaitTimeCloseSession', $("#parameter_edit_time").val());
            l_formdata.append('MessageFinalized', $('#parameter_edit_message').summernote('code'));
            l_formdata.append('MessageApproved', $('#parameter_edit_message_approved').summernote('code'));
            l_formdata.append('MessageDisapproved', $('#parameter_edit_message_disapproved').summernote('code'));
            $.daAlert({
                content: {
                    type: "text",
                    value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
                },
                buttons: [
                    {
                        type: "cancel",
                        fn: function () {
                            return true;
                        }
                    },
                    {
                        type: "save",
                        fn: function () {
                            $.ajax({
                                data: l_formdata,
                                url: '/Parameter/Edit',
                                type: 'post',
                                dataType: 'json',
                                contentType: false,
                                processData: false,
                                beforeSend: _beforeSend,
                                success: _success
                            });
                            function _beforeSend() {
                                gf_addLoadingTo($("#parameter_edit_save"));
                            }
                            function _success(r) {
                                if (parseInt(r.status) == 1000) {
                                    $.jGrowl(r.response, {
                                        header: 'Información',
                                        type: 'primary'
                                    });
                                    location.href = '/Parameter/Edit';
                                } else {
                                    $.jGrowl(r.response, {
                                        header: 'Error',
                                        type: 'danger',
                                        clipboard: r.exception
                                    });
                                    gf_removeLoadingTo($("#parameter_edit_save"));
                                }
                            }
                            return true;
                        }
                    }
                ],
                maximizable: false,
                title: "Confirmación"
            });
        });
        //init
        $('#parameter_edit_course, #parameter_edit_time').data('PresaveRequired', true);
        $("#parameter_edit_time").data({
            "PresaveList": "integer",
            "PresaveJson": '{"min":0}'
        });
        gf_Helpers_CourseAreaDropDown({
            $: $("#parameter_edit_course"),
            callback: function (o) {
                o.addClass("selectpicker").val(p_model.CarrierCourseId).selectpicker();
            }
        });
    }(gf_getClone(ngon.Model)));
});