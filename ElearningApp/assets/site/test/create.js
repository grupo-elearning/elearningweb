﻿"use strict";
$("document").ready(function () {
    var l_dataTable;
    $("#test_create_form").submit(function (evt) {
        evt.preventDefault();
    });
    $('#test_create_save').click(function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        var l_psave = gf_presaveForm($("#test_create_form")), l_data;
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return;
        }
        l_data = fn_getData();
        if (l_data == null) {
            return;
        }
        $.daAlert({
            content: {
                type: "text",
                value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "save",
                    fn: function () {
                        $.ajax({
                            data: JSON.stringify(l_data),
                            url: '/Test/Create',
                            type: 'post',
                            dataType: 'json',
                            contentType: "application/json; charset=utf-8",
                            beforeSend: _beforeSend,
                            success: _success
                        });
                        function _beforeSend() {
                            gf_addLoadingTo($("#test_create_save"));
                        }
                        function _success(r) {
                            if (parseInt(r.status) == 1000) {
                                $.jGrowl(r.response, {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                location.href = '/Test/Index';
                            } else {
                                $.jGrowl(r.response, {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: r.exception
                                });
                                gf_removeLoadingTo($("#test_create_save"));
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: "Confirmación"
        });
    });
    $("#test_create_course").change(function () {
        l_dataTable
            .clear()
            .draw(false);
    });
    $("#test_create_add_question").click(function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        var l_course = $("#test_create_course").val(), l_ids = [], l_rows = l_dataTable.rows().nodes(), l_data;
        if (!gf_isValue(l_course)) {
            $.jGrowl("Primero debe seleccionar un Curso", {
                header: "Validación"
            });
            return;
        }
        $(l_rows).each(function () {
            l_data = l_dataTable.row($(this)).data();
            l_ids.push(l_data[1]);
        });
        $.daAlert({
            content: {
                type: "ajax",
                value: {
                    url: '/Test/Question',
                    data: {
                        "CourseId": l_course,
                        "QuestionIds": l_ids.toString()
                    },
                }
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "add",
                    fn: function () {
                        return fn_addQuestion();
                    }
                }
            ],
            title: "Seleccionar Preguntas",
            focus: 1,
            maxWidth: "800px"
        });
    });
    $("#test_create_random").change(function () {
        if ($(this).is(":checked")) {
            $("#test_create_content_table_question").hide();
            $("#test_create_question_quantity")
                .prop("disabled", false)
                .val(($("#test_create_question_quantity").data("lastValue") ? $("#test_create_question_quantity").data("lastValue") : ''))
                .data("PresaveRequired", true);
        } else {
            $("#test_create_content_table_question").show();
            $("#test_create_question_quantity")
                .prop("disabled", true)
                .data({
                    lastValue: $("#test_create_question_quantity").val(),
                    PresaveRequired: false
                })
                .val('');
        }
    });
    $("#test_create_form input[name=time]").change(function () {
        var l_value = $(this).val();
        if (l_value == 'NL') {
            $("#test_create_content_time_limit").hide();
            $("#test_create_time_limit").val("");
            $("#test_create_time_limit").data("PresaveRequired", false);
        } else {
            $("#test_create_content_time_limit").show();
            $("#test_create_time_limit").data("PresaveRequired", true);
        }
    });
    $("#test_create_table_question").on("click", "a[data-action=delete]", function (evt) {
        evt.preventDefault();
        l_dataTable.row($(this).closest("tr")).remove().draw(false);
    });
    l_dataTable = $("#test_create_table_question").daDataTable({
        "DataTable": {
            "aoColumns": [
                {
                    "data": null,
                    "orderable": false,
                    "searchable": false,
                    "className": "cls-1812230025",
                    "width": "50px",
                    "mRender": function (o) {
                        return `<input type="number" class="form-control mb-0 text-right" min="0" data-input="order" value="` + o[4] + `" />`;
                    }
                },
                {
                    "data": 2,
                    "className": "text-left"
                },
                {
                    "data": 3,
                    "className": "text-left"
                },
                {
                    "data": null,
                    "orderable": false,
                    "searchable": false,
                    "className": "text-center",
                    "width": "50px",
                    "mRender": function (o) {
                        return `<a href="javascript:void(0)" class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" data-action="delete" title="Eliminar" >
                                    <i class="fa fa-trash m-0"></i>
                                </a>`;
                    }
                }
            ],
            "order": [[1, 'asc']],
            "processing": true,
            "bLengthChange": false,
            "iDisplayLength": 10
        },
        "theme": "devexpress",
        "imgProcessing": "/assets/dalba/img/DXR.gif",
        "search": {
            "btn": { "column": 3 },
            "noSearch": [0]
        }
    }).Datatable;
    //fn
    function fn_getData() {
        var l_response = {
            "Name": $("#test_create_name").val().trim(),
            "Status": $("#test_create_status").is(":checked") ? "A" : "I",
            "CourseId": $("#test_create_course").val(),
            "MinScore": $("#test_create_min_score").val(),
            "MaxScore": $("#test_create_max_score").val(),
            "ApproveScore": $("#test_create_approve_score").val(),
            "TestType": $("#test_create_type").val(),
            "Proceed": $("#test_create_proceed").val(),
            "Time": "",
            "TimeLimit": 0,
            "RandomQuestions": $("#test_create_random").is(":checked") ? "S" : "N",
            "QuestionsNumber": 0,
            "QRecord": [],
            "QQuestionId": [],
            "QOrder": []
        }, l_error;
        $("#test_create_form input[type=radio][name=time]").each(function () {
            if ($(this).is(":checked")) {
                l_response.Time = $(this).val();
            }
        });
        if (l_response.Time == "TE" || l_response.Time == "TP") {
            l_response.TimeLimit = $("#test_create_time_limit").val();
        }
        if (l_response.RandomQuestions == "S") {
            l_response.QuestionsNumber = $("#test_create_question_quantity").val()
            //
            l_response.QRecord.push(-1);
            l_response.QQuestionId.push(-1);
            l_response.QOrder.push(0);
        } else {
            var l_records = l_dataTable.rows().nodes(), l_order, l_data;
            if (l_records.length == 0) {
                $.jGrowl("Ingrese por lo menos una Pregunta", {
                    header: 'Validación'
                });
                return null;
            }
            //validar orden
            l_error = false;
            for (var i = 0; i < l_records.length; i++) {
                l_order = $(l_records[i]).find("input[data-input=order]").val();
                if (!gf_isPositive(l_order, true) || !gf_isInteger(l_order)) {
                    $.jGrowl("El Orden de las Preguntas debe ser un Número Entero mayor a Cero", {
                        header: 'Validación'
                    });
                    l_error = true;
                    break;
                }
                l_data = l_dataTable.row(l_records[i]).data();
                l_response.QRecord.push(l_data[0]);
                l_response.QQuestionId.push(l_data[1]);
                l_response.QOrder.push(l_order);
            }
            if (l_error) {
                return null;
            }
        }
        return l_response;
    }
    function fn_addQuestion() {
        var l_dt_questions, l_rows, l_row_data;
        l_dt_questions = $("#test_question_table_questions").DataTable();
        l_rows = l_dt_questions.rows().nodes();
        if ($(l_rows).find(":checkbox:checked").length == 0) {
            $.jGrowl("Seleccione por lo menos una Pregunta", {
                header: 'Validación'
            });
            return false;
        }
        $(l_rows).find(":checkbox:checked").each(function () {
            l_row_data = l_dt_questions.row($(this).closest("tr")).data();
            l_dataTable.row.add([
                0,
                l_row_data[0],
                l_row_data[1],
                l_row_data[2],
                ''
            ]).draw(false);
        });
        return true;
    }
    //init
    $("#test_create_random").trigger("change");
    $("#test_create_form input[type=radio][name=time]").each(function () {
        if ($(this).is(":checked")) {
            $(this).trigger("change");
        }
    });
    $("#test_create_type, #test_create_proceed").addClass('selectpicker').val(null).selectpicker();
    $("#test_create_name, #test_create_course, #test_create_min_score, #test_create_max_score, #test_create_approve_score, #test_create_type, #test_create_proceed").data({
        "PresaveRequired": true
    });
    $("#test_create_name").data("PresaveJson", '{"maxLength":200}');
    $("#test_create_min_score, #test_create_max_score, #test_create_approve_score").data({
        "PresaveList": "numeric",
        "PresaveJson": '{"min":0}'
    });
    $("#test_create_time_limit, #test_create_question_quantity").data({
        "PresaveList": "integer",
        "PresaveJson": '{"min":1}'
    });
    gf_Helpers_CourseAreaDropDown({
        $: $("#test_create_course"),
        callback: function (o) {
            o.addClass("selectpicker").val(null).selectpicker();
        }
    });
});