﻿$(document).ready(function () {

    console.log(ngon);
    var file_actual = "";

    $('#microa_create_descripcion').summernote({
        placeholder: 'Ingrese el contenido del micro-aprendizaje',
        height: 650,
        toolbar: [
            ['color', ['color']],
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert', ['picture', 'video']]
        ],
        lang: 'es-ES',
        lang: 'es-ES',
        callbacks: {
            onKeydown: function (e) {
                var t = e.currentTarget.innerText;
                if (t.trim().length >= 400) {
                    //delete keys, arrow keys, copy, cut, select all
                    if (e.keyCode != 8 && !(e.keyCode >= 37 && e.keyCode <= 40) && e.keyCode != 46 && !(e.keyCode == 88 && e.ctrlKey) && !(e.keyCode == 67 && e.ctrlKey) && !(e.keyCode == 65 && e.ctrlKey))
                        e.preventDefault();
                }
            },
            onKeyup: function (e) {
                var t = e.currentTarget.innerText;
            },
            onPaste: function (e) {
                var t = e.currentTarget.innerText;
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                var maxPaste = bufferText.length;
                if (t.length + bufferText.length > 400) {
                    maxPaste = 400 - t.length;
                }
                if (maxPaste > 0) {
                    document.execCommand('insertText', false, bufferText.substring(0, maxPaste));
                }
            }
        }
    });

    $("#microa_create_form").submit(function (evt) {
        evt.preventDefault();
    });


    $('#microa_create_save').click(function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        var l_psave = gf_presaveForm($("#microa_create_form")), l_data, l_formdata = new FormData();
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return;
        }

        if ($('#microa_create_descripcion').summernote('isEmpty')) {
            $.jGrowl("Contenido es campo requerido", {
                header: 'Validación'
            });
            return;
        }

        let head = {
            codi_mcap: parseInt(ngon.Codi_Mcap),
            titulo: $("#microa_create_name").val().trim(),
            estado: "1",
            user_system: ngon.username,
            ip: ngon.ipaddress,
            fech_inicio: $("#finicio").val().trim(),
            fech_final: $("#ffinal").val().trim()
        }
        console.log(JSON.stringify(head));
        l_formdata.append('head', JSON.stringify(head));
        l_formdata.append('html', $('#microa_create_descripcion').summernote('code'));
        l_formdata.append('file_actual', file_actual);

        $.daAlert({
            content: {
                type: "text",
                value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "save",
                    fn: function () {
                        $.ajax({
                            data: l_formdata,
                            url: ngon.UrlApi_microa_saveUpd,
                            headers: {
                                'Authorization': ngon.token_access
                            },
                            type: 'post',
                            dataType: 'json',
                            contentType: false,
                            processData: false,
                            beforeSend: _beforeSend,
                            success: _success
                        });
                        function _beforeSend() {
                            gf_addLoadingTo($("#microa_create_save"));
                            gf_triggerKeepActive();
                        }
                        function _success(res) {
                            if (res.Status != undefined && res.Status == 200) {
                                $.jGrowl("Se guardo correctamente", {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                location.href = '/MicroAprendizaje/Index';
                            } else {
                                $.jGrowl("Error", {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: "Error"
                                });
                                gf_removeLoadingTo($("#microa_create_save"));
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: "Confirmación"
        });


    });

    $("#microa_create_name").data({
        "PresaveRequired": true
    });

    function fn_initialData() {
        //Load Tipo Filtro
        console.log("initialData");
        $.ajax({
            data: JSON.stringify({ Codi_Mcap: ngon.Codi_Mcap, UserSystem: ngon.username, Ip: ngon.ipaddress }),
            url: ngon.UrlApi_microa_id,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                console.log("before");
            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    drawhtml(res.Body);
                } else {
                    $.jGrowl("No se pudo carga el Micro-Aprendizaje", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo carga el Micro-Aprendizaje"
                    });
                }
            },
            complete: function () { },
            error: function () {
                $.jGrowl("No se pudo carga el Micro-Aprendizaje", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "No se pudo carga el Micro-Aprendizaje"
                });
            }
        });
    }


    function drawhtml(object) {
        console.log(object);
        $("#microa_create_name").val(object.Titulo);
        $("#finicio").val(object.Fech_Inicio);
        $("#ffinal").val(object.Fech_Final);
        file_actual = object.File_Name;
        console.log(file_actual);
        if (object.Estado == "1") {
            $("#microa_create_status").prop('checked', true);
        } else {
            $("#microa_create_status").prop('checked', false);
        }
        $('#microa_create_descripcion').summernote('code', object.Html);
    }



    fn_initialData();

});