﻿$(document).ready(function () {

    console.log(ngon);
    var cantPag = 8;
    var cPag = 1;

    $("#micro_index_table tbody").on('click', 'a[data-action=delete]', function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        let codi_mcap = $(this).attr("data-id");
        var l_datatable = $("#slider_index_table").DataTable();
        var l_data = l_datatable.row($(this).closest('tr')).data();
        $.daAlert({
            content: {
                type: 'text',
                value: '<div class="text-center p-05">¿Está seguro que desea eliminar?</div>'
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "remove",
                    fn: function (p, e) {
                        var l_success = false;
                        $.ajax({
                            data: JSON.stringify({ Codi_Mcap: codi_mcap, UserSystem: ngon.username, Ip: ngon.ipaddress }),
                            url: ngon.UrlApi_microa_delete,
                            headers: {
                                'Authorization': ngon.token_access
                            },
                            type: 'post',
                            dataType: 'json',
                            contentType: "application/json; charset=utf-8",
                            success: _success,
                            complete: _complete
                        });
                        function _success(res) {
                            if (res.Status != undefined && res.Status == 200) {
                                $.jGrowl("Se eliminó correctamente", {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                l_success = true;
                            } else {
                                $.jGrowl("Error", {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: "Error"
                                });
                            }
                        }
                        function _complete() {
                            if (l_success) {
                                loadDataInitial();
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: 'Confirmación'
        });
    });

    $("#micro_index_table tbody").on('click', 'input[data-action=activate]', function (evt) {
        evt.preventDefault();
        console.log("hOLAS:  " + $(this).attr("data-id"));
        let codi_mcap = $(this).attr("data-id");

        let estado = $(this).is(":checked") ? "0" : "1";
        let new_estado = '';
        if (estado == '0') {
            new_estado = '1';
        } else {
            new_estado = '0';
        }

        changeEstadoVisible(codi_mcap, new_estado, $(this));

    });

    $("#micro_index_table tbody").on('change', 'input[data-action=upddate]', function (evt) {
        evt.preventDefault();
        let codi_mcap = $(this).attr("data-id");
        console.log(codi_mcap);
        let tipo = $(this).attr("data-typ");
        let finicio = "";
        let ffinal = "";
        if (tipo == "INICIAL") {
            finicio = $(this).val();
            ffinal = $("#ffinal_" + $(this).attr("data-pos")).val();
        }
        if (tipo == "FINAL") {
            finicio = $("#finicial_" + $(this).attr("data-pos")).val();
            ffinal = $(this).val();
        }

        changeEstadoFechas(codi_mcap, finicio, ffinal, $(this))

    });

    l_dataTable = $("#micro_index_table").daDataTable({
        "DataTable": {
            "aoColumns": [
                {
                    "className": "text-left"
                },
                {
                    "width": "40px",
                    "className": "text-center"
                },
                {
                    "width": "40px",
                    "className": "text-center"
                },
                {
                    "width": "50px",
                    "className": "text-center"
                }
            ],
            "processing": true,
            "bLengthChange": false,
            "iDisplayLength": 10
        },
        "theme": "devexpress",
        "imgProcessing": "/assets/dalba/img/DXR.gif",
        "search": {
            "btn": { "column": 5 }
        }
    }).Datatable;


    function loadDataInitial() {
        //Load Tipo Filtro
        $.ajax({
            data: JSON.stringify({ Start: '0', End: '0', Pag: '0', Vis: 'ALL_ADMIN' }),
            url: ngon.UrlApi_microa_list,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    drawTableMicro(res.ListBody);
                } else {
                    $.jGrowl("No se pudo cargar los Micro-Aprendizajes", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo cargar los Micro-Aprendizajes"
                    });
                }
            },
            complete: function () { },
            error: function () {
                $.jGrowl("No se pudo cargar los Micro-Aprendizajes", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "No se pudo cargar los Micro-Aprendizajes"
                });
            }
        });
    }

    function drawTableMicro(listMicro) {
        let trDOM;
        
        l_dataTable.clear().draw();
        for (let itm in listMicro) {
            trDOM = l_dataTable.row.add([
                listMicro[itm].Titulo,
                `<input type="date" data-action="upddate" data-typ="INICIAL" data-pos="` + itm + `" id="finicial_` + itm+ `" data-id="` + listMicro[itm].Codi_Mcap + `" data-date="` + listMicro[itm].Fech_Inicio +`" value="` + listMicro[itm].Fech_Inicio+`" class="form-control">`,
                `<input type="date" data-action="upddate" data-typ="FINAL" data-pos="` + itm + `" id="ffinal_` + itm + `" data-id="` + listMicro[itm].Codi_Mcap + `" data-date="` + listMicro[itm].Fech_Final +`" value="` + listMicro[itm].Fech_Final +`" class="form-control">`,
                `<a href="/MicroAprendizaje/Edit/` + listMicro[itm].Codi_Mcap + `" class="btn btn-xs btn-raised btn-warning m-0 cls-1810072234" title="Editar" >
                                    <i class="fa fa-edit m-0"></i>
                                </a>
                                <a href="javascript:void(0)" class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" data-action="delete" data-id="`+ listMicro[itm].Codi_Mcap + `" title="Eliminar" >
                             <i class="fa fa-trash m-0"></i>
                     </a>`
            ]).draw(false).node();

        }
    }

    function changeEstadoVisible(codi_mcap, estado, etiqueta) {
        $.ajax({
            data: JSON.stringify({ Codi_Mcap: codi_mcap, Estado: estado, UserSystem: ngon.username, Ip: ngon.ipaddress }),
            url: ngon.UrlApi_microa_updStatus,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    $.jGrowl("Se actualizo correctamente.", {
                        header: 'Información',
                        type: 'primary'
                    });
                    if (estado == "1") {
                        etiqueta.prop('checked', true);
                    } else {
                        etiqueta.prop('checked', false);
                    }
                } else {
                    $.jGrowl("No se pudo cargar los slider", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo cargar los slider"
                    });
                }
            },
            complete: function () { },
            error: function () {
                $.jGrowl("No se pudo cargar los slider", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "No se pudo cargar los slider"
                });
            }
        });
    }

    function changeEstadoFechas(codi_mcap, finicio, ffinal, etiqueta) {
        $.ajax({
            data: JSON.stringify({ Codi_Mcap: codi_mcap, Fech_Inicio: finicio, Fech_Final: ffinal, UserSystem: ngon.username, Ip: ngon.ipaddress }),
            url: ngon.UrlApi_microa_updDate,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    $.jGrowl("Se actualizo correctamente.", {
                        header: 'Información',
                        type: 'primary'
                    });
                } else {
                    $.jGrowl("No se pudo actualizar la fecha", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo actualizar la fecha"
                    });
                }
            },
            complete: function () { },
            error: function () {
                $.jGrowl("No se pudo actualizar la fecha", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "No se pudo actualizar la fecha"
                });
            }
        });
    }

    loadDataInitial();



});