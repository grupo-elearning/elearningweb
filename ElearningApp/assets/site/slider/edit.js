﻿$(document).ready(function () {

    console.log(ngon);

    $('#slider_create_descripcion').summernote({
        placeholder: 'Ingrese la descripción',
        height: 250,
        toolbar: [
        ],
        lang: 'es-ES',
        callbacks: {
            onKeydown: function (e) {
                var t = e.currentTarget.innerText;
                if (t.trim().length >= 400) {
                    //delete keys, arrow keys, copy, cut, select all
                    if (e.keyCode != 8 && !(e.keyCode >= 37 && e.keyCode <= 40) && e.keyCode != 46 && !(e.keyCode == 88 && e.ctrlKey) && !(e.keyCode == 67 && e.ctrlKey) && !(e.keyCode == 65 && e.ctrlKey))
                        e.preventDefault();
                }
            },
            onKeyup: function (e) {
                var t = e.currentTarget.innerText;
            },
            onPaste: function (e) {
                var t = e.currentTarget.innerText;
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                var maxPaste = bufferText.length;
                if (t.length + bufferText.length > 400) {
                    maxPaste = 400 - t.length;
                }
                if (maxPaste > 0) {
                    document.execCommand('insertText', false, bufferText.substring(0, maxPaste));
                }
            }
        }
    });



    $("#slider_create_form").submit(function (evt) {
        evt.preventDefault();
    });
    $('#slider_create_image').change(function () {
        var l_validate = gf_fileValidation({
            fileInput: $(this),
            imgPreview: $("#slider_create_image_preview"),
            fnError: function () {
                $.jGrowl("Solo puede seleccionar archivos tipo: jpg, jpeg, png, bmp", {
                    header: "Validación"
                });
            }
        });
        if (l_validate) {
            $("#slider_create_image_notification").hide();
            $("#slider_create_image_btn_add").hide();
            $("#slider_create_image_btn_remove").show();
        } else {
            $("#slider_create_image_preview").attr("src", "/assets/app/img/no-image-icon.svg");
            $("#slider_create_image_notification").show();
            $("#slider_create_image_btn_remove").hide();
            $("#slider_create_image_btn_add").show();
        }
    });
    $('#slider_create_image_btn_add').click(function (evt) {
        evt.preventDefault();
        $("#slider_create_image").trigger("click");
    });
    $('#slider_create_image_btn_remove').click(function (evt) {
        evt.preventDefault();
        $("#slider_create_image").val("").trigger("change");
    });


    $('#slider_create_save').click(function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        var l_psave = gf_presaveForm($("#slider_create_form")), l_data, l_formdata = new FormData();
        if (l_psave.hasError) {
            $.jGrowl("Ingrese correctamente los datos del formulario", {
                header: 'Validación'
            });
            return;
        }

        if ($('#slider_create_descripcion').summernote('isEmpty')) {
            $.jGrowl("Descripción es campo requerido", {
                header: 'Validación'
            });
            return;
        }

        let head = {
            codi_sldr: ngon.Codi_Sldr,
            titulo: $("#slider_create_name").val().trim(),
            descripcion: $($("#slider_create_descripcion").summernote("code")).text(),
            img: "",
            estado: $("#slider_create_status").is(":checked") ? "1" : "0",
            user_system: ngon.username,
            ip: ngon.ipaddress
        }
        console.log(JSON.stringify(head));
        l_formdata.append('head', JSON.stringify(head));

        let l_pos_file = 0;
        //Slider IMG
        let l_img = $('#slider_create_image')[0].files;
        l_pos_file = 0;
        for (var i = 0; i < l_img.length; i++) {
            l_formdata.append("Files[" + l_pos_file + "].File", l_img[i]);
            l_formdata.append("Files[" + l_pos_file + "].Extension", l_img[i].name.split('.').pop());
            l_formdata.append("Files[" + l_pos_file + "].RealName", l_img[i].name);
            console.log(l_img[i].name);
            l_formdata.append("Files[" + l_pos_file + "].WasRemoved", false);
            l_formdata.append("Files[" + l_pos_file + "].WasRetrieved", false);
            l_formdata.append("Files[" + l_pos_file + "].WasChoosed", true);
            l_formdata.append("Files[" + l_pos_file + "].TipoDoc", "IMAGEN_SLIDER");
            l_pos_file++;
        }


        $.daAlert({
            content: {
                type: "text",
                value: "<div class='text-center p-05'>¿Está seguro que desea guardar?</div>"
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "save",
                    fn: function () {
                        $.ajax({
                            data: l_formdata,
                            url: ngon.UrlApi_slider_saveUpd,
                            headers: {
                                'Authorization': ngon.token_access
                            },
                            type: 'post',
                            dataType: 'json',
                            contentType: false,
                            processData: false,
                            beforeSend: _beforeSend,
                            success: _success
                        });
                        function _beforeSend() {
                            gf_addLoadingTo($("#slider_create_save"));
                            gf_triggerKeepActive();
                        }
                        function _success(res) {
                            if (res.Status != undefined && res.Status == 200) {
                                $.jGrowl("Se actualizo correctamente.", {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                location.href = '/Slider/Index';
                            } else {
                                $.jGrowl("Error", {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: "Error"
                                });
                                gf_removeLoadingTo($("#slider_create_save"));
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: "Confirmación"
        });


    });

    $("#slider_create_name, #slider_create_image_display").data({
        "PresaveRequired": true
    });

    //Contador de Caracteres para summernote
    $("#slider_create_descripcion").on("keypress", function () {
        var limiteCaracteres = 50;
        var caracteres = $(this).text();
        var totalCaracteres = caracteres.length;


        //Check and Limit Charaters
        if (totalCaracteres >= limiteCaracteres) {
            return false;
        }
    });

    function fn_initialData() {
        //Load Tipo Filtro
        console.log("initialData");
        $.ajax({
            data: JSON.stringify({ Codi_Sldr: ngon.Codi_Sldr, UserSystem: ngon.username, Ip: ngon.ipaddress }),
            url: ngon.UrlApi_slider_id,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {
                console.log("before");
            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    drawhtml(res.Body);
                } else {
                    $.jGrowl("No se pudo carga el slider", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo carga el slider"
                    });
                }
            },
            complete: function () { },
            error: function () {
                $.jGrowl("No se pudo carga el slider", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "No se pudo carga el slider"
                });
            }
        });
    }

    function drawhtml(object) {
        $("#slider_create_name").val(object.Titulo);
        $("#slider_create_image_display").val(object.ImgNombreReal);
        $('#slider_create_descripcion').summernote('editor.insertText', object.Descripcion);
        $("#slider_create_image_preview").attr("src", object.Img);
        if (object.Estado == "1") {
            $("#slider_create_status").prop('checked', true);
        } else {
            $("#slider_create_status").prop('checked', false);
        }

        $("#slider_create_image_notification").hide();
        $("#slider_create_image_btn_add").hide();
        $("#slider_create_image_btn_remove").show();
    }

    fn_initialData();

});