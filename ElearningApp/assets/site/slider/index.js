﻿$(document).ready(function () {

    console.log(ngon);

    l_dataTable = $("#slider_index_table").daDataTable({
        "DataTable": {
            "aoColumns": [
                {
                    "className": "text-left"
                },
                {
                    "className": "text-left"
                },
                {
                    "width": "50px"
                },
                {
                    "width": "50px",
                    "className": "text-center"
                },
                {
                    "width": "50px",
                     "className": "text-center"
                }
            ],
            "processing": true,
            "bLengthChange": false,
            "iDisplayLength": 10
        },
        "theme": "devexpress",
        "imgProcessing": "/assets/dalba/img/DXR.gif",
        "search": {
            "btn": { "column": 4 }
        }
    }).Datatable;

    $("#slider_index_table tbody").on('click', 'a[data-action=delete]', function (evt) {
        evt.preventDefault();
        $(this).trigger("blur");
        console.log("hOLAS:  " + $(this).attr("data-id"));
        let codi_sldr = $(this).attr("data-id"); 
        var l_datatable = $("#slider_index_table").DataTable();
        var l_data = l_datatable.row($(this).closest('tr')).data();
        $.daAlert({
            content: {
                type: 'text',
                value: '<div class="text-center p-05">¿Está seguro que desea eliminar?</div>'
            },
            buttons: [
                {
                    type: "cancel",
                    fn: function () {
                        return true;
                    }
                },
                {
                    type: "remove",
                    fn: function (p, e) {
                        var l_success = false;
                        $.ajax({
                            data: JSON.stringify({ Codi_Sldr: codi_sldr, UserSystem: ngon.username, Ip: ngon.ipaddress }),
                            url: ngon.UrlApi_slider_delete,
                            headers: {
                                'Authorization': ngon.token_access
                            },
                            type: 'post',
                            dataType: 'json',
                            contentType: "application/json; charset=utf-8",
                            success: _success,
                            complete: _complete
                        });
                        function _success(res) {
                            if (res.Status != undefined && res.Status == 200) {
                                $.jGrowl("Se eliminó correctamente", {
                                    header: 'Información',
                                    type: 'primary'
                                });
                                l_success = true;
                            } else {
                                $.jGrowl("Error", {
                                    header: 'Error',
                                    type: 'danger',
                                    clipboard: "Error"
                                });
                            }
                        }
                        function _complete() {
                            if (l_success) {
                                loadDataInitial();
                            }
                        }
                        return true;
                    }
                }
            ],
            maximizable: false,
            title: 'Confirmación'
        });
    });

    $("#slider_index_table tbody").on('click', 'input[data-action=activate]', function (evt) {
        evt.preventDefault();
        console.log("hOLAS:  " + $(this).attr("data-id"));
        let codi_sldr = $(this).attr("data-id");
        var l_datatable = $("#slider_index_table").DataTable();

        let estado = $(this).is(":checked") ? "0" : "1";
        let new_estado = '';
        if (estado == '0') {
            new_estado = '1';
        } else {
            new_estado = '0';
        }

        changeEstadoVisible(codi_sldr, new_estado, $(this));

    });

    function loadDataInitial() {
        //Load Tipo Filtro
        $.ajax({
            data: '',
            url: ngon.UrlApi_slider_listAll,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    drawTableSlider(res.ListBody);
                } else {
                    $.jGrowl("No se pudo cargar los slider", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo cargar los slider"
                    });
                }
            },
            complete: function () { },
            error: function () {
                $.jGrowl("No se pudo cargar los slider", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "No se pudo cargar los slider"
                });
            }
        });
    }

    function drawTableSlider(listSlider) {
        let trDOM;
        l_dataTable.clear().draw();
        for (let itm in listSlider) {
                console.log(itm);
                trDOM = l_dataTable.row.add([
                    listSlider[itm].Titulo,
                    listSlider[itm].Descripcion,
                    `<img src="`+listSlider[itm].Img+`" alt="Imagen" style="width:80px">`,
                    `<div class="togglebutton">
                             <label class="m-0">
                                 <input type="checkbox" data-action="activate" ` + (listSlider[itm].Estado == '1' ? `checked` : ``) + ` data-id="` + listSlider[itm].Codi_Sldr +`"  /><span class="toggle m-0"></span>
                             </label>
                     </div>`,
                    `<a href="/Slider/Edit/`+listSlider[itm].Codi_Sldr+`" class="btn btn-xs btn-raised btn-warning m-0 cls-1810072234" title="Editar" >
                                    <i class="fa fa-edit m-0"></i>
                                </a>
                                <a href="javascript:void(0)" class="btn btn-xs btn-raised btn-danger m-0 cls-1810072234" data-action="delete" data-id="`+ listSlider[itm].Codi_Sldr+`" title="Eliminar" >
                             <i class="fa fa-trash m-0"></i>
                     </a>`
                ]).draw(false).node();

        }
    }

    function changeEstadoVisible(codi_sldr, estado, etiqueta) {
        $.ajax({
            data: JSON.stringify({ Codi_Sldr: codi_sldr, Estado: estado, UserSystem: ngon.username, Ip: ngon.ipaddress }),
            url: ngon.UrlApi_slider_updStatus,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    $.jGrowl("Se actualizo correctamente.", {
                        header: 'Información',
                        type: 'primary'
                    });
                    if (estado == "1") {
                        etiqueta.prop('checked', true);
                    } else {
                        etiqueta.prop('checked', false);
                    }
                } else {
                    $.jGrowl("No se pudo cargar los slider", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo cargar los slider"
                    });
                }
            },
            complete: function () { },
            error: function () {
                $.jGrowl("No se pudo cargar los slider", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "No se pudo cargar los slider"
                });
            }
        });
    }

    loadDataInitial();



});