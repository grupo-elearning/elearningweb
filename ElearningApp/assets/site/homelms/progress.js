﻿$(document).ready(function () {

    console.log(ngon);
    var porc_avance = 0;

    $("#avatar_id").attr("src",ngon.UrlImgUser);

    $('#cursos_index_table tbody').on('click', 'a[data-action=certificado]', function (evt) {
        evt.preventDefault();
        var l_register = $(this).attr("data-id"), l_this = $(this);
        let estado = $(this).attr("data-estado");
        console.log(l_register);
        if (estado != "Finalizado") {
            $.jGrowl("El Certificado no está disponible", {
                header: 'Advertencia',
                type: 'warning',
                clipboard: ""
            });
            return;
        }

        $.ajax({
            data: JSON.stringify({ Codi_Pins: l_register }),
            url: ngon.UrlApi_miprog_getCertificado,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: _beforeSend,
            success: _success,
            complete: _complete
        });
        function _beforeSend() {
            l_this.css('cssText', 'color: transparent !important;');
            gf_addLoadingTo(l_this);
        }
        function _success(res) {
            if (res.Status != undefined && res.Status == 200) {
                fn_programming_build_certificate(res.ListBody);
            } else {
                $.jGrowl("Error", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "Error"
                });
            }
        }
        function _complete() {
            l_this.css('cssText', '');
            gf_removeLoadingTo(l_this);
        }

    });

    l_dataTable_cursos = $("#cursos_index_table").daDataTable({
        "DataTable": {
            "aoColumns": [
                {
                    "className": "text-left h6"
                },
                {
                    "width": "70px",
                    "className": "text-center"
                },
                {
                    "width": "80px",
                    "className": "text-center"
                },
                {
                    "width": "90px"
                },
                {
                    "width": "90px",
                    "className": "text-center"
                },
                {
                    "width": "50px",
                    "className": "text-center"
                }
            ],
            "processing": true,
            "bLengthChange": false,
            "iDisplayLength": 10
        },
        "theme": "devexpress",
        "imgProcessing": "/assets/dalba/img/DXR.gif",
        "search": {
            "btn": { "column": 5 }
        }
    }).Datatable;

    l_dataTable_ranking = $("#ranking_index_table").daDataTable({
        "DataTable": {
            "aoColumns": [
                {
                    "width": "70px",
                    "className": "text-center h6"
                },
                {
                    "className": "text-left h6",
                    "style": "font-size: 15px;"
                },
                {
                    "width": "90px",
                    "className": "text-center"
                },
                {
                    "width": "80px",
                    "className": "text-center"
                },
                {
                    "width": "140px",
                    "className": "text-center h6"
                }
            ],
            "processing": true,
            "bLengthChange": false,
            "iDisplayLength": 10,
            "columnDefs": [
                { "targets": [2], "visible": false }
            ]
        },
        "theme": "devexpress",
        "imgProcessing": "/assets/dalba/img/DXR.gif",
        "search": {
            "btn": null,
            "noSearch": [0]
        }
    }).Datatable;

    $("#id_nombre").text(ngon.fullname);

    function loadDataInitial() {
        //Load Area
        $.ajax({
            data: JSON.stringify({ Ficha_Sap: ngon.ficha_sap}),
            url: ngon.UrlApi_miprog_getArea,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    $("#id_area").text(res.Body.Nombre_Area);
                } else {
                    $.jGrowl("No se pudo cargar el area", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo cargar el area"
                    });
                }
            },
            complete: function () { },
            error: function () {
                $.jGrowl("No se pudo cargar el area", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "No se pudo cargar el area"
                });
            }
        });

        //Load Ultimo Access
        $.ajax({
            data: JSON.stringify({ Codi_Sist: 2, Codi_User: ngon.user_name }),
            url: ngon.UrlApi_miprog_lastAccess,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    $("#id_ultimo").text(res.Body.Ultima_Fecha);
                } else {
                    $.jGrowl("No se pudo cargar el ultimo acceso", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo cargar el ultimo acceso"
                    });
                }
            },
            complete: function () { },
            error: function () {
                $.jGrowl("No se pudo cargar el ultimo acceso", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "No se pudo cargar el ultimo accesor"
                });
            }
        });

        //Load Lista Cursos
        $.ajax({
            data: JSON.stringify({ Codi_Pers: ngon.codi_pers }),
            url: ngon.UrlApi_miprog_listCursos,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    listarCursos(res.ListBody);
                } else {
                    $.jGrowl("No se pudo cargar los cursos", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo cargar los cursos"
                    });
                }
            },
            complete: function () { },
            error: function () {
                $.jGrowl("No se pudo cargar los cursos", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "No se pudo cargar los cursos"
                });
            }
        });

        //Load Lista Ranking
        $.ajax({
            data: '',
            url: ngon.UrlApi_miprog_listRanking,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    listarRanking(res.ListBody);
                } else {
                    $.jGrowl("No se pudo cargar el ranking", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo cargar el ranking"
                    });
                }
            },
            complete: function () { },
            error: function () {
                $.jGrowl("No se pudo cargar el ranking", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "No se pudo cargar el ranking"
                });
            }
        });

        //Load Identificar Insignia
        $.ajax({
            data: JSON.stringify({ Codi_Pers: ngon.codi_pers }),
            url: ngon.UrlApi_insign_identificar,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    listarIndenticarInsignia(res.Body);
                } else {
                    $.jGrowl("No se pudo cargar los cursos", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo cargar los cursos"
                    });
                }
            },
            complete: function () { },
            error: function () {
                $.jGrowl("No se pudo cargar los cursos", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "No se pudo cargar los cursos"
                });
            }
        });

    }

    function listarCursos(listaBox) {
        let trDOM;
        let cant_lista = listaBox.length;
        let cant_final = 0;
        l_dataTable_cursos.clear().draw();
        for (let i = 0; i < listaBox.length; i++) {
            //listaBox[i].Img
            //porc_avance = listaBox[i].Porc_Avance;
            if (listaBox[i].Estado === "Finalizado") {
                cant_final++;
            }
            trDOM = l_dataTable_cursos.row.add([
                listaBox[i].Nombre,
                `<h5 class="mb-0 mt-0">` + (listaBox[i].Estado === "Finalizado" ? `<span class="badge badge-success">Finalizado</span>` : `<span class="badge badge-warning">Iniciar</span>`) +`</h5>`,
                listaBox[i].Nota,
                listaBox[i].Fecha_Crea,
                listaBox[i].Fecha_Apro,
                `<td>
                    <a href="javascript:void(0)" class="btn btn-xs btn-raised btn-success m-0 cls-1810072234"  data-action="certificado" data-id="`+ listaBox[i].Codi_Pins + `" data-estado="` + listaBox[i].Estado +`" title="Certificado">
                        <i class="zmdi zmdi-file m-0"></i>
                    </a>
                </td>`
            ]).draw(false).node();

        }

        if (cant_lista > 0 && cant_final > 0) {
            porc_avance = Math.round(((cant_final * 100) / cant_lista));
        }

        updateAvance();
    }

    function listarRanking(listaBox) {
        let trDOM;
        l_dataTable_ranking.clear().draw();
        for (let i = 0; i < listaBox.length; i++) {
            let image_medal = ``;
            image_medal = `<img src="` + listaBox[i].File_Url + `" style="width: 30px;" title="` + listaBox[i].Nombre_Insginia +`"/>`;
            trDOM = l_dataTable_ranking.row.add([
                (i+1),
                listaBox[i].Nombre_Completo,
                listaBox[i].Cantidad,
                image_medal,
                listaBox[i].Puntos
            ]).draw(false).node();

        }
    }

    function listarIndenticarInsignia(object) {
        console.log(object);
        $("#img_insignia").attr("src", object.Url);
        $("#name_insignia").text(object.Nombre);
    }

    function updateAvance() {


        $("#id_progress").attr("aria-valuenow", porc_avance);
        $("#id_progress").css("width", porc_avance+"%");
        $("#id_progress").text(porc_avance+"%");
    }

    function fn_programming_build_certificate(p_data) {
        var l_template = p_data[4];
        //pdf
        //[orientation One of "portrait" or "landscape"(or shortcuts "p"(Default), "l")]
        //[unit Measurement unit to be used when coordinates are specified. One of "pt" (points), "mm" (Default), "cm", "in"]
        //[format One of 'a3', 'a4' (Default),'a5' ,'letter' ,'legal']
        var l_pdf = new jsPDF(l_template.sOrientation, "pt", l_template.sPageFormat);
        //add fonts
        //=====> ShiningTimes
        l_pdf.addFileToVFS(jspdfCustomfonts.ShiningTimes.file, jspdfCustomfonts.ShiningTimes.base64);
        l_pdf.addFont(jspdfCustomfonts.ShiningTimes.file, jspdfCustomfonts.ShiningTimes.name, "normal");
        //=====> SoDamnBeautiful
        l_pdf.addFileToVFS(jspdfCustomfonts.SoDamnBeautiful.file, jspdfCustomfonts.SoDamnBeautiful.base64);
        l_pdf.addFont(jspdfCustomfonts.SoDamnBeautiful.file, jspdfCustomfonts.SoDamnBeautiful.name, "normal");
        //=====> Aliquest
        l_pdf.addFileToVFS(jspdfCustomfonts.Aliquest.file, jspdfCustomfonts.Aliquest.base64);
        l_pdf.addFont(jspdfCustomfonts.Aliquest.file, jspdfCustomfonts.Aliquest.name, "normal");
        //=====> AnthoniSignature
        l_pdf.addFileToVFS(jspdfCustomfonts.AnthoniSignature.file, jspdfCustomfonts.AnthoniSignature.base64);
        l_pdf.addFont(jspdfCustomfonts.AnthoniSignature.file, jspdfCustomfonts.AnthoniSignature.name, "normal");
        //=====> Yaty
        l_pdf.addFileToVFS(jspdfCustomfonts.Yaty.file, jspdfCustomfonts.Yaty.base64);
        l_pdf.addFont(jspdfCustomfonts.Yaty.file, jspdfCustomfonts.Yaty.name, "normal");
        //=====> SilverstainSignature
        l_pdf.addFileToVFS(jspdfCustomfonts.SilverstainSignature.file, jspdfCustomfonts.SilverstainSignature.base64);
        l_pdf.addFont(jspdfCustomfonts.SilverstainSignature.file, jspdfCustomfonts.SilverstainSignature.name, "normal");
        //=====> Abecedary
        l_pdf.addFileToVFS(jspdfCustomfonts.Abecedary.file, jspdfCustomfonts.Abecedary.base64);
        l_pdf.addFont(jspdfCustomfonts.Abecedary.file, jspdfCustomfonts.Abecedary.name, "normal");
        //=====> Bintanghu
        l_pdf.addFileToVFS(jspdfCustomfonts.Bintanghu.file, jspdfCustomfonts.Bintanghu.base64);
        l_pdf.addFont(jspdfCustomfonts.Bintanghu.file, jspdfCustomfonts.Bintanghu.name, "normal");
        //=====> Hatachi
        l_pdf.addFileToVFS(jspdfCustomfonts.Hatachi.file, jspdfCustomfonts.Hatachi.base64);
        l_pdf.addFont(jspdfCustomfonts.Hatachi.file, jspdfCustomfonts.Hatachi.name, "normal");
        //add board
        l_pdf.addImage(l_template.oImage.sBase64, 'JPEG', 0, 0, l_template.oSize.fWidthPt, l_template.oSize.fHeightPt);
        //add children
        for (var i in l_template.oChildren) {
            if (l_template.oChildren[i].sType == 'image') {
                l_pdf.addImage(l_template.oChildren[i].oImage.sBase64, 'PNG', l_template.oChildren[i].oPosition.fLeftPt, l_template.oChildren[i].oPosition.fTopPt + 0.5, l_template.oChildren[i].oSize.fWidthPt, l_template.oChildren[i].oSize.fHeightPt);
            } else if (l_template.oChildren[i].sType == 'text') {
                l_pdf.setFont(l_template.oChildren[i].oFont.sFamily);
                l_pdf.setFontSize(l_template.oChildren[i].oFont.iSizePt);
                l_pdf.setFontStyle(l_template.oChildren[i].oFont.sStyle);
                l_pdf.setTextColor(l_template.oChildren[i].oFont.sColor);
                if (l_template.oChildren[i].oSize.bFullWidth) {
                    l_pdf.customText(fn_get_text_to_certificate(l_template.oChildren[i], p_data), { align: "center" }, l_template.oChildren[i].oPosition.fLeftPt, (l_template.oChildren[i].oPosition.fTopPt + l_template.oChildren[i].oSize.fHeightPt - 4));
                } else {
                    l_pdf.text(l_template.oChildren[i].oPosition.fLeftPt, (l_template.oChildren[i].oPosition.fTopPt + l_template.oChildren[i].oSize.fHeightPt - 4), fn_get_text_to_certificate(l_template.oChildren[i], p_data));
                }
            }
        }
        //save
        l_pdf.save('Constancia_' + p_data[1] + '.pdf');
        //fn 
        function fn_get_text_to_certificate(p_child, p_data) {
            if (p_child.oData.sKey == 'person_name') {
                return p_data[1];
            } else if (p_child.oData.sKey == 'course_name') {
                return p_data[0];
            } else if (p_child.oData.sKey == 'test_score') {
                return p_data[3].toString();
            } else if (p_child.oData.sKey == 'approved_date') {
                var l_date = moment(p_data[2], 'DD/MM/YYYY');
                if (p_child.oData.sDateFormat == 'dd/mm/yyyy') {
                    return l_date.format('DD/MM/YYYY');
                } else if (p_child.oData.sDateFormat == 'DIA de MES de AÑO') {
                    return l_date.format('DD') + ' de ' + gf_getNameMonth(l_date.format('MM')) + ' de ' + l_date.format('YYYY');
                } else {
                    return p_data[2];
                }
            } else {
                return '';
            }
        }
    }
    

    loadDataInitial();

    //jsPDF
    (function (API) {
        API.customText = function (txt, options, x, y) {
            options = options || {};
            /* Use the options align property to specify desired text alignment
             * Param x will be ignored if desired text alignment is 'center'.
             * Usage of options can easily extend the function to apply different text 
             * styles and sizes 
            */
            if (options.align == "center") {
                // Get current font size
                var fontSize = this.internal.getFontSize();
                // Get page width
                var pageWidth = this.internal.pageSize.width;
                // Get the actual text's width
                /* You multiply the unit width of your string by your font size and divide
                 * by the internal scale factor. The division is necessary
                 * for the case where you use units other than 'pt' in the constructor
                 * of jsPDF.
                */
                var txtWidth = this.getStringUnitWidth(txt) * fontSize / this.internal.scaleFactor;
                // Calculate text's x coordinate
                x = (pageWidth - txtWidth) / 2;
            }
            // Draw text at x,y
            this.text(txt, x, y);
        }
    })(jsPDF.API);

});