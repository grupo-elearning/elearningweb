﻿$(document).ready(function () {

    $("#id_mcursos").on("click", function () {
        console.log("Dando click");
    });

    loadDataInitial();

    function loadDataInitial() {
        //Load Tipo Filtro
        $.ajax({
            data: '',
            url: ngon.UrlApi_HomeLms_listSlider,
            headers: {
                'Authorization': ngon.token_access
            },
            type: 'post',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.Status != undefined && res.Status == 200) {
                    listarSlider(res.ListBody);
                } else {
                    $.jGrowl("No se pudo cargar los slider", {
                        header: 'Error',
                        type: 'danger',
                        clipboard: "No se pudo cargar los slider"
                    });
                }
            },
            complete: function () { },
            error: function () {
                $.jGrowl("No se pudo cargar los slider", {
                    header: 'Error',
                    type: 'danger',
                    clipboard: "No se pudo cargar los slider"
                });
            }
        });
    }

    function listarSlider(listaBox) {
        if (listaBox.length > 0) {
            $("#car_indicador").empty();
            $("#car_listbox").empty();
        }

        for (let i = 0; i < listaBox.length; i++) {
            
            let activo = "";
            let item = "";
            if (i == 0) {
                activo = `active`;
            }

            $("#car_indicador").append(`<li data-target="#carousel-home-magazine" data-slide-to="` + i + `" class="` + activo + `"></li>`);

            $("#car_listbox").append(`
                        <div class="carousel-item  ` + activo +`">
                            <img src="`+ listaBox[i].Img +`" class="img-fluid" alt="..." style="max-height: 620px; width: 100%;">
                            <div class="carousel-caption-blog">
                                <h2 class="color-primary">
                                    <a class="hover-light" href="#">`+ listaBox[i].Titulo +`</a>
                                </h2>
                                <p class="d-none d-md-block">
                                    `+ listaBox[i].Descripcion +`
                                </p>
                            </div>
                        </div>`);
                       
        }
    }

});