﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ElearningApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "IEBrowser",
                url: "IEBrowser/{id}",
                defaults: new { controller = "Error", action = "IEBrowser", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Login",
                url: "Login/{id}",
                defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "CarrierLogin",
                url: "ConductorAcceso/{id}",
                defaults: new { controller = "Account", action = "CarrierLogin", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Carrier",
                url: "Conductor/{id}",
                defaults: new { controller = "Home", action = "Carrier", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "VerifyYourEmail",
                url: "VerifyYourEmail/{code}",
                defaults: new { controller = "Account", action = "VerifyYourEmail", code = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Presentation",
                url: "Presentacion/{id}",
                defaults: new { controller = "Programming", action = "Presentation", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Classroom",
                url: "Aula/{id}",
                defaults: new { controller = "Programming", action = "Classroom", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}