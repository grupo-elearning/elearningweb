﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class ProgrammingController : Controller
    {
        private string l_storage_url_elearning = Helpers.GetAppSettings("Azure_StorageUrl_ELearning");
        private string l_current_course_name = "";
        // GET: Programming
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Create()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Edit(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            Programming_Entity l_model = null;
            var l_programming = l_list.RetrieveProgramming(l_id);
            bool l_noData = l_programming.Rows.Count == 0;
            if (!l_noData)
            {
                l_model = new Programming_Entity
                {
                    Id = Convert.ToDecimal(l_programming.Rows[0]["Codi_Pcur"]),
                    Edit = l_programming.Rows[0]["Editar"].ToString() == "S",
                    EditMessage = l_programming.Rows[0]["Mensaje"].ToString(),
                    Name = l_programming.Rows[0]["Nombre"].ToString(),
                    CourseId = Convert.ToDecimal(l_programming.Rows[0]["Codi_Curs"]),
                    ModeId = Convert.ToDecimal(l_programming.Rows[0]["Codi_Moda"]),
                    Vacancies = Convert.ToDecimal(l_programming.Rows[0]["Nro_Vacante"]),
                    HasTest = l_programming.Rows[0]["Flag_Examen"].ToString(),
                    TestId = Convert.ToDecimal(l_programming.Rows[0]["Codi_Exam"]),
                    Attempts = Convert.ToDecimal(l_programming.Rows[0]["Nro_Intentos"]),
                    HasInstructor = l_programming.Rows[0]["Flag_Instructor"].ToString(),
                    InstructorId = Convert.ToDecimal(l_programming.Rows[0]["Codi_Inst"]),
                    InstructorFullname = l_programming.Rows[0]["Instructor"].ToString(),
                    InstructorDocument = l_programming.Rows[0]["Tipo_Documento"].ToString(),
                    InstructorDocumentNumber = l_programming.Rows[0]["Nro_Doc"].ToString(),
                    PlaceId = Convert.ToDecimal(l_programming.Rows[0]["Codi_Luga"]),
                    PlaceDescription = l_programming.Rows[0]["Lugar_Otro"].ToString(),
                    UnlimitedRegistration = l_programming.Rows[0]["Flag_Ilimitado_Inscripcion"].ToString(),
                    RegistrationLimit = Convert.IsDBNull(l_programming.Rows[0]["Fecha_Fin_Inscripcion"]) ? DateTime.MinValue : Convert.ToDateTime(l_programming.Rows[0]["Fecha_Fin_Inscripcion"]),
                    ApprovalType = l_programming.Rows[0]["Flag_Modo_Aprobacion"].ToString(),
                    MinimumAssistance = Convert.ToDecimal(l_programming.Rows[0]["Nro_Asistencia"]),
                    ProgrammingType = l_programming.Rows[0]["Tipo_Programacion"].ToString(),
                    FromDate = Convert.IsDBNull(l_programming.Rows[0]["Fecha_Inicio"]) ? DateTime.MinValue : Convert.ToDateTime(l_programming.Rows[0]["Fecha_Inicio"]),
                    ToDate = Convert.IsDBNull(l_programming.Rows[0]["Fecha_Fin"]) ? DateTime.MinValue : Convert.ToDateTime(l_programming.Rows[0]["Fecha_Fin"]),
                    FromHour = l_programming.Rows[0]["Hora_Inicio"].ToString(),
                    ToHour = l_programming.Rows[0]["Hora_Fin"].ToString(),
                    Duration = Convert.ToDecimal(l_programming.Rows[0]["Duracion"]),
                    DurationScale = l_programming.Rows[0]["Escala_Duracion"].ToString(),
                    Interval = Convert.ToDecimal(l_programming.Rows[0]["Intervalo"]),
                    IntervalScale = l_programming.Rows[0]["Escala_Intervalo"].ToString(),
                    Weekend = l_programming.Rows[0]["Flag_Sabado_Domingo"].ToString(),
                    UnlimitedProgramming = l_programming.Rows[0]["Flag_Ilimitado_Programacion"].ToString(),
                    AccessType = l_programming.Rows[0]["Tipo_Acceso"].ToString(),
                    PersonType = l_programming.Rows[0]["Tipo_Persona"].ToString(),
                    AllArea = l_programming.Rows[0]["Flag_Todo_Area"].ToString(),
                    HasCertificate = l_programming.Rows[0]["Flag_Certificado"].ToString(),
                    HasPoll = l_programming.Rows[0]["Flag_Encuesta"].ToString(),
                    PollId = Convert.ToDecimal(l_programming.Rows[0]["Codi_Encu"]),
                    ShowContent = l_programming.Rows[0]["Flag_Mostrar_Contenido"].ToString(),
                    CertificateId = Convert.ToDecimal(l_programming.Rows[0]["Codi_Cons"]),
                    CapacitacionHours = Convert.ToDecimal(l_programming.Rows[0]["Capa_Horas"]),
                    CapacitacionFromHour = l_programming.Rows[0]["Capa_Hora_Inicio"].ToString(),
                    CapacitacionToHour = l_programming.Rows[0]["Capa_Hora_Fin"].ToString(),
                    Areas = RetrieveArea(l_id, l_list),
                    Persons = RetrievePerson(l_id, l_list),
                    Events = RetrieveEvent(l_id, l_list)
                };
            }

            Helpers.ClearDataTable(l_programming);

            l_list = null;

            ViewBag.NGon.Model = l_model;

            if (l_noData)
            {
                return View("NotFound");
            }
            else
            {
                return View(l_model);
            }
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Presentation(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            //validate status programming
            if (!l_list.GetStatusProgramming(l_id))
            {
                return View("IsInactive");
            }
            //validate access to presentation
            if (!l_list.GetAccessToPresentationStatus((int)SessionHelpers.GetUser().ProviderId, (int)SessionHelpers.GetUser().PersonId, l_id))
            {
                return View("NotPresentation");
            }
            //get presentation
            var l_course = l_list.GetPresentationCourse(l_id).Rows[0];
            var l_program = l_list.GetPresentationCourseProgramming(l_id).Rows[0];
            l_current_course_name = Convert.ToString(l_course["Curso"]);
            ProgrammingPresentation_Model l_model = new ProgrammingPresentation_Model
            {
                ProgrammingId = l_id,
                Course = new Course_Entity
                {
                    Id = Convert.ToDecimal(l_course["Codi_Curs"]),
                    Name = l_current_course_name,
                    Category = l_course["Categoria"].ToString(),
                    Area = l_course["Area"].ToString(),
                    Objetives = l_course["Objetivo"].ToString(),
                    Summary = l_course["Resumen"].ToString(),
                    FrontPage = l_course["Descripcion_Portada"].ToString(),
                    ImageUrl = l_storage_url_elearning + Helpers.DIRECTORY_COURSE_CONTENT
                                                            .Replace("{name}", l_current_course_name)
                                                            .Replace("{filename}", Convert.ToString(l_course["Url_Imagen"])),
                    Classification = l_course["Clasificacion"].ToString() == "T" ? "Transversal" : "Específico",
                    Publish = l_course["Flag_Publicado"].ToString(),
                    Themes = GetPresentationCourseTheme(Convert.ToInt32(l_course["Codi_Curs"]), l_list)
                },
                Programming = new Programming_Entity
                {
                    Mode = l_program["Modalidad"].ToString(),
                    InstructorFullname = l_program["Instructor"].ToString(),
                    HasTest = l_program["Examen"].ToString(),
                    PlaceDescription = l_program["Lugar"].ToString(),
                    sFromDate = l_program["Fecha_Inicio"].ToString(),
                    sToDate = l_program["Fecha_Fin"].ToString(),
                    FromHour = l_program["Hora_Inicio"].ToString(),
                    ToHour = l_program["Hora_Fin"].ToString(),
                    Weekend = l_program["Sabado_Domingo"].ToString(),
                    AccessType = l_program["Tipo_Acceso"].ToString(),
                    ApprovalType = l_program["Modo_Aprobacion"].ToString(),
                    sMinimumAssistance = l_program["Asistencia_Aprobacion"].ToString(),
                    sRegistrationLimit = l_program["Fin_Inscripcion"].ToString(),
                    ModeId = Convert.ToDecimal(l_program["Codi_Moda"]),
                    ProgrammingType = l_program["Tipo_Generacion"].ToString(),
                    ShowContent = l_program["Flag_Mostrar_Contenido"].ToString(),
                    HasCertificate = l_program["Certificado"].ToString(),
                    HasPoll = l_program["Encuesta"].ToString()
                },
                ModeElearning = Convert.ToDecimal(l_program["Codi_Moda_Elearning"]),
                ModeInPerson = Convert.ToDecimal(l_program["Codi_Moda_Presencial"]),
                Register = Convert.ToInt32(l_program["Flag_Inscribirse"]) == 1,
                NoRegisterMessage = l_program["Mensaje_No_Inscribirse"].ToString(),
                Initiated = Convert.ToInt32(l_program["Flag_Curso_Iniciado"]) == 1,
                InitiatedMessage = l_program["Mensaje_Curso_Iniciado"].ToString(),
                StorageUrl = l_storage_url_elearning + Helpers.DIRECTORY_COURSE_CONTENT
                                                            .Replace("{name}", l_current_course_name)
                                                            .Replace("{filename}", "")
            };

            l_model.Registered = l_list.GetRegisterStatus((int)SessionHelpers.GetUser().PersonId, l_id);

            l_list = null;

            ViewBag.NGon.Model = l_model;

            return View(l_model);
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Register(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            //validate status programming
            if (!l_list.GetStatusProgramming(l_id))
            {
                return View("IsInactive");
            }
            //get register
            Programming_Entity l_model = new Programming_Entity
            {
                Id = l_id,
                Events = GetEvent(l_id, Convert.ToInt32(SessionHelpers.GetUser().PersonId), 0, l_list)
            };

            ViewBag.NGon.Model = l_model;

            return View(l_model);
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult RegisterMassive(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            //validate status programming
            if (!l_list.GetStatusProgramming(l_id))
            {
                return View("IsInactive");
            }
            //get register massive
            Programming_Entity l_model = new Programming_Entity
            {
                Id = l_id,
                Events = GetEvent(l_id, 0, Convert.ToInt32(SessionHelpers.GetUser().ProviderId), l_list),
                Persons = GetRegisterMassiveEmployee(Convert.ToInt32(SessionHelpers.GetUser().ProviderId), l_list)
            };

            ViewBag.NGon.Model = l_model;

            return View(l_model);
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Classroom(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            Bus_Crud l_crud = new Bus_Crud();
            //validate status programming
            if (!l_list.GetStatusProgramming(l_id))
            {
                return View("IsInactive");
            }
            //get classroom
            var l_classroom = l_list.GetClassroom(l_id, Convert.ToInt32(SessionHelpers.GetUser().PersonId), RequestHelpers.GetClientIpAddress(HttpContext.Request));

            if (Convert.ToInt32(l_classroom.Rows[0]["ErrorCode"]) != 0)
            {
                ProgrammingClassroom_Model l_nmodel = new ProgrammingClassroom_Model
                {
                    ProgrammingId = l_id,
                    FromCarrierLogin = SessionHelpers.IsFromCarrierLogin(),
                    ErrorMessage = l_classroom.Rows[0]["ErrorMessage"].ToString(),
                    ErrorCode = Convert.ToInt32(l_classroom.Rows[0]["ErrorCode"]),
                    Details = GetNotClassroomInformation(l_id, Convert.ToInt32(SessionHelpers.GetUser().PersonId), Convert.ToInt32(l_classroom.Rows[0]["ErrorCode"]), l_list)
                };

                l_list = null;
                l_crud = null;

                ViewBag.NGon.Model = l_nmodel;

                return View("NotClassroom", l_nmodel);
            }

            ProgrammingClassroom_Model l_model = new ProgrammingClassroom_Model
            {
                ProgrammingId = l_id,
                TestId = Convert.ToDecimal(l_classroom.Rows[0]["Codi_Exam"]),
                PollId = Convert.ToDecimal(l_classroom.Rows[0]["Codi_Encu"]),
                IsCarrier = Convert.ToInt32(l_classroom.Rows[0]["Flag_Transportista"]) == 1,
                CurrentThemeItem = Convert.ToDecimal(l_classroom.Rows[0]["Codi_Cote"]),
                RegisterId = Convert.ToDecimal(l_classroom.Rows[0]["Codi_Pins"]),
                AttemptId = Convert.ToDecimal(l_classroom.Rows[0]["Codi_Exin"]),
                IsApproved = Convert.ToInt32(l_classroom.Rows[0]["Flag_Aprobado"]) == 1,
                HasCertificate = Convert.ToInt32(l_classroom.Rows[0]["Flag_Certificado"]) == 1,
                SecondsRemaining = Convert.ToInt64(l_classroom.Rows[0]["Segundos_Restantes"])
            };
            //  Get Course
            var l_course = l_list.GetClassroomCourse(Convert.ToInt32(l_classroom.Rows[0]["Codi_Curs"]));
            l_current_course_name = Convert.ToString(l_course.Rows[0]["Nombre"]);
            l_model.Course = new Course_Entity
            {
                Id = Convert.ToDecimal(l_course.Rows[0]["Codi_Curs"]),
                Name = l_current_course_name,
                ImageUrl = l_storage_url_elearning + Helpers.DIRECTORY_COURSE_CONTENT
                                                            .Replace("{name}", l_current_course_name)
                                                            .Replace("{filename}", Convert.ToString(l_course.Rows[0]["Url_Imagen"])),
                Themes = GetClassroomCourseTheme(Convert.ToInt32(l_course.Rows[0]["Codi_Curs"]), l_list)
            };
            if (l_model.TestId != 0)
            {
                //  Get Test
                var l_test = l_list.RetrieveTest(Convert.ToInt32(l_model.TestId));
                l_model.Test = new Test_Entity
                {
                    TestType = l_test.Rows[0]["Tipo_Examen"].ToString(),
                    Proceed = l_test.Rows[0]["Procede"].ToString(),
                    Time = l_test.Rows[0]["Tiempo"].ToString(),
                    TimeLimit = Convert.ToDecimal(l_test.Rows[0]["Cant_Tiempo"])
                };
                Helpers.ClearDataTable(l_test);
            }

            //  Insert Content Advance
            var l_content_advance = l_crud.InsertContentAdvance(Convert.ToInt32(l_classroom.Rows[0]["Codi_Pins"]), Convert.ToInt32(l_classroom.Rows[0]["Codi_Curs"]));
            Helpers.ClearDataTable(l_content_advance);

            Helpers.ClearDataTable(l_classroom);
            Helpers.ClearDataTable(l_course);

            l_list = null;
            l_crud = null;

            ViewBag.NGon.Model = l_model;

            return View(l_model);
        }
        [AppLog]
        public ActionResult Test(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            var l_test = l_list.GetClassroomTest(l_id);

            Test_Entity l_model = new Test_Entity
            {
                TestType = l_test.Rows[0]["Tipo_Examen"].ToString(),
                Proceed = l_test.Rows[0]["Procede"].ToString(),
                Time = l_test.Rows[0]["Tiempo"].ToString(),
                TimeLimit = Convert.ToDecimal(l_test.Rows[0]["Cant_Tiempo"]),
                CurrentTime = Convert.ToInt32(l_test.Rows[0]["Tiempo_Actual"]),
                AttemptId = Convert.ToDecimal(l_test.Rows[0]["Codi_Exin"]),
                Questions = GetClassroomTestQuestion(Convert.ToInt32(l_test.Rows[0]["Codi_Exin"]), l_list)
            };

            Helpers.ClearDataTable(l_test);

            l_list = null;

            ViewBag.NGon.Model = l_model;

            return PartialView("PartialView/_TestPartialView", l_model);
        }
        [AppLog]
        public ActionResult Poll(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            var l_poll = l_list.GetClassroomPoll(l_id);

            ClassroomPoll_Model l_model = new ClassroomPoll_Model
            {
                RegisterId = Convert.ToDecimal(l_id),
                Source = l_poll.Rows[0]["Origen"].ToString(),
                Id = Convert.ToDecimal(l_poll.Rows[0]["Id"]),
                HasComment = l_poll.Rows[0]["Flag_Comentario"].ToString(),
                CommentQuestion = l_poll.Rows[0]["Pregunta_Comentario"].ToString(),
                Comment = l_poll.Rows[0]["Comentario"].ToString(),
                Date = Convert.ToDateTime(l_poll.Rows[0]["Fecha_Realizado"]),
                Questions = GetClassroomPollQuestion(Convert.ToString(l_poll.Rows[0]["Origen"]), Convert.ToInt32(l_poll.Rows[0]["Id"]), l_list)
            };

            Helpers.ClearDataTable(l_poll);

            l_list = null;

            ViewBag.NGon.Model = l_model;

            return PartialView("PartialView/_PollPartialView", l_model);
        }
        public ActionResult EditRegisterMassive(string programming, string item)
        {
            int l_programming = Convert.ToInt32(Helpers.GetBase64Decode(programming));
            int l_item = Convert.ToInt32(Helpers.GetBase64Decode(item));
            Bus_List l_list = new Bus_List();
            Programming_Entity l_model = new Programming_Entity
            {
                Persons = GetRegisteredEmployeeInEvent(l_programming, l_item, Convert.ToInt32(SessionHelpers.GetUser().ProviderId), l_list)
            };

            ViewBag.NGon.Model = l_model;

            return PartialView("PartialView/_EditRegisterMassivePartialView");
        }
        public ActionResult ViewRegisterMassive(string programming, string item)
        {
            int l_programming = Convert.ToInt32(Helpers.GetBase64Decode(programming));
            int l_item = Convert.ToInt32(Helpers.GetBase64Decode(item));
            Bus_List l_list = new Bus_List();
            Programming_Entity l_model = new Programming_Entity
            {
                Persons = GetRegisteredEmployeeInEvent(l_programming, l_item, Convert.ToInt32(SessionHelpers.GetUser().ProviderId), l_list)
            };

            ViewBag.NGon.Model = l_model;

            return PartialView("PartialView/_ViewRegisterMassivePartialView");
        }
        public ActionResult IURegisterMassive(string programming)
        {
            int l_programming = Convert.ToInt32(Helpers.GetBase64Decode(programming));
            Bus_List l_list = new Bus_List();
            Programming_Entity l_model = new Programming_Entity
            {
                Persons = RetrieveRegisterMassive(Convert.ToInt32(SessionHelpers.GetUser().ProviderId), l_programming, l_list)
            };

            ViewBag.NGon.Model = l_model;

            return View("PartialView/_IURegisterMassivePartialView");
        }
        public ActionResult AttemptFinalized(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            var l_attempt = l_list.GetAttemptFinalized(l_id);
            string l_url, l_token = "";

            AttemptFinalized_Model l_model = new AttemptFinalized_Model
            {
                MessageFinalized = l_attempt.Rows[0]["Mensaje"].ToString(),
                Score = Convert.ToDecimal(l_attempt.Rows[0]["Nota"]),
                IsApproved = Convert.ToInt32(l_attempt.Rows[0]["Flag_Aprobado"]) == 1,
                HasCertificate = Convert.ToInt32(l_attempt.Rows[0]["Flag_Certificado"]) == 1,
                IsCarrier = Convert.ToInt32(l_attempt.Rows[0]["Flag_Transportista"]) == 1,
                FromCarrierLogin = SessionHelpers.IsFromCarrierLogin(),
                Test = new Test_Entity
                {
                    TestType = l_attempt.Rows[0]["Tipo_Examen"].ToString()
                },
                Parameter = new Parameter_Entity
                {
                    iWaitTimeCloseSession = Convert.ToInt32(l_attempt.Rows[0]["Transportista_Tiempo"]),
                    MessageApproved = l_attempt.Rows[0]["Mensaje_Aprobado"].ToString(),
                    MessageDisapproved = l_attempt.Rows[0]["Mensaje_Desaprobado"].ToString()
                }
            };

            if (l_model.Test.TestType == "T")
            {
                l_model.Questions = GetAttemptFinalizedQuestion(l_id, l_list);
            }

            if (l_model.IsCarrier && l_model.IsApproved)
            {
                var l_pers = l_list.GetDatosPersona(SessionHelpers.GetUser().PersonId);

                var l_api_data = new Capac_Notas();
                l_api_data.ApePaterno = l_pers.Rows[0]["AP_PATERNO"].ToString();
                l_api_data.ApeMaterno = l_pers.Rows[0]["AP_MATERNO"].ToString();
                l_api_data.Nombres = l_pers.Rows[0]["NOMBRES"].ToString();
                l_api_data.Licencia = l_pers.Rows[0]["NRO_LICENCIA"].ToString();

                l_api_data.NroDoc = SessionHelpers.GetUser().PersonDocumentNumber;
                l_api_data.Fecha = "01/01/2020";//Helpers.GetUTCDate().ToString("dd/MM/YYYY HH:mm");
                l_api_data.Nota = l_model.Score;
                l_api_data.Estado = l_model.IsApproved ? "A" : "D";

                l_url = WebConfigurationManager.AppSettings["URL_API_ELEARNING_LOCAL"].ToString();
                var my_jsondata_login = new
                {
                    Username = WebConfigurationManager.AppSettings["Usuario_local_elearning"].ToString(),
                    Password = WebConfigurationManager.AppSettings["Password_local_elearning"].ToString(),
                    usuario_clave = WebConfigurationManager.AppSettings["Usuario_local_elearning_clave"].ToString(),
                    password_clave = WebConfigurationManager.AppSettings["Password_local_elearning_clave"].ToString()
                };

                l_token = Helpers.executeAPI(my_jsondata_login, l_url + "/api/login/authenticate", "");
                l_token = l_token.Replace("\"", "");

                string l_return = Helpers.executeAPI(l_api_data, l_url + "/api/Insert/UpdCapacitacion", l_token);
                var json_data = JsonConvert.DeserializeObject<Object>(l_return);

                l_model.CarrierProtocol = Helpers.GetParameterByKey("PROTOCOLO_TRANSPORTISTA")
                    .Replace("{document_number}", SessionHelpers.GetUser().PersonDocumentNumber)
                    .Replace("{date_time}", Helpers.GetUTCDate().ToString("dd/MM/YYYY HH:mm"))
                    .Replace("{score}", Convert.ToString(l_model.Score))
                    .Replace("{status_approve}", l_model.IsApproved ? "A" : "D");
            }

            Helpers.ClearDataTable(l_attempt);
            l_list = null;

            ViewBag.NGon.Model = l_model;

            return PartialView("PartialView/_AttemptFinalizedPartialView", l_model);
        }
        [HttpPost]
        public JsonResult EditRegisterMassive(ProgrammingRegisterMassive_Entity p_Entity, string ScheduleDate, string ScheduleTime)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.ProviderId = SessionHelpers.GetUser().ProviderId;
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                var l_response = l_crud.DeleteProgrammingRegisterMassive(p_Entity);

                if (l_response.Rows[0]["Value"].ToString() == "1000" && l_response.Columns.Contains("Codi_Pers"))
                {
                    //send email
                    EmailHelpers.SendInformationRegisterMassive(l_response, ScheduleDate, ScheduleTime, Convert.ToInt32(p_Entity.ProgrammingId));
                }

                return Helpers.GetMessageCrud(l_response);
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public JsonResult RegisterMassive(ProgrammingRegisterMassive_Entity p_Entity, string ScheduleDate, string ScheduleTime)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.ProviderId = SessionHelpers.GetUser().ProviderId;
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                var l_response = l_crud.InsertProgrammingRegisterMassive(p_Entity);

                if (l_response.Rows[0]["Value"].ToString() == "1000" && l_response.Columns.Contains("Codi_Pers"))
                {
                    //send email
                    EmailHelpers.SendInformationRegisterMassive(l_response, ScheduleDate, ScheduleTime, Convert.ToInt32(p_Entity.ProgrammingId));
                }

                return Helpers.GetMessageCrud(l_response);
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public JsonResult Register(ProgrammingRegister_Entity p_Entity, string Schedule)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.PersonId = SessionHelpers.GetUser().PersonId;
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                var l_response = l_crud.InsertProgrammingRegister(p_Entity);

                if (l_response.Rows[0]["Value"].ToString() == "1000")
                {
                    //send email
                    EmailHelpers.SendInformationRegisterIndividual(Schedule);
                }

                return Helpers.GetMessageCrud(l_response);
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public JsonResult DeleteRegister(ProgrammingRegister_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.PersonId = SessionHelpers.GetUser().PersonId;
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.DeleteProgrammingRegister(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public JsonResult Create(Programming_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Id = 0;
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                var l_response = l_crud.InsertUpdateProgramming(p_Entity);
                if (p_Entity.AccessType == "V" && l_response.Rows[0]["Value"].ToString() == "1000")
                {
                    p_Entity.Id = Convert.ToDecimal(l_response.Rows[0]["Id"]);
                    //send email
                    if (SessionHelpers.GetProfile().AreaId == Convert.ToDecimal(Helpers.GetParameterByKey("CODI_AREA_ADMINISTRACION_SEG_EMPRESARIAL")))
                    {
                        EmailHelpers.SendNotificationRegisterPrivateCapacitacion(p_Entity);
                    }
                    else
                    {
                        EmailHelpers.SendNotificationRegisterPrivate(p_Entity);
                    }
                }

                if (l_response.Columns.Contains("Error"))
                {
                    var l_error = Convert.ToString(l_response.Rows[0]["Error"]);
                    if (l_error != "")
                    {
                        EmailHelpers.SendError(l_error);
                    }
                }

                return Helpers.GetMessageCrud(l_response);
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public JsonResult Edit(Programming_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                p_Entity.ProfilId = SessionHelpers.GetProfileId();
                var l_response = l_crud.InsertUpdateProgramming(p_Entity);
                if (p_Entity.AccessType == "V" && l_response.Rows[0]["Value"].ToString() == "1000")
                {
                    //send email
                    if (SessionHelpers.GetProfile().AreaId == Convert.ToDecimal(Helpers.GetParameterByKey("CODI_AREA_ADMINISTRACION_SEG_EMPRESARIAL")))
                    {
                        EmailHelpers.SendNotificationRegisterPrivateCapacitacion(p_Entity);
                    }
                    else
                    {
                        EmailHelpers.SendNotificationRegisterPrivate(p_Entity);
                    }
                }

                if (l_response.Columns.Contains("Error"))
                {
                    var l_error = Convert.ToString(l_response.Rows[0]["Error"]);
                    if (l_error != "")
                    {
                        EmailHelpers.SendError(l_error);
                    }
                }

                return Helpers.GetMessageCrud(l_response);
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public ActionResult Delete(Programming_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();

                var l_response = l_crud.DeleteProgramming(p_Entity);

                if (l_response.Columns.Contains("Error"))
                {
                    var l_error = Convert.ToString(l_response.Rows[0]["Error"]);
                    if (l_error != "")
                    {
                        EmailHelpers.SendError(l_error);
                    }
                }

                return Helpers.GetMessageCrud(l_response);
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public JsonResult GetList()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetProgrammingList(Convert.ToInt32(SessionHelpers.GetProfile().AreaId));
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pcur"],//0
                        row["Curso"],//1
                        row["Modalidad"],//2
                        row["Nro_Vacante"],//3
                        row["Lugar"],//4
                        Convert.IsDBNull(row["Fecha_Inicio"]) ? "" : Convert.ToDateTime(row["Fecha_Inicio"]).ToString("dd/MM/yyyy"),//5
                        Convert.IsDBNull(row["Fecha_Inicio"]) ? "" : Convert.ToDateTime(row["Fecha_Inicio"]).ToString("yyyyMMdd"),//6
                        Convert.IsDBNull(row["Fecha_Fin"]) ? "" : Convert.ToDateTime(row["Fecha_Fin"]).ToString("dd/MM/yyyy"),//7
                        Convert.IsDBNull(row["Fecha_Fin"]) ? "" : Convert.ToDateTime(row["Fecha_Fin"]).ToString("yyyyMMdd"),//8
                        row["Nombre"],//9
                        row["Estado"]//10
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult RefreshCalendarToRegister(int programming)
        {
            Bus_List l_list;
            try
            {
                l_list = new Bus_List();
                var l_records = GetEvent(programming, Convert.ToInt32(SessionHelpers.GetUser().PersonId), Convert.ToInt32(SessionHelpers.GetUser().ProviderId), l_list);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_records
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
            }
        }
        [HttpPost]
        public ActionResult Instructor(Programming_Model p_Model)
        {
            ViewBag.NGon.Model = p_Model;
            return PartialView("PartialView/_InstructorPartialView", p_Model);
        }
        [HttpPost]
        public ActionResult Person(Programming_Model p_Model)
        {
            ViewBag.NGon.Model = p_Model;
            return PartialView("PartialView/_PersonPartialView", p_Model);
        }
        [HttpPost]
        public ActionResult NewEvent()
        {
            return PartialView("PartialView/_NewEventPartialView");
        }
        [HttpPost]
        public ActionResult Area(Programming_Model p_Model)
        {
            ViewBag.NGon.Model = p_Model;
            return PartialView("PartialView/_AreaPartialView", p_Model);
        }
        [HttpPost]
        public ActionResult Provider(Programming_Model p_Model)
        {
            ViewBag.NGon.Model = p_Model;
            return PartialView("PartialView/_ProviderPartialView", p_Model);
        }
        [HttpPost]
        public ActionResult Question(Programming_Model p_Model)
        {
            ViewBag.NGon.Model = p_Model;
            return PartialView("PartialView/_QuestionPartialView", p_Model);
        }
        [HttpPost]
        public ActionResult PreviewPoll(Programming_Model p_Model)
        {
            Bus_List l_list = new Bus_List();
            var l_question = GetPollQuestion(Convert.ToInt32(p_Model.PollId), l_list);
            l_list = null;
            return PartialView("PartialView/_PreviewPollPartialView", l_question);
        }
        [HttpPost]
        public JsonResult GetPerson(Programming_Model p_Model)
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetPerson2Programming(p_Model);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pers"],//0
                        row["Tipo_Doc"],//1
                        row["Nro_Doc"],//2
                        row["Trabajador"],//3
                        row["Tipo"],//4
                        row["Razon_Social"],//5
                        row["Area"]//6
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetInstructor(Programming_Model p_Model)
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetInstructor2Programming(p_Model);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_inst"],//0
                        row["ABREVIATURA"],//1
                        row["NRO_DOC"],//2
                        row["instructor"],//3
                        row["tipo"]//4
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetArea(Programming_Model p_Model)
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetArea2Programming(p_Model);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_area"],//0
                        row["area"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetProvider(Programming_Model p_Model)
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetProvider2Programming(p_Model);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["ruc"],//0
                        row["razon_social"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetQuestion(Programming_Model p_Model)
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetQuestion2Programming(p_Model);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["PREGUNTA"],//0
                        row["TIPO_PREGUNTA"],//1
                        row["ORDEN"]//2
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetInTest(int id)
        {
            Bus_List l_list;
            try
            {
                l_list = new Bus_List();
                var l_status = l_list.GetInTest(id);
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_status
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
            }
        }
        [HttpPost]
        public JsonResult GetInInteractiveContent(int register, int content)
        {
            Bus_List l_list;
            try
            {
                l_list = new Bus_List();
                var l_status = l_list.GetInInteractiveContent(register, content);
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_status
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
            }
        }
        [HttpPost]
        public JsonResult GetNewAttemptValidate(int register)
        {
            Bus_List l_list;
            List<object> l_result = null;
            try
            {
                l_list = new Bus_List();
                var l_records = l_list.GetNewAttemptValidate(register);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result = new List<object>
                    {
                        row["ErrorCode"],//0
                        row["ErrorMessage"]//1
                    };
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetAttemptQuestion(int id)
        {
            Bus_List l_list;
            List<object> l_result = null;
            try
            {
                l_list = new Bus_List();
                var l_records = l_list.GetAttemptQuestion(id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result = new List<object>
                    {
                        row["Flag_Contestado"],//0
                        GetAttemptQuestionAnswer(id, l_list)//1
                    };
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult SaveAttemptQuestion(Attempt_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.UpdateAttemptQuestion(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public JsonResult SaveAttempt(Attempt_Entity p_Entity)
        {
            Bus_Crud l_crud;
            Bus_List l_list;
            try
            {
                l_crud = new Bus_Crud();
                l_list = new Bus_List();

                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                var l_response = l_crud.UpdateAttempt(p_Entity);

                /* Si el intento es de la charla del covid cosume la api */
                var l_return = l_list.GetPersonaPrivado(p_Entity.Id);
                decimal Codi_Pers = 0, Nota = 0, Nota_Aprob = 0;
                foreach (DataRow row in l_return.Rows)
                {
                    Codi_Pers = Convert.ToDecimal(row["CODI_PERS"].ToString());
                    Nota = Convert.ToDecimal(row["NOTA"].ToString());
                    Nota_Aprob = Convert.ToDecimal(row["NOTA_APRO"].ToString());
                }

                if (Codi_Pers != 0)
                {
                    var my_jsondata_login = new
                    {
                        Username = WebConfigurationManager.AppSettings["Usuario_api_local"].ToString(),
                        Password = WebConfigurationManager.AppSettings["Password_api_local"].ToString()
                    };
                    var l_url = WebConfigurationManager.AppSettings["URL_API_VISITA"].ToString();
                    //l_url = "http://localhost:60242/";
                    string l_token = Helpers.executeAPI(my_jsondata_login, l_url + "/api/login/authenticate", "");
                    l_token = l_token.Replace("\"", "");

                    var my_jsondata_data = new
                    {
                        CodiPers = Codi_Pers,
                        Nota = Nota,
                        NotaAprob = Nota_Aprob
                    };

                    string l_return_api = Helpers.executeAPI(my_jsondata_data, l_url + "/api/Local/AgregarProveedorContactoSustentaPrivado", l_token);
                    var json_data = JsonConvert.DeserializeObject<ResponseApi>(l_return_api);

                    /* Listar declaración jurada */
                    if (Nota >= Nota_Aprob)
                    {
                        List<object> l_result = new List<object>();

                        var l_allRecords = l_list.GetDeclaracionJurada(Codi_Pers);
                        foreach (DataRow row in l_allRecords.Rows)
                        {
                            l_result.Add(new List<object>
                                {
                                    row["HTML"],//0
                                    row["NOMBRES"],//1
                                    row["NRO_DOC"],//2
                                    row["EMPRESA"],//3
                                    (Convert.IsDBNull(row["FECHA"]) ? default(DateTime) : Convert.ToDateTime(row["FECHA"])).ToString("dd/MM/yyyy hh:mm:ss")//4
                                });
                        }

                        return new JsonResult()
                        {
                            Data = new
                            {
                                status = 5,
                                response = l_result
                            },
                            MaxJsonLength = int.MaxValue,
                            JsonRequestBehavior = JsonRequestBehavior.AllowGet
                        };
                    }
                }

                return Helpers.GetMessageCrud(l_response);
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public JsonResult SaveAttemptCurrentTime(Attempt_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.UpdateAttemptCurrentTime(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public JsonResult SaveVisit(int register, int content)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                return Helpers.GetMessageCrud(l_crud.InsertVisitContentAdvance(register, content));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public JsonResult GetClassroomCertificate(int id)
        {
            Bus_List l_list;
            List<object> l_result;
            AzureStorageHelpers l_storage = new AzureStorageHelpers("Azure_ContainerName_ELearning");
            try
            {
                l_list = new Bus_List();
                var l_validate = l_list.GetDownloadCertificateValidate(id);
                if (Convert.ToInt32(l_validate.Rows[0]["ErrorCode"]) != 0)
                {
                    var l_message = Convert.ToString(l_validate.Rows[0]["ErrorMessage"]);
                    Helpers.ClearDataTable(l_validate);
                    throw new Exception(l_message);
                }
                Helpers.ClearDataTable(l_validate);

                var l_record = l_list.GetClassroomCertificate(id);
                var l_certificate = l_record.Rows[0]["Constancia_Configuracion"].ToString();
                var l_json = JsonConvert.DeserializeObject<CertificateTemplate>(l_certificate);
                l_json.oImage.sBase64 = "data:image/jpeg;base64," + l_storage.GetBase64(Helpers.DIRECTORY_CERTIFICATE
                    .Replace("{filename}", l_json.sId));
                foreach (var l_child in l_json.oChildren)
                {
                    if (l_child.sType == "image")
                    {
                        l_child.oImage.sBase64 = "data:image/jpeg;base64," + l_storage.GetBase64(Helpers.DIRECTORY_CERTIFICATE
                            .Replace("{filename}", l_child.sId));
                    }
                }

                l_result = new List<object> {
                    l_record.Rows[0]["Curso"],//0
                    l_record.Rows[0]["Nombre"],//1
                    Convert.ToDateTime(l_record.Rows[0]["Fecha_Aprobado"]).ToString("dd/MM/yyyy"),//2
                    l_record.Rows[0]["Nota_Aprobado"],//3
                    l_json//4
                };

                Helpers.ClearDataTable(l_record);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = int.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
                l_storage.Clear();
                l_storage = null;
            }
        }
        [HttpPost]
        public JsonResult SaveClassroomLog(int register, int item = 0)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                var l_result = l_crud.SaveClassroomLog(register, item, Convert.ToInt32(SessionHelpers.GetSessionLog()));
                if (l_result.Columns.Contains("Item"))
                {
                    return Helpers.GetMessageCrud(l_result, l_result.Rows[0]["Item"]);
                }
                else
                {
                    return Helpers.GetMessageCrud(l_result);
                }
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public JsonResult SaveRegisterPoll(RegisterPoll_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.InsertRegisterPoll(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public JsonResult GetPreviewCertificate(Programming_Model p_Model)
        {
            Bus_List l_list;
            AzureStorageHelpers l_storage = null;
            try
            {
                l_list = new Bus_List();
                l_storage = new AzureStorageHelpers("Azure_ContainerName_ELearning");
                var l_certificate = l_list.RetrieveCertificate(Convert.ToInt32(p_Model.CertificateId));
                var l_certificate_JSON = l_certificate.Rows[0]["Configuracion"].ToString();
                var l_json = JsonConvert.DeserializeObject<CertificateTemplate>(l_certificate_JSON);
                l_json.oImage.sBase64 = "data:image/jpeg;base64," + l_storage.GetBase64(Helpers.DIRECTORY_CERTIFICATE
                    .Replace("{filename}", l_json.sId));
                foreach (var l_child in l_json.oChildren)
                {
                    if (l_child.sType == "image")
                    {
                        l_child.oImage.sBase64 = "data:image/jpeg;base64," + l_storage.GetBase64(Helpers.DIRECTORY_CERTIFICATE
                            .Replace("{filename}", l_child.sId));
                    }
                }

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_json
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
            }
        }
        [HttpPost]
        public JsonResult GetMaximumAttemptsToInteractiveValidate(int register, int content)
        {
            Bus_List l_list;
            try
            {
                l_list = new Bus_List();
                var l_status = l_list.GetMaximumAttemptsToInteractiveValidate(register, content);
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_status
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
            }
        }
        [HttpPost]
        public JsonResult GetCurrentAttemptToInteractive(int register, int content)
        {
            Bus_List l_list;
            try
            {
                l_list = new Bus_List();
                var l_value = l_list.GetCurrentAttemptToInteractive(register, content);
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_value
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
            }
        }

        [HttpPost]
        public JsonResult UpdateStatus(Programming_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.UpdateStatusProgramming(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public ActionResult MigrateToCapacitacion()
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                return Helpers.GetMessageCrud(l_crud.MigrateToCapacitacionProgrammingRegister());
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }

        //[HttpPost]
        //public JsonResult ApiActNotas(AttemptFinalized_Model p_Model)
        //{
        //    try
        //    {
        //        var t_token = GetAccessToken();
        //        t_token.Wait();
        //        var token = t_token.Result;

        //        var t_upd = UpdCapacitacion(p_Model.ApiData, token);
        //        t_upd.Wait();
        //        var result = t_upd.Result;

        //        return new JsonResult()
        //        {
        //            Data = new
        //            {
        //                status = result.Status,
        //                response = result.Message,
        //                exception = result.Exception
        //            },
        //            MaxJsonLength = Int32.MaxValue,
        //            JsonRequestBehavior = JsonRequestBehavior.AllowGet
        //        };
        //    }
        //    catch (Exception e)
        //    {
        //        return Helpers.GetException(e);
        //    }
        //}

        public async Task<string> GetAccessToken()
        {
            try
            {
                HttpClient _client = new HttpClient(new HttpClientHandler());

                var param = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("Username", ".*USUAELEA*"),
                    new KeyValuePair<string, string>("Password", "1.ELEAPASS.1"),
                    new KeyValuePair<string, string>("usuario_clave", "USUARIO_API_ELEARNING"),
                    new KeyValuePair<string, string>("password_clave", "PASS_API_ELEARNING")
                };

                var request = new HttpRequestMessage(HttpMethod.Post, "http://servicios-ti.siderperu.pe/ApiElearningLocal/api/login/authenticate")
                {
                    Content = new FormUrlEncodedContent(param)
                };

                var response = await _client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    return data.Replace("\"", "");
                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                throw;
            }
        }

        public async Task<ResponseApi> UpdCapacitacion(Capac_Notas model, string token)
        {
            try
            {
                HttpClient _client = new HttpClient(new HttpClientHandler());
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token);

                var json = JsonConvert.SerializeObject(model);
                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _client.PostAsync("http://servicios-ti.siderperu.pe/ApiElearningLocal/api/Insert/UpdCapacitacion", httpContent);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<ResponseApi>(content);
                }
                else { throw new Exception(); }
            }
            catch
            {
                throw;
            }
        }

        #region private
        private List<object> GetAttemptFinalizedQuestion(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.GetAttemptFinalizedQuestion(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Tipo"],//0
                        row["Pregunta"],//1
                        GetAttemptFinalizedQuestionAnswer(Convert.ToInt32(row["Codi_Prex"]), p_bus)//2
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetAttemptFinalizedQuestionAnswer(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.GetAttemptFinalizedQuestionAnswer(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Respuesta"]//0
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> RetrieveArea(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.RetrieveProgrammingArea(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Prar"],//0
                        row["Codi_Area"],//1
                        row["Area"]//2
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> RetrievePerson(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.RetrieveProgrammingPerson(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pins"],//0
                        row["Codi_Pers"],//1
                        row["Tipo_Doc"],//2
                        row["Nro_Doc"],//3
                        row["Trabajador"],//4
                        row["Tipo"],//5
                        row["Razon_Social"]//6
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> RetrieveEvent(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.RetrieveProgrammingEvent(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pcur"],//0
                        row["Item"],//1
                        Convert.ToDateTime(row["Fecha"]).ToString("dd/MM/yyyy"),//2
                        row["Hora_Inicio"],//3
                        row["Hora_Fin"],//4
                        row["Flag_Modificado"]//5
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetPresentationCourseTheme(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.GetPresentationCourseTheme(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Tecu"],//0
                        row["Descripcion"],//1
                        GetPresentationCourseThemeContent(Convert.ToInt32(row["Codi_Tecu"]), p_bus)//2
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetPresentationCourseThemeContent(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.GetPresentationCourseThemeContent(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Cote"],//0
                        row["Codi_Tcon"],//1
                        row["Titulo_Contenido"],//2
                        row["Url_Youtube"],//3
                        row["Texto"],//4
                        row["Nombre_Doc"],//5
                        row["Secuencia"],//6
                        row["Flag_Vista_Previa"],//7
                        row["Url_Canal_Sider"]//8
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetClassroomCourseTheme(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.GetClassroomCourseTheme(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Tecu"],//0
                        row["Descripcion"],//1
                        GetClassroomCourseThemeContent(Convert.ToInt32(row["Codi_Tecu"]), p_bus)//2
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetClassroomCourseThemeContent(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.GetClassroomCourseThemeContent(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Cote"],//0
                        row["Codi_Tcon"],//1
                        row["Titulo_Contenido"],//2
                        row["Url_Youtube"],//3
                        row["Texto"],//4
                        l_storage_url_elearning + Helpers.DIRECTORY_COURSE_CONTENT
                                                            .Replace("{name}", l_current_course_name)
                                                            .Replace("{filename}", Convert.ToString(row["Nombre_Doc"])),//5
                        row["Secuencia"],//6
                        row["Codi_Cuin"],//7
                        row["Nombre_Curso"],//8
                        row["Url_Curso"],//9
                        row["Url_Canal_Sider"]//10
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetClassroomTestQuestion(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            int l_tpre_completar;
            try
            {
                l_result = new List<object>();
                l_tpre_completar = Convert.ToInt32(Helpers.GetParameterByKey("CODI_TPRE_COMPLETAR"));
                var l_records = p_bus.GetClassroomTestQuestion(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Prex"],//0
                        row["Tipo"],//1
                        row["Flag_Actual"],//2
                        (Convert.ToInt32(row["Codi_Tpre"]) == l_tpre_completar ? Helpers.GetTestQuestion(row["Pregunta"].ToString()) : row["Pregunta"].ToString()).Replace("\n", "<br />"),//3
                        GetClassroomTestQuestionAnswer(Convert.ToInt32(row["Codi_Prex"]), p_bus)//4
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetClassroomTestQuestionAnswer(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.GetClassroomTestQuestionAnswer(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Prep"],//0
                        row["Respuesta"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetEvent(int p_programming, int p_person, int p_provider, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.ListProgrammingEvent(p_programming, p_person, p_provider);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pcur"],//0
                        row["Item"],//1
                        Convert.ToDateTime(row["Fecha"]).ToString("dd/MM/yyyy"),//2
                        row["Hora_Inicio"],//3
                        row["Hora_Fin"],//4
                        row["Nro_Vacantes"],//5
                        row["Inscritos"],//6
                        row["Flag"]//7
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetRegisterMassiveEmployee(int p_provider, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.GetEmployeeRegisterMassive(p_provider);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pers"],//0
                        row["Nombre"],//1
                        Convert.IsDBNull(row["Fecha_Ultima_Capa_Logi"]) ? "" : Convert.ToDateTime(row["Fecha_Ultima_Capa_Logi"]).ToString("dd/MM/yyyy"),//2
                        row["Suspendido"],//3
                        Convert.IsDBNull(row["Fecha_Ultima_Capa_Logi"]) ? "" : Convert.ToDateTime(row["Fecha_Ultima_Capa_Logi"]).ToString("yyyyMMdd"),//4
                        row["Persona_vulnerable"]//5
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> RetrieveRegisterMassive(int p_provider, int p_programming, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.RetrieveRegisterMassive(p_provider, p_programming);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pins"],//0
                        row["Codi_Pers"],//1
                        row["Nombre"]//2
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetRegisteredEmployeeInEvent(int p_programming, int p_item, int p_provider, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.GetRegisteredEmployeeInEvent(p_programming, p_item, p_provider);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pins"],//0
                        row["Codi_Pcur"],//1
                        row["Item"],//2
                        row["Nombre"]//3
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetAttemptQuestionAnswer(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.GetAttemptQuestionAnswer(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Prep"],//0
                        row["Orden_Seleccionado"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetNotClassroomInformation(int p_programming, int p_person, int p_errorcode, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.GetNotClassroomInformation(p_programming, p_person, p_errorcode);
                if (p_errorcode == 3)
                {
                    foreach (DataRow row in l_records.Rows)
                    {
                        l_result.Add(Convert.ToDateTime(row["Fecha_Inicio"]).ToString("dd/MM/yyyy"));//0
                        l_result.Add(Convert.ToDateTime(row["Fecha_Fin"]).ToString("dd/MM/yyyy"));//1
                    }
                }
                else
                {
                    foreach (DataRow row in l_records.Rows)
                    {
                        l_result.Add(new List<object>
                        {
                            Convert.ToDateTime(row["Fecha"]).ToString("dd/MM/yyyy"),//0
                            row["Hora_Inicio"],//1
                            row["Hora_Fin"],//2
                            Convert.ToDateTime(row["Fecha"]).ToString("yyyyMMdd")//3
                        });
                    }
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetClassroomPollQuestion(string p_source, int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.GetClassroomPollQuestion(p_source, p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Id"],//0
                        row["Pregunta"],//1
                        row["Orden"],//2
                        row["Evaluacion"],//3
                        row["Codi_Preg"]//4
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetPollQuestion(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.GetPollQuestion2Programming(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Orden"],//0
                        row["Pregunta"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        public JsonResult DeleteInscribed(decimal CodiPins)
        {
            Bus_Crud l_crud;
            var l_message = "";
            var l_code = "";
            var l_id = 0;
            try
            {
                l_crud = new Bus_Crud();
                var l_response = l_crud.DeleteInscribed(CodiPins);

                foreach (DataRow row in l_response.Rows)
                {
                    l_message = row["Mensaje"].ToString();
                    l_code = row["Value"].ToString();
                    l_id = Convert.ToInt32(row["ID"]);
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        status = l_code,
                        response = l_message,
                        id = l_id
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };

            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        #endregion private
    }
}
