﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class TestController : Controller
    {
        // GET: Test
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Create()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Edit(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            Test_Entity l_model = null;
            var l_test = l_list.RetrieveTest(l_id);
            bool l_noData = l_test.Rows.Count == 0;
            if (!l_noData)
            {
                l_model = new Test_Entity
                {
                    Id = l_id,
                    Name = Convert.ToString(l_test.Rows[0]["Descripcion"]),
                    CourseId = Convert.ToDecimal(l_test.Rows[0]["Codi_Curs"]),
                    Status = Convert.ToString(l_test.Rows[0]["Estado"]),
                    MinScore = Convert.ToDecimal(l_test.Rows[0]["Nota_Min"]),
                    MaxScore = Convert.ToDecimal(l_test.Rows[0]["Nota_Max"]),
                    ApproveScore = Convert.ToDecimal(l_test.Rows[0]["Nota_Apro"]),
                    TestType = Convert.ToString(l_test.Rows[0]["Tipo_Examen"]),
                    Proceed = Convert.ToString(l_test.Rows[0]["Procede"]),
                    Time = Convert.ToString(l_test.Rows[0]["Tiempo"]),
                    TimeLimit = Convert.ToDecimal(l_test.Rows[0]["Cant_Tiempo"]),
                    RandomQuestions = Convert.ToString(l_test.Rows[0]["Aleatorio"]),
                    QuestionsNumber = Convert.ToDecimal(l_test.Rows[0]["Cant_Pregunta"]),
                    Questions = RetrieveQuestion(l_id, l_list)
                };
            }

            Helpers.ClearDataTable(l_test);

            l_list = null;

            ViewBag.NGon.Model = l_model;

            if (l_noData)
            {
                return View("NotFound");
            }
            else
            {
                return View(l_model);
            }
        }
        [HttpPost]
        public ActionResult Question(Test_Model p_Model)
        {
            ViewBag.NGon.Model = p_Model;
            return PartialView("PartialView/_QuestionPartialView", p_Model);
        }
        [HttpPost]
        public JsonResult GetList()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetTestList(Convert.ToInt32(SessionHelpers.GetProfile().AreaId));
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Exam"],//0
                        row["Descripcion"],//1
                        row["Curso"],//2
                        row["Aleatorio"],//3
                        row["Rango"],//4
                        row["Tipo"],//5
                        row["Estado"]//6
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetQuestion(Test_Model p_Model)
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetListQuestion2Test(p_Model);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_preg"],//0
                        row["PREGUNTA"],//1
                        row["tipo"]//2
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public ActionResult Create(Test_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.InsertUpdateTest(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public ActionResult Edit(Test_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.InsertUpdateTest(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public ActionResult Delete(Test_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.DeleteTest(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        #region private
        private List<object> RetrieveQuestion(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.RetrieveTestQuestion(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Expr"],//0
                        row["Codi_Preg"],//1
                        row["Pregunta"],//2
                        row["Tipo"],//3
                        row["Orden"]//4
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        #endregion private
    }
}
