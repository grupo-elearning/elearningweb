﻿using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ElearningApp.Filters;
using ElearningApp.Models;
using Microsoft.WindowsAzure.Storage.Blob;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class ViewMicroAprendizajeController : Controller
    {
        // GET: ViewMicroAprendizaje
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            ViewBag.NGon.token_access = Helpers.LoginApiEleaLms();
            ViewBag.NGon.username = SessionHelpers.GetUsername();
            ViewBag.NGon.ipaddress = SessionHelpers.GetIPAddress();
            ViewBag.NGon.fullname = SessionHelpers.GetUser().FullName;
            ViewBag.NGon.perfil = SessionHelpers.GetProfile().Description.ToUpper();
            ViewBag.NGon.UrlApi_microa_list = System.Configuration.ConfigurationManager.AppSettings["UrlApi_microa_list"];
            ViewBag.NGon.UrlImgUser = System.Configuration.ConfigurationManager.AppSettings["UrlImgUser"];
            ViewBag.NGon.UrlImgUser = ViewBag.NGon.UrlImgUser + SessionHelpers.GetUser().EmployeeFichaSap + ".jpg";
            return View();
        }

        // GET: ViewMicroAprendizaje
        [AppAuthorize]
        [AppLog]
        public ActionResult Item(string id)
        {
            ViewBag.NGon.token_access = Helpers.LoginApiEleaLms();
            ViewBag.NGon.username = SessionHelpers.GetUsername();
            ViewBag.NGon.ipaddress = SessionHelpers.GetIPAddress();
            ViewBag.NGon.fullname = SessionHelpers.GetUser().FullName;
            ViewBag.NGon.perfil = SessionHelpers.GetProfile().Description.ToUpper();
            ViewBag.NGon.UrlApi_microa_id = System.Configuration.ConfigurationManager.AppSettings["UrlApi_microa_id"];
            ViewBag.NGon.UrlApi_HomeLms_saveInteraccion = System.Configuration.ConfigurationManager.AppSettings["UrlApi_HomeLms_saveInteraccion"];
            ViewBag.NGon.UrlImgUser = System.Configuration.ConfigurationManager.AppSettings["UrlImgUser"];
            ViewBag.NGon.UrlImgUser = ViewBag.NGon.UrlImgUser + SessionHelpers.GetUser().EmployeeFichaSap + ".jpg";
            ViewBag.NGon.Codi_Mcap = id;
            return View();
        }
    }
}