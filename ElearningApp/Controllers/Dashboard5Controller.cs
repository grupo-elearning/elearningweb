﻿using ClosedXML.Excel;
using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class Dashboard5Controller : Controller
    {
        // GET: Dashboard5
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetRegistered(int programming)
        {
            Bus_Report l_report;
            List<object> l_result;
            try
            {
                l_report = new Bus_Report();
                l_result = new List<object>();
                var l_records = l_report.Dashboard5GetRegistered(programming);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Ficha_Sap"],//0
                        row["Apellidos_Nombres"],//1
                        row["Nota_Aprobado"],//2
                        row["Area"],//3
                        Convert.IsDBNull(row["Fecha_Aprobado"]) ? "" : Convert.ToDateTime(row["Fecha_Aprobado"]).ToString("dd/MM/yyyy"),//4
                        Convert.IsDBNull(row["Fecha_Aprobado"]) ? "" : Convert.ToDateTime(row["Fecha_Aprobado"]).ToString("yyyyMMdd"),//5
                        row["Codi_Pins"],//6
                        row["Evaluado"],//7
                        row["Descargar_Constancia"]//8
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_report = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetExecuted(int course)
        {
            Bus_Report l_report;
            List<object> l_result;
            try
            {
                l_report = new Bus_Report();
                l_result = new List<object>();
                var l_records = l_report.Dashboard5GetExecuted(course);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Area"],//0
                        row["Area"],//1
                        row["Personal"],//2
                        row["Avance"],//3
                        row["Ejecutado"]//4
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_report = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetCertificate(int register)
        {
            Bus_Report l_report;
            List<object> l_result;
            AzureStorageHelpers l_storage = new AzureStorageHelpers("Azure_ContainerName_ELearning");
            try
            {
                l_report = new Bus_Report();
                var l_record = l_report.Dashboard5GetCertificate(register);
                var l_certificate = l_record.Rows[0]["Constancia_Configuracion"].ToString();
                var l_json = JsonConvert.DeserializeObject<CertificateTemplate>(l_certificate);
                l_json.oImage.sBase64 = "data:image/jpeg;base64," + l_storage.GetBase64(Helpers.DIRECTORY_CERTIFICATE
                    .Replace("{filename}", l_json.sId));
                foreach (var l_child in l_json.oChildren)
                {
                    if (l_child.sType == "image")
                    {
                        l_child.oImage.sBase64 = "data:image/jpeg;base64," + l_storage.GetBase64(Helpers.DIRECTORY_CERTIFICATE
                            .Replace("{filename}", l_child.sId));
                    }
                }

                l_result = new List<object> {
                    l_record.Rows[0]["Curso"],//0
                    l_record.Rows[0]["Nombre"],//1
                    Convert.ToDateTime(l_record.Rows[0]["Fecha_Aprobado"]).ToString("dd/MM/yyyy"),//2
                    l_record.Rows[0]["Nota_Aprobado"],//3
                    l_json//4
                };

                Helpers.ClearDataTable(l_record);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = int.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_report = null;
                l_result = null;
                l_storage.Clear();
                l_storage = null;
            }
        }
        [HttpPost]
        public JsonResult ExecutedExportToExcel(Dashboard5ExecutedExport_Model p_Model)
        {
            string l_name = "Excel-" + SessionHelpers.GetUsername() + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";
            string l_path = Path.Combine(Server.MapPath("~/temp/"), l_name);
            try
            {
                using (XLWorkbook wb = new XLWorkbook())
                using (IXLWorksheet ws = wb.Worksheets.Add("Dashboard5"))
                {
                    //head
                    ws.Cell("A1").Value = "Área";
                    ws.Cell("B1").Value = "Personal";
                    ws.Cell("C1").Value = "Avance";
                    ws.Cell("D1").Value = "Ejecutado";
                    ws.Cell("E1").Value = "Programado";
                    ws.Cell("F1").Value = "Realizado";
                    ws.Cell("G1").Value = "Ejecutado";
                    //body
                    for (int i = 0; i < p_Model.Area.Length; i++)
                    {
                        ws.Cell("A" + (i + 2)).Value = p_Model.Area[i];
                        ws.Cell("B" + (i + 2)).Value = p_Model.Staff[i];
                        ws.Cell("C" + (i + 2)).Value = p_Model.Advance[i];
                        ws.Cell("D" + (i + 2)).Value = Convert.ToString(p_Model.Executed[i]) + " %";
                        ws.Cell("D" + (i + 2)).Style
                            .Fill.SetBackgroundColor(GetStatusColor(p_Model.Executed[i]));
                    }
                    //avg
                    ws.Range("E2:E" + Convert.ToString(p_Model.Area.Length + 1)).Merge().Value = p_Model.AVGProgrammed;
                    ws.Range("F2:F" + Convert.ToString(p_Model.Area.Length + 1)).Merge().Value = p_Model.AVGCompleted;
                    ws.Range("G2:G" + Convert.ToString(p_Model.Area.Length + 1)).Merge().Value = Convert.ToString(p_Model.AVGExecuted) + " %";
                    //totals
                    ws.Cell("B" + Convert.ToString(p_Model.Area.Length + 2)).Value = p_Model.Staff.Sum();
                    ws.Cell("C" + Convert.ToString(p_Model.Area.Length + 2)).Value = p_Model.Advance.Sum();
                    ws.Cell("D" + Convert.ToString(p_Model.Area.Length + 2)).Value = Convert.ToString(p_Model.AVGExecuted) + " %";
                    ws.Cell("E" + Convert.ToString(p_Model.Area.Length + 2)).Value = p_Model.AVGProgrammed;
                    ws.Cell("F" + Convert.ToString(p_Model.Area.Length + 2)).Value = p_Model.AVGCompleted;
                    //style
                    ws.Range("A1:G1").Style.Font.SetBold();
                    ws.Range("D2:D" + Convert.ToString(p_Model.Area.Length + 1)).Style
                            .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    ws.Range("E2:G" + Convert.ToString(p_Model.Area.Length + 1)).Style
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                        .Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                    ws.Range("A1:G" + Convert.ToString(p_Model.Area.Length + 1)).Style
                        .Border.InsideBorder = XLBorderStyleValues.Thin;
                    ws.Range("A1:G" + Convert.ToString(p_Model.Area.Length + 1)).Style
                        .Border.OutsideBorder = XLBorderStyleValues.Thin;
                    var l_range_totals = ws.Range("B" + Convert.ToString(p_Model.Area.Length + 2) + ":F" + Convert.ToString(p_Model.Area.Length + 2));
                    l_range_totals.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                    l_range_totals.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    l_range_totals.Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    //
                    ws.RangeUsed().SetAutoFilter();
                    ws.Columns().AdjustToContents();
                    wb.SaveAs(l_path);
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = new { name = l_name }
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
        }
        #region private
        private XLColor GetStatusColor(decimal executed)
        {
            return executed == 0 ? XLColor.FromArgb(255, 97, 84) : executed == 100 ? XLColor.FromArgb(87, 205, 31) : XLColor.FromArgb(255, 231, 0);
        }
        #endregion private


        [HttpPost]
        public JsonResult UpdateNote(Upd_Note_Entity p_Entity)
        {
            var l_code = 0;
            var l_message = "";

            Bus_Crud l_crud;
            Bus_List l_list;
            try
            {
                l_crud = new Bus_Crud();
                l_list = new Bus_List();

                var l_dt_prog = l_list.GetProgInscrito(p_Entity.CodiPins);
                var l_codi_pers = Convert.ToInt32(l_dt_prog.Rows[0]["CODI_PERS"].ToString());
                var l_codi_pcur = Convert.ToInt32(l_dt_prog.Rows[0]["CODI_PCUR"].ToString());

                var l_dt_cod_tema = l_list.GetCodigoTema(l_codi_pcur);
                var l_codi_cote = Convert.ToInt32(l_dt_cod_tema.Rows[0]["CODI_COTE"].ToString());

                //var l_dt_classroom = l_list.GetClassroom(l_codi_pcur, l_codi_pers, "::1");
                //var l_codi_error = Convert.ToInt32(l_dt_classroom.Rows[0]["ErrorCode"].ToString());

                string token = "";

                //Ejecutar API - Authenticate
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://webprogcursoapi.azurewebsites.net/api/login/authenticate");
                    var p_api_auth = new Api_Auth_Entity
                    {
                        Username = "test123",
                        Password = "123456",
                    };

                    var postTask = client.PostAsJsonAsync<Api_Auth_Entity>("", p_api_auth);
                    postTask.Wait();

                    var result = postTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<string>();
                        readTask.Wait();
                        token = readTask.Result;
                    }
                    else
                    {
                        l_code = 3000;
                        l_message = "Hubo un error al conectarse a la API.";
                    }
                }

                //Ejecutar API - Update Score
                if (token == null || token == "")
                {
                    l_code = 3000;
                    l_message = "Token no es válido.";
                }
                else
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri("https://webprogcursoapi.azurewebsites.net/api/interactives/add");
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(token);

                        var l_param_api = new Api_Note_Entity
                        {
                            RegisterId = p_Entity.CodiPins,
                            ContentId = l_codi_cote,
                            CurrentAttempt = 2,//Intentos
                            Score = p_Entity.Nota,
                            FromDate = DateTime.Now,
                            ToDate = DateTime.Now
                        };

                        var postTask = client.PostAsJsonAsync<Api_Note_Entity>("", l_param_api);
                        postTask.Wait();

                        var result = postTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<object>();
                            readTask.Wait();
                            var obj = readTask.Result;
                            l_code = 1000;
                            l_message = "La nota se actualizó correctamente.";
                            //var readTask = result.Content.ReadAsAsync<string>();
                            //readTask.Wait();
                            //token = readTask.Result;
                        }
                        else
                        {
                            l_code = 3000;
                            l_message = "No se pudo actualizar la Nota.";
                        }
                    }
                }

                return new JsonResult()
                {
                    Data = new
                    {
                        status = l_code,
                        response = l_message,
                        exception = l_message,
                        others = 0
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }

        [HttpPost]
        public JsonResult SendRecordatorio(Send_Recordatorio_Entity p_Entity)
        {
            var l_code = 0;
            var l_message = "";

            Bus_Crud l_crud;
            Bus_List l_list;
            try
            {
                l_crud = new Bus_Crud();
                l_list = new Bus_List();

                var l_prog = l_list.GetProgramming(p_Entity.ProgrammingId);

                var l_programming = new Programming_Entity();
                l_programming.Id = p_Entity.ProgrammingId;
                l_programming.CourseId = Convert.ToDecimal(l_prog.Rows[0]["CODI_CURS"]);
                l_programming.FromDate = Convert.ToDateTime(l_prog.Rows[0]["FECHA_INICIO"] == DBNull.Value ? "1900-01-01" : l_prog.Rows[0]["FECHA_INICIO"]);
                l_programming.ToDate = Convert.ToDateTime(l_prog.Rows[0]["FECHA_FIN"] == DBNull.Value ? "1900-01-01" : l_prog.Rows[0]["FECHA_FIN"]);
                l_programming.UnlimitedProgramming = Convert.ToString(l_prog.Rows[0]["FLAG_ILIMITADO_PROGRAMACION"]);
                l_programming.ModeId = Convert.ToInt32(l_prog.Rows[0]["CODI_MODA"]);
                l_programming.FromHour = Convert.ToString(l_prog.Rows[0]["HORA_INICIO"]);
                l_programming.ToHour = Convert.ToString(l_prog.Rows[0]["HORA_FIN"]);
                l_programming.PlaceId = Convert.ToInt32(l_prog.Rows[0]["CODI_LUGA"]);
                l_programming.PlaceDescription = Convert.ToString(l_prog.Rows[0]["LUGAR_OTRO"]);

                var dt_fichas = new DataTable();
                dt_fichas.Columns.Add("FICHA_SAP");
                DataRow l_row;
                foreach (var l_ficha in p_Entity.Fichas)
                {
                    l_row = dt_fichas.NewRow();
                    l_row["FICHA_SAP"] = l_ficha;
                    dt_fichas.Rows.Add(l_row);
                    l_row = null;
                }

                if (SessionHelpers.GetProfile().AreaId == Convert.ToDecimal(Helpers.GetParameterByKey("CODI_AREA_ADMINISTRACION_SEG_EMPRESARIAL")))
                {
                    EmailHelpers.SendNotificationRegisterPrivateCapacitacionCheck(l_programming, dt_fichas);
                }
                else
                {
                    EmailHelpers.SendNotificationRegisterPrivateCheck(l_programming, dt_fichas);
                }

                l_code = 1000;
                l_message = "Recordatorio enviado correctamente";

                return new JsonResult()
                {
                    Data = new
                    {
                        status = l_code,
                        response = l_message
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
    }
}