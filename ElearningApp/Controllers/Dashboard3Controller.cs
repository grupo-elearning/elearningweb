﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class Dashboard3Controller : Controller
    {
        // GET: Dashboard3
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ParticipantDetail(int id)
        {
            Bus_Report l_report = new Bus_Report();
            var l_model = new Dashboard3ParticipantModel
            {
                Attempts = ConvertToDashboard3ParticipantAttemptModelList(l_report.Dashboard3GetParticipantAttempt(id), l_report).ToList()
            };
            return PartialView("PartialView/_ParticipantDetailPartialView", l_model);
        }
        [HttpPost]
        public JsonResult GetDashboard3(int provider, DateTime from_date, DateTime to_date)
        {
            Bus_Report l_report;
            List<object> l_result;
            try
            {
                l_report = new Bus_Report();
                l_result = new List<object>
                {
                    GetFulfillment(provider, from_date, to_date, l_report),//0
                    GetStatusParticipants(provider, from_date, to_date, l_report),//1
                    GetSchedule(provider, from_date, to_date, l_report)//2
                };

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_report = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetStatusParticipantsDetail(string approved, int provider, DateTime from_date, DateTime to_date)
        {
            Bus_Report l_report;
            List<object> l_result;
            try
            {
                l_report = new Bus_Report();
                l_result = new List<object>();
                var l_records = l_report.Dashboard3GetStatusParticipantsDetail(Convert.ToInt32(SessionHelpers.GetProfile().AreaId), approved, provider, from_date, to_date);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        Convert.ToDateTime(row["Fecha"]).ToString("dd/MM/yyyy"),//0
                        row["Hora_Inicio"],//1
                        row["Hora_Fin"],//2
                        row["Transportista"],//3
                        row["Conductor"],//4
                        row["Estatus"],//5
                        row["Nota"],//6
                        Convert.ToDateTime(row["Fecha"]).ToString("yyyyMMdd"),//7
                        row["Codi_Pins"],//8
                        row["CODIGO_SAP"],//9
                        row["NRO_DOC"]//10
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_report = null;
                l_result = null;
            }
        }
        #region private
        private IEnumerable<Dashboard3ParticipantAttemptModel> ConvertToDashboard3ParticipantAttemptModelList(DataTable p_dt, Bus_Report p_bus)
        {
            return p_dt.AsEnumerable().Select(row => new Dashboard3ParticipantAttemptModel
            {
                Codi_Exin = Convert.ToInt32(row["Codi_Exin"]),
                Intento = Convert.IsDBNull(row["Intento"]) ? 0 : Convert.ToDecimal(row["Intento"]),
                Nota = Convert.IsDBNull(row["Nota"]) ? 0 : Convert.ToDecimal(row["Nota"]),
                Flag_Aprobado = Convert.IsDBNull(row["Flag_Aprobado"]) ? "" : Convert.ToString(row["Flag_Aprobado"]),
                Questions = ConvertToDashboard3ParticipantQuestionModelList(p_bus.Dashboard3GetParticipantQuestion(Convert.ToInt32(row["Codi_Exin"])), p_bus).ToList()
            });
        }
        private IEnumerable<Dashboard3ParticipantQuestionModel> ConvertToDashboard3ParticipantQuestionModelList(DataTable p_dt, Bus_Report p_bus)
        {
            return p_dt.AsEnumerable().Select(row => new Dashboard3ParticipantQuestionModel
            {
                Pregunta = Convert.IsDBNull(row["Pregunta"]) ? "" : Convert.ToString(row["Pregunta"]),
                Flag_Contestado = Convert.IsDBNull(row["Flag_Contestado"]) ? "" : Convert.ToString(row["Flag_Contestado"]),
                Flag_Actual = Convert.IsDBNull(row["Flag_Actual"]) ? "" : Convert.ToString(row["Flag_Actual"]),
                Tipo_Pregunta = Convert.IsDBNull(row["Tipo_Pregunta"]) ? "" : Convert.ToString(row["Tipo_Pregunta"]),
                Estado = Convert.IsDBNull(row["Estado"]) ? "" : Convert.ToString(row["Estado"]),
                Answers = ConvertToDashboard3ParticipantAnswerModelList(p_bus.Dashboard3GetParticipantAnswer(Convert.ToInt32(row["Codi_Prex"]))).ToList()
            });
        }
        private IEnumerable<Dashboard3ParticipantAnswerModel> ConvertToDashboard3ParticipantAnswerModelList(DataTable p_dt)
        {
            return p_dt.AsEnumerable().Select(row => new Dashboard3ParticipantAnswerModel
            {
                Respuesta = Convert.IsDBNull(row["Respuesta"]) ? "" : Convert.ToString(row["Respuesta"]),
                Correcta = Convert.IsDBNull(row["Correcta"]) ? "" : Convert.ToString(row["Correcta"]),
                Orden = Convert.IsDBNull(row["Orden"]) ? 0 : Convert.ToDecimal(row["Orden"]),
                Seleccionado = Convert.IsDBNull(row["Seleccionado"]) ? "" : Convert.ToString(row["Seleccionado"]),
                Orden_Seleccionado = Convert.IsDBNull(row["Orden_Seleccionado"]) ? 0 : Convert.ToDecimal(row["Orden_Seleccionado"])
            });
        }
        private List<object> GetFulfillment(int p_provider, DateTime p_from_date, DateTime p_to_date, Bus_Report p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.Dashboard3GetFulfillment(Convert.ToInt32(SessionHelpers.GetProfile().AreaId), p_provider, p_from_date, p_to_date);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pcur"],//0
                        row["Programacion"],//1
                        row["Programados"],//2
                        row["Asistencias"]//3
                    });
                }
                Helpers.ClearDataTable(l_records);
                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetStatusParticipants(int p_provider, DateTime p_from_date, DateTime p_to_date, Bus_Report p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.Dashboard3GetStatusParticipants(Convert.ToInt32(SessionHelpers.GetProfile().AreaId), p_provider, p_from_date, p_to_date);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Descripcion"],//0
                        row["Total"]//1
                    });
                }
                Helpers.ClearDataTable(l_records);
                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetSchedule(int p_provider, DateTime p_from_date, DateTime p_to_date, Bus_Report p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.Dashboard3GetSchedule(Convert.ToInt32(SessionHelpers.GetProfile().AreaId), p_provider, p_from_date, p_to_date);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Hora"],//0
                        row["Turno"]//1
                    });
                }
                Helpers.ClearDataTable(l_records);
                return l_result;
            }
            catch
            {
                throw;
            }
        }
        #endregion private
    }
}