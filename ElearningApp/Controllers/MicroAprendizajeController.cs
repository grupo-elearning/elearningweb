﻿using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ElearningApp.Filters;
using ElearningApp.Models;
using Microsoft.WindowsAzure.Storage.Blob;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class MicroAprendizajeController : Controller
    {
        // GET: MicroAprendizaje
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            ViewBag.NGon.token_access = Helpers.LoginApiEleaLms();
            ViewBag.NGon.username = SessionHelpers.GetUsername();
            ViewBag.NGon.ipaddress = SessionHelpers.GetIPAddress();
            ViewBag.NGon.UrlApi_microa_list = System.Configuration.ConfigurationManager.AppSettings["UrlApi_microa_list"];
            ViewBag.NGon.UrlApi_microa_delete = System.Configuration.ConfigurationManager.AppSettings["UrlApi_microa_delete"];
            ViewBag.NGon.UrlApi_microa_updStatus = System.Configuration.ConfigurationManager.AppSettings["UrlApi_microa_updStatus"];
            ViewBag.NGon.UrlApi_microa_updDate = System.Configuration.ConfigurationManager.AppSettings["UrlApi_microa_updDate"];
            return View();
        }

        [AppAuthorize]
        [AppLog]
        public ActionResult Add()
        {
            ViewBag.NGon.token_access = Helpers.LoginApiEleaLms();
            ViewBag.NGon.username = SessionHelpers.GetUsername();
            ViewBag.NGon.ipaddress = SessionHelpers.GetIPAddress();
            ViewBag.NGon.UrlApi_microa_saveUpd = System.Configuration.ConfigurationManager.AppSettings["UrlApi_microa_saveUpd"];
            return View();
        }

        [AppAuthorize]
        [AppLog]
        public ActionResult Edit(string id)
        {
            ViewBag.NGon.token_access = Helpers.LoginApiEleaLms();
            ViewBag.NGon.username = SessionHelpers.GetUsername();
            ViewBag.NGon.ipaddress = SessionHelpers.GetIPAddress();
            ViewBag.NGon.UrlApi_microa_id = System.Configuration.ConfigurationManager.AppSettings["UrlApi_microa_id"];
            ViewBag.NGon.UrlApi_microa_saveUpd = System.Configuration.ConfigurationManager.AppSettings["UrlApi_microa_saveUpd"];
            ViewBag.NGon.Codi_Mcap = id;
            return View();
        }

    }
}