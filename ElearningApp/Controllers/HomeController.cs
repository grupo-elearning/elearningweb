﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        //[AppIEBrowser]
        [AppLog]
        [AppNoCache]

        public ActionResult Index()
        {
            Bus_List l_list = new Bus_List();
            HomeIndex_Model l_model = new HomeIndex_Model
            {
                Courses = MyCoursesToIEnumerable(l_list.GetMyCourses2Home(new Home_Model
                {
                    LoggedIn = SessionHelpers.IsAuthenticated() ? "S" : "N",
                    PersonId = SessionHelpers.IsAuthenticated() ? SessionHelpers.GetUser().PersonId : 0,
                    ProviderId = SessionHelpers.IsAuthenticated() ? SessionHelpers.GetUser().ProviderId : 0,
                })),
                Notices = MyNoticesToIEnumerable(l_list.GetMyNotices2Home(new Home_Model
                {
                    PersonId = SessionHelpers.IsAuthenticated() ? SessionHelpers.GetUser().PersonId : 0,
                    ProviderId = SessionHelpers.IsAuthenticated() ? SessionHelpers.GetUser().ProviderId : 0,
                    ProfileId = SessionHelpers.IsAuthenticated() ? SessionHelpers.GetProfileId() : 0
                })),
                FromLogistica = SessionHelpers.IsAuthenticated() ? SessionHelpers.IsFromLogistica() : false,
                IsPerson = SessionHelpers.IsAuthenticated() ? SessionHelpers.GetUser().PersonId != 0 : false,
                IsProvider = SessionHelpers.IsAuthenticated() ? SessionHelpers.GetUser().ProviderId != 0 : false
            };
            //if (TempData["FromLogin"] != null && SessionHelpers.IsAuthenticated() && SessionHelpers.IsFromLogistica() && SessionHelpers.GetUser().PersonId != 0)
            //{
            //    //remove temporal data
            //    TempData.Remove("FromLogin");
            //    //
            //    var l_result = l_model.Courses.Where(c => (c.RegisterMode == "R") && c.Registered == 1 && c.Initiated == 1).Select(c => c.ProgrammingId).ToList();
            //    if (l_result.Count == 1)
            //    {
            //        return RedirectToAction("Classroom", "Programming", new { id = Helpers.GetBase64Encode(Convert.ToString(l_result[0])) });
            //    }
            //}
            ViewBag.NGon.Model = l_model;
            return View(l_model);
        }
        //public ActionResult Index()
        //{
        //    Bus_List l_list = new Bus_List();


        //    if ((!SessionHelpers.IsAuthenticated()) || (SessionHelpers.IsAuthenticated() && SessionHelpers.GetUser().PersonType.Equals("E")))
        //    {

        //        HomeIndex_Model l_model = new HomeIndex_Model
        //        {
        //            Courses = MyCoursesToIEnumerable(l_list.GetMyCourses2Home(new Home_Model
        //            {
        //                LoggedIn = SessionHelpers.IsAuthenticated() ? "S" : "N",
        //                PersonId = SessionHelpers.IsAuthenticated() ? SessionHelpers.GetUser().PersonId : 0,
        //                ProviderId = SessionHelpers.IsAuthenticated() ? SessionHelpers.GetUser().ProviderId : 0,
        //            })),
        //            Notices = MyNoticesToIEnumerable(l_list.GetMyNotices2Home(new Home_Model
        //            {
        //                PersonId = SessionHelpers.IsAuthenticated() ? SessionHelpers.GetUser().PersonId : 0,
        //                ProviderId = SessionHelpers.IsAuthenticated() ? SessionHelpers.GetUser().ProviderId : 0,
        //                ProfileId = SessionHelpers.IsAuthenticated() ? SessionHelpers.GetProfileId() : 0
        //            })),
        //            FromLogistica = SessionHelpers.IsAuthenticated() ? SessionHelpers.IsFromLogistica() : false,
        //            IsPerson = SessionHelpers.IsAuthenticated() ? SessionHelpers.GetUser().PersonId != 0 : false,
        //            IsProvider = SessionHelpers.IsAuthenticated() ? SessionHelpers.GetUser().ProviderId != 0 : false
        //        };
        //        //if (TempData["FromLogin"] != null && SessionHelpers.IsAuthenticated() && SessionHelpers.IsFromLogistica() && SessionHelpers.GetUser().PersonId != 0)
        //        //{
        //        //    //remove temporal data
        //        //    TempData.Remove("FromLogin");
        //        //    //
        //        //    var l_result = l_model.Courses.Where(c => (c.RegisterMode == "R") && c.Registered == 1 && c.Initiated == 1).Select(c => c.ProgrammingId).ToList();
        //        //    if (l_result.Count == 1)
        //        //    {
        //        //        return RedirectToAction("Classroom", "Programming", new { id = Helpers.GetBase64Encode(Convert.ToString(l_result[0])) });
        //        //    }
        //        //}
        //        ViewBag.NGon.Model = l_model;
        //        return View(l_model);
        //    }
        //    else
        //    {

        //        //return RedirectToAction("Index", "HomeLMS");
        //        return RedirectToAction("Index", "Home");

        //    }

        //}
        [AppAuthenticate]
        [AppAuthorize]
        [AppLog]
        [AppNoCache]
        public ActionResult Carrier()
        {
            Bus_List l_list = new Bus_List();
            var l_programs = CarrierProgramsToIEnumerable(l_list.GetCarrierPrograms2Home()).ToList();

            if (l_programs.Count == 1)
            {
                return RedirectToAction("Classroom", "Programming", new { id = Helpers.GetBase64Encode(Convert.ToString(l_programs[0].ProgrammingId)) });
            }

            return View(l_programs);
        }
        #region private
        private IEnumerable<Notice_Entity> MyNoticesToIEnumerable(DataTable dataTable)
        {
            return dataTable.AsEnumerable().Select(row => new Notice_Entity
            {
                Description = row["Descripcion"].ToString(),
                Url = row["Url"].ToString()
            });
        }
        private IEnumerable<MyCourses_Model> MyCoursesToIEnumerable(DataTable dataTable)
        {
            string l_storage_url_elearning = Helpers.GetAppSettings("Azure_StorageUrl_ELearning");
            return dataTable.AsEnumerable().Select(row => new MyCourses_Model
            {
                ProgrammingId = Convert.ToDecimal(row["Codi_Pcur"]),
                CourseName = row["Curso_Nombre"].ToString(),
                CourseObjetives = row["Curso_Objetivo"].ToString(),
                CourseSummary = row["Curso_Resumen"].ToString(),
                CourseFrontPage = row["Curso_Descripcion_Portada"].ToString(),
                CourseImageUrl = l_storage_url_elearning + Helpers.DIRECTORY_COURSE_CONTENT
                                                            .Replace("{name}", Convert.ToString(row["Curso_Nombre"]))
                                                            .Replace("{filename}", Convert.ToString(row["Curso_Imagen"])),
                CourseCategory = row["Categoria"].ToString(),
                Mode = row["Modalidad"].ToString(),
                Instructor = row["Instructor"].ToString(),
                HasTest = row["Examen"].ToString(),
                Place = row["Lugar"].ToString(),
                FromDate = row["Fecha_Inicio"].ToString(),
                ToDate = row["Fecha_Fin"].ToString(),
                FromHour = row["Hora_Inicio"].ToString(),
                ToHour = row["Hora_Fin"].ToString(),
                Weekend = row["Sabado_Domingo"].ToString(),
                AccessType = row["Tipo_Acceso"].ToString(),
                ApprovalType = row["Modo_Aprobacion"].ToString(),
                MinimumAssistance = row["Asistencia_Aprobacion"].ToString(),
                Vacancies = Convert.ToDecimal(row["Nro_Vacante"]),
                RegistrationLimit = row["Fin_Inscripcion"].ToString(),
                ToRegister = Convert.ToDecimal(row["Flag_Inscribirse"]),
                RegistrationNumbers = Convert.ToDecimal(row["Nro_Inscritos"]),
                Registered = Convert.ToDecimal(row["Inscrito"]),
                FilterClass = row["Filter_Class"].ToString(),
                RegisterMode = row["Flag_Modo_Inscripcion"].ToString(),
                Initiated = Convert.ToDecimal(row["Curso_Iniciado"]),
                ShowContent = row["Mostrar_Contenido"].ToString(),
                HasCertificate = row["Certificado"].ToString(),
                HasPoll = row["Encuesta"].ToString()
            });
        }
        private IEnumerable<CarrierPrograms_Model> CarrierProgramsToIEnumerable(DataTable dataTable)
        {
            return dataTable.AsEnumerable().Select(row => new CarrierPrograms_Model
            {
                ProgrammingId = Convert.ToDecimal(row["Codi_Pcur"]),
                FromDate = row["Fecha_Inicio"].ToString(),
                ToDate = row["Fecha_Fin"].ToString(),
                FromHour = row["Hora_Inicio"].ToString(),
                ToHour = row["Hora_Fin"].ToString(),
                Name = row["Nombre"].ToString()
            });
        }
        #endregion private
    }
}