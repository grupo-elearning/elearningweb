﻿using ClosedXML.Excel;
using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class ProviderController : Controller
    {
        // GET: Provider
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Create()
        {
            Bus_List l_list = new Bus_List();
            Provider_Model l_model = new Provider_Model();
            var l_logistica_admin = SessionHelpers.IsLogisticaAdminProfile();
            var l_profile = l_logistica_admin ? Convert.ToDecimal(Helpers.GetParameterByKey("CODI_PERF_ADMINISTRADOR")) : SessionHelpers.GetProfileId();
            l_model.Documents = ConvertToProviderDocument_Model(l_list.GetProviderDocumentByProfile(new Provider_Model
            {
                ProfileId = l_profile
            }));
            l_model.LogisticaDocuments = l_logistica_admin ? ConvertToProviderDocumentLogistica_Model(l_list.GetProviderDocumentLogistica("P")) : null;
            l_list = null;
            ViewBag.NGon.Model = l_model;
            return View(l_model);
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Edit(string id = null)
        {
            int l_id = SessionHelpers.GetUser().ProviderId == 0 ? Convert.ToInt32(Helpers.GetBase64Decode(id)) : Convert.ToInt32(SessionHelpers.GetUser().ProviderId);
            Bus_List l_list = new Bus_List();
            Provider_Entity l_model = null;
            var l_logistica_admin = SessionHelpers.IsLogisticaAdminProfile();
            var l_profile = l_logistica_admin ? Convert.ToDecimal(Helpers.GetParameterByKey("CODI_PERF_ADMINISTRADOR")) : SessionHelpers.GetProfileId();
            var l_provider = l_list.RetrieveProvider(l_id);
            bool l_noData = l_provider.Rows.Count == 0;
            if (!l_noData)
            {
                var l_ruc = Convert.IsDBNull(l_provider.Rows[0]["RUC"]) ? "" : Convert.ToString(l_provider.Rows[0]["RUC"]);
                l_model = new Provider_Entity
                {
                    Id = l_id,
                    SAPCode = Convert.IsDBNull(l_provider.Rows[0]["CODIGO_SAP"]) ? "" : Convert.ToString(l_provider.Rows[0]["CODIGO_SAP"]),
                    Name = Convert.IsDBNull(l_provider.Rows[0]["NOMBRE"]) ? "" : Convert.ToString(l_provider.Rows[0]["NOMBRE"]),
                    BusinessName = Convert.IsDBNull(l_provider.Rows[0]["RAZON_SOCIAL"]) ? "" : Convert.ToString(l_provider.Rows[0]["RAZON_SOCIAL"]),
                    Address = Convert.IsDBNull(l_provider.Rows[0]["DIRECCION_FISCAL"]) ? "" : Convert.ToString(l_provider.Rows[0]["DIRECCION_FISCAL"]),
                    RUC = l_ruc,
                    HasRetention = Convert.IsDBNull(l_provider.Rows[0]["FLAG_RETENCION"]) ? "N" : Convert.ToString(l_provider.Rows[0]["FLAG_RETENCION"]),
                    Retention = Convert.IsDBNull(l_provider.Rows[0]["RETENCION"]) ? 0 : Convert.ToDecimal(l_provider.Rows[0]["RETENCION"]),
                    Status = Convert.IsDBNull(l_provider.Rows[0]["ESTADO"]) ? "I" : Convert.ToString(l_provider.Rows[0]["ESTADO"]),
                    Email = Convert.IsDBNull(l_provider.Rows[0]["Email_Prov"]) ? "" : Convert.ToString(l_provider.Rows[0]["Email_Prov"]),
                    Password = Convert.IsDBNull(l_provider.Rows[0]["PASS_USER"]) ? "" : Convert.ToString(l_provider.Rows[0]["PASS_USER"]),
                    User = Convert.IsDBNull(l_provider.Rows[0]["CODI_USER"]) ? "" : Convert.ToString(l_provider.Rows[0]["CODI_USER"]),
                    Phones = RetrieveProviderPhones(l_id, l_list),
                    Employees = RetrieveProviderEmployees(l_id, l_list),
                    Vehicles = RetrieveProviderVehicles(l_id, l_list),
                    Accounts = RetrieveProviderAccounts(l_id, l_ruc, l_list),
                    Emails = RetrieveProviderEmails(l_id, l_list),
                    IncotermsId = Convert.IsDBNull(l_provider.Rows[0]["CODI_TERM"]) ? 0 : Convert.ToDecimal(l_provider.Rows[0]["CODI_TERM"]),
                    IncotermsDescription = Convert.IsDBNull(l_provider.Rows[0]["INCOTERMS"]) ? "" : Convert.ToString(l_provider.Rows[0]["INCOTERMS"])
                };
                if (SessionHelpers.IsProviderProfile())
                {
                    l_model.Documents = ConvertToProviderDocument_Model(l_list.GetProviderDocumentByProvider(l_id), l_ruc);
                    l_model.LogisticaDocuments = ConvertToProviderDocumentLogistica_Model(l_list.GetProviderDocumentLogistica("P"));
                }
                else
                {
                    l_model.Documents = ConvertToProviderDocument_Model(l_list.GetProviderDocumentByProfile(new Provider_Model
                    {
                        ProviderId = l_id,
                        ProfileId = l_profile
                    }), l_ruc);
                    l_model.LogisticaDocuments = l_logistica_admin ? ConvertToProviderDocumentLogistica_Model(l_list.GetProviderDocumentLogistica("P")) : null;
                }
            }

            Helpers.ClearDataTable(l_provider);
            l_list = null;

            ViewBag.NGon.Model = l_model;

            if (l_noData)
            {
                return View("NotFound");
            }
            else
            {
                return View(l_model);
            }
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Detail(string id = null)
        {
            int l_id = SessionHelpers.GetUser().ProviderId == 0 ? Convert.ToInt32(Helpers.GetBase64Decode(id)) : Convert.ToInt32(SessionHelpers.GetUser().ProviderId);
            Bus_List l_list = new Bus_List();
            var l_logistica_admin = SessionHelpers.IsLogisticaAdminProfile();
            var l_profile = l_logistica_admin ? Convert.ToDecimal(Helpers.GetParameterByKey("CODI_PERF_ADMINISTRADOR")) : SessionHelpers.GetProfileId();
            var l_provider = l_list.RetrieveProvider(l_id);
            var l_ruc = Convert.IsDBNull(l_provider.Rows[0]["RUC"]) ? "" : l_provider.Rows[0]["RUC"].ToString();
            ProviderDetail_Model l_model = new ProviderDetail_Model
            {
                Provider = new Provider_Entity
                {
                    Id = l_id,
                    BusinessName = Convert.IsDBNull(l_provider.Rows[0]["RAZON_SOCIAL"]) ? "" : l_provider.Rows[0]["RAZON_SOCIAL"].ToString(),
                    RUC = l_ruc,
                    Name = Convert.IsDBNull(l_provider.Rows[0]["NOMBRE"]) ? "" : l_provider.Rows[0]["NOMBRE"].ToString(),
                    SAPCode = Convert.IsDBNull(l_provider.Rows[0]["CODIGO_SAP"]) ? "" : l_provider.Rows[0]["CODIGO_SAP"].ToString(),
                    Email = Convert.IsDBNull(l_provider.Rows[0]["Email_Prov"]) ? "" : Convert.ToString(l_provider.Rows[0]["Email_Prov"]),
                    IncotermsDescription = Convert.IsDBNull(l_provider.Rows[0]["INCOTERMS"]) ? "" : Convert.ToString(l_provider.Rows[0]["INCOTERMS"])
                },
                Employees = GetProviderEmployee(l_id, "V", l_profile, l_list),
                Vehicles = GetProviderVehicle(l_id, "V", l_profile, l_list)
            };
            l_model.Documents = ConvertToProviderDocument_Model(l_list.GetProviderDocumentByProfile(new Provider_Model
            {
                ProviderId = l_id,
                ProfileId = l_profile
            }), l_ruc);
            l_model.LogisticaDocuments = l_logistica_admin ? ConvertToProviderDocumentLogistica_Model(l_list.GetProviderDocumentLogistica("P")) : null;
            l_model.EmployeesTrained = l_logistica_admin ? GetProviderEmployeeTrainedLogistica(l_id, l_list) : null;
            l_model.EmployeesDocuments = GetProviderEmployeeDocumentToApproveByProfile(Convert.ToInt32(l_profile), l_list);
            l_model.LogisticaEmployeesDocuments = l_logistica_admin ? ConvertToProviderDocumentLogistica_Model(l_list.GetProviderDocumentLogistica("T")) : null;
            l_model.VehiclesDocuments = GetProviderVehicleDocumentToApproveByProfile(Convert.ToInt32(l_profile), l_list);
            l_model.LogisticaVehiclesDocuments = l_logistica_admin ? ConvertToProviderDocumentLogistica_Model(l_list.GetProviderDocumentLogistica("U")) : null;
            //get employee document
            var l_employee_url = Helpers.GetAppSettings("Azure_StorageUrl_Administracion_Proveedor") + Helpers.DIRECTORY_PROVIDER_EMPLOYEE.Replace("{ruc}", l_ruc);
            foreach (List<object> employee in l_model.Employees)
            {
                employee.Add(GetProviderEmployeeDocumentToApproveByProfile(l_id, Convert.ToInt32(employee[0]), Convert.ToInt32(l_profile), l_employee_url, l_list));
            }
            //get vehicle document
            var l_vehicle_url = Helpers.GetAppSettings("Azure_StorageUrl_Administracion_Proveedor") + Helpers.DIRECTORY_PROVIDER_VEHICLE.Replace("{ruc}", l_ruc);
            foreach (List<object> vehicle in l_model.Vehicles)
            {
                vehicle.Add(GetProviderVehicleDocumentToApproveByProfile(l_id, Convert.ToInt32(vehicle[0]), Convert.ToInt32(l_profile), l_vehicle_url, l_list));
            }
            //
            l_list = null;
            ViewBag.NGon.Model = l_model;
            return View(l_model);
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Employee(string id = null)
        {
            int l_id = SessionHelpers.GetUser().ProviderId == 0 ? Convert.ToInt32(Helpers.GetBase64Decode(id)) : Convert.ToInt32(SessionHelpers.GetUser().ProviderId);
            Bus_List l_list = new Bus_List();
            var l_logistica_admin = SessionHelpers.IsLogisticaAdminProfile();
            var l_profile = l_logistica_admin ? Convert.ToDecimal(Helpers.GetParameterByKey("CODI_PERF_ADMINISTRADOR")) : SessionHelpers.GetProfileId();
            var l_provider = l_list.RetrieveProvider(l_id);
            ProviderEmployee_Model l_model = new ProviderEmployee_Model
            {
                Provider = new Provider_Entity
                {
                    Id = l_id,
                    BusinessName = Convert.IsDBNull(l_provider.Rows[0]["RAZON_SOCIAL"]) ? "" : l_provider.Rows[0]["RAZON_SOCIAL"].ToString()
                },
                Employees = GetProviderEmployee(l_id, "V", l_profile, l_list)
            };
            if (SessionHelpers.IsProviderProfile())
            {
                l_model.Documents = GetProviderEmployeeDocument(l_list);
                l_model.LogisticaDocuments = ConvertToProviderDocumentLogistica_Model(l_list.GetProviderDocumentLogistica("T"));
                l_model.EmployeesTrained = GetProviderEmployeeTrainedLogistica(l_id, l_list);
            }
            else
            {
                l_model.Documents = GetProviderEmployeeDocumentToApproveByProfile(Convert.ToInt32(l_profile), l_list);
                l_model.LogisticaDocuments = l_logistica_admin ? ConvertToProviderDocumentLogistica_Model(l_list.GetProviderDocumentLogistica("T")) : null;
                l_model.EmployeesTrained = l_logistica_admin ? GetProviderEmployeeTrainedLogistica(l_id, l_list) : null;
            }
            //get employee document
            var l_ruc = l_list.GetProviderRUC(l_id);
            var l_url = Helpers.GetAppSettings("Azure_StorageUrl_Administracion_Proveedor") + Helpers.DIRECTORY_PROVIDER_EMPLOYEE.Replace("{ruc}", l_ruc);
            foreach (List<object> employee in l_model.Employees)
            {
                employee.Add(GetProviderEmployeeDocumentToApproveByProfile(l_id, Convert.ToInt32(employee[0]), Convert.ToInt32(l_profile), l_url, l_list));
            }
            l_list = null;
            ViewBag.NGon.Model = l_model;
            return View(l_model);
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Vehicle(string id = null)
        {
            int l_id = SessionHelpers.GetUser().ProviderId == 0 ? Convert.ToInt32(Helpers.GetBase64Decode(id)) : Convert.ToInt32(SessionHelpers.GetUser().ProviderId);
            Bus_List l_list = new Bus_List();
            var l_logistica_admin = SessionHelpers.IsLogisticaAdminProfile();
            var l_profile = l_logistica_admin ? Convert.ToDecimal(Helpers.GetParameterByKey("CODI_PERF_ADMINISTRADOR")) : SessionHelpers.GetProfileId();
            var l_provider = l_list.RetrieveProvider(l_id);
            ProviderVehicle_Model l_model = new ProviderVehicle_Model
            {
                Provider = new Provider_Entity
                {
                    Id = l_id,
                    BusinessName = Convert.IsDBNull(l_provider.Rows[0]["RAZON_SOCIAL"]) ? "" : l_provider.Rows[0]["RAZON_SOCIAL"].ToString()
                },
                Vehicles = GetProviderVehicle(l_id, "V", l_profile, l_list)
            };
            if (SessionHelpers.IsProviderProfile())
            {
                l_model.Documents = GetProviderVehicleDocument(l_list);
                l_model.LogisticaDocuments = ConvertToProviderDocumentLogistica_Model(l_list.GetProviderDocumentLogistica("U"));
            }
            else
            {
                l_model.Documents = GetProviderVehicleDocumentToApproveByProfile(Convert.ToInt32(l_profile), l_list);
                l_model.LogisticaDocuments = l_logistica_admin ? ConvertToProviderDocumentLogistica_Model(l_list.GetProviderDocumentLogistica("U")) : null;
            }
            //get vehicle document
            var l_ruc = l_list.GetProviderRUC(l_id);
            var l_url = Helpers.GetAppSettings("Azure_StorageUrl_Administracion_Proveedor") + Helpers.DIRECTORY_PROVIDER_VEHICLE.Replace("{ruc}", l_ruc);
            foreach (List<object> vehicle in l_model.Vehicles)
            {
                vehicle.Add(GetProviderVehicleDocumentToApproveByProfile(l_id, Convert.ToInt32(vehicle[0]), Convert.ToInt32(l_profile), l_url, l_list));
            }
            l_list = null;
            ViewBag.NGon.Model = l_model;
            return View(l_model);
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult ApproveDocument()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult ToApprove(string id = null)
        {
            int l_id = SessionHelpers.GetUser().ProviderId == 0 ? Convert.ToInt32(Helpers.GetBase64Decode(id)) : Convert.ToInt32(SessionHelpers.GetUser().ProviderId);
            Bus_List l_list = new Bus_List();
            var l_profile = SessionHelpers.GetProfileId();
            var l_provider = l_list.RetrieveProvider(l_id);
            var l_ruc = Convert.IsDBNull(l_provider.Rows[0]["RUC"]) ? "" : l_provider.Rows[0]["RUC"].ToString();
            ProviderToApprove_Model l_model = new ProviderToApprove_Model
            {
                Provider = new Provider_Entity
                {
                    Id = l_id,
                    BusinessName = Convert.IsDBNull(l_provider.Rows[0]["RAZON_SOCIAL"]) ? "" : l_provider.Rows[0]["RAZON_SOCIAL"].ToString(),
                    RUC = l_ruc,
                    Name = Convert.IsDBNull(l_provider.Rows[0]["NOMBRE"]) ? "" : l_provider.Rows[0]["NOMBRE"].ToString(),
                    SAPCode = Convert.IsDBNull(l_provider.Rows[0]["CODIGO_SAP"]) ? "" : l_provider.Rows[0]["CODIGO_SAP"].ToString(),
                    Email = Convert.IsDBNull(l_provider.Rows[0]["Email_Prov"]) ? "" : Convert.ToString(l_provider.Rows[0]["Email_Prov"]),
                    IncotermsDescription = Convert.IsDBNull(l_provider.Rows[0]["INCOTERMS"]) ? "" : Convert.ToString(l_provider.Rows[0]["INCOTERMS"])
                },
                Employees = GetProviderEmployee(l_id, "A", l_profile, l_list),
                Vehicles = GetProviderVehicle(l_id, "A", l_profile, l_list)
            };
            l_model.Documents = ConvertToProviderDocument_Model(l_list.GetProviderDocumentByProfile(new Provider_Model
            {
                ProviderId = l_id,
                ProfileId = l_profile
            }), l_ruc);
            l_model.EmployeesDocuments = GetProviderEmployeeDocumentToApproveByProfile(Convert.ToInt32(l_profile), l_list);
            l_model.VehiclesDocuments = GetProviderVehicleDocumentToApproveByProfile(Convert.ToInt32(l_profile), l_list);
            //get employee document
            var l_employee_url = Helpers.GetAppSettings("Azure_StorageUrl_Administracion_Proveedor") + Helpers.DIRECTORY_PROVIDER_EMPLOYEE.Replace("{ruc}", l_ruc);
            foreach (List<object> employee in l_model.Employees)
            {
                employee.Add(GetProviderEmployeeDocumentToApproveByProfile(l_id, Convert.ToInt32(employee[0]), Convert.ToInt32(l_profile), l_employee_url, l_list));
            }
            //get vehicle document
            var l_vehicle_url = Helpers.GetAppSettings("Azure_StorageUrl_Administracion_Proveedor") + Helpers.DIRECTORY_PROVIDER_VEHICLE.Replace("{ruc}", l_ruc);
            foreach (List<object> vehicle in l_model.Vehicles)
            {
                vehicle.Add(GetProviderVehicleDocumentToApproveByProfile(l_id, Convert.ToInt32(vehicle[0]), Convert.ToInt32(l_profile), l_vehicle_url, l_list));
            }
            //
            l_list = null;
            ViewBag.NGon.Model = l_model;
            return View(l_model);
        }
        [HttpPost]
        public ActionResult ListDetail(int id)
        {
            Bus_List l_list = new Bus_List();
            Provider_Model l_model = new Provider_Model
            {
                ProviderId = id,
                Employees = GetProviderEmployee(id, "V", SessionHelpers.GetProfileId(), l_list),
                Vehicles = GetProviderVehicle(id, "V", SessionHelpers.GetProfileId(), l_list)
            };
            l_list = null;
            ViewBag.NGon.Model = l_model;
            return PartialView("PartialView/_ListDetailPartialView", l_model);
        }
        [HttpPost]
        public ActionResult ListApproveDocumentDetail(int id)
        {
            Bus_List l_list = new Bus_List();
            Provider_Model l_model = new Provider_Model
            {
                ProviderId = id,
                Employees = GetProviderEmployee(id, "A", SessionHelpers.GetProfileId(), l_list),
                Vehicles = GetProviderVehicle(id, "A", SessionHelpers.GetProfileId(), l_list)
            };
            l_list = null;
            ViewBag.NGon.Model = l_model;
            return PartialView("PartialView/_ListDetailPartialView", l_model);
        }
        [HttpPost]
        public JsonResult Create(Provider_Entity p_Entity)
        {
            Bus_List l_list;
            Bus_Crud l_crud;
            AzureStorageHelpers l_storage = null;
            DataTable l_dt = null;
            try
            {
                l_list = new Bus_List();
                //validate ruc
                var l_validate = l_list.GetProviderIdByRUC(new Provider_Model
                {
                    ProviderRUC = p_Entity.RUC
                });
                if (l_validate.Rows.Count > 0)
                {
                    throw new Exception("El RUC que ingreso ya existe");
                }
                //save
                l_crud = new Bus_Crud();
                l_storage = new AzureStorageHelpers("Azure_ContainerName_Administracion_Proveedor", BlobContainerPublicAccessType.Blob, "Azure_AccountName_Administracion", "Azure_AccessKey_Administracion");
                //save document
                if (p_Entity.DocumentFile != null)
                {
                    for (int i = 0; i < p_Entity.DocumentRecordId.Length; i++)
                    {
                        if (p_Entity.DocumentFile[i] != null && p_Entity.DocumentFile[i].File != null)
                        {
                            var l_dsupport = l_list.GetSustenanceDocumentCode(Convert.ToInt32(p_Entity.DocumentTypeId[i]));
                            p_Entity.DocumentFile[i].RealName = p_Entity.DocumentFile[i].File.FileName;
                            p_Entity.DocumentFile[i].Name = Helpers.GetFileNameOfProviderDocument(p_Entity.RUC, l_dsupport, p_Entity.DocumentFile[i].File.FileName);
                            p_Entity.DocumentFile[i].Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.DocumentFile[i].File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.DocumentFile[i].File.ContentLength) + "}";
                            //upload to storage
                            l_storage.UploadBlob(Helpers.DIRECTORY_PROVIDER_COMPANY
                                .Replace("{ruc}", p_Entity.RUC)
                                .Replace("{filename}", p_Entity.DocumentFile[i].Name), p_Entity.DocumentFile[i].File.InputStream);
                            p_Entity.DocumentFile[i].Uploaded = true;
                        }
                    }
                }
                //save account
                if (p_Entity.AccountFile != null)
                {
                    for (int i = 0; i < p_Entity.AccountFile.Length; i++)
                    {
                        if (p_Entity.AccountFile[i] != null && p_Entity.AccountFile[i].File != null)
                        {
                            p_Entity.AccountFile[i].RealName = p_Entity.AccountFile[i].File.FileName;
                            p_Entity.AccountFile[i].Name = Helpers.GetFileNameOfProviderAccountDocument(p_Entity.AccountFile[i].File.FileName);
                            p_Entity.AccountFile[i].Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.AccountFile[i].File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.AccountFile[i].File.ContentLength) + "}";
                            //upload to storage
                            l_storage.UploadBlob(Helpers.DIRECTORY_PROVIDER_COMPANY
                                .Replace("{ruc}", p_Entity.RUC)
                                .Replace("{filename}", p_Entity.AccountFile[i].Name), p_Entity.AccountFile[i].File.InputStream);
                            p_Entity.AccountFile[i].Uploaded = true;
                        }
                    }
                }
                //save in db
                p_Entity.Approved = "";
                p_Entity.ProcessInitial = "";
                p_Entity.Process = "";
                p_Entity.Actions = "N";
                p_Entity.AppId = SessionHelpers.GetAppId();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                l_dt = l_crud.InsertUpdateProvider(p_Entity);
                if (l_dt.Rows[0]["Value"].ToString() != "1000")
                {
                    try
                    {
                        //remove document
                        if (p_Entity.DocumentFile != null)
                        {
                            for (int i = 0; i < p_Entity.DocumentRecordId.Length; i++)
                            {
                                if (p_Entity.DocumentFile[i] != null && p_Entity.DocumentFile[i].File != null && p_Entity.DocumentFile[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_COMPANY
                                        .Replace("{ruc}", p_Entity.RUC)
                                        .Replace("{filename}", p_Entity.DocumentFile[i].Name));
                                }
                            }
                        }
                        //remove account
                        if (p_Entity.AccountFile != null)
                        {
                            for (int i = 0; i < p_Entity.AccountFile.Length; i++)
                            {
                                if (p_Entity.AccountFile[i] != null && p_Entity.AccountFile[i].File != null && p_Entity.AccountFile[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_COMPANY
                                        .Replace("{ruc}", p_Entity.RUC)
                                        .Replace("{filename}", p_Entity.AccountFile[i].Name));
                                }
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                else
                {
                    String l_updAPI = WebConfigurationManager.AppSettings["upd_API"].ToString();
                    if (l_updAPI == "S")
                    {
                        var query = p_Entity.Modify.Select((elemento, indice) => new
                        {
                            Elemento = elemento,
                            Indice = indice
                        }).Where(elem => elem.Elemento != "").Select(elem => elem.Indice);


                        foreach (int element in query)
                        {
                            var my_jsondata_login = new
                            {
                                Username = WebConfigurationManager.AppSettings["Usuario_api_local"].ToString(),
                                Password = WebConfigurationManager.AppSettings["Password_api_local"].ToString()
                            };
                            //Tranform it to Json object
                            //string json_data = JsonConvert.SerializeObject(my_jsondata_login);
                            var l_url = WebConfigurationManager.AppSettings["URL_API_VISITA"].ToString();
                            //obtiene token de API CONSUMIDA
                            string l_token = Helpers.executeAPI(my_jsondata_login, l_url + "/api/login/authenticate", "");
                            l_token = l_token.Replace("\"", "");

                            var my_jsondata_data = new
                            {
                                userid = Convert.ToInt32(p_Entity.Userid[element]),
                                codiProv = p_Entity.Id,
                                nombreProveedor = p_Entity.Name,
                                rasonSocial = p_Entity.BusinessName,
                                ruc = p_Entity.RUC,
                                emailProveedor = p_Entity.Email.ToString(),
                                telefono = "",
                                codiPcon = p_Entity.PersonRecordId[element].ToString(), //PersonRecordId,PersonPersonId
                                emailColaborador = p_Entity.PersonEmail[element].ToString(),
                                codiPers = p_Entity.PersonPersonId[element].ToString(),
                                apPaterno = p_Entity.PersonLastname1[element].ToString(),
                                apMaterno = p_Entity.PersonLastname2[element].ToString(),
                                nombres = p_Entity.PersonFirstname[element].ToString(),
                                codiTdoc = p_Entity.PersonDocumentType[element].ToString(),
                                nroDoc = p_Entity.PersonDocumentNumber[element].ToString(),
                                tipo = "E",
                                fechaNacimiento = "2020-02-02"
                            };
                            //json_data = JsonConvert.SerializeObject(my_jsondata_data);
                            string l_return = Helpers.executeAPI(my_jsondata_data, l_url + "/api/Local/NuevoColaboradorTrabajador", l_token);
                            var json_data = JsonConvert.DeserializeObject<ResponseApi>(l_return);

                            if (json_data.Status == 1000)
                            {
                                Bus_Crud bus_Crud = new Bus_Crud();
                                UpdPersona upd = new UpdPersona();
                                upd.CodiProv = my_jsondata_data.codiProv;
                                upd.NroDoc = my_jsondata_data.nroDoc;
                                upd.UserId = json_data.Codigo;

                                bus_Crud.Update_Userid_Persona(upd);
                            }
                        }
                    }

                    //send email
                    EmailHelpers.SendEmailProvider(p_Entity.BusinessName, p_Entity.Email, p_Entity.RUC, Convert.ToString(l_dt.Rows[0]["Mensaje"]));
                    l_dt.Rows[0]["Mensaje"] = "Se registró correctamente";
                    try
                    {
                        //set profile to elearning
                        var l_profile_response = l_crud.UpdateProfiles(new Profiles_Entity
                        {
                            PrfRecordId = new decimal[] { 0 },
                            PrfUsername = new string[] { p_Entity.RUC },
                            AppProfileId = Convert.ToDecimal(Helpers.GetParameterByKey("CODI_SIPE_ELEARNING_PROVEEDOR")),
                            FromProvider = 1,
                            Username = SessionHelpers.GetUsername(),
                            IPAddress = SessionHelpers.GetIPAddress()
                        });
                        if (l_profile_response.Rows[0]["Value"].ToString() == "1000")
                        {
                            //set area to my area
                            var l_area_response = l_crud.InsertUpdateAreaProvider(new AreaProvider_Entity
                            {
                                PrvRecordId = new decimal[] { 0 },
                                PrvProviderId = new decimal[] { Convert.ToDecimal(l_dt.Rows[0]["id"]) },
                                AreaId = SessionHelpers.GetProfile().AreaId,
                                FromProvider = 1,
                                Username = SessionHelpers.GetUsername(),
                                IPAddress = SessionHelpers.GetIPAddress()
                            });
                            Helpers.ClearDataTable(l_area_response);
                        }
                        Helpers.ClearDataTable(l_profile_response);
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetMessageCrud(l_dt);
            }
            catch (Exception e)
            {
                if (l_storage != null)
                {
                    try
                    {
                        //remove document
                        if (p_Entity.DocumentFile != null)
                        {
                            for (int i = 0; i < p_Entity.DocumentRecordId.Length; i++)
                            {
                                if (p_Entity.DocumentFile[i] != null && p_Entity.DocumentFile[i].File != null && p_Entity.DocumentFile[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_COMPANY
                                        .Replace("{ruc}", p_Entity.RUC)
                                        .Replace("{filename}", p_Entity.DocumentFile[i].Name));
                                }
                            }
                        }
                        //remove account
                        if (p_Entity.AccountFile != null)
                        {
                            for (int i = 0; i < p_Entity.AccountFile.Length; i++)
                            {
                                if (p_Entity.AccountFile[i] != null && p_Entity.AccountFile[i].File != null && p_Entity.AccountFile[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_COMPANY
                                        .Replace("{ruc}", p_Entity.RUC)
                                        .Replace("{filename}", p_Entity.AccountFile[i].Name));
                                }
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
                Helpers.ClearDataTable(l_dt);
            }
        }
        [HttpPost]
        public JsonResult Edit(Provider_Entity p_Entity)
        {
            Bus_List l_list;
            Bus_Crud l_crud;
            AzureStorageHelpers l_storage = null;
            DataTable l_dt = null;
            DataRow l_row;
            List<String> filesUpload;
            try
            {
                l_list = new Bus_List();
                l_crud = new Bus_Crud();
                filesUpload = new List<String>();
                l_storage = new AzureStorageHelpers("Azure_ContainerName_Administracion_Proveedor", BlobContainerPublicAccessType.Blob, "Azure_AccountName_Administracion", "Azure_AccessKey_Administracion");
                //save document
                if (p_Entity.DocumentFile != null)
                {
                    for (int i = 0; i < p_Entity.DocumentRecordId.Length; i++)
                    {
                        if (p_Entity.DocumentFile[i].File != null)
                        {
                            var l_dsupport = l_list.GetSustenanceDocumentCode(Convert.ToInt32(p_Entity.DocumentTypeId[i]));
                            p_Entity.DocumentFile[i].RealName = p_Entity.DocumentFile[i].File.FileName;
                            p_Entity.DocumentFile[i].Name = Helpers.GetFileNameOfProviderDocument(p_Entity.RUC, l_dsupport, p_Entity.DocumentFile[i].File.FileName);
                            p_Entity.DocumentFile[i].Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.DocumentFile[i].File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.DocumentFile[i].File.ContentLength) + "}";
                            //upload to storage
                            l_storage.UploadBlob(Helpers.DIRECTORY_PROVIDER_COMPANY
                                .Replace("{ruc}", p_Entity.RUC)
                                .Replace("{filename}", p_Entity.DocumentFile[i].Name), p_Entity.DocumentFile[i].File.InputStream);
                            filesUpload.Add(Helpers.DIRECTORY_PROVIDER_COMPANY.Replace("{ruc}", p_Entity.RUC).Replace("{filename}", p_Entity.DocumentFile[i].Name));
                            p_Entity.DocumentFile[i].Uploaded = true;
                            p_Entity.DocumentFile[i].File.InputStream.Seek(0, SeekOrigin.Begin);

                            if (Convert.ToInt32(p_Entity.DocumentTypeId[i]) == 75)  //Validando y extrayendo listado de covid
                            {
                                Response_Validar_Covid respon = new Response_Validar_Covid();
                                respon = CallApiValidarCovid(p_Entity.DocumentFile[i].File, p_Entity.Id, p_Entity.PersonDocumentNumber);
                                if (respon.Status == 100)
                                {
                                    p_Entity.DtDnisCovid = new DataTable();
                                    p_Entity.DtDnisCovid.Columns.Add("Dni");
                                    p_Entity.DtDnisCovid.Columns.Add("Codi_Tdos");
                                    p_Entity.DtDnisCovid.Columns.Add("Codi_Pcon");

                                    for (int h = 0; h < respon.Body.Count; h++)
                                    {
                                        l_row = p_Entity.DtDnisCovid.NewRow(); //
                                        l_row["Dni"] = respon.Body[h].Dni;
                                        l_row["Codi_Tdos"] = 0;
                                        l_row["Codi_Pcon"] = 0;
                                        p_Entity.DtDnisCovid.Rows.Add(l_row);
                                        l_row = null;
                                    }
                                }
                                else
                                {
                                    //Delete files subidos y return error
                                    for (int y = 0; y < filesUpload.Count; y++)
                                    {
                                        l_storage.DeleteBlob(filesUpload[y]);
                                    }

                                    return new JsonResult()
                                    {
                                        Data = new
                                        {
                                            status = 3000,
                                            response = "Población Vulnerable : " + respon.Message,
                                            exception = "Población Vulnerable : " + respon.Message,
                                            codi = "ssd",
                                            estado = "",
                                            others = ""
                                        },
                                        MaxJsonLength = Int32.MaxValue,
                                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                                    };
                                }
                            }

                        }
                        else
                        {
                            p_Entity.DocumentFile[i].Name = p_Entity.DocumentFile[i].Name != "" ? p_Entity.DocumentOldFileName[i] : "";
                            p_Entity.DocumentFile[i].RealName = p_Entity.DocumentFile[i].Name != "" ? p_Entity.DocumentOldFileRealname[i] : "";
                            p_Entity.DocumentFile[i].Properties = "";
                        }
                    }
                }
                //save account
                if (p_Entity.AccountFile != null)
                {
                    for (int i = 0; i < p_Entity.AccountFile.Length; i++)
                    {
                        if (p_Entity.AccountFile[i] != null && p_Entity.AccountFile[i].File != null)
                        {
                            p_Entity.AccountFile[i].RealName = p_Entity.AccountFile[i].File.FileName;
                            p_Entity.AccountFile[i].Name = Helpers.GetFileNameOfProviderAccountDocument(p_Entity.AccountFile[i].File.FileName);
                            p_Entity.AccountFile[i].Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.AccountFile[i].File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.AccountFile[i].File.ContentLength) + "}";
                            //upload to storage
                            l_storage.UploadBlob(Helpers.DIRECTORY_PROVIDER_COMPANY
                                .Replace("{ruc}", p_Entity.RUC)
                                .Replace("{filename}", p_Entity.AccountFile[i].Name), p_Entity.AccountFile[i].File.InputStream);
                            p_Entity.AccountFile[i].Uploaded = true;
                        }
                        else
                        {
                            if (p_Entity.AccountOldFileName[i] != "")
                            {
                                p_Entity.AccountFile[i] = new File_Entity
                                {
                                    Name = p_Entity.AccountOldFileName[i],
                                    RealName = p_Entity.AccountOldFileRealname[i],
                                    Properties = ""
                                };
                            }
                        }
                    }
                }
                //save in db
                p_Entity.Approved = "";
                p_Entity.ProcessInitial = "";
                p_Entity.Process = "";
                p_Entity.AppId = SessionHelpers.GetAppId();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                l_dt = l_crud.InsertUpdateProvider(p_Entity);
                if (l_dt.Rows[0]["Value"].ToString() != "1000")
                {
                    try
                    {
                        //remove document
                        if (p_Entity.DocumentFile != null)
                        {
                            for (int i = 0; i < p_Entity.DocumentRecordId.Length; i++)
                            {
                                if (p_Entity.DocumentFile[i] != null && p_Entity.DocumentFile[i].File != null && p_Entity.DocumentFile[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_COMPANY
                                        .Replace("{ruc}", p_Entity.RUC)
                                        .Replace("{filename}", p_Entity.DocumentFile[i].Name));
                                }
                            }
                        }
                        //remove account
                        if (p_Entity.AccountFile != null)
                        {
                            for (int i = 0; i < p_Entity.AccountFile.Length; i++)
                            {
                                if (p_Entity.AccountFile[i] != null && p_Entity.AccountFile[i].File != null && p_Entity.AccountFile[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_COMPANY
                                        .Replace("{ruc}", p_Entity.RUC)
                                        .Replace("{filename}", p_Entity.AccountFile[i].Name));
                                }
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                else
                {
                    String l_upd_API_SGS_Reg = WebConfigurationManager.AppSettings["upd_API_SGS_reg"].ToString();
                    String l_upd_API_SGS_Hab = WebConfigurationManager.AppSettings["upd_API_SGS_hab"].ToString();

                    var query = p_Entity.Modify.Select((elemento, indice) => new
                    {
                        Elemento = elemento,
                        Indice = indice
                    }).Where(elem => elem.Elemento == "M").Select(elem => elem.Indice);


                    foreach (int element in query)
                    {
                        var my_jsondata_login = new
                        {
                            Username = WebConfigurationManager.AppSettings["Usuario_api_local"].ToString(),
                            Password = WebConfigurationManager.AppSettings["Password_api_local"].ToString()
                        };
                        //Tranform it to Json object
                        //string json_data = JsonConvert.SerializeObject(my_jsondata_login);
                        var l_url = WebConfigurationManager.AppSettings["URL_API_VISITA"].ToString();
                        //obtiene token de API CONSUMIDA
                        string l_token = Helpers.executeAPI(my_jsondata_login, l_url + "/api/login/authenticate", "");
                        l_token = l_token.Replace("\"", "");


                        var my_jsondata_data = new
                        {
                            userid = Convert.ToInt32(p_Entity.Userid[element]),
                            codiProv = p_Entity.Id,
                            nombreProveedor = p_Entity.Name,
                            rasonSocial = p_Entity.BusinessName,
                            ruc = p_Entity.RUC,
                            emailProveedor = p_Entity.Email.ToString(),
                            telefono = "",
                            codiPcon = p_Entity.PersonRecordId[element].ToString(),
                            emailColaborador = p_Entity.PersonEmail[element].ToString(),
                            codiPers = p_Entity.PersonPersonId[element].ToString(),
                            apPaterno = p_Entity.PersonLastname1[element].ToString(),
                            apMaterno = p_Entity.PersonLastname2[element].ToString(),
                            nombres = p_Entity.PersonFirstname[element].ToString(),
                            codiTdoc = p_Entity.PersonDocumentType[element].ToString(),
                            nroDoc = p_Entity.PersonDocumentNumber[element].ToString(),
                            tipo = "E",
                            fechaNacimiento = p_Entity.PersonBirthday[element].ToString()
                        };
                        //json_data = JsonConvert.SerializeObject(my_jsondata_data);
                        string l_return = Helpers.executeAPI(my_jsondata_data, l_url + "/api/Local/NuevoColaboradorTrabajador", l_token);
                        var json_data = JsonConvert.DeserializeObject<ResponseApi>(l_return);

                        if (json_data.Status == 1000)
                        {
                            Bus_Crud bus_Crud = new Bus_Crud();
                            UpdPersona upd = new UpdPersona();
                            upd.CodiProv = my_jsondata_data.codiProv;
                            upd.NroDoc = my_jsondata_data.nroDoc;
                            upd.UserId = json_data.Codigo;

                            bus_Crud.Update_Userid_Persona(upd);
                        }

                        
                            // ACA AGREGAR EL CODIGO PARA CONSUMIR LA API REGISTRO USUARIO
                            decimal codigo_persona = -1;
                            DataTable ldt_pers;

                            if (p_Entity.Modify[element].ToString() == "M")
                            {
                                codigo_persona = Convert.ToDecimal(p_Entity.PersonPersonId[element]);
                                ldt_pers = new DataTable();
                                ldt_pers.Columns.Add("CODI_PERS");
                                DataRow lrow;
                                lrow = ldt_pers.NewRow();
                                lrow["CODI_PERS"] = codigo_persona;
                                ldt_pers.Rows.Add(lrow);

                            }
                            else
                            {
                                if (p_Entity.PersonPersonId[element].ToString() == "" || p_Entity.PersonPersonId[element].ToString() == "0" || p_Entity.PersonPersonId[element].ToString() == null)
                                {
                                    ldt_pers = l_list.PersonaNuevaList(Convert.ToDecimal(p_Entity.PersonDocumentNumber[element].ToString()));
                                }
                                else
                                {
                                    codigo_persona = Convert.ToDecimal(p_Entity.PersonPersonId[element]);
                                    ldt_pers = new DataTable();
                                    ldt_pers.Columns.Add("CODI_PERS");
                                    DataRow lrow;
                                    lrow = ldt_pers.NewRow();
                                    lrow["CODI_PERS"] = codigo_persona;
                                    ldt_pers.Rows.Add(lrow);
                                }

                            }

                            foreach (DataRow row in ldt_pers.Rows)
                            {
                                codigo_persona = Convert.ToDecimal(row["CODI_PERS"]);

                                List<Usuario_Sgs> list_usuario_sgs = new List<Usuario_Sgs>
                                {
                                    new Usuario_Sgs
                                    {
                                        codigo = p_Entity.PersonDocumentNumber[element].ToString(),
                                        nombres = p_Entity.PersonFirstname[element].ToString(),
                                        apellidos = p_Entity.PersonLastname1[element].ToString() + " " + p_Entity.PersonLastname2[element].ToString().ToString(),
                                        documento = p_Entity.PersonDocumentNumber[element].ToString(),
                                        tipo = "C",
                                        fechaNacimiento = (p_Entity.PersonBirthday[element].ToString() == null) ? "01/01/1990" : p_Entity.PersonBirthday[element].ToString(),
                                        unidadNegocio = "CHIMBOTE-TERCEROS",
                                        codUnidadNegocio = "20",//preguntar 
                                        modalidad = "PRESENCIAL",
                                        sexo = "M",
                                        codigoTrabajador = p_Entity.PersonDocumentNumber[element].ToString(),
                                        grupoRiesgo = "N",
                                        activo = true
                                    }
                                };

                                var my_jsondata_data_sgs = new
                                {
                                    usuarios = list_usuario_sgs
                                };

                                string l_return_sgs = Helpers.executeAPI(my_jsondata_data_sgs, l_url + "/api/Local/RegistrarUsuario", l_token);
                                //string l_return_sgs = Helpers.executeAPI(my_jsondata_data_sgs, l_url + "/api/Visitas/RegistroUsuario", "");
                                var json_data_sgs = JsonConvert.DeserializeObject<ResponseSGS>(l_return_sgs);

                                l_crud.InsertLogSGS(json_data_sgs, codigo_persona);
                            }
                        
                    }


                        if (p_Entity.pa_del_codi_pcon != null) { 
                            // ACA AGREGAR EL CODIGO PARA CONSUMIR LA API HABILITAR/DESHABILITAR USUARIO
                            var my_jsondata_login_SGS = new
                            {
                                Username = WebConfigurationManager.AppSettings["Usuario_api_local"].ToString(),
                                Password = WebConfigurationManager.AppSettings["Password_api_local"].ToString()
                            };
                            //Tranform it to Json object
                            //string json_data = JsonConvert.SerializeObject(my_jsondata_login);
                            var l_url_SGS = WebConfigurationManager.AppSettings["URL_API_VISITA"].ToString();
                            //obtiene token de API CONSUMIDA
                            string l_token_SGS = Helpers.executeAPI(my_jsondata_login_SGS, l_url_SGS + "/api/login/authenticate", "");
                            l_token_SGS = l_token_SGS.Replace("\"", "");

                            var l_result_persona_deleted = l_list.PersonaDeletedList(p_Entity);
                            foreach (DataRow row in l_result_persona_deleted.Rows)
                            {
                                List<UsuarioHabDes> list_usuario_hab_des = new List<UsuarioHabDes>
                                        {
                                            new UsuarioHabDes
                                            {
                                                documento = Convert.ToString(row["NRO_DOC"]),
                                                activo = "false"
                                            }
                                        };

                                var my_jsondata_data_sgs = new
                                {
                                    usuarios = list_usuario_hab_des
                                };


                                string l_return_sgs = Helpers.executeAPI(my_jsondata_data_sgs, l_url_SGS + "/api/Local/HabilitaDeshabilitaUsuario", l_token_SGS);
                                var json_data_sgs = JsonConvert.DeserializeObject<ResponseSGS>(l_return_sgs);

                                l_crud.InsertLogSGS(json_data_sgs, Convert.ToDecimal(row["CODI_PERS"]));
                            }
                        }
                    if (p_Entity.Actions == "E")
                    {
                        //send email
                        EmailHelpers.SendEmailProvider(p_Entity.BusinessName, p_Entity.Email, p_Entity.RUC, Convert.ToString(l_dt.Rows[0]["Mensaje"]));
                    }
                    l_dt.Rows[0]["Mensaje"] = "Se registró correctamente";
                }
                return Helpers.GetMessageCrud(l_dt);
            }
            catch (Exception e)
            {
                if (l_storage != null)
                {
                    try
                    {
                        //remove document
                        if (p_Entity.DocumentFile != null)
                        {
                            for (int i = 0; i < p_Entity.DocumentRecordId.Length; i++)
                            {
                                if (p_Entity.DocumentFile[i] != null && p_Entity.DocumentFile[i].File != null && p_Entity.DocumentFile[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_COMPANY
                                        .Replace("{ruc}", p_Entity.RUC)
                                        .Replace("{filename}", p_Entity.DocumentFile[i].Name));
                                }
                            }
                        }
                        //remove account
                        if (p_Entity.AccountFile != null)
                        {
                            for (int i = 0; i < p_Entity.AccountFile.Length; i++)
                            {
                                if (p_Entity.AccountFile[i] != null && p_Entity.AccountFile[i].File != null && p_Entity.AccountFile[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_COMPANY
                                        .Replace("{ruc}", p_Entity.RUC)
                                        .Replace("{filename}", p_Entity.AccountFile[i].Name));
                                }
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
                Helpers.ClearDataTable(l_dt);
            }
        }

        public Response_Validar_Covid CallApiValidarCovid(HttpPostedFileBase file, decimal Codi_Prov, String[] Nro_doc)
        {
            Response_Validar_Covid response = new Response_Validar_Covid();
            String dnis_no_existentes = "";
            using (var client = new HttpClient())
            {
                using (var content = new MultipartFormDataContent())
                {
                    byte[] Bytes = new byte[file.InputStream.Length + 1];
                    file.InputStream.Read(Bytes, 0, Bytes.Length);
                    var fileContent = new ByteArrayContent(Bytes);
                    fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = file.FileName };
                    content.Add(fileContent, "file");
                    content.Add(new StringContent(Codi_Prov.ToString()), "Codi_prov");

                    var requestUri = "";
                    requestUri = System.Configuration.ConfigurationManager.AppSettings.Get("UrlApiCovid");
                    var result = client.PostAsync(requestUri, content).Result;

                    if (result.StatusCode == HttpStatusCode.OK)
                    {
                        var resultStrig = result.Content.ReadAsStringAsync();
                        response = JsonConvert.DeserializeObject<Response_Validar_Covid>(resultStrig.Result);

                        if (response.Status == 100)
                        {
                            for (int k = 0; k < response.Body.Count; k++)
                            {
                                String dni = response.Body[k].Dni;
                                String existe = response.Body[k].Existe;
                                for (int g = 0; g < Nro_doc.Length; g++)
                                {
                                    if (dni == Nro_doc[g])
                                    {
                                        existe = "SI";
                                        break;
                                    }
                                    else
                                    {
                                        existe = "NO";
                                    }
                                }

                                response.Body[k].Existe = existe;
                                if (existe.Equals("NO"))
                                {
                                    dnis_no_existentes += dni + ",";
                                }
                            }

                            if (dnis_no_existentes.Length > 0)
                            {
                                response.Status = 400;
                                response.Message = "Los dnis " + dnis_no_existentes + " no existen. Verificar excel.";
                            }

                        }
                        else
                        {
                            response.Status = 400;
                        }
                    }
                    else
                    {
                        response.Status = 400;
                        response.Message = "Error validando excel.";
                    }
                }
            }


            return response;
        }

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        [HttpPost]
        public JsonResult ToApprove(ProviderToApprove_Entity p_Entity)
        {
            Bus_Crud l_crud;
            DataTable l_dt = null;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                l_dt = l_crud.ToApproveDocumentProvider(p_Entity);
                if (l_dt.Rows[0]["Value"].ToString() == "1000")
                {
                    var l_provider = Array.IndexOf(p_Entity.ProviderSwitch, "R") >= 0;
                    var l_employee = Array.IndexOf(p_Entity.EmployeeSwitch, "R") >= 0;
                    var l_vehicle = Array.IndexOf(p_Entity.VehicleSwitch, "R") >= 0;
                    if (l_provider || l_employee || l_vehicle)
                    {
                        EmailHelpers.SendEmailProviderDocumentDeny(l_provider, l_employee, l_vehicle, p_Entity);
                    }
                }
                return Helpers.GetMessageCrud(l_dt);
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public JsonResult Employee(ProviderEmployee_Entity p_Entity)
        {
            Bus_List l_list = null;
            Bus_Crud l_crud;
            AzureStorageHelpers l_storage = null;
            DataTable l_dt = null;
            try
            {
                l_list = new Bus_List();
                l_crud = new Bus_Crud();
                l_storage = new AzureStorageHelpers("Azure_ContainerName_Administracion_Proveedor", BlobContainerPublicAccessType.Blob, "Azure_AccountName_Administracion", "Azure_AccessKey_Administracion");
                var l_ruc = l_list.GetProviderRUC(Convert.ToInt32(p_Entity.ProviderId));
                //save photo
                if (p_Entity.EmployeeFile != null)
                {
                    for (int i = 0; i < p_Entity.EmployeePcon.Length; i++)
                    {
                        if (p_Entity.EmployeeFile[i] != null && p_Entity.EmployeeFile[i].File != null)
                        {
                            p_Entity.EmployeeFile[i].RealName = p_Entity.EmployeeFile[i].File.FileName;
                            p_Entity.EmployeeFile[i].Name = Helpers.GetFileNameOfProviderEmployeePhoto(p_Entity.EmployeeFile[i].File.FileName);
                            p_Entity.EmployeeFile[i].Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.EmployeeFile[i].File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.EmployeeFile[i].File.ContentLength) + "}";
                            //upload to storage
                            l_storage.UploadBlob(Helpers.DIRECTORY_PROVIDER_EMPLOYEE
                                .Replace("{ruc}", l_ruc)
                                .Replace("{filename}", p_Entity.EmployeeFile[i].Name), p_Entity.EmployeeFile[i].File.InputStream);
                            p_Entity.EmployeeFile[i].Uploaded = true;
                        }
                    }
                }
                //save employee document
                if (p_Entity.EmployeeDocumentFile != null)
                {
                    for (int i = 0; i < p_Entity.EmployeeDocumentPcon.Length; i++)
                    {
                        if (p_Entity.EmployeeDocumentFile[i] != null && p_Entity.EmployeeDocumentFile[i].File != null)
                        {
                            var l_didentity = l_list.GetIdentityDocumentCode(Convert.ToInt32(p_Entity.EmployeeDocumentPcon[i]));
                            var l_dsupport = l_list.GetSustenanceDocumentCode(Convert.ToInt32(p_Entity.EmployeeDocumentTdos[i]));
                            p_Entity.EmployeeDocumentFile[i].RealName = p_Entity.EmployeeDocumentFile[i].File.FileName;
                            p_Entity.EmployeeDocumentFile[i].Name = Helpers.GetFileNameOfProviderEmployeeDocument(l_didentity, l_dsupport, p_Entity.EmployeeDocumentFile[i].File.FileName);
                            p_Entity.EmployeeDocumentFile[i].Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.EmployeeDocumentFile[i].File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.EmployeeDocumentFile[i].File.ContentLength) + "}";
                            //upload to storage
                            l_storage.UploadBlob(Helpers.DIRECTORY_PROVIDER_EMPLOYEE
                                .Replace("{ruc}", l_ruc)
                                .Replace("{filename}", p_Entity.EmployeeDocumentFile[i].Name), p_Entity.EmployeeDocumentFile[i].File.InputStream);
                            p_Entity.EmployeeDocumentFile[i].Uploaded = true;
                        }
                    }
                }
                //save in db
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                l_dt = l_crud.InsertUpdateProviderEmployee(p_Entity);
                if (l_dt.Rows[0]["Value"].ToString() != "1000")
                {
                    try
                    {
                        //remove photo
                        if (p_Entity.EmployeeFile != null)
                        {
                            for (int i = 0; i < p_Entity.EmployeePcon.Length; i++)
                            {
                                if (p_Entity.EmployeeFile[i] != null && p_Entity.EmployeeFile[i].File != null && p_Entity.EmployeeFile[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_EMPLOYEE
                                        .Replace("{ruc}", l_ruc)
                                        .Replace("{filename}", p_Entity.EmployeeFile[i].Name));
                                }
                            }
                        }
                        //remove employee document
                        if (p_Entity.EmployeeDocumentFile != null)
                        {
                            for (int i = 0; i < p_Entity.EmployeeDocumentPcon.Length; i++)
                            {
                                if (p_Entity.EmployeeDocumentFile[i] != null && p_Entity.EmployeeDocumentFile[i].File != null && p_Entity.EmployeeDocumentFile[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_EMPLOYEE
                                        .Replace("{ruc}", l_ruc)
                                        .Replace("{filename}", p_Entity.EmployeeDocumentFile[i].Name));
                                }
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                else
                {
                    try
                    {
                        //remove delete
                        for (int i = 0; i < p_Entity.DeleteDocumentPcdo.Length; i++)
                        {
                            if (p_Entity.DeleteDocumentPcdo[i] != 0)
                            {
                                l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_EMPLOYEE
                                    .Replace("{ruc}", l_ruc)
                                    .Replace("{filename}", p_Entity.DeleteDocumentName[i]));
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetMessageCrud(l_dt);
            }
            catch (Exception e)
            {
                if (l_storage != null)
                {
                    try
                    {
                        if (l_list == null)
                        {
                            l_list = new Bus_List();
                        }
                        var l_ruc = l_list.GetProviderRUC(Convert.ToInt32(p_Entity.ProviderId));
                        //remove photo
                        if (p_Entity.EmployeeFile != null)
                        {
                            for (int i = 0; i < p_Entity.EmployeePcon.Length; i++)
                            {
                                if (p_Entity.EmployeeFile[i] != null && p_Entity.EmployeeFile[i].File != null && p_Entity.EmployeeFile[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_EMPLOYEE
                                        .Replace("{ruc}", l_ruc)
                                        .Replace("{filename}", p_Entity.EmployeeFile[i].Name));
                                }
                            }
                        }
                        //remove employee document
                        if (p_Entity.EmployeeDocumentFile != null)
                        {
                            for (int i = 0; i < p_Entity.EmployeeDocumentPcon.Length; i++)
                            {
                                if (p_Entity.EmployeeDocumentFile[i] != null && p_Entity.EmployeeDocumentFile[i].File != null && p_Entity.EmployeeDocumentFile[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_EMPLOYEE
                                        .Replace("{ruc}", l_ruc)
                                        .Replace("{filename}", p_Entity.EmployeeDocumentFile[i].Name));
                                }
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
                Helpers.ClearDataTable(l_dt);
            }
        }
        [HttpPost]
        public JsonResult EmployeeMassiveValidate(Provider_Model p_Model)
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetProviderEmployeeMassiveValidation(p_Model);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_pcdo"],//0
                        row["estado"],//1
                        row["nombres"],//2
                        row["nro_doc"],//3
                        row["fecha_ini"],//4
                        row["fecha_fin"]//5
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult EmployeeMassive(ProviderEmployeeMassive_Entity p_Entity)
        {
            Bus_List l_list = null;
            Bus_Crud l_crud;
            AzureStorageHelpers l_storage = null;
            DataTable l_dt = null;
            try
            {
                l_list = new Bus_List();
                l_crud = new Bus_Crud();
                l_storage = new AzureStorageHelpers("Azure_ContainerName_Administracion_Proveedor", BlobContainerPublicAccessType.Blob, "Azure_AccountName_Administracion", "Azure_AccessKey_Administracion");
                var l_ruc = l_list.GetProviderRUC(Convert.ToInt32(p_Entity.ProviderId));
                var l_dsupport = l_list.GetSustenanceDocumentCode(Convert.ToInt32(p_Entity.DocumentId));
                //save employee document
                p_Entity.EmployeeDocumentFileName = new string[p_Entity.EmployeeId.Length];
                p_Entity.EmployeeDocumentFileRealName = new string[p_Entity.EmployeeId.Length];
                p_Entity.EmployeeDocumentFileUploaded = new bool[p_Entity.EmployeeId.Length];
                for (int i = 0; i < p_Entity.EmployeeId.Length; i++)
                {
                    var l_didentity = l_list.GetIdentityDocumentCode(Convert.ToInt32(p_Entity.EmployeeId[i]));
                    p_Entity.EmployeeDocumentFileRealName[i] = p_Entity.DocumentFile.File.FileName;
                    p_Entity.EmployeeDocumentFileName[i] = Helpers.GetFileNameOfProviderEmployeeDocument(l_didentity, l_dsupport, p_Entity.DocumentFile.File.FileName);
                    //upload to storage
                    l_storage.UploadBlob(Helpers.DIRECTORY_PROVIDER_EMPLOYEE
                        .Replace("{ruc}", l_ruc)
                        .Replace("{filename}", p_Entity.EmployeeDocumentFileName[i]), p_Entity.DocumentFile.File.InputStream);
                    p_Entity.EmployeeDocumentFileUploaded[i] = true;
                    p_Entity.DocumentFile.File.InputStream.Position = 0;
                }
                //save in db
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                l_dt = l_crud.InsertUpdateProviderEmployeeMassive(p_Entity);
                if (l_dt.Rows[0]["Value"].ToString() != "1000")
                {
                    try
                    {
                        //remove employee document
                        for (int i = 0; i < p_Entity.EmployeeId.Length; i++)
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_EMPLOYEE
                                .Replace("{ruc}", l_ruc)
                                .Replace("{filename}", p_Entity.EmployeeDocumentFileName[i]));
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetMessageCrud(l_dt);
            }
            catch (Exception e)
            {
                if (l_storage != null)
                {
                    try
                    {
                        if (l_list == null)
                        {
                            l_list = new Bus_List();
                        }
                        var l_ruc = l_list.GetProviderRUC(Convert.ToInt32(p_Entity.ProviderId));
                        //remove employee document
                        for (int i = 0; i < p_Entity.EmployeeId.Length; i++)
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_EMPLOYEE
                                .Replace("{ruc}", l_ruc)
                                .Replace("{filename}", p_Entity.EmployeeDocumentFileName[i]));
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
                Helpers.ClearDataTable(l_dt);
            }
        }
        [HttpPost]
        public JsonResult Vehicle(ProviderVehicle_Entity p_Entity)
        {
            Bus_List l_list = null;
            Bus_Crud l_crud;
            AzureStorageHelpers l_storage = null;
            DataTable l_dt = null;
            try
            {
                l_list = new Bus_List();
                l_crud = new Bus_Crud();
                l_storage = new AzureStorageHelpers("Azure_ContainerName_Administracion_Proveedor", BlobContainerPublicAccessType.Blob, "Azure_AccountName_Administracion", "Azure_AccessKey_Administracion");
                var l_ruc = l_list.GetProviderRUC(Convert.ToInt32(p_Entity.ProviderId));
                //save photo
                if (p_Entity.VehicleFile != null)
                {
                    for (int i = 0; i < p_Entity.VehiclePrun.Length; i++)
                    {
                        if (p_Entity.VehicleFile[i] != null && p_Entity.VehicleFile[i].File != null)
                        {
                            p_Entity.VehicleFile[i].RealName = p_Entity.VehicleFile[i].File.FileName;
                            p_Entity.VehicleFile[i].Name = Helpers.GetFileNameOfProviderVehiclePhoto(p_Entity.VehicleFile[i].File.FileName);
                            p_Entity.VehicleFile[i].Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.VehicleFile[i].File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.VehicleFile[i].File.ContentLength) + "}";
                            //upload to storage
                            l_storage.UploadBlob(Helpers.DIRECTORY_PROVIDER_VEHICLE
                                .Replace("{ruc}", l_ruc)
                                .Replace("{filename}", p_Entity.VehicleFile[i].Name), p_Entity.VehicleFile[i].File.InputStream);
                            p_Entity.VehicleFile[i].Uploaded = true;
                        }
                    }
                }
                //save Vehicle document
                if (p_Entity.VehicleDocumentFile != null)
                {
                    for (int i = 0; i < p_Entity.VehicleDocumentPrun.Length; i++)
                    {
                        if (p_Entity.VehicleDocumentFile[i] != null && p_Entity.VehicleDocumentFile[i].File != null)
                        {
                            var l_dplate = l_list.GetPlateDocumentCode(Convert.ToInt32(p_Entity.VehicleDocumentPrun[i]));
                            var l_dsupport = l_list.GetSustenanceDocumentCode(Convert.ToInt32(p_Entity.VehicleDocumentTdos[i]));
                            p_Entity.VehicleDocumentFile[i].RealName = p_Entity.VehicleDocumentFile[i].File.FileName;
                            p_Entity.VehicleDocumentFile[i].Name = Helpers.GetFileNameOfProviderVehicleDocument(l_dplate, l_dsupport, p_Entity.VehicleDocumentFile[i].File.FileName);
                            p_Entity.VehicleDocumentFile[i].Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.VehicleDocumentFile[i].File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.VehicleDocumentFile[i].File.ContentLength) + "}";
                            //upload to storage
                            l_storage.UploadBlob(Helpers.DIRECTORY_PROVIDER_VEHICLE
                                .Replace("{ruc}", l_ruc)
                                .Replace("{filename}", p_Entity.VehicleDocumentFile[i].Name), p_Entity.VehicleDocumentFile[i].File.InputStream);
                            p_Entity.VehicleDocumentFile[i].Uploaded = true;
                        }
                    }
                }
                //save in db
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                l_dt = l_crud.InsertUpdateProviderVehicle(p_Entity);
                if (l_dt.Rows[0]["Value"].ToString() != "1000")
                {
                    try
                    {
                        //remove photo
                        if (p_Entity.VehicleFile != null)
                        {
                            for (int i = 0; i < p_Entity.VehiclePrun.Length; i++)
                            {
                                if (p_Entity.VehicleFile[i] != null && p_Entity.VehicleFile[i].File != null && p_Entity.VehicleFile[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_VEHICLE
                                        .Replace("{ruc}", l_ruc)
                                        .Replace("{filename}", p_Entity.VehicleFile[i].Name));
                                }
                            }
                        }
                        //remove Vehicle document
                        if (p_Entity.VehicleDocumentFile != null)
                        {
                            for (int i = 0; i < p_Entity.VehicleDocumentPrun.Length; i++)
                            {
                                if (p_Entity.VehicleDocumentFile[i] != null && p_Entity.VehicleDocumentFile[i].File != null && p_Entity.VehicleDocumentFile[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_VEHICLE
                                        .Replace("{ruc}", l_ruc)
                                        .Replace("{filename}", p_Entity.VehicleDocumentFile[i].Name));
                                }
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                else
                {
                    try
                    {
                        //remove delete
                        for (int i = 0; i < p_Entity.DeleteDocumentPcdo.Length; i++)
                        {
                            if (p_Entity.DeleteDocumentPcdo[i] != 0)
                            {
                                l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_VEHICLE
                                    .Replace("{ruc}", l_ruc)
                                    .Replace("{filename}", p_Entity.DeleteDocumentName[i]));
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetMessageCrud(l_dt);
            }
            catch (Exception e)
            {
                if (l_storage != null)
                {
                    try
                    {
                        if (l_list == null)
                        {
                            l_list = new Bus_List();
                        }
                        var l_ruc = l_list.GetProviderRUC(Convert.ToInt32(p_Entity.ProviderId));
                        //remove photo
                        if (p_Entity.VehicleFile != null)
                        {
                            for (int i = 0; i < p_Entity.VehiclePrun.Length; i++)
                            {
                                if (p_Entity.VehicleFile[i] != null && p_Entity.VehicleFile[i].File != null && p_Entity.VehicleFile[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_VEHICLE
                                        .Replace("{ruc}", l_ruc)
                                        .Replace("{filename}", p_Entity.VehicleFile[i].Name));
                                }
                            }
                        }
                        //remove Vehicle document
                        if (p_Entity.VehicleDocumentFile != null)
                        {
                            for (int i = 0; i < p_Entity.VehicleDocumentPrun.Length; i++)
                            {
                                if (p_Entity.VehicleDocumentFile[i] != null && p_Entity.VehicleDocumentFile[i].File != null && p_Entity.VehicleDocumentFile[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_VEHICLE
                                        .Replace("{ruc}", l_ruc)
                                        .Replace("{filename}", p_Entity.VehicleDocumentFile[i].Name));
                                }
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
                Helpers.ClearDataTable(l_dt);
            }
        }
        [HttpPost]
        public JsonResult VehicleMassiveValidate(Provider_Model p_Model)
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetProviderVehicleMassiveValidation(p_Model);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_pcdo"],//0
                        row["estado"],//1
                        row["nombres"],//2
                        row["nro_doc"],//3
                        row["fecha_ini"],//4
                        row["fecha_fin"]//5
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult VehicleMassive(ProviderVehicleMassive_Entity p_Entity)
        {
            Bus_List l_list = null;
            Bus_Crud l_crud;
            AzureStorageHelpers l_storage = null;
            DataTable l_dt = null;
            try
            {
                l_list = new Bus_List();
                l_crud = new Bus_Crud();
                l_storage = new AzureStorageHelpers("Azure_ContainerName_Administracion_Proveedor", BlobContainerPublicAccessType.Blob, "Azure_AccountName_Administracion", "Azure_AccessKey_Administracion");
                var l_ruc = l_list.GetProviderRUC(Convert.ToInt32(p_Entity.ProviderId));
                var l_dsupport = l_list.GetSustenanceDocumentCode(Convert.ToInt32(p_Entity.DocumentId));
                //save Vehicle document
                p_Entity.VehicleDocumentFileName = new string[p_Entity.VehicleId.Length];
                p_Entity.VehicleDocumentFileRealName = new string[p_Entity.VehicleId.Length];
                p_Entity.VehicleDocumentFileUploaded = new bool[p_Entity.VehicleId.Length];
                for (int i = 0; i < p_Entity.VehicleId.Length; i++)
                {
                    var l_didentity = l_list.GetIdentityDocumentCode(Convert.ToInt32(p_Entity.VehicleId[i]));
                    p_Entity.VehicleDocumentFileRealName[i] = p_Entity.DocumentFile.File.FileName;
                    p_Entity.VehicleDocumentFileName[i] = Helpers.GetFileNameOfProviderVehicleDocument(l_didentity, l_dsupport, p_Entity.DocumentFile.File.FileName);
                    //upload to storage
                    l_storage.UploadBlob(Helpers.DIRECTORY_PROVIDER_VEHICLE
                        .Replace("{ruc}", l_ruc)
                        .Replace("{filename}", p_Entity.VehicleDocumentFileName[i]), p_Entity.DocumentFile.File.InputStream);
                    p_Entity.VehicleDocumentFileUploaded[i] = true;
                    p_Entity.DocumentFile.File.InputStream.Position = 0;
                }
                //save in db
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                l_dt = l_crud.InsertUpdateProviderVehicleMassive(p_Entity);
                if (l_dt.Rows[0]["Value"].ToString() != "1000")
                {
                    try
                    {
                        //remove Vehicle document
                        for (int i = 0; i < p_Entity.VehicleId.Length; i++)
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_VEHICLE
                                .Replace("{ruc}", l_ruc)
                                .Replace("{filename}", p_Entity.VehicleDocumentFileName[i]));
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetMessageCrud(l_dt);
            }
            catch (Exception e)
            {
                if (l_storage != null)
                {
                    try
                    {
                        if (l_list == null)
                        {
                            l_list = new Bus_List();
                        }
                        var l_ruc = l_list.GetProviderRUC(Convert.ToInt32(p_Entity.ProviderId));
                        //remove Vehicle document
                        for (int i = 0; i < p_Entity.VehicleId.Length; i++)
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_PROVIDER_VEHICLE
                                .Replace("{ruc}", l_ruc)
                                .Replace("{filename}", p_Entity.VehicleDocumentFileName[i]));
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
                Helpers.ClearDataTable(l_dt);
            }
        }
        [HttpPost]
        public JsonResult GetList(Provider_Model p_Model, JQueryDataTableModel p_DataTable)
        {
            p_Model.Status = "X";
            p_Model.Mode = "V";
            p_Model.ProfileId = SessionHelpers.GetProfileId();
            var l_mfa = Helpers.GetParameterByKey("ESTA_MTO_ADMIN");
            Bus_List l_list = new Bus_List();
            var l_records = ConvertToProviderList(l_list.GetProviderList(p_Model));
            Func<ProviderList, string> l_ordering = (c => p_DataTable.iSortCol_0 == 2 ? c.RUC :
                p_DataTable.iSortCol_0 == 3 ? c.BusinessName :
                p_DataTable.iSortCol_0 == 4 ? c.SAPCode :
                p_DataTable.iSortCol_0 == 5 ? c.Address : c.Status);
            var l_filtereds = l_records.Where(c => c.RUC.ToLower().Contains(p_DataTable.sSearch_2 == null ? "" : p_DataTable.sSearch_2.ToLower())
                && c.BusinessName.ToLower().Contains(p_DataTable.sSearch_3 == null ? "" : p_DataTable.sSearch_3.ToLower())
                && c.SAPCode.ToLower().Contains(p_DataTable.sSearch_4 == null ? "" : p_DataTable.sSearch_4.ToLower())
                && c.Address.ToLower().Contains(p_DataTable.sSearch_5 == null ? "" : p_DataTable.sSearch_5.ToLower())
                && c.Status.ToLower().Contains(p_DataTable.sSearch_6 == null ? "" : p_DataTable.sSearch_6.ToLower()));
            if (p_DataTable.sSortDir_0 == "asc")
            {
                l_filtereds = l_filtereds.OrderBy(l_ordering);
            }
            else
            {
                l_filtereds = l_filtereds.OrderByDescending(l_ordering);
            }

            var l_result = from c in l_filtereds
                           select new[] {
                               Convert.ToString(c.Id),//0
                               c.SAPCode,//1
                               c.Name,//2
                               c.BusinessName,//3
                               c.Address,//4
                               c.RUC,//5
                               c.Approved,//6
                               c.Status,//7
                               Convert.ToString(c.StatusId),//8
                               c.StatusCode,//9
                               l_mfa.Contains("/" + c.StatusCode + "/") ? "S": "N",//10
                               c.Process,//11
                               c.Color,//12
                               Convert.ToString(c.IncotermsId),//13
                               c.FlagLogistica//14
                           };

            var l_ttl = l_records.Count();
            var l_ttlFiltereds = l_filtereds.Count();

            l_result = l_result.Skip(p_DataTable.iDisplayStart).Take(p_DataTable.iDisplayLength);

            l_list = null;
            l_records = null;
            l_filtereds = null;

            return Json(new
            {
                sEcho = p_DataTable.sEcho,
                iTotalRecords = Convert.ToString(l_ttl),
                iTotalDisplayRecords = Convert.ToString(l_ttlFiltereds),
                aaData = l_result
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetApproveDocumentList(Provider_Model p_Model, JQueryDataTableModel p_DataTable)
        {
            p_Model.Status = "X";
            p_Model.Mode = "A";
            p_Model.ProfileId = SessionHelpers.GetProfileId();
            var l_mfa = Helpers.GetParameterByKey("ESTA_MTO_ADMIN");
            Bus_List l_list = new Bus_List();
            var l_records = ConvertToProviderList(l_list.GetProviderList(p_Model));
            Func<ProviderList, string> l_ordering = (c => p_DataTable.iSortCol_0 == 2 ? c.RUC :
                p_DataTable.iSortCol_0 == 3 ? c.BusinessName :
                p_DataTable.iSortCol_0 == 4 ? c.SAPCode :
                p_DataTable.iSortCol_0 == 5 ? c.Address : c.Status);
            var l_filtereds = l_records.Where(c => c.RUC.ToLower().Contains(p_DataTable.sSearch_2 == null ? "" : p_DataTable.sSearch_2.ToLower())
                && c.BusinessName.ToLower().Contains(p_DataTable.sSearch_3 == null ? "" : p_DataTable.sSearch_3.ToLower())
                && c.SAPCode.ToLower().Contains(p_DataTable.sSearch_4 == null ? "" : p_DataTable.sSearch_4.ToLower())
                && c.Address.ToLower().Contains(p_DataTable.sSearch_5 == null ? "" : p_DataTable.sSearch_5.ToLower())
                && c.Status.ToLower().Contains(p_DataTable.sSearch_6 == null ? "" : p_DataTable.sSearch_6.ToLower()));
            if (p_DataTable.sSortDir_0 == "asc")
            {
                l_filtereds = l_filtereds.OrderBy(l_ordering);
            }
            else
            {
                l_filtereds = l_filtereds.OrderByDescending(l_ordering);
            }

            var l_result = from c in l_filtereds
                           select new[] {
                               Convert.ToString(c.Id),//0
                               c.SAPCode,//1
                               c.Name,//2
                               c.BusinessName,//3
                               c.Address,//4
                               c.RUC,//5
                               c.Approved,//6
                               c.Status,//7
                               Convert.ToString(c.StatusId),//8
                               c.StatusCode,//9
                               l_mfa.Contains(("/" + c.StatusCode + "/")) ? "S": "N",//10
                               c.Process,//11
                               c.Color//12
                           };

            var l_ttl = l_records.Count();
            var l_ttlFiltereds = l_filtereds.Count();

            l_result = l_result.Skip(p_DataTable.iDisplayStart).Take(p_DataTable.iDisplayLength);

            l_list = null;
            l_records = null;
            l_filtereds = null;

            return Json(new
            {
                sEcho = p_DataTable.sEcho,
                iTotalRecords = Convert.ToString(l_ttl),
                iTotalDisplayRecords = Convert.ToString(l_ttlFiltereds),
                aaData = l_result
            }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetPersonDataByDocument(Provider_Model p_Model)
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetProviderPersonDataByDocument(p_Model);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_pers"],//0
                        row["ap_paterno"],//1
                        row["ap_materno"],//2
                        row["nombres"],//3
                        row["codi_tdoc"],//4
                        row["nro_doc"],//5
                        Convert.IsDBNull(row["NRO_LICENCIA"]) ? "" : Convert.ToString(row["NRO_LICENCIA"])//6
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetProviderIdByRUC(Provider_Model p_Model)
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetProviderIdByRUC(p_Model);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["CODI_PROV"]//0
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult ExportToExcel(Provider_Model p_Model)
        {
            p_Model.Status = "X";
            p_Model.Mode = "V";
            p_Model.ProfileId = SessionHelpers.GetProfileId();
            Bus_List l_list = new Bus_List();
            string l_name = "Excel-" + SessionHelpers.GetUsername() + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";
            string l_path = Path.Combine(Server.MapPath("~/temp/"), l_name);
            try
            {
                var l_source = l_list.GetProviderList(p_Model);
                //remove
                l_source.Columns.Remove("codi_prov");
                l_source.Columns.Remove("nombre");
                l_source.Columns.Remove("flag_aprobado");
                l_source.Columns.Remove("codi_esta");
                l_source.Columns.Remove("abreviatura");
                l_source.Columns.Remove("proceso");
                l_source.Columns.Remove("color");
                //for status
                foreach (DataRow row in l_source.Rows)
                {
                    row["estado"] = Convert.ToString(row["estado"]) == "A" ? "ACTIVO" : "INACTIVO";
                }
                //rename
                l_source.Columns["codigo_sap"].ColumnName = "Cód. Sap";
                l_source.Columns["razon_social"].ColumnName = "Razón Social";
                l_source.Columns["direccion_fiscal"].ColumnName = "Email";
                l_source.Columns["estado"].ColumnName = "Estado";
                l_source.Columns["ruc"].ColumnName = "Ruc";
                using (XLWorkbook wb = new XLWorkbook())
                using (IXLWorksheet ws = wb.Worksheets.Add(l_source, "Proveedor"))
                {
                    wb.SaveAs(l_path);
                }
                Helpers.ClearDataTable(l_source);
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = new { name = l_name }
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
            }
        }

        [AppLog]
        public ActionResult UpdateProvider()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UpdateProvider(UpdProvider_Model p_Model)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                DataRow l_row = null;
                p_Model.DtProveedores = new DataTable();
                p_Model.DtProveedores.Columns.Add("pv_codi_prov");
                p_Model.DtProveedores.Columns.Add("pv_flag_logistica");

                foreach (var prov in p_Model.Proveedores)
                {
                    l_row = p_Model.DtProveedores.NewRow();
                    l_row["pv_codi_prov"] = prov.CodiProv;
                    l_row["pv_flag_logistica"] = prov.FlagLogistica;
                    p_Model.DtProveedores.Rows.Add(l_row);
                    l_row = null;
                }

                return Helpers.GetMessageCrud(l_crud.UpdateProvider(p_Model));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }

        #region private
        private class ProviderList
        {
            public decimal Id { get; set; }
            public string SAPCode { get; set; }
            public string Name { get; set; }
            public string BusinessName { get; set; }
            public string Address { get; set; }
            public string RUC { get; set; }
            public string Approved { get; set; }
            public string Status { get; set; }
            public decimal StatusId { get; set; }
            public string StatusCode { get; set; }
            public string Process { get; set; }
            public string Color { get; set; }
            public decimal IncotermsId { get; set; }
            public string FlagLogistica { get; set; }
        }
        private IEnumerable<ProviderList> ConvertToProviderList(DataTable p_dt)
        {
            return p_dt.AsEnumerable().Select(row => new ProviderList
            {
                Id = Convert.ToDecimal(row["codi_prov"]),
                SAPCode = row["codigo_sap"].ToString(),
                Name = row["nombre"].ToString(),
                BusinessName = row["razon_social"].ToString(),
                Address = row["direccion_fiscal"].ToString(),
                RUC = row["ruc"].ToString(),
                Approved = row["flag_aprobado"].ToString(),
                Status = row["estado"].ToString(),
                StatusId = Convert.ToDecimal(row["codi_esta"]),
                StatusCode = row["abreviatura"].ToString(),
                Process = row["proceso"].ToString(),
                Color = row["color"].ToString(),
                IncotermsId = Convert.IsDBNull(row["CODI_TERM"]) ? 0 : Convert.ToDecimal(row["CODI_TERM"]),
                FlagLogistica = row["flag_logistica"].ToString(),
            });
        }
        private IEnumerable<ProviderDocument_Model> ConvertToProviderDocument_Model(DataTable p_dt, string p_ruc = "")
        {
            var l_provider_url = Helpers.GetAppSettings("Azure_StorageUrl_Administracion_Proveedor") + Helpers.DIRECTORY_PROVIDER_COMPANY.Replace("{ruc}", p_ruc);
            var l_model_url = Helpers.GetAppSettings("Azure_StorageUrl_Administracion_Proveedor") + Helpers.DIRECTORY_DOCUMENT_MODEL.Replace("{filename}", "");
            return p_dt.AsEnumerable().Select(row => new ProviderDocument_Model
            {
                CodiTdos = Convert.ToDecimal(row["codi_tdos"]),
                Descripcion = row["descripcion"].ToString(),
                DescripcionCorta = row["descripcion_corta"].ToString(),
                Tipo = row["tipo"].ToString(),
                FlagFecha = row["flag_fecha"].ToString(),
                TipoFoto = row["tipo_foto"].ToString(),
                Estado = row["estado"].ToString(),
                Nombre = row["NOMBRE"].ToString(),
                NombreReal = row["NOMBRE_REAL"].ToString(),
                FechaEmision = Convert.IsDBNull(row["FECHA_EMISION"]) ? "" : Convert.ToDateTime(row["FECHA_EMISION"]).ToString("dd/MM/yyyy"),
                FechaVencimiento = Convert.IsDBNull(row["FECHA_VENCIMIENTO"]) ? "" : Convert.ToDateTime(row["FECHA_VENCIMIENTO"]).ToString("dd/MM/yyyy"),
                CodiPrdo = Convert.ToDecimal(row["codi_prdo"]),
                EstadoDoc = row["estado_doc"].ToString(),
                Area = row["area"].ToString(),
                Semaforo = row["semaforo"].ToString(),
                Url = l_provider_url.Replace("{filename}", Convert.ToString(row["NOMBRE"])),
                DescripcionRechazo = p_dt.Columns.Contains("DESCRIPCION_RECHAZO") ? Convert.IsDBNull(row["DESCRIPCION_RECHAZO"]) ? "" : Convert.ToString(row["DESCRIPCION_RECHAZO"]) : "",
                Modelo = p_dt.Columns.Contains("MODELO") ? Convert.IsDBNull(row["MODELO"]) ? "" : Convert.ToString(row["MODELO"]) : "",
                ModeloUrl = l_model_url
            });
        }
        private IEnumerable<ProviderDocumentLogistica_Model> ConvertToProviderDocumentLogistica_Model(DataTable p_dt)
        {
            return p_dt.AsEnumerable().Select(row => new ProviderDocumentLogistica_Model
            {
                CodiTdos = Convert.ToDecimal(row["Codi_Tdos"]),
                Descripcion = row["Descripcion"].ToString()
            });
        }
        private List<object> GetProviderEmployee(int p_id, string p_type, decimal p_profile, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_ruc = p_bus.GetProviderRUC(p_id);
                var l_url = Helpers.GetAppSettings("Azure_StorageUrl_Administracion_Proveedor") + Helpers.DIRECTORY_PROVIDER_EMPLOYEE.Replace("{ruc}", l_ruc);
                var l_records = p_bus.GetProviderEmployee(new Provider_Model
                {
                    ProviderId = Convert.ToDecimal(p_id),
                    Type = p_type,
                    ProfileId = p_profile
                });
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pcon"],//0
                        row["TipoDoc"],//1
                        row["NroDoc"],//2
                        row["Nombres"],//3
                        row["foto"],//4
                        l_url.Replace("{filename}", Convert.ToString(row["foto"])),//5
                        row["flag_tipo"],//6
                        row["vulnerable"]//7
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetProviderVehicle(int p_id, string p_type, decimal p_profile, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_ruc = p_bus.GetProviderRUC(p_id);
                var l_url = Helpers.GetAppSettings("Azure_StorageUrl_Administracion_Proveedor") + Helpers.DIRECTORY_PROVIDER_VEHICLE.Replace("{ruc}", l_ruc);
                var l_records = p_bus.GetProviderVehicle(new Provider_Model
                {
                    ProviderId = Convert.ToDecimal(p_id),
                    Type = p_type,
                    ProfileId = p_profile
                });
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_prun"],//0
                        row["descripcion"],//1
                        row["foto"],//2
                        l_url.Replace("{filename}", Convert.ToString(row["foto"])),//3
                        row["flag_tipo"]//4
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetProviderEmployeeDocumentToApproveByProfile(int p_profile, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_url = Helpers.GetAppSettings("Azure_StorageUrl_Administracion_Proveedor") + Helpers.DIRECTORY_DOCUMENT_MODEL.Replace("{filename}", "");
                var l_records = p_bus.GetProviderEmployeeDocumentToApproveByProfile(p_profile);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_tdos"],//0
                        row["descripcion"],//1
                        row["descripcion_corta"],//2
                        row["tipo"],//3
                        row["flag_fecha"],//4
                        row["tipo_foto"],//5
                        row["estado"],//6
                        row["area"],//7
                        row["flag_numeracion"],//8
                        row["FLAG_MASIVO"],//9
                        l_records.Columns.Contains("MODELO") ? Convert.IsDBNull(row["MODELO"]) ? "" : Convert.ToString(row["MODELO"]) : "",//10
                        l_url//11
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetProviderEmployeeDocument(Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_url = Helpers.GetAppSettings("Azure_StorageUrl_Administracion_Proveedor") + Helpers.DIRECTORY_DOCUMENT_MODEL.Replace("{filename}", "");
                var l_records = p_bus.GetProviderEmployeeDocument();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_tdos"],//0
                        row["descripcion"],//1
                        row["descripcion_corta"],//2
                        row["tipo"],//3
                        row["flag_fecha"],//4
                        row["tipo_foto"],//5
                        row["estado"],//6
                        row["area"],//7
                        row["flag_numeracion"],//8
                        row["FLAG_MASIVO"],//9
                        l_records.Columns.Contains("MODELO") ? Convert.IsDBNull(row["MODELO"]) ? "" : Convert.ToString(row["MODELO"]) : "",//10
                        l_url//11
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetProviderEmployeeDocumentToApproveByProfile(int p_provider, int p_employee, int p_profile, string p_url, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.GetProviderEmployeeDocumentToApproveByProfile(p_provider, p_employee, p_profile);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_pcdo"],//0
                        row["codi_tdos"],//1
                        Convert.IsDBNull(row["nombre"]) ? "" : Convert.ToString(row["nombre"]),//2
                        Convert.IsDBNull(row["propiedades"]) ? "" : Convert.ToString(row["propiedades"]),//3
                        Convert.IsDBNull(row["fecha_emision"]) ? "" : Convert.ToDateTime(row["fecha_emision"]).ToString("dd/MM/yyyy"),//4
                        Convert.IsDBNull(row["fecha_vencimiento"]) ? "" : Convert.ToDateTime(row["fecha_vencimiento"]).ToString("dd/MM/yyyy"),//5
                        Convert.IsDBNull(row["nro_doc"]) ? "" : Convert.ToString(row["nro_doc"]),//6
                        Convert.IsDBNull(row["rechazado"]) ? "" : Convert.ToString(row["rechazado"]),//7
                        Convert.IsDBNull(row["estado_doc"]) ? "" : Convert.ToString(row["estado_doc"]),//8
                        row["fecha_aprobacion"],//9
                        Convert.IsDBNull(row["semaforo"]) ? "" : Convert.ToString(row["semaforo"]),//10
                        Convert.IsDBNull(row["nombre_real"]) ? "" : Convert.ToString(row["nombre_real"]),//11
                        Convert.IsDBNull(row["flag_fecha"]) ? "" : Convert.ToString(row["flag_fecha"]),//12
                        Convert.IsDBNull(row["NRO_DOC"]) ? "" : Convert.ToString(row["NRO_DOC"]),//13
                        p_url.Replace("{filename}", Convert.IsDBNull(row["nombre"]) ? "" : Convert.ToString(row["nombre"])),//14
                        l_records.Columns.Contains("DESCRIPCION_RECHAZO") ? Convert.IsDBNull(row["DESCRIPCION_RECHAZO"]) ? "" : Convert.ToString(row["DESCRIPCION_RECHAZO"]) : ""//15
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetProviderVehicleDocumentToApproveByProfile(int p_profile, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_url = Helpers.GetAppSettings("Azure_StorageUrl_Administracion_Proveedor") + Helpers.DIRECTORY_DOCUMENT_MODEL.Replace("{filename}", "");
                var l_records = p_bus.GetProviderVehicleDocumentToApproveByProfile(p_profile);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_tdos"],//0
                        row["descripcion"],//1
                        row["descripcion_corta"],//2
                        row["tipo"],//3
                        row["flag_fecha"],//4
                        row["tipo_foto"],//5
                        row["estado"],//6
                        row["area"],//7
                        row["flag_numeracion"],//8
                        row["FLAG_MASIVO"],//9
                        l_records.Columns.Contains("MODELO") ? Convert.IsDBNull(row["MODELO"]) ? "" : Convert.ToString(row["MODELO"]) : "",//10
                        l_url//11
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetProviderVehicleDocument(Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_url = Helpers.GetAppSettings("Azure_StorageUrl_Administracion_Proveedor") + Helpers.DIRECTORY_DOCUMENT_MODEL.Replace("{filename}", "");
                var l_records = p_bus.GetProviderVehicleDocument();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_tdos"],//0
                        row["descripcion"],//1
                        row["descripcion_corta"],//2
                        row["tipo"],//3
                        row["flag_fecha"],//4
                        row["tipo_foto"],//5
                        row["estado"],//6
                        row["area"],//7
                        row["flag_numeracion"],//8
                        row["FLAG_MASIVO"],//9
                        l_records.Columns.Contains("MODELO") ? Convert.IsDBNull(row["MODELO"]) ? "" : Convert.ToString(row["MODELO"]) : "",//10
                        l_url//11
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetProviderVehicleDocumentToApproveByProfile(int p_provider, int p_vehicle, int p_profile, string p_url, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.GetProviderVehicleDocumentToApproveByProfile(p_provider, p_vehicle, p_profile);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_pcdo"],//0
                        row["codi_tdos"],//1
                        row["nombre"],//2
                        row["propiedades"],//3
                        Convert.IsDBNull(row["fecha_emision"]) ? "" : Convert.ToDateTime(row["fecha_emision"]).ToString("dd/MM/yyyy"),//4
                        Convert.IsDBNull(row["fecha_vencimiento"]) ? "" : Convert.ToDateTime(row["fecha_vencimiento"]).ToString("dd/MM/yyyy"),//5
                        row["nro_doc"],//6
                        row["rechazado"],//7
                        row["estado_doc"],//8
                        row["fecha_aprobacion"],//9
                        row["semaforo"],//10
                        row["nombre_real"],//11
                        row["flag_fecha"],//12
                        Convert.IsDBNull(row["NRO_DOC"]) ? "" : Convert.ToString(row["NRO_DOC"]),//13
                        p_url.Replace("{filename}", Convert.ToString(row["nombre"])),//14
                        l_records.Columns.Contains("DESCRIPCION_RECHAZO") ? Convert.IsDBNull(row["DESCRIPCION_RECHAZO"]) ? "" : Convert.ToString(row["DESCRIPCION_RECHAZO"]) : ""//15
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> RetrieveProviderEmails(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.RetrieveProviderEmails(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pema"],//0
                        row["Email"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> RetrieveProviderPhones(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.RetrieveProviderPhones(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["CODI_PTEL"],//0
                        row["TELEFONO"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> RetrieveProviderEmployees(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.RetrieveProviderEmployees(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["CODI_PCON"],//0
                        row["CODI_PERS"],//1
                        Convert.IsDBNull(row["AP_PATERNO"]) ? "" : row["AP_PATERNO"],//2
                        Convert.IsDBNull(row["AP_MATERNO"]) ? "" : row["AP_MATERNO"],//3
                        row["NOMBRES"],//4
                        Convert.IsDBNull(row["EMAIL"]) ? "" : row["EMAIL"],//5
                        row["CODI_TDOC"],//6
                        row["NRO_DOC"],//7
                        Convert.IsDBNull(row["CODI_CTEL"]) ? 0 : row["CODI_CTEL"],//8
                        Convert.IsDBNull(row["TELEFONO"]) ? "" : row["TELEFONO"],//9
                        Convert.IsDBNull(row["NRO_LICENCIA"]) ? "" : row["NRO_LICENCIA"],//10
                        Convert.IsDBNull(row["USERID"]) ? 0 : row["USERID"],//11
                        (Convert.IsDBNull(row["FECHA_NACIMIENTO"]) ? default(DateTime) : Convert.ToDateTime(row["FECHA_NACIMIENTO"])).ToString("dd/MM/yyyy")//12
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> RetrieveProviderVehicles(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.RetrieveProviderVehicles(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["CODI_PRUN"],//0
                        row["CODI_UNVE"],//1
                        row["NRO_PLACA"]//2
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> RetrieveProviderAccounts(int p_id, string p_ruc, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_url = Helpers.GetAppSettings("Azure_StorageUrl_Administracion_Proveedor") + Helpers.DIRECTORY_PROVIDER_COMPANY.Replace("{ruc}", p_ruc);
                var l_records = p_bus.RetrieveProviderAccounts(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["ITEM"],//0
                        row["NRO_CUENTA"],//1
                        row["CODI_TCUE"],//2
                        row["CODI_BANC"],//3
                        row["CODI_TMON"],//4
                        row["NOMBRE"],//5
                        row["NOMBRE_REAL"],//6
                        l_url.Replace("{filename}", Convert.ToString(row["NOMBRE"]))//7
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> GetProviderEmployeeTrainedLogistica(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.GetProviderEmployeeTrainedLogistica(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(row["CODI_PCON"]);
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        #endregion private
    }
}