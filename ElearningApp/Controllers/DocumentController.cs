﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class DocumentController : Controller
    {
        // GET: Document
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Create()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Edit(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            Document_Entity l_model = null;
            var l_document = l_list.RetrieveDocument(l_id);
            bool l_noData = l_document.Rows.Count == 0;
            if (!l_noData)
            {
                l_model = new Document_Entity
                {
                    Id = l_id,
                    Description = Convert.ToString(l_document.Rows[0]["Descripcion"]),
                    ShortDescription = Convert.ToString(l_document.Rows[0]["Descripcion_Corta"]),
                    Type = Convert.ToString(l_document.Rows[0]["Tipo"]),
                    Model = new File_Entity
                    {
                        Name = Convert.ToString(l_document.Rows[0]["Modelo"])
                    },
                    Order = Convert.ToDecimal(l_document.Rows[0]["Orden"]),
                    HasNumber = Convert.ToString(l_document.Rows[0]["Flag_Numeracion"]),
                    HasDates = Convert.ToString(l_document.Rows[0]["Flag_Fecha"]),
                    HasMassive = Convert.ToString(l_document.Rows[0]["Flag_Masivo"]),
                    HasLogistica = Convert.ToString(l_document.Rows[0]["Flag_Logistica"]),
                    Status = Convert.ToString(l_document.Rows[0]["Estado"])
                };
            }

            Helpers.ClearDataTable(l_document);
            l_list = null;

            ViewBag.NGon.Model = l_model;

            if (l_noData)
            {
                return View("NotFound");
            }
            else
            {
                return View(l_model);
            }
        }
        [HttpPost]
        public ActionResult Create(Document_Entity p_Entity)
        {
            Bus_Crud l_crud;
            AzureStorageHelpers l_storage = null;
            DataTable l_dt = null;
            try
            {
                l_crud = new Bus_Crud();
                l_storage = new AzureStorageHelpers("Azure_ContainerName_Administracion_Proveedor", BlobContainerPublicAccessType.Blob, "Azure_AccountName_Administracion", "Azure_AccessKey_Administracion");
                //save model
                if (p_Entity.Model.File != null)
                {
                    p_Entity.Model.Name = DateTime.Now.ToString("ddMMyyyyHHmmssffffff") + "-" + p_Entity.Model.File.FileName;
                    p_Entity.Model.Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.Model.File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.Model.File.ContentLength) + "}";
                    //upload to storage
                    l_storage.UploadBlob(Helpers.DIRECTORY_DOCUMENT_MODEL
                        .Replace("{filename}", p_Entity.Model.Name), p_Entity.Model.File.InputStream);
                    p_Entity.Model.Uploaded = true;
                }
                //save in db
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                l_dt = l_crud.InsertUpdateDocument(p_Entity);
                if (l_dt.Rows[0]["Value"].ToString() != "1000")
                {
                    try
                    {
                        if (p_Entity.Model.Uploaded)
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_DOCUMENT_MODEL
                                .Replace("{filename}", p_Entity.Model.Name));
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetMessageCrud(l_dt);
            }
            catch (Exception e)
            {
                if (l_storage != null)
                {
                    try
                    {
                        if (p_Entity.Model.Uploaded)
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_DOCUMENT_MODEL
                                .Replace("{filename}", p_Entity.Model.Name));
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
                Helpers.ClearDataTable(l_dt);
            }
        }
        [HttpPost]
        public ActionResult Edit(Document_Entity p_Entity)
        {
            Bus_Crud l_crud;
            AzureStorageHelpers l_storage = null;
            DataTable l_dt = null;
            try
            {
                l_crud = new Bus_Crud();
                l_storage = new AzureStorageHelpers("Azure_ContainerName_Administracion_Proveedor", BlobContainerPublicAccessType.Blob, "Azure_AccountName_Administracion", "Azure_AccessKey_Administracion");
                //save model
                if (p_Entity.Model.File != null)
                {
                    p_Entity.Model.Name = DateTime.Now.ToString("ddMMyyyyHHmmssffffff") + "-" + p_Entity.Model.File.FileName;
                    p_Entity.Model.Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.Model.File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.Model.File.ContentLength) + "}";
                    //upload to storage
                    l_storage.UploadBlob(Helpers.DIRECTORY_DOCUMENT_MODEL
                        .Replace("{filename}", p_Entity.Model.Name), p_Entity.Model.File.InputStream);
                    p_Entity.Model.Uploaded = true;
                }
                //save in db
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                l_dt = l_crud.InsertUpdateDocument(p_Entity);
                if (l_dt.Rows[0]["Value"].ToString() == "1000")
                {
                    try
                    {
                        if (p_Entity.Model.Name != p_Entity.Model.OldName)
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_DOCUMENT_MODEL
                                .Replace("{filename}", p_Entity.Model.OldName));
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                else
                {
                    try
                    {
                        if (p_Entity.Model.Uploaded)
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_DOCUMENT_MODEL
                                .Replace("{filename}", p_Entity.Model.Name));
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetMessageCrud(l_dt);
            }
            catch (Exception e)
            {
                if (l_storage != null)
                {
                    try
                    {
                        if (p_Entity.Model.Uploaded)
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_DOCUMENT_MODEL
                                .Replace("{filename}", p_Entity.Model.Name));
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
                Helpers.ClearDataTable(l_dt);
            }
        }
        [HttpPost]
        public ActionResult Delete(Document_Entity p_Entity)
        {
            Bus_Crud l_crud;
            Bus_List l_list;
            AzureStorageHelpers l_storage = null;
            try
            {
                l_crud = new Bus_Crud();
                l_list = new Bus_List();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                var l_document = l_list.RetrieveDocument(Convert.ToInt32(p_Entity.Id));
                var l_response = l_crud.DeleteDocument(p_Entity);
                if (l_response.Rows[0]["Value"].ToString() == "1000")
                {
                    l_storage = new AzureStorageHelpers("Azure_ContainerName_Administracion_Proveedor", BlobContainerPublicAccessType.Blob, "Azure_AccountName_Administracion", "Azure_AccessKey_Administracion");
                    try
                    {
                        if (l_document.Rows[0]["Modelo"].ToString() != "")
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_DOCUMENT_MODEL
                                .Replace("{filename}", l_document.Rows[0]["Modelo"].ToString()));
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }

                Helpers.ClearDataTable(l_document);

                return Helpers.GetMessageCrud(l_response);
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
            }
        }
        [HttpPost]
        public JsonResult GetList()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetDocumentList();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Tdos"],//0
                        row["Descripcion"],//1
                        row["Flag_Fecha"],//2
                        row["Flag_Numeracion"],//3
                        row["Flag_Masivo"],//4
                        row["Orden"],//5
                        row["Estado"],//6
                        row["Tipo"]//7
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
    }
}
