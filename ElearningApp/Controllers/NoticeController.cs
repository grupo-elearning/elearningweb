﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class NoticeController : Controller
    {
        // GET: Notice
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Create()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Edit(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            AzureStorageHelpers l_storage = new AzureStorageHelpers("Azure_ContainerName_ELearning");
            Notice_Entity l_model = null;
            var l_notice = l_list.RetrieveNotice(l_id);
            bool l_noData = l_notice.Rows.Count == 0;
            if (!l_noData)
            {
                l_model = new Notice_Entity
                {
                    Id = l_id,
                    Description = l_notice.Rows[0]["Descripcion"].ToString(),
                    Status = l_notice.Rows[0]["Estado"].ToString(),
                    Html = new File_Entity
                    {
                        Name = l_notice.Rows[0]["Nombre_Html"].ToString()
                    }
                };
                //get content
                l_model.Content = Encoding.UTF8.GetString(l_storage.DownloadBlob(Helpers.DIRECTORY_NOTICE.Replace("{filename}", l_model.Html.Name)).ToArray());
                var l_elearning = l_model.Content.IndexOf("data-elearning=\"notice\"");
                l_model.Content = l_model.Content.Substring(l_elearning + 24, l_model.Content.Length - (l_elearning + 38));
            }

            Helpers.ClearDataTable(l_notice);
            l_storage.Clear();
            l_storage = null;
            l_list = null;

            if (l_noData)
            {
                return View("NotFound");
            }
            else
            {
                return View(l_model);
            }
        }
        [HttpPost]
        public ActionResult Create(Notice_Entity p_Entity)
        {
            Bus_Crud l_crud;
            AzureStorageHelpers l_storage = null;
            DataTable l_dt = null;
            try
            {
                l_crud = new Bus_Crud();
                l_storage = new AzureStorageHelpers("Azure_ContainerName_ELearning");
                //save html
                p_Entity.Html.Name = DateTime.Now.ToString("ddMMyyyyHHmmssffffff") + "-html-notice.html";
                p_Entity.Html.Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.Html.File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.Html.File.ContentLength) + "}";
                //upload to storage
                l_storage.UploadBlob(Helpers.DIRECTORY_NOTICE
                    .Replace("{filename}", p_Entity.Html.Name), p_Entity.Html.File.InputStream, p_Entity.Html.File.ContentType);
                p_Entity.Html.Uploaded = true;
                //save in db
                p_Entity.Url = string.Concat(Helpers.GetAppSettings("Azure_StorageUrl_ELearning"), Helpers.DIRECTORY_NOTICE
                    .Replace("{filename}", p_Entity.Html.Name));
                p_Entity.AreaId = SessionHelpers.GetProfile().AreaId;
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                l_dt = l_crud.InsertUpdateNotice(p_Entity);
                if (l_dt.Rows[0]["Value"].ToString() != "1000")
                {
                    try
                    {
                        if (p_Entity.Html.Uploaded)
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_NOTICE
                                .Replace("{filename}", p_Entity.Html.Name));
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetMessageCrud(l_dt);
            }
            catch (Exception e)
            {
                if (l_storage != null)
                {
                    try
                    {
                        if (p_Entity.Html.Uploaded)
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_NOTICE
                                .Replace("{filename}", p_Entity.Html.Name));
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
                Helpers.ClearDataTable(l_dt);
            }
        }
        [HttpPost]
        public ActionResult Edit(Notice_Entity p_Entity)
        {
            Bus_Crud l_crud;
            AzureStorageHelpers l_storage = null;
            DataTable l_dt = null;
            try
            {
                l_crud = new Bus_Crud();
                l_storage = new AzureStorageHelpers("Azure_ContainerName_ELearning");
                //save html
                p_Entity.Html.Name = DateTime.Now.ToString("ddMMyyyyHHmmssffffff") + "-html-notice.html";
                p_Entity.Html.Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.Html.File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.Html.File.ContentLength) + "}";
                //upload to storage
                l_storage.UploadBlob(Helpers.DIRECTORY_NOTICE
                    .Replace("{filename}", p_Entity.Html.Name), p_Entity.Html.File.InputStream, p_Entity.Html.File.ContentType);
                p_Entity.Html.Uploaded = true;
                //save in db
                p_Entity.Url = string.Concat(Helpers.GetAppSettings("Azure_StorageUrl_ELearning"), Helpers.DIRECTORY_NOTICE
                        .Replace("{filename}", p_Entity.Html.Name));
                p_Entity.AreaId = SessionHelpers.GetProfile().AreaId;
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                l_dt = l_crud.InsertUpdateNotice(p_Entity);
                if (l_dt.Rows[0]["Value"].ToString() == "1000")
                {
                    try
                    {
                        l_storage.DeleteBlob(Helpers.DIRECTORY_NOTICE
                            .Replace("{filename}", p_Entity.Html.OldName));
                    }
                    catch
                    {
                        //prevent
                    }
                }
                else
                {
                    try
                    {
                        if (p_Entity.Html.Uploaded)
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_NOTICE
                                .Replace("{filename}", p_Entity.Html.Name));
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetMessageCrud(l_dt);
            }
            catch (Exception e)
            {
                if (l_storage != null)
                {
                    try
                    {
                        if (p_Entity.Html.Uploaded)
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_NOTICE
                                .Replace("{filename}", p_Entity.Html.Name));
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
                Helpers.ClearDataTable(l_dt);
            }
        }
        [HttpPost]
        public ActionResult Delete(Notice_Entity p_Entity)
        {
            Bus_Crud l_crud;
            AzureStorageHelpers l_storage = null;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                var l_response = l_crud.DeleteNotice(p_Entity);
                if (l_response.Rows[0]["Value"].ToString() == "1000")
                {
                    l_storage = new AzureStorageHelpers("Azure_ContainerName_ELearning");
                    try
                    {
                        l_storage.DeleteBlob(Helpers.DIRECTORY_NOTICE
                            .Replace("{filename}", p_Entity.Html.Name));
                    }
                    catch
                    {
                        //prevent
                    }
                }

                return Helpers.GetMessageCrud(l_response);
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
            }
        }
        [HttpPost]
        public JsonResult GetList()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetNoticeList(Convert.ToInt32(SessionHelpers.GetProfile().AreaId));
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Anun"],//0
                        row["Descripcion"],//1
                        row["Nombre_Html"],//2
                        row["Url"],//3
                        row["Estado"]//4
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
    }
}
