﻿using ClosedXML.Excel;
using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class Dashboard4Controller : Controller
    {
        // GET: Dashboard4
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetDashboard4()
        {
            Bus_Report l_report;
            try
            {
                l_report = new Bus_Report();
                var l_model = ConvertToDashboard4ProviderModelList(l_report.Dashboard4GetProvider(Convert.ToInt32(SessionHelpers.GetProfile().AreaId))).ToList();
                foreach (Dashboard4ProviderModel provider in l_model)
                {
                    var l_data_source = ConvertToDashboard4DataSourceList(l_report.Dashboard4GetProviderData(provider.Id)).ToList();
                    //provider document
                    foreach (var document in l_data_source.GroupBy(e => e.Codi_Tdoc_Prov).Select(e => e.FirstOrDefault()).ToList())
                    {
                        provider.Dc.Add(new Dashboard4DocumentModel
                        {
                            Id = document.Codi_Tdoc_Prov,
                            Nm = document.Descripcion_Doc_Proveedor,
                            St = document.Estado_Doc_Prov
                        });
                    }
                    //remove empty DC
                    provider.Dc = provider.Dc.Where(e => e.Id != 0).ToList();
                    //provider employee
                    foreach (var employee in l_data_source.GroupBy(e => e.Codi_Pcon).Select(e => e.FirstOrDefault()).ToList())
                    {
                        var l_employee = new Dashboard4ProviderEmployeeModel
                        {
                            Id = employee.Codi_Pcon,
                            Nd = employee.Nro_Doc,
                            Fn = employee.Trabajador,
                            Fi = employee.Fecha_Ingreso,
                            Dc = new List<Dashboard4DocumentModel>()
                        };
                        foreach (var document in l_data_source.Where(e => e.Codi_Pcon == l_employee.Id).GroupBy(e => e.Codi_Tdoc_Trab).Select(e => e.FirstOrDefault()).ToList())
                        {
                            l_employee.Dc.Add(new Dashboard4DocumentModel
                            {
                                Id = document.Codi_Tdoc_Trab,
                                Nm = document.Descripcion_Doc_Trabajador,
                                St = document.Estado_Doc_Trab
                            });
                        }
                        //remove empty DC
                        l_employee.Dc = l_employee.Dc.Where(e => e.Id != 0).ToList();
                        //
                        provider.Ep.Add(l_employee);
                    }
                    //remove empty Ep
                    provider.Ep = provider.Ep.Where(e => e.Id != 0).ToList();
                    //provider vehicle
                    foreach (var vehicle in l_data_source.GroupBy(e => e.Codi_Prun).Select(e => e.FirstOrDefault()).ToList())
                    {
                        var l_vehicle = new Dashboard4ProviderVehicleModel
                        {
                            Id = vehicle.Codi_Prun,
                            Nm = vehicle.Unidad,
                            Dc = new List<Dashboard4DocumentModel>()
                        };
                        foreach (var document in l_data_source.Where(e => e.Codi_Prun == l_vehicle.Id).GroupBy(e => e.Codi_Tdoc_Unidad).Select(e => e.FirstOrDefault()).ToList())
                        {
                            l_vehicle.Dc.Add(new Dashboard4DocumentModel
                            {
                                Id = document.Codi_Tdoc_Unidad,
                                Nm = document.Descripcion_Doc_Unidad,
                                St = document.Estado_Doc_Unid
                            });
                        }
                        //remove empty DC
                        l_vehicle.Dc = l_vehicle.Dc.Where(e => e.Id != 0).ToList();
                        //
                        provider.Vc.Add(l_vehicle);
                    }
                    //remove empty Vc
                    provider.Vc = provider.Vc.Where(e => e.Id != 0).ToList();
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_model
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_report = null;
            }
        }
        [HttpPost]
        public JsonResult ExportToExcel(Dashboard4_Model p_Model)
        {
            string l_name = "Excel-" + SessionHelpers.GetUsername() + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";
            string l_path = Path.Combine(Server.MapPath("~/temp/"), l_name);
            try
            {
                using (XLWorkbook wb = new XLWorkbook())
                using (IXLWorksheet ws = wb.Worksheets.Add("Dashboard4"))
                {
                    //build table
                    if (p_Model.Export == "all")
                    {
                        ws.Cell("A1").Value = "CODIGO SAP";
                        ws.Cell("B1").Value = "TRANSPORTISTA";
                        ws.Cell("C1").Value = "DOCUMENTO";
                        ws.Cell("D1").Value = "DOCUMENTO ESTADO";
                        ws.Cell("E1").Value = "N° DOC";
                        ws.Cell("F1").Value = "TRABAJADOR";
                        ws.Cell("G1").Value = "FECHA INGRESO";
                        ws.Cell("H1").Value = "TRABAJADOR DOC.";
                        ws.Cell("I1").Value = "TRABAJADOR DOC. ESTADO";
                        ws.Cell("J1").Value = "VEHICULO";
                        ws.Cell("K1").Value = "VEHICULO DOC.";
                        ws.Cell("L1").Value = "VEHICULO DOC. ESTADO";
                        //body
                        for (int i = 0; i < p_Model.Pro.Length; i++)
                        {
                            ws.Cell("A" + (i + 2)).Value = p_Model.Ruc[i];
                            ws.Cell("B" + (i + 2)).Value = p_Model.Pro[i];
                            ws.Cell("C" + (i + 2)).Value = p_Model.Pdo[i];
                            ws.Cell("D" + (i + 2)).Value = p_Model.Pds[i];
                            ws.Cell("E" + (i + 2)).Value = p_Model.End[i];
                            ws.Cell("F" + (i + 2)).Value = p_Model.Emp[i];
                            ws.Cell("G" + (i + 2)).Value = p_Model.Efi[i];
                            ws.Cell("H" + (i + 2)).Value = p_Model.Edo[i];
                            ws.Cell("I" + (i + 2)).Value = p_Model.Eds[i];
                            ws.Cell("J" + (i + 2)).Value = p_Model.Veh[i];
                            ws.Cell("K" + (i + 2)).Value = p_Model.Vdo[i];
                            ws.Cell("L" + (i + 2)).Value = p_Model.Vds[i];
                        }
                    }
                    else if (p_Model.Export == "provider")
                    {
                        ws.Cell("A1").Value = "DOCUMENTO";
                        ws.Cell("B1").Value = "DOCUMENTO ESTADO";
                        ws.Cell("C1").Value = "N° DOC";
                        ws.Cell("D1").Value = "TRABAJADOR";
                        ws.Cell("E1").Value = "FECHA INGRESO";
                        ws.Cell("F1").Value = "TRABAJADOR DOC.";
                        ws.Cell("G1").Value = "TRABAJADOR DOC. ESTADO";
                        ws.Cell("H1").Value = "VEHICULO";
                        ws.Cell("I1").Value = "VEHICULO DOC.";
                        ws.Cell("J1").Value = "VEHICULO DOC. ESTADO";
                        //body
                        for (int i = 0; i < p_Model.Pro.Length; i++)
                        {
                            ws.Cell("A" + (i + 2)).Value = p_Model.Pdo[i];
                            ws.Cell("B" + (i + 2)).Value = p_Model.Pds[i];
                            ws.Cell("C" + (i + 2)).Value = p_Model.End[i];
                            ws.Cell("D" + (i + 2)).Value = p_Model.Emp[i];
                            ws.Cell("E" + (i + 2)).Value = p_Model.Efi[i];
                            ws.Cell("F" + (i + 2)).Value = p_Model.Edo[i];
                            ws.Cell("G" + (i + 2)).Value = p_Model.Eds[i];
                            ws.Cell("H" + (i + 2)).Value = p_Model.Veh[i];
                            ws.Cell("I" + (i + 2)).Value = p_Model.Vdo[i];
                            ws.Cell("J" + (i + 2)).Value = p_Model.Vds[i];
                        }
                    }
                    else if (p_Model.Export == "type")
                    {
                        if (p_Model.Type == "company")
                        {
                            ws.Cell("A1").Value = "DOCUMENTO";
                            ws.Cell("B1").Value = "DOCUMENTO ESTADO";
                            //body
                            for (int i = 0; i < p_Model.Pro.Length; i++)
                            {
                                ws.Cell("A" + (i + 2)).Value = p_Model.Pdo[i];
                                ws.Cell("B" + (i + 2)).Value = p_Model.Pds[i];
                            }
                        }
                        else if (p_Model.Type == "employee")
                        {
                            int l_row = 1, l_column = 1;
                            var l_data_source = ConvertToExportRowList(p_Model);
                            ws.Cell(l_row, l_column).Value = "N° DOC";
                            l_column++;
                            ws.Cell(l_row, l_column).Value = "TRABAJADOR";
                            l_column++;
                            ws.Cell(l_row, l_column).Value = "FECHA INGRESO";
                            //title
                            foreach (var document in l_data_source.GroupBy(e => e.Edo).Select(e => e.FirstOrDefault()).OrderBy(e => e.Edo).ToList())
                            {
                                l_column++;
                                ws.Cell(l_row, l_column).Value = document.Edo;
                            }
                            //body
                            foreach (var employee in l_data_source.GroupBy(e => e.Eid).Select(e => e.FirstOrDefault()).ToList())
                            {
                                l_column = 1;
                                l_row++;
                                ws.Cell(l_row, l_column).Value = employee.End;
                                l_column++;
                                ws.Cell(l_row, l_column).Value = employee.Emp;
                                l_column++;
                                ws.Cell(l_row, l_column).Value = employee.Efi;
                                foreach (var document in l_data_source.Where(e => e.Eid == employee.Eid).GroupBy(e => e.Edo).Select(e => e.FirstOrDefault()).OrderBy(e => e.Edo).ToList())
                                {
                                    l_column++;
                                    ws.Cell(l_row, l_column).Value = document.Eds;
                                }
                            }
                        }
                        else if (p_Model.Type == "vehicle")
                        {
                            int l_row = 1, l_column = 1;
                            var l_data_source = ConvertToExportRowList(p_Model);
                            ws.Cell(l_row, l_column).Value = "VEHICULO";
                            //title
                            foreach (var document in l_data_source.GroupBy(e => e.Vdo).Select(e => e.FirstOrDefault()).OrderBy(e => e.Vdo).ToList())
                            {
                                l_column++;
                                ws.Cell(l_row, l_column).Value = document.Vdo;
                            }
                            //body
                            foreach (var vehicle in l_data_source.GroupBy(e => e.Vid).Select(e => e.FirstOrDefault()).ToList())
                            {
                                l_column = 1;
                                l_row++;
                                ws.Cell(l_row, l_column).Value = vehicle.Veh;
                                foreach (var document in l_data_source.Where(e => e.Vid == vehicle.Vid).GroupBy(e => e.Vdo).Select(e => e.FirstOrDefault()).OrderBy(e => e.Vdo).ToList())
                                {
                                    l_column++;
                                    ws.Cell(l_row, l_column).Value = document.Vds;
                                }
                            }
                        }
                    }
                    //end build table
                    //set theme
                    if (p_Model.Theme == "dalba")
                    {
                        var l_rows = Convert.ToString(ws.RowsUsed().Count());
                        if (p_Model.Export == "all")
                        {
                            AddStatus(ws.Range("D2:D" + l_rows));//provider
                            AddStatus(ws.Range("I2:I" + l_rows));//employee
                            AddStatus(ws.Range("L2:L" + l_rows));//vehicle
                        }
                        else if (p_Model.Export == "provider")
                        {
                            AddStatus(ws.Range("B2:B" + l_rows));//provider
                            AddStatus(ws.Range("F2:F" + l_rows));//employee
                            AddStatus(ws.Range("J2:J" + l_rows));//vehicle
                        }
                        else if (p_Model.Export == "type")
                        {
                            if (p_Model.Type == "company")
                            {
                                AddStatus(ws.Range("B2:B" + l_rows));//provider
                            }
                            else if (p_Model.Type == "employee")
                            {
                                AddStatus(ws.Range(2, 2, ws.RowsUsed().Count(), ws.ColumnsUsed().Count()));//employee
                            }
                            else if (p_Model.Type == "vehicle")
                            {
                                AddStatus(ws.Range(2, 2, ws.RowsUsed().Count(), ws.ColumnsUsed().Count()));//vehicle
                            }
                        }
                        var l_table = ws.RangeUsed().CreateTable();
                        l_table.Theme = XLTableTheme.TableStyleLight9;
                    }
                    else
                    {
                        ws.RangeUsed().SetAutoFilter();
                    }
                    //end set theme
                    ws.Columns().AdjustToContents();
                    wb.SaveAs(l_path);
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = new { name = l_name }
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
        }
        #region private
        private void AddStatus(IXLRange p_rng)
        {
            p_rng
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            p_rng
                .AddConditionalFormat()
                .IconSet(XLIconSetStyle.ThreeTrafficLights1, false, true)
                .AddValue(XLCFIconSetOperator.EqualOrGreaterThan, 0, XLCFContentType.Number)
                .AddValue(XLCFIconSetOperator.EqualOrGreaterThan, 1, XLCFContentType.Number)
                .AddValue(XLCFIconSetOperator.EqualOrGreaterThan, 1, XLCFContentType.Number);
        }
        private List<ExportRow> ConvertToExportRowList(Dashboard4_Model p_Model)
        {
            var l_list = new List<ExportRow>();
            for (int i = 0; i < p_Model.Pid.Length; i++)
            {
                l_list.Add(new ExportRow
                {
                    Pid = p_Model.Pid[i],
                    Pro = p_Model.Pro[i],
                    Pdo = p_Model.Pdo[i],
                    Pds = p_Model.Pds[i],
                    Eid = p_Model.Eid[i],
                    End = p_Model.End[i],
                    Emp = p_Model.Emp[i],
                    Efi = p_Model.Efi[i],
                    Edo = p_Model.Edo[i],
                    Eds = p_Model.Eds[i],
                    Vid = p_Model.Vid[i],
                    Veh = p_Model.Veh[i],
                    Vdo = p_Model.Vdo[i],
                    Vds = p_Model.Vds[i]
                });
            }
            return l_list;
        }
        private IEnumerable<Dashboard4ProviderModel> ConvertToDashboard4ProviderModelList(DataTable p_dt)
        {
            return p_dt.AsEnumerable().Select(row => new Dashboard4ProviderModel
            {
                Id = Convert.ToInt32(row["Codi_Prov"]),
                Ru = Convert.ToString(row["CODIGO_SAP"]),
                Bn = Convert.ToString(row["Razon_Social"]),
                Dc = new List<Dashboard4DocumentModel>(),
                Ep = new List<Dashboard4ProviderEmployeeModel>(),
                Vc = new List<Dashboard4ProviderVehicleModel>()
            });
        }
        private IEnumerable<Dashboard4DataSource> ConvertToDashboard4DataSourceList(DataTable p_dt)
        {
            return p_dt.AsEnumerable().Select(row => new Dashboard4DataSource
            {
                Codi_Tdoc_Prov = Convert.IsDBNull(row["codi_tdoc_prov"]) ? 0 : Convert.ToDecimal(row["codi_tdoc_prov"]),
                Descripcion_Doc_Proveedor = Convert.IsDBNull(row["descripcion_doc_proveedor"]) ? "" : Convert.ToString(row["descripcion_doc_proveedor"]),
                Estado_Doc_Prov = Convert.IsDBNull(row["estado_doc_prov"]) ? 0 : Convert.ToDecimal(row["estado_doc_prov"]),
                Codi_Pcon = Convert.IsDBNull(row["codi_pcon"]) ? 0 : Convert.ToDecimal(row["codi_pcon"]),
                Nro_Doc = Convert.IsDBNull(row["nro_doc"]) ? "" : Convert.ToString(row["nro_doc"]),
                Trabajador = Convert.IsDBNull(row["trabajador"]) ? "" : Convert.ToString(row["trabajador"]),
                Fecha_Ingreso = Convert.IsDBNull(row["fecha_ingreso"]) ? "" : Convert.ToString(row["fecha_ingreso"]),
                Codi_Tdoc_Trab = Convert.IsDBNull(row["codi_tdoc_trab"]) ? 0 : Convert.ToDecimal(row["codi_tdoc_trab"]),
                Descripcion_Doc_Trabajador = Convert.IsDBNull(row["descripcion_doc_trabajador"]) ? "" : Convert.ToString(row["descripcion_doc_trabajador"]),
                Estado_Doc_Trab = Convert.IsDBNull(row["estado_doc_trab"]) ? 0 : Convert.ToDecimal(row["estado_doc_trab"]),
                Codi_Prun = Convert.IsDBNull(row["codi_prun"]) ? 0 : Convert.ToDecimal(row["codi_prun"]),
                Unidad = Convert.IsDBNull(row["unidad"]) ? "" : Convert.ToString(row["unidad"]),
                Codi_Tdoc_Unidad = Convert.IsDBNull(row["codi_tdoc_unidad"]) ? 0 : Convert.ToDecimal(row["codi_tdoc_unidad"]),
                Descripcion_Doc_Unidad = Convert.IsDBNull(row["descripcion_doc_unidad"]) ? "" : Convert.ToString(row["descripcion_doc_unidad"]),
                Estado_Doc_Unid = Convert.IsDBNull(row["estado_doc_unid"]) ? 0 : Convert.ToDecimal(row["estado_doc_unid"])
            });
        }
        private class ExportRow
        {
            public decimal Pid { get; set; }
            public string Pro { get; set; }
            public string Pdo { get; set; }
            public string Pds { get; set; }
            public decimal Eid { get; set; }
            public string End { get; set; }
            public string Emp { get; set; }
            public string Efi { get; set; }
            public string Edo { get; set; }
            public string Eds { get; set; }
            public decimal Vid { get; set; }
            public string Veh { get; set; }
            public string Vdo { get; set; }
            public string Vds { get; set; }
        }
        #endregion private
    }
}