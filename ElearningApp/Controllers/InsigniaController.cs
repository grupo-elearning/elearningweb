﻿using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ElearningApp.Filters;
using ElearningApp.Models;
using Microsoft.WindowsAzure.Storage.Blob;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class InsigniaController : Controller
    {
        // GET: Insignia
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            ViewBag.NGon.token_access = Helpers.LoginApiEleaLms();
            ViewBag.NGon.username = SessionHelpers.GetUsername();
            ViewBag.NGon.ipaddress = SessionHelpers.GetIPAddress();
            ViewBag.NGon.UrlApi_insign_list = System.Configuration.ConfigurationManager.AppSettings["UrlApi_insign_list"];
            ViewBag.NGon.UrlApi_insign_delete = System.Configuration.ConfigurationManager.AppSettings["UrlApi_insign_delete"];
            return View();
        }

        [AppAuthorize]
        [AppLog]
        public ActionResult Add()
        {
            ViewBag.NGon.token_access = Helpers.LoginApiEleaLms();
            ViewBag.NGon.username = SessionHelpers.GetUsername();
            ViewBag.NGon.ipaddress = SessionHelpers.GetIPAddress();
            ViewBag.NGon.UrlApi_insign_saveUpd = System.Configuration.ConfigurationManager.AppSettings["UrlApi_insign_saveUpd"];
            return View();
        }

        [AppAuthorize]
        [AppLog]
        public ActionResult Edit(string id)
        {
            ViewBag.NGon.token_access = Helpers.LoginApiEleaLms();
            ViewBag.NGon.username = SessionHelpers.GetUsername();
            ViewBag.NGon.ipaddress = SessionHelpers.GetIPAddress();
            ViewBag.NGon.UrlApi_insign_saveUpd = System.Configuration.ConfigurationManager.AppSettings["UrlApi_insign_saveUpd"];
            ViewBag.NGon.UrlApi_insign_id = System.Configuration.ConfigurationManager.AppSettings["UrlApi_insign_id"];
            ViewBag.NGon.Codi_Lmin = id;
            return View();
        }

        [AppAuthorize]
        [AppLog]
        public ActionResult Reglas()
        {
            ViewBag.NGon.token_access = Helpers.LoginApiEleaLms();
            ViewBag.NGon.username = SessionHelpers.GetUsername();
            ViewBag.NGon.ipaddress = SessionHelpers.GetIPAddress();
            ViewBag.NGon.UrlApi_insign_listRegla = System.Configuration.ConfigurationManager.AppSettings["UrlApi_insign_listRegla"];
            ViewBag.NGon.UrlApi_insign_listCriterio = System.Configuration.ConfigurationManager.AppSettings["UrlApi_insign_listCriterio"];
            ViewBag.NGon.UrlApi_insign_listCondicion = System.Configuration.ConfigurationManager.AppSettings["UrlApi_insign_listCondicion"];
            ViewBag.NGon.UrlApi_insign_saveUpdRegla = System.Configuration.ConfigurationManager.AppSettings["UrlApi_insign_saveUpdRegla"];
            ViewBag.NGon.UrlApi_insign_deleteRegla = System.Configuration.ConfigurationManager.AppSettings["UrlApi_insign_deleteRegla"];
            return View();
        }

    }
}