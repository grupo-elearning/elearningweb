﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class PersonController : Controller
    {
        // GET: Person
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetList()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetPersonList();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Documento"],//0
                        row["Nro_Doc"],//1
                        row["Nombre"],//2
                        row["Codi_User"],//3
                        row["Pass_User"],//4
                        row["Codi_Pers"],//5
                        row["email"],//6
                        row["codi_tdoc"],//7
                        row["celular"]//8
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult Send(Person_Model p_Model)
        {
            try
            {
                EmailHelpers.SendCredentialsAgain(p_Model);
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = "Se envío correctamente"
                    },
                    MaxJsonLength = int.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
        }
      
        public ActionResult RegisterModal(PersonView_Model p_model)//PersonView_Model
        {
            ViewBag.tipoDoc = p_model.tipoDoc;
            ViewBag.nroDoc = p_model.nroDoc;
            ViewBag.codiPers = p_model.codiPers;
            return PartialView("_RegisterModalPartialView");
        }

        public ActionResult SavePerson(Register_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.IpAddress = RequestHelpers.GetClientIpAddress(HttpContext.Request);
                var l_response = l_crud.SavePerson(p_Entity);
                return Helpers.GetMessageCrud(l_response);
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
    }
}