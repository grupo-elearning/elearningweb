﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class QuestionController : Controller
    {
        // GET: Question
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Create()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Edit(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            Question_Entity l_model = null;
            var l_question = l_list.RetrieveQuestion(l_id);
            bool l_noData = l_question.Rows.Count == 0;
            if (!l_noData)
            {
                l_model = new Question_Entity
                {
                    Id = l_id,
                    CourseId = Convert.ToDecimal(l_question.Rows[0]["Codi_Curs"]),
                    TypeId = Convert.ToDecimal(l_question.Rows[0]["Codi_Tpre"]),
                    TypeMode = Convert.ToString(l_question.Rows[0]["Tipo"]),
                    Question = Convert.ToString(l_question.Rows[0]["Pregunta"]),
                    Status = Convert.ToString(l_question.Rows[0]["Estado"]),
                    Answers = RetrieveAnswer(l_id, l_list)
                };
            }

            Helpers.ClearDataTable(l_question);
            l_list = null;

            ViewBag.NGon.Model = l_model;

            if (l_noData)
            {
                return View("NotFound");
            }
            else
            {
                return View(l_model);
            }
        }
        [HttpPost]
        public ActionResult Create(Question_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Id = 0;
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.InsertUpdateQuestion(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public ActionResult Edit(Question_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.InsertUpdateQuestion(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public ActionResult Delete(Question_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.DeleteQuestion(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public ActionResult GetList()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetQuestionList(Convert.ToInt32(SessionHelpers.GetProfile().AreaId));
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_preg"],//0
                        row["curso"],//1
                        row["area"],//2
                        Convert.ToString(row["pregunta"]).Replace("\n", "<br />"),//3
                        row["tipo_pregunta"],//4
                        row["estado"]//5
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        #region private
        private List<object> RetrieveAnswer(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.RetrieveQuestionAnswer(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pres"],//0
                        row["Respuesta"],//1
						row["Correcta"],//2
                        row["Orden"]//3
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        #endregion private
    }
}
