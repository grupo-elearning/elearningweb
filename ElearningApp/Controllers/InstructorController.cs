﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class InstructorController : Controller
    {
        // GET: Instructor
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Create()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Edit(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            Instructor_Entity l_model = null;
            var l_instructor = l_list.RetrieveInstructor(l_id);
            bool l_noData = l_instructor.Rows.Count == 0;
            if (!l_noData)
            {
                l_model = new Instructor_Entity
                {
                    Id = l_id,
                    Type = l_instructor.Rows[0]["Tipo"].ToString(),
                    PersonId = Convert.ToDecimal(l_instructor.Rows[0]["Codi_Pers"]),
                    Status = l_instructor.Rows[0]["Estado"].ToString(),
                    LastName1 = l_instructor.Rows[0]["Ap_Paterno"].ToString(),
                    LastName2 = l_instructor.Rows[0]["Ap_Materno"].ToString(),
                    FirstName = l_instructor.Rows[0]["Nombres"].ToString(),
                    DocumentTypeId = Convert.ToDecimal(l_instructor.Rows[0]["Codi_Tdoc"]),
                    DocumentNumber = l_instructor.Rows[0]["Nro_Doc"].ToString(),
                    Email = l_instructor.Rows[0]["Email"].ToString(),
                    PersonType = l_instructor.Rows[0]["Persona_Tipo"].ToString()
                };
            }

            Helpers.ClearDataTable(l_instructor);
            l_list = null;

            ViewBag.NGon.Model = l_model;

            if (l_noData)
            {
                return View("NotFound");
            }
            else
            {
                return View(l_model);
            }
        }
        [HttpPost]
        public ActionResult Create(Instructor_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Id = 0;
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.InsertUpdateInstructor(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public ActionResult Edit(Instructor_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.InsertUpdateInstructor(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public ActionResult Delete(Instructor_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.DeleteInstructor(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public JsonResult GetList()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetInstructorList();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Inst"],//0
                        row["Documento"],//1
                        row["Nombre"],//2
                        row["Email"],//3
                        row["Tipo"],//4
                        row["Estado"]//5
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetPerson(int document, string number)
        {
            Bus_List l_list;
            List<object> l_result = null;
            try
            {
                l_list = new Bus_List();
                var l_records = l_list.GetPerson2Instructor(document, number);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result = new List<object>
                    {
                        row["Codi_Pers"],//0
                        row["Ap_Paterno"],//1
                        row["Ap_Materno"],//2
                        row["Nombres"],//3
                        row["Email"],//4
                        row["Tipo"]//5
                    };
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
    }
}
