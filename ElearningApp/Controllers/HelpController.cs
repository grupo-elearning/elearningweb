﻿using ElearningApp.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class HelpController : Controller
    {
        // GET: Help
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            return View();
        }
    }
}