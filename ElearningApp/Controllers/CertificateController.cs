﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class CertificateController : Controller
    {
        // GET: Certificate
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Create()
        {
            Certificate_Entity l_model = new Certificate_Entity
            {
                DateFormat = Helpers.GetParameterByKey("CERTIFICADO_FORMATO_FECHA")
            };
            //get date format
            if (SessionHelpers.GetProfile().AreaId == Convert.ToDecimal(Helpers.GetParameterByKey("CODI_AREA_ADMINISTRACION_SEG_EMPRESARIAL")))
            {
                l_model.DateFormat = Helpers.GetParameterByKey("CERTIFICADO_FORMATO_FECHA_ADMINISTRACION");
            }
            else if (SessionHelpers.GetProfile().AreaId == Convert.ToDecimal(Helpers.GetParameterByKey("CODI_AREA_COMERCIAL")))
            {
                l_model.DateFormat = Helpers.GetParameterByKey("CERTIFICADO_FORMATO_FECHA_COMERCIAL");
            }
            ViewBag.NGon.Model = l_model;
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Edit(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            AzureStorageHelpers l_storage = new AzureStorageHelpers("Azure_ContainerName_ELearning");
            Certificate_Entity l_model = null;
            var l_certificate = l_list.RetrieveCertificate(l_id);
            bool l_noData = l_certificate.Rows.Count == 0;
            if (!l_noData)
            {
                l_model = new Certificate_Entity
                {
                    Id = l_id,
                    Description = l_certificate.Rows[0]["Descripcion"].ToString(),
                    CertificateJSON = l_certificate.Rows[0]["Configuracion"].ToString(),
                    Status = l_certificate.Rows[0]["Estado"].ToString(),
                    DateFormat = Helpers.GetParameterByKey("CERTIFICADO_FORMATO_FECHA")
                };
                //get date format
                if (SessionHelpers.GetProfile().AreaId == Convert.ToDecimal(Helpers.GetParameterByKey("CODI_AREA_ADMINISTRACION_SEG_EMPRESARIAL")))
                {
                    l_model.DateFormat = Helpers.GetParameterByKey("CERTIFICADO_FORMATO_FECHA_ADMINISTRACION");
                }
                else if (SessionHelpers.GetProfile().AreaId == Convert.ToDecimal(Helpers.GetParameterByKey("CODI_AREA_COMERCIAL")))
                {
                    l_model.DateFormat = Helpers.GetParameterByKey("CERTIFICADO_FORMATO_FECHA_COMERCIAL");
                }
                //get base64
                var l_json = JsonConvert.DeserializeObject<CertificateTemplate>(l_model.CertificateJSON);
                l_json.oImage.sBase64 = "data:image/jpeg;base64," + l_storage.GetBase64(Helpers.DIRECTORY_CERTIFICATE
                    .Replace("{filename}", l_json.sId));
                foreach (var l_child in l_json.oChildren)
                {
                    if (l_child.sType == "image")
                    {
                        l_child.oImage.sBase64 = "data:image/jpeg;base64," + l_storage.GetBase64(Helpers.DIRECTORY_CERTIFICATE
                            .Replace("{filename}", l_child.sId));
                    }
                }
                var l_serializer = new JavaScriptSerializer
                {
                    MaxJsonLength = Int32.MaxValue
                };
                l_model.CertificateJSON = l_serializer.Serialize(l_json);
            }

            Helpers.ClearDataTable(l_certificate);
            l_list = null;
            l_storage.Clear();
            l_storage = null;

            ViewBag.NGon.Model = l_model;

            if (l_noData)
            {
                return View("NotFound");
            }
            else
            {
                return View(l_model);
            }
        }
        [HttpPost]
        public ActionResult Create(Certificate_Entity p_Entity)
        {
            Bus_Crud l_crud;
            AzureStorageHelpers l_storage = null;
            DataTable l_dt = null;
            try
            {
                l_crud = new Bus_Crud();
                l_storage = new AzureStorageHelpers("Azure_ContainerName_ELearning");
                //save files
                for (int i = 0; i < p_Entity.Files.Length; i++)
                {
                    if (p_Entity.Files[i].File != null)
                    {
                        p_Entity.Files[i].Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.Files[i].File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.Files[i].File.ContentLength) + "}";
                        //upload to storage
                        l_storage.UploadBlob(Helpers.DIRECTORY_CERTIFICATE
                            .Replace("{filename}", p_Entity.Files[i].Name), p_Entity.Files[i].File.InputStream, p_Entity.Files[i].File.ContentType);
                        p_Entity.Files[i].Uploaded = true;
                    }
                }
                //save in db
                p_Entity.Id = 0;
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                l_dt = l_crud.InsertUpdateCertificate(p_Entity);
                if (l_dt.Rows[0]["Value"].ToString() != "1000")
                {
                    try
                    {
                        //remove files
                        for (int i = 0; i < p_Entity.Files.Length; i++)
                        {
                            if (p_Entity.Files[i].File != null && p_Entity.Files[i].Uploaded)
                            {
                                l_storage.DeleteBlob(Helpers.DIRECTORY_CERTIFICATE
                                    .Replace("{filename}", p_Entity.Files[i].Name));
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetMessageCrud(l_dt);
            }
            catch (Exception e)
            {
                if (l_storage != null)
                {
                    try
                    {
                        //remove files
                        for (int i = 0; i < p_Entity.Files.Length; i++)
                        {
                            if (p_Entity.Files[i].File != null && p_Entity.Files[i].Uploaded)
                            {
                                l_storage.DeleteBlob(Helpers.DIRECTORY_CERTIFICATE
                                    .Replace("{filename}", p_Entity.Files[i].Name));
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
                Helpers.ClearDataTable(l_dt);
            }
        }
        [HttpPost]
        public ActionResult Edit(Certificate_Entity p_Entity)
        {
            Bus_List l_list;
            Bus_Crud l_crud;
            AzureStorageHelpers l_storage = null;
            DataTable l_dt = null;
            try
            {
                l_list = new Bus_List();
                //validate name
                var l_validate = l_list.GetCertificateDescriptionValidate(p_Entity);
                if (l_validate.Rows[0]["Value"].ToString() != "1000")
                {
                    return Helpers.GetMessageCrud(l_validate);
                }
                //save
                l_crud = new Bus_Crud();
                l_storage = new AzureStorageHelpers("Azure_ContainerName_ELearning");
                //save files
                for (int i = 0; i < p_Entity.Files.Length; i++)
                {
                    if (p_Entity.Files[i].File != null)
                    {
                        p_Entity.Files[i].Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.Files[i].File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.Files[i].File.ContentLength) + "}";
                        //upload to storage
                        l_storage.UploadBlob(Helpers.DIRECTORY_CERTIFICATE
                            .Replace("{filename}", p_Entity.Files[i].Name), p_Entity.Files[i].File.InputStream, p_Entity.Files[i].File.ContentType);
                        p_Entity.Files[i].Uploaded = true;
                    }
                }
                //save in db
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                l_dt = l_crud.InsertUpdateCertificate(p_Entity);
                if (l_dt.Rows[0]["Value"].ToString() != "1000")
                {
                    try
                    {
                        //remove files
                        for (int i = 0; i < p_Entity.Files.Length; i++)
                        {
                            if (p_Entity.Files[i].File != null && p_Entity.Files[i].Uploaded)
                            {
                                l_storage.DeleteBlob(Helpers.DIRECTORY_CERTIFICATE
                                    .Replace("{filename}", p_Entity.Files[i].Name));
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                else
                {
                    try
                    {
                        //delete files
                        if (p_Entity.DeletedFiles != null)
                        {
                            for (int i = 0; i < p_Entity.DeletedFiles.Length; i++)
                            {
                                l_storage.DeleteBlob(Helpers.DIRECTORY_CERTIFICATE
                                    .Replace("{filename}", p_Entity.DeletedFiles[i]));
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetMessageCrud(l_dt);
            }
            catch (Exception e)
            {
                if (l_storage != null)
                {
                    try
                    {
                        //remove files
                        for (int i = 0; i < p_Entity.Files.Length; i++)
                        {
                            if (p_Entity.Files[i].File != null && p_Entity.Files[i].Uploaded)
                            {
                                l_storage.DeleteBlob(Helpers.DIRECTORY_CERTIFICATE
                                    .Replace("{filename}", p_Entity.Files[i].Name));
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
                Helpers.ClearDataTable(l_dt);
            }
        }
        [HttpPost]
        public ActionResult Delete(Certificate_Entity p_Entity)
        {
            Bus_Crud l_crud;
            Bus_List l_list;
            AzureStorageHelpers l_storage = null;
            try
            {
                l_crud = new Bus_Crud();
                l_list = new Bus_List();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                var l_certificate = l_list.RetrieveCertificate(Convert.ToInt32(p_Entity.Id));
                var l_response = l_crud.DeleteCertificate(p_Entity);
                if (l_response.Rows[0]["Value"].ToString() == "1000")
                {
                    l_storage = new AzureStorageHelpers("Azure_ContainerName_ELearning");
                    try
                    {
                        var l_json = JsonConvert.DeserializeObject<CertificateTemplate>(l_certificate.Rows[0]["Configuracion"].ToString());
                        l_storage.DeleteBlob(Helpers.DIRECTORY_CERTIFICATE.Replace("{filename}", l_json.sId));
                        foreach (var l_child in l_json.oChildren)
                        {
                            if (l_child.sType == "image")
                            {
                                l_storage.DeleteBlob(Helpers.DIRECTORY_CERTIFICATE.Replace("{filename}", l_child.sId));
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }

                Helpers.ClearDataTable(l_certificate);

                return Helpers.GetMessageCrud(l_response);
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
            }
        }
        [HttpPost]
        public JsonResult GetList()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetCertificateList();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Cons"],//0
                        row["Descripcion"],//1
                        row["Estado"]//2
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
    }
}