﻿using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ElearningApp.Filters;
using ElearningApp.Models;
using Microsoft.WindowsAzure.Storage.Blob;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class SliderController : Controller
    {
        // GET: Slider
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            ViewBag.NGon.token_access = Helpers.LoginApiEleaLms();
            ViewBag.NGon.username = SessionHelpers.GetUsername();
            ViewBag.NGon.ipaddress = SessionHelpers.GetIPAddress();
            ViewBag.NGon.UrlApi_slider_listAll = System.Configuration.ConfigurationManager.AppSettings["UrlApi_slider_listAll"];
            ViewBag.NGon.UrlApi_slider_delete = System.Configuration.ConfigurationManager.AppSettings["UrlApi_slider_delete"];
            ViewBag.NGon.UrlApi_slider_updStatus = System.Configuration.ConfigurationManager.AppSettings["UrlApi_slider_updStatus"];
            return View();
        }

        // GET: Slider
        [AppAuthorize]
        [AppLog]
        public ActionResult Add()
        {
            ViewBag.NGon.token_access = Helpers.LoginApiEleaLms();
            ViewBag.NGon.username = SessionHelpers.GetUsername();
            ViewBag.NGon.ipaddress = SessionHelpers.GetIPAddress();
            ViewBag.NGon.UrlApi_slider_saveUpd = System.Configuration.ConfigurationManager.AppSettings["UrlApi_slider_saveUpd"];
            return View();
        }

        // GET: Slider
        [AppAuthorize]
        [AppLog]
        public ActionResult Edit(string id)
        {
            ViewBag.NGon.token_access = Helpers.LoginApiEleaLms();
            ViewBag.NGon.username = SessionHelpers.GetUsername();
            ViewBag.NGon.ipaddress = SessionHelpers.GetIPAddress();
            ViewBag.NGon.UrlApi_slider_id = System.Configuration.ConfigurationManager.AppSettings["UrlApi_slider_id"];
            ViewBag.NGon.UrlApi_slider_saveUpd = System.Configuration.ConfigurationManager.AppSettings["UrlApi_slider_saveUpd"];
            ViewBag.NGon.Codi_Sldr = id;
            return View();
        }
    }
}