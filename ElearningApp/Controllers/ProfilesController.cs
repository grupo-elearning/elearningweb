﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class ProfilesController : Controller
    {
        // GET: Profiles
        [AppAuthorize]
        [AppLog]
        public ActionResult Edit()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Edit(Profiles_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.UpdateProfiles(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public ActionResult EditSend(Profiles_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                var l_response = l_crud.UpdateProfilesProvider(p_Entity);

                if (l_response.Rows[0]["Value"].ToString() == "1000")
                {
                    //send email
                    EmailHelpers.SendProviderDataFromProfileMaintenance(p_Entity);
                }

                return Helpers.GetMessageCrud(l_response);
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public JsonResult GetList(int id)
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_profiles = l_list.RetrieveProfiles(id);
                foreach (DataRow row in l_profiles.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Sipeus"],//0
                        row["Codi_User"],//1
                        row["Codigo"],//2
                        row["Documento_Ruc"],//3
                        row["Nombre"],//4
                        row["Provider_Password"],//5
                        row["Provider_Email"]//6
                    });
                }

                Helpers.ClearDataTable(l_profiles);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public ActionResult Item(Profiles_Model p_Model)
        {
            ViewBag.NGon.Model = p_Model;
            return PartialView("PartialView/_ItemPartialView", p_Model);
        }
        [HttpPost]
        public JsonResult GetItem(Profiles_Model p_Model)
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetItem2Profiles(p_Model);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_User"],//0
                        row["Codigo"],//1
                        row["Documento_Ruc"],//2
                        row["Nombre"],//3
                        row["Provider_Password"],//4
                        row["Provider_Email"]//5
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
    }
}