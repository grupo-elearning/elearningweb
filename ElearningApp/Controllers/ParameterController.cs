﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class ParameterController : Controller
    {
        // GET: Parameter
        [AppAuthorize]
        [AppLog]
        public ActionResult Edit()
        {
            Bus_List l_list = new Bus_List();
            var l_parameter = l_list.RetrieveParameter();
            Parameter_Entity l_model = new Parameter_Entity
            {
                CarrierCourseId = l_parameter.Rows[0]["Codi_Curs_Transportista"].ToString(),
                WaitTimeCloseSession = l_parameter.Rows[0]["Tiempo_Cerrar_Session"].ToString(),
                MessageFinalized = l_parameter.Rows[0]["Mensaje_Fin_Examen"].ToString(),
                MessageApproved = l_parameter.Rows[0]["Mensaje_Aprobado_Fin_Examen"].ToString(),
                MessageDisapproved = l_parameter.Rows[0]["Mensaje_Desaprobado_Fin_Examen"].ToString()
            };

            Helpers.ClearDataTable(l_parameter);
            l_list = null;

            ViewBag.NGon.Model = l_model;
            return View(l_model);
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Parameter_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.UpdateParameter(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
    }
}