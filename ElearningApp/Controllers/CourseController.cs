﻿using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ElearningApp.Filters;
using ElearningApp.Models;
using Microsoft.WindowsAzure.Storage.Blob;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class CourseController : Controller
    {
        private string l_storage_url_elearning = Helpers.GetAppSettings("Azure_StorageUrl_ELearning");
        private string l_current_course_name = "";
        // GET: Course
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Create()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Edit(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            Course_Entity l_model = null;
            var l_course = l_list.RetrieveCourse(l_id);
            bool l_noData = l_course.Rows.Count == 0;
            if (!l_noData)
            {
                l_current_course_name = Convert.ToString(l_course.Rows[0]["Nombre"]);
                l_model = new Course_Entity
                {
                    Id = Convert.ToDecimal(l_course.Rows[0]["Codi_Curs"]),
                    Name = l_current_course_name,
                    Status = Convert.ToString(l_course.Rows[0]["Estado"]),
                    Area = Convert.ToString(l_course.Rows[0]["Area"]),
                    CategoryId = Convert.ToDecimal(l_course.Rows[0]["Codi_Cate"]),
                    Classification = Convert.ToString(l_course.Rows[0]["Clasificacion"]),
                    ImageName = Convert.ToString(l_course.Rows[0]["Url_Imagen"]),
                    ImageUrl = l_storage_url_elearning + Helpers.DIRECTORY_COURSE_CONTENT
                                                            .Replace("{name}", l_current_course_name)
                                                            .Replace("{filename}", Convert.ToString(l_course.Rows[0]["Url_Imagen"])),
                    FrontPage = Convert.ToString(l_course.Rows[0]["Descripcion_Portada"]),
                    Objetives = Convert.ToString(l_course.Rows[0]["Objetivo"]),
                    Summary = Convert.ToString(l_course.Rows[0]["Resumen"]),
                    HasValidity = Convert.ToString(l_course.Rows[0]["Flag_Vigencia"]),
                    Validity = Convert.ToDecimal(l_course.Rows[0]["Cant_Vigencia"]),
                    ValidityScale = Convert.ToString(l_course.Rows[0]["Escala_Vigencia"]),
                    Themes = RetrieveTheme(l_id, l_list),
                    StorageUrl = l_storage_url_elearning + Helpers.DIRECTORY_COURSE_CONTENT
                                                            .Replace("{name}", l_current_course_name)
                                                            .Replace("{filename}", "")
                };
            }

            Helpers.ClearDataTable(l_course);

            l_list = null;

            ViewBag.NGon.Model = l_model;

            if (l_noData)
            {
                return View("NotFound");
            }
            else
            {
                return View(l_model);
            }
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Presentation(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            Course_Entity l_model = null;
            var l_course = l_list.RetrieveCourse(l_id);
            bool l_noData = l_course.Rows.Count == 0;
            if (!l_noData)
            {
                l_current_course_name = Convert.ToString(l_course.Rows[0]["Nombre"]);
                l_model = new Course_Entity
                {
                    Id = Convert.ToDecimal(l_course.Rows[0]["Codi_Curs"]),
                    Category = l_course.Rows[0]["Categoria"].ToString(),
                    Area = l_course.Rows[0]["Area"].ToString(),
                    Name = l_current_course_name,
                    Objetives = l_course.Rows[0]["Objetivo"].ToString(),
                    Summary = l_course.Rows[0]["Resumen"].ToString(),
                    FrontPage = l_course.Rows[0]["Descripcion_Portada"].ToString(),
                    ImageUrl = l_storage_url_elearning + Helpers.DIRECTORY_COURSE_CONTENT
                                                            .Replace("{name}", l_current_course_name)
                                                            .Replace("{filename}", Convert.ToString(l_course.Rows[0]["Url_Imagen"])),
                    Classification = l_course.Rows[0]["Clasificacion"].ToString() == "T" ? "Transversal" : "Específico",
                    Themes = RetrieveTheme(l_id, l_list)
                };
            }

            Helpers.ClearDataTable(l_course);

            l_list = null;

            ViewBag.NGon.Model = l_model;

            if (l_noData)
            {
                return View("NotFound");
            }
            else
            {
                return View(l_model);
            }
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Classroom(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            Course_Entity l_model = null;
            var l_course = l_list.RetrieveCourse(l_id);
            bool l_noData = l_course.Rows.Count == 0;
            if (!l_noData)
            {
                l_current_course_name = Convert.ToString(l_course.Rows[0]["Nombre"]);
                l_model = new Course_Entity
                {
                    Id = Convert.ToDecimal(l_course.Rows[0]["Codi_Curs"]),
                    Category = l_course.Rows[0]["Categoria"].ToString(),
                    Area = l_course.Rows[0]["Area"].ToString(),
                    Name = l_current_course_name,
                    Objetives = l_course.Rows[0]["Objetivo"].ToString(),
                    Summary = l_course.Rows[0]["Resumen"].ToString(),
                    FrontPage = l_course.Rows[0]["Descripcion_Portada"].ToString(),
                    ImageUrl = l_storage_url_elearning + Helpers.DIRECTORY_COURSE_CONTENT
                                                            .Replace("{name}", l_current_course_name)
                                                            .Replace("{filename}", Convert.ToString(l_course.Rows[0]["Url_Imagen"])),
                    Classification = l_course.Rows[0]["Clasificacion"].ToString() == "T" ? "Transversal" : "Específico",
                    Themes = RetrieveTheme(l_id, l_list)
                };
            }

            Helpers.ClearDataTable(l_course);
            l_list = null;

            ViewBag.NGon.Model = l_model;

            if (l_noData)
            {
                return View("NotFound");
            }
            else
            {
                return View(l_model);
            }
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(Course_Entity p_Entity)
        {
            Bus_List l_list;
            Bus_Crud l_crud;
            AzureStorageHelpers l_storage = null;
            try
            {
                l_list = new Bus_List();
                //validate name
                var l_validate = l_list.GetCourseNameValidate(p_Entity);
                if (l_validate.Rows[0]["Value"].ToString() != "1000")
                {
                    return Helpers.GetMessageCrud(l_validate);
                }
                //save
                l_crud = new Bus_Crud();
                l_storage = new AzureStorageHelpers("Azure_ContainerName_ELearning");
                //save img
                if (p_Entity.ImageFile != null)
                {
                    p_Entity.ImageFile.Name = DateTime.Now.ToString("ddMMyyyyHHmmssffffff") + "-" + p_Entity.ImageFile.File.FileName;
                    p_Entity.ImageFile.Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.ImageFile.File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.ImageFile.File.ContentLength) + "}";
                    //upload to storage
                    l_storage.UploadBlob(Helpers.DIRECTORY_COURSE_CONTENT
                        .Replace("{name}", p_Entity.Name)
                        .Replace("{filename}", p_Entity.ImageFile.Name), p_Entity.ImageFile.File.InputStream, p_Entity.ImageFile.File.ContentType);
                    p_Entity.ImageFile.Uploaded = true;
                }
                //save files
                if (p_Entity.ThemeItemFiles != null)
                {
                    for (int i = 0; i < p_Entity.ThemeItemFiles.Length; i++)
                    {
                        if (p_Entity.ThemeItemFiles[i] != null)
                        {
                            p_Entity.ThemeItemFiles[i].Name = DateTime.Now.ToString("ddMMyyyyHHmmssffffff") + "-" + p_Entity.ThemeItemFiles[i].File.FileName;
                            p_Entity.ThemeItemFiles[i].Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.ThemeItemFiles[i].File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.ThemeItemFiles[i].File.ContentLength) + "}";
                            //upload to storage
                            l_storage.UploadBlob(Helpers.DIRECTORY_COURSE_CONTENT
                                .Replace("{name}", p_Entity.Name)
                                .Replace("{filename}", p_Entity.ThemeItemFiles[i].Name), p_Entity.ThemeItemFiles[i].File.InputStream, p_Entity.ThemeItemFiles[i].File.ContentType);
                            p_Entity.ThemeItemFiles[i].Uploaded = true;
                        }
                    }
                }
                //save in db
                p_Entity.AreaId = SessionHelpers.GetProfile().AreaId;
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                var l_response = l_crud.InsertUpdateCourse(p_Entity);
                if (l_response.Rows[0]["Value"].ToString() != "1000")
                {
                    try
                    {
                        //remove img
                        if (p_Entity.ImageFile != null && p_Entity.ImageFile.Uploaded)
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_COURSE_CONTENT
                                .Replace("{name}", p_Entity.Name)
                                .Replace("{filename}", p_Entity.ImageFile.Name));
                        }
                        //remove files
                        if (p_Entity.ThemeItemFiles != null)
                        {
                            for (int i = 0; i < p_Entity.ThemeItemFiles.Length; i++)
                            {
                                if (p_Entity.ThemeItemFiles[i] != null && p_Entity.ThemeItemFiles[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_COURSE_CONTENT
                                        .Replace("{name}", p_Entity.Name)
                                        .Replace("{filename}", p_Entity.ThemeItemFiles[i].Name));
                                }
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                if (l_response.Columns.Contains("Error"))
                {
                    var l_error = Convert.ToString(l_response.Rows[0]["Error"]);
                    if (l_error != "")
                    {
                        EmailHelpers.SendError(l_error);
                    }
                }
                return Helpers.GetMessageCrud(l_response);
            }
            catch (Exception e)
            {
                if (l_storage != null)
                {
                    try
                    {
                        //remove img
                        if (p_Entity.ImageFile != null && p_Entity.ImageFile.Uploaded)
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_COURSE_CONTENT
                                .Replace("{name}", p_Entity.Name)
                                .Replace("{filename}", p_Entity.ImageFile.Name));
                        }
                        //remove files
                        if (p_Entity.ThemeItemFiles != null)
                        {
                            for (int i = 0; i < p_Entity.ThemeItemFiles.Length; i++)
                            {
                                if (p_Entity.ThemeItemFiles[i] != null && p_Entity.ThemeItemFiles[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_COURSE_CONTENT
                                        .Replace("{name}", p_Entity.Name)
                                        .Replace("{filename}", p_Entity.ThemeItemFiles[i].Name));
                                }
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
            }
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Course_Entity p_Entity)
        {
            Bus_List l_list;
            Bus_Crud l_crud;
            AzureStorageHelpers l_storage = null;
            DataTable l_dt = null;
            try
            {
                l_list = new Bus_List();
                //validate name
                var l_validate = l_list.GetCourseNameValidate(p_Entity);
                if (l_validate.Rows[0]["Value"].ToString() != "1000")
                {
                    return Helpers.GetMessageCrud(l_validate);
                }
                //save
                l_crud = new Bus_Crud();
                l_storage = new AzureStorageHelpers("Azure_ContainerName_ELearning");
                //save img
                if (p_Entity.ImageFile != null && p_Entity.ImageFile.File != null)
                {
                    p_Entity.ImageFile.Name = DateTime.Now.ToString("ddMMyyyyHHmmssffffff") + "-" + p_Entity.ImageFile.File.FileName;
                    p_Entity.ImageFile.Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.ImageFile.File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.ImageFile.File.ContentLength) + "}";
                    //upload to storage
                    l_storage.UploadBlob(Helpers.DIRECTORY_COURSE_CONTENT
                        .Replace("{name}", p_Entity.Name)
                        .Replace("{filename}", p_Entity.ImageFile.Name), p_Entity.ImageFile.File.InputStream, p_Entity.ImageFile.File.ContentType);
                    p_Entity.ImageFile.Uploaded = true;
                }
                if (p_Entity.ImageFile.WasRetrieved)
                {
                    p_Entity.ImageFile.Name = p_Entity.ImageRetrieved;
                }
                //save files
                if (p_Entity.ThemeItemFiles != null)
                {
                    for (int i = 0; i < p_Entity.ThemeItemFiles.Length; i++)
                    {
                        if (p_Entity.ThemeItemFiles[i] != null && !p_Entity.ThemeItemFiles[i].WasRetrieved)
                        {
                            p_Entity.ThemeItemFiles[i].Name = DateTime.Now.ToString("ddMMyyyyHHmmssffffff") + "-" + p_Entity.ThemeItemFiles[i].File.FileName;
                            p_Entity.ThemeItemFiles[i].Properties = "{\"Type\":\"" + Convert.ToString(p_Entity.ThemeItemFiles[i].File.ContentType) + "\",\"Length\":" + Convert.ToString(p_Entity.ThemeItemFiles[i].File.ContentLength) + "}";
                            //upload to storage
                            l_storage.UploadBlob(Helpers.DIRECTORY_COURSE_CONTENT
                                .Replace("{name}", p_Entity.Name)
                                .Replace("{filename}", p_Entity.ThemeItemFiles[i].Name), p_Entity.ThemeItemFiles[i].File.InputStream, p_Entity.ThemeItemFiles[i].File.ContentType);
                            p_Entity.ThemeItemFiles[i].Uploaded = true;
                        }
                    }
                }
                //save in db
                p_Entity.AreaId = SessionHelpers.GetProfile().AreaId;
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                l_dt = l_crud.InsertUpdateCourse(p_Entity);
                if (l_dt.Rows[0]["Value"].ToString() == "1000")
                {
                    try
                    {
                        //remove old img
                        if (!p_Entity.ImageFile.WasRetrieved)
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_COURSE_CONTENT
                                .Replace("{name}", p_Entity.OldName)
                                .Replace("{filename}", p_Entity.ImageRetrieved));
                        }
                        //remove removed files
                        if (p_Entity.ThemeItemRemoved != null)
                        {
                            for (var i = 0; i < p_Entity.ThemeItemRemoved.Length; i++)
                            {
                                l_storage.DeleteBlob(Helpers.DIRECTORY_COURSE_CONTENT
                                    .Replace("{name}", p_Entity.OldName)
                                    .Replace("{filename}", p_Entity.ThemeItemRemoved[i].Name));
                            }
                        }
                        //rename files
                        if (p_Entity.OldName != p_Entity.Name)
                        {
                            l_storage.RenameDirectory(Helpers.DIRECTORY_COURSE_CONTENT
                                .Replace("{name}", p_Entity.OldName)
                                .Replace("/{filename}", ""), Helpers.DIRECTORY_COURSE_CONTENT
                                .Replace("{name}", p_Entity.Name)
                                .Replace("/{filename}", ""));
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                else
                {
                    try
                    {
                        //remove img
                        if (p_Entity.ImageFile != null && p_Entity.ImageFile.Uploaded)
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_COURSE_CONTENT
                                .Replace("{name}", p_Entity.Name)
                                .Replace("{filename}", p_Entity.ImageFile.Name));
                        }
                        //remove files
                        if (p_Entity.ThemeItemFiles != null)
                        {
                            for (int i = 0; i < p_Entity.ThemeItemFiles.Length; i++)
                            {
                                if (p_Entity.ThemeItemFiles[i] != null && !p_Entity.ThemeItemFiles[i].WasRetrieved && p_Entity.ThemeItemFiles[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_COURSE_CONTENT
                                        .Replace("{name}", p_Entity.Name)
                                        .Replace("{filename}", p_Entity.ThemeItemFiles[i].Name));
                                }
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                if (l_dt.Columns.Contains("Error"))
                {
                    var l_error = Convert.ToString(l_dt.Rows[0]["Error"]);
                    if (l_error != "")
                    {
                        EmailHelpers.SendError(l_error);
                    }
                }
                return Helpers.GetMessageCrud(l_dt);
            }
            catch (Exception e)
            {
                if (l_storage != null)
                {
                    try
                    {
                        //remove img
                        if (p_Entity.ImageFile != null && p_Entity.ImageFile.Uploaded)
                        {
                            l_storage.DeleteBlob(Helpers.DIRECTORY_COURSE_CONTENT
                                .Replace("{name}", p_Entity.Name)
                                .Replace("{filename}", p_Entity.ImageFile.Name));
                        }
                        //remove files
                        if (p_Entity.ThemeItemFiles != null)
                        {
                            for (int i = 0; i < p_Entity.ThemeItemFiles.Length; i++)
                            {
                                if (p_Entity.ThemeItemFiles[i] != null && !p_Entity.ThemeItemFiles[i].WasRetrieved && p_Entity.ThemeItemFiles[i].Uploaded)
                                {
                                    l_storage.DeleteBlob(Helpers.DIRECTORY_COURSE_CONTENT
                                        .Replace("{name}", p_Entity.Name)
                                        .Replace("{filename}", p_Entity.ThemeItemFiles[i].Name));
                                }
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
                Helpers.ClearDataTable(l_dt);
            }
        }
        [HttpPost]
        public ActionResult Delete(Course_Entity p_Entity)
        {
            Bus_Crud l_crud;
            Bus_List l_list;
            AzureStorageHelpers l_storage = null;
            try
            {
                l_crud = new Bus_Crud();
                l_list = new Bus_List();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                var l_course = l_list.RetrieveCourse(Convert.ToInt32(p_Entity.Id));
                var l_response = l_crud.DeleteCourse(p_Entity);
                if (l_response.Rows[0]["Value"].ToString() == "1000")
                {
                    l_storage = new AzureStorageHelpers("Azure_ContainerName_ELearning");
                    try
                    {
                        l_storage.DeleteDirectory(Helpers.DIRECTORY_COURSE_CONTENT
                            .Replace("{name}", l_course.Rows[0]["Nombre"].ToString())
                            .Replace("/{filename}", ""));
                    }
                    catch
                    {
                        //prevent
                    }
                }

                Helpers.ClearDataTable(l_course);

                if (l_response.Columns.Contains("Error"))
                {
                    var l_error = Convert.ToString(l_response.Rows[0]["Error"]);
                    if (l_error != "")
                    {
                        EmailHelpers.SendError(l_error);
                    }
                }

                return Helpers.GetMessageCrud(l_response);
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
            }
        }
        [HttpPost]
        public JsonResult GetList(Course_Model p_Model)
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetCourseList(p_Model, Convert.ToInt32(SessionHelpers.GetProfile().AreaId));
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Curs"],//0
                        row["Categoria"],//1
                        row["Nombre"],//2
                        row["Area"],//3
                        row["Resumen"],//4
                        row["Estado"]//5
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        #region private
        private class VideoUrl
        {
            public string url_youtube { get; set; }
            public string url_sider_channel { get; set; }
        }
        private List<object> RetrieveTheme(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.RetrieveCourseTheme(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Tecu"],//0
                        row["Descripcion"],//1
                        RetrieveThemeItem(Convert.ToInt32(row["Codi_Tecu"]), p_bus),//2
                        row["Secuencia"]//3
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        private List<object> RetrieveThemeItem(int p_id, Bus_List p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.RetrieveCourseThemeItem(p_id);
                var l_tcon_video = Convert.ToInt32(Helpers.GetParameterByKey("CODI_TCON_VIDEO"));
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Tipo"],//0
                        row["Titulo_Contenido"],//1
                        //Helpers.Coalesce(new object[]{ row["Texto"], row["Nombre_Doc"], row["Nombre_Curso"] }),//2
                        l_tcon_video == Convert.ToInt32(row["Codi_Tcon"]) && Convert.ToString(row["Nombre_Doc"]) == "" ?
                            new VideoUrl{ url_youtube = Convert.ToString(row["Url_Youtube"]), url_sider_channel = Convert.ToString(row["Url_Canal_Sider"])} :
                            Helpers.Coalesce(new object[]{ row["Texto"], row["Nombre_Doc"], row["Nombre_Curso"] }) as object,//2
                        row["Secuencia"],//3
                        Convert.ToString(row["Nombre_Doc"]) == "" ? null : Convert.ToString(row["Nombre_Doc"]),//4
                        row["Codi_Cuin"],//5
                        row["Codi_Tcon"],//6
                        row["Codi_Cote"],//7
                        true,//8
                        row["Url_Curso"],//9
                        //classroom
                        row["Texto"],//10
                        l_storage_url_elearning + Helpers.DIRECTORY_COURSE_CONTENT
                                                    .Replace("{name}", l_current_course_name)
                                                    .Replace("{filename}", Convert.ToString(row["Nombre_Doc"])),//11
                        row["Url_Youtube"],//12
                        row["Url_Canal_Sider"]//13
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        #endregion private
    }
}