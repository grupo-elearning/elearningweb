﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Dao;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    public class HelpersController : Controller
    {
        #region DropDown
        [HttpPost]
        public JsonResult IncotermsDropDown()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.IncotermsDropDown();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Term"],//0
                        row["Descripcion"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult ProgrammingAreaDropDownAll(int Year = 0)
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.ProgrammingAreaDropDownAll(Convert.ToInt32(SessionHelpers.GetProfile().AreaId), Year);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pcur"],//0
                        row["Nombre"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult CertificateDropDown()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.CertificateDropDown();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Cons"],//0
                        row["Descripcion"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult MoneyTypeDropDown()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.MoneyTypeDropDown();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_tmon"],//0
                        row["descripcion"],//1
                        row["simbolo"]//2
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult BankDropDown()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.BankDropDown();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_banc"],//0
                        row["descripcion"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult AccountTypeDropDown()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.AccountTypeDropDown();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_tcue"],//0
                        row["descripcion"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult VehicleTypeDropDown()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.VehicleTypeDropDown();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_unve"],//0
                        row["descripcion"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult PollDropDown()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.PollDropDown();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Encu"],//0
                        row["Descripcion"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult QuestionType2TestDropDown()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.QuestionType2TestDropDown();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Tpre"],//0
                        row["Descripcion"],//1
                        row["Tipo"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult CourseAreaDropDownAll()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.CourseAreaDropDownAll(Convert.ToInt32(SessionHelpers.GetProfile().AreaId));
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Curs"],//0
                        row["Curso"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult ProviderAreaDropDownAll()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.ProviderAreaDropDownAll(Convert.ToInt32(SessionHelpers.GetProfile().AreaId));
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Prov"],//0
                        row["Razon_Social"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult InteractiveContentDropDown()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.InteractiveContentDropDown();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Cuin"],//0
                        row["Nombre_Curso"],//1
						row["Url_Curso"]//2
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult ELearningProfileDropDown()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.ELearningProfileDropDown();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Sipe"],//0
                        row["Descripcion"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult AreaDropDown()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.AreaDropDown();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Area"],//0
                        row["Descripcion"],//1
						row["Descripcion_Corta"]//2
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult QuestionTypeDropDown()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.QuestionTypeDropDown();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_tpre"],//0
                        row["descripcion"],//1
						row["tipo"]//2
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult CategoryDropDown()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.CategoryDropDown();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_cate"],//0
                        row["descripcion"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult TestCourseDropDown(int id)
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.TestCourseDropDown(id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["codi_exam"],//0
                        row["descripcion"],//1
                        row["aleatorio"]//2
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult ModeDropDown()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.ModeDropDown();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Moda"],//0
                        row["Descripcion"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult PlaceDropDown()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.PlaceDropDown();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["CODI_LUGA"],//0
                        row["descripcion"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult CourseAreaDropDown()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.CourseAreaDropDown(Convert.ToInt32(SessionHelpers.GetProfile().AreaId));
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Curs"],//0
                        row["Curso"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult CourseDropDown()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.CourseDropDown();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Curs"],//0
                        row["Nombre"]//1
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult DocumentTypeDropDown()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.DocumentTypeDropDown();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Tdoc"],//0
                        row["Descripcion_corta"],//1
						row["Descripcion_corta"]//2
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        #endregion DropDown
        #region Parameter
        [HttpPost]
        public JsonResult GetParameterByKey(string key)
        {
            Dao_List Dao;
            try
            {
                Dao = new Dao_List();
                var l_value = Dao.GetParameterByKey(key);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_value
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                Dao = null;
            }
        }
        #endregion Parameter
        #region Helpers
        [HttpPost]
        public JsonResult InSiderperu()
        {
            try
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = Helpers.GetParameterByKey("IP_ADDRESS_SIDERPERU").Contains(string.Concat("/", RequestHelpers.GetClientIpAddress(HttpContext.Request), "/"))
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
        }
        [HttpPost]
        public JsonResult GetUTCTime()
        {
            try
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = Helpers.GetUTCDate().ToString("HH:mm")
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
        }
        [HttpPost]
        public JsonResult GetUTCDate()
        {
            try
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = Helpers.GetUTCDate().ToString("dd/MM/yyyy")
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
        }
        [HttpGet]
        [AppDeleteFile]
        public ActionResult DownloadExcel(string FileName, string DownloadName = "")
        {
            string l_name = DownloadName == "" ? FileName : DownloadName;
            string l_path = Path.Combine(Server.MapPath("~/temp/"), FileName);
            return File(l_path, "application/vnd.ms-excel", l_name);
        }
        #endregion Helpers
    }
}