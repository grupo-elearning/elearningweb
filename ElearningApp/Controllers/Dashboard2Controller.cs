﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class Dashboard2Controller : Controller
    {
        private string l_storage_url_elearning = Helpers.GetAppSettings("Azure_StorageUrl_ELearning");
        // GET: Dashboard2
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult GetCampusInformation(DateTime from_date, DateTime to_date)
        {
            Bus_Report l_report;
            List<object> l_result;
            try
            {
                l_report = new Bus_Report();
                l_result = new List<object>();
                var l_records = l_report.Dashboard2GetCampusInformation(Convert.ToInt32(SessionHelpers.GetProfile().AreaId), from_date, to_date);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        Convert.ToDateTime(row["Fecha"]).ToString("yyyy-MM-dd"),//0
                        row["Active_Users"],//1
                        row["New_Users"],//2
                        row["Total_Users"],//3
                        row["Hours_On_Campus"]//4
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_report = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetAcademicSituation()
        {
            Bus_Report l_report;
            List<object> l_result;
            try
            {
                l_report = new Bus_Report();
                l_result = new List<object>();
                var l_records = l_report.Dashboard2GetAcademicSituation(Convert.ToInt32(SessionHelpers.GetProfile().AreaId));
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pcur"],//0
                        row["Curso_Nombre"],//1
                        row["Programacion"],//2
                        l_storage_url_elearning + Helpers.DIRECTORY_COURSE_CONTENT
                                                    .Replace("{name}", Convert.ToString(row["Curso_Nombre"]))
                                                    .Replace("{filename}", Convert.ToString(row["Curso_Imagen"])),//3
                        row["Promedio"]//4
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_report = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetStatisticsCourse()
        {
            Bus_Report l_report;
            List<object> l_result;
            try
            {
                l_report = new Bus_Report();
                l_result = new List<object>();
                var l_records = l_report.Dashboard2GetStatisticsCourse(Convert.ToInt32(SessionHelpers.GetProfile().AreaId));
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pcur"],//0
                        l_storage_url_elearning + Helpers.DIRECTORY_COURSE_CONTENT
                                                    .Replace("{name}", Convert.ToString(row["Curso_Nombre"]))
                                                    .Replace("{filename}", Convert.ToString(row["Curso_Imagen"])),//1
                        row["Programacion"],//2
                        row["Tipo_Inscripcion"],//3
                        row["Estado"],//4
                        row["Inscritos"],//5
                        row["Satisfaccion"]//6
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_report = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetStatisticsStudent()
        {
            Bus_Report l_report;
            List<object> l_result;
            try
            {
                l_report = new Bus_Report();
                l_result = new List<object>();
                var l_records = l_report.Dashboard2GetStatisticsStudent(Convert.ToInt32(SessionHelpers.GetProfile().AreaId));
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pers"],//0
                        row["Nombres"],//1
                        row["Apellido"],//2
                        row["Email"],//3
                        row["Tiempo"]//4
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_report = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetSatisfaction()
        {
            Bus_Report l_report;
            List<object> l_result;
            try
            {
                l_report = new Bus_Report();
                l_result = new List<object>();
                var l_records = l_report.Dashboard2GetSatisfaction(Convert.ToInt32(SessionHelpers.GetProfile().AreaId));
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pcur"],//0
                        row["Curso_Nombre"],//1
                        row["Programacion"],//2
                        l_storage_url_elearning + Helpers.DIRECTORY_COURSE_CONTENT
                                                    .Replace("{name}", Convert.ToString(row["Curso_Nombre"]))
                                                    .Replace("{filename}", Convert.ToString(row["Curso_Imagen"])),//3
                        row["Promedio"]//4
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_report = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetCourseCharacteristic(int course, DateTime from_date, DateTime to_date)
        {
            Bus_Report l_report;
            List<object> l_result;
            try
            {
                l_report = new Bus_Report();
                l_result = new List<object>();
                var l_records = l_report.Dashboard2GetCourseCharacteristic(Convert.ToInt32(SessionHelpers.GetProfile().AreaId), course, from_date, to_date);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        Convert.ToDateTime(row["Fecha"]).ToString("yyyy-MM-dd"),//0
                        row["Registers_Status"],//1
                        row["Registers"],//2
                        row["Visits_To_Course"],//3
                        row["Hours_On_Course"],//4
                        row["Completed_Contents"],//5
                        row["Graduates"]//6
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_report = null;
                l_result = null;
            }
        }
    }
}