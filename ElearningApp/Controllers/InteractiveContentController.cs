﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class InteractiveContentController : Controller
    {
        // GET: InteractiveContent
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Create()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Edit(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            InteractiveContent_Entity l_model = null;
            var l_interactive_content = l_list.RetrieveInteractiveContent(l_id);
            bool l_noData = l_interactive_content.Rows.Count == 0;
            if (!l_noData)
            {
                l_model = new InteractiveContent_Entity
                {
                    Id = l_id,
                    Name = l_interactive_content.Rows[0]["Nombre_Curso"].ToString(),
                    Url = l_interactive_content.Rows[0]["Url_Curso"].ToString(),
                    Directory = l_interactive_content.Rows[0]["Nombre_Directorio"].ToString(),
                    Status = l_interactive_content.Rows[0]["Estado"].ToString()
                };
            }

            Helpers.ClearDataTable(l_interactive_content);
            l_list = null;

            ViewBag.NGon.Model = l_model;

            if (l_noData)
            {
                return View("NotFound");
            }
            else
            {
                return View(l_model);
            }
        }
        [HttpPost]
        public ActionResult Create(InteractiveContent_Entity p_Entity)
        {
            Bus_List l_list;
            Bus_Crud l_crud;
            AzureStorageHelpers l_storage = null;
            DataTable l_dt = null;
            try
            {
                l_list = new Bus_List();
                //validate name
                var l_validate = l_list.GetInteractiveContentNameValidate(p_Entity);
                if (l_validate.Rows[0]["Value"].ToString() != "1000")
                {
                    return Helpers.GetMessageCrud(l_validate);
                }
                //save
                l_crud = new Bus_Crud();

                //l_storage = new AzureStorageHelpers("Azure_ContainerName_ELearning");

                //save files
                //p_Entity.Directory = DateTime.Now.ToString("ddMMyyyyHHmmssffffff") + "-" + p_Entity.Directory;
                p_Entity.Directory = DateTime.Now.ToString("yyyyMMddHHmmssffffff") + "-" + p_Entity.Directory;
                for (int i = 0; i < p_Entity.Files.Length; i++)
                {
                    var full_path = Server.MapPath("~/CursosElearning/" + string.Concat(p_Entity.Directory, p_Entity.Files[i].Path));
                    var path = Path.GetDirectoryName(full_path);
                    // Determine whether the directory exists.
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    
                    //upload to storage
                    //l_storage.UploadBlob(Helpers.DIRECTORY_INTERACTIVECONTENT
                    //    .Replace("{filename}", string.Concat(p_Entity.Directory, p_Entity.Files[i].Path)), p_Entity.Files[i].File.InputStream, p_Entity.Files[i].File.ContentType);
                    p_Entity.Files[i].File.SaveAs(full_path);
                    p_Entity.Files[i].Uploaded = true;
                }
                //save in db
                p_Entity.Url = string.Concat(Helpers.GetAppSettings("Curso_Interactivo_Url"), string.Concat(p_Entity.Directory, p_Entity.Url));

                //p_Entity.Url = string.Concat(Helpers.GetAppSettings("Azure_StorageUrl_ELearning"), Helpers.DIRECTORY_INTERACTIVECONTENT
                //        .Replace("{filename}", string.Concat(p_Entity.Directory, p_Entity.Url)));

                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                l_dt = l_crud.InsertUpdateInteractiveContent(p_Entity);
                if (l_dt.Rows[0]["Value"].ToString() != "1000")
                {
                    try
                    {
                        //Eliminar los archivos
                        var path = Server.MapPath(string.Concat("~/CursosElearning/", p_Entity.Directory));
                        if (Directory.Exists(path))
                        {
                            Directory.Delete(path, true);
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetMessageCrud(l_dt);
            }
            catch (Exception e)
            {
                try
                {//Eliminar los archivos
                    var path = Server.MapPath(string.Concat("~/CursosElearning/", p_Entity.Directory));
                    if (Directory.Exists(path))
                    {
                        Directory.Delete(path, true);
                    }
                } catch { }
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_crud = null;
                //if (l_storage != null)
                //{
                //    l_storage.Clear();
                //    l_storage = null;
                //}
                Helpers.ClearDataTable(l_dt);
            }
        }
        [HttpPost]
        public ActionResult Edit(InteractiveContent_Entity p_Entity)
        {
            Bus_List l_list;
            Bus_Crud l_crud;
            AzureStorageHelpers l_storage = null;
            DataTable l_dt = null;
            try
            {
                l_list = new Bus_List();
                //validate name
                var l_validate = l_list.GetInteractiveContentNameValidate(p_Entity);
                if (l_validate.Rows[0]["Value"].ToString() != "1000")
                {
                    return Helpers.GetMessageCrud(l_validate);
                }
                //save
                l_crud = new Bus_Crud();
                l_storage = new AzureStorageHelpers("Azure_ContainerName_ELearning");
                //save files
                if (!p_Entity.WasRetrieved)
                {
                    p_Entity.Directory = DateTime.Now.ToString("ddMMyyyyHHmmssffffff") + "-" + p_Entity.Directory;
                    for (int i = 0; i < p_Entity.Files.Length; i++)
                    {
                        var full_path = Server.MapPath("~/CursosElearning/" + string.Concat(p_Entity.Directory, p_Entity.Files[i].Path));
                        var path = Path.GetDirectoryName(full_path);
                        // Determine whether the directory exists.
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }

                        //upload to storage
                        //l_storage.UploadBlob(Helpers.DIRECTORY_INTERACTIVECONTENT
                        //    .Replace("{filename}", string.Concat(p_Entity.Directory, p_Entity.Files[i].Path)), p_Entity.Files[i].File.InputStream, p_Entity.Files[i].File.ContentType);
                        p_Entity.Files[i].File.SaveAs(full_path);
                        p_Entity.Files[i].Uploaded = true;
                    }
                    //get url
                    p_Entity.Url = string.Concat(Helpers.GetAppSettings("Curso_Interactivo_Url"), string.Concat(p_Entity.Directory, p_Entity.Url));

                    //p_Entity.Url = string.Concat(Helpers.GetAppSettings("Azure_StorageUrl_ELearning"), Helpers.DIRECTORY_INTERACTIVECONTENT
                    //    .Replace("{filename}", string.Concat(p_Entity.Directory, p_Entity.Url)));
                }
                //save in db
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                l_dt = l_crud.InsertUpdateInteractiveContent(p_Entity);
                if (l_dt.Rows[0]["Value"].ToString() == "1000")
                {
                    try
                    {
                        if (!p_Entity.WasRetrieved)
                        {
                            //remove old directory
                            //l_storage.DeleteDirectory(Helpers.DIRECTORY_INTERACTIVECONTENT
                            //    .Replace("{filename}", p_Entity.OldDirectory));
                            var path = Server.MapPath(string.Concat("~/CursosElearning/", p_Entity.OldDirectory));
                            if (Directory.Exists(path))
                            {
                                Directory.Delete(path, true);
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                else
                {
                    try
                    {
                        if (!p_Entity.WasRetrieved)
                        {
                            //remove files
                            //for (int i = 0; i < p_Entity.Files.Length; i++)
                            //{
                            //    if (p_Entity.Files[i].Uploaded)
                            //    {
                            //        l_storage.DeleteBlob(Helpers.DIRECTORY_INTERACTIVECONTENT
                            //            .Replace("{filename}", string.Concat(p_Entity.Directory, p_Entity.Files[i].Path)));
                            //    }
                            //}
                            var path = Server.MapPath(string.Concat("~/CursosElearning/", p_Entity.Directory));
                            if (Directory.Exists(path))
                            {
                                Directory.Delete(path, true);
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetMessageCrud(l_dt);
            }
            catch (Exception e)
            {
                if (l_storage != null)
                {
                    try
                    {
                        if (!p_Entity.WasRetrieved)
                        {
                            //remove files
                            //for (int i = 0; i < p_Entity.Files.Length; i++)
                            //{
                            //    if (p_Entity.Files[i].Uploaded)
                            //    {
                            //        l_storage.DeleteBlob(Helpers.DIRECTORY_INTERACTIVECONTENT
                            //            .Replace("{filename}", string.Concat(p_Entity.Directory, p_Entity.Files[i].Path)));
                            //    }
                            //}
                            var path = Server.MapPath(string.Concat("~/CursosElearning/", p_Entity.Directory));
                            if (Directory.Exists(path))
                            {
                                Directory.Delete(path, true);
                            }
                        }
                    }
                    catch
                    {
                        //prevent
                    }
                }
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
                Helpers.ClearDataTable(l_dt);
            }
        }
        [HttpPost]
        public ActionResult Delete(InteractiveContent_Entity p_Entity)
        {
            Bus_Crud l_crud;
            Bus_List l_list;
            AzureStorageHelpers l_storage = null;
            try
            {
                l_crud = new Bus_Crud();
                l_list = new Bus_List();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                var l_directory = l_list.RetrieveInteractiveContent(Convert.ToInt32(p_Entity.Id)).Rows[0]["Nombre_Directorio"].ToString();
                var l_response = l_crud.DeleteInteractiveContent(p_Entity);
                if (l_response.Rows[0]["Value"].ToString() == "1000")
                {
                    var path = Server.MapPath(string.Concat("~/CursosElearning/", l_directory));
                    if (Directory.Exists(path)) {
                        Directory.Delete(path, true);
                    }
                    //l_storage = new AzureStorageHelpers("Azure_ContainerName_ELearning");
                    //try
                    //{
                    //    //remove directory
                    //    l_storage.DeleteDirectory(Helpers.DIRECTORY_INTERACTIVECONTENT
                    //        .Replace("{filename}", l_directory));
                    //}
                    //catch
                    //{
                    //    //prevent
                    //}
                }

                return Helpers.GetMessageCrud(l_response);
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_crud = null;
                if (l_storage != null)
                {
                    l_storage.Clear();
                    l_storage = null;
                }
            }
        }
        [HttpPost]
        public JsonResult GetList()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetInteractiveContentList();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Cuin"],//0
                        row["Nombre_Curso"],//1
                        row["Url_Curso"],//2
                        row["Estado"]//3
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
    }
}
