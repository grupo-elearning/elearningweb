﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class PlaceController : Controller
    {
        // GET: Place
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Create()
        {
            return View();
        }
        [AppAuthorize]
        [AppLog]
        public ActionResult Edit(string id)
        {
            int l_id = Convert.ToInt32(Helpers.GetBase64Decode(id));
            Bus_List l_list = new Bus_List();
            Place_Entity l_model = null;
            var l_place = l_list.RetrievePlace(l_id);
            bool l_noData = l_place.Rows.Count == 0;
            if (!l_noData)
            {
                l_model = new Place_Entity
                {
                    Id = l_id,
                    Description = l_place.Rows[0]["Descripcion"].ToString(),
                    Status = l_place.Rows[0]["Estado"].ToString(),
                };
            }

            Helpers.ClearDataTable(l_place);
            l_list = null;

            ViewBag.NGon.Model = l_model;

            if (l_noData)
            {
                return View("NotFound");
            }
            else
            {
                return View(l_model);
            }
        }
        [HttpPost]
        public ActionResult Create(Place_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Id = 0;
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.InsertUpdatePlace(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public ActionResult Edit(Place_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.InsertUpdatePlace(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public ActionResult Delete(Place_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.DeletePlace(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public JsonResult GetList()
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetPlaceList();
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Luga"],//0
                        row["Descripcion"],//1
                        row["Estado"]//2
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
    }
}
