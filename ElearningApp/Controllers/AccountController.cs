﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Dao;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        [AppNoCache]
        public ActionResult Logout()
        {
            var l_from_carrier_login = SessionHelpers.IsFromCarrierLogin();
            SessionHelpers.DestroySession();
            if (l_from_carrier_login)
            {
                return RedirectToAction("CarrierLogin");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [AppIEBrowser]
        [AppNoCache]
        public ActionResult Login()
        {
            if (!SessionHelpers.IsAuthenticated())
            {
                if (TempData["VerifyYourEmailCode"] != null)
                {
                    ViewBag.VerifyYourEmailCode = TempData["VerifyYourEmailCode"].ToString();
                    ViewBag.VerifyYourEmailMessage = TempData["VerifyYourEmailMessage"].ToString();
                    TempData.Remove("VerifyYourEmailCode");
                    TempData.Remove("VerifyYourEmailMessage");
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [AppIEBrowser]
        [AppNoCache]
        public ActionResult CarrierLogin()
        {
            if (!SessionHelpers.IsAuthenticated())
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [AppNoCache]
        public ActionResult VerifyYourEmail(string code)
        {
            Bus_Crud l_crud;
            try
            {
                var l_code = EncryptorHelpers.Decrypt(code).Split('-');
                l_crud = new Bus_Crud();
                var l_response = l_crud.SetVerifyYourEmail(new Register_Entity
                {
                    RegisteredId = Convert.ToDecimal(l_code[0]),
                    Actions = l_code[1],
                    Origin = l_code[2]
                });
                if (l_response.Rows[0]["Value"].ToString() == "1000")
                {
                    EmailHelpers.SendCredentialsAfterVerifyYourEmail(Convert.ToString(l_response.Rows[0]["Username"]), Convert.ToString(l_response.Rows[0]["Password"]), Convert.ToString(l_response.Rows[0]["Email"]));
                }

                TempData["VerifyYourEmailCode"] = l_response.Rows[0]["Value"].ToString();
                TempData["VerifyYourEmailMessage"] = l_response.Rows[0]["Mensaje"].ToString();

                RouteData.Values.Remove("id");
                Helpers.ClearDataTable(l_response);

                return RedirectToAction("Login", "Account");
            }
            catch
            {
                RouteData.Values.Remove("id");
                return RedirectToAction("Index", "Home");
            }
            finally
            {
                l_crud = null;
            }
        }
        #region _PartialView
        [HttpPost]
        public ActionResult LoginModal()
        {
            string l_username = "", l_password = "", l_rememberme = "N";
            if (Request.Cookies["remember_me"] != null)
            {
                //get cookie
                l_username = EncryptorHelpers.Decrypt(Request.Cookies["remember_me"].Values["username"]);
                l_password = EncryptorHelpers.Decrypt(Request.Cookies["remember_me"].Values["password"]);
                l_rememberme = "S";
            }
            ViewBag.Username = l_username;
            ViewBag.Password = l_password;
            ViewBag.RememberMe = l_rememberme;
            return PartialView("PartialView/_LoginModalPartialView");
        }
        [HttpPost]
        public ActionResult RegisterModal()
        {
            return PartialView("PartialView/_RegisterModalPartialView");
        }
        [HttpPost]
        public ActionResult ChangePasswordModal()
        {
            return PartialView("PartialView/_ChangePasswordModalPartialView");
        }
        [HttpPost]
        public ActionResult RetrieveUserPassModal()
        {
            return PartialView("PartialView/_RetrieveUserPassModalPartialView");
        }
        #endregion _PartialView
        [HttpPost]
        public ActionResult GetRegister(Register_Model p_Model)
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                var l_row = l_list.GetRegister(p_Model).Rows[0];
                l_result = new List<object>
                {
                    l_row["Accion"],//0
                    l_row["Ap_Paterno"],//1
                    l_row["Ap_Materno"],//2
                    l_row["Nombres"],//3
                    l_row["Email"],//4
                    l_row["Celular"],//5
                    l_row["Mensaje"]//6
                };

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = int.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public ActionResult Register(Register_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.IpAddress = RequestHelpers.GetClientIpAddress(HttpContext.Request);
                var l_response = l_crud.InsertRegister(p_Entity);
                if (l_response.Rows[0]["Value"].ToString() == "1000")
                {
                    p_Entity.VerificationCode = l_response.Rows[0]["Code"].ToString();
                    EmailHelpers.SendVerifyYourEmail(p_Entity);
                }
                return Helpers.GetMessageCrud(l_response);
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public ActionResult ChangePassword(ChangePassword_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.UserId = SessionHelpers.GetUsername();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                var l_response = l_crud.UpdatePassword(p_Entity);
                if (l_response.Rows[0]["Value"].ToString() == "1000")
                {
                    //send email
                    EmailHelpers.SendNewPassword(p_Entity.NewPassword);
                }
                return Helpers.GetMessageCrud(l_response);
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public JsonResult GetLoginValidate(Account_Model p_Model)
        {
            Dao_List l_dao;
            List<object> l_result;
            DataTable l_records;
            p_Model.AppId = Convert.ToDecimal(System.Configuration.ConfigurationManager.AppSettings.Get("App_AppId"));
            try
            {
                SessionHelpers.RemoveVirtualSession();
                l_dao = new Dao_List();
                l_records = l_dao.GetLoginValidate(p_Model);
                if (l_records.Rows[0]["Value"].ToString() == "1000")
                {
                    SessionHelpers.SetVirtualLogin("General");
                    SessionHelpers.SetVirtualUsername(p_Model.Username);
                    SessionHelpers.SetVirtualPassword(p_Model.Password);
                    l_result = GetProfiles(p_Model, l_dao);
                    SessionHelpers.SetVirtualProfiles(l_result);
                }
                else
                {
                    l_result = new List<object>();
                    foreach (DataRow row in l_records.Rows)
                    {
                        l_result.Add(new List<object>
                        {
                            "Error",//0
                            row["Value"],//1
                            row["Mensaje"]//2
                        });
                    }
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_dao = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetCarrierLoginValidate(Account_Model p_Model)
        {
            Dao_List l_dao;
            List<object> l_result;
            DataTable l_records;
            try
            {
                SessionHelpers.RemoveVirtualSession();
                l_dao = new Dao_List();
                l_records = l_dao.GetCarrierLoginValidate(p_Model);
                if (l_records.Rows[0]["Value"].ToString() == "1000")
                {
                    SessionHelpers.SetVirtualLogin("Carrier");
                    SessionHelpers.SetVirtualUsername(p_Model.Username);
                    l_result = GetCarrierProfiles(p_Model, l_dao);
                    SessionHelpers.SetVirtualProfiles(l_result);
                }
                else
                {
                    l_result = new List<object>();
                    foreach (DataRow row in l_records.Rows)
                    {
                        l_result.Add(new List<object>
                        {
                            "Error",//0
                            row["Value"],//1
                            row["Mensaje"]//2
                        });
                    }
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_dao = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult Login(Account_Model p_Model)
        {
            if (SessionHelpers.ExistsProfileVirtual(p_Model.ProfileId))
            {
                p_Model.Browser = RequestHelpers.GetBrowserInformation(HttpContext.Request);
                p_Model.IPAddress = RequestHelpers.GetClientIpAddress(HttpContext.Request);
                SessionHelpers.InitializeSession(p_Model);
                SessionHelpers.SetAuthenticated();
                if (p_Model.RememberMe)
                {
                    //set cookie
                    HttpCookie l_cookie = new HttpCookie("remember_me");
                    l_cookie.Values.Add("username", EncryptorHelpers.Encrypt(SessionHelpers.GetVirtualUsername()));
                    l_cookie.Values.Add("password", EncryptorHelpers.Encrypt(SessionHelpers.GetVirtualPassword()));
                    l_cookie.Expires = DateTime.Now.AddDays(365);
                    l_cookie.HttpOnly = true;
                    Response.Cookies.Add(l_cookie);
                }
                else
                {
                    if (Response.Cookies["remember_me"] != null)
                    {
                        Response.Cookies["remember_me"].Expires = DateTime.Now.AddDays(-1);
                    }
                }
                SessionHelpers.RemoveVirtualSession();
                //set temporal data
                TempData["FromLogin"] = true;
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1
                    },
                    MaxJsonLength = int.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 0
                    },
                    MaxJsonLength = int.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
        }
        [HttpPost]
        public JsonResult CarrierLogin(Account_Model p_Model)
        {
            if (SessionHelpers.ExistsProfileVirtual(p_Model.ProfileId))
            {
                p_Model.Browser = RequestHelpers.GetBrowserInformation(HttpContext.Request);
                p_Model.IPAddress = RequestHelpers.GetClientIpAddress(HttpContext.Request);
                SessionHelpers.InitializeSession(p_Model);
                SessionHelpers.SetAuthenticated();
                SessionHelpers.RemoveVirtualSession();
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            else
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        status = 0
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
        }
        [HttpPost]
        public JsonResult GetStatus()
        {
            return new JsonResult()
            {
                Data = new
                {
                    status = SessionHelpers.IsAuthenticated(),
                    session = SessionHelpers.GetSessionID()
                },
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        [HttpPost]
        public JsonResult KeepActive()
        {
            return new JsonResult()
            {
                Data = null,
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpPost]
        public ActionResult RetrieveUserPass(Register_Entity p_Entity)
        {
            Bus_Crud l_crud;
            Bus_List l_list;
            try
            {
                l_crud = new Bus_Crud();
                l_list = new Bus_List();
                var p_Model = new ValEmail_Model();
                p_Model.Email = p_Entity.Email;
                p_Model.AppId = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings.Get("App_AppId"));
                
                var l_response = l_list.GetValidarEmail(p_Model);
                if (l_response.Rows[0]["Value"].ToString() == "1000")
                {
                    var l_username = l_response.Rows[0]["USER_RED"].ToString();

                    var l_user_info = l_list.GetUserInfo(l_username);

                    var l_full_name = l_user_info.Rows[0]["FULL_NAME"].ToString();
                    var l_password = l_user_info.Rows[0]["PASSWORD"].ToString();

                    var l_person = new Person_Model();
                    l_person.Email = p_Model.Email;
                    l_person.Fullname = l_full_name;
                    l_person.Username = l_username;
                    l_person.Password = l_password;

                    EmailHelpers.SendCredentials(l_person);

                    return new JsonResult()
                    {
                        Data = new
                        {
                            status = "1000",
                            response = "Sus Credenciales fueron enviadas a su Correo Electrónico.",
                            exception = "",
                            others = ""
                        },
                        MaxJsonLength = Int32.MaxValue,
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                else {
                    return Helpers.GetMessageCrud(l_response);
                }
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        #region private
        [NonAction]
        private List<object> GetProfiles(Account_Model p_Model, Dao_List p_dao)
        {
            List<object> l_result = new List<object>();
            DataTable l_records = p_dao.GetProfileList(p_Model);
            foreach (DataRow row in l_records.Rows)
            {
                l_result.Add(new List<object>
                {
                    "Success",//0
                    row["codi_perf"],//1
                    row["descripcion"]//2
                });
            }

            Helpers.ClearDataTable(l_records);

            return l_result;
        }
        private List<object> GetCarrierProfiles(Account_Model p_Model, Dao_List p_dao)
        {
            List<object> l_result = new List<object>();
            DataTable l_records = p_dao.GetCarrierProfileList(p_Model);
            foreach (DataRow row in l_records.Rows)
            {
                l_result.Add(new List<object>
                {
                    "Success",//0
                    row["Codi_Perf"],//1
                    row["Descripcion"]//2
                });
            }

            Helpers.ClearDataTable(l_records);

            return l_result;
        }
        #endregion private
    }
}