﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class Dashboard1Controller : Controller
    {
        // GET: Dashboard1
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Detail(int id)
        {
            Bus_Report l_report = new Bus_Report();
            string l_url_photo = Helpers.GetAppSettings("Azure_StorageUrl_Administracion_Proveedor");
            var l_record = l_report.Dashboard1GetRegisteredDetail(id).Rows[0];
            Dashboard1RegisteredDetail_Model l_model = new Dashboard1RegisteredDetail_Model
            {
                RegisterId = Convert.ToDecimal(l_record["Codi_Pins"]),
                PersonLastName1 = l_record["Ap_Paterno"].ToString(),
                PersonLastName2 = l_record["Ap_Materno"].ToString(),
                PersonFirstName = l_record["Nombres"].ToString(),
                PersonEmail = l_record["Persona_Email"].ToString(),
                PersonFichaSap = l_record["Ficha_Sap"].ToString(),
                PersonType = l_record["Tipo"].ToString(),
                PersonPhoto = "",
                ProviderSAPCode = l_record["Codigo_Sap"].ToString(),
                ProviderRuc = l_record["Ruc"].ToString(),
                ProviderBusinessName = l_record["Razon_Social"].ToString(),
                ProviderEmail = l_record["Proveedor_Email"].ToString(),
                TotalAttempts = Convert.ToInt32(l_record["Intento_Total"]),
                ApproveScore = Math.Round(Convert.ToDecimal(l_record["Nota_Aprobado"]), 2),
                InTest = Convert.ToInt32(l_record["Intento_Progreso"]) == 1,
                Attempts = GetAttempt(Convert.ToInt32(l_record["Codi_Pins"]), l_report)
            };
            //get photo
            if (l_model.PersonType == "I")
            {
                if (l_model.PersonFichaSap != "")
                {
                    var l_storage = new AzureStorageHelpers("Azure_ContainerName_FotoTrabajador", BlobContainerPublicAccessType.Blob, "Azure_AccountName_FotoTrabajador", "Azure_AccessKey_FotoTrabajador");
                    var l_blob = l_storage.GetBlob(Helpers.DIRECTORY_EMPLOYEE_PHOTO.Replace("{filename}", l_model.PersonFichaSap + ".jpg"));
                    if (l_blob.Exists())
                    {
                        l_model.PersonPhoto = Helpers.GetAppSettings("Azure_StorageUrl_FotoTrabajador") + l_blob.Name;
                    }
                    l_storage.Clear();
                }
            }
            else if (l_model.PersonType == "E")
            {
                l_model.PersonPhoto = Convert.IsDBNull(l_record["Foto"]) ? "" : (l_url_photo + Helpers.DIRECTORY_PROVIDER_EMPLOYEE.Replace("{ruc}", l_record["Ruc"].ToString()).Replace("{filename}", l_record["Foto"].ToString()));
            }

            return PartialView("PartialView/_DetailPartialView", l_model);
        }
        [HttpPost]
        public JsonResult GetDashboard1(int year)
        {
            Bus_Report l_report;
            List<object> l_result;
            try
            {
                l_report = new Bus_Report();
                l_result = new List<object>();
                var l_records = l_report.Dashboard1GetVacanciesVSRegistered(Convert.ToInt32(SessionHelpers.GetProfile().AreaId), year);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pcur"],//0
                        row["Nombre"],//1
                        row["Nro_Vacante"],//2
                        row["Inscritos"],//3
                        Convert.ToString(row["Ilimitado"]) == "N" ? Convert.ToDateTime(row["Fecha_Inicio"]).ToString("dd/MM/yyyy") : "Ilimitado",//4
                        Convert.ToString(row["Ilimitado"]) == "N" ? Convert.ToDateTime(row["Fecha_Fin"]).ToString("dd/MM/yyyy") : "Ilimitado"//5
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        status = 1,
                        response = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_report = null;
                l_result = null;
            }
        }
        [HttpPost]
        public JsonResult GetProgramming(int programming)
        {
            Bus_Report l_report;
            List<object> l_result;
            try
            {
                l_report = new Bus_Report();
                l_result = new List<object>();
                var l_records = l_report.Dashboard1GetProgrammingRegistered(programming);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Pins"],//0
                        row["Documento"],//1
                        row["Nro_Doc"],//2
                        row["Nombre"],//3
                        row["Tipo"],//4
                        row["Proveedor"]//5
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_report = null;
                l_result = null;
            }
        }
        #region private
        private List<object> GetAttempt(int p_id, Bus_Report p_bus)
        {
            List<object> l_result;
            try
            {
                l_result = new List<object>();
                var l_records = p_bus.Dashboard1GetRegisteredAttempt(p_id);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        Convert.IsDBNull(row["Fecha_Inicio"]) ? "" : Convert.ToDateTime(row["Fecha_Inicio"]).ToString("dd/MM/yyyy"),//0
                        Convert.IsDBNull(row["Fecha_Fin"]) ? "" : Convert.ToDateTime(row["Fecha_Fin"]).ToString("dd/MM/yyyy"),//1
                        Math.Round(Convert.ToDecimal(row["Nota"]), 2),//2
                        row["Flag_Actual"],//3
                        row["Tiempo_Resolver"],//4
                        row["Flag_Aprobado"]//5
                    });
                }

                Helpers.ClearDataTable(l_records);

                return l_result;
            }
            catch
            {
                throw;
            }
        }
        #endregion private
    }
}