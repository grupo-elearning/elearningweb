﻿using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class AreaProviderController : Controller
    {
        // GET: AreaProvider
        [AppAuthorize]
        [AppLog]
        public ActionResult Edit()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Edit(AreaProvider_Entity p_Entity)
        {
            Bus_Crud l_crud;
            try
            {
                l_crud = new Bus_Crud();
                p_Entity.Username = SessionHelpers.GetUsername();
                p_Entity.IPAddress = SessionHelpers.GetIPAddress();
                return Helpers.GetMessageCrud(l_crud.InsertUpdateAreaProvider(p_Entity));
            }
            catch (Exception e)
            {
                return Helpers.GetException(e);
            }
            finally
            {
                l_crud = null;
            }
        }
        [HttpPost]
        public JsonResult GetList(int id)
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_area_provider = l_list.RetrieveAreaProvider(id);
                foreach (DataRow row in l_area_provider.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Proa"],//0
                        row["Codi_Prov"],//1
                        row["Ruc"],//2
                        row["Razon_Social"]//3
                    });
                }

                Helpers.ClearDataTable(l_area_provider);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
        [HttpPost]
        public ActionResult Provider(AreaProvider_Model p_Model)
        {
            ViewBag.NGon.Model = p_Model;
            return PartialView("PartialView/_ProviderPartialView", p_Model);
        }
        [HttpPost]
        public JsonResult GetProvider(AreaProvider_Model p_Model)
        {
            Bus_List l_list;
            List<object> l_result;
            try
            {
                l_list = new Bus_List();
                l_result = new List<object>();
                var l_records = l_list.GetProvider2AreaProvider(p_Model);
                foreach (DataRow row in l_records.Rows)
                {
                    l_result.Add(new List<object>
                    {
                        row["Codi_Prov"],//0
                        row["Ruc"],//1
                        row["Razon_Social"]//2
                    });
                }

                Helpers.ClearDataTable(l_records);

                return new JsonResult()
                {
                    Data = new
                    {
                        data = l_result
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        data = new string[0]
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            finally
            {
                l_list = null;
                l_result = null;
            }
        }
    }
}
