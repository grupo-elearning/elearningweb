﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ElearningApp.Filters;
using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;

namespace ElearningApp.Controllers
{
    [AppAuthenticate]
    public class HomeLMSController : Controller
    {
        [AppAuthorize]
        [AppLog]
        public ActionResult Index()
        {
            ViewBag.NGon.fullname = SessionHelpers.GetUser().FullName;
            ViewBag.NGon.perfil = SessionHelpers.GetProfile().Description.ToUpper();
            ViewBag.NGon.token_access = Helpers.LoginApiEleaLms();
            ViewBag.NGon.UrlApi_HomeLms_listSlider = System.Configuration.ConfigurationManager.AppSettings["UrlApi_HomeLms_listSlider"];
            ViewBag.NGon.UrlImgUser = System.Configuration.ConfigurationManager.AppSettings["UrlImgUser"];
            ViewBag.NGon.UrlImgUser = ViewBag.NGon.UrlImgUser + SessionHelpers.GetUser().EmployeeFichaSap + ".jpg";
            return View();
        }

        [AppAuthorize]
        [AppLog]
        public ActionResult Progress()
        {
            ViewBag.NGon.fullname = SessionHelpers.GetUser().FullName;
            ViewBag.NGon.perfil = SessionHelpers.GetProfile().Description.ToUpper();
            ViewBag.NGon.codi_pers = SessionHelpers.GetUser().PersonId;
            ViewBag.NGon.codi_trab = SessionHelpers.GetUser().EmployeeId;
            ViewBag.NGon.ficha_sap = SessionHelpers.GetUser().EmployeeFichaSap;
            ViewBag.NGon.user_name = SessionHelpers.GetUsername();
            ViewBag.NGon.token_access = Helpers.LoginApiEleaLms();
            ViewBag.NGon.UrlImgUser = System.Configuration.ConfigurationManager.AppSettings["UrlImgUser"];
            ViewBag.NGon.UrlImgUser = ViewBag.NGon.UrlImgUser + SessionHelpers.GetUser().EmployeeFichaSap + ".jpg";
            ViewBag.NGon.UrlApi_miprog_lastAccess = System.Configuration.ConfigurationManager.AppSettings["UrlApi_miprog_lastAccess"];
            ViewBag.NGon.UrlApi_miprog_getArea = System.Configuration.ConfigurationManager.AppSettings["UrlApi_miprog_getArea"];
            ViewBag.NGon.UrlApi_miprog_listCursos = System.Configuration.ConfigurationManager.AppSettings["UrlApi_miprog_listCursos"];
            ViewBag.NGon.UrlApi_miprog_listRanking = System.Configuration.ConfigurationManager.AppSettings["UrlApi_miprog_listRanking"];
            ViewBag.NGon.UrlApi_miprog_getCertificado = System.Configuration.ConfigurationManager.AppSettings["UrlApi_miprog_getCertificado"];
            ViewBag.NGon.UrlApi_insign_identificar = System.Configuration.ConfigurationManager.AppSettings["UrlApi_insign_identificar"];
            return View();
        }

        [AppAuthorize]
        [AppLog]
        public ActionResult ListMyCourse()
        {
            ViewBag.NGon.fullname = SessionHelpers.GetUser().FullName;
            ViewBag.NGon.perfil = SessionHelpers.GetProfile().Description.ToUpper();
            ViewBag.NGon.codi_pers = SessionHelpers.GetUser().PersonId;
            ViewBag.NGon.codi_trab = SessionHelpers.GetUser().EmployeeId;
            ViewBag.NGon.ficha_sap = SessionHelpers.GetUser().EmployeeFichaSap;
            ViewBag.NGon.user_name = SessionHelpers.GetUsername();
            ViewBag.NGon.token_access = Helpers.LoginApiEleaLms();
            ViewBag.NGon.UrlImgUser = System.Configuration.ConfigurationManager.AppSettings["UrlImgUser"];
            ViewBag.NGon.UrlImgUser = ViewBag.NGon.UrlImgUser + SessionHelpers.GetUser().EmployeeFichaSap + ".jpg";
            return View();
        }

    }
}
