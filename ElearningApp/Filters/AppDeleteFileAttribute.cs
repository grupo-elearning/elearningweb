﻿using System.Web.Mvc;

namespace ElearningApp.Filters
{
    public class AppDeleteFileAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            filterContext.HttpContext.Response.Flush();
            string l_path = (filterContext.Result as FilePathResult).FileName;
            System.IO.File.Delete(l_path);
        }
    }
}