﻿using ElearningApp.Models;
using ElearningApp.Models.Dao;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ElearningApp.Filters
{
    public class AppLogAttribute : ActionFilterAttribute
    {
        private string Controller = null;
        private string Action = null;
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            Action = filterContext.ActionDescriptor.ActionName;
        }
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            decimal l_sid = SessionHelpers.GetSessionLog();
            if (l_sid != 0)
            {
                Dao_Crud l_dao = new Dao_Crud();
                try
                {
                    l_dao.SaveLog(new Log_Entity
                    {
                        ControllerName = Controller,
                        ActionName = Action,
                        SessionId = l_sid
                    });
                }
                finally
                {
                    l_dao = null;
                }
            }
        }
    }
}