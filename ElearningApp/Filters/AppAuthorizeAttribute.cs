﻿using ElearningApp.Models;
using ElearningApp.Models.Business;
using ElearningApp.Models.Dao;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ElearningApp.Filters
{
    public class AppAuthorizeAttribute : AuthorizeAttribute
    {
        private string Controller = null;
        private string Action = null;

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            Action = filterContext.ActionDescriptor.ActionName;

            if (!AuthorizeCore(filterContext.HttpContext))
            {
                HandleUnauthorizedRequest(filterContext);
            }
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }

            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetAuthorizeAccessToControllerAction(Convert.ToInt32(SessionHelpers.GetProfileId()), Controller, Action);
            }
            catch
            {
                return false;
            }
            finally
            {
                dao = null;
            }
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary(new { controller = "Error", action = "Forbidden" }));
        }
    }
}