﻿using ElearningApp.Models;
using System.Web.Mvc;
using System.Web.Routing;

namespace ElearningApp.Filters
{
    public class AppIEBrowserAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (RequestHelpers.IsIEBrowser(filterContext.HttpContext.Request))
            {
                filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary(new { controller = "Error", action = "IEBrowser" }));
            }
        }
    }
}