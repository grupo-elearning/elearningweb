﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using ElearningApp.App_Start;
using ElearningApp.Controllers;
using ElearningApp.Models.Dao;
using ElearningApp.Models.Entity;
using ElearningApp.Models;

namespace ElearningApp
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Código que se ejecuta al iniciar la aplicación
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
        }
        //protected void Application_BeginRequest()
        //{
        //    Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //    Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
        //    Response.Cache.SetNoStore();
        //}
        protected void Application_Error(object sender, EventArgs e)
        {
            var exception = Server.GetLastError();

            // Log exception to database if you want to.

            var httpException = exception as HttpException;
            if (httpException != null && httpException.GetHttpCode() == 403)
            {
                // Process 403 HTTP errors
                Response.Clear();
                Server.ClearError();
                Response.TrySkipIisCustomErrors = true;

                IController controller = new ErrorController();

                var routeData = new RouteData();
                routeData.Values.Add("controller", "Error");
                routeData.Values.Add("action", "Forbidden");

                var requestContext = new RequestContext(
                     new HttpContextWrapper(Context), routeData);
                controller.Execute(requestContext);
            }
            else if (httpException != null && httpException.GetHttpCode() == 404)
            {
                // Process 404 HTTP errors
                Response.Clear();
                Server.ClearError();
                Response.TrySkipIisCustomErrors = true;

                IController controller = new ErrorController();

                var routeData = new RouteData();
                routeData.Values.Add("controller", "Error");
                routeData.Values.Add("action", "NotFound");

                var requestContext = new RequestContext(
                     new HttpContextWrapper(Context), routeData);
                controller.Execute(requestContext);
            }
            else if (httpException != null)
            {
                // Process HTTP errors
                Response.Clear();
                Server.ClearError();
                Response.TrySkipIisCustomErrors = true;

                IController controller = new ErrorController();

                var routeData = new RouteData();
                routeData.Values.Add("controller", "Error");
                routeData.Values.Add("action", "Error");

                var requestContext = new RequestContext(
                     new HttpContextWrapper(Context), routeData);
                controller.Execute(requestContext);
            }
            else
            {
                // Process HTTP errors
                Response.Clear();
                Server.ClearError();
                Response.TrySkipIisCustomErrors = true;

                IController controller = new ErrorController();

                var routeData = new RouteData();
                routeData.Values.Add("controller", "Error");
                routeData.Values.Add("action", "Error");

                var requestContext = new RequestContext(
                     new HttpContextWrapper(Context), routeData);
                controller.Execute(requestContext);
            }
        }
        protected void Session_End(object sender, EventArgs e)
        {
            // Código que se ejecuta cuando finaliza una sesión.
            // Nota: el evento Session_End se desencadena sólo cuando el modo sessionstate
            // se establece como InProc en el archivo Web.config. Si el modo de sesión se establece como StateServer 
            // o SQLServer, el evento no se genera.
            Dao_Crud l_dao = null;
            try
            {
                l_dao = new Dao_Crud();
                l_dao.EndSession(new Session_Entity
                {
                    SessionId = SessionHelpers.GetSessionLog(GetSession())
                });
            }
            finally
            {
                l_dao = null;
            }
        }
        #region private
        private HttpSessionState GetSession()
        {
            //Check if current context exists
            if (HttpContext.Current != null)
            {
                return HttpContext.Current.Session;
            }
            else
            {
                return this.Session;
            }
        }
        #endregion private
    }
}