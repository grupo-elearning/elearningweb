﻿using ClosedXML.Excel;
using ElearningApp.Models.Dao;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;

namespace ElearningApp.Models
{
    public class Helpers
    {
        public static string APP_VERSION = "14.3";
        public const string DIRECTORY_PROVIDER_EMPLOYEE = "administracion/proveedor/{ruc}/trabajador/{filename}";
        public const string DIRECTORY_PROVIDER_VEHICLE = "administracion/proveedor/{ruc}/vehiculo/{filename}";
        public const string DIRECTORY_PROVIDER_COMPANY = "administracion/proveedor/{ruc}/empresa/{filename}";
        public const string DIRECTORY_DOCUMENT_MODEL = "administracion/documento-sustentatorio/{filename}";
        public const string DIRECTORY_COURSE_CONTENT = "elearning/curso/{name}/{filename}";
        public const string DIRECTORY_CERTIFICATE = "elearning/constancia/{filename}";
        public const string DIRECTORY_INTERACTIVECONTENT = "elearning/curso-interactivo/{filename}";
        public const string DIRECTORY_NOTICE = "elearning/anuncio/{filename}";
        public const string DIRECTORY_EMPLOYEE_PHOTO = "capacitacion/fotos_trabajador/{filename}";

        public static List<AppObject_Entity> RemoveDocumentsOfVehiclesFromAppObjects(List<AppObject_Entity> p_menu)
        {
            if (SessionHelpers.IsAuthenticated())
            {
                var l_user = SessionHelpers.GetUser();
                if (l_user.ProviderId != 0)
                {
                    var l_fob = Convert.ToInt32(GetParameterByKey("CODI_TERM_FOB"));
                    var l_dao = new Dao_List();
                    var l_incoterms = l_dao.GetIncotermsByProvider(Convert.ToInt32(l_user.ProviderId));
                    if (l_incoterms == l_fob)
                    {
                        var l_object = Convert.ToDecimal(GetParameterByKey("CODI_OBJE_DOCUMENTS_OF_VEHICLES"));
                        return p_menu.Where(e => e.Id != l_object).ToList();
                    }
                }
            }
            return p_menu;
        }
        public static bool IsLogisticaDocument(decimal p_tdos, IEnumerable<ProviderDocumentLogistica_Model> p_documents)
        {
            return p_documents.Any(x => x.CodiTdos == p_tdos);
        }
        public static string GetHtmlTooltipFromText(string p_text, int p_length = 30)
        {
            return p_text.Length < p_length ? "" : (" data-toggle=\"tooltip\" data-placement=\"top\" title=\"" + p_text + "\" ");
        }
        public static string GetFileNameOfProviderVehiclePhoto(string p_realName)
        {
            return DateTime.Now.ToString("ddMMyyyyHHmmss") + GetFileNameOfProvider(p_realName);
        }
        public static string GetFileNameOfProviderVehicleDocument(string p_plate, string p_sustenance, string p_realName)
        {
            return p_plate + "-" + p_sustenance + "-" + DateTime.Now.ToString("ddMMyyyyHHmmss") + GetFileNameOfProvider(p_realName);
        }
        public static string GetFileNameOfProviderEmployeePhoto(string p_realName)
        {
            return DateTime.Now.ToString("ddMMyyyyHHmmss") + GetFileNameOfProvider(p_realName);
        }
        public static string GetFileNameOfProviderEmployeeDocument(string p_identity, string p_sustenance, string p_realName)
        {
            return p_identity + "-" + p_sustenance + "-" + DateTime.Now.ToString("ddMMyyyyHHmmss") + GetFileNameOfProvider(p_realName);
        }
        public static string GetFileNameOfProviderDocument(string p_ruc, string p_code, string p_realName)
        {
            return p_ruc + "-" + p_code + "-" + DateTime.Now.ToString("ddMMyyyyHHmmss") + GetFileNameOfProvider(p_realName);
        }
        public static string GetFileNameOfProviderAccountDocument(string p_realName)
        {
            return DateTime.Now.ToString("ddMMyyyyHHmmss") + GetFileNameOfProvider(p_realName);
        }
        public static string ReplaceFirstOccurrence(string p_source, string p_find, string p_replace)
        {
            int l_index = p_source.IndexOf(p_find);
            string result = p_source.Remove(l_index, p_find.Length).Insert(l_index, p_replace);
            return result;
        }
        public static string ReplaceLastOccurrence(string p_source, string p_find, string p_replace)
        {
            int l_index = p_source.LastIndexOf(p_find);
            string result = p_source.Remove(l_index, p_find.Length).Insert(l_index, p_replace);
            return result;
        }
        public static void ClearDataTable(DataTable p_dt)
        {
            if (p_dt != null)
            {
                p_dt.Clear();
                p_dt.Dispose();
                p_dt = null;
            }
        }
        public static string GetTestQuestion(string p_question)
        {
            List<string> l_list = new List<string>();
            bool l_open = false;
            string l_word = null, l_return;
            for (int i = 0; i < p_question.Length; i++)
            {
                if (p_question[i] == '{')
                {
                    l_open = true;
                    l_word = "{";
                }
                else if (p_question[i] == '}')
                {
                    l_open = false;
                    if (l_word != null)
                    {
                        l_word += "}";
                        l_list.Add(l_word);
                        l_word = null;
                    }
                }
                else
                {
                    if (l_open)
                    {
                        l_word += Convert.ToString(p_question[i]);
                    }
                }
            }
            l_return = p_question;
            foreach (var i in l_list)
            {
                l_return = l_return.Replace(i, "{select_of_answer}");
            }
            return l_return;
        }
        public static string GetBase64Encode(string plainText)
        {
            try
            {
                var plainTextBytes = System.Text.Encoding.GetEncoding("utf-8").GetBytes(plainText);
                return Convert.ToBase64String(plainTextBytes)
                    .Replace('+', '-')
                    .Replace('/', '_');
            }
            catch
            {
                throw;
            }
        }
        public static string GetBase64Decode(string base64EncodedData)
        {
            try
            {
                var base64EncodedBytes = Convert.FromBase64String(base64EncodedData
                .Replace('-', '+')
                .Replace('_', '/'));
                return System.Text.Encoding.GetEncoding("utf-8").GetString(base64EncodedBytes);
            }
            catch
            {
                throw;
            }
        }
        public static string GetAppSettings(string p_key)
        {
            return System.Configuration.ConfigurationManager.AppSettings.Get(p_key);
        }
        public static string GetParameterByKey(string p_key)
        {
            Dao_List Dao;
            try
            {
                Dao = new Dao_List();
                return Dao.GetParameterByKey(p_key);
            }
            catch
            {
                return "";
            }
            finally
            {
                Dao = null;
            }
        }
        public static DateTime GetUTCDate()
        {
            Dao_List Dao;
            try
            {
                Dao = new Dao_List();
                return Dao.GetUTCDate();
            }
            catch
            {
                return default(DateTime);
            }
            finally
            {
                Dao = null;
            }
        }
        public static string GetAppActiveObject(string p_c, string p_a, string p_ac, string p_aa, List<AppObject_Entity> p_l = null)
        {
            bool l_isParent = false;
            decimal l_parentId = 0;
            if (p_c == p_ac && p_a == p_aa)
            {
                return "active";
            }
            foreach (var item in p_l)
            {
                //if (item.Controller == p_ac && item.Action == p_aa)
                if (item.Controller == p_ac)
                {
                    l_parentId = item.ParentId;
                    break;
                }
            }
            while (!l_isParent && l_parentId != 0)
            {
                l_isParent = IsParent(ref l_parentId, p_c, p_a, p_l);
            }
            if (l_isParent)
            {
                return "active";
            }
            return "";
        }
        public static List<AppObject_Entity> GetAppObject()
        {
            Dao_List l_dao = new Dao_List();
            int l_idx = 0;
            List<AppObject_Entity> l_list = l_dao.GetAppObject(new AppObject_Model
            {
                AppId = Convert.ToInt32(SessionHelpers.GetAppId()),
                ProfileId = Convert.ToInt32(SessionHelpers.GetProfileId()),
                UserId = SessionHelpers.GetUsername(),
                ParentId = 0
            });
            while (l_idx < l_list.Count)
            {
                if (!l_list.ElementAt(l_idx).Completed)
                {
                    var l_nlist = l_dao.GetAppObject(new AppObject_Model
                    {
                        AppId = Convert.ToInt32(SessionHelpers.GetAppId()),
                        ProfileId = Convert.ToInt32(SessionHelpers.GetProfileId()),
                        UserId = SessionHelpers.GetUsername(),
                        ParentId = Convert.ToInt32(l_list.ElementAt(l_idx).Id)
                    });
                    foreach (var item in l_nlist)
                    {
                        l_list.Add(item);
                    }
                    l_list.ElementAt(l_idx).HasChildren = true;
                    l_list.ElementAt(l_idx).Completed = true;
                }
                l_idx++;
            }
            return l_list;
        }
        public static JsonResult GetMessageCrud(DataTable p_dt, object p_others = null)
        {
            string l_message = null, l_code = null;
            try
            {
                foreach (DataRow row in p_dt.Rows)
                {
                    l_message = row["Mensaje"].ToString();
                    l_code = row["Value"].ToString();
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        status = l_code,
                        response = l_message,
                        exception = l_message,
                        others = p_others
                    },
                    MaxJsonLength = Int32.MaxValue,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            catch
            {
                throw;
            }
        }
        public static JsonResult GetException(Exception p_e)
        {
            string l_message = p_e.Message;
            string l_exception = "An exception occurred.\n" +
                "- Type : " + p_e.GetType().Name + "\n" +
                "- Message: " + p_e.Message + "\n" +
                "- Stack Trace: " + p_e.StackTrace;
            Exception l_ie = p_e.InnerException;
            if (l_ie != null)
            {
                l_exception += "\n";
                l_exception += "The Inner Exception:\n" +
                    "- Exception Name: " + l_ie.GetType().Name + "\n" +
                    "- Message: " + l_ie.Message + "\n" +
                    "- Stack Trace: " + l_ie.StackTrace;
            }
            return new JsonResult()
            {
                Data = new
                {
                    status = -1,
                    response = l_message,
                    exception = l_exception
                },
                MaxJsonLength = Int32.MaxValue,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        public static Stream CreateThumbnail(Stream p_stream, int p_thumbWi, int p_thumbHi)
        {
            Image l_image;
            Bitmap l_thumbnail;
            MemoryStream l_mstream;
            int l_wi, l_hi;
            try
            {
                l_image = Image.FromStream(p_stream);

                if (l_image.Width <= p_thumbWi && l_image.Height <= p_thumbHi)
                {
                    p_stream.Position = 0;
                    return p_stream;
                }

                l_wi = p_thumbWi;
                l_hi = p_thumbHi;

                if (l_image.Width > l_image.Height)
                {
                    l_wi = p_thumbWi;
                    l_hi = (int)(l_image.Height * ((decimal)p_thumbWi / l_image.Width));
                }
                else
                {
                    l_hi = p_thumbHi;
                    l_wi = (int)(l_image.Width * ((decimal)p_thumbHi / l_image.Height));
                }

                l_thumbnail = new Bitmap(l_wi, l_hi);

                using (Graphics g = Graphics.FromImage(l_thumbnail))
                {
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.FillRectangle(Brushes.Transparent, 0, 0, l_wi, l_hi);
                    g.DrawImage(l_image, 0, 0, l_wi, l_hi);
                }
                l_mstream = new MemoryStream();
                l_thumbnail.Save(l_mstream, l_image.RawFormat);
                l_mstream.Position = 0;
                return l_mstream;
            }
            catch
            {
                return null;
            }
        }
        public static Stream CloneStream(Stream p_stream)
        {
            MemoryStream l_stream = new MemoryStream();
            long l_pos = p_stream.Position;
            p_stream.CopyTo(l_stream);
            p_stream.Position = l_pos;
            l_stream.Position = l_pos;
            return l_stream;
        }
        public static string GetMonthName(int p_month)
        {
            string[] l_names = { "enero", "febrero", "marzo", "abril", "mayo", "junio",
                "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"};
            return l_names[p_month - 1];
        }
        public static DataTable ExceltoDatatable(Stream p_stream, bool p_hasTitle = true)
        {
            using (XLWorkbook l_wb = new XLWorkbook(p_stream))
            {
                DataTable l_return = new DataTable();
                DataRow l_row;
                IXLWorksheet l_ws = l_wb.Worksheet(1);
                int l_total_column = l_ws.LastColumnUsed().ColumnNumber(),
                    l_total_rows = l_ws.LastRowUsed().RowNumber(),
                    l_first_row_data;
                IXLRow l_first_row_used = l_ws.FirstRowUsed();
                // get column name
                for (var l_i = 1; l_i <= l_total_column; l_i++)
                {
                    l_return.Columns.Add(p_hasTitle ? GetStringFromIXLCell(l_first_row_used.Cell(l_i)) : ("Column" + Convert.ToString(l_i)));
                }
                // get data
                l_first_row_data = p_hasTitle ? (l_first_row_used.RowNumber() + 1) : l_first_row_used.RowNumber();
                for (var l_i = l_first_row_data; l_i <= l_total_rows; l_i++)
                {
                    l_row = l_return.NewRow();
                    for (var l_j = 1; l_j <= l_total_column; l_j++)
                    {
                        l_row[l_j - 1] = GetStringFromIXLCell(l_ws.Row(l_i).Cell(l_j));
                    }
                    l_return.Rows.Add(l_row);
                    l_row = null;
                }
                return l_return;
            }
        }
        public static string Coalesce(object[] p_list)
        {
            foreach (var item in p_list)
            {
                if (!Convert.IsDBNull(item) && item.ToString().Trim() != "")
                {
                    return item.ToString();
                }
            }
            return "";
        }
        public static string GetConnectionStrings(string ps_name)
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings[ps_name].ConnectionString;
        }
        #region private
        private static string GetFileNameOfProvider(string p_realName)
        {
            int l_maxLength = 6, l_length = p_realName.Length;
            string l_return, l_name, l_extension;
            if (l_length <= l_maxLength)
            {
                l_return = p_realName;
            }
            else
            {
                var l_data = p_realName.Split('.');
                l_extension = l_data[l_data.Length - 1];
                l_name = string.Join("", l_data, 0, l_data.Length - 1);
                if (l_extension.Length + 1 < l_maxLength)
                {
                    l_return = p_realName.Substring(l_length - l_maxLength, l_maxLength);
                }
                else
                {
                    l_return = l_name.Substring(l_name.Length - 1, 1) + "." + l_extension;

                }
            }
            return l_return;
        }
        private static string GetStringFromIXLCell(IXLCell p_cell)
        {
            return p_cell.ValueCached == null ? p_cell.Value.ToString().Trim() : p_cell.ValueCached.ToString().Trim();
        }
        private static bool IsParent(ref decimal p_parentId, string p_controller, string p_action, List<AppObject_Entity> p_list)
        {
            foreach (var item in p_list)
            {
                if (item.Id == p_parentId)
                {
                    p_parentId = item.ParentId;
                    if (item.Controller == p_controller && item.Action == p_action)
                    {
                        return true;
                    }
                    break;
                }
            }
            return false;
        }
        public static string executeAPI(object json_data, string url, string token)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            using (var client = new HttpClient())
            {
                if (token != "")
                {
                    client.DefaultRequestHeaders.Add("authorization", token);
                }
                client.BaseAddress = new Uri(url);
                //HTTP POST
                var postTask = client.PostAsJsonAsync("", json_data);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return result.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    return "ERROR";
                }
            }
        }

        public static string LoginApiEleaLms()
        {
            string token = "";
            RequestModel_Auth ApiAuth;

            try
            {
                using (var clientAUTH = new HttpClient())
                {
                    ApiAuth = new RequestModel_Auth();

                    ApiAuth.Username = System.Configuration.ConfigurationManager.AppSettings["Api_EleaLms_Usua"];
                    ApiAuth.Password = System.Configuration.ConfigurationManager.AppSettings["Api_EleaLms_Pass"];

                    string urlToken = System.Configuration.ConfigurationManager.AppSettings["UrlApi_HomeLms_Token"];

                    clientAUTH.BaseAddress = new Uri(urlToken);

                    //HTTP POST
                    var postTask = clientAUTH.PostAsJsonAsync<RequestModel_Auth>("", ApiAuth);
                    postTask.Wait();

                    var result = postTask.Result;

                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<string>();
                        readTask.Wait();

                        token = readTask.Result;
                    }
                    else
                    {
                        token = "";
                    }
                }
            }
            catch (Exception e)
            {
                token = "";
            }

            return token;
        }

        #endregion private
    }
}