﻿using ElearningApp.Models.Dao;
using System;
using System.Data;

namespace ElearningApp.Models.Business
{
    public class Bus_Report
    {
        public DataTable Dashboard5GetCertificate(int p_register)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard5GetCertificate(p_register);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard5GetExecuted(int p_course)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard5GetExecuted(p_course);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard5GetRegistered(int p_programming)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard5GetRegistered(p_programming);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard3GetParticipantAnswer(int p_question)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard3GetParticipantAnswer(p_question);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard3GetParticipantQuestion(int p_attempt)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard3GetParticipantQuestion(p_attempt);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard3GetParticipantAttempt(int p_register)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard3GetParticipantAttempt(p_register);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard4GetProviderData(int p_provider)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard4GetProviderData(p_provider);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard4GetProvider(int p_area)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard4GetProvider(p_area);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard2GetSatisfaction(int p_area)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard2GetSatisfaction(p_area);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard3GetStatusParticipantsDetail(int p_area, string p_approved, int p_provider, DateTime p_from_date, DateTime p_to_date)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard3GetStatusParticipantsDetail(p_area, p_approved, p_provider, p_from_date, p_to_date);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard3GetSchedule(int p_area, int p_provider, DateTime p_from_date, DateTime p_to_date)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard3GetSchedule(p_area, p_provider, p_from_date, p_to_date);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard3GetStatusParticipants(int p_area, int p_provider, DateTime p_from_date, DateTime p_to_date)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard3GetStatusParticipants(p_area, p_provider, p_from_date, p_to_date);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard3GetFulfillment(int p_area, int p_provider, DateTime p_from_date, DateTime p_to_date)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard3GetFulfillment(p_area, p_provider, p_from_date, p_to_date);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard2GetCourseCharacteristic(int p_area, int p_course, DateTime p_from_date, DateTime p_to_date)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard2GetCourseCharacteristic(p_area, p_course, p_from_date, p_to_date);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard2GetStatisticsStudent(int p_area)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard2GetStatisticsStudent(p_area);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard2GetAcademicSituation(int p_area)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard2GetAcademicSituation(p_area);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard2GetStatisticsCourse(int p_area)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard2GetStatisticsCourse(p_area);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard2GetCampusInformation(int p_area, DateTime p_from_date, DateTime p_to_date)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard2GetCampusInformation(p_area, p_from_date, p_to_date);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard1GetRegisteredAttempt(int p_id)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard1GetRegisteredAttempt(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard1GetRegisteredDetail(int p_id)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard1GetRegisteredDetail(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard1GetProgrammingRegistered(int p_programming)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard1GetProgrammingRegistered(p_programming);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Dashboard1GetVacanciesVSRegistered(int p_area, int p_year)
        {
            Dao_Report dao;
            try
            {
                dao = new Dao_Report();
                return dao.Dashboard1GetVacanciesVSRegistered(p_area, p_year);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
    }
}