﻿using ElearningApp.Models.Dao;
using ElearningApp.Models.Entity;
using System;
using System.Data;

namespace ElearningApp.Models.Business
{
    public class Bus_List
    {
        public bool GetAccessToPresentationStatus(int p_provider, int p_person, int p_programming)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetAccessToPresentationStatus(p_provider, p_person, p_programming);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveDocument(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveDocument(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetDocumentList()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetDocumentList();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable IncotermsDropDown()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.IncotermsDropDown();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveProviderEmails(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveProviderEmails(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable ProgrammingAreaDropDownAll(int p_AreaId, int p_Year)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.ProgrammingAreaDropDownAll(p_AreaId, p_Year);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetCertificateDescriptionValidate(Certificate_Entity p_Entity)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetCertificateDescriptionValidate(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetPersonList()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetPersonList();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetMyNotices2Home(Home_Model p_Model)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetMyNotices2Home(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveNotice(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveNotice(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetNoticeList(int p_area)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetNoticeList(p_area);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public string GetCourseNameByProgramming(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetCourseNameByProgramming(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public bool GetStatusProgramming(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetStatusProgramming(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public int GetCurrentAttemptToInteractive(int p_register, int p_content)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetCurrentAttemptToInteractive(p_register, p_content);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public bool GetMaximumAttemptsToInteractiveValidate(int p_register, int p_content)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetMaximumAttemptsToInteractiveValidate(p_register, p_content);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable CertificateDropDown()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.CertificateDropDown();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveCertificate(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveCertificate(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetCertificateList()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetCertificateList();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetPollQuestion2Programming(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetPollQuestion2Programming(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public string GetValidationHtmlProviderVehicleDocument(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetValidationHtmlProviderVehicleDocument(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public string GetValidationHtmlProviderEmployeeDocument(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetValidationHtmlProviderEmployeeDocument(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public string GetValidationHtmlProviderDocument(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetValidationHtmlProviderDocument(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProviderEmployeeTrainedLogistica(int p_provider)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProviderEmployeeTrainedLogistica(p_provider);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProviderDocumentLogistica(string p_type)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProviderDocumentLogistica(p_type);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProviderEmployeeDocument()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProviderEmployeeDocument();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProviderVehicleDocument()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProviderVehicleDocument();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProviderDocumentByProvider(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProviderDocumentByProvider(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetEmailInformation()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetEmailInformation();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveProviderAccounts(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveProviderAccounts(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveProviderVehicles(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveProviderVehicles(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveProviderEmployees(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveProviderEmployees(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable PersonaNuevaList(decimal p_nro_doc)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.PersonaNuevaList(p_nro_doc);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable PersonaDeletedList(Provider_Entity p_Entity)
        {
            Dao_List dao;
            DataRow l_row;

            try
            {
                dao = new Dao_List();
                p_Entity.DtDelCodiPcon = new DataTable();
                p_Entity.DtDelCodiPcon.Columns.Add("pn_codi_pcon");
                for (int i = 0; i < p_Entity.pa_del_codi_pcon.Length; i++)
                {
                    l_row = p_Entity.DtDelCodiPcon.NewRow();
                    l_row["pn_codi_pcon"] = p_Entity.pa_del_codi_pcon[i];
                    p_Entity.DtDelCodiPcon.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.PersonaDeletedList(p_Entity);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveProviderPhones(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveProviderPhones(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProviderVehicleMassiveValidation(Provider_Model p_Model)
        {
            Dao_List dao;
            DataRow l_row;
            try
            {
                dao = new Dao_List();
                //build Vehicle
                p_Model.DtVehicle = new DataTable();
                p_Model.DtVehicle.Columns.Add("codi_pcon");
                for (var i = 0; i < p_Model.VehicleId.Length; i++)
                {
                    l_row = p_Model.DtVehicle.NewRow();
                    l_row["codi_pcon"] = p_Model.VehicleId[i];
                    p_Model.DtVehicle.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.GetProviderVehicleMassiveValidation(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public string GetPlateDocumentCode(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetPlateDocumentCode(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProviderEmployeeMassiveValidation(Provider_Model p_Model)
        {
            Dao_List dao;
            DataRow l_row;
            try
            {
                dao = new Dao_List();
                //build employee
                p_Model.DtEmployee = new DataTable();
                p_Model.DtEmployee.Columns.Add("codi_pcon");
                for (var i = 0; i < p_Model.EmployeeId.Length; i++)
                {
                    l_row = p_Model.DtEmployee.NewRow();
                    l_row["codi_pcon"] = p_Model.EmployeeId[i];
                    p_Model.DtEmployee.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.GetProviderEmployeeMassiveValidation(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public string GetIdentityDocumentCode(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetIdentityDocumentCode(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProviderVehicleDocumentToApproveByProfile(int p_provider, int p_vehicle, int p_profile)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProviderVehicleDocumentToApproveByProfile(p_provider, p_vehicle, p_profile);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProviderVehicleDocumentToApproveByProfile(int p_profile)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProviderVehicleDocumentToApproveByProfile(p_profile);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveProvider(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveProvider(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProviderEmployeeDocumentToApproveByProfile(int p_provider, int p_employee, int p_profile)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProviderEmployeeDocumentToApproveByProfile(p_provider, p_employee, p_profile);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProviderEmployeeDocumentToApproveByProfile(int p_profile)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProviderEmployeeDocumentToApproveByProfile(p_profile);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public string GetSustenanceDocumentCode(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetSustenanceDocumentCode(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProviderIdByRUC(Provider_Model p_Model)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProviderIdByRUC(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProviderPersonDataByDocument(Provider_Model p_Model)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProviderPersonDataByDocument(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProviderDocumentByProfile(Provider_Model p_Model)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProviderDocumentByProfile(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable MoneyTypeDropDown()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.MoneyTypeDropDown();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable BankDropDown()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.BankDropDown();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable AccountTypeDropDown()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.AccountTypeDropDown();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable VehicleTypeDropDown()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.VehicleTypeDropDown();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProviderVehicle(Provider_Model p_Model)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProviderVehicle(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public string GetProviderRUC(int p_provider)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProviderRUC(p_provider);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProviderEmployee(Provider_Model p_Model)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProviderEmployee(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProviderList(Provider_Model p_Model)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProviderList(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetDownloadCertificateValidate(int p_register)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetDownloadCertificateValidate(p_register);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public bool GetRegisterWithPoll(int p_register)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetRegisterWithPoll(p_register);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetClassroomPollQuestion(string p_source, int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetClassroomPollQuestion(p_source, p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetClassroomPoll(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetClassroomPoll(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrievePollQuestion(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrievePollQuestion(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrievePoll(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrievePoll(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable PollDropDown()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.PollDropDown();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable QuestionType2TestDropDown()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.QuestionType2TestDropDown();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public string GetPersonEmail(int p_person, string p_type)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetPersonEmail(p_person, p_type);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public string GetProviderEmail(int p_provider)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProviderEmail(p_provider);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveProgrammingPrivatePerson(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveProgrammingPrivatePerson(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable CourseAreaDropDownAll(int p_area)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.CourseAreaDropDownAll(p_area);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable ProviderAreaDropDownAll(int p_area)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.ProviderAreaDropDownAll(p_area);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetCourseNameValidate(Course_Entity p_Entity)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetCourseNameValidate(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveInteractiveContent(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveInteractiveContent(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetInteractiveContentNameValidate(InteractiveContent_Entity p_Entity)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetInteractiveContentNameValidate(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public bool GetInInteractiveContent(int p_register, int p_content)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetInInteractiveContent(p_register, p_content);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetClassroomCertificate(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetClassroomCertificate(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetInteractiveContentList()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetInteractiveContentList();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InteractiveContentDropDown()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.InteractiveContentDropDown();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetNotClassroomInformation(int p_programming, int p_person, int p_errorcode)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetNotClassroomInformation(p_programming, p_person, p_errorcode);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveCourseFile(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveCourseFile(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveParameter()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveParameter();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetItem2Profiles(Profiles_Model p_Model)
        {
            Dao_List dao;
            DataRow l_row;
            string[] l_ids;
            try
            {
                dao = new Dao_List();
                //build dtItemIds
                l_ids = p_Model.ItemIds.Split(',');
                p_Model.DtItemIds = new DataTable();
                p_Model.DtItemIds.Columns.Add("Codigo");
                for (var i = 0; i < l_ids.Length; i++)
                {
                    l_row = p_Model.DtItemIds.NewRow();
                    l_row["Codigo"] = Convert.ToDecimal(l_ids[i]);
                    p_Model.DtItemIds.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.GetItem2Profiles(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveProfiles(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveProfiles(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable ELearningProfileDropDown()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.ELearningProfileDropDown();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetCarrierPrograms2Home()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetCarrierPrograms2Home();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveCertificate()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveCertificate();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveInstructor(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveInstructor(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetPerson2Instructor(int p_document, string p_number)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetPerson2Instructor(p_document, p_number);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetInstructorList()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetInstructorList();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrievePlace(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrievePlace(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetPlaceList()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetPlaceList();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveCategory(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveCategory(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetAttemptFinalizedQuestionAnswer(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetAttemptFinalizedQuestionAnswer(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetAttemptFinalizedQuestion(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetAttemptFinalizedQuestion(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetAttemptFinalized(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetAttemptFinalized(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetAttemptQuestionAnswer(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetAttemptQuestionAnswer(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetAttemptQuestion(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetAttemptQuestion(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetNewAttemptValidate(int p_register)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetNewAttemptValidate(p_register);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public bool GetInTest(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetInTest(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetCategoryList()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetCategoryList();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetClassroom(int p_programming, int p_person, string p_ipaddress)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetClassroom(p_programming, p_person, p_ipaddress);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProvider2AreaProvider(AreaProvider_Model p_Model)
        {
            Dao_List dao;
            DataRow l_row;
            string[] l_ids;
            try
            {
                dao = new Dao_List();
                //build dtProviderIds
                l_ids = p_Model.ProviderIds.Split(',');
                p_Model.DtProviderIds = new DataTable();
                p_Model.DtProviderIds.Columns.Add("Codi_Prov");
                for (var i = 0; i < l_ids.Length; i++)
                {
                    l_row = p_Model.DtProviderIds.NewRow();
                    l_row["Codi_Prov"] = Convert.ToDecimal(l_ids[i]);
                    p_Model.DtProviderIds.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.GetProvider2AreaProvider(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveAreaProvider(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveAreaProvider(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable AreaDropDown()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.AreaDropDown();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveRegisterMassive(int p_provider, int p_programming)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveRegisterMassive(p_provider, p_programming);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetRegisteredEmployeeInEvent(int p_programming, int p_item, int p_provider)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetRegisteredEmployeeInEvent(p_programming, p_item, p_provider);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetEmployeeRegisterMassive(int p_provider)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetEmployeeRegisterMassive(p_provider);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public bool GetRegisterStatus(int p_person, int p_programming)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetRegisterStatus(p_person, p_programming);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetClassroomTestQuestionAnswer(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetClassroomTestQuestionAnswer(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetClassroomTestQuestion(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetClassroomTestQuestion(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetClassroomTest(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetClassroomTest(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetClassroomCourseThemeContent(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetClassroomCourseThemeContent(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetClassroomCourseTheme(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetClassroomCourseTheme(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetClassroomCourse(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetClassroomCourse(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable ListProgrammingEvent(int p_programming, int p_person, int p_provider)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.ListProgrammingEvent(p_programming, p_person, p_provider);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetPresentationCourseProgramming(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetPresentationCourseProgramming(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetPresentationCourseThemeContent(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetPresentationCourseThemeContent(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetPresentationCourseTheme(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetPresentationCourseTheme(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetPresentationCourse(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetPresentationCourse(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveProgrammingEvent(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveProgrammingEvent(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveProgrammingPerson(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveProgrammingPerson(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveProgrammingArea(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveProgrammingArea(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveProgramming(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveProgramming(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProgrammingList(int p_area)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProgrammingList(p_area);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetMyCourses2Home(Home_Model p_Model)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetMyCourses2Home(p_Model);
            }
            catch(Exception ex)
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetRegister(Register_Model p_Model)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetRegister(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetEmailFormatById(decimal p_FormatId, string p_connection = "ELearning")
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetEmailFormatById(p_FormatId, p_connection);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetQuestion2Programming(Programming_Model p_Model)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetQuestion2Programming(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetProvider2Programming(Programming_Model p_Model)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProvider2Programming(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetArea2Programming(Programming_Model p_Model)
        {
            Dao_List dao;
            DataRow l_row;
            string[] l_ids;
            try
            {
                dao = new Dao_List();
                //build dtAreaIds
                l_ids = p_Model.AreaIds.Split(',');
                p_Model.DtAreaIds = new DataTable();
                p_Model.DtAreaIds.Columns.Add("codi_area");
                for (var i = 0; i < l_ids.Length; i++)
                {
                    l_row = p_Model.DtAreaIds.NewRow();
                    l_row["codi_area"] = Convert.ToDecimal(l_ids[i]);
                    p_Model.DtAreaIds.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.GetArea2Programming(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetInstructor2Programming(Programming_Model p_Model)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetInstructor2Programming(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveTestQuestion(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveTestQuestion(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetListQuestion2Test(Test_Model p_Model)
        {
            Dao_List dao;
            DataRow l_row;
            string[] l_ids;
            try
            {
                dao = new Dao_List();
                //build dtQuestionIds
                l_ids = p_Model.QuestionIds.Split(',');
                p_Model.DtQuestionIds = new DataTable();
                p_Model.DtQuestionIds.Columns.Add("codi_preg");
                for (var i = 0; i < l_ids.Length; i++)
                {
                    l_row = p_Model.DtQuestionIds.NewRow();
                    l_row["codi_preg"] = Convert.ToDecimal(l_ids[i]);
                    p_Model.DtQuestionIds.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.GetListQuestion2Test(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetTestList(int p_area)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetTestList(p_area);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveTest(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveTest(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable QuestionTypeDropDown()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.QuestionTypeDropDown();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetQuestionList(int p_area)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetQuestionList(p_area);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveQuestionAnswer(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveQuestionAnswer(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveQuestion(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveQuestion(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetPerson2Programming(Programming_Model p_Model)
        {
            Dao_List dao;
            DataRow l_row;
            string[] l_ids;
            try
            {
                dao = new Dao_List();
                //build dtPersonIds
                l_ids = p_Model.PersonIds.Split(',');
                p_Model.DtPersonIds = new DataTable();
                p_Model.DtPersonIds.Columns.Add("codi_pers");
                for (var i = 0; i < l_ids.Length; i++)
                {
                    l_row = p_Model.DtPersonIds.NewRow();
                    l_row["codi_pers"] = Convert.ToDecimal(l_ids[i]);
                    p_Model.DtPersonIds.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.GetPerson2Programming(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable TestCourseDropDown(int p_CourseId)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.TestCourseDropDown(p_CourseId);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable ModeDropDown()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.ModeDropDown();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable PlaceDropDown()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.PlaceDropDown();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable CourseAreaDropDown(int p_AreaId)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.CourseAreaDropDown(p_AreaId);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable CourseDropDown()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.CourseDropDown();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveCourse(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveCourse(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveCourseTheme(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveCourseTheme(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable RetrieveCourseThemeItem(int p_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveCourseThemeItem(p_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetCourseList(Course_Model p_Model, int p_area)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetCourseList(p_Model, p_area);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable CategoryDropDown()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.CategoryDropDown();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable DocumentTypeDropDown()
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.DocumentTypeDropDown();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }

        public DataTable GetProgInscrito(int codi_pins)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProgInscrito(codi_pins);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetCodigoTema(int codi_pcur)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetCodigoTema(codi_pcur);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }

        public DataTable GetValidarEmail(ValEmail_Model p_Model)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetValidarEmail(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetUserInfo(string username)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetUserInfo(username);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }


        public DataTable GetDatosPersona(decimal p_pers_id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetDatosPersona(p_pers_id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetPersonaPrivado(decimal p_codi_exin)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetPersonaPrivado(p_codi_exin);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable GetDeclaracionJurada(decimal codi_pers)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetDeclaracionJurada(codi_pers);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }

        public DataTable GetProgramming(Int64 p_Id)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.GetProgramming(p_Id);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }

        public DataTable RetrieveProgrammingPrivatePersonCheck(int p_id, DataTable p_fichas)
        {
            Dao_List dao;
            try
            {
                dao = new Dao_List();
                return dao.RetrieveProgrammingPrivatePersonCheck(p_id, p_fichas);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
    }
}