﻿using ElearningApp.Models.Dao;
using ElearningApp.Models.Entity;
using System;
using System.Data;

namespace ElearningApp.Models.Business
{
    public class Bus_Crud
    {
        public DataTable MigrateToCapacitacionProgrammingRegister()
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.MigrateToCapacitacionProgrammingRegister();
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable DeleteDocument(Document_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.DeleteDocument(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertUpdateDocument(Document_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.InsertUpdateDocument(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable DeleteNotice(Notice_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.DeleteNotice(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertUpdateNotice(Notice_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.InsertUpdateNotice(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable UpdateStatusProgramming(Programming_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.UpdateStatusProgramming(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable DeleteCertificate(Certificate_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.DeleteCertificate(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertUpdateCertificate(Certificate_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.InsertUpdateCertificate(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertUpdateProviderVehicleMassive(ProviderVehicleMassive_Entity p_Entity)
        {
            Dao_Crud dao;
            DataRow l_row;
            try
            {
                dao = new Dao_Crud();
                //build Vehicle
                p_Entity.DtVehicle = new DataTable();
                p_Entity.DtVehicle.Columns.Add("codi_pcon");
                p_Entity.DtVehicle.Columns.Add("nom_arc");
                p_Entity.DtVehicle.Columns.Add("nom_real");
                for (var i = 0; i < p_Entity.VehicleId.Length; i++)
                {
                    l_row = p_Entity.DtVehicle.NewRow();
                    l_row["codi_pcon"] = p_Entity.VehicleId[i];
                    l_row["nom_arc"] = p_Entity.VehicleDocumentFileName[i];
                    l_row["nom_real"] = p_Entity.VehicleDocumentFileRealName[i];
                    p_Entity.DtVehicle.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.InsertUpdateProviderVehicleMassive(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertUpdateProviderVehicle(ProviderVehicle_Entity p_Entity)
        {
            Dao_Crud dao;
            DataRow l_row;
            try
            {
                dao = new Dao_Crud();
                //build Vehicle
                p_Entity.DtVehicle = new DataTable();
                p_Entity.DtVehicle.Columns.Add("recordid");
                p_Entity.DtVehicle.Columns.Add("nom_arc");
                for (var i = 0; i < p_Entity.VehiclePrun.Length; i++)
                {
                    l_row = p_Entity.DtVehicle.NewRow();
                    l_row["recordid"] = p_Entity.VehiclePrun[i];
                    l_row["nom_arc"] = p_Entity.VehicleFile[i].File != null ? p_Entity.VehicleFile[i].Name : "";
                    p_Entity.DtVehicle.Rows.Add(l_row);
                    l_row = null;
                }
                //build document
                p_Entity.DtDocument = new DataTable();
                p_Entity.DtDocument.Columns.Add("pd_id_job");
                p_Entity.DtDocument.Columns.Add("pd_tipe_doc");
                p_Entity.DtDocument.Columns.Add("pd_id_gen");
                p_Entity.DtDocument.Columns.Add("pdt_fecha_inicio");
                p_Entity.DtDocument.Columns.Add("pdt_fecha_fin");
                p_Entity.DtDocument.Columns.Add("nom_arc");
                p_Entity.DtDocument.Columns.Add("nom_real");
                p_Entity.DtDocument.Columns.Add("pd_nro_doc");
                for (var i = 0; i < p_Entity.VehicleDocumentPrun.Length; i++)
                {
                    l_row = p_Entity.DtDocument.NewRow();
                    l_row["pd_id_job"] = p_Entity.VehicleDocumentPrun[i];
                    l_row["pd_tipe_doc"] = p_Entity.VehicleDocumentTdos[i];
                    l_row["pd_id_gen"] = p_Entity.VehicleDocumentPcdo[i];
                    l_row["pdt_fecha_inicio"] = p_Entity.VehicleDocumentIssueDate[i];
                    l_row["pdt_fecha_fin"] = p_Entity.VehicleDocumentExpirationDate[i];
                    l_row["nom_arc"] = p_Entity.VehicleDocumentFile[i].File != null ? p_Entity.VehicleDocumentFile[i].Name : p_Entity.VehicleDocumentOldFileName[i];
                    l_row["nom_real"] = p_Entity.VehicleDocumentFile[i].File != null ? p_Entity.VehicleDocumentFile[i].RealName : p_Entity.VehicleDocumentOldFileRealName[i];
                    l_row["pd_nro_doc"] = p_Entity.VehicleDocumentNumber[i];
                    p_Entity.DtDocument.Rows.Add(l_row);
                    l_row = null;
                }
                //build delete
                p_Entity.DtDelete = new DataTable();
                p_Entity.DtDelete.Columns.Add("codi_dele");
                for (var i = 0; i < p_Entity.DeleteDocumentPcdo.Length; i++)
                {
                    l_row = p_Entity.DtDelete.NewRow();
                    l_row["codi_dele"] = p_Entity.DeleteDocumentPcdo[i];
                    p_Entity.DtDelete.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.InsertUpdateProviderVehicle(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertUpdateProviderEmployeeMassive(ProviderEmployeeMassive_Entity p_Entity)
        {
            Dao_Crud dao;
            DataRow l_row;
            try
            {
                dao = new Dao_Crud();
                //build employee
                p_Entity.DtEmployee = new DataTable();
                p_Entity.DtEmployee.Columns.Add("codi_pcon");
                p_Entity.DtEmployee.Columns.Add("nom_arc");
                p_Entity.DtEmployee.Columns.Add("nom_real");
                for (var i = 0; i < p_Entity.EmployeeId.Length; i++)
                {
                    l_row = p_Entity.DtEmployee.NewRow();
                    l_row["codi_pcon"] = p_Entity.EmployeeId[i];
                    l_row["nom_arc"] = p_Entity.EmployeeDocumentFileName[i];
                    l_row["nom_real"] = p_Entity.EmployeeDocumentFileRealName[i];
                    p_Entity.DtEmployee.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.InsertUpdateProviderEmployeeMassive(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertUpdateProviderEmployee(ProviderEmployee_Entity p_Entity)
        {
            Dao_Crud dao;
            DataRow l_row;
            try
            {
                dao = new Dao_Crud();
                //build employee
                p_Entity.DtEmployee = new DataTable();
                p_Entity.DtEmployee.Columns.Add("recordid");
                p_Entity.DtEmployee.Columns.Add("nom_arc");
                for (var i = 0; i < p_Entity.EmployeePcon.Length; i++)
                {
                    l_row = p_Entity.DtEmployee.NewRow();
                    l_row["recordid"] = p_Entity.EmployeePcon[i];
                    l_row["nom_arc"] = p_Entity.EmployeeFile[i].File != null ? p_Entity.EmployeeFile[i].Name : "";
                    p_Entity.DtEmployee.Rows.Add(l_row);
                    l_row = null;
                }
                //build document
                p_Entity.DtDocument = new DataTable();
                p_Entity.DtDocument.Columns.Add("pd_id_job");
                p_Entity.DtDocument.Columns.Add("pd_tipe_doc");
                p_Entity.DtDocument.Columns.Add("pd_id_gen");
                p_Entity.DtDocument.Columns.Add("pdt_fecha_inicio");
                p_Entity.DtDocument.Columns.Add("pdt_fecha_fin");
                p_Entity.DtDocument.Columns.Add("nom_arc");
                p_Entity.DtDocument.Columns.Add("nom_real");
                p_Entity.DtDocument.Columns.Add("pd_nro_doc");
                for (var i = 0; i < p_Entity.EmployeeDocumentPcon.Length; i++)
                {
                    l_row = p_Entity.DtDocument.NewRow();
                    l_row["pd_id_job"] = p_Entity.EmployeeDocumentPcon[i];
                    l_row["pd_tipe_doc"] = p_Entity.EmployeeDocumentTdos[i];
                    l_row["pd_id_gen"] = p_Entity.EmployeeDocumentPcdo[i];
                    l_row["pdt_fecha_inicio"] = p_Entity.EmployeeDocumentIssueDate[i];
                    l_row["pdt_fecha_fin"] = p_Entity.EmployeeDocumentExpirationDate[i];
                    l_row["nom_arc"] = p_Entity.EmployeeDocumentFile[i].File != null ? p_Entity.EmployeeDocumentFile[i].Name : p_Entity.EmployeeDocumentOldFileName[i];
                    l_row["nom_real"] = p_Entity.EmployeeDocumentFile[i].File != null ? p_Entity.EmployeeDocumentFile[i].RealName : p_Entity.EmployeeDocumentOldFileRealName[i];
                    l_row["pd_nro_doc"] = p_Entity.EmployeeDocumentNumber[i];
                    p_Entity.DtDocument.Rows.Add(l_row);
                    l_row = null;
                }
                //build delete
                p_Entity.DtDelete = new DataTable();
                p_Entity.DtDelete.Columns.Add("codi_dele");
                for (var i = 0; i < p_Entity.DeleteDocumentPcdo.Length; i++)
                {
                    l_row = p_Entity.DtDelete.NewRow();
                    l_row["codi_dele"] = p_Entity.DeleteDocumentPcdo[i];
                    p_Entity.DtDelete.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.InsertUpdateProviderEmployee(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable ToApproveDocumentProvider(ProviderToApprove_Entity p_Entity)
        {
            Dao_Crud dao;
            DataRow l_row;
            try
            {
                dao = new Dao_Crud();
                //build provider
                p_Entity.DtProvider = new DataTable();
                p_Entity.DtProvider.Columns.Add("codi_prdo");
                p_Entity.DtProvider.Columns.Add("estado_doc");
                p_Entity.DtProvider.Columns.Add("observacion");
                for (var i = 0; i < p_Entity.ProviderPrdo.Length; i++)
                {
                    l_row = p_Entity.DtProvider.NewRow();
                    l_row["codi_prdo"] = p_Entity.ProviderPrdo[i];
                    l_row["estado_doc"] = p_Entity.ProviderSwitch[i];
                    l_row["observacion"] = p_Entity.ProviderReason[i];
                    p_Entity.DtProvider.Rows.Add(l_row);
                    l_row = null;
                }
                //build employee
                p_Entity.DtEmployee = new DataTable();
                p_Entity.DtEmployee.Columns.Add("codi_pcdo");
                p_Entity.DtEmployee.Columns.Add("estado_doc");
                p_Entity.DtEmployee.Columns.Add("observacion");
                for (var i = 0; i < p_Entity.EmployeePcdo.Length; i++)
                {
                    l_row = p_Entity.DtEmployee.NewRow();
                    l_row["codi_pcdo"] = p_Entity.EmployeePcdo[i];
                    l_row["estado_doc"] = p_Entity.EmployeeSwitch[i];
                    l_row["observacion"] = p_Entity.EmployeeReason[i];
                    p_Entity.DtEmployee.Rows.Add(l_row);
                    l_row = null;
                }
                //build vehicle
                p_Entity.DtVehicle = new DataTable();
                p_Entity.DtVehicle.Columns.Add("codi_prud");
                p_Entity.DtVehicle.Columns.Add("estado_doc");
                p_Entity.DtVehicle.Columns.Add("observacion");
                for (var i = 0; i < p_Entity.VehiclePcdo.Length; i++)
                {
                    l_row = p_Entity.DtVehicle.NewRow();
                    l_row["codi_prud"] = p_Entity.VehiclePcdo[i];
                    l_row["estado_doc"] = p_Entity.VehicleSwitch[i];
                    l_row["observacion"] = p_Entity.VehicleReason[i];
                    p_Entity.DtVehicle.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.ToApproveDocumentProvider(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertUpdateProvider(Provider_Entity p_Entity)
        {
            Dao_Crud dao;
            DataRow l_row;
            try
            {
                dao = new Dao_Crud();
                //build emails
                p_Entity.DtEmail = new DataTable();
                p_Entity.DtEmail.Columns.Add("Codi_Pema");
                p_Entity.DtEmail.Columns.Add("Email");
                for (var i = 0; i < p_Entity.EmailRecordId.Length; i++)
                {
                    l_row = p_Entity.DtEmail.NewRow();
                    l_row["Codi_Pema"] = p_Entity.EmailRecordId[i];
                    l_row["Email"] = p_Entity.EmailAddress[i];
                    p_Entity.DtEmail.Rows.Add(l_row);
                    l_row = null;
                }
                //build phones
                p_Entity.DtPhone = new DataTable();
                p_Entity.DtPhone.Columns.Add("codi_ptel");
                p_Entity.DtPhone.Columns.Add("telefono");
                for (var i = 0; i < p_Entity.PhoneRecordId.Length; i++)
                {
                    l_row = p_Entity.DtPhone.NewRow();
                    l_row["codi_ptel"] = p_Entity.PhoneRecordId[i];
                    l_row["telefono"] = p_Entity.PhoneNumber[i];
                    p_Entity.DtPhone.Rows.Add(l_row);
                    l_row = null;
                }
                //build person
                p_Entity.DtPerson = new DataTable();
                p_Entity.DtPerson.Columns.Add("codi_pcon");
                p_Entity.DtPerson.Columns.Add("codi_pers");
                p_Entity.DtPerson.Columns.Add("ap_paterno");
                p_Entity.DtPerson.Columns.Add("ap_materno");
                p_Entity.DtPerson.Columns.Add("nombres");
                p_Entity.DtPerson.Columns.Add("fecha_nac");
                p_Entity.DtPerson.Columns.Add("codi_tdoc");
                p_Entity.DtPerson.Columns.Add("nro_doc");
                p_Entity.DtPerson.Columns.Add("email");
                p_Entity.DtPerson.Columns.Add("nro_licencia");
                for (var i = 0; i < p_Entity.PersonRecordId.Length; i++)
                {
                    l_row = p_Entity.DtPerson.NewRow();
                    l_row["codi_pcon"] = p_Entity.PersonRecordId[i];
                    l_row["codi_pers"] = p_Entity.PersonPersonId[i];
                    l_row["ap_paterno"] = p_Entity.PersonLastname1[i];
                    l_row["ap_materno"] = p_Entity.PersonLastname2[i];
                    l_row["nombres"] = p_Entity.PersonFirstname[i];
                    l_row["fecha_nac"] = p_Entity.PersonBirthday[i];
                    l_row["codi_tdoc"] = p_Entity.PersonDocumentType[i];
                    l_row["nro_doc"] = p_Entity.PersonDocumentNumber[i];
                    l_row["email"] = p_Entity.PersonEmail[i];
                    l_row["nro_licencia"] = p_Entity.PersonLicenseNumber[i];
                    p_Entity.DtPerson.Rows.Add(l_row);
                    l_row = null;
                }
                //build person phones
                p_Entity.DtPersonPhone = new DataTable();
                p_Entity.DtPersonPhone.Columns.Add("codi_ctel");
                p_Entity.DtPersonPhone.Columns.Add("codi_pcon");
                p_Entity.DtPersonPhone.Columns.Add("telefono");
                for (var i = 0; i < p_Entity.PersonPhoneRecordId.Length; i++)
                {
                    l_row = p_Entity.DtPersonPhone.NewRow();
                    l_row["codi_ctel"] = p_Entity.PersonPhoneRecordId[i];
                    l_row["codi_pcon"] = p_Entity.PersonPhonePersonRecordId[i];
                    l_row["telefono"] = p_Entity.PersonPhoneNumber[i];
                    p_Entity.DtPersonPhone.Rows.Add(l_row);
                    l_row = null;
                }
                //build vehicle
                p_Entity.DtVehicle = new DataTable();
                p_Entity.DtVehicle.Columns.Add("codi_prun");
                p_Entity.DtVehicle.Columns.Add("codi_untr");
                p_Entity.DtVehicle.Columns.Add("nro_placa");
                for (var i = 0; i < p_Entity.VehicleRecordId.Length; i++)
                {
                    l_row = p_Entity.DtVehicle.NewRow();
                    l_row["codi_prun"] = p_Entity.VehicleRecordId[i];
                    l_row["codi_untr"] = p_Entity.VehicleTypeId[i];
                    l_row["nro_placa"] = p_Entity.VehiclePlate[i];
                    p_Entity.DtVehicle.Rows.Add(l_row);
                    l_row = null;
                }
                //build document
                p_Entity.DtDocument = new DataTable();
                p_Entity.DtDocument.Columns.Add("codi_prdo");
                p_Entity.DtDocument.Columns.Add("codi_tdos");
                p_Entity.DtDocument.Columns.Add("nombre_img");
                p_Entity.DtDocument.Columns.Add("properties_image");
                p_Entity.DtDocument.Columns.Add("fecha_inicio");
                p_Entity.DtDocument.Columns.Add("fecha_fin");
                p_Entity.DtDocument.Columns.Add("nombre_real");
                for (var i = 0; i < p_Entity.DocumentRecordId.Length; i++)
                {
                    l_row = p_Entity.DtDocument.NewRow();
                    l_row["codi_prdo"] = p_Entity.DocumentRecordId[i];
                    l_row["codi_tdos"] = p_Entity.DocumentTypeId[i];
                    l_row["nombre_img"] = p_Entity.DocumentFile[i].Name;
                    l_row["properties_image"] = p_Entity.DocumentFile[i].Properties;
                    l_row["fecha_inicio"] = p_Entity.DocumentIssueDate[i];
                    l_row["fecha_fin"] = p_Entity.DocumentExpirationDate[i];
                    l_row["nombre_real"] = p_Entity.DocumentFile[i].RealName;
                    p_Entity.DtDocument.Rows.Add(l_row);
                    l_row = null;
                }
                //build account
                p_Entity.DtAccount = new DataTable();
                p_Entity.DtAccount.Columns.Add("id");
                p_Entity.DtAccount.Columns.Add("item");
                p_Entity.DtAccount.Columns.Add("nro_cuenta");
                p_Entity.DtAccount.Columns.Add("codi_tcue");
                p_Entity.DtAccount.Columns.Add("codi_banc");
                p_Entity.DtAccount.Columns.Add("codi_tmon");
                p_Entity.DtAccount.Columns.Add("nombre");
                p_Entity.DtAccount.Columns.Add("nombre_real");
                for (var i = 0; i < p_Entity.AccountRecordId.Length; i++)
                {
                    l_row = p_Entity.DtAccount.NewRow();
                    l_row["id"] = i + 1;
                    l_row["item"] = p_Entity.AccountRecordId[i];
                    l_row["nro_cuenta"] = p_Entity.AccountNumber[i];
                    l_row["codi_tcue"] = p_Entity.AccountTypeId[i];
                    l_row["codi_banc"] = p_Entity.AccountBank[i];
                    l_row["codi_tmon"] = p_Entity.AccountMoney[i];
                    l_row["nombre"] = p_Entity.AccountFile[i].Name;
                    l_row["nombre_real"] = p_Entity.AccountFile[i].RealName;
                    p_Entity.DtAccount.Rows.Add(l_row);
                    l_row = null;
                }
                //build observation
                p_Entity.DtObservation = new DataTable();
                p_Entity.DtObservation.Columns.Add("id");
                p_Entity.DtObservation.Columns.Add("item");
                p_Entity.DtObservation.Columns.Add("observacion");
                p_Entity.DtObservation.Columns.Add("respuesta");
                p_Entity.DtObservation.Columns.Add("flag_correcto");
                //for {
                l_row = p_Entity.DtObservation.NewRow();
                l_row["id"] = "0";
                l_row["item"] = "0";
                l_row["observacion"] = "";
                l_row["respuesta"] = "";
                l_row["flag_correcto"] = "";
                p_Entity.DtObservation.Rows.Add(l_row);
                l_row = null;
                //}
                return dao.InsertUpdateProvider(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertLogSGS(ResponseSGS json_data_sgs, decimal codi_pers)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.InsertLogSGS(json_data_sgs, codi_pers);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertRegisterPoll(RegisterPoll_Entity p_Entity)
        {
            Dao_Crud dao;
            DataRow l_row;
            try
            {
                dao = new Dao_Crud();
                //build dtQuestion
                p_Entity.DtQuestion = new DataTable();
                p_Entity.DtQuestion.Columns.Add("Id");
                p_Entity.DtQuestion.Columns.Add("Pregunta");
                p_Entity.DtQuestion.Columns.Add("Orden");
                p_Entity.DtQuestion.Columns.Add("Evaluacion");
                for (var i = 0; i < p_Entity.QtnId.Length; i++)
                {
                    l_row = p_Entity.DtQuestion.NewRow();
                    l_row["Id"] = p_Entity.QtnId[i];
                    l_row["Pregunta"] = p_Entity.QtnQuestion[i];
                    l_row["Orden"] = p_Entity.QtnOrder[i];
                    l_row["Evaluacion"] = p_Entity.QtnScore[i];
                    p_Entity.DtQuestion.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.InsertRegisterPoll(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable SaveClassroomLog(int p_register, int p_item, int p_session)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.SaveClassroomLog(p_register, p_item, p_session);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable DeleteInteractiveContent(InteractiveContent_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.DeleteInteractiveContent(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertUpdateInteractiveContent(InteractiveContent_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.InsertUpdateInteractiveContent(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable UpdateProfilesProvider(Profiles_Entity p_Entity)
        {
            Dao_Crud dao;
            DataRow l_row;
            try
            {
                dao = new Dao_Crud();
                //build dtProfiles
                p_Entity.DtProfile = new DataTable();
                p_Entity.DtProfile.Columns.Add("Codi_Sipeus");
                p_Entity.DtProfile.Columns.Add("Codi_User");
                p_Entity.DtProfile.Columns.Add("Codi_Prov");
                p_Entity.DtProfile.Columns.Add("Email_Prov");
                for (var i = 0; i < p_Entity.PrfRecordId.Length; i++)
                {
                    l_row = p_Entity.DtProfile.NewRow();
                    l_row["Codi_Sipeus"] = p_Entity.PrfRecordId[i];
                    l_row["Codi_User"] = p_Entity.PrfUsername[i];
                    l_row["Codi_Prov"] = p_Entity.ProviderId[i];
                    l_row["Email_Prov"] = p_Entity.ProviderEmail[i];
                    p_Entity.DtProfile.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.UpdateProfilesProvider(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable UpdatePassword(ChangePassword_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.UpdatePassword(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable UpdateParameter(Parameter_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.UpdateParameter(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable UpdateProfiles(Profiles_Entity p_Entity)
        {
            Dao_Crud dao;
            DataRow l_row;
            try
            {
                dao = new Dao_Crud();
                //build dtProfiles
                p_Entity.DtProfile = new DataTable();
                p_Entity.DtProfile.Columns.Add("Codi_Sipeus");
                p_Entity.DtProfile.Columns.Add("Codi_User");
                for (var i = 0; i < p_Entity.PrfRecordId.Length; i++)
                {
                    l_row = p_Entity.DtProfile.NewRow();
                    l_row["Codi_Sipeus"] = p_Entity.PrfRecordId[i];
                    l_row["Codi_User"] = p_Entity.PrfUsername[i];
                    p_Entity.DtProfile.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.UpdateProfiles(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable DeleteInstructor(Instructor_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.DeleteInstructor(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertUpdateInstructor(Instructor_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.InsertUpdateInstructor(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable DeletePlace(Place_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.DeletePlace(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertUpdatePlace(Place_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.InsertUpdatePlace(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable DeleteCategory(Category_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.DeleteCategory(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable UpdateAttemptCurrentTime(Attempt_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.UpdateAttemptCurrentTime(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable UpdateAttempt(Attempt_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.UpdateAttempt(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable UpdateAttemptQuestion(Attempt_Entity p_Entity)
        {
            Dao_Crud dao;
            DataRow l_row;
            try
            {
                dao = new Dao_Crud();
                //build dtAnswer
                p_Entity.DtAnswer = new DataTable();
                p_Entity.DtAnswer.Columns.Add("Codi_Prep");
                p_Entity.DtAnswer.Columns.Add("Orden_Seleccionado");
                for (var i = 0; i < p_Entity.AnswerId.Length; i++)
                {
                    l_row = p_Entity.DtAnswer.NewRow();
                    l_row["Codi_Prep"] = p_Entity.AnswerId[i];
                    l_row["Orden_Seleccionado"] = p_Entity.AnswerOrder[i];
                    p_Entity.DtAnswer.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.UpdateAttemptQuestion(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertUpdateCategory(Category_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.InsertUpdateCategory(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertVisitContentAdvance(int p_register, int p_content)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.InsertVisitContentAdvance(p_register, p_content);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertContentAdvance(int p_register, int l_course)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.InsertContentAdvance(p_register, l_course);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertUpdateAreaProvider(AreaProvider_Entity p_Entity)
        {
            Dao_Crud dao;
            DataRow l_row;
            try
            {
                dao = new Dao_Crud();
                //build dtProvider
                p_Entity.DtProvider = new DataTable();
                p_Entity.DtProvider.Columns.Add("Codi_Proa");
                p_Entity.DtProvider.Columns.Add("Codi_Prov");
                for (var i = 0; i < p_Entity.PrvRecordId.Length; i++)
                {
                    l_row = p_Entity.DtProvider.NewRow();
                    l_row["Codi_Proa"] = p_Entity.PrvRecordId[i];
                    l_row["Codi_Prov"] = p_Entity.PrvProviderId[i];
                    p_Entity.DtProvider.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.InsertUpdateAreaProvider(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable DeleteProgrammingRegister(ProgrammingRegister_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.DeleteProgrammingRegister(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable DeleteProgramming(Programming_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.DeleteProgramming(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable DeleteTest(Test_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.DeleteTest(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable DeleteQuestion(Question_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.DeleteQuestion(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable DeleteCourse(Course_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.DeleteCourse(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable DeleteProgrammingRegisterMassive(ProgrammingRegisterMassive_Entity p_Entity)
        {
            Dao_Crud dao;
            DataRow l_row;
            try
            {
                dao = new Dao_Crud();
                //build dtEvent
                p_Entity.DtEvent = new DataTable();
                p_Entity.DtEvent.Columns.Add("Codi_Pins");
                p_Entity.DtEvent.Columns.Add("Codi_Pcur");
                p_Entity.DtEvent.Columns.Add("Item");
                for (var i = 0; i < p_Entity.EvtRecordIdA.Length; i++)
                {
                    l_row = p_Entity.DtEvent.NewRow();
                    l_row["Codi_Pins"] = p_Entity.EvtRecordIdA[i];
                    l_row["Codi_Pcur"] = p_Entity.EvtRecordIdB[i];
                    l_row["Item"] = p_Entity.EvtRecordIdC[i];
                    p_Entity.DtEvent.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.DeleteProgrammingRegisterMassive(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertProgrammingRegisterMassive(ProgrammingRegisterMassive_Entity p_Entity)
        {
            Dao_Crud dao;
            DataRow l_row;
            try
            {
                dao = new Dao_Crud();
                //build dtRegister
                p_Entity.DtRegister = new DataTable();
                p_Entity.DtRegister.Columns.Add("Codi_Pins");
                p_Entity.DtRegister.Columns.Add("Codi_Pers");
                for (var i = 0; i < p_Entity.RgRecordId.Length; i++)
                {
                    l_row = p_Entity.DtRegister.NewRow();
                    l_row["Codi_Pins"] = p_Entity.RgRecordId[i];
                    l_row["Codi_Pers"] = p_Entity.RgPersonId[i];
                    p_Entity.DtRegister.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.InsertProgrammingRegisterMassive(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertProgrammingRegister(ProgrammingRegister_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.InsertProgrammingRegister(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable SetVerifyYourEmail(Register_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.SetVerifyYourEmail(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertUpdateProgramming(Programming_Entity p_Entity)
        {
            Dao_Crud dao;
            DataRow l_row;
            try
            {
                dao = new Dao_Crud();
                //build dtCalendar
                p_Entity.DtCalendar = new DataTable();
                p_Entity.DtCalendar.Columns.Add("item");
                p_Entity.DtCalendar.Columns.Add("fecha", typeof(DateTime));
                p_Entity.DtCalendar.Columns.Add("hora_inicio");
                p_Entity.DtCalendar.Columns.Add("hora_fin");
                for (var i = 0; i < p_Entity.CalRecordId.Length; i++)
                {
                    l_row = p_Entity.DtCalendar.NewRow();
                    l_row["item"] = p_Entity.CalRecordId[i];
                    l_row["fecha"] = p_Entity.CalDate[i];
                    l_row["hora_inicio"] = p_Entity.CalFromHour[i];
                    l_row["hora_fin"] = p_Entity.CalToHour[i];
                    p_Entity.DtCalendar.Rows.Add(l_row);
                    l_row = null;
                }
                //build dtPerson
                p_Entity.DtPerson = new DataTable();
                p_Entity.DtPerson.Columns.Add("codi_pins");
                p_Entity.DtPerson.Columns.Add("codi_pers");
                for (var i = 0; i < p_Entity.PerRecordId.Length; i++)
                {
                    l_row = p_Entity.DtPerson.NewRow();
                    l_row["codi_pins"] = p_Entity.PerRecordId[i];
                    l_row["codi_pers"] = p_Entity.PerPersonId[i];
                    p_Entity.DtPerson.Rows.Add(l_row);
                    l_row = null;
                }
                //build dtArea
                p_Entity.DtArea = new DataTable();
                p_Entity.DtArea.Columns.Add("codi_prar");
                p_Entity.DtArea.Columns.Add("codi_area");
                for (var i = 0; i < p_Entity.AreaRecordId.Length; i++)
                {
                    l_row = p_Entity.DtArea.NewRow();
                    l_row["codi_prar"] = p_Entity.AreaRecordId[i];
                    l_row["codi_area"] = p_Entity.AreaAreaId[i];
                    p_Entity.DtArea.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.InsertUpdateProgramming(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertUpdateTest(Test_Entity p_Entity)
        {
            Dao_Crud dao;
            DataRow l_row;
            try
            {
                dao = new Dao_Crud();
                //build dtQuestion
                p_Entity.DtQuestion = new DataTable();
                p_Entity.DtQuestion.Columns.Add("codi_expr");
                p_Entity.DtQuestion.Columns.Add("codi_preg");
                p_Entity.DtQuestion.Columns.Add("orden");
                for (var i = 0; i < p_Entity.QRecord.Length; i++)
                {
                    l_row = p_Entity.DtQuestion.NewRow();
                    l_row["codi_expr"] = p_Entity.QRecord[i];
                    l_row["codi_preg"] = p_Entity.QQuestionId[i];
                    l_row["orden"] = p_Entity.QOrder[i];
                    p_Entity.DtQuestion.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.InsertUpdateTest(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertUpdateQuestion(Question_Entity p_Entity)
        {
            Dao_Crud dao;
            DataRow l_row;
            try
            {
                dao = new Dao_Crud();
                //build DtAnswer
                p_Entity.DtAnswer = new DataTable();
                p_Entity.DtAnswer.Columns.Add("Codi_Pres");
                p_Entity.DtAnswer.Columns.Add("Respuesta");
                p_Entity.DtAnswer.Columns.Add("Correcta");
                p_Entity.DtAnswer.Columns.Add("Orden");
                for (var i = 0; i < p_Entity.AnswerId.Length; i++)
                {
                    l_row = p_Entity.DtAnswer.NewRow();
                    l_row["Codi_Pres"] = p_Entity.AnswerId[i];
                    l_row["Respuesta"] = p_Entity.AnswerText[i];
                    l_row["Correcta"] = p_Entity.AnswerCorrect[i];
                    l_row["Orden"] = p_Entity.AnswerOrder[i];
                    p_Entity.DtAnswer.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.InsertUpdateQuestion(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertUpdateCourse(Course_Entity p_Entity)
        {
            Dao_Crud dao;
            DataRow l_row;
            try
            {
                dao = new Dao_Crud();
                //build DtDetail
                p_Entity.DtDetail = new DataTable();
                p_Entity.DtDetail.Columns.Add("codi_tecu");
                p_Entity.DtDetail.Columns.Add("descripcion_tema");
                p_Entity.DtDetail.Columns.Add("secuencia_tema");
                p_Entity.DtDetail.Columns.Add("codi_cote");
                p_Entity.DtDetail.Columns.Add("codi_tcon");
                p_Entity.DtDetail.Columns.Add("codi_cuin");
                p_Entity.DtDetail.Columns.Add("titulo_contenido");
                p_Entity.DtDetail.Columns.Add("texto_contenido");
                p_Entity.DtDetail.Columns.Add("url_youtube_contenido");
                p_Entity.DtDetail.Columns.Add("url_canal_sider_contenido");
                p_Entity.DtDetail.Columns.Add("nombre_doc_contenido");
                p_Entity.DtDetail.Columns.Add("propiedad_doc_contenido");
                p_Entity.DtDetail.Columns.Add("secuencia_contenido");
                for (var i = 0; i < p_Entity.ThemeId.Length; i++)
                {
                    l_row = p_Entity.DtDetail.NewRow();
                    l_row["codi_tecu"] = p_Entity.ThemeId[i];
                    l_row["descripcion_tema"] = p_Entity.ThemeName[i];
                    l_row["secuencia_tema"] = p_Entity.ThemeOrder[i];
                    l_row["codi_cote"] = p_Entity.ThemeItemId[i];
                    l_row["codi_tcon"] = p_Entity.ThemeItemTypeId[i];
                    l_row["codi_cuin"] = p_Entity.ThemeItemInteractive[i];
                    l_row["titulo_contenido"] = p_Entity.ThemeItemTitle[i];
                    l_row["texto_contenido"] = p_Entity.ThemeItemText[i];
                    l_row["url_youtube_contenido"] = p_Entity.ThemeItemUrlYoutube[i];
                    l_row["url_canal_sider_contenido"] = p_Entity.ThemeItemUrlSiderChannel[i];
                    l_row["nombre_doc_contenido"] = p_Entity.ThemeItemFiles[i] == null ? "" : p_Entity.ThemeItemFiles[i].Name;
                    l_row["propiedad_doc_contenido"] = p_Entity.ThemeItemFiles[i] == null ? "" : p_Entity.ThemeItemFiles[i].Properties;
                    l_row["secuencia_contenido"] = p_Entity.ThemeItemOrder[i];
                    p_Entity.DtDetail.Rows.Add(l_row);
                    l_row = null;
                }
                return dao.InsertUpdateCourse(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable InsertRegister(Register_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.InsertRegister(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable DeleteInscribed(decimal p_CodiPins)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.DeleteInscribed(p_CodiPins);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }

        public DataTable UpdateProvider(UpdProvider_Model p_Model)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.UpdateProvider(p_Model);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
        public DataTable Update_Userid_Persona(UpdPersona p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.Update_Userid_Persona(p_Entity);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }

        public DataTable SavePerson(Register_Entity p_Entity)
        {
            Dao_Crud dao;
            try
            {
                dao = new Dao_Crud();
                return dao.SavePerson(p_Entity);
            }
            catch
            {
                throw;
            }
            finally
            {
                dao = null;
            }
        }
    }
}