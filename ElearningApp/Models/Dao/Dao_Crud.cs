﻿using ElearningApp.Models.Entity;
using System;
using System.Data;
using System.Data.SqlClient;

namespace ElearningApp.Models.Dao
{
    public class Dao_Crud
    {
        public DataTable MigrateToCapacitacionProgrammingRegister()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Migrar_A_Capacitacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable DeleteDocument(Document_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Del_Documento";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_tdos", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertUpdateDocument(Document_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Upd_Documento";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_tdos", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_descripcion", p_Entity.Description));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_descripcion_corta", p_Entity.ShortDescription));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo", p_Entity.Type));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_modelo", p_Entity.Model.Name));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_orden", p_Entity.Order));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_numeracion", p_Entity.HasNumber));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_fecha", p_Entity.HasDates));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_masivo", p_Entity.HasMassive));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_logistica", p_Entity.HasLogistica));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_estado", p_Entity.Status));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable DeleteNotice(Notice_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Del_Anuncio";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_anun", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertUpdateNotice(Notice_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Upd_Anuncio";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_anun", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_Entity.AreaId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_descripcion", p_Entity.Description));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nombre_html", p_Entity.Html.Name));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_url", p_Entity.Url));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_estado", p_Entity.Status));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable CloseClassroomLog(int p_session)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Close_Log_Aula";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_sesi", p_session));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable UpdateStatusProgramming(Programming_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Upd_Estado_Programacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_estado", p_Entity.Status));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable DeleteCertificate(Certificate_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Del_Constancia";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_cons", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertUpdateCertificate(Certificate_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Upd_Constancia";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_cons", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_descripcion", p_Entity.Description));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_configuracion", p_Entity.CertificateJSON));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_estado", p_Entity.Status));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertUpdateProviderVehicleMassive(ProviderVehicleMassive_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_PROV_Document_Insert_Masivo_Vehiculo";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pb_codi_tdoc", p_Entity.DocumentId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pb_nro_doc", p_Entity.DocumentNumber));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_fech_emi", p_Entity.IssueDate));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_fech_ven", p_Entity.ExpirationDate));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_user_name", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@TABLE_TRAB",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtVehicle
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertUpdateProviderVehicle(ProviderVehicle_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_PROV_Ins_Upd_Documento_Vehiculos";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_user_name", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@TABLE_TRAB",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtDocument
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@TABLE_PERFIL",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtVehicle
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@TABLE_DELETE",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtDelete
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertUpdateProviderEmployeeMassive(ProviderEmployeeMassive_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_PROV_Document_Insert_Masivo";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pb_codi_tdoc", p_Entity.DocumentId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pb_nro_doc", p_Entity.DocumentNumber));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_fech_emi", p_Entity.IssueDate));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_fech_ven", p_Entity.ExpirationDate));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_user_name", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@TABLE_TRAB",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtEmployee
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertUpdateProviderEmployee(ProviderEmployee_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_PROV_Ins_Upd_Documento";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_user_name", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@TABLE_TRAB",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtDocument
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@TABLE_PERFIL",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtEmployee
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@TABLE_DELETE",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtDelete
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable ToApproveDocumentProvider(ProviderToApprove_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Visi_Upd_Aprobar_documento";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_documento_empresa",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtProvider
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_documento_trabajador",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtEmployee
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_documento_unidad",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtVehicle
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertUpdateProvider(Provider_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Upd_Proveedor";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codigo_sap", p_Entity.SAPCode));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nombre", p_Entity.Name));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_razon_social", p_Entity.BusinessName));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ruc", p_Entity.RUC));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_direccion_fiscal", p_Entity.Address));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pda_fecha_ingreso", p_Entity.AdmissionDate));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_aprobado", p_Entity.Approved));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_estado", p_Entity.Status));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_estado_frm_ini", p_Entity.ProcessInitial));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_estado_frm", p_Entity.Process));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_retencion", p_Entity.Retention));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_retencion", p_Entity.HasRetention));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_user", p_Entity.RUC));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_email_user", p_Entity.Email));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_term", p_Entity.IncotermsId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_sist", p_Entity.AppId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo", p_Entity.Actions));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_email",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtEmail
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_telefono_cab",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtPhone
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_cuenta",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtAccount
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_Trabajador",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtPerson
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_Unidad_Transporte",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtVehicle
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_Documento_Sustenta",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtDocument
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_telefono_cont",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtPersonPhone
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_observacion",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtObservation
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@tbl_trabajador_covid",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtDnisCovid
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertLogSGS(ResponseSGS json_data_sgs, decimal codi_pers)
        {
            SqlConnection l_cn = null;
            SqlDataAdapter l_da = null;
            DataTable l_dt = null;
            try
            {
                l_cn = ConnectionHelpers.ConnectToSql("Administracion");
                if (l_cn != null)
                {
                    l_cn.Open();
                    l_da = new SqlDataAdapter();
                    l_dt = new DataTable();
                    l_da.SelectCommand = l_cn.CreateCommand();
                    l_da.SelectCommand.CommandText = "dbo.Sp_Prov_Ins_Log_SGS_CUARENTENA";
                    l_da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    l_da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pers", codi_pers));
                    l_da.SelectCommand.Parameters.Add(new SqlParameter("@pv_data", json_data_sgs.data));
                    l_da.SelectCommand.Parameters.Add(new SqlParameter("@pn_error_code", json_data_sgs.errorCode));
                    l_da.SelectCommand.Parameters.Add(new SqlParameter("@pv_message", json_data_sgs.message));

                    l_da.Fill(l_dt);
                }
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                if (l_cn != null)
                {
                    l_cn.Close();
                    l_cn.Dispose();
                }
            }
            return l_dt;
        }
        public DataTable InsertRegisterPoll(RegisterPoll_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Inscrito_Encuesta";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pins", p_Entity.RegisterId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_comentario", p_Entity.HasComment));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_pregunta_comentario", p_Entity.CommentQuestion));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_comentario", p_Entity.Comment));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_preguntas",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtQuestion
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable SaveClassroomLog(int p_register, int p_item, int p_session)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Save_Log_Aula";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pins", p_register));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_item", p_item));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_sesi", p_session));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable DeleteInteractiveContent(InteractiveContent_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Del_Curso_Interactivo";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_cuin", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertUpdateInteractiveContent(InteractiveContent_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Upd_Curso_Interactivo";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_cuin", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nombre_curso", p_Entity.Name));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_url_curso", p_Entity.Url));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nombre_directorio", p_Entity.Directory));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_estado", p_Entity.Status));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable UpdateProfilesProvider(Profiles_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Seguridad"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Upd_Perfiles_Proveedor";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_sipe", p_Entity.AppProfileId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_perfiles",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtProfile
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable UpdatePassword(ChangePassword_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Seguridad"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Upd_Password";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_old_password", p_Entity.OldPassword));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_new_password", p_Entity.NewPassword));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_user", p_Entity.UserId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable UpdateParameter(Parameter_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Upd_Parametros_Logistica";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_curs_transportista", p_Entity.CarrierCourseId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tiempo_cerrar_sesion", p_Entity.WaitTimeCloseSession));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_mensaje_fin_intento", p_Entity.MessageFinalized));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_mensaje_aprobado_fin", p_Entity.MessageApproved));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_mensaje_desaprobado_fin", p_Entity.MessageDisapproved));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable UpdateProfiles(Profiles_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Seguridad"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Upd_Perfiles";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_sipe", p_Entity.AppProfileId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_nvo_proveedor", p_Entity.FromProvider));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_perfiles",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtProfile
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable DeleteInstructor(Instructor_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Del_Instructor";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_inst", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertUpdateInstructor(Instructor_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Upd_Instructor";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_inst", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pers", p_Entity.PersonId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_tdoc", p_Entity.DocumentTypeId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nro_doc", p_Entity.DocumentNumber));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ap_paterno", p_Entity.LastName1));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ap_materno", p_Entity.LastName2));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nombres", p_Entity.FirstName));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_email", p_Entity.Email));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo", p_Entity.Type));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_estado", p_Entity.Status));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_guardar_persona", p_Entity.SavePerson));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable DeletePlace(Place_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Del_Lugar";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_luga", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertUpdatePlace(Place_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Upd_Lugar";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_luga", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_descripcion", p_Entity.Description));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_estado", p_Entity.Status));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable DeleteCategory(Category_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Del_Categoria";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_cate", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable UpdateAttemptCurrentTime(Attempt_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Upd_Tiempo_Actual_Intento";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_exin", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_segundos", p_Entity.Seconds));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable UpdateAttempt(Attempt_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Upd_Intento_Examen";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_exin", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_segundos", p_Entity.Seconds));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tiempo", p_Entity.Time));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable UpdateAttemptQuestion(Attempt_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Upd_Programa_Pregunta";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prex", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_segundos", p_Entity.Seconds));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tiempo", p_Entity.Time));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_contestado", p_Entity.Answered));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_respuestas",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtAnswer
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertUpdateCategory(Category_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Upd_Categoria";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_cate", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_descripcion", p_Entity.Description));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_estado", p_Entity.Status));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertVisitContentAdvance(int p_register, int p_content)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Visita_Avance_Contenido";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pins", p_register));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_cote", p_content));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertContentAdvance(int p_register, int l_course)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Avance_Contenido";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pins", p_register));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_curs", l_course));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertUpdateAreaProvider(AreaProvider_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Upd_Proveedor_Area";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_Entity.AreaId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_nvo_proveedor", p_Entity.FromProvider));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_proveedor",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtProvider
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable DeleteProgrammingRegister(ProgrammingRegister_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Del_Inscripcion_Individual";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_Entity.ProgrammingId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_item", p_Entity.Item));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pers", p_Entity.PersonId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable DeleteProgramming(Programming_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Del_Programacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable DeleteTest(Test_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Del_Examen";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_exam", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable DeleteQuestion(Question_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Del_Pregunta";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_preg", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable DeleteCourse(Course_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Del_Curso";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_curs", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable DeleteProgrammingRegisterMassive(ProgrammingRegisterMassive_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Del_Inscripcion_Masiva";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_Entity.ProviderId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_Entity.ProgrammingId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_eventos",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtEvent
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertProgrammingRegisterMassive(ProgrammingRegisterMassive_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Inscripcion_Masiva";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_Entity.ProviderId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_Entity.ProgrammingId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_item", p_Entity.Item));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_personas",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtRegister
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertProgrammingRegister(ProgrammingRegister_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Inscripcion_Individual";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pers", p_Entity.PersonId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_Entity.ProgrammingId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_item", p_Entity.Item));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable SetVerifyYourEmail(Register_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Seguridad"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Validar_Insertar_Usuario";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_id", p_Entity.RegisteredId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_accion", p_Entity.Actions));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_origen", p_Entity.Origin));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertUpdateProgramming(Programming_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Upd_Programacion_CUARENTENA";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_nombre", p_Entity.Name));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_curs", p_Entity.CourseId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_moda", p_Entity.ModeId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_nro_vacante", p_Entity.Vacancies));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_examen", p_Entity.HasTest));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_examen", p_Entity.TestId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_nro_intentos", p_Entity.Attempts));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_inst", p_Entity.HasInstructor));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_inst", p_Entity.InstructorId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_encuesta", p_Entity.HasPoll));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_encu", p_Entity.PollId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_mostrar_contenido", p_Entity.ShowContent));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_luga", p_Entity.PlaceId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_lugar_otros", p_Entity.PlaceDescription));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pda_fecha_limite_ins", p_Entity.RegistrationLimit));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_tipo_aprobacion", p_Entity.ApprovalType));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_nro_asistencia", p_Entity.MinimumAssistance));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_programacion", p_Entity.ProgrammingType));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pda_fecha_inicio", p_Entity.FromDate));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pda_fecha_fin", p_Entity.ToDate));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_hora_inicio", p_Entity.FromHour));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_hora_fin", p_Entity.ToHour));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_duracion", p_Entity.Duration));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_escala_duracion", p_Entity.DurationScale));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_intervalo", p_Entity.Interval));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_escala_intervalo", p_Entity.IntervalScale));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_fds", p_Entity.Weekend));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo_acceso", p_Entity.AccessType));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo_persona", p_Entity.PersonType));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_todo_area", p_Entity.AllArea));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ilimitado_inscripcion", p_Entity.UnlimitedRegistration));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ilimitado_programacion", p_Entity.UnlimitedProgramming));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_certificado", p_Entity.HasCertificate));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_cons", p_Entity.CertificateId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_capa_horas", p_Entity.CapacitacionHours));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_capa_hora_inicio", p_Entity.CapacitacionFromHour));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_capa_hora_fin", p_Entity.CapacitacionToHour));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_perf", p_Entity.ProfilId));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_detalle_calendario",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtCalendar
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_trabajador_inscrito",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtPerson
                    });
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_programa_area",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtArea
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertUpdateTest(Test_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Upd_Examen";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_examen", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_curs", p_Entity.CourseId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_descripcion", p_Entity.Name));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_nota_min", p_Entity.MinScore));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_nota_max", p_Entity.MaxScore));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_nota_apro", p_Entity.ApproveScore));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo_examen", p_Entity.TestType));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_procede", p_Entity.Proceed));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tiempo", p_Entity.Time));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_cant_tiempo", p_Entity.TimeLimit));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_aleatorio", p_Entity.RandomQuestions));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_estado", p_Entity.Status));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_cant_pregunta", p_Entity.QuestionsNumber));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_pregunta",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtQuestion
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertUpdateQuestion(Question_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Upd_Pregunta";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_preg", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_curs", p_Entity.CourseId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_tpre", p_Entity.TypeId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_pregunta", p_Entity.Question));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_estado", p_Entity.Status));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_respuestas",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtAnswer
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertUpdateCourse(Course_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Upd_Curso";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_curso", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_cate", p_Entity.CategoryId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_Entity.AreaId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nombre", p_Entity.Name));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_objetivo", p_Entity.Objetives));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_resumen", p_Entity.Summary));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_descripcion_portada", p_Entity.FrontPage));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_url_img", p_Entity.ImageFile.Name));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_clasificacion", p_Entity.Classification));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_estado", p_Entity.Status));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_vigencia", p_Entity.HasValidity));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_cant_vigencia", p_Entity.Validity));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_escala_vigencia", p_Entity.ValidityScale));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_usuario", p_Entity.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IPAddress));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_temas",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtDetail
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InsertRegister(Register_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Ins_Registro";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ap_paterno", p_Entity.LastName1));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ap_materno", p_Entity.LastName2));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nombres", p_Entity.FirstName));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_tdoc", p_Entity.DocumentType));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nro_documento", p_Entity.DocumentNumber));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_email", p_Entity.Email));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_celular", p_Entity.Phone));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_accion", p_Entity.Actions));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip_address", p_Entity.IpAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public DataTable DeleteInscribed(decimal p_CodiPins)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Del_Eliminar_Inscrito";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pb_codi_pins", p_CodiPins));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public DataTable UpdateProvider(UpdProvider_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Seguridad"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Upd_Prov_Flag_Logistica";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pv_table_prov",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Model.DtProveedores
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public DataTable Update_Userid_Persona(UpdPersona p_Entity)
        {
            SqlConnection l_cn = null;
            SqlDataAdapter l_da = null;
            DataTable l_dt = null;
            try
            {
                l_cn = ConnectionHelpers.ConnectToSql("Administracion");
                if (l_cn != null)
                {
                    l_cn.Open();
                    l_da = new SqlDataAdapter();
                    l_dt = new DataTable();
                    l_da.SelectCommand = l_cn.CreateCommand();
                    l_da.SelectCommand.CommandText = "dbo.Sp_Visi_Upd_Userid_persona";
                    l_da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    l_da.SelectCommand.Parameters.Add(new SqlParameter("@pb_codiProv", p_Entity.CodiProv));
                    l_da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nro_doc", p_Entity.NroDoc));
                    l_da.SelectCommand.Parameters.Add(new SqlParameter("@pb_userid", p_Entity.UserId));
                    l_da.Fill(l_dt);
                }
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                if (l_cn != null)
                {
                    l_cn.Close();
                    l_cn.Dispose();
                }
            }
            return l_dt;
        }

        /* Codigo de Aplicacion - No manipular - */
        #region App
        public void SaveLog(Log_Entity p_Entity)
        {
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Seguridad"))
                using (SqlCommand cmd = new SqlCommand
                {
                    CommandText = "dbo.SP_SEGU_Guardar_Sesion_Ope",
                    CommandType = CommandType.StoredProcedure,
                    Connection = conn
                })
                {
                    conn.Open();
                    cmd.Parameters.Add(new SqlParameter("@CodiSesi", p_Entity.SessionId));
                    cmd.Parameters.Add(new SqlParameter("@ControllerName", p_Entity.ControllerName));
                    cmd.Parameters.Add(new SqlParameter("@FunctionName", p_Entity.ActionName));
                    cmd.ExecuteNonQuery();
                }
            }
            catch
            {
            }
        }
        public decimal StartSession(Session_Entity p_Entity)
        {
            decimal l_return = 0;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Seguridad"))
                using (SqlCommand cmd = new SqlCommand
                {
                    CommandText = "dbo.SP_SEGU_Nueva_Sesion",
                    CommandType = CommandType.StoredProcedure,
                    Connection = conn
                })
                {
                    conn.Open();
                    cmd.Parameters.Add(new SqlParameter("@CodeApp", p_Entity.AppId));
                    cmd.Parameters.Add(new SqlParameter("@CodeUser", p_Entity.Username));
                    cmd.Parameters.Add(new SqlParameter("@CodeProf", p_Entity.ProfileId));
                    cmd.Parameters.Add(new SqlParameter("@NamePc", p_Entity.Pcname));
                    cmd.Parameters.Add(new SqlParameter("@NumberIp", p_Entity.IPAddress));
                    cmd.Parameters.Add(new SqlParameter("@Informacion", p_Entity.Information));
                    cmd.Parameters.Add("@SesionCode", SqlDbType.Decimal).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_return = Convert.ToDecimal(cmd.Parameters["@SesionCode"].Value);
                }
            }
            catch {
            }
            return l_return;
        }
        public void EndSession(Session_Entity p_Entity)
        {
            try
            {
                try
                {
                    CloseClassroomLog(Convert.ToInt32(p_Entity.SessionId));
                }
                catch
                {
                    //prevent
                }
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Seguridad"))
                using (SqlCommand cmd = new SqlCommand
                {
                    CommandText = "dbo.SP_SEGU_Cerrar_Sesion",
                    CommandType = CommandType.StoredProcedure,
                    Connection = conn
                })
                {
                    conn.Open();
                    cmd.Parameters.Add(new SqlParameter("@CodiSesi", p_Entity.SessionId));
                    cmd.ExecuteNonQuery();
                }
            }
            catch
            {
            }
        }

        public DataTable SavePerson(Register_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Save_Person";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_paterno", p_Entity.LastName1));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_materno", p_Entity.LastName2));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nombres", p_Entity.FirstName));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_email", p_Entity.Email));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pers", p_Entity.RegisteredId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_celular", p_Entity.Phone));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_Entity.IpAddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        #endregion App
    }
}