﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ElearningApp.Models.Dao
{
    public class Dao_Report
    {
        public DataTable Dashboard5GetCertificate(int p_register)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard5_Certificado";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pins", p_register));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard5GetExecuted(int p_course)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard5_Curso_Ejecutado";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_curs", p_course));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard5GetRegistered(int p_programming)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard5_Inscritos";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_programming));
                    da.Fill(l_dt);
                }
            }
            catch(Exception ex)
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard3GetParticipantAnswer(int p_question)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard3_Participantes_Det_Respuestas";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prex", p_question));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard3GetParticipantQuestion(int p_attempt)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard3_Participantes_Det_Preguntas";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_exin", p_attempt));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard3GetParticipantAttempt(int p_register)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard3_Participantes_Det_Intentos";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pins", p_register));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard4GetProviderData(int p_provider)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_Visi_Sel_Rpt_Indicador_All";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_provider));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard4GetProvider(int p_area)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard4_Proveedor";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard2GetSatisfaction(int p_area)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard2_Satisfaccion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard3GetStatusParticipantsDetail(int p_area, string p_approved, int p_provider, DateTime p_from_date, DateTime p_to_date)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard3_Aprobados_Desaprobados_Detalle";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_provider));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pd_fecha_ini", p_from_date));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pd_fecha_fin", p_to_date));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_aprobado", p_approved));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard3GetSchedule(int p_area, int p_provider, DateTime p_from_date, DateTime p_to_date)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard3_Capacitaciones_Turno";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_provider));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pd_fecha_ini", p_from_date));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pd_fecha_fin", p_to_date));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard3GetStatusParticipants(int p_area, int p_provider, DateTime p_from_date, DateTime p_to_date)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard3_Aprobados_vs_Desaprobados";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_provider));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pd_fecha_ini", p_from_date));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pd_fecha_fin", p_to_date));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard3GetFulfillment(int p_area, int p_provider, DateTime p_from_date, DateTime p_to_date)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard3_Programados_vs_Asistencias";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_provider));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pd_fecha_ini", p_from_date));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pd_fecha_fin", p_to_date));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard2GetCourseCharacteristic(int p_area, int p_course, DateTime p_from_date, DateTime p_to_date)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard2_Caracteristica_Curso";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_curs", p_course));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pd_fecha_ini", p_from_date));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pd_fecha_fin", p_to_date));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard2GetStatisticsStudent(int p_area)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard2_Estadistica_Alumno";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard2GetAcademicSituation(int p_area)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard2_Situacion_Academica";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard2GetStatisticsCourse(int p_area)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard2_Estadistica_Curso";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard2GetCampusInformation(int p_area, DateTime p_from_date, DateTime p_to_date)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard2_Informacion_Campus";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pd_fecha_inicio", p_from_date));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pd_fecha_fin", p_to_date));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard1GetRegisteredAttempt(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard1_Inscrito_Detalle_Intento";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pins", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard1GetRegisteredDetail(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard1_Inscrito_Detalle";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pins", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard1GetProgrammingRegistered(int p_programming)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard1_Programa_Inscritos";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_programming));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable Dashboard1GetVacanciesVSRegistered(int p_area, int p_year)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Dashboard1_Vacantes_vs_Inscritos";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_year", p_year));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
    }
}