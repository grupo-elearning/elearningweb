﻿using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ElearningApp.Models.Dao
{
    public class Dao_List
    {
        public bool GetAccessToPresentationStatus(int p_provider, int p_person, int p_programming)
        {
            int l_value = 0;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlCommand cmd = new SqlCommand("dbo.Sp_Elea_Obt_Acceso_Presentacion", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_prov", p_provider));
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_pers", p_person));
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_programming));
                    cmd.Parameters.Add("@pn_status", SqlDbType.TinyInt).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToInt32(cmd.Parameters["@pn_status"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value == 1;
        }
        public DataTable RetrieveDocument(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Documento";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_tdos", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetDocumentList()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_List_Documento";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public int GetIncotermsByProvider(int p_id)
        {
            int l_value = 0;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlCommand cmd = new SqlCommand("dbo.Sp_Elea_Obt_Incoterms_Proveedor", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_prov", p_id));
                    cmd.Parameters.Add("@pn_codi_term", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToInt32(cmd.Parameters["@pn_codi_term"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value;
        }
        public DataTable IncotermsDropDown()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_DropDown_Incoterms";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveProviderEmails(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Prov_Sel_Ret_Proveedor_Email";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable ProgrammingAreaDropDownAll(int p_area, int p_year)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_DropDown_All_Programa_Curso_Area1";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_anio", p_year));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetCertificateDescriptionValidate(Certificate_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Validar_Descripcion_Constancia";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_cons", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_descripcion", p_Entity.Description));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetPersonList()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_List_Persona";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetMyNotices2Home(Home_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_List_Mis_Anuncios";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pers", p_Model.PersonId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_Model.ProviderId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_perf", p_Model.ProfileId));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveNotice(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Anuncio";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_anun", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetNoticeList(int p_area)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_List_Anuncio";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public string GetCourseNameByProgramming(int p_id)
        {
            string l_value = "";
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlCommand cmd = new SqlCommand("dbo.Sp_Elea_Obt_Nombre_Curso_Por_Programa", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_id));
                    cmd.Parameters.Add("@pv_curso_nombre", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToString(cmd.Parameters["@pv_curso_nombre"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value;
        }
        public bool GetStatusProgramming(int p_id)
        {
            int l_value = 1;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlCommand cmd = new SqlCommand("dbo.Sp_Elea_Obt_Activo_Programa_Curso", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_id));
                    cmd.Parameters.Add("@pn_status", SqlDbType.TinyInt).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToInt32(cmd.Parameters["@pn_status"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value == 1;
        }
        public int GetCurrentAttemptToInteractive(int p_register, int p_content)
        {
            int l_value = 0;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlCommand cmd = new SqlCommand("dbo.Sp_Elea_Obt_Intento_Actual_Interactivo", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_pins", p_register));
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_cote", p_content));
                    cmd.Parameters.Add("@pn_result", SqlDbType.BigInt).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToInt32(cmd.Parameters["@pn_result"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value;
        }
        public bool GetMaximumAttemptsToInteractiveValidate(int p_register, int p_content)
        {
            int l_value = 1;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlCommand cmd = new SqlCommand("dbo.Sp_Elea_Validar_Intentos_Max_Interactivo", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_pins", p_register));
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_cote", p_content));
                    cmd.Parameters.Add("@pn_status", SqlDbType.TinyInt).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToInt32(cmd.Parameters["@pn_status"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value == 1;
        }
        public DataTable CertificateDropDown()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_DropDown_Constancia";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveCertificate(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Constancia";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_cons", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetCertificateList()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_List_Constancia";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetPollQuestion2Programming(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_List_Pregunta_Encuesta";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_encu", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public string GetValidationHtmlProviderVehicleDocument(int p_id)
        {
            string l_value = "";
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlCommand cmd = new SqlCommand("dbo.SP_Visi_Obt_Tipo_Vehiculo", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Clave", p_id));
                    cmd.Parameters.Add("@Dato", SqlDbType.NVarChar, int.MaxValue).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToString(cmd.Parameters["@Dato"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value;
        }
        public string GetValidationHtmlProviderEmployeeDocument(int p_id)
        {
            string l_value = "";
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlCommand cmd = new SqlCommand("dbo.SP_Visi_Obt_Tipo_Trabajador", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Clave", p_id));
                    cmd.Parameters.Add("@Dato", SqlDbType.NVarChar, int.MaxValue).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToString(cmd.Parameters["@Dato"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value;
        }
        public string GetValidationHtmlProviderDocument(int p_id)
        {
            string l_value = "";
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlCommand cmd = new SqlCommand("dbo.SP_Visi_Obt_Tipo_Doc", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Clave", p_id));
                    cmd.Parameters.Add("@Dato", SqlDbType.NVarChar, int.MaxValue).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToString(cmd.Parameters["@Dato"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value;
        }
        public DataTable GetProviderEmployeeTrainedLogistica(int p_provider)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Trabajador_Capa_Logi";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pi_codi_prov", p_provider));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetProviderDocumentLogistica(string p_type)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Doc_Logistica";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo", p_type));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetProviderEmployeeDocument()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_PROV_Listar_documentos_Proveedor_Type";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetProviderVehicleDocument()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_PROV_Listar_documentos_Proveedor_Type_Otros";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetProviderDocumentByProvider(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_PROV_Listar_documentos_Proveedor";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_id_proveedor", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetEmailInformation()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_VISI_Correo_Informacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveProviderAccounts(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Prov_Sel_Ret_Proveedor_Cuenta";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveProviderVehicles(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_PROV_Listar_Proveedor_Vehiculo";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveProviderEmployees(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Prov_Sel_Ret_Proveedor_Contacto_CUARENTENA" +
                        "";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable PersonaNuevaList(decimal p_nro_doc)
        {
            SqlConnection l_cn = null;
            SqlDataAdapter l_da = null;
            DataTable l_dt = null;
            try
            {
                l_cn = ConnectionHelpers.ConnectToSql("AZURE");
                if (l_cn != null)
                {
                    l_cn.Open();
                    l_da = new SqlDataAdapter();
                    l_dt = new DataTable();
                    l_da.SelectCommand = l_cn.CreateCommand();
                    l_da.SelectCommand.CommandText = "dbo.Sp_Prov_Sel_Ret_Persona_Temp_CUARENTENA";
                    l_da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    l_da.SelectCommand.Parameters.Add(new SqlParameter("@pn_nro_doc ", p_nro_doc));
                    l_da.Fill(l_dt);
                }
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                if (l_cn != null)
                {
                    l_cn.Close();
                    l_cn.Dispose();
                }
            }
            return l_dt;
        }
        public DataTable PersonaDeletedList(Provider_Entity p_Entity)
        {
            SqlConnection l_cn = null;
            SqlDataAdapter l_da = null;
            DataTable l_dt = null;
            try
            {
                l_cn = ConnectionHelpers.ConnectToSql("Administracion");
                if (l_cn != null)
                {
                    l_cn.Open();
                    l_da = new SqlDataAdapter();
                    l_dt = new DataTable();
                    l_da.SelectCommand = l_cn.CreateCommand();
                    l_da.SelectCommand.CommandText = "dbo.Sp_Prov_Sel_Ret_Persona_Deleted_List_CUARENTENA";
                    l_da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    l_da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@ptb_deleted_pcon",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Entity.DtDelCodiPcon
                    });
                    l_da.Fill(l_dt);
                }
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                if (l_cn != null)
                {
                    l_cn.Close();
                    l_cn.Dispose();
                }
            }
            return l_dt;
        }
        public DataTable RetrieveProviderPhones(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Prov_Sel_Ret_Proveedor_Tel_cab";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetProviderVehicleMassiveValidation(Provider_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_PROV_Listar_Validation_Masivo_Vehiculo";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_tdoc", p_Model.DocumentId));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@ptb_trabajador_documento",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Model.DtVehicle
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public string GetPlateDocumentCode(int p_id)
        {
            string l_value = "";
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlCommand cmd = new SqlCommand("dbo.SP_Visi_Obt_Tipo_Doc_Placa", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Clave", p_id));
                    cmd.Parameters.Add("@Dato", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToString(cmd.Parameters["@Dato"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value;
        }
        public DataTable GetProviderEmployeeMassiveValidation(Provider_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_PROV_Listar_Validation_Masivo";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_tdoc", p_Model.DocumentId));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@ptb_trabajador_documento",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Model.DtEmployee
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public string GetIdentityDocumentCode(int p_id)
        {
            string l_value = "";
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlCommand cmd = new SqlCommand("dbo.SP_Visi_Obt_Tipo_Doc_Identidad", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Clave", p_id));
                    cmd.Parameters.Add("@Dato", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToString(cmd.Parameters["@Dato"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value;
        }
        public DataTable GetProviderVehicleDocumentToApproveByProfile(int p_provider, int p_vehicle, int p_profile)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_PROV_Listar_documentos_Proveedor_Vehiculo";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_id_proveedor", p_provider));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prun", p_vehicle));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_id_perfil", p_profile));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetProviderVehicleDocumentToApproveByProfile(int p_profile)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_Visi_Listar_documentos_Type_Aprobar_Vehiculos";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_perfil", p_profile));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveProvider(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_PROV_Sel_Ret_Proveedor_Cabecera";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetProviderEmployeeDocumentToApproveByProfile(int p_provider, int p_employee, int p_profile)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_PROV_Listar_documentos_Proveedor_trabajador";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_id_proveedor", p_provider));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcon", p_employee));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_id_perfil", p_profile));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetProviderEmployeeDocumentToApproveByProfile(int p_profile)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_Visi_Listar_documentos_Proveedor_Type_Aprobar";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_perfil", p_profile));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public string GetSustenanceDocumentCode(int p_id)
        {
            string l_value = "";
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlCommand cmd = new SqlCommand("dbo.SP_Visi_Obt_Tipo_Doc_Sustentatorio", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Clave", p_id));
                    cmd.Parameters.Add("@Dato", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToString(cmd.Parameters["@Dato"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value;
        }
        public DataTable GetProviderIdByRUC(Provider_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_PROV_Count_Ruc_Existe";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@codi_ruc", p_Model.ProviderRUC));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetProviderPersonDataByDocument(Provider_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_PROV_Listar_datos_personas";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_id_Type", p_Model.DocumentType));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_id_dni", p_Model.DocumentNumber));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_Model.ProviderId));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetProviderDocumentByProfile(Provider_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_PROV_Listar_documentos_Proveedor_Aprobar";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_id_proveedor", p_Model.ProviderId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_id_perfil", p_Model.ProfileId));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable MoneyTypeDropDown()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Prov_DropDown_Moneda";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable BankDropDown()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Prov_DropDown_Banco";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable AccountTypeDropDown()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Prov_DropDown_Tipo_Cuenta";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable VehicleTypeDropDown()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_PROV_Listar_tipo_vehiculos";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetProviderVehicle(Provider_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_PROV_Listar_Proveedor_Trabajador_Vehiculo";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_proveedorid", p_Model.ProviderId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_tipo", p_Model.Type));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_perfil", p_Model.ProfileId));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public string GetProviderRUC(int p_provider)
        {
            string l_value = "";
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlCommand cmd = new SqlCommand("dbo.SP_PROV_obtener_ruc_proveedor", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Clave", p_provider));
                    cmd.Parameters.Add("@Dato", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToString(cmd.Parameters["@Dato"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value;
        }
        public DataTable GetProviderEmployee(Provider_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_PROV_Listar_Proveedor_Trabajador";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_proveedorid", p_Model.ProviderId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_tipo", p_Model.Type));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_perfil", p_Model.ProfileId));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetProviderList(Provider_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Visi_Listar_Proveedor";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo", p_Model.Filter_Type));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codigo_sap", p_Model.Filter_RUC));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_estado", p_Model.Status));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag", p_Model.Mode));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_perfil", p_Model.ProfileId));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetDownloadCertificateValidate(int p_register)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Validacion_Descargar_Certificado";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pins", p_register));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public bool GetRegisterWithPoll(int p_register)
        {
            int l_value = 0;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlCommand cmd = new SqlCommand("dbo.Sp_Elea_Obt_Con_Encuesta", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_pins", p_register));
                    cmd.Parameters.Add("@pn_status", SqlDbType.TinyInt).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToInt32(cmd.Parameters["@pn_status"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value == 1;
        }
        public DataTable GetClassroomPollQuestion(string p_source, int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Encuesta_Pregunta_Para_Aula";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_origen", p_source));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codigo", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetClassroomPoll(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Encuesta_Para_Aula";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pins", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrievePollQuestion(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Encuesta_Pregunta";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_encu", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrievePoll(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Encuesta";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_encu", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable PollDropDown()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_DropDown_Encuesta";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable QuestionType2TestDropDown()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_DropDown_Tipo_Pregunta_Examen";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public string GetPersonEmail(int p_person, string p_type)
        {
            string l_value = "";
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlCommand cmd = new SqlCommand("dbo.Sp_Elea_Obt_Email_Persona", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_pers", p_person));
                    cmd.Parameters.Add(new SqlParameter("@pv_tipo", p_type));
                    cmd.Parameters.Add("@pv_email", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToString(cmd.Parameters["@pv_email"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value;
        }
        public string GetProviderEmail(int p_provider)
        {
            string l_value = "";
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlCommand cmd = new SqlCommand("dbo.Sp_Elea_Obt_Email_Proveedor", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_prov", p_provider));
                    cmd.Parameters.Add("@pv_email", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToString(cmd.Parameters["@pv_email"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value;
        }
        public DataTable RetrieveProgrammingPrivatePerson(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Programacion_Privada_Persona";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable CourseAreaDropDownAll(int p_area)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_DropDown_All_Curso_Area";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable ProviderAreaDropDownAll(int p_area)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_DropDown_All_Proveedor_Area";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetCourseNameValidate(Course_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Validar_Nombre_Curso";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_curs", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nombre", p_Entity.Name));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveInteractiveContent(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Curso_Interactivo";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_cuin", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetInteractiveContentNameValidate(InteractiveContent_Entity p_Entity)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Validar_Nombre_Curso_Interactivo";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_cuin", p_Entity.Id));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nombre", p_Entity.Name));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public bool GetInInteractiveContent(int p_register, int p_content)
        {
            int l_value = 0;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlCommand cmd = new SqlCommand("dbo.Sp_Elea_Obt_En_Curso_Interactivo", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_pins", p_register));
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_cote", p_content));
                    cmd.Parameters.Add("@pn_status", SqlDbType.TinyInt).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToInt32(cmd.Parameters["@pn_status"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value == 1;
        }
        public DataTable GetClassroomCertificate(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Certificado_Para_Aula";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pins", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetInteractiveContentList()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_List_Curso_Interactivo";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable InteractiveContentDropDown()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_DropDown_Curso_Interactivo";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetNotClassroomInformation(int p_programming, int p_person, int p_errorcode)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Info_Para_No_Aula";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_programming));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pers", p_person));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_error_code", p_errorcode));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveCourseFile(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Curso_Archivo";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_curs", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveParameter()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Parametros_Logistica";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetItem2Profiles(Profiles_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_List_Item_Para_Perfiles";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_sipe", p_Model.AppProfileId));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_codi_item",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Model.DtItemIds
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveProfiles(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Perfiles";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_sipe", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable ELearningProfileDropDown()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_DropDown_Perfil_ELearning";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetCarrierPrograms2Home()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_List_Transportista_Programas";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetCarrierProfileList(Account_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Usuario_Perfil_Sistema_Transportista";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_user", p_Model.Username));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetCarrierLoginValidate(Account_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Validar_Acceso_Transportista";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_user", p_Model.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_pass_user", p_Model.Password));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveCertificate()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Certificado";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveInstructor(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Instructor";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_inst", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetPerson2Instructor(int p_document, string p_number)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Persona_Para_Instructor";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_tdoc", p_document));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nro_doc", p_number));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetInstructorList()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_List_Instructor";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrievePlace(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Lugar";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_luga", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetPlaceList()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_List_Lugar";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveCategory(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Categoria";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_cate", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetAttemptFinalizedQuestionAnswer(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Preg_Respuesta_Para_Fin_Intento";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prex", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetAttemptFinalizedQuestion(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Preg_Para_Fin_Intento";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_exin", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetAttemptFinalized(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Info_Para_Fin_Intento";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_exin", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public bool GetAuthorizeAccessToControllerAction(int p_profile, string p_controller, string p_action)
        {
            int l_value = 0;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlCommand cmd = new SqlCommand("dbo.Sp_Elea_Autorizar_Acceso_Vista", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_perf", p_profile));
                    cmd.Parameters.Add(new SqlParameter("@pv_controlador", p_controller));
                    cmd.Parameters.Add(new SqlParameter("@pv_accion", p_action));
                    cmd.Parameters.Add("@pn_status", SqlDbType.TinyInt).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToInt32(cmd.Parameters["@pn_status"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value == 1;
        }
        public DataTable GetAttemptQuestionAnswer(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Programa_Preg_Respuesta_Para_Aula";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prex", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetAttemptQuestion(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Programa_Pregunta_Para_Aula";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prex", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetNewAttemptValidate(int p_register)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Validacion_Nvo_Intento";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pins", p_register));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public bool GetInTest(int p_id)
        {
            int l_value = 0;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlCommand cmd = new SqlCommand("dbo.Sp_Elea_Obt_En_Examen", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_pins", p_id));
                    cmd.Parameters.Add("@pn_status", SqlDbType.TinyInt).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToInt32(cmd.Parameters["@pn_status"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value == 1;
        }
        public DataTable GetCategoryList()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_List_Categoria";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetClassroom(int p_programming, int p_person, string p_ipaddress)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Aula";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_programming));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pers", p_person));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_ip", p_ipaddress));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public bool GetFromLogistica(User_Entity p_Entity)
        {
            int l_value = 0;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlCommand cmd = new SqlCommand("dbo.Sp_Elea_Obt_Es_De_Logistica", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_prov", p_Entity.ProviderId));
                    cmd.Parameters.Add(new SqlParameter("@pn_pers_codi_prov", p_Entity.PersonProviderId));
                    cmd.Parameters.Add("@pn_status", SqlDbType.TinyInt).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToInt32(cmd.Parameters["@pn_status"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value == 1;
        }
        public DataTable GetProvider2AreaProvider(AreaProvider_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_List_Proveedor_Para_AreaProveedor";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_codi_prov",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Model.DtProviderIds
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveAreaProvider(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Area_Proveedor";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable AreaDropDown()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_DropDown_Area";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveRegisterMassive(int p_provider, int p_programming)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Inscripcion_Masiva";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_provider));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_programming));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetRegisteredEmployeeInEvent(int p_programming, int p_item, int p_provider)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Trabajador_Inscrito";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_programming));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_item", p_item));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_provider));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetEmployeeRegisterMassive(int p_provider)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Trabajador_Inscripcion_Masiva";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_provider));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public bool GetRegisterStatus(int p_person, int p_programming)
        {
            int l_value = 0;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlCommand cmd = new SqlCommand("dbo.Sp_Elea_Obt_Inscrito", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_pers", p_person));
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_programming));
                    cmd.Parameters.Add("@pn_status", SqlDbType.TinyInt).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    l_value = Convert.ToInt32(cmd.Parameters["@pn_status"].Value);
                }
            }
            catch
            {
                throw;
            }
            return l_value == 1;
        }
        public DataTable GetClassroomTestQuestionAnswer(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Examen_Preg_Respuesta_Para_Aula";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prex", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetClassroomTestQuestion(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Examen_Pregunta_Para_Aula";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_exin", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetClassroomTest(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Examen_Para_Aula";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pins", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetClassroomCourseThemeContent(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Curso_Tema_Cont_Para_Aula";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_tecu", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetClassroomCourseTheme(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Curso_Tema_Para_Aula";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_curs", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetClassroomCourse(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Curso_Para_Aula";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_curs", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable ListProgrammingEvent(int p_programming, int p_person, int p_provider)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_List_Programacion_Detalle_Para_Inscribir";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_programming));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pers", p_person));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_provider));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetPresentationCourseProgramming(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Curso_Programa_Para_Presentacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetPresentationCourseThemeContent(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Curso_Tema_Cont_Para_Presentacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_tecu", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetPresentationCourseTheme(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Curso_Tema_Para_Presentacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_curs", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetPresentationCourse(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Curso_Para_Presentacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveProgrammingEvent(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Programacion_Detalle";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveProgrammingPerson(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Programacion_Persona";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveProgrammingArea(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Programacion_Area";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveProgramming(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Programacion_Curso";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetProgrammingList(int p_area)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_List_Programacion";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetMyCourses2Home(Home_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_List_Mis_Cursos";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_flag_session", p_Model.LoggedIn));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pers", p_Model.PersonId));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_prov", p_Model.ProviderId));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetRegister(Register_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Datos_Persona";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_tdoc", p_Model.DocumentType));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_nro_documento", p_Model.DocumentNumber));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetEmailFormatById(decimal p_FormatId, string p_connection = "ELearning")
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql(p_connection))
                {
                    if (p_connection == "ELearning")
                    {
                        using (SqlDataAdapter da = new SqlDataAdapter())
                        {
                            conn.Open();
                            l_dt = new DataTable();
                            da.SelectCommand = conn.CreateCommand();
                            da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Formato_Correo";
                            da.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_fcor", p_FormatId));
                            da.Fill(l_dt);
                        }
                    }
                    else if (p_connection == "Administracion")
                    {
                        using (SqlDataAdapter da = new SqlDataAdapter())
                        {
                            conn.Open();
                            l_dt = new DataTable();
                            da.SelectCommand = conn.CreateCommand();
                            da.SelectCommand.CommandText = "dbo.SP_Visi_Sel_Formato_Correo";
                            da.SelectCommand.CommandType = CommandType.StoredProcedure;
                            da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_fcor", p_FormatId));
                            da.Fill(l_dt);
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetQuestion2Programming(Programming_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_Elea_Sel_List_Pregunta_Examen";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_examen", p_Model.TestId));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public string GetParameterByKey(string p_key, string p_connection = "ELearning")
        {
            string l_value = "";
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql(p_connection))
                {
                    if (p_connection == "ELearning")
                    {
                        using (SqlCommand cmd = new SqlCommand("dbo.Sp_Elea_Obt_Parametro_Clave", conn))
                        {
                            conn.Open();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@Clave", p_key));
                            cmd.Parameters.Add("@Dato", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                            cmd.ExecuteNonQuery();
                            l_value = Convert.ToString(cmd.Parameters["@Dato"].Value);
                        }
                    }
                    else if (p_connection == "Administracion")
                    {
                        using (SqlCommand cmd = new SqlCommand("dbo.SP_Visi_Obt_Parametro_Clave", conn))
                        {
                            conn.Open();
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@Clave", p_key));
                            cmd.Parameters.Add("@Dato", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                            cmd.ExecuteNonQuery();
                            l_value = Convert.ToString(cmd.Parameters["@Dato"].Value);
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return l_value;
        }
        public DateTime GetUTCDate()
        {
            DateTime l_utc = default(DateTime);
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Cotizador"))
                using (SqlCommand cmd = new SqlCommand("SELECT dbo.Sf_Elea_Obt_S_Fecha_Hora_UTC()", conn))
                {
                    conn.Open();
                    cmd.CommandType = CommandType.Text;
                    l_utc = Convert.ToDateTime(cmd.ExecuteScalar());
                }
            }
            catch
            {
                throw;
            }
            return l_utc;
        }
        public DataTable GetProvider2Programming(Programming_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_List_Proveedor_Area";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_Model.AreaId));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetArea2Programming(Programming_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_List_Area";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pn_codi_area_sel",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Model.DtAreaIds
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetInstructor2Programming(Programming_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_Elea_Sel_List_instructor";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo", p_Model.PersonType));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_inst", p_Model.InstructorId));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveTestQuestion(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Examen_Pregunta";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_examen", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetListQuestion2Test(Test_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_ELEA_Sel_List_Preg";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_curs", p_Model.CourseId));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_preg_sel",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Model.DtQuestionIds
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetTestList(int p_area)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_List_Examen";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveTest(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Examen";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_examen", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable QuestionTypeDropDown()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_ELEA_DropDown_Tipo_Pregunta";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetQuestionList(int p_area)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_List_Pregunta";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveQuestionAnswer(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Pregunta_Respuesta";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_preg", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveQuestion(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Pregunta";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_preg", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetPerson2Programming(Programming_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_List_Persona_Privado";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo", p_Model.PersonType));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_pers",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_Model.DtPersonIds
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable TestCourseDropDown(int p_course)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_ELEA_DropDown_Examen_Curso";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_curs", p_course));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable ModeDropDown()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_DropDown_Modalidad";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable PlaceDropDown()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_ELEA_DropDown_Lugar";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable CourseAreaDropDown(int p_area)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_ELEA_DropDown_Curso_area";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable CourseDropDown()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_ELEA_DropDown_Curso";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveCourse(int p_id)
        {
            SqlConnection l_cn = null;
            SqlDataAdapter l_da = null;
            DataTable l_dt = null;
            try
            {
                l_cn = ConnectionHelpers.ConnectToSql("ELearning");
                if (l_cn != null)
                {
                    l_cn.Open();
                    l_da = new SqlDataAdapter();
                    l_dt = new DataTable();
                    l_da.SelectCommand = l_cn.CreateCommand();
                    l_da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Curso";
                    l_da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    l_da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_curs", p_id));
                    l_da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (l_cn != null)
                {
                    l_cn.Close();
                    l_cn.Dispose();
                }
            }
            return l_dt;
        }
        public DataTable RetrieveCourseTheme(int p_id)
        {
            SqlConnection l_cn = null;
            SqlDataAdapter l_da = null;
            DataTable l_dt = null;
            try
            {
                l_cn = ConnectionHelpers.ConnectToSql("ELearning");
                if (l_cn != null)
                {
                    l_cn.Open();
                    l_da = new SqlDataAdapter();
                    l_dt = new DataTable();
                    l_da.SelectCommand = l_cn.CreateCommand();
                    l_da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Curso_Tema";
                    l_da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    l_da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_curs", p_id));
                    l_da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (l_cn != null)
                {
                    l_cn.Close();
                    l_cn.Dispose();
                }
            }
            return l_dt;
        }
        public DataTable RetrieveCourseThemeItem(int p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Curso_Tema_Contenido";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_tecu", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetCourseList(Course_Model p_Model, int p_area)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_List_Curso";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_clasificacion", p_Model.Classification));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_area", p_area));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable CategoryDropDown()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.SP_ELEA_DropDown_Categoria";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable DocumentTypeDropDown()
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_DropDown_Tipo_Documento";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public DataTable GetProgInscrito(int codi_pins)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Prog_Inscrito";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pins", codi_pins));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetCodigoTema(int codi_pcur)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Obt_Codigo_Tema";
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", codi_pcur));
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public DataTable GetDatosPersona(decimal p_pers_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Visi_Get_Datos_Personal_Persona";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@codi_pers", p_pers_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        /* Codigo de Aplicacion - No manipular - */
        #region App
        public DataTable GetProfileDescription(decimal p_ProfileId)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Seguridad"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Segu_Descripcion_Perfil";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Pn_codiPerf", p_ProfileId));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetProfileList(Account_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Seguridad"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Segu_Usuario_PerfilSistemas";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodeUsuario", p_Model.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@CodeSistema", p_Model.AppId));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetLoginValidate(Account_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Seguridad"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Segu_Validar_Acceso";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@UserId", p_Model.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@Password", p_Model.Password));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@AppId", p_Model.AppId));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public Profile_Entity GetProfileInformation(decimal p_Profile)
        {
            Profile_Entity l_profile = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Seguridad"))
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "dbo.Sp_Segu_Perfil_Informacion";
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_perf", p_Profile));
                    using (var reader = cmd.ExecuteReader(CommandBehavior.SingleResult))
                    {
                        while (reader.Read())
                        {
                            l_profile = new Profile_Entity
                            {
                                Description = reader.IsDBNull(0) ? "" : reader.GetString(0),
                                AreaId = reader.IsDBNull(1) ? 0 : reader.GetDecimal(1),
                                AreaDescription = reader.IsDBNull(2) ? "" : reader.GetString(2),
                                PlaceId = reader.IsDBNull(3) ? 0 : reader.GetInt32(3),
                                Group = reader.IsDBNull(4) ? "" : reader.GetString(4)
                            };
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return l_profile;
        }
        public User_Entity GetUserInformation(string p_Username)
        {
            User_Entity l_user = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "dbo.Sp_Elea_Obt_Usuario_Informacion";
                    cmd.Parameters.Add(new SqlParameter("@pv_codi_user", p_Username));
                    using (var reader = cmd.ExecuteReader(CommandBehavior.SingleResult))
                    {
                        while (reader.Read())
                        {
                            l_user = new User_Entity
                            {
                                FullName = reader.IsDBNull(0) ? "" : reader.GetString(0),
                                PersonId = reader.IsDBNull(1) ? 0 : reader.GetInt64(1),
                                ProviderId = reader.IsDBNull(2) ? 0 : reader.GetDecimal(2),
                                PersonType = reader.IsDBNull(3) ? "" : reader.GetString(3),
                                EmployeeId = reader.IsDBNull(4) ? 0 : reader.GetInt64(4),
                                EmployeeFichaSap = reader.IsDBNull(5) ? "" : reader.GetString(5),
                                PersonProviderId = reader.IsDBNull(6) ? 0 : reader.GetDecimal(6),
                                PersonDocumentNumber = reader.IsDBNull(7) ? "" : reader.GetString(7),
                                ProviderIncotermsId = reader.IsDBNull(8) ? 0 : reader.GetInt32(8)
                            };
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return l_user;
        }
        public List<AppObject_Entity> GetAppObject(AppObject_Model p_Model)
        {
            List<AppObject_Entity> l_list = new List<AppObject_Entity>();
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Seguridad"))
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    conn.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "dbo.Sp_Segu_List_Obj_Security";
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_sistem", p_Model.AppId));
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_perfil", p_Model.ProfileId));
                    cmd.Parameters.Add(new SqlParameter("@pv_codi_user", p_Model.UserId));
                    cmd.Parameters.Add(new SqlParameter("@pn_codi_objPadre", p_Model.ParentId));
                    using (var reader = cmd.ExecuteReader(CommandBehavior.SingleResult))
                    {
                        while (reader.Read())
                        {
                            l_list.Add(new AppObject_Entity
                            {
                                Icon = reader.IsDBNull(0) ? "" : reader.GetString(0),
                                Name = reader.IsDBNull(1) ? "" : reader.GetString(1),
                                Controller = reader.IsDBNull(2) ? "" : reader.GetString(2),
                                Action = reader.IsDBNull(3) ? "" : reader.GetString(3),
                                Id = reader.IsDBNull(4) ? 0 : reader.GetInt32(4),
                                Type = reader.IsDBNull(5) ? 0 : reader.GetInt32(5),
                                Color = reader.IsDBNull(6) ? "" : reader.GetString(6),
                                ParentId = reader.IsDBNull(7) ? 0 : reader.GetInt32(7),
                                Completed = reader.IsDBNull(8) ? true : reader.GetInt32(8) == 1,
                                Order = reader.IsDBNull(9) ? 0 : reader.GetDecimal(9),
                                Description = reader.IsDBNull(10) ? "" : reader.GetString(10)
                            });
                        }
                    }
                }
            }
            catch
            {

                throw;
            }
            return l_list;
        }

        public DataTable GetValidarEmail(ValEmail_Model p_Model)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Seguridad"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Validar_Email";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_email", p_Model.Email));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_app_id", p_Model.AppId));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetUserInfo(string username)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Seguridad"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Get_User_Info";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_user_red", username));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetPersonaPrivado(decimal p_codi_exin)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_List_Persona_Privado_CUARENTENA";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_exin", p_codi_exin));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable GetDeclaracionJurada(decimal codi_pers)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_List_Dec_Jur";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pers", codi_pers));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }

        public DataTable GetProgramming(Int64 p_id)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Get_Programacion_Curso";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_pcur", p_id));
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        public DataTable RetrieveProgrammingPrivatePersonCheck(int p_id, DataTable p_fichas)
        {
            DataTable l_dt = null;
            try
            {
                using (SqlConnection conn = ConnectionHelpers.ConnectToSql("ELearning"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Elea_Sel_Ret_Programacion_Privada_Persona_Check";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_pcur", p_id));
                    da.SelectCommand.Parameters.Add(new SqlParameter
                    {
                        ParameterName = "@pt_fichas",
                        SqlDbType = SqlDbType.Structured,
                        Value = p_fichas
                    });
                    da.Fill(l_dt);
                }
            }
            catch
            {
                throw;
            }
            return l_dt;
        }
        #endregion App
    }
}