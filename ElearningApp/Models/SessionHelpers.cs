﻿using ElearningApp.Models.Business;
using ElearningApp.Models.Dao;
using ElearningApp.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace ElearningApp.Models
{
    public class SessionHelpers
    {
        public static bool SetAuthenticated()
        {
            try
            {
                HttpContext.Current.Session["Authenticated"] = true;
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool SetValue(string key, string value)
        {
            try
            {
                HttpContext.Current.Session[key] = value;
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static string GetValue(string key)
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session[key]);
            }
            catch
            {
                return null;
            }
        }
        public static bool IsAuthenticated()
        {
            try
            {
                return Convert.ToBoolean(HttpContext.Current.Session["Authenticated"]);
            }
            catch
            {
                return false;
            }
        }
        public static decimal GetSessionLog(HttpSessionState p_session = null)
        {
            try
            {
                if (p_session != null)
                {
                    return Convert.ToDecimal(p_session["SessionLog"]);
                }
                return Convert.ToDecimal(HttpContext.Current.Session["SessionLog"]);
            }
            catch
            {
                return 0;
            }
        }
        public static void SetVirtualUsername(string username)
        {
            HttpContext.Current.Session["VirtualUsername"] = username;
        }
        public static void SetVirtualProfiles(List<object> profiles)
        {
            var l_profiles = new List<decimal>();
            foreach (List<object> row in profiles)
            {
                l_profiles.Add(Convert.ToDecimal(row.ElementAt(1)));
            }
            HttpContext.Current.Session["VirtualProfiles"] = l_profiles;
        }
        public static void SetVirtualLogin(string login)
        {
            HttpContext.Current.Session["VirtualLogin"] = login;
        }
        public static void SetVirtualPassword(string password)
        {
            HttpContext.Current.Session["VirtualPassword"] = password;
        }
        public static void RemoveVirtualSession()
        {
            try
            {
                HttpContext.Current.Session.Remove("VirtualLogin");
                HttpContext.Current.Session.Remove("VirtualUsername");
                HttpContext.Current.Session.Remove("VirtualProfiles");
                HttpContext.Current.Session.Remove("VirtualPassword");
            }
            catch
            {
            }
        }
        public static string GetVirtualUsername()
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session["VirtualUsername"]);
            }
            catch
            {
                return null;
            }
        }
        public static string GetVirtualPassword()
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session["VirtualPassword"]);
            }
            catch
            {
                return null;
            }
        }
        public static bool ExistsProfileVirtual(decimal profile)
        {
            try
            {
                return (HttpContext.Current.Session["VirtualProfiles"] as List<decimal>).Contains(profile);
            }
            catch
            {
                return false;
            }
        }
        public static bool InitializeSession(Account_Model p_Model)
        {
            Dao_Crud l_crud;
            Dao_List l_list;
            try
            {
                l_crud = new Dao_Crud();
                l_list = new Dao_List();
                HttpContext.Current.Session["Username"] = HttpContext.Current.Session["VirtualUsername"];
                HttpContext.Current.Session["Login"] = HttpContext.Current.Session["VirtualLogin"];
                HttpContext.Current.Session["ProfileId"] = p_Model.ProfileId;
                HttpContext.Current.Session["IPAddress"] = p_Model.IPAddress;
                HttpContext.Current.Session["AppId"] = Convert.ToDecimal(Helpers.GetAppSettings("App_AppId"));
                //
                HttpContext.Current.Session["SessionLog"] = l_crud.StartSession(new Session_Entity
                {
                    AppId = GetAppId(),
                    ProfileId = GetProfileId(),
                    Username = GetUsername(),
                    IPAddress = GetIPAddress(),
                    Information = p_Model.Browser
                });
                HttpContext.Current.Session["SessionID"] = HttpContext.Current.Session.SessionID + "l" + Convert.ToString(GetSessionLog());
                HttpContext.Current.Session["User"] = l_list.GetUserInformation(GetUsername());
                HttpContext.Current.Session["Profile"] = l_list.GetProfileInformation(GetProfileId());
                HttpContext.Current.Session["FromLogistica"] = l_list.GetFromLogistica(GetUser());
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                l_crud = null;
                l_list = null;
            }
        }
        public static bool DestroySession()
        {
            Dao_Crud l_dao;
            decimal l_sessionid;
            try
            {
                l_dao = new Dao_Crud();
                l_sessionid = GetSessionLog();
                if (HttpContext.Current.Session.Count != 0)
                {
                    HttpContext.Current.Session.Abandon();
                    //HttpContext.Current.Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                }
                l_dao.EndSession(new Session_Entity
                {
                    SessionId = l_sessionid
                });
                return true;
            }
            catch
            {
                return false;
            }
            finally
            {

            }
        }
        public static decimal GetAppId()
        {
            try
            {
                return Convert.ToDecimal(HttpContext.Current.Session["AppId"]);
            }
            catch
            {
                return 0;
            }
        }
        public static decimal GetProfileId()
        {
            try
            {
                return Convert.ToDecimal(HttpContext.Current.Session["ProfileId"]);
            }
            catch
            {
                return 0;
            }
        }
        public static string GetIPAddress()
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session["IPAddress"]);
            }
            catch
            {
                return null;
            }
        }
        public static string GetUsername()
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session["Username"]);
            }
            catch
            {
                return null;
            }
        }
        public static User_Entity GetUser()
        {
            try
            {
                return HttpContext.Current.Session["User"] as User_Entity;
            }
            catch
            {
                return null;
            }
        }
        public static Profile_Entity GetProfile()
        {
            try
            {
                return HttpContext.Current.Session["Profile"] as Profile_Entity;
            }
            catch
            {
                return null;
            }
        }
        public static string GetSessionID()
        {
            try
            {
                return Convert.ToString(HttpContext.Current.Session["SessionID"]);
            }
            catch
            {
                return "";
            }
        }
        public static bool IsFromLogistica()
        {
            try
            {
                return Convert.ToBoolean(HttpContext.Current.Session["FromLogistica"]);
            }
            catch
            {
                return false;
            }
        }
        public static bool IsFromGeneralLogin()
        {
            try
            {
                return HttpContext.Current.Session["Login"].ToString() == "General";
            }
            catch
            {
                return false;
            }
        }
        public static bool IsFromCarrierLogin()
        {
            try
            {
                return HttpContext.Current.Session["Login"].ToString() == "Carrier";
            }
            catch
            {
                return false;
            }
        }
        public static bool IsProviderProfile()
        {
            return Convert.ToDecimal(Helpers.GetParameterByKey("CODI_PERF_PROVEEDOR")) == GetProfileId();
        }
        public static bool IsLogisticaAdminProfile()
        {
            return Convert.ToDecimal(Helpers.GetParameterByKey("CODI_PERF_ADMIN_LOGISTICA")) == GetProfileId();
        }
        public static bool IsCapacitacionAdminProfile()
        {
            return Convert.ToDecimal(Helpers.GetParameterByKey("CODI_PERF_ADMIN_CAPACITACION")) == GetProfileId();
        }
    }
}