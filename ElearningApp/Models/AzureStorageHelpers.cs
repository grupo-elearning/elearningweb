﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.IO;

namespace ElearningApp.Models
{
    public class AzureStorageHelpers
    {
        private readonly string AccountName;
        private readonly string AccessKey;
        private readonly string Containername;
        private CloudStorageAccount StorageAccount = null;
        private CloudBlobClient BlobClient = null;
        private CloudBlobContainer Blobcontainer = null;
        public AzureStorageHelpers(string p_container, BlobContainerPublicAccessType p_paccess = BlobContainerPublicAccessType.Blob, string p_accountName = "Azure_AccountName", string p_accessKey = "Azure_AccessKey")
        {
            bool l_created;
            // Get Azure config
            AccountName = System.Configuration.ConfigurationManager.AppSettings.Get(p_accountName);
            AccessKey = System.Configuration.ConfigurationManager.AppSettings.Get(p_accessKey);
            Containername = System.Configuration.ConfigurationManager.AppSettings.Get(p_container);
            //
            StorageAccount = new CloudStorageAccount(
                new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(
                    AccountName,
                    AccessKey), true);
            // Create a blob client.
            BlobClient = StorageAccount.CreateCloudBlobClient();
            // Get a reference to a container
            Blobcontainer = BlobClient.GetContainerReference(Containername);
            // If "mycontainer" doesn't exist, create it
            l_created = Blobcontainer.CreateIfNotExists();
            if (l_created)
            {
                // Set permissions
                Blobcontainer.SetPermissions(new BlobContainerPermissions
                {
                    PublicAccess = p_paccess
                });
            }
        }
        public void UploadBlob(string p_name, Stream p_stream, string p_ContentType = null)
        {
            CloudBlockBlob blockBlob = null;
            try
            {
                // Get a reference to a blob
                blockBlob = Blobcontainer.GetBlockBlobReference(p_name);
                // Set ContentType
                if (p_ContentType != null)
                {
                    blockBlob.Properties.ContentType = p_ContentType;
                }
                // Upload the blob
                blockBlob.UploadFromStream(p_stream);
            }
            catch
            {
                throw;
            }
            finally
            {
                blockBlob = null;
            }
        }
        public void UploadBlob(string p_name, string p_path)
        {
            CloudBlockBlob blockBlob = null;
            try
            {
                // Get a reference to a blob
                blockBlob = Blobcontainer.GetBlockBlobReference(p_name);
                // Upload the blob
                blockBlob.UploadFromFile(p_path);
            }
            catch
            {
                throw;
            }
            finally
            {
                blockBlob = null;
            }
        }
        public MemoryStream DownloadBlob(string p_name)
        {
            MemoryStream stream_return = null;
            CloudBlockBlob blockBlob = null;
            try
            {
                stream_return = new MemoryStream();
                // Get a reference to a blob
                blockBlob = Blobcontainer.GetBlockBlobReference(p_name);
                // Save the blob contents to a file
                blockBlob.DownloadToStream(stream_return);
                stream_return.Position = 0;
                return stream_return;
            }
            catch
            {
                throw;
            }
            finally
            {
                blockBlob = null;
            }
        }
        public void DownloadBlob(string p_name, string p_path)
        {
            CloudBlockBlob blockBlob = null;
            try
            {
                // Get a reference to a blob
                blockBlob = Blobcontainer.GetBlockBlobReference(p_name);
                // Save the blob contents to a file
                using (var fileStream = File.OpenWrite(p_path))
                {
                    blockBlob.DownloadToStream(fileStream);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                blockBlob = null;
            }
        }
        public void DeleteBlob(string p_name)
        {
            CloudBlockBlob blockBlob = null;
            try
            {
                // Get a reference to a blob
                blockBlob = Blobcontainer.GetBlockBlobReference(p_name);
                // Delete the blob.
                blockBlob.DeleteIfExists();
            }
            catch
            {
                throw;
            }
            finally
            {
                blockBlob = null;
            }
        }
        public void DeleteDirectory(string p_name)
        {
            foreach (IListBlobItem blob in Blobcontainer.GetDirectoryReference(p_name).ListBlobs(true))
            {
                if (blob.GetType() == typeof(CloudBlob) || blob.GetType().BaseType == typeof(CloudBlob))
                {
                    ((CloudBlob)blob).DeleteIfExists();
                }
            }
        }
        public void RenameDirectory(string p_oldName, string p_newName)
        {
            foreach (IListBlobItem blob in Blobcontainer.GetDirectoryReference(p_oldName).ListBlobs(true))
            {
                if (blob.GetType() == typeof(CloudBlob) || blob.GetType().BaseType == typeof(CloudBlob))
                {
                    var l_oldBlob = Blobcontainer.GetBlockBlobReference(((CloudBlob)blob).Name);
                    var l_newBlob = Blobcontainer.GetBlockBlobReference(Helpers.ReplaceFirstOccurrence(((CloudBlob)blob).Name, p_oldName, p_newName));
                    // Copy
                    l_newBlob.StartCopy(l_oldBlob);
                    l_oldBlob.Delete();
                }
            }
        }
        public CloudBlockBlob GetBlob(string p_name)
        {
            try
            {
                // Get a reference to a blob
                return Blobcontainer.GetBlockBlobReference(p_name);
            }
            catch
            {
                throw;
            }
        }
        public string GetBase64(string p_name)
        {
            return Convert.ToBase64String(DownloadBlob(p_name).ToArray());
        }
        public void Clear()
        {
            StorageAccount = null;
            BlobClient = null;
            Blobcontainer = null;
        }
    }
}