﻿using ElearningApp.Models.Business;
using ElearningApp.Models.Entity;
using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ElearningApp.Models
{
    public class EmailHelpers
    {
        private static void SendEmailSqlServer(string correos, string subject, string html, string copy)
        {
            var my_jsondata_login = new
            {
                Username = "test",
                Password = "123456"
            };
            //Tranform it to Json object
            string l_token = "";

            var my_jsondata_data = new
            {
                Profile = "ProfileCampus",
                To = correos,
                Copy = (copy == null ? "-" : copy),
                Subject = subject,
                Body = html
            };
            string l_return = Helpers.executeAPI(my_jsondata_data,
                System.Configuration.ConfigurationManager.AppSettings.Get("SendEmail_SqlServer"), l_token);
            var json_data = JsonConvert.DeserializeObject<object>(l_return);
        }
        public static void SendError(string p_Message)
        {
            SendGridClient l_send_grid = null;
            try
            {
                l_send_grid = new SendGridClient(GetSendGridKey());
                //send
                var l_sgmessage = new SendGridMessage()
                {
                    From = new EmailAddress(Helpers.GetParameterByKey("EMAIL_ERROR_FROM")),
                    Subject = "Error",
                    HtmlContent = p_Message
                };
                //destination
                if (ToDevelopers())
                {
                    l_sgmessage.AddTos(GetDeveloperEmail());
                }
                else
                {
                    l_sgmessage.AddTos(GetErrorEmail());
                }
                //
                //SendEmailAsync(l_send_grid, l_sgmessage);

                //Uso de la API para enviar correos
                string correos = "";
                if (ToDevelopers())
                {
                    foreach (EmailAddress l_mail in GetDeveloperEmail())
                    {
                        correos += l_mail.Email + ";";
                    }
                }
                else
                {
                    foreach (EmailAddress l_mail in GetErrorEmail())
                    {
                        correos += l_mail.Email + ";";
                    }
                }
                correos = correos.Substring(0, correos.Length - 1);
                SendEmailSqlServer(correos, "Error", p_Message, null);
            }
            catch
            {
                throw;
            }
        }
        public static void SendCredentialsAgain(Person_Model p_Person)
        {
            Bus_List l_list = null;
            SendGridClient l_send_grid = null;
            string l_fcor, l_template, l_html_content, l_email;
            string[] l_scheme;
            try
            {
                l_list = new Bus_List();
                l_send_grid = new SendGridClient(GetSendGridKey());
                l_fcor = Helpers.GetParameterByKey("FORMATO_EMAIL_RE_ENVIO_CREDENCIALES");
                l_scheme = GetEmailScheme(Convert.ToDecimal(l_fcor), l_list);
                l_template = l_scheme[6]
                    .Replace("{tag_style}", l_scheme[2])
                    .Replace("{tag_head}", l_scheme[3])
                    .Replace("{tag_body}", l_scheme[4])
                    .Replace("{tag_foot}", l_scheme[5]);
                //get email
                l_email = l_list.GetPersonEmail(p_Person.Id, "P");
                l_html_content = l_template
                    .Replace("{fullname}", p_Person.Fullname)
                    .Replace("{username}", p_Person.Username)
                    .Replace("{password}", p_Person.Password);
                //send
                var l_sgmessage = new SendGridMessage()
                {
                    From = new EmailAddress(l_scheme[0]),
                    Subject = l_scheme[1],
                    HtmlContent = l_html_content
                };
                //destination
                if (ToDevelopers())
                {
                    l_sgmessage.AddTos(GetDeveloperEmail());
                }
                else
                {
                    l_sgmessage.AddTo(l_email);
                }
                //
                //SendEmailAsync(l_send_grid, l_sgmessage, l_scheme[7], l_scheme[8]);

                //Uso de la API para enviar correos
                string correos = "";
                if (ToDevelopers())
                {
                    foreach (EmailAddress l_mail in GetDeveloperEmail())
                    {
                        correos += l_mail.Email + ";";
                    }
                    correos = correos.Substring(0, correos.Length - 1);
                }
                else
                {
                    correos = l_email;
                }
                SendEmailSqlServer(correos, l_scheme[1], l_html_content, null);
            }
            catch
            {
                throw;
            }
            finally
            {
                l_list = null;
            }
        }
        public static void SendEmailProviderDocumentDeny(bool p_Provider, bool p_Employee, bool p_Vehicle, ProviderToApprove_Entity p_Entity)
        {
            Bus_List l_list = null;
            SendGridClient l_send_grid = null;
            string l_fcor, l_template, l_provider_html, l_employee_html, l_vehicle_html, l_provider_email;
            string[] l_scheme;
            bool l_to_developers = ToDevelopers();
            DataTable l_dt_email_info = null;
            try
            {
                l_list = new Bus_List();
                l_send_grid = new SendGridClient(GetSendGridKey());
                l_fcor = Helpers.GetParameterByKey("FORMATO_EMAIL_ENVIAR_RECHAZADO");
                l_scheme = GetEmailScheme(Convert.ToDecimal(l_fcor), l_list);
                l_template = l_scheme[6]
                    .Replace("{tag_style}", l_scheme[2])
                    .Replace("{tag_head}", l_scheme[3])
                    .Replace("{tag_body}", l_scheme[4])
                    .Replace("{tag_foot}", l_scheme[5]);
                //get provider email
                l_provider_email = l_list.GetProviderEmail(Convert.ToInt32(p_Entity.ProviderId));
                //get info
                l_dt_email_info = l_list.GetEmailInformation();
                //build content
                l_provider_html = "";
                l_employee_html = "";
                l_vehicle_html = "";
                if (p_Provider)
                {
                    //provider
                    l_provider_html += "<h3>Proveedor:</h3>";
                    for (var i = 0; i < p_Entity.ProviderPrdo.Length; i++)
                    {
                        if (p_Entity.ProviderSwitch[i] == "R")
                        {
                            l_provider_html += l_list.GetValidationHtmlProviderDocument(Convert.ToInt32(p_Entity.ProviderPrdo[i]));
                        }
                    }
                }
                if (p_Employee)
                {
                    //employee
                    l_employee_html += "<h3>Trabajador:</h3>";
                    for (var i = 0; i < p_Entity.EmployeePcdo.Length; i++)
                    {
                        if (p_Entity.EmployeeSwitch[i] == "R")
                        {
                            l_employee_html += l_list.GetValidationHtmlProviderEmployeeDocument(Convert.ToInt32(p_Entity.EmployeePcdo[i]));
                        }
                    }
                }
                if (p_Vehicle)
                {
                    //vehicle
                    l_vehicle_html += "<h3>Vehículo:</h3>";
                    for (var i = 0; i < p_Entity.VehiclePcdo.Length; i++)
                    {
                        if (p_Entity.VehicleSwitch[i] == "R")
                        {
                            l_vehicle_html += l_list.GetValidationHtmlProviderVehicleDocument(Convert.ToInt32(p_Entity.VehiclePcdo[i]));
                        }
                    }
                }
                //get final template
                l_template = l_template
                    .Replace("{sider_web_url}", l_dt_email_info.Rows[0]["SiderperuWebUrl"].ToString())
                    .Replace("{sider_web_text}", l_dt_email_info.Rows[0]["SiderperuWebTexto"].ToString())
                    .Replace("{gerdau_web_url}", l_dt_email_info.Rows[0]["GerdarWebUrl"].ToString())
                    .Replace("{gerdau_web_text}", l_dt_email_info.Rows[0]["GerdarWebTexto"].ToString())
                    .Replace("{sider_short_name}", l_dt_email_info.Rows[0]["SiderperuShortName"].ToString())//
                    .Replace("{invoice_proveedor}", l_provider_html)
                    .Replace("{invoice_trabajador}", l_employee_html)
                    .Replace("{invoice_vehiculo}", l_vehicle_html);
                //send
                var l_sgmessage = new SendGridMessage()
                {
                    From = new EmailAddress(l_scheme[0]),
                    Subject = l_scheme[1],
                    HtmlContent = l_template
                };
                //destination
                if (l_to_developers)
                {
                    l_sgmessage.AddTos(GetDeveloperEmail());
                }
                else
                {
                    l_sgmessage.AddTo(l_provider_email);
                }
                //
                //SendEmailAsync(l_send_grid, l_sgmessage, l_scheme[7], l_scheme[8]);

                //Uso de la API para enviar correos
                string correos = "";
                if (l_to_developers)
                {
                    foreach (EmailAddress l_mail in GetDeveloperEmail())
                    {
                        correos += l_mail.Email + ";";
                    }
                    correos = correos.Substring(0, correos.Length - 1);
                }
                else
                {
                    correos = l_provider_email;
                }
                SendEmailSqlServer(correos, l_scheme[1], l_template, null);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static void SendEmailProvider(string p_BusinessName, string p_Email, string p_Username, string p_Password)
        {
            Bus_List l_list = null;
            SendGridClient l_send_grid = null;
            string l_fcor, l_template, l_html_content;
            string[] l_scheme;
            bool l_to_developers = ToDevelopers();
            try
            {
                l_list = new Bus_List();
                l_send_grid = new SendGridClient(GetSendGridKey());
                l_fcor = Helpers.GetParameterByKey("FORMATO_EMAIL_ENVIAR_PASSWORD");
                l_scheme = GetEmailScheme(Convert.ToDecimal(l_fcor), l_list);
                l_template = l_scheme[6]
                    .Replace("{tag_style}", l_scheme[2])
                    .Replace("{tag_head}", l_scheme[3])
                    .Replace("{tag_body}", l_scheme[4])
                    .Replace("{tag_foot}", l_scheme[5]);
                //get content
                l_html_content = l_template
                    .Replace("{fullname}", p_BusinessName)
                    .Replace("{username}", p_Username)
                    .Replace("{password}", p_Password);
                //send
                var l_sgmessage = new SendGridMessage()
                {
                    From = new EmailAddress(l_scheme[0]),
                    Subject = l_scheme[1],
                    HtmlContent = l_html_content
                };
                //destination
                if (l_to_developers)
                {
                    l_sgmessage.AddTos(GetDeveloperEmail());
                }
                else
                {
                    l_sgmessage.AddTo(Convert.ToString(p_Email));
                }
                //
                //SendEmailAsync(l_send_grid, l_sgmessage, l_scheme[7], l_scheme[8]);

                //Uso de la API para enviar correos
                string correos = "";
                if (l_to_developers)
                {
                    foreach (EmailAddress l_mail in GetDeveloperEmail())
                    {
                        correos += l_mail.Email + ";";
                    }
                    correos = correos.Substring(0, correos.Length - 1);
                }
                else
                {
                    correos = p_Email;
                }

                SendEmailSqlServer(correos, l_scheme[1], l_html_content, null);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static void SendNewPassword(string p_NewPassword)
        {
            Bus_List l_list = null;
            SendGridClient l_send_grid = null;
            string l_fcor, l_template, l_html_content, l_email;
            string[] l_scheme;
            try
            {
                l_list = new Bus_List();
                l_send_grid = new SendGridClient(GetSendGridKey());
                l_fcor = Helpers.GetParameterByKey("FORMATO_EMAIL_CAMBIO_CONTRASENIA");
                l_scheme = GetEmailScheme(Convert.ToDecimal(l_fcor), l_list);
                l_template = l_scheme[6]
                    .Replace("{tag_style}", l_scheme[2])
                    .Replace("{tag_head}", l_scheme[3])
                    .Replace("{tag_body}", l_scheme[4])
                    .Replace("{tag_foot}", l_scheme[5]);
                //get email
                if (SessionHelpers.GetUser().PersonId != 0)
                {
                    l_email = l_list.GetPersonEmail(Convert.ToInt32(SessionHelpers.GetUser().PersonId), "P");
                }
                else
                {
                    l_email = l_list.GetProviderEmail(Convert.ToInt32(SessionHelpers.GetUser().ProviderId));
                }
                //get content
                l_html_content = l_template
                    .Replace("{fullname}", SessionHelpers.GetUser().FullName)
                    .Replace("{new_password}", p_NewPassword);
                //send
                var l_sgmessage = new SendGridMessage()
                {
                    From = new EmailAddress(l_scheme[0]),
                    Subject = l_scheme[1],
                    HtmlContent = l_html_content
                };
                //destination
                if (ToDevelopers())
                {
                    l_sgmessage.AddTos(GetDeveloperEmail());
                }
                else
                {
                    l_sgmessage.AddTo(l_email);
                }
                //
                //SendEmailAsync(l_send_grid, l_sgmessage, l_scheme[7], l_scheme[8]);

                //Uso de la API para enviar correos
                string correos = "";
                if (ToDevelopers())
                {
                    foreach (EmailAddress l_mail in GetDeveloperEmail())
                    {
                        correos += l_mail.Email + ";";
                    }
                    correos = correos.Substring(0, correos.Length - 1);
                }
                else
                {
                    correos = l_email;
                }
                SendEmailSqlServer(correos, l_scheme[1], l_html_content, null);
            }
            catch
            {
                throw;
            }
            finally
            {
                l_list = null;
            }
        }
        public static void SendNotificationRegisterPrivate(Programming_Entity p_Programming)
        {
            Bus_List l_list = null;
            SendGridClient l_send_grid = null;
            string l_fcor, l_template, l_html_content, l_course_name, l_mode, l_place_description, l_from_hour, l_to_hour;
            string[] l_scheme;
            bool l_to_developers = ToDevelopers();
            try
            {
                l_list = new Bus_List();
                l_send_grid = new SendGridClient(GetSendGridKey());
                l_fcor = Helpers.GetParameterByKey("FORMATO_EMAIL_NOTIFICACION_REGISTRO_PRIVADO");
                l_scheme = GetEmailScheme(Convert.ToDecimal(l_fcor), l_list);
                l_template = l_scheme[6]
                    .Replace("{tag_style}", l_scheme[2])
                    .Replace("{tag_head}", l_scheme[3])
                    .Replace("{tag_body}", l_scheme[4])
                    .Replace("{tag_foot}", l_scheme[5]);
                //get course name
                var l_course = l_list.RetrieveCourse(Convert.ToInt32(p_Programming.CourseId));
                l_course_name = Convert.ToString(l_course.Rows[0]["Nombre"]);
                Helpers.ClearDataTable(l_course);
                //get mode
                l_mode = "-";
                l_from_hour = "--:--";
                l_to_hour = "--:--";
                if (p_Programming.ModeId == Convert.ToDecimal(Helpers.GetParameterByKey("CODI_MODA_PRES")))
                {
                    l_mode = "Presencial";
                    l_from_hour = p_Programming.FromHour.Substring(0, 2) + ":" + p_Programming.FromHour.Substring(2);
                    l_to_hour = p_Programming.ToHour.Substring(0, 2) + ":" + p_Programming.ToHour.Substring(2);
                }
                else if (p_Programming.ModeId == Convert.ToDecimal(Helpers.GetParameterByKey("CODI_MODA_ELEA")))
                {
                    l_mode = "ELearning";
                    l_from_hour = "--:--";
                    l_to_hour = "--:--";
                }
                //get place
                l_place_description = "";
                if (p_Programming.PlaceId == 0)
                {
                    l_place_description = p_Programming.PlaceDescription;
                }
                else
                {
                    var l_place = l_list.RetrievePlace(Convert.ToInt32(p_Programming.PlaceId));
                    l_place_description = l_place.Rows[0]["Descripcion"].ToString();
                    Helpers.ClearDataTable(l_place);
                }
                //get final subject
                l_scheme[1] = l_scheme[1].Replace("{course_name}", l_course_name);
                //get final template
                l_template = l_template
                    .Replace("{course_name}", l_course_name)
                    .Replace("{mode}", l_mode)
                    .Replace("{place}", l_place_description)
                    .Replace("{from_date}", p_Programming.UnlimitedProgramming == "S" ? "Ilimitado" : p_Programming.FromDate.ToString("dd/MM/yyyy"))
                    .Replace("{to_date}", p_Programming.UnlimitedProgramming == "S" ? "Ilimitado" : p_Programming.ToDate.ToString("dd/MM/yyyy"))
                    .Replace("{from_hour}", l_from_hour)
                    .Replace("{to_hour}", l_to_hour);
                //get persons
                var l_persons = l_list.RetrieveProgrammingPrivatePerson(Convert.ToInt32(p_Programming.Id));
                //send by person
                foreach (DataRow row in l_persons.Rows)
                {
                    l_html_content = l_template
                        .Replace("{fullname}", Convert.ToString(row["Nombre"]))
                        .Replace("{username}", Convert.ToString(row["Codi_User"]))
                        .Replace("{password}", Convert.ToString(row["Pass_User"]));
                    //send
                    var l_sgmessage = new SendGridMessage()
                    {
                        From = new EmailAddress(l_scheme[0]),
                        Subject = l_scheme[1],
                        HtmlContent = l_html_content
                    };
                    //destination
                    if (l_to_developers)
                    {
                        l_sgmessage.AddTos(GetDeveloperEmail());
                    }
                    else
                    {
                        l_sgmessage.AddTo(Convert.ToString(row["Email"]));
                    }
                    //
                    //SendEmailAsync(l_send_grid, l_sgmessage, l_scheme[7], l_scheme[8]);

                    //Uso de la API para enviar correos
                    string correos = "";
                    if (l_to_developers)
                    {
                        foreach (EmailAddress l_mail in GetDeveloperEmail())
                        {
                            correos += l_mail.Email + ";";
                        }
                        correos = correos.Substring(0, correos.Length - 1);
                    }
                    else
                    {
                        correos = Convert.ToString(row["Email"]);
                    }
                    SendEmailSqlServer(correos, l_scheme[1], l_html_content, null);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                l_list = null;
            }
        }
        public static void SendNotificationRegisterPrivateCheck(Programming_Entity p_Programming, DataTable dt_fichas)
        {
            Bus_List l_list = null;
            SendGridClient l_send_grid = null;
            string l_fcor, l_template, l_html_content, l_course_name, l_mode, l_place_description, l_from_hour, l_to_hour;
            string[] l_scheme;
            bool l_to_developers = ToDevelopers();
            try
            {
                l_list = new Bus_List();
                l_send_grid = new SendGridClient(GetSendGridKey());
                l_fcor = Helpers.GetParameterByKey("FORMATO_EMAIL_NOTIFICACION_REGISTRO_PRIVADO");
                l_scheme = GetEmailScheme(Convert.ToDecimal(l_fcor), l_list);
                l_template = l_scheme[6]
                    .Replace("{tag_style}", l_scheme[2])
                    .Replace("{tag_head}", l_scheme[3])
                    .Replace("{tag_body}", l_scheme[4])
                    .Replace("{tag_foot}", l_scheme[5]);
                //get course name
                var l_course = l_list.RetrieveCourse(Convert.ToInt32(p_Programming.CourseId));
                l_course_name = Convert.ToString(l_course.Rows[0]["Nombre"]);
                Helpers.ClearDataTable(l_course);
                //get mode
                l_mode = "-";
                l_from_hour = "--:--";
                l_to_hour = "--:--";
                if (p_Programming.ModeId == Convert.ToDecimal(Helpers.GetParameterByKey("CODI_MODA_PRES")))
                {
                    l_mode = "Presencial";
                    l_from_hour = p_Programming.FromHour.Substring(0, 2) + ":" + p_Programming.FromHour.Substring(2);
                    l_to_hour = p_Programming.ToHour.Substring(0, 2) + ":" + p_Programming.ToHour.Substring(2);
                }
                else if (p_Programming.ModeId == Convert.ToDecimal(Helpers.GetParameterByKey("CODI_MODA_ELEA")))
                {
                    l_mode = "ELearning";
                    l_from_hour = "--:--";
                    l_to_hour = "--:--";
                }
                //get place
                l_place_description = "";
                if (p_Programming.PlaceId == 0)
                {
                    l_place_description = p_Programming.PlaceDescription;
                }
                else
                {
                    var l_place = l_list.RetrievePlace(Convert.ToInt32(p_Programming.PlaceId));
                    l_place_description = l_place.Rows[0]["Descripcion"].ToString();
                    Helpers.ClearDataTable(l_place);
                }
                //get final subject
                l_scheme[1] = l_scheme[1].Replace("{course_name}", l_course_name);
                //get final template
                l_template = l_template
                    .Replace("{course_name}", l_course_name)
                    .Replace("{mode}", l_mode)
                    .Replace("{place}", l_place_description)
                    .Replace("{from_date}", p_Programming.UnlimitedProgramming == "S" ? "Ilimitado" : p_Programming.FromDate.ToString("dd/MM/yyyy"))
                    .Replace("{to_date}", p_Programming.UnlimitedProgramming == "S" ? "Ilimitado" : p_Programming.ToDate.ToString("dd/MM/yyyy"))
                    .Replace("{from_hour}", l_from_hour)
                    .Replace("{to_hour}", l_to_hour);
                //get persons
                var l_persons = l_list.RetrieveProgrammingPrivatePersonCheck(Convert.ToInt32(p_Programming.Id), dt_fichas);
                //send by person
                foreach (DataRow row in l_persons.Rows)
                {
                    l_html_content = l_template
                        .Replace("{fullname}", Convert.ToString(row["Nombre"]))
                        .Replace("{username}", Convert.ToString(row["Codi_User"]))
                        .Replace("{password}", Convert.ToString(row["Pass_User"]));
                    //send
                    var l_sgmessage = new SendGridMessage()
                    {
                        From = new EmailAddress(l_scheme[0]),
                        Subject = l_scheme[1],
                        HtmlContent = l_html_content
                    };
                    //destination
                    if (l_to_developers)
                    {
                        l_sgmessage.AddTos(GetDeveloperEmail());
                    }
                    else
                    {
                        l_sgmessage.AddTo(Convert.ToString(row["Email"]));
                    }
                    //
                    //SendEmailAsync(l_send_grid, l_sgmessage, l_scheme[7], l_scheme[8]);

                    //Uso de la API para enviar correos
                    string correos = "";
                    if (l_to_developers)
                    {
                        foreach (EmailAddress l_mail in GetDeveloperEmail())
                        {
                            correos += l_mail.Email + ";";
                        }
                        correos = correos.Substring(0, correos.Length - 1);
                    }
                    else
                    {
                        correos = Convert.ToString(row["Email"]);
                    }
                    SendEmailSqlServer("pedro.reque@ex.gerdau.com", l_scheme[1], l_html_content, null);
                    //SendEmailSqlServer(correos, l_scheme[1], l_html_content, null);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                l_list = null;
            }
        }
        public static void SendNotificationRegisterPrivateCapacitacion(Programming_Entity p_Programming)
        {
            Bus_List l_list = null;
            SendGridClient l_send_grid = null;
            string l_fcor, l_template, l_html_content, l_course_name;
            string[] l_scheme;
            bool l_to_developers = ToDevelopers();
            try
            {
                l_list = new Bus_List();
                l_send_grid = new SendGridClient(GetSendGridKey());
                l_fcor = Helpers.GetParameterByKey("FORMATO_EMAIL_NOTIFICACION_REGISTRO_PRIVADO_CAPACITACION");
                l_scheme = GetEmailScheme(Convert.ToDecimal(l_fcor), l_list);
                l_template = l_scheme[6]
                    .Replace("{tag_style}", l_scheme[2])
                    .Replace("{tag_head}", l_scheme[3])
                    .Replace("{tag_body}", l_scheme[4])
                    .Replace("{tag_foot}", l_scheme[5]);
                //get course name
                var l_course = l_list.RetrieveCourse(Convert.ToInt32(p_Programming.CourseId));
                l_course_name = Convert.ToString(l_course.Rows[0]["Nombre"]);
                Helpers.ClearDataTable(l_course);
                //get final template
                l_template = l_template
                    .Replace("{course_name}", l_course_name)
                    .Replace("{from_date}", p_Programming.UnlimitedProgramming == "S" ? "Ilimitado" : p_Programming.FromDate.ToString("dd/MM/yyyy"))
                    .Replace("{to_date}", p_Programming.UnlimitedProgramming == "S" ? "Ilimitado" : p_Programming.ToDate.ToString("dd/MM/yyyy"));
                var l_persons = l_list.RetrieveProgrammingPrivatePerson(Convert.ToInt32(p_Programming.Id));
                //send by person
                foreach (DataRow row in l_persons.Rows)
                {
                    l_html_content = l_template
                        .Replace("{fullname}", Convert.ToString(row["Nombre"]))
                        .Replace("{username}", Convert.ToString(row["Codi_User"]))
                        .Replace("{password}", Convert.ToString(row["Pass_User"]));
                    //send
                    var l_sgmessage = new SendGridMessage()
                    {
                        From = new EmailAddress(l_scheme[0]),
                        Subject = l_scheme[1],
                        HtmlContent = l_html_content
                    };
                    //destination
                    if (l_to_developers)
                    {
                        l_sgmessage.AddTos(GetDeveloperEmail());
                    }
                    else
                    {
                        l_sgmessage.AddTo(Convert.ToString(row["Email"]));
                    }
                    //
                    //SendEmailAsync(l_send_grid, l_sgmessage, l_scheme[7], l_scheme[8]);

                    //Uso de la API para enviar correos
                    string correos = "";
                    if (l_to_developers)
                    {
                        foreach (EmailAddress l_mail in GetDeveloperEmail())
                        {
                            correos += l_mail.Email + ";";
                        }
                        correos = correos.Substring(0, correos.Length - 1);
                    }
                    else
                    {
                        correos = Convert.ToString(row["Email"]);
                    }
                    SendEmailSqlServer(correos, l_scheme[1], l_html_content, null);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                l_list = null;
            }
        }
        public static void SendNotificationRegisterPrivateCapacitacionCheck(Programming_Entity p_Programming, DataTable dt_fichas)
        {
            Bus_List l_list = null;
            SendGridClient l_send_grid = null;
            string l_fcor, l_template, l_html_content, l_course_name;
            string[] l_scheme;
            bool l_to_developers = ToDevelopers();
            try
            {
                l_list = new Bus_List();
                l_send_grid = new SendGridClient(GetSendGridKey());
                l_fcor = Helpers.GetParameterByKey("FORMATO_EMAIL_NOTIFICACION_REGISTRO_PRIVADO_CAPACITACION");
                l_scheme = GetEmailScheme(Convert.ToDecimal(l_fcor), l_list);
                l_template = l_scheme[6]
                    .Replace("{tag_style}", l_scheme[2])
                    .Replace("{tag_head}", l_scheme[3])
                    .Replace("{tag_body}", l_scheme[4])
                    .Replace("{tag_foot}", l_scheme[5]);
                //get course name
                var l_course = l_list.RetrieveCourse(Convert.ToInt32(p_Programming.CourseId));
                l_course_name = Convert.ToString(l_course.Rows[0]["Nombre"]);
                Helpers.ClearDataTable(l_course);
                //get final template
                l_template = l_template
                    .Replace("{course_name}", l_course_name)
                    .Replace("{from_date}", p_Programming.UnlimitedProgramming == "S" ? "Ilimitado" : p_Programming.FromDate.ToString("dd/MM/yyyy"))
                    .Replace("{to_date}", p_Programming.UnlimitedProgramming == "S" ? "Ilimitado" : p_Programming.ToDate.ToString("dd/MM/yyyy"));
                var l_persons = l_list.RetrieveProgrammingPrivatePersonCheck(Convert.ToInt32(p_Programming.Id), dt_fichas);
                //send by person
                foreach (DataRow row in l_persons.Rows)
                {
                    l_html_content = l_template
                        .Replace("{fullname}", Convert.ToString(row["Nombre"]))
                        .Replace("{username}", Convert.ToString(row["Codi_User"]))
                        .Replace("{password}", Convert.ToString(row["Pass_User"]));
                    //send
                    var l_sgmessage = new SendGridMessage()
                    {
                        From = new EmailAddress(l_scheme[0]),
                        Subject = l_scheme[1],
                        HtmlContent = l_html_content
                    };
                    //destination
                    if (l_to_developers)
                    {
                        l_sgmessage.AddTos(GetDeveloperEmail());
                    }
                    else
                    {
                        l_sgmessage.AddTo(Convert.ToString(row["Email"]));
                    }
                    //
                    //SendEmailAsync(l_send_grid, l_sgmessage, l_scheme[7], l_scheme[8]);

                    //Uso de la API para enviar correos
                    string correos = "";
                    if (l_to_developers)
                    {
                        foreach (EmailAddress l_mail in GetDeveloperEmail())
                        {
                            correos += l_mail.Email + ";";
                        }
                        correos = correos.Substring(0, correos.Length - 1);
                    }
                    else
                    {
                        correos = Convert.ToString(row["Email"]);
                    }
                    //SendEmailSqlServer("pedro.reque@ex.gerdau.com", l_scheme[1], l_html_content, null);
                    SendEmailSqlServer(correos, l_scheme[1], l_html_content, null);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                l_list = null;
            }
        }
        public static void SendCredentialsAfterVerifyYourEmail(string p_Username, string p_Password, string p_Email)
        {
            Bus_List l_list = null;
            SendGridClient l_send_grid = null;
            string l_fcor, l_template, l_html_content, l_email = p_Email == null ? "" : p_Email;
            string[] l_scheme;
            try
            {
                l_list = new Bus_List();
                l_send_grid = new SendGridClient(GetSendGridKey());
                l_fcor = Helpers.GetParameterByKey("FORMATO_EMAIL_VERIFICA_TU_EMAIL_CREDENCIALES");
                l_scheme = GetEmailScheme(Convert.ToDecimal(l_fcor), l_list);
                l_template = l_scheme[6]
                    .Replace("{tag_style}", l_scheme[2])
                    .Replace("{tag_head}", l_scheme[3])
                    .Replace("{tag_body}", l_scheme[4])
                    .Replace("{tag_foot}", l_scheme[5]);
                l_html_content = l_template
                    .Replace("{username}", p_Username)
                    .Replace("{password}", p_Password);
                //send
                var l_sgmessage = new SendGridMessage()
                {
                    From = new EmailAddress(l_scheme[0]),
                    Subject = l_scheme[1],
                    HtmlContent = l_html_content
                };
                //destination
                if (ToDevelopers())
                {
                    l_sgmessage.AddTos(GetDeveloperEmail());
                }
                else
                {
                    l_sgmessage.AddTo(l_email);
                }
                //
                //SendEmailAsync(l_send_grid, l_sgmessage, l_scheme[7], l_scheme[8]);

                //Uso de la API para enviar correos
                string correos = "";
                if (ToDevelopers())
                {
                    foreach (EmailAddress l_mail in GetDeveloperEmail())
                    {
                        correos += l_mail.Email + ";";
                    }
                    correos = correos.Substring(0, correos.Length - 1);
                }
                else
                {
                    correos = Convert.ToString(l_email);
                }
                SendEmailSqlServer(correos, l_scheme[1], l_html_content, null);
            }
            catch
            {
                throw;
            }
            finally
            {
                l_list = null;
            }
        }
        public static void SendProviderDataFromProfileMaintenance(Profiles_Entity p_Entity)
        {
            Bus_List l_list = null;
            SendGridClient l_send_grid = null;
            string l_fcor, l_template, l_html_content;
            string[] l_scheme;
            bool l_to_developers = ToDevelopers();
            try
            {
                l_list = new Bus_List();
                l_send_grid = new SendGridClient(GetSendGridKey());
                l_fcor = Helpers.GetParameterByKey("FORMATO_EMAIL_PROVEEDOR_EMAIL_EDITAR_ENVIAR");
                l_scheme = GetEmailScheme(Convert.ToDecimal(l_fcor), l_list);
                l_template = l_scheme[6]
                    .Replace("{tag_style}", l_scheme[2])
                    .Replace("{tag_head}", l_scheme[3])
                    .Replace("{tag_body}", l_scheme[4])
                    .Replace("{tag_foot}", l_scheme[5]);
                //send by provider
                for (int i = 0; i < p_Entity.ProviderId.Length; i++)
                {
                    if (p_Entity.ProviderId[i] != 0)
                    {
                        l_html_content = l_template
                            .Replace("{fullname}", p_Entity.ProviderBusinessName[i])
                            .Replace("{username}", p_Entity.ProviderUsername[i])
                            .Replace("{password}", p_Entity.ProviderPassword[i]);
                        //send
                        var l_sgmessage = new SendGridMessage()
                        {
                            From = new EmailAddress(l_scheme[0]),
                            Subject = l_scheme[1],
                            HtmlContent = l_html_content
                        };
                        //destination
                        if (l_to_developers)
                        {
                            l_sgmessage.AddTos(GetDeveloperEmail());
                        }
                        else
                        {
                            l_sgmessage.AddTo(p_Entity.ProviderEmail[i]);
                        }
                        //
                        //SendEmailAsync(l_send_grid, l_sgmessage, l_scheme[7], l_scheme[8]);

                        //Uso de la API para enviar correos
                        string correos = "";
                        if (l_to_developers)
                        {
                            foreach (EmailAddress l_mail in GetDeveloperEmail())
                            {
                                correos += l_mail.Email + ";";
                            }
                            correos = correos.Substring(0, correos.Length - 1);
                        }
                        else
                        {
                            correos = Convert.ToString(p_Entity.ProviderEmail[i]);
                        }
                        SendEmailSqlServer(correos, l_scheme[1], l_html_content, null);
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                l_list = null;
            }
        }
        public static void SendInformationRegisterIndividual(string p_Schedule)
        {
            Bus_List l_list = null;
            SendGridClient l_send_grid = null;
            string l_fcor, l_template, l_html_content, l_email;
            string[] l_scheme;
            try
            {
                l_list = new Bus_List();
                l_send_grid = new SendGridClient(GetSendGridKey());
                l_fcor = Helpers.GetParameterByKey("FORMATO_EMAIL_INFORMACION_REGISTRO_INDIVIDUAL");
                l_scheme = GetEmailScheme(Convert.ToDecimal(l_fcor), l_list);
                l_template = l_scheme[6]
                    .Replace("{tag_style}", l_scheme[2])
                    .Replace("{tag_head}", l_scheme[3])
                    .Replace("{tag_body}", l_scheme[4])
                    .Replace("{tag_foot}", l_scheme[5]);
                //get email
                l_email = l_list.GetPersonEmail(Convert.ToInt32(SessionHelpers.GetUser().PersonId), "P");
                //get subject final
                l_scheme[1] = l_scheme[1].Replace("{schedule}", p_Schedule);
                //get content
                l_html_content = l_template
                    .Replace("{fullname}", SessionHelpers.GetUser().FullName)
                    .Replace("{schedule}", p_Schedule);
                //send
                var l_sgmessage = new SendGridMessage()
                {
                    From = new EmailAddress(l_scheme[0]),
                    Subject = l_scheme[1],
                    HtmlContent = l_html_content
                };
                //destination
                if (ToDevelopers())
                {
                    l_sgmessage.AddTos(GetDeveloperEmail());
                }
                else
                {
                    l_sgmessage.AddTo(l_email);
                }
                //
                //SendEmailAsync(l_send_grid, l_sgmessage, l_scheme[7], l_scheme[8]);

                //Uso de la API para enviar correos
                string correos = "";
                if (ToDevelopers())
                {
                    foreach (EmailAddress l_mail in GetDeveloperEmail())
                    {
                        correos += l_mail.Email + ";";
                    }
                    correos = correos.Substring(0, correos.Length - 1);
                }
                else
                {
                    correos = Convert.ToString(l_email);
                }
                SendEmailSqlServer(correos, l_scheme[1], l_html_content, null);
            }
            catch
            {
                throw;
            }
            finally
            {
                l_list = null;
            }
        }
        public static void SendInformationRegisterMassive(DataTable p_Response, string p_ScheduleDate, string p_ScheduleTime, int p_Programming)
        {
            Bus_List l_list = null;
            SendGridClient l_send_grid = null;
            string l_fcor, l_template, l_html_content, l_html_tbody, l_provider_email, l_course_name, l_note_style;
            string[] l_scheme;
            try
            {
                l_list = new Bus_List();
                l_send_grid = new SendGridClient(GetSendGridKey());
                l_fcor = Helpers.GetParameterByKey("FORMATO_EMAIL_INFORMACION_REGISTRO_MASIVO");
                l_scheme = GetEmailScheme(Convert.ToDecimal(l_fcor), l_list);
                l_template = l_scheme[6]
                    .Replace("{tag_style}", l_scheme[2])
                    .Replace("{tag_head}", l_scheme[3])
                    .Replace("{tag_body}", l_scheme[4])
                    .Replace("{tag_foot}", l_scheme[5]);
                //get programming
                var l_programming = l_list.RetrieveProgramming(p_Programming);
                l_course_name = l_list.GetCourseNameByProgramming(p_Programming);
                l_note_style = Convert.ToString(l_programming.Rows[0]["Codi_Moda"]) == Helpers.GetParameterByKey("CODI_MODA_PRES") ? "block" : "none";
                Helpers.ClearDataTable(l_programming);
                //get provider email
                l_provider_email = l_list.GetProviderEmail(Convert.ToInt32(SessionHelpers.GetUser().ProviderId));
                //get subject final
                l_scheme[1] = l_scheme[1].Replace("{schedule_date}", p_ScheduleDate).Replace("{schedule_time}", p_ScheduleTime);
                //build list
                l_html_tbody = "";
                foreach (DataRow row in p_Response.Rows)
                {
                    l_html_tbody += "<tr><td class='e-text-left'>" + Convert.ToString(row["Nombre"]) + "</td><td class='e-text-center'>" + Convert.ToString(row["Codi_User"]) + "</td><td class='e-text-center'>" + Convert.ToString(row["Pass_User"]) + "</td></tr>";
                }
                l_html_content = l_template
                    .Replace("{fullname}", SessionHelpers.GetUser().FullName)
                    .Replace("{schedule_date}", p_ScheduleDate)
                    .Replace("{schedule_time}", p_ScheduleTime)
                    .Replace("{list}", l_html_tbody)
                    .Replace("{course_name}", l_course_name)
                    .Replace("{note_style}", l_note_style);
                //send
                var l_sgmessage = new SendGridMessage()
                {
                    From = new EmailAddress(l_scheme[0]),
                    Subject = l_scheme[1],
                    HtmlContent = l_html_content
                };
                //destination
                if (ToDevelopers())
                {
                    l_sgmessage.AddTos(GetDeveloperEmail());
                }
                else
                {
                    l_sgmessage.AddTo(l_provider_email);
                }
                //
                //SendEmailAsync(l_send_grid, l_sgmessage, l_scheme[7], l_scheme[8]);

                //Uso de la API para enviar correos
                string correos = "";
                if (ToDevelopers())
                {
                    foreach (EmailAddress l_mail in GetDeveloperEmail())
                    {
                        correos += l_mail.Email + ";";
                    }
                    correos = correos.Substring(0, correos.Length - 1);
                }
                else
                {
                    correos = Convert.ToString(l_provider_email);
                }
                SendEmailSqlServer(correos, l_scheme[1], l_html_content, null);
            }
            catch
            {
                throw;
            }
            finally
            {
                l_list = null;
            }
        }
        public static void SendVerifyYourEmail(Register_Entity p_Entity)
        {
            Bus_List l_list = null;
            SendGridClient l_send_grid = null;
            string l_fcor, l_template, l_html_content;
            string[] l_scheme;
            try
            {
                l_list = new Bus_List();
                l_send_grid = new SendGridClient(GetSendGridKey());
                l_fcor = Helpers.GetParameterByKey("FORMATO_EMAIL_VERIFICA_TU_EMAIL");
                l_scheme = GetEmailScheme(Convert.ToDecimal(l_fcor), l_list);
                l_template = l_scheme[6]
                    .Replace("{tag_style}", l_scheme[2])
                    .Replace("{tag_head}", l_scheme[3])
                    .Replace("{tag_body}", l_scheme[4])
                    .Replace("{tag_foot}", l_scheme[5]);
                l_html_content = l_template
                    .Replace("{fullname}", String.Concat(p_Entity.FirstName, ' ', p_Entity.LastName1, ' ', p_Entity.LastName2))
                    .Replace("{code}", EncryptorHelpers.Encrypt(p_Entity.VerificationCode));
                //send
                var l_sgmessage = new SendGridMessage()
                {
                    From = new EmailAddress(l_scheme[0]),
                    Subject = l_scheme[1],
                    HtmlContent = l_html_content
                };
                //destination
                if (ToDevelopers())
                {
                    l_sgmessage.AddTos(GetDeveloperEmail());
                }
                else
                {
                    l_sgmessage.AddTo(p_Entity.Email);
                }
                //
                //SendEmailAsync(l_send_grid, l_sgmessage, l_scheme[7], l_scheme[8]);

                //Uso de la API para enviar correos
                string correos = "";
                if (ToDevelopers())
                {
                    foreach (EmailAddress l_mail in GetDeveloperEmail())
                    {
                        correos += l_mail.Email + ";";
                    }
                    correos = correos.Substring(0, correos.Length - 1);
                }
                else
                {
                    correos = Convert.ToString(p_Entity.Email);
                }
                SendEmailSqlServer(correos, l_scheme[1], l_html_content, null);
            }
            catch
            {
                throw;
            }
            finally
            {
                l_list = null;
            }
        }

        public static void SendCredentials(Person_Model p_Person)
        {
            Bus_List l_list = null;
            SendGridClient l_send_grid = null;
            string l_fcor, l_template, l_html_content, l_email;
            string[] l_scheme;
            try
            {
                l_list = new Bus_List();
                l_send_grid = new SendGridClient(GetSendGridKey());
                l_fcor = Helpers.GetParameterByKey("FORMATO_EMAIL_RE_ENVIO_CREDENCIALES");
                l_scheme = GetEmailScheme(Convert.ToDecimal(l_fcor), l_list);
                l_template = l_scheme[6]
                    .Replace("{tag_style}", l_scheme[2])
                    .Replace("{tag_head}", l_scheme[3])
                    .Replace("{tag_body}", l_scheme[4])
                    .Replace("{tag_foot}", l_scheme[5]);

                l_email = p_Person.Email;

                l_html_content = l_template
                    .Replace("{fullname}", p_Person.Fullname)
                    .Replace("{username}", p_Person.Username)
                    .Replace("{password}", p_Person.Password);
                //send
                var l_sgmessage = new SendGridMessage()
                {
                    From = new EmailAddress(l_scheme[0]),
                    Subject = l_scheme[1],
                    HtmlContent = l_html_content
                };
                //destination
                if (ToDevelopers())
                {
                    l_sgmessage.AddTos(GetDeveloperEmail());
                }
                else
                {
                    l_sgmessage.AddTo(l_email);
                }
                //
                //SendEmailAsync(l_send_grid, l_sgmessage, l_scheme[7], l_scheme[8]);

                //Uso de la API para enviar correos
                string correos = "";
                if (ToDevelopers())
                {
                    foreach (EmailAddress l_mail in GetDeveloperEmail())
                    {
                        correos += l_mail.Email + ";";
                    }
                    correos = correos.Substring(0, correos.Length - 1);
                }
                else
                {
                    correos = Convert.ToString(l_email);
                }
                SendEmailSqlServer(correos, l_scheme[1], l_html_content, null);
            }
            catch
            {
                throw;
            }
            finally
            {
                l_list = null;
            }
        }

        #region private
        private static void SendEmailAsync(SendGridClient p_sg, SendGridMessage p_sgmessage, string p_ccs = "", string p_bccs = "")
        {
            try
            {
                var l_ccs = GetCcsBccsEmail(p_ccs);
                var l_bccs = GetCcsBccsEmail(p_bccs);
                if (l_ccs != null)
                {
                    p_sgmessage.AddCcs(l_ccs);
                }
                if (l_bccs != null)
                {
                    p_sgmessage.AddBccs(l_bccs);
                }
                p_sgmessage.AddHeader("Priority", "Urgent");
                p_sgmessage.AddHeader("Importance", "high");
                p_sg.SendEmailAsync(p_sgmessage);
            }
            catch (Exception)
            {
                throw;
            }
        }
        private static bool ToDevelopers()
        {
            return Helpers.GetParameterByKey("ENVIAR_EMAIL_A_DESARROLLADORES") == "S";
        }
        private static List<EmailAddress> GetDeveloperEmail()
        {
            var l_list = Helpers.GetParameterByKey("EMAIL_DESARROLLO").Split(';');
            var l_return = new List<EmailAddress>();
            for (var i = 0; i < l_list.Length; i++)
            {
                if (l_list[i].Trim() != "")
                {
                    l_return.Add(new EmailAddress(l_list[i]));
                }
            }
            return l_return;
        }
        private static List<EmailAddress> GetErrorEmail()
        {
            var l_list = Helpers.GetParameterByKey("EMAIL_ERROR").Split(';');
            var l_return = new List<EmailAddress>();
            for (var i = 0; i < l_list.Length; i++)
            {
                if (l_list[i].Trim() != "")
                {
                    l_return.Add(new EmailAddress(l_list[i]));
                }
            }
            return l_return;
        }
        private static List<EmailAddress> GetCcsBccsEmail(string p_list)
        {
            var l_list = p_list.Split(';');
            var l_return = new List<EmailAddress>();
            for (var i = 0; i < l_list.Length; i++)
            {
                if (l_list[i].Trim() != "")
                {
                    l_return.Add(new EmailAddress(l_list[i]));
                }
            }
            return l_return.Count == 0 ? null : l_return;
        }
        private static string GetSendGridKey()
        {
            return Helpers.GetAppSettings("SendGrid_ApiKey");
        }
        private static string[] GetEmailScheme(decimal p_fcor, Bus_List p_Bus, string p_connection = "ELearning")
        {
            DataTable l_format = null;
            string[] l_scheme = new string[9];
            try
            {
                l_format = p_Bus.GetEmailFormatById(p_fcor, p_connection);
                l_scheme[0] = Convert.IsDBNull(l_format.Rows[0]["From"]) ? "" : l_format.Rows[0]["From"].ToString();//From
                l_scheme[1] = Convert.IsDBNull(l_format.Rows[0]["Subject"]) ? "" : l_format.Rows[0]["Subject"].ToString();//Subject
                l_scheme[2] = Convert.IsDBNull(l_format.Rows[0]["Style"]) ? "" : l_format.Rows[0]["Style"].ToString();//Style
                l_scheme[3] = Convert.IsDBNull(l_format.Rows[0]["Head"]) ? "" : l_format.Rows[0]["Head"].ToString();//Head
                l_scheme[4] = Convert.IsDBNull(l_format.Rows[0]["Body"]) ? "" : l_format.Rows[0]["Body"].ToString();//Body
                l_scheme[5] = Convert.IsDBNull(l_format.Rows[0]["Foot"]) ? "" : l_format.Rows[0]["Foot"].ToString();//Foot
                l_scheme[6] = Convert.IsDBNull(l_format.Rows[0]["Scheme"]) ? "" : l_format.Rows[0]["Scheme"].ToString();//Scheme
                l_scheme[7] = l_format.Columns.Contains("Cc") ? Convert.IsDBNull(l_format.Rows[0]["Cc"]) ? "" : l_format.Rows[0]["Cc"].ToString() : "";//Cc
                l_scheme[8] = l_format.Columns.Contains("Bcc") ? Convert.IsDBNull(l_format.Rows[0]["Bcc"]) ? "" : l_format.Rows[0]["Bcc"].ToString() : "";//Bcc
                return l_scheme;
            }
            catch
            {
                throw new Exception("Error en 'GetEmailScheme'");
            }
            finally
            {
                Helpers.ClearDataTable(l_format);
            }
        }
        #endregion private
    }
}