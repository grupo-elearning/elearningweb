﻿namespace ElearningApp.Models.Entity
{
    public class CertificateTemplate
    {
        public string sId { get; set; }
        public CertificateTemplateImage oImage { get; set; }
        public CertificateTemplateSize oSize { get; set; }
        public string sOrientation { get; set; }
        public string sPageFormat { get; set; }
        public CertificateTemplateChildren[] oChildren { get; set; }
    }
    public class CertificateTemplateImage
    {
        public string sBase64 { get; set; }
        public string oFile { get; set; }
    }
    public class CertificateTemplateSize
    {
        public bool bFullWidth { get; set; }
        public decimal fWidthPt { get; set; }
        public decimal fHeightPt { get; set; }
    }
    public class CertificateTemplateChildren
    {
        public string sId { get; set; }
        public string sType { get; set; }
        public CertificateTemplateData oData { get; set; }
        public bool bDrawn { get; set; }
        public bool bResize { get; set; }
        public CertificateTemplateFont oFont { get; set; }
        public CertificateTemplateText oText { get; set; }
        public CertificateTemplateImage oImage { get; set; }
        public CertificateTemplatePosition oPosition { get; set; }
        public CertificateTemplateSize oSize { get; set; }
    }
    public class CertificateTemplateData
    {
        public string sKey { get; set; }
        public string sDateFormat { get; set; }
    }
    public class CertificateTemplateFont
    {
        public string sFamily { get; set; }
        public string sWebFamily { get; set; }
        public string sStyle { get; set; }
        public decimal iSizePt { get; set; }
        public string sColor { get; set; }
    }
    public class CertificateTemplateText
    {
        public string sText { get; set; }
    }
    public class CertificateTemplatePosition
    {
        public decimal fLeftPt { get; set; }
        public decimal fTopPt { get; set; }
    }
}