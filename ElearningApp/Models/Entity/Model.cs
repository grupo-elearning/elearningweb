﻿using System;
using System.Collections.Generic;
using System.Data;

namespace ElearningApp.Models.Entity
{
    public class Dashboard5ExecutedExport_Model
    {
        public string[] Area { get; set; }
        public decimal[] Staff { get; set; }
        public decimal[] Advance { get; set; }
        public decimal[] Executed { get; set; }
        public decimal AVGProgrammed { get; set; }
        public decimal AVGCompleted { get; set; }
        public decimal AVGExecuted { get; set; }
    }
    public class Person_Model
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class Dashboard4_Model
    {
        public string Theme { get; set; }
        public string Export { get; set; }
        public string Type { get; set; }
        public decimal[] Pid { get; set; }
        public string[] Ruc { get; set; }
        public string[] Pro { get; set; }
        public string[] Pdo { get; set; }
        public string[] Pds { get; set; }
        public decimal[] Eid { get; set; }
        public string[] End { get; set; }
        public string[] Emp { get; set; }
        public string[] Efi { get; set; }
        public string[] Edo { get; set; }
        public string[] Eds { get; set; }
        public decimal[] Vid { get; set; }
        public string[] Veh { get; set; }
        public string[] Vdo { get; set; }
        public string[] Vds { get; set; }
    }
    public class ProviderDocumentLogistica_Model
    {
        public decimal CodiTdos { get; set; }
        public string Descripcion { get; set; }
    }
    public class HomeIndex_Model
    {
        public bool FromLogistica { get; set; }
        public bool IsPerson { get; set; }
        public bool IsProvider { get; set; }
        public IEnumerable<MyCourses_Model> Courses { get; set; }
        public IEnumerable<Notice_Entity> Notices { get; set; }
    }
    public class ProviderDetail_Model
    {
        public Provider_Entity Provider { get; set; }
        public IEnumerable<ProviderDocument_Model> Documents { get; set; }
        public IEnumerable<ProviderDocumentLogistica_Model> LogisticaDocuments { get; set; }
        public List<object> Employees { get; set; }
        public List<object> EmployeesDocuments { get; set; }
        public IEnumerable<ProviderDocumentLogistica_Model> LogisticaEmployeesDocuments { get; set; }
        public List<object> Vehicles { get; set; }
        public List<object> VehiclesDocuments { get; set; }
        public IEnumerable<ProviderDocumentLogistica_Model> LogisticaVehiclesDocuments { get; set; }
        public List<object> EmployeesTrained { get; set; }
    }
    public class ProviderToApprove_Model
    {
        public Provider_Entity Provider { get; set; }
        public IEnumerable<ProviderDocument_Model> Documents { get; set; }
        public List<object> Employees { get; set; }
        public List<object> EmployeesDocuments { get; set; }
        public List<object> Vehicles { get; set; }
        public List<object> VehiclesDocuments { get; set; }
    }
    public class ProviderVehicle_Model
    {
        public Provider_Entity Provider { get; set; }
        public List<object> Vehicles { get; set; }
        public List<object> Documents { get; set; }
        public IEnumerable<ProviderDocumentLogistica_Model> LogisticaDocuments { get; set; }
    }
    public class ProviderEmployee_Model
    {
        public Provider_Entity Provider { get; set; }
        public List<object> Employees { get; set; }
        public List<object> Documents { get; set; }
        public IEnumerable<ProviderDocumentLogistica_Model> LogisticaDocuments { get; set; }
        public List<object> EmployeesTrained { get; set; }
    }
    public class ProviderDocument_Model
    {
        public decimal CodiTdos { get; set; }
        public string Descripcion { get; set; }
        public string DescripcionCorta { get; set; }
        public string Tipo { get; set; }
        public string FlagFecha { get; set; }
        public string TipoFoto { get; set; }
        public string Estado { get; set; }
        public string Nombre { get; set; }
        public string NombreReal { get; set; }
        public string FechaEmision { get; set; }
        public string FechaVencimiento { get; set; }
        public decimal CodiPrdo { get; set; }
        public string EstadoDoc { get; set; }
        public string Area { get; set; }
        public string Semaforo { get; set; }
        public string Url { get; set; }
        public string DescripcionRechazo { get; set; }
        public string Modelo { get; set; }
        public string ModeloUrl { get; set; }
    }
    public class Provider_Model
    {
        public string Filter_Type { get; set; }
        public string Filter_RUC { get; set; }
        public string Status { get; set; }
        public string Mode { get; set; }
        public decimal ProfileId { get; set; }
        public decimal ProviderId { get; set; }
        public string Type { get; set; }
        public decimal DocumentType { get; set; }
        public string DocumentNumber { get; set; }
        public string ProviderRUC { get; set; }
        public decimal DocumentId { get; set; }
        public decimal[] EmployeeId { get; set; }
        public DataTable DtEmployee { get; set; }
        public decimal[] VehicleId { get; set; }
        public DataTable DtVehicle { get; set; }
        public List<object> Employees { get; set; }
        public List<object> Vehicles { get; set; }
        public IEnumerable<ProviderDocument_Model> Documents { get; set; }
        public IEnumerable<ProviderDocumentLogistica_Model> LogisticaDocuments { get; set; }
    }
    public class ClassroomPoll_Model
    {
        public decimal RegisterId { get; set; }
        public decimal Id { get; set; }
        public string Source { get; set; }
        public string HasComment { get; set; }
        public string CommentQuestion { get; set; }
        public string Comment { get; set; }
        public DateTime Date { get; set; }
        public List<object> Questions { get; set; }
    }
    public class Dashboard1RegisteredDetail_Model
    {
        public decimal RegisterId { get; set; }
        public string PersonLastName1 { get; set; }
        public string PersonLastName2 { get; set; }
        public string PersonFirstName { get; set; }
        public string PersonEmail { get; set; }
        public string PersonFichaSap { get; set; }
        public string PersonType { get; set; }
        public string PersonPhoto { get; set; }
        public string ProviderSAPCode { get; set; }
        public string ProviderRuc { get; set; }
        public string ProviderBusinessName { get; set; }
        public string ProviderEmail { get; set; }
        public int TotalAttempts { get; set; }
        public decimal ApproveScore { get; set; }
        public bool InTest { get; set; }
        public List<object> Attempts { get; set; }
    }
    public class AttemptFinalized_Model
    {
        public string MessageFinalized { get; set; }
        public decimal Score { get; set; }
        public bool IsApproved { get; set; }
        public bool IsCarrier { get; set; }
        public bool FromCarrierLogin { get; set; }
        public bool HasCertificate { get; set; }
        public string CarrierProtocol { get; set; }
        public Test_Entity Test { get; set; }
        public Parameter_Entity Parameter { get; set; }
        public List<object> Questions { get; set; }

        public Capac_Notas ApiData { get; set; }
    }
    public class Profiles_Model
    {
        public decimal AppProfileId { get; set; }
        public string ItemIds { get; set; }
        public DataTable DtItemIds { get; set; }
    }
    public class CarrierPrograms_Model
    {
        public decimal ProgrammingId { get; set; }
        public string Name { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string FromHour { get; set; }
        public string ToHour { get; set; }
    }
    public class MyCourses_Model
    {
        public decimal ProgrammingId { get; set; }
        public string CourseName { get; set; }
        public string CourseObjetives { get; set; }
        public string CourseSummary { get; set; }
        public string CourseFrontPage { get; set; }
        public string CourseImageUrl { get; set; }
        public string CourseCategory { get; set; }
        public string Mode { get; set; }
        public string Instructor { get; set; }
        public string HasTest { get; set; }
        public string Place { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string FromHour { get; set; }
        public string ToHour { get; set; }
        public string Weekend { get; set; }
        public string AccessType { get; set; }
        public string ApprovalType { get; set; }
        public string MinimumAssistance { get; set; }
        public decimal Vacancies { get; set; }
        public string RegistrationLimit { get; set; }
        public decimal ToRegister { get; set; }
        public decimal RegistrationNumbers { get; set; }
        public string FilterClass { get; set; }
        public string RegisterMode { get; set; }
        public decimal Registered { get; set; }
        public decimal Initiated { get; set; }
        public string ShowContent { get; set; }
        public string HasCertificate { get; set; }
        public string HasPoll { get; set; }
    }
    public class AreaProvider_Model
    {
        public string ProviderIds { get; set; }
        public DataTable DtProviderIds { get; set; }
    }
    public class Course_Model
    {
        public string Classification { get; set; }
    }
    public class ProgrammingClassroom_Model
    {
        public decimal AttemptId { get; set; }
        public decimal RegisterId { get; set; }
        public decimal ProgrammingId { get; set; }
        public decimal TestId { get; set; }
        public decimal PollId { get; set; }
        public bool IsCarrier { get; set; }
        public bool FromCarrierLogin { get; set; }
        public bool IsApproved { get; set; }
        public bool HasCertificate { get; set; }
        public decimal CurrentThemeItem { get; set; }
        public Int64 SecondsRemaining { get; set; }
        public string ErrorMessage { get; set; }
        public int ErrorCode { get; set; }
        public Course_Entity Course { get; set; }
        public Test_Entity Test { get; set; }
        public List<object> Details { get; set; }
    }
    public class ProgrammingPresentation_Model
    {
        public decimal ProgrammingId { get; set; }
        public bool Registered { get; set; }
        public decimal ModeElearning { get; set; }
        public decimal ModeInPerson { get; set; }
        public bool Register { get; set; }
        public string NoRegisterMessage { get; set; }
        public bool Initiated { get; set; }
        public string InitiatedMessage { get; set; }
        public string StorageUrl { get; set; }
        public Course_Entity Course { get; set; }
        public Programming_Entity Programming { get; set; }
    }
    public class Home_Model
    {
        public string LoggedIn { get; set; }
        public decimal PersonId { get; set; }
        public decimal ProviderId { get; set; }
        public decimal ProfileId { get; set; }
    }
    public class Register_Model
    {
        public decimal DocumentType { get; set; }
        public string DocumentNumber { get; set; }
    }
    public class Test_Model
    {
        public decimal CourseId { get; set; }
        public string QuestionIds { get; set; }
        public decimal ProfileId { get; set; }
        public DataTable DtQuestionIds { get; set; }
    }
    public class Programming_Model
    {
        public decimal CertificateId { get; set; }
        public decimal PollId { get; set; }
        public decimal TestId { get; set; }
        public decimal AreaId { get; set; }
        public string AreaIds { get; set; }
        public decimal InstructorId { get; set; }
        public string PersonIds { get; set; }
        public string PersonType { get; set; }
        public DataTable DtPersonIds { get; set; }
        public DataTable DtAreaIds { get; set; }
    }

    public class ProviderLog {
        public int CodiProv { get; set; }
        public string FlagLogistica { get; set; }
    }
    public class UpdProvider_Model {
        public List<ProviderLog> Proveedores { get; set; }
        public DataTable DtProveedores { get; set; }
    }

    public class Response_Validar_Covid
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public List<Model_Response_Covid> Body { get; set; }
    }

    public class Model_Response_Covid
    {
        public String Existe { get; set; }
        public String Dni { get; set; }
        public Int64 Codi_Prov { get; set; }
    }

    public class UpdPersona
    {
        public decimal CodiProv { get; set; }
        public string NroDoc { get; set; }
        public decimal UserId { get; set; }
    }

    public class Capac_Notas
    {
        public string NroDoc { get; set; }
        public string Fecha { get; set; }
        public decimal Nota { get; set; }
        public string Estado { get; set; }
        public string ApePaterno { get; set; }
        public string ApeMaterno { get; set; }
        public string Nombres { get; set; }
        public string Licencia { get; set; }
    }

    public class ResponseApi
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public int Codigo { get; set; }
        public int UserId { get; set; }
    }

    public class PersonView_Model
    {
        public int tipoDoc { get; set; }
        public string nroDoc { get; set; }
        public string codiPers { get; set; }
    }
    #region EleaLmsApi
    public class RequestModel_Auth
    {
        public string Username { get; set; }
        public string Password { get; set; }

    }
    #endregion EleaLmsApi
    /* Codigo de Aplicacion - No manipular - */
    #region App
    public class Account_Model
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public decimal ProfileId { get; set; }
        public decimal AppId { get; set; }
        public string IPAddress { get; set; }
        public bool RememberMe { get; set; }
        public string Browser { get; set; }
    }
    public class AppObject_Model
    {
        public int AppId { get; set; }
        public int ProfileId { get; set; }
        public string UserId { get; set; }
        public int ParentId { get; set; }
    }

    public class ValEmail_Model
    {
        public int AppId { get; set; }
        public string Email { get; set; }
    }
    #endregion App
}