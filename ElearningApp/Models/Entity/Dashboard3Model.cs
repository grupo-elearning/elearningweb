﻿using System.Collections.Generic;

namespace ElearningApp.Models.Entity
{
    public class Dashboard3ParticipantModel
    {
        public List<Dashboard3ParticipantAttemptModel> Attempts { get; set; }
    }
    public class Dashboard3ParticipantAttemptModel
    {
        public int Codi_Exin { get; set; }
        public decimal Intento { get; set; }
        public decimal Nota { get; set; }
        public string Flag_Aprobado { get; set; }
        public List<Dashboard3ParticipantQuestionModel> Questions { get; set; }
    }
    public class Dashboard3ParticipantQuestionModel
    {
        public string Pregunta { get; set; }
        public string Flag_Contestado { get; set; }
        public string Flag_Actual { get; set; }
        public string Tipo_Pregunta { get; set; }
        public string Estado { get; set; }
        public List<Dashboard3ParticipantAnswerModel> Answers { get; set; }
    }
    public class Dashboard3ParticipantAnswerModel
    {
        public string Respuesta { get; set; }
        public string Correcta { get; set; }
        public decimal Orden { get; set; }
        public string Seleccionado { get; set; }
        public decimal Orden_Seleccionado { get; set; }
    }
}