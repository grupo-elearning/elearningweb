﻿using System.Collections.Generic;

namespace ElearningApp.Models.Entity
{
    public class Dashboard4DataSource
    {
        public decimal Codi_Tdoc_Prov { get; set; }
        public string Descripcion_Doc_Proveedor { get; set; }
        public decimal Estado_Doc_Prov { get; set; }
        public decimal Codi_Pcon { get; set; }
        public string Nro_Doc { get; set; }
        public string Trabajador { get; set; }
        public string Fecha_Ingreso { get; set; }
        public decimal Codi_Tdoc_Trab { get; set; }
        public string Descripcion_Doc_Trabajador { get; set; }
        public decimal Estado_Doc_Trab { get; set; }
        public decimal Codi_Prun { get; set; }
        public string Unidad { get; set; }
        public decimal Codi_Tdoc_Unidad { get; set; }
        public string Descripcion_Doc_Unidad { get; set; }
        public decimal Estado_Doc_Unid { get; set; }
    }
    public class Dashboard4ProviderModel
    {
        public int Id { get; set; }
        public string Ru { get; set; }
        public string Bn { get; set; }
        public List<Dashboard4DocumentModel> Dc { get; set; }
        public List<Dashboard4ProviderEmployeeModel> Ep { get; set; }
        public List<Dashboard4ProviderVehicleModel> Vc { get; set; }
    }
    public class Dashboard4ProviderVehicleModel
    {
        public decimal Id { get; set; }
        public string Nm { get; set; }
        public List<Dashboard4DocumentModel> Dc { get; set; }
    }
    public class Dashboard4ProviderEmployeeModel
    {
        public decimal Id { get; set; }
        public string Nd { get; set; }
        public string Fn { get; set; }
        public string Fi { get; set; }
        public List<Dashboard4DocumentModel> Dc { get; set; }
    }
    public class Dashboard4DocumentModel
    {
        public decimal Id { get; set; }
        public string Nm { get; set; }
        public decimal St { get; set; }
    }
}