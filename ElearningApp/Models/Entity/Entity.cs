﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;

namespace ElearningApp.Models.Entity
{
    public class Document_Entity : Audit_Entity
    {
        public decimal Id { get; set; }
        public string Description { get; set; }
        public string ShortDescription { get; set; }
        public string Type { get; set; }
        public File_Entity Model { get; set; }
        public decimal Order { get; set; }
        public string HasNumber { get; set; }
        public string HasDates { get; set; }
        public string HasMassive { get; set; }
        public string HasLogistica { get; set; }
        public string Status { get; set; }
    }
    public class Notice_Entity : Audit_Entity
    {
        public decimal Id { get; set; }
        public decimal AreaId { get; set; }
        public string Description { get; set; }
        public File_Entity Html { get; set; }
        public string Url { get; set; }
        public string Content { get; set; }
        public string Status { get; set; }
    }
    public class ProviderVehicleMassive_Entity : Audit_Entity
    {
        public decimal ProviderId { get; set; }
        public decimal DocumentId { get; set; }
        public string IssueDate { get; set; }
        public string ExpirationDate { get; set; }
        public string DocumentNumber { get; set; }
        public File_Entity DocumentFile { get; set; }
        //dt Vehicle
        public decimal[] VehicleId { get; set; }
        public string[] VehicleDocumentFileName { get; set; }
        public string[] VehicleDocumentFileRealName { get; set; }
        public bool[] VehicleDocumentFileUploaded { get; set; }
        public DataTable DtVehicle { get; set; }
    }
    public class ProviderVehicle_Entity : Audit_Entity
    {
        public decimal ProviderId { get; set; }
        //dt vehicle
        public decimal[] VehiclePrun { get; set; }
        public File_Entity[] VehicleFile { get; set; }
        public DataTable DtVehicle { get; set; }
        //dt document
        public decimal[] VehicleDocumentPrun { get; set; }
        public decimal[] VehicleDocumentPcdo { get; set; }
        public decimal[] VehicleDocumentTdos { get; set; }
        public string[] VehicleDocumentNumber { get; set; }
        public File_Entity[] VehicleDocumentFile { get; set; }
        public string[] VehicleDocumentIssueDate { get; set; }
        public string[] VehicleDocumentExpirationDate { get; set; }
        public string[] VehicleDocumentOldFileName { get; set; }
        public string[] VehicleDocumentOldFileRealName { get; set; }
        public DataTable DtDocument { get; set; }
        //dt delete
        public decimal[] DeleteDocumentPcdo { get; set; }
        public string[] DeleteDocumentName { get; set; }
        public DataTable DtDelete { get; set; }
    }
    public class ProviderEmployeeMassive_Entity : Audit_Entity
    {
        public decimal ProviderId { get; set; }
        public decimal DocumentId { get; set; }
        public string IssueDate { get; set; }
        public string ExpirationDate { get; set; }
        public string DocumentNumber { get; set; }
        public File_Entity DocumentFile { get; set; }
        //dt employee
        public decimal[] EmployeeId { get; set; }
        public string[] EmployeeDocumentFileName { get; set; }
        public string[] EmployeeDocumentFileRealName { get; set; }
        public bool[] EmployeeDocumentFileUploaded { get; set; }
        public DataTable DtEmployee { get; set; }
    }
    public class ProviderEmployee_Entity : Audit_Entity
    {
        public decimal ProviderId { get; set; }
        //dt employee
        public decimal[] EmployeePcon { get; set; }
        public File_Entity[] EmployeeFile { get; set; }
        public DataTable DtEmployee { get; set; }
        //dt document
        public decimal[] EmployeeDocumentPcon { get; set; }
        public decimal[] EmployeeDocumentPcdo { get; set; }
        public decimal[] EmployeeDocumentTdos { get; set; }
        public string[] EmployeeDocumentNumber { get; set; }
        public File_Entity[] EmployeeDocumentFile { get; set; }
        public string[] EmployeeDocumentIssueDate { get; set; }
        public string[] EmployeeDocumentExpirationDate { get; set; }
        public string[] EmployeeDocumentOldFileName { get; set; }
        public string[] EmployeeDocumentOldFileRealName { get; set; }
        public DataTable DtDocument { get; set; }
        //dt delete
        public decimal[] DeleteDocumentPcdo { get; set; }
        public string[] DeleteDocumentName { get; set; }
        public DataTable DtDelete { get; set; }
    }
    public class ProviderToApprove_Entity : Audit_Entity
    {
        public decimal ProviderId { get; set; }
        //dt provider
        public decimal[] ProviderPrdo { get; set; }
        public string[] ProviderSwitch { get; set; }
        public string[] ProviderReason { get; set; }
        public DataTable DtProvider { get; set; }
        //dt employee
        public decimal[] EmployeePcdo { get; set; }
        public string[] EmployeeSwitch { get; set; }
        public string[] EmployeeReason { get; set; }
        public DataTable DtEmployee { get; set; }
        //dt vehicle
        public decimal[] VehiclePcdo { get; set; }
        public string[] VehicleSwitch { get; set; }
        public string[] VehicleReason { get; set; }
        public DataTable DtVehicle { get; set; }
    }
    public class Provider_Entity : Audit_Entity
    {
        private string sapcode = "";
        private string address = "";
        private DateTime admissiondate = DateTime.ParseExact("1900-01-01 00:00:00", "yyyy-MM-dd HH:mm:ss",
            System.Globalization.CultureInfo.InvariantCulture);
        public decimal Id { get; set; }
        public string RUC { get; set; }
        public string Name { get; set; }
        public string BusinessName { get; set; }
        public string SAPCode
        {
            get
            {
                return sapcode;
            }
            set
            {
                if (value == null)
                {
                    sapcode = "";
                }
                else
                {
                    sapcode = value;
                }
            }
        }
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                if (value == null)
                {
                    address = "";
                }
                else
                {
                    address = value;
                }
            }
        }
        public DateTime AdmissionDate
        {
            get
            {
                return admissiondate;
            }
            set
            {
                if (value == default(DateTime) || value == null)
                {
                    admissiondate = DateTime.ParseExact("1900-01-01 00:00:00", "yyyy-MM-dd HH:mm:ss",
                        System.Globalization.CultureInfo.InvariantCulture);
                }
                else
                {
                    admissiondate = value;
                }
            }
        }
        public string Email { get; set; }
        public string HasRetention { get; set; }
        public decimal Retention { get; set; }
        public string Approved { get; set; }
        public string ProcessInitial { get; set; }
        public decimal ProcessId { get; set; }
        public string Process { get; set; }
        public string Status { get; set; }
        public string Actions { get; set; }
        public decimal AppId { get; set; }
        public string Password { get; set; }
        public string User { get; set; }
        public decimal IncotermsId { get; set; }
        public string IncotermsDescription { get; set; }
        //dt emails
        public decimal[] EmailRecordId { get; set; }
        public string[] EmailAddress { get; set; }
        public DataTable DtEmail { get; set; }
        //dt phones
        public decimal[] PhoneRecordId { get; set; }
        public string[] PhoneNumber { get; set; }
        public DataTable DtPhone { get; set; }
        //dt person
        public decimal[] PersonRecordId { get; set; } //codipcon
        public decimal[] PersonPersonId { get; set; }
        public decimal[] PersonDocumentType { get; set; }
        public string[] PersonDocumentNumber { get; set; }
        public string[] PersonLicenseNumber { get; set; }
        public string[] PersonLastname1 { get; set; }
        public string[] PersonLastname2 { get; set; }
        public string[] PersonFirstname { get; set; }
        public string[] PersonBirthday { get; set; }
        public string[] PersonEmail { get; set; }
        public string[] Modify { get; set; }//Trabajadores modificados
        public string[] Userid { get; set; }//userid
        public DataTable DtPerson { get; set; }
        //dt person phone
        public decimal[] PersonPhoneRecordId { get; set; }
        public decimal[] PersonPhonePersonRecordId { get; set; }
        public string[] PersonPhoneNumber { get; set; }
        public DataTable DtPersonPhone { get; set; }
        //dt vehicle
        public decimal[] VehicleRecordId { get; set; }
        public decimal[] VehicleTypeId { get; set; }
        public string[] VehiclePlate { get; set; }
        public DataTable DtVehicle { get; set; }
        //dt document
        public decimal[] DocumentRecordId { get; set; }
        public decimal[] DocumentTypeId { get; set; }
        public string[] DocumentHasDate { get; set; }
        public string[] DocumentIssueDate { get; set; }
        public string[] DocumentExpirationDate { get; set; }
        public string[] DocumentOldFileName { get; set; }
        public string[] DocumentOldFileRealname { get; set; }
        public File_Entity[] DocumentFile { get; set; }
        public DataTable DtDocument { get; set; }
        //dt account
        public decimal[] AccountRecordId { get; set; }
        public decimal[] AccountTypeId { get; set; }
        public string[] AccountNumber { get; set; }
        public decimal[] AccountBank { get; set; }
        public decimal[] AccountMoney { get; set; }
        public string[] AccountOldFileName { get; set; }
        public string[] AccountOldFileRealname { get; set; }
        public File_Entity[] AccountFile { get; set; }
        public DataTable DtAccount { get; set; }
        //dt observation
        public DataTable DtObservation { get; set; }
        //
        public List<object> Emails { get; set; }
        public List<object> Phones { get; set; }
        public List<object> Employees { get; set; }
        public List<object> Vehicles { get; set; }
        public List<object> Accounts { get; set; }
        public IEnumerable<ProviderDocument_Model> Documents { get; set; }
        public IEnumerable<ProviderDocumentLogistica_Model> LogisticaDocuments { get; set; }
        public int[] pa_del_codi_pcon { get; set; }
        public DataTable DtDelCodiPcon { get; set; }
        public DataTable DtDnisCovid { get; set; }

    }
    public class RegisterPoll_Entity : Audit_Entity
    {
        public decimal RegisterId { get; set; }
        public string HasComment { get; set; }
        public string CommentQuestion { get; set; }
        public string Comment { get; set; }
        //dt question
        public decimal[] QtnId { get; set; }
        public string[] QtnQuestion { get; set; }
        public decimal[] QtnOrder { get; set; }
        public decimal[] QtnScore { get; set; }
        public DataTable DtQuestion { get; set; }
    }
    public class Poll_Entity : Audit_Entity
    {
        public decimal Id { get; set; }
        public string Name { get; set; }
        public string HasComment { get; set; }
        public string CommentQuestion { get; set; }
        public string Status { get; set; }
        //
        public List<object> Questions { get; set; }
    }
    public class InteractiveContent_Entity : Audit_Entity
    {
        public decimal Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Directory { get; set; }
        public string Status { get; set; }
        public string OldDirectory { get; set; }
        public bool WasRetrieved { get; set; }
        public bool WasChoosed { get; set; }
        //dt directory
        public File_Entity[] Files { get; set; }
    }
    public class ChangePassword_Entity : Audit_Entity
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string UserId { get; set; }
    }
    public class Parameter_Entity : Audit_Entity
    {
        public string CarrierCourseId { get; set; }
        public string WaitTimeCloseSession { get; set; }
        public int iWaitTimeCloseSession { get; set; }
        public string MessageFinalized { get; set; }
        public string MessageApproved { get; set; }
        public string MessageDisapproved { get; set; }
    }
    public class Profiles_Entity : Audit_Entity
    {
        public decimal AppProfileId { get; set; }
        public int FromProvider { get; set; }
        //dt profiles
        public decimal[] PrfRecordId { get; set; }
        public string[] PrfUsername { get; set; }
        public DataTable DtProfile { get; set; }
        //dt providers
        public decimal[] ProviderId { get; set; }
        public string[] ProviderBusinessName { get; set; }
        public string[] ProviderEmail { get; set; }
        public string[] ProviderUsername { get; set; }
        public string[] ProviderPassword { get; set; }
    }
    public class Certificate_Entity : Audit_Entity
    {
        public decimal Id { get; set; }
        public string Description { get; set; }
        public string CertificateJSON { get; set; }
        public string Status { get; set; }
        public File_Entity[] Files { get; set; }
        public string DateFormat { get; set; }
        public string[] DeletedFiles { get; set; }
    }
    public class Instructor_Entity : Audit_Entity
    {
        private string email = "";
        public decimal Id { get; set; }
        public decimal PersonId { get; set; }
        public decimal DocumentTypeId { get; set; }
        public string DocumentNumber { get; set; }
        public string LastName1 { get; set; }
        public string LastName2 { get; set; }
        public string FirstName { get; set; }
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                if (value == null)
                {
                    email = "";
                }
                else
                {
                    email = value;
                }
            }
        }
        public string Type { get; set; }
        public string PersonType { get; set; }
        public string SavePerson { get; set; }
        public string Status { get; set; }
    }
    public class Place_Entity : Audit_Entity
    {
        public decimal Id { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
    }
    public class Attempt_Entity : Audit_Entity
    {
        public decimal Id { get; set; }
        public int Seconds { get; set; }
        public string Answered { get; set; }
        public string Time { get; set; }
        //dt answer
        public decimal[] AnswerId { get; set; }
        public decimal[] AnswerOrder { get; set; }
        public DataTable DtAnswer { get; set; }
    }
    public class Category_Entity : Audit_Entity
    {
        public decimal Id { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
    }
    public class AreaProvider_Entity : Audit_Entity
    {
        public decimal AreaId { get; set; }
        public int FromProvider { get; set; }
        //dt provider
        public decimal[] PrvRecordId { get; set; }
        public decimal[] PrvProviderId { get; set; }
        public DataTable DtProvider { get; set; }
    }
    public class ProgrammingRegisterMassive_Entity : Audit_Entity
    {
        public decimal ProviderId { get; set; }
        public decimal ProgrammingId { get; set; }
        public decimal Item { get; set; }
        //dt register
        public decimal[] RgRecordId { get; set; }
        public decimal[] RgPersonId { get; set; }
        public DataTable DtRegister { get; set; }
        //dt event
        public decimal[] EvtRecordIdA { get; set; }
        public decimal[] EvtRecordIdB { get; set; }
        public decimal[] EvtRecordIdC { get; set; }
        public DataTable DtEvent { get; set; }
    }
    public class ProgrammingRegister_Entity : Audit_Entity
    {
        public decimal PersonId { get; set; }
        public decimal ProgrammingId { get; set; }
        public decimal Item { get; set; }
    }
    public class DestinationEmail_Entity : Audit_Entity
    {
        public decimal TemplateId { get; set; }
        public string ProcessName { get; set; }
        public decimal ProcessCode { get; set; }
        public string GroupName { get; set; }
        public decimal GroupCode { get; set; }
        public string DestinationName { get; set; }
        public string DestinationEmail { get; set; }
    }
    public class Programming_Entity : Audit_Entity
    {
        private string capacitacionfromhour = "";
        private string capacitaciontohour = "";
        public decimal Id { get; set; }
        public bool Edit { get; set; }
        public string EditMessage { get; set; }
        public string Name { get; set; }
        public decimal CourseId { get; set; }
        public decimal ModeId { get; set; }
        public string Mode { get; set; }
        public decimal Vacancies { get; set; }
        public string HasTest { get; set; }
        public decimal TestId { get; set; }
        public decimal Attempts { get; set; }
        public string HasInstructor { get; set; }
        public decimal InstructorId { get; set; }
        public string InstructorFullname { get; set; }
        public string InstructorDocument { get; set; }
        public string InstructorDocumentNumber { get; set; }
        public string ShowContent { get; set; }
        public decimal PlaceId { get; set; }
        public string PlaceDescription { get; set; }
        public DateTime RegistrationLimit { get; set; }
        public string sRegistrationLimit { get; set; }
        public string ApprovalType { get; set; }
        public decimal MinimumAssistance { get; set; }
        public string sMinimumAssistance { get; set; }
        public string ProgrammingType { get; set; }
        public DateTime FromDate { get; set; }
        public string sFromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string sToDate { get; set; }
        public string FromHour { get; set; }
        public string ToHour { get; set; }
        public decimal Duration { get; set; }
        public string DurationScale { get; set; }
        public decimal Interval { get; set; }
        public string IntervalScale { get; set; }
        public string Weekend { get; set; }
        public string AccessType { get; set; }
        public string PersonType { get; set; }
        public string AllArea { get; set; }
        public string UnlimitedRegistration { get; set; }
        public string UnlimitedProgramming { get; set; }
        public string HasCertificate { get; set; }
        public decimal CertificateId { get; set; }
        public string HasPoll { get; set; }
        public decimal PollId { get; set; }
        public decimal CapacitacionHours { get; set; }
        public string CapacitacionFromHour
        {
            get
            {
                return capacitacionfromhour;
            }
            set
            {
                if (value == null)
                {
                    capacitacionfromhour = "";
                }
                else
                {
                    capacitacionfromhour = value;
                }
            }
        }
        public string CapacitacionToHour
        {
            get
            {
                return capacitaciontohour;
            }
            set
            {
                if (value == null)
                {
                    capacitaciontohour = "";
                }
                else
                {
                    capacitaciontohour = value;
                }
            }
        }
        public string Status { get; set; }
        //dt calendar
        public decimal[] CalRecordId { get; set; }
        public DateTime[] CalDate { get; set; }
        public string[] CalFromHour { get; set; }
        public string[] CalToHour { get; set; }
        public DataTable DtCalendar { get; set; }
        //dt person
        public decimal[] PerRecordId { get; set; }
        public decimal[] PerPersonId { get; set; }
        public DataTable DtPerson { get; set; }
        //dt area
        public decimal[] AreaRecordId { get; set; }
        public decimal[] AreaAreaId { get; set; }
        public DataTable DtArea { get; set; }
        //
        public List<object> Areas { get; set; }
        public List<object> Persons { get; set; }
        public List<object> Events { get; set; }

        public decimal ProfilId { get; set; }
    }
    public class Test_Entity : Audit_Entity
    {
        public decimal Id { get; set; }
        public decimal ProgrammingId { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public decimal CourseId { get; set; }
        public decimal MinScore { get; set; }
        public decimal MaxScore { get; set; }
        public decimal ApproveScore { get; set; }
        public string TestType { get; set; }
        public string Proceed { get; set; }
        public string Time { get; set; }
        public decimal TimeLimit { get; set; }
        public string RandomQuestions { get; set; }
        public decimal QuestionsNumber { get; set; }
        //dt question
        public decimal[] QRecord { get; set; }
        public decimal[] QQuestionId { get; set; }
        public decimal[] QOrder { get; set; }
        public DataTable DtQuestion { get; set; }
        //
        public List<object> Questions { get; set; }
        public int CurrentTime { get; set; }
        public decimal AttemptId { get; set; }
    }
    public class Question_Entity : Audit_Entity
    {
        public decimal Id { get; set; }
        public decimal CourseId { get; set; }
        public decimal TypeId { get; set; }
        public string TypeMode { get; set; }
        public string Question { get; set; }
        public string Status { get; set; }
        //dt answer
        public decimal[] AnswerId { get; set; }
        public string[] AnswerText { get; set; }
        public string[] AnswerCorrect { get; set; }
        public int[] AnswerOrder { get; set; }
        public DataTable DtAnswer { get; set; }
        //
        public List<object> Answers { get; set; }
    }
    public class Course_Entity : Audit_Entity
    {
        public decimal Id { get; set; }
        public string Area { get; set; }
        public decimal AreaId { get; set; }
        public string Name { get; set; }
        public string OldName { get; set; }
        public string Status { get; set; }
        public string Category { get; set; }
        public decimal CategoryId { get; set; }
        public string Classification { get; set; }
        public string Publish { get; set; }
        public File_Entity ImageFile { get; set; }
        public string ImageName { get; set; }
        public string ImageUrl { get; set; }
        public string ImageRetrieved { get; set; }
        public string FrontPage { get; set; }
        public string Objetives { get; set; }
        public string Summary { get; set; }
        public string StorageUrl { get; set; }
        public string HasValidity { get; set; }
        public decimal Validity { get; set; }
        public string ValidityScale { get; set; }
        //dt detail
        public decimal[] ThemeId { get; set; }
        public string[] ThemeName { get; set; }
        public decimal[] ThemeOrder { get; set; }
        public decimal[] ThemeItemId { get; set; }
        public decimal[] ThemeItemTypeId { get; set; }
        public string[] ThemeItemTitle { get; set; }
        public decimal[] ThemeItemOrder { get; set; }
        public string[] ThemeItemText { get; set; }
        public string[] ThemeItemUrlYoutube { get; set; }
        public string[] ThemeItemUrlSiderChannel { get; set; }
        public string[] ThemeItemName { get; set; }
        public decimal[] ThemeItemInteractive { get; set; }
        public File_Entity[] ThemeItemFiles { get; set; }
        public File_Entity[] ThemeItemRemoved { get; set; }
        public DataTable DtDetail { get; set; }
        //
        public List<object> Themes { get; set; }
    }
    public class File_Entity : Audit_Entity
    {
        private string extension = "";
        private string name = "";
        private string oldname = "";
        private string path = "";
        private string properties = "";
        private string realname = "";
        private string thumbnail = "";
        public decimal Item { get; set; }
        public HttpPostedFileBase File { get; set; }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value == null)
                {
                    name = "";
                }
                else
                {
                    name = value;
                }
            }
        }
        public string OldName
        {
            get
            {
                return oldname;
            }
            set
            {
                if (value == null)
                {
                    oldname = "";
                }
                else
                {
                    oldname = value;
                }
            }
        }
        public string Path
        {
            get
            {
                return path;
            }
            set
            {
                if (value == null)
                {
                    path = "";
                }
                else
                {
                    path = value;
                }
            }
        }
        public string Properties
        {
            get
            {
                return properties;
            }
            set
            {
                if (value == null)
                {
                    properties = "";
                }
                else
                {
                    properties = value;
                }
            }
        }
        public string Thumbnail
        {
            get
            {
                return thumbnail;
            }
            set
            {
                if (value == null)
                {
                    thumbnail = "";
                }
                else
                {
                    thumbnail = value;
                }
            }
        }
        public string Extension
        {
            get
            {
                return extension;
            }
            set
            {
                if (value == null)
                {
                    extension = "";
                }
                else
                {
                    extension = value;
                }
            }
        }
        public string RealName
        {
            get
            {
                return realname;
            }
            set
            {
                if (value == null)
                {
                    realname = "";
                }
                else
                {
                    realname = value;
                }
            }
        }
        public bool WasRetrieved { get; set; }
        public bool WasRemoved { get; set; }
        public bool WasChoosed { get; set; }
        public bool Uploaded { get; set; }
    }
    public class Register_Entity
    {
        private string phone = "";
        public string VerificationCode { get; set; }
        public string LastName1 { get; set; }
        public string LastName2 { get; set; }
        public string FirstName { get; set; }
        public decimal DocumentType { get; set; }
        public string DocumentNumber { get; set; }
        public string Email { get; set; }
        public string Phone
        {
            get
            {
                return phone;
            }
            set
            {
                if (value == null)
                {
                    phone = "";
                }
                else
                {
                    phone = value;
                }
            }
        }
        public string IpAddress { get; set; }
        public string Actions { get; set; }
        public decimal RegisteredId { get; set; }
        public string Origin { get; set; }
    }

    public class Upd_Note_Entity
    {
        public int CodiPins { get; set; }
        public decimal Nota { get; set; }
    }

    public class Send_Recordatorio_Entity
    {
        public Int64 ProgrammingId { get; set; }
        public List<string> Fichas { get; set; }
        public DataTable DtFichas { get; set; }
    }

    public class Api_Note_Entity {
        public int RegisterId { get; set; }
        public int ContentId { get; set; }
        public int CurrentAttempt { get; set; }
        public decimal Score { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public class ResponseSGS
    {
        public string data { get; set; }
        public int errorCode { get; set; }
        public string message { get; set; }

    }

    public class Usuario_Sgs
    {
        public string codigo { get; set; }
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public string documento { get; set; }
        public string tipo { get; set; }
        public string fechaNacimiento { get; set; }
        public string unidadNegocio { get; set; }
        public string codUnidadNegocio { get; set; }
        public string modalidad { get; set; }
        public string sexo { get; set; }
        public string codigoTrabajador { get; set; }
        public string grupoRiesgo { get; set; }
        public Boolean activo { get; set; }
    }

    public class UsuarioHabDes
    {
        public string documento { get; set; }
        public string activo { get; set; }
    }

    public class Api_Auth_Entity
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public abstract class Audit_Entity
    {
        public string Username { get; set; }
        public string IPAddress { get; set; }
    }
    /* Codigo de Aplicacion - No manipular - */
    #region App
    public class Profile_Entity
    {
        public string Description { get; set; }
        public decimal AreaId { get; set; }
        public string AreaDescription { get; set; }
        public decimal PlaceId { get; set; }
        public string Group { get; set; }
    }
    public class User_Entity
    {
        public decimal PersonId { get; set; }
        public decimal PersonProviderId { get; set; }
        public decimal ProviderId { get; set; }
        public string FullName { get; set; }
        public string PersonType { get; set; }
        public decimal EmployeeId { get; set; }
        public string EmployeeFichaSap { get; set; }
        public string PersonDocumentNumber { get; set; }
        public int ProviderIncotermsId { get; set; }
    }
    public class Log_Entity
    {
        private string controllername = "";
        private string actionname = "";
        public string ControllerName
        {
            get
            {
                return controllername;
            }
            set
            {
                if (value == null)
                {
                    controllername = "";
                }
                else
                {
                    controllername = value;
                }
            }
        }
        public string ActionName
        {
            get
            {
                return actionname;
            }
            set
            {
                if (value == null)
                {
                    actionname = "";
                }
                else
                {
                    actionname = value;
                }
            }
        }
        public decimal SessionId { get; set; }
    }
    public class Session_Entity
    {
        private string pcname = "";
        private string information = "";
        public decimal AppId { get; set; }
        public decimal ProfileId { get; set; }
        public string Username { get; set; }
        public string Pcname
        {
            get
            {
                return pcname;
            }
            set
            {
                if (value == null)
                {
                    pcname = "";
                }
                else
                {
                    pcname = value;
                }
            }
        }
        public string IPAddress { get; set; }
        public decimal SessionId { get; set; }
        public string Information
        {
            get
            {
                return information;
            }
            set
            {
                if (value == null)
                {
                    information = "";
                }
                else
                {
                    information = value;
                }
            }
        }
    }
    public class AppObject_Entity
    {
        public decimal Id { get; set; }
        public decimal ParentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Icon { get; set; }
        public decimal Type { get; set; }
        public string Color { get; set; }
        public decimal Order { get; set; }
        public bool Completed { get; set; }
        public bool HasChildren { get; set; }
    }
    #endregion App
}