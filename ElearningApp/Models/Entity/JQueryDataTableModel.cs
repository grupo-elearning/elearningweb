﻿namespace ElearningApp.Models.Entity
{
    /// <summary>
    /// Parámetros enviados al servidor
    /// </summary>
    public class JQueryDataTableModel
    {
        /// <summary>
        /// Indicador de si se necesita recuperar la informacion desde la base de datos
        /// </summary> 
        public bool bRetrieve { get; set; }
        /// <summary>
        /// Mostrar el punto de inicio en el conjunto de datos actual
        /// </summary>       
        public int iDisplayStart { get; set; }
        /// <summary>
        /// Número de registros que la tabla puede mostrar en el sorteo actual.
        /// Se espera que el número de registros devueltos sea igual a este número,
        /// a menos que el servidor tenga menos registros para devolver
        /// </summary>       
        public int iDisplayLength { get; set; }
        /// <summary>
        /// Número de columnas que se muestran (útil para obtener información
        /// de búsqueda de columna individual)
        /// </summary>       
        public int iColumns { get; set; }
        /// <summary>
        /// Campo de búsqueda global
        /// </summary>       
        public string sSearch { get; set; }
        /// <summary>
        /// Verdadero si el filtro global debe tratarse como una expresión regular
        /// para el filtrado avanzado, falso si no es así
        /// </summary>       
        public bool bRegex { get; set; }
        /// <summary>
        /// Indicador de si una columna está marcada como buscable o no en el
        /// lado del cliente
        /// </summary>       
        //public bool bSearchable_ { get; set; }
        /// <summary>
        /// Filtro de columna individual
        /// </summary>       
        //public string sSearch_ { get; set; }
        /// <summary>
        /// Verdadero si el filtro de columna individual debe tratarse como
        /// una expresión regular para filtrado avanzado, falso si no
        /// </summary>       
        //public bool bRegex_ { get; set; }
        /// <summary>
        /// Indicador de si una columna está marcada como ordenable o
        /// no en el lado del cliente
        /// </summary>       
        //public bool bSortable_ { get; set; }
        /// <summary>
        /// Número de columnas para ordenar
        /// </summary>       
        public int iSortingCols { get; set; }
        /// <summary>
        /// Columna ordenada (necesitará decodificar este número para
        /// su base de datos)
        /// </summary>   
        public int iSortCol_0 { get; set; }//solo si iSortingCols = 1
        /// <summary>
        /// Dirección que se ordenará: "desc" o "asc"
        /// </summary>       
        public string sSortDir_0 { get; set; }//solo si iSortingCols = 1
        /// </summary>
        /// El valor especificado por mDataProp para cada columna.
        /// Esto puede ser útil para garantizar que el procesamiento
        /// de datos sea independiente del orden de las columnas
        /// </summary>       
        //public string mDataProp_ { get; set; }
        /// </summary>
        /// Información para DataTables a usar para renderizar
        /// </summary>       
        public string sEcho { get; set; }
        /// <sumary>
        /// Uso provicional
        /// </sumary>
        public string sSearch_0 { get; set; }//columna 0
        public string sSearch_1 { get; set; }//columna 1
        public string sSearch_2 { get; set; }//columna 2
        public string sSearch_3 { get; set; }//columna 3
        public string sSearch_4 { get; set; }//columna 4
        public string sSearch_5 { get; set; }//columna 5
        public string sSearch_6 { get; set; }//columna 6
        public string sSearch_7 { get; set; }//columna 7
        public string sSearch_8 { get; set; }//columna 8
        public string sSearch_9 { get; set; }//columna 9
        public string sSearch_10 { get; set; }//columna 10
        public string sSearch_11 { get; set; }//columna 11
        public string sSearch_12 { get; set; }//columna 12
        public string sSearch_13 { get; set; }//columna 13
        public string sSearch_14 { get; set; }//columna 14
        public string sSearch_15 { get; set; }//columna 15
        public string sSearch_16 { get; set; }//columna 16
        public string sSearch_17 { get; set; }//columna 17
        public string sSearch_18 { get; set; }//columna 18
        public string sSearch_19 { get; set; }//columna 19
        public string sSearch_20 { get; set; }//columna 20
    }
}